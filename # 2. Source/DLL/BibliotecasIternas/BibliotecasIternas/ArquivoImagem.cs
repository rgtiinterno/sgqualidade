﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Net.Mime;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

namespace BibliotecasInternas
{
    public class ArquivoImagem
    {
        private static byte[] CompressBytes(byte[] data)
        {
            // Fonte: http://stackoverflow.com/a/271264/194717
            using (var compressedStream = new MemoryStream())
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
            {
                zipStream.Write(data, 0, data.Length);
                zipStream.Close();
                return compressedStream.ToArray();
            }
        }

        private static byte[] DecompressBytes(byte[] bSource)
        {
            // Fonte: http://stackoverflow.com/questions/6350776/help-with-programmatic-compression-decompression-to-memorystream-with-gzipstream
            using (var inStream = new MemoryStream(bSource))
            using (var gzip = new GZipStream(inStream, CompressionMode.Decompress))
            using (var outStream = new MemoryStream())
            {
                gzip.CopyTo(outStream);
                return outStream.ToArray();
            }
        }

        /// <summary>
        /// Função responsável por gerar uma imagem compactada para salvar no banco de dados
        /// </summary>
        /// <param name="arquivo">Arquivo a ser comprimido</param>
        /// <returns></returns>
        public static byte[] EncriptImage(string arquivo)
        {
            byte[] arrayDeBytes;

            using (Stream stream = new FileStream(arquivo, FileMode.Open))
            {
                arrayDeBytes = new byte[stream.Length + 1];
                stream.Read(arrayDeBytes, 0, arrayDeBytes.Length);
            }

            return CompressBytes(arrayDeBytes);
        }

        /// <summary>
        ///  Função responsável por descompactar uma imagem compactada 
        /// </summary>
        /// <param name="bytes">Arquivo compactado</param>
        /// <param name="nomeArquivo">nome do arquivo que irá receber os dados descompactados</param>
        /// <returns></returns>
        public static string DescriptImage(byte[] bytes, string nomeArquivo)
        {
            byte[] bytesArquivo = (bytes);

            string sFile = nomeArquivo;
            File.WriteAllBytes(sFile, DecompressBytes(bytesArquivo));

            return sFile;
        }
    }
}
