﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Setores
{
    public partial class FormSetores : Form
    {
        public bool salvo = false;
        private GSETORES itemEdit;

        public FormSetores(GSETORES setores = null)
        {
            InitializeComponent();
            itemEdit = setores;
        }

        private void FormSetores_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtCodigo.ReadOnly = true;
                txtDescricao.Text = itemEdit.DESCRICAO;
            }
            else
            {
                using (ControleGSetores controle = new ControleGSetores())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                    txtCodigo.ReadOnly = true;
                }
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDescricao.Text))
            {
                GSETORES item = new GSETORES
                {
                    CODIGO = Convert.ToInt16(txtCodigo.Text),
                    CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    DESCRICAO = txtDescricao.Text
                };

                using (ControleGSetores controle = new ControleGSetores())
                {
                    if (itemEdit != null)
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO && x.CODCOLIGADA == itemEdit.CODCOLIGADA);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe a descrição");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}