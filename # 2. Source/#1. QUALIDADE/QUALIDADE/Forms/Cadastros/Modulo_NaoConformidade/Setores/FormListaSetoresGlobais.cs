﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Setores
{
    public partial class FormListaSetoresGlobais : Form
    {
        public FormListaSetoresGlobais()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"SetoresGlobais.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListaSetoresGlobais_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleGSetores controle = new ControleGSetores())
            {
                short CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
                Cursor.Current = Cursors.WaitCursor;
                gSETORESBindingSource = new BindingSource();
                gSETORESBindingSource.DataSource = typeof(GSETORES);
                gSETORESBindingSource.DataSource = controle.GetAll().Where(x => x.CODCOLIGADA == CODCOLIGADA).ToList();
                dgvListagem.DataSource = gSETORESBindingSource;
                gSETORESBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {gSETORESBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormSetores frm = new FormSetores())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    tsbAtualizar.PerformClick();
                }

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is GSETORES item && item.CKSELECIONADO)
            {
                using (FormSetores frm = new FormSetores(item))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        tsbAtualizar.PerformClick();
                    }

                    frm.Dispose();
                }
            }
            else
                QUALIDADE.Dominio.Global.MsgErro("Selecione ao menos um registro.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is GSETORES item && item.CKSELECIONADO)
            {
                using (ControleGSetores controle = new ControleGSetores())
                {
                    controle.Delete(x => x.CODIGO == item.CODIGO && x.CODCOLIGADA == item.CODCOLIGADA);
                    controle.SaveAll();
                    tsbAtualizar.PerformClick();
                }
            }
            else
               Global.MsgErro("Selecione ao menos um registro.");
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }
    }
}