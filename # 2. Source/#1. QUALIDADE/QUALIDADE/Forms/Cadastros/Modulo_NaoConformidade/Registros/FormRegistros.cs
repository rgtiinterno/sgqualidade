﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloNC.Registros
{
    public partial class FormRegistros : Form
    {
        public bool salvo = false;
        private CTPREGISTRO tpRegistro;

        public FormRegistros(CTPREGISTRO registro = null)
        {
            InitializeComponent();
            tpRegistro = registro;

            if (tpRegistro != null)
                txtNome.Text = tpRegistro.DESCRICAO;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNome.Text))
            {
                using (ControleCTPRegistros controle = new ControleCTPRegistros())
                {
                    CTPREGISTRO registro = new CTPREGISTRO
                    {
                        DESCRICAO = txtNome.Text
                    };

                    if (tpRegistro == null)
                    {
                        registro.CODREGISTRO = controle.GetNewID();
                        controle.Create(registro);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        registro.CODREGISTRO = tpRegistro.CODREGISTRO;
                        controle.Update(registro, x => x.CODREGISTRO == tpRegistro.CODREGISTRO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe o tipo de registro.");
        }
    }
}
