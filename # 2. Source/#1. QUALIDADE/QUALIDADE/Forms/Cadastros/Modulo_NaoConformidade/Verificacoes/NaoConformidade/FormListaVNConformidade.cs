﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.NaoConformidade;
using QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.EficaciaAcao;
using QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.PlanoAcao;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Enums;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade
{
    public partial class FormListaVNConformidade : Form
    {
        private string nomeForm;
        public FormListaVNConformidade(string NomeForm)
        {
            InitializeComponent();
            nomeForm = NomeForm;
        }

        private void FormListaVNConformidade_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODNCONFORM", HeaderText = "Cód. Conformidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. C.Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCCUSTO", HeaderText = "Cód. C.Custo", AllowEditing = false, AllowFiltering = true, Visible = true });

            if (nomeForm == "PA")
            {
                this.Text = "Verificação de Plano de Ação";

                tsbVerPlanoAcao.Visible = true;
                tsbVerEficacia.Visible = false;

                dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "CUMPRIDO", HeaderText = "Plano de Ação Cumprido", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVERIFPACAO", HeaderText = "Dt. Verif. Plano Ação", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTLIMITEVEFICACIA", HeaderText = "Dt. Limite Verif. Efic. Ação", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUVERIFPACAO", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEUSUVERIFPACAO", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACAOPACAO", HeaderText = "Comentários", AllowEditing = false, AllowFiltering = true, Visible = true });

                dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "ACAOEFICAZ", HeaderText = "Ação Eficaz", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTVEFICACIACIA", HeaderText = "Dt. Verif. Eficácia Ação", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridNumericColumn() { MappingName = "NUMSACP", HeaderText = "Núm. SACP", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUVERIFEFIC", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEUSUVERIFEFIC", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACAOEFIC", HeaderText = "Comentários", AllowEditing = false, AllowFiltering = true, Visible = false });

            }
            else
            {
                this.Text = "Verificação de Eficácia da Ação";

                tsbVerPlanoAcao.Visible = false;
                tsbVerEficacia.Visible = true;

                dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "CUMPRIDO", HeaderText = "Plano de Ação Cumprido", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVERIFPACAO", HeaderText = "Dt. Verif. Plano Ação", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTLIMITEVEFICACIA", HeaderText = "Dt. Limite Verif. Efic. Ação", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUVERIFPACAO", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEUSUVERIFPACAO", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACAOPACAO", HeaderText = "Comentários", AllowEditing = false, AllowFiltering = true, Visible = false });

                dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "ACAOEFICAZ", HeaderText = "Ação Eficaz", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTVEFICACIACIA", HeaderText = "Dt. Verif. Eficácia Ação", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridNumericColumn() { MappingName = "NUMSACP", HeaderText = "Núm. SACP", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUVERIFEFIC", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = false });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEUSUVERIFEFIC", HeaderText = "Verificador", AllowEditing = false, AllowFiltering = true, Visible = true });
                dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACAOEFIC", HeaderText = "Comentários", AllowEditing = false, AllowFiltering = true, Visible = true });

            }

            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
            {
                Cursor.Current = Cursors.WaitCursor;
                cVPACAOEFICACIABindingSource = new BindingSource();
                cVPACAOEFICACIABindingSource.DataSource = typeof(CVPACAOEFICACIA);
                cVPACAOEFICACIABindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, nomeForm).ToList();
                dgvListagem.DataSource = cVPACAOEFICACIABindingSource;
                cVPACAOEFICACIABindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {cVPACAOEFICACIABindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbVerPlanoAcao.Enabled = true;
                    tsbVerEficacia.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbVerPlanoAcao.Enabled = false;
                    tsbVerEficacia.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbVerPlanoAcao_Click(object sender, EventArgs e)
        {
            CVPACAOEFICACIA cNaoConform = (dgvListagem.SelectedItem as CVPACAOEFICACIA);

            if (cNaoConform.CKSELECIONADO)
            {
                using (ControleCNConformidade controle = new ControleCNConformidade())
                {
                    CNCONFORM nConf = controle.Find(
                        cNaoConform.CODNCONFORM, cNaoConform.CODCOLIGADA, cNaoConform.CODCCUSTO);

                    if (nConf.FINALIZADORELATOR && nConf.FINALIZADORESPONSAVEL)
                    {
                        using (FormVerificarPlanoAcao frm = new FormVerificarPlanoAcao(cNaoConform))
                        {
                            frm.StartPosition = FormStartPosition.CenterParent;
                            frm.WindowState = FormWindowState.Normal;
                            frm.ShowDialog();

                            if (frm.salvo)
                                tsbAtualizar.PerformClick();

                            frm.Dispose();
                        }
                    }
                    else
                        Global.MsgErro("Conformidade Ainda não concluída ou não finalizada");
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbVerEficacia_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CVPACAOEFICACIA item && item.CKSELECIONADO)
            {
                using (ControleCNConformidade controle = new ControleCNConformidade())
                {
                    CNCONFORM nConform = controle.Find(item.CODNCONFORM, item.CODCOLIGADA, item.CODCCUSTO);
                    if (nConform.FINALIZADORESPONSAVEL)
                    {
                        if (nConform.CODRELATOR == FormPrincipal.getUsuarioAcesso().CODUSUARIO)
                        {
                            using (FormVerficarEficacia frm = new FormVerficarEficacia(item))
                            {
                                frm.StartPosition = FormStartPosition.CenterParent;
                                frm.WindowState = FormWindowState.Normal;
                                frm.ShowDialog();

                                if (frm.salvo)
                                {
                                    if (!frm.eficaz)
                                    {
                                        using (FormNConformidade form = new FormNConformidade())
                                        {
                                            form.StartPosition = FormStartPosition.CenterParent;
                                            form.WindowState = FormWindowState.Normal;
                                            form.ShowDialog();

                                            if (form.salvo)
                                                tsbAtualizar.PerformClick();

                                            frm.Dispose();
                                        }
                                    }

                                    tsbAtualizar.PerformClick();
                                }
                            }
                        }
                        else
                            Global.MsgSemPermissao();
                    }
                    else
                        Global.MsgInformacao(" - Não conformidade ainda não foi concluída.");
                }
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            try
            {
                if (e.RowType == RowType.DefaultRow)
                {
                    if (e.RowData is CVPACAOEFICACIA item && item.DTLIMITEVEFICACIA.HasValue && item.ACAOEFICAZ == null)
                    {
                        if (item.DTLIMITEVEFICACIA.Value.Subtract(DateTime.Now).TotalDays < 0)
                        {
                            e.Style.BackColor = Color.FromArgb(215, 134, 142);
                        }
                        else if (item.DTLIMITEVEFICACIA.Value.Subtract(DateTime.Now).TotalDays >= 0 &&
                                    item.DTLIMITEVEFICACIA.Value.Subtract(DateTime.Now).TotalDays <= 5
                                )
                        {
                            e.Style.BackColor = Color.LightGoldenrodYellow;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex);
                frm.ShowDialog();
            }
        }
    }
}
