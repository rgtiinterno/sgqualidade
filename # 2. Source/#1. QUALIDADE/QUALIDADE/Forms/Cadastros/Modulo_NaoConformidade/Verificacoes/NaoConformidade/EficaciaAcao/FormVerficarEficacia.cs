﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.EficaciaAcao
{
    public partial class FormVerficarEficacia : Form
    {
        private CVPACAOEFICACIA cVerificarPAEF; 
        public bool salvo = false;
        public bool eficaz = true;

        public FormVerficarEficacia(CVPACAOEFICACIA cVerifPAEF)
        {
            InitializeComponent();
            cVerificarPAEF = cVerifPAEF;
        }

        private void FormVerficarEficacia_Load(object sender, EventArgs e)
        {
            ckSim.Checked = (cVerificarPAEF.ACAOEFICAZ.HasValue) ? cVerificarPAEF.ACAOEFICAZ.Value : false;
            txtNumSACP.Text = (cVerificarPAEF.NUMSACP.HasValue) ? cVerificarPAEF.NUMSACP.Value.ToString() : null;
            txtObservacao.Text = cVerificarPAEF.OBSERVACAOEFIC;

            if (cVerificarPAEF.ACAOEFICAZ.HasValue)
            {
                btnGravar.Visible = false;
                ckSim.Enabled = false;
                ckNao.Enabled = false;
                txtObservacao.ReadOnly = true;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtObservacao.Text))
            {
                try
                {
                    using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
                    {
                        using (ControleCCNConformHst controleHst = new ControleCCNConformHst())
                        {
                            using (ControleCNConformidade controleNConformidade = new ControleCNConformidade())
                            {
                                List<CCNCONFORMHST> listaHistoricos = new List<CCNCONFORMHST>();
                                CNCONFORM nConformidade = controleNConformidade.Find(cVerificarPAEF.CODNCONFORM, cVerificarPAEF.CODCOLIGADA, cVerificarPAEF.CODCCUSTO);

                                cVerificarPAEF.ACAOEFICAZ = ckSim.Checked;
                                cVerificarPAEF.NUMSACP = txtNumSACP.Text.DBNullToInt();
                                cVerificarPAEF.OBSERVACAOEFIC = txtObservacao.Text;
                                cVerificarPAEF.CODUSUVERIFEFIC = FormPrincipal.getUsuarioAcesso().CODUSUARIO;
                                cVerificarPAEF.DTVEFICACIACIA = DateTime.Now;
                                nConformidade.VERIFICACAOPLANOEFICACIA = true;

                                int ID = controleHst.GetID();
                                CCNCONFORMHST historico = new CCNCONFORMHST
                                {
                                    CODIGO = ID,
                                    CODCOLIGADA = cVerificarPAEF.CODCOLIGADA,
                                    CODNCONFORM = cVerificarPAEF.CODNCONFORM,
                                    CODCCUSTO = cVerificarPAEF.CODCCUSTO,
                                    HISTORICO = $@"Eficácia da Ação verificada no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                pelo usuário {FormPrincipal.getUsuarioAcesso().CODUSUARIO} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                    RECCREATEDON = DateTime.Now
                                };
                                listaHistoricos.Add(historico);

                                if (!ckSim.Checked)
                                {
                                    CCNCONFORMHST historicoNovo = new CCNCONFORMHST
                                    {
                                        CODIGO = ID + 1,
                                        CODCOLIGADA = cVerificarPAEF.CODCOLIGADA,
                                        CODNCONFORM = cVerificarPAEF.CODNCONFORM,
                                        CODCCUSTO = cVerificarPAEF.CODCCUSTO,
                                        STATUS = "A",
                                        HISTORICO = $@"Não conformidade reaberta no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                    pelo usuário {FormPrincipal.getUsuarioAcesso().CODUSUARIO} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                        RECCREATEDON = DateTime.Now
                                    };
                                    listaHistoricos.Add(historicoNovo);
                                    eficaz = false;
                                }

                                controle.Update(cVerificarPAEF, x => x.CODCOLIGADA == cVerificarPAEF.CODCOLIGADA &&
                                x.CODCCUSTO == cVerificarPAEF.CODCCUSTO && x.CODNCONFORM == cVerificarPAEF.CODNCONFORM);
                                controleHst.CreateAll(listaHistoricos);
                                controleNConformidade.Update(nConformidade, x => x.CODCOLIGADA == cVerificarPAEF.CODCOLIGADA &&
                                x.CODCCUSTO == cVerificarPAEF.CODCCUSTO && x.CODNCONFORM == cVerificarPAEF.CODNCONFORM);

                                controle.SaveAll();
                                controleHst.SaveAll();
                                controleNConformidade.SaveAll();

                                this.salvo = true;
                                this.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
            else
                Global.MsgErro("O campo 'Comentário' é obrigatório");
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !ckSim.Checked;
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !ckNao.Checked;
        }
    }
}
