﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.PlanoAcao
{
    public partial class FormVerificarPlanoAcao : Form
    {
        private CVPACAOEFICACIA cVerificarPAEF;
        public bool salvo = false;
        private bool somenteVisualizcao = false;

        public FormVerificarPlanoAcao(CVPACAOEFICACIA cVerifPAEF)
        {
            InitializeComponent();
            cVerificarPAEF = cVerifPAEF;
        }

        public FormVerificarPlanoAcao(CVPACAOEFICACIA cVerifPAEF, bool visualizacao)
        {
            InitializeComponent();
            cVerificarPAEF = cVerifPAEF;
            somenteVisualizcao = visualizacao;
        }

        private void FormVerificarPlanoAcao_Load(object sender, EventArgs e)
        {
            if (cVerificarPAEF != null)
            {
                ckSim.Checked = (cVerificarPAEF.CUMPRIDO.HasValue) ? cVerificarPAEF.CUMPRIDO.Value : false;
                txtDtVerificacaoEficacia.Text = (cVerificarPAEF.DTLIMITEVEFICACIA.HasValue) ? cVerificarPAEF.DTLIMITEVEFICACIA.Value.ToString() : null;
                txtObservacao.Text = cVerificarPAEF.OBSERVACAOPACAO;
            }

            if (somenteVisualizcao)
            {
                ckNao.Enabled = false;
                ckSim.Enabled = false;
                txtDtVerificacaoEficacia.ReadOnly = true;
                txtObservacao.ReadOnly = true;
                btnGravar.Visible = false;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private bool ValidaInformacoes()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtDtVerificacaoEficacia.Text.Replace("/", "").Trim()) && (!Global.ValidaData(txtDtVerificacaoEficacia.Text)))
                msg += "Informe uma data limite para a verificação da eficácia da ação." + Environment.NewLine;

            if (Convert.ToDateTime(txtDtVerificacaoEficacia.Text).Subtract(
           cVerificarPAEF.RECCREATEDON.Value).TotalDays < 0)
                msg += "A data limite para verificação da eficácia deve ser maior ou igual a " +
                   "data de criação da não conformidade." + Environment.NewLine;

            if (string.IsNullOrEmpty(txtObservacao.Text))
                msg += "O campo 'Comentário' é obrigatório." + Environment.NewLine;


            if (string.IsNullOrWhiteSpace(msg))
                return true;
            else
            {
                FormMsg form = new FormMsg(msg);
                form.ShowDialog();
                return false;
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaInformacoes())
            {
                try
                {
                    using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
                    {
                        using (ControleCCNConformHst controleHst = new ControleCCNConformHst())
                        {
                            using (ControleCNConformidade controleNConformidade = new ControleCNConformidade())
                            {
                                List<CCNCONFORMHST> listaHistoricos = new List<CCNCONFORMHST>();

                                cVerificarPAEF.CUMPRIDO = ckSim.Checked;
                                cVerificarPAEF.DATAVERIFPACAO = DateTime.Now;
                                cVerificarPAEF.OBSERVACAOPACAO = txtObservacao.Text;
                                cVerificarPAEF.CODUSUVERIFPACAO = FormPrincipal.getUsuarioAcesso().CODUSUARIO;
                                cVerificarPAEF.DTLIMITEVEFICACIA = Convert.ToDateTime(txtDtVerificacaoEficacia.Text);

                                int ID = controleHst.GetID();
                                CCNCONFORMHST historico = new CCNCONFORMHST
                                {
                                    CODIGO = ID,
                                    CODCOLIGADA = cVerificarPAEF.CODCOLIGADA,
                                    CODNCONFORM = cVerificarPAEF.CODNCONFORM,
                                    CODCCUSTO = cVerificarPAEF.CODCCUSTO,
                                    HISTORICO = $@"Plano de Ação verificada no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                    pelo usuário {FormPrincipal.getUsuarioAcesso().CODUSUARIO} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                    RECCREATEDON = DateTime.Now
                                };
                                listaHistoricos.Add(historico);


                                controle.updatePlanoAcao(cVerificarPAEF);
                                controleHst.CreateAll(listaHistoricos);
                                controleNConformidade.UpdatePlanoAcao(
                                    cVerificarPAEF.CODCOLIGADA, cVerificarPAEF.CODCCUSTO,
                                    cVerificarPAEF.CODNCONFORM, true
                                );

                                controleHst.SaveAll();

                                this.salvo = true;
                                this.Close();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !ckSim.Checked;
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !ckNao.Checked;
        }
    }
}
