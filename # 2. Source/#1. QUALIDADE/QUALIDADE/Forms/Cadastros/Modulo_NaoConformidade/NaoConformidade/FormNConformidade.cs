﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.PlanoAcao;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Data.Extensions;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.NaoConformidade
{
    public partial class FormNConformidade : Form
    {
        public bool salvo = false;
        public bool planoAcao = false;
        private bool inicioCarregamento = true;
        private bool Integracao;
        private List<GARQANX> anexos;
        private CNCONFORM itemEdit;
        private List<CRELATORESCONFORM> relatores;

        public FormNConformidade(CNCONFORM nConformidade = null, bool? Integra = null)
        {
            InitializeComponent();
            anexos = new List<GARQANX>();
            itemEdit = nConformidade;
            relatores = new List<CRELATORESCONFORM>();
            Integracao = Integra.HasValue ? Integra.Value : false;
        }

        private void FormNConformidade_Load(object sender, EventArgs e)
        {
            txtDataConformidade.Text = DateTime.Now.ToString("dd/MM/yyyy");
            tabControl.TabPages.Remove(tabPlanoAcao);
            tabControl.TabPages.Remove(tabAnexos);

            CarregaCombos();
            CarregaAnexos();
            DesabilitaColunas();

            btnGravar.Text = "&Finalizar";

            if (itemEdit == null)
            {
                using (ControleCNConformidade controle = new ControleCNConformidade())
                {
                    txtCodConformidade.Text = $@"{controle.GetID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA)}";
                }
            }
            else if (itemEdit != null && Integracao)
            {
                txtCodConformidade.Text = $"{itemEdit.CODNCONFORM}";
                cbCCusto.SelectedValue = itemEdit.CODCCUSTO;
                cbTipoAcao.SelectedIndex = (itemEdit.TPACAO == "C") ? 1 : 0;
                txtDataConformidade.Text = itemEdit.DATAABERTURANC.ToString("dd/MM/yyyy");
                txtNConfRealPotencial.Text = itemEdit.NCONFREALPONTENCIAL;
                ckSimAcaoCorrretiva.Checked = itemEdit.NECACAOCORPREV;
            }
            else
            {
                txtCodConformidade.Text = $"{itemEdit.CODNCONFORM}";
                cbCCusto.SelectedValue = itemEdit.CODCCUSTO;
                cbTipoAcao.SelectedIndex = (itemEdit.TPACAO == "C") ? 1 : 0;
                cbClassif.SelectedValue = itemEdit.CODCLASSIF;
                cbTPRegistro.SelectedValue = itemEdit.CODREGISTRO;
                if (itemEdit.CODSETOR == 0)
                {
                    using (ControleCExtratificacao controle = new ControleCExtratificacao())
                    {
                        CEXTRATIFICACAO extrat = controle.Find(itemEdit.CODEXTRATIF, itemEdit.CODCOLIGADA);
                        if (extrat != null && extrat.CODSETOR.HasValue)
                            cbSetor.SelectedValue = extrat.CODSETOR.Value;
                    }
                }
                else
                {
                    cbSetor.SelectedValue = itemEdit.CODSETOR;
                }
                txtDataConformidade.Text = itemEdit.DATAABERTURANC.ToString("dd/MM/yyyy");
                cbExtratificacao.SelectedValue = itemEdit.CODEXTRATIF;
                txtNConfRealPotencial.Text = itemEdit.NCONFREALPONTENCIAL;
                txtAcaoImediata.Text = itemEdit.ACAOIMEDTOMADA;
                txtEvidenciaObs.Text = itemEdit.EVDOBJETIVA;
                txtAnaliseCausa.Text = itemEdit.ANLISECAUSA1;
                ckAfetaQualidadeSIM.Checked = itemEdit.AFETAQUALIDADE.HasValue ? itemEdit.AFETAQUALIDADE.Value : false;
                ckFatoIsoladoSIM.Checked = itemEdit.FATOISOLADO.HasValue ? itemEdit.FATOISOLADO.Value : false;
                //txtDescricaoAbrangencia.Text = itemEdit.ANLISECAUSA3;
                txtEvidenciaTratativa.Text = itemEdit.EVDTRATATIVA;
                ckSimAcaoCorrretiva.Checked = itemEdit.NECACAOCORPREV;
                txtQue.Text = itemEdit.PLANOACAOQUE;
                txtQuem.Text = itemEdit.PLANOACAOQUEM;
                calendQuando.Value = itemEdit.PLANOACAOQUANDO.HasValue ? itemEdit.PLANOACAOQUANDO.Value : DateTime.Now;
                cbExecutor.SelectedValue = itemEdit.CODEXECUTOR;
                CarregaRespRelatExecutores(itemEdit.CODCCUSTO, itemEdit.CODNCONFORM);

                if (itemEdit.STATUS != "A")
                {
                    txtCodConformidade.ReadOnly = true;
                    cbCCusto.Enabled = false;
                    cbTipoAcao.Enabled = false;
                    cbClassif.Enabled = false;
                    cbTPRegistro.Enabled = false;
                    cbSetor.Enabled = false;
                    cbExtratificacao.Enabled = false;
                    txtNConfRealPotencial.ReadOnly = true;
                    txtAcaoImediata.ReadOnly = true;
                    txtEvidenciaObs.ReadOnly = true;
                    txtAnaliseCausa.ReadOnly = true;
                    txtDescricaoAbrangencia.ReadOnly = true;
                    txtEvidenciaTratativa.ReadOnly = true;
                    ckSimAcaoCorrretiva.Enabled = false;
                    ckNaoAcaoCorrretiva.Enabled = false;
                    ckAfetaQualidadeNAO.Enabled = false;
                    ckAfetaQualidadeSIM.Enabled = false;
                    ckFatoIsoladoNAO.Enabled = false;
                    ckFatoIsoladoSIM.Enabled = false;
                    txtQue.ReadOnly = true;
                    txtQuem.ReadOnly = true;
                    calendQuando.Enabled = false;
                    cbExecutor.Enabled = false;
                    cbRelatores.Enabled = false;
                    btnAddRelatores.Enabled = false;
                    btnGravar.Enabled = false;
                }

                if (ckFatoIsoladoSIM.Checked && !ckAfetaQualidadeSIM.Checked)
                {
                    txtDescricaoAbrangencia.Text = null;
                    txtDescricaoAbrangencia.ReadOnly = true;
                }
                else
                {
                    txtDescricaoAbrangencia.Text = itemEdit.DESCRICAOABRAGENCIA;
                    txtDescricaoAbrangencia.ReadOnly = false;
                }
            }
        }

        private void CarregaRespRelatExecutores(string cCusto, int nConformidade)
        {
            using (ControleGUsuario controleUsuario = new ControleGUsuario())
            {

                using (ControleCRelatoresNConform controle = new ControleCRelatoresNConform())
                {
                    relatores = controle.Get(x => x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA &&
                        x.CODNCONFORM == nConformidade && x.CODCCUSTO == cCusto).ToList();

                    relatores.ForEach(x => x.NOMEUSUARIO = controleUsuario.Find(x.CODRELATOR).NOME);
                }
            }
            CarregaDGVRelatores();
        }

        private void CarregaCombos()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                cbCCusto.DataSource = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "C");

                cbCCusto.ValueMember = "CODCCUSTO";
                cbCCusto.DisplayMember = "NOME";
                cbCCusto.SelectedIndex = -1;
            }

            using (ControleCTClassificacao controle = new ControleCTClassificacao())
            {
                cbClassif.DataSource = controle.GetAll().ToList();
                cbClassif.ValueMember = "CODCLASSIF";
                cbClassif.DisplayMember = "DESCRICAO";
                cbClassif.SelectedIndex = -1;
            }

            using (ControleCTPRegistros controle = new ControleCTPRegistros())
            {
                cbTPRegistro.DataSource = controle.GetAll().ToList();
                cbTPRegistro.ValueMember = "CODREGISTRO";
                cbTPRegistro.DisplayMember = "DESCRICAO";
                cbTPRegistro.SelectedIndex = -1;
            }

            using (ControleGSetores controle = new ControleGSetores())
            {
                cbSetor.DataSource = controle.Get(x => x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                cbSetor.ValueMember = "CODIGO";
                cbSetor.DisplayMember = "DESCRICAO";
                cbSetor.SelectedIndex = -1;
            }

            cbTipoAcao.SelectedIndex = 1;
            inicioCarregamento = false;
        }

        private void ckNaoAcaoCorrretiva_CheckedChanged(object sender, EventArgs e)
        {
            ckSimAcaoCorrretiva.Checked = !ckNaoAcaoCorrretiva.Checked;
            if (ckNaoAcaoCorrretiva.Checked)
            {
                if (tabControl.TabPages.Contains(tabPlanoAcao))
                    tabControl.TabPages.Remove(tabPlanoAcao);

                btnGravar.Text = "Finalizar";
            }
        }

        private void ckSimAcaoCorrretiva_CheckedChanged(object sender, EventArgs e)
        {
            ckNaoAcaoCorrretiva.Checked = !ckSimAcaoCorrretiva.Checked;
            if (ckSimAcaoCorrretiva.Checked)
            {
                if (!tabControl.TabPages.Contains(tabPlanoAcao))
                    tabControl.TabPages.Add(tabPlanoAcao);

                btnGravar.Text = "Salvar";
            }
        }

        private void lblEvidenciaObs_Click(object sender, EventArgs e)
        {
            string[] caminhoArquivo = Funcoes.SelecionaArquivoAllFormat();

            int numSeq = (anexos == null) ? 1 : anexos.Count + 1;

            if (caminhoArquivo != null)
            {
                foreach (string arquivo in caminhoArquivo)
                {
                    GARQANX item = new GARQANX
                    {
                        CKSELECIONADO = false,
                        CODEXTERNO = Convert.ToInt32(txtCodConformidade.Text),
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODSISTEMA = "C",
                        CAMPOREF = "EVDOBJETIVA",
                        NOMEDOC = Path.GetFileNameWithoutExtension(arquivo),
                        EXTENSAO = Path.GetExtension(arquivo).ToLower(),
                        BYTES = Global.EncriptPDF(arquivo),
                        NUMSEQUENCIAL = numSeq,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    anexos.Add(item);
                    numSeq++;
                }

                if (tabControl.TabPages.Contains(tabAnexos))
                    CarregaDGVEvdObservada();
                else
                    CarregaAnexos();
            }
        }

        private void lblEvidenciaTrataiva_Click(object sender, EventArgs e)
        {
            string[] caminhoArquivo = Funcoes.SelecionaArquivoAllFormat();

            int numSeq = (anexos == null) ? 1 : anexos.Count + 1;

            if (caminhoArquivo != null)
            {
                foreach (string arquivo in caminhoArquivo)
                {
                    GARQANX item = new GARQANX
                    {
                        CKSELECIONADO = false,
                        CODEXTERNO = Convert.ToInt32(txtCodConformidade.Text),
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODSISTEMA = "C",
                        CAMPOREF = "EVDTRATATIVA",
                        NOMEDOC = Path.GetFileNameWithoutExtension(arquivo),
                        EXTENSAO = Path.GetExtension(arquivo).ToLower(),
                        BYTES = Global.EncriptPDF(arquivo),
                        NUMSEQUENCIAL = numSeq,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    anexos.Add(item);
                    numSeq++;
                }

                if (tabControl.TabPages.Contains(tabAnexos))
                    CarregaDGVEvdTrat();
                else
                    CarregaAnexos();
            }
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (cbCCusto.SelectedIndex < 0)
                msg += " - Informe um centro de custo.\n";

            if (cbClassif.SelectedIndex < 0)
                msg += " - Informe a classificação da não conformidade.\n";

            if (cbTipoAcao.SelectedIndex < 0)
                msg += " - Informe o tipo de ação.\n";

            if (cbTPRegistro.SelectedIndex < 0)
                msg += " - Informe o tipo de registro.\n";

            if (cbSetor.SelectedIndex < 0)
                msg += " - Informe o setor.\n";

            if (cbExtratificacao.SelectedIndex < 0)
                msg += " - Informe a estratificação.\n";

            if (!Global.ValidaData(txtDataConformidade.Text))
                msg += " - Informe uma data valida para a não conformidade.\n";

            if (string.IsNullOrEmpty(txtNConfRealPotencial.Text))
                msg += " - Informe a não conformidade real ou potencial.\n";

            if (string.IsNullOrEmpty(txtAnaliseCausa.Text))
                msg += " - Informe a análise de causa.\n";

            if (string.IsNullOrEmpty(txtEvidenciaObs.Text) &&
                anexos.Where(x => x.CAMPOREF == "EVDOBJETIVA").ToList().Count <= 0)
                msg += " - Informe a evidência observada.\n";

            if (cbExecutor.SelectedIndex < 0)
                msg += " - Informe ao menos um usuário para ser o executor da não conformidade.\n";

            if (ckSimAcaoCorrretiva.Checked)
            {
                if (string.IsNullOrEmpty(txtQue.Text) || string.IsNullOrEmpty(txtQuem.Text))
                    msg += " - Todos os campos do plano de ação devem ser preenchidos.\n";
            }

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void CarregaAnexos()
        {
            if (itemEdit != null)
            {
                using (ControleGArquivoAnx controle = new ControleGArquivoAnx())
                {
                    anexos = controle.Get(x => x.CODCOLIGADA == itemEdit.CODCOLIGADA &&
                           (x.CAMPOREF == "EVDOBJETIVA" || x.CAMPOREF == "EVDTRATATIVA") &&
                            x.CODEXTERNO == itemEdit.CODNCONFORM).ToList();
                }
            }

            Cursor.Current = Cursors.WaitCursor;
            if (anexos.Count > 0)
            {
                CarregaDGVEvdObservada();
                CarregaDGVEvdTrat();

                if (!tabControl.TabPages.Contains(tabAnexos))
                    tabControl.TabPages.Add(tabAnexos);
            }
            else
            {
                if (tabControl.TabPages.Contains(tabAnexos))
                    tabControl.TabPages.Remove(tabAnexos);
            }
            Cursor.Current = Cursors.Default;
        }

        #region: "Evidencia Observada"
        private void CarregaDGVEvdObservada()
        {
            if (dgvEvdObservada.Columns != null)
            {
                dgvEvdObservada.Columns.Clear();
            }

            dgvEvdObservada.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CODEXTERNO", HeaderText = "Cód. Externo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CODSISTEMA", HeaderText = "Cód. Sistema", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "NUMSEQUENCIAL", HeaderText = "Sequêncial", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "NOMEDOC", HeaderText = "Nome Doc.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "BYTES", HeaderText = "Bytes", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "CAMPOREF", HeaderText = "Campo Ref.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDON", HeaderText = "Data Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdObservada.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Data Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            BindingSource bindEvdObjetiva = new BindingSource();
            bindEvdObjetiva.DataSource = typeof(GARQANX);
            bindEvdObjetiva.DataSource = anexos.Where(x => x.CAMPOREF == "EVDOBJETIVA");
            dgvEvdObservada.DataSource = bindEvdObjetiva;
            bindEvdObjetiva.EndEdit();

            if (dgvEvdObservada != null && dgvEvdObservada.View != null &&
                dgvEvdObservada.View.Records != null && ((dgvEvdObservada.View.Records.Count) > 0))
            {
                tsbAdicionarEvdObs.Enabled = true;
                tsbRemoverEvdObs.Enabled = true;
                tsbVerAnexoEvdObs.Enabled = true;
                tsbAtualizarEvdObs.Enabled = true;
            }
            else
            {
                tsbAdicionarEvdObs.Enabled = true;
                tsbRemoverEvdObs.Enabled = false;
                tsbVerAnexoEvdObs.Enabled = false;
                tsbAtualizarEvdObs.Enabled = true;
            }
        }

        private void tsbAtualizarEvdObs_Click(object sender, EventArgs e)
        {
            CarregaDGVEvdObservada();
        }

        private void tsbVerAnexoEvdObs_Click(object sender, EventArgs e)
        {
            if (dgvEvdObservada.CurrentItem is GARQANX item && item.CKSELECIONADO)
            {
                string arquivoTemp = string.Format
                (
                    @"{0}\{1}{2}", Global.PastaTemp(),
                    item.NOMEDOC, item.EXTENSAO
                );

                Process.Start(Global.DescriptPDF(item.BYTES, arquivoTemp));
            }
            else
            {
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
            }
        }

        private void tsbRemoverEvdObs_Click(object sender, EventArgs e)
        {
            if (dgvEvdObservada.CurrentItem is GARQANX item && item.CKSELECIONADO)
            {
                anexos.Remove(item);
                tsbAtualizarEvdObs.PerformClick();
            }
            else
            {
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
            }
        }

        private void tsbAdicionarEvdObs_Click(object sender, EventArgs e)
        {
            lblEvidenciaObs_Click(sender, e);
        }
        #endregion

        #region: "Evidencia Tratativa"
        private void CarregaDGVEvdTrat()
        {
            if (dgvEvdTratativa.Columns != null)
            {
                dgvEvdTratativa.Columns.Clear();
            }

            dgvEvdTratativa.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CODEXTERNO", HeaderText = "Cód. Externo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CODSISTEMA", HeaderText = "Cód. Sistema", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "NUMSEQUENCIAL", HeaderText = "Sequêncial", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "NOMEDOC", HeaderText = "Nome Doc.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "BYTES", HeaderText = "Bytes", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "CAMPOREF", HeaderText = "Campo Ref.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDON", HeaderText = "Data Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvEvdTratativa.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Data Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            BindingSource bindEvdTratativa = new BindingSource();
            bindEvdTratativa.DataSource = typeof(GARQANX);
            bindEvdTratativa.DataSource = anexos.Where(x => x.CAMPOREF == "EVDTRATATIVA");
            dgvEvdTratativa.DataSource = bindEvdTratativa;
            bindEvdTratativa.EndEdit();

            if (dgvEvdTratativa != null && dgvEvdTratativa.View != null &&
                dgvEvdTratativa.View.Records != null && ((dgvEvdTratativa.View.Records.Count) > 0))
            {
                tsbAdicionarEvdTrat.Enabled = true;
                tsbRemoverEvdTrat.Enabled = true;
                tsbVerAnexoEvdTrat.Enabled = true;
                tsbAtualizarEvdTrat.Enabled = true;
            }
            else
            {
                tsbAdicionarEvdTrat.Enabled = true;
                tsbRemoverEvdTrat.Enabled = false;
                tsbVerAnexoEvdTrat.Enabled = false;
                tsbAtualizarEvdTrat.Enabled = true;
            }
        }

        private void tsbAtualizarEvdTrat_Click(object sender, EventArgs e)
        {
            CarregaDGVEvdTrat();
        }

        private void tsbVerAnexoEvdTrat_Click(object sender, EventArgs e)
        {
            if (dgvEvdTratativa.CurrentItem is GARQANX item && item.CKSELECIONADO)
            {
                string arquivoTemp = string.Format
                (
                    @"{0}\{1}{2}", Global.PastaTemp(),
                    item.NOMEDOC, item.EXTENSAO
                );

                Process.Start(Global.DescriptPDF(item.BYTES, arquivoTemp));
            }
            else
            {
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
            }
        }

        private void tsbRemoverEvdTrat_Click(object sender, EventArgs e)
        {
            if (dgvEvdTratativa.CurrentItem is GARQANX item && item.CKSELECIONADO)
            {
                anexos.Remove(item);
                tsbAtualizarEvdObs.PerformClick();
            }
            else
            {
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
            }
        }

        private void tsbAdicionarEvdTrat_Click(object sender, EventArgs e)
        {
            lblEvidenciaTrataiva_Click(sender, e);
        }
        #endregion

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    CNCONFORM item = new CNCONFORM
                    {
                        CODNCONFORM = Convert.ToInt32(txtCodConformidade.Text),
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue),
                        TPACAO = cbTipoAcao.Text.Substring(0, 1).ToUpper(),
                        CODCLASSIF = Convert.ToInt32(cbClassif.SelectedValue),
                        CODREGISTRO = Convert.ToInt32(cbTPRegistro.SelectedValue),
                        DATAABERTURANC = Convert.ToDateTime(txtDataConformidade.Text),
                        STATUS = ckSimAcaoCorrretiva.Checked ? "A" : "F",
                        FINALIZADORESPONSAVEL = false,
                        FINALIZADORELATOR = ckSimAcaoCorrretiva.Checked ? false : true,
                        VERIFICACAOPLANOEFICACIA = false,
                        VERIFICACAOPLANOACAO = false,
                        CODSETOR = Convert.ToInt32(cbSetor.SelectedValue), 
                        CODEXTRATIF = Convert.ToInt32(cbExtratificacao.SelectedValue),
                        NCONFREALPONTENCIAL = txtNConfRealPotencial.Text,
                        ACAOIMEDTOMADA = txtAcaoImediata.Text,
                        EVDOBJETIVA = txtEvidenciaObs.Text,
                        EVDTRATATIVA = txtEvidenciaTratativa.Text,
                        CODRELATOR = FormPrincipal.getUsuarioAcesso().CODUSUARIO,
                        NECACAOCORPREV = ckSimAcaoCorrretiva.Checked,
                        ANLISECAUSA1 = txtAnaliseCausa.Text,
                        ANLISECAUSA2 = null,
                        ANLISECAUSA3 = null,
                        FATOISOLADO = ckFatoIsoladoSIM.Checked,
                        AFETAQUALIDADE = ckAfetaQualidadeSIM.Checked,
                        DESCRICAOABRAGENCIA = txtDescricaoAbrangencia.Text,
                        CODEXECUTOR = Convert.ToInt32(cbExecutor.SelectedValue),
                        PLANOACAOQUE = !(tabControl.TabPages.Contains(tabPlanoAcao)) ? null : txtQue.Text,
                        PLANOACAOQUEM = !(tabControl.TabPages.Contains(tabPlanoAcao)) ? null : txtQuem.Text,
                        PLANOACAOQUANDO = !(tabControl.TabPages.Contains(tabPlanoAcao)) ? null : calendQuando.Value.DBNullToDateTime(),
                    };

                    AtualizaRelatores();

                    using (ControleCNConformidade controle = new ControleCNConformidade())
                    {
                        using (ControleCRelatoresNConform controleRelatores = new ControleCRelatoresNConform())
                        {
                            using (ControleGArquivoAnx controleAnexos = new ControleGArquivoAnx())
                            {
                                using (ControleCCNConformHst controleHst = new ControleCCNConformHst())
                                {
                                    using (ControleCVPAcaoEficacia controleCVPAcaoEficacia = new ControleCVPAcaoEficacia())
                                    {
                                        if (itemEdit == null || Integracao)
                                        {
                                            item.DATAABERTURANC = DateTime.Now;
                                            item.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                                            item.RECCREATEDON = DateTime.Now;

                                            List<CCNCONFORMHST> historicos = new List<CCNCONFORMHST>();

                                            int ID = controleHst.GetID();
                                            CCNCONFORMHST historico = new CCNCONFORMHST
                                            {
                                                CODIGO = ID,
                                                CODCOLIGADA = item.CODCOLIGADA,
                                                CODNCONFORM = item.CODNCONFORM,
                                                CODCCUSTO = item.CODCCUSTO,
                                                STATUS = "A",
                                                HISTORICO = $@"Criado no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                                    pelo usuário {item.CODRELATOR} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                                RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                RECCREATEDON = DateTime.Now
                                            };

                                            historicos.Add(historico);

                                            if (item.STATUS == "F")
                                            {
                                                CCNCONFORMHST hstConclusao = new CCNCONFORMHST
                                                {
                                                    CODIGO = ID + 1,
                                                    CODCOLIGADA = item.CODCOLIGADA,
                                                    CODNCONFORM = item.CODNCONFORM,
                                                    CODCCUSTO = item.CODCCUSTO,
                                                    STATUS = "F",
                                                    HISTORICO = $@"Finalizada no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                                        pelo usuário {item.CODRELATOR} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                    RECCREATEDON = DateTime.Now
                                                };
                                                historicos.Add(hstConclusao);
                                            }

                                            controle.Create(item);
                                            controleRelatores.CreateAll(relatores);
                                            controleHst.CreateAll(historicos);

                                            controle.SaveAll();
                                            controleRelatores.SaveAll();
                                            controleHst.SaveAll();

                                            if (anexos != null && anexos.Count > 0)
                                            {
                                                anexos.ForEach(x => x.CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue));
                                                controleAnexos.CreateAll(anexos);
                                                controleAnexos.SaveAll();
                                            }

                                            CVPACAOEFICACIA acaoEficacia = new CVPACAOEFICACIA
                                            {
                                                CODCOLIGADA = item.CODCOLIGADA,
                                                CODNCONFORM = item.CODNCONFORM,
                                                CODCCUSTO = item.CODCCUSTO,
                                                RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                RECCREATEDON = DateTime.Now
                                            };

                                            controleCVPAcaoEficacia.Create(acaoEficacia);
                                            controleCVPAcaoEficacia.SaveAll();

                                            salvo = true;
                                        }
                                        else
                                        {
                                            item.DATAABERTURANC = itemEdit.DATAABERTURANC;
                                            item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                                            item.RECMODIFIEDON = DateTime.Now;

                                            int ID = controleHst.GetID();
                                            CCNCONFORMHST historico = new CCNCONFORMHST
                                            {
                                                CODIGO = ID,
                                                CODCOLIGADA = item.CODCOLIGADA,
                                                CODNCONFORM = item.CODNCONFORM,
                                                CODCCUSTO = item.CODCCUSTO,
                                                STATUS = item.STATUS,
                                                HISTORICO = $@"Editado no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                                pelo usuário {item.CODRELATOR} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                                RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                RECCREATEDON = DateTime.Now
                                            };

                                            controle.Update(item, x => x.CODCOLIGADA == itemEdit.CODCOLIGADA &&
                                            x.CODNCONFORM == itemEdit.CODNCONFORM);
                                            controleHst.Create(historico);

                                            controle.SaveAll();
                                            controleHst.SaveAll();

                                            #region: "Atualização Relatores"
                                            int l = 1;
                                            controleRelatores.Delete(
                                                   itemEdit.CODCOLIGADA, itemEdit.CODNCONFORM,
                                                   itemEdit.CODCCUSTO
                                            );

                                            foreach (CRELATORESCONFORM it in relatores)
                                            {
                                                it.NUMSEQUENCIAL = l;
                                                controleRelatores.Create(it);

                                                l++;
                                            }
                                            controleRelatores.SaveAll();
                                            #endregion

                                            #region: "Atualização Anexos"
                                            List<GARQANX> list = controleAnexos.Get(x => x.CODCOLIGADA == item.CODCOLIGADA &&
                                                x.CODCCUSTO == item.CODCCUSTO && x.CODEXTERNO == item.CODNCONFORM).ToList();

                                            if (list != null)
                                            {
                                                controleAnexos.Delete(x => x.CODCOLIGADA == item.CODCOLIGADA &&
                                                x.CODCCUSTO == item.CODCCUSTO && x.CODEXTERNO == item.CODNCONFORM);
                                                controleAnexos.SaveAll();

                                                if (anexos != null)
                                                {
                                                    anexos.ForEach(x => x.CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue));
                                                    controleAnexos.CreateAll(anexos);
                                                    controleAnexos.SaveAll();
                                                }
                                            }
                                            #endregion

                                            if (!itemEdit.FINALIZADORELATOR && item.FINALIZADORELATOR)
                                            {
                                                CVPACAOEFICACIA acaoEficacia = new CVPACAOEFICACIA
                                                {
                                                    CODCOLIGADA = item.CODCOLIGADA,
                                                    CODNCONFORM = item.CODNCONFORM,
                                                    CODCCUSTO = item.CODCCUSTO,
                                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                    RECCREATEDON = DateTime.Now
                                                };

                                                CVPACAOEFICACIA acaoEficAnterior = controleCVPAcaoEficacia.Find(
                                                    itemEdit.CODNCONFORM, itemEdit.CODCOLIGADA, itemEdit.CODCCUSTO);

                                                controleCVPAcaoEficacia.Update(acaoEficacia, x => x.CNCONFORM == acaoEficAnterior.CNCONFORM &&
                                                    x.CODCCUSTO == acaoEficAnterior.CODCCUSTO && x.CODCOLIGADA == acaoEficAnterior.CODCOLIGADA);
                                                controleCVPAcaoEficacia.SaveAll();

                                                using (FormVerificarPlanoAcao frm = new FormVerificarPlanoAcao(acaoEficacia))
                                                {
                                                    frm.StartPosition = FormStartPosition.CenterParent;
                                                    frm.WindowState = FormWindowState.Normal;
                                                    frm.ShowDialog();

                                                    if (frm.salvo)
                                                        salvo = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Enviar e-mail
                    string assunto = $"[{cbClassif.Text}] - Novo registro de não conformidade (SGI - Sistema de Gestão Integrada)";
                    bool prioridade = (Convert.ToInt16(cbClassif.SelectedValue) == 3);

                    string corpoEmail =
                        $@"<br>Uma nova não conformidade foi atribuida ao seu usuário.<br><br>
                        Resumo da não conformidade<br><br>
                        <strong>Código:</strong> {txtCodConformidade.Text}<br>
                        <strong>Centro de Custo:</strong> {cbCCusto.Text}<br>
                        <strong>Classificação:</strong> {cbClassif.Text}<br>
                        <strong>Setor (Processo / Área):</strong> {cbSetor.Text}<br>
                        <strong>Descrição da não conformidade:</strong> {txtNConfRealPotencial.Text}<br>
                        <strong>Não conformidade aberta por:</strong> {FormPrincipal.getUsuarioAcesso().NOME}<br>
                        <strong>Data da não conformidade:</strong> {txtDataConformidade.Text}<br><br>
                        E-mail enviado automaticamente pelo sistema SGI - Sistema de Gestão Integrada<br>
                        <img width='350' src='cid:ImageLogo' alt='Logo do sistema' />";

                    GUSUARIO usr = new GUSUARIO();
                    MailAddress mail = null;
                    using (ControleGUsuario controle = new ControleGUsuario())
                    {
                        usr = controle.Find(item.CODEXECUTOR);
                        if (usr != null && !string.IsNullOrEmpty(usr.EMAIL))
                            mail = new MailAddress(usr.EMAIL, usr.NOME);
                    }

                    if (mail == null)
                    {
                        mail = new MailAddress(
                            FormPrincipal.getUsuarioAcesso().EMAIL,
                            FormPrincipal.getUsuarioAcesso().NOME
                        );
                    }

                    List<string> copiasEmail = new List<string>();
                    relatores.ForEach(x => copiasEmail.Add(Convert.ToString(x.CODRELATOR)));

                    using (ControleGCentroCusto controle = new ControleGCentroCusto())
                    {
                        GUSUARIO user = controle.RetornaGerenteCCusto(
                            item.CODCCUSTO, item.CODCOLIGADA);

                        if (user != null && user.EMAIL != mail.Address)
                        {
                            if (!copiasEmail.Exists(x => x == Convert.ToString(user.CODUSUARIO)))
                                copiasEmail.Add(Convert.ToString(user.CODUSUARIO));
                        }
                    }

                    if (prioridade)
                    {
                        using (ControleGDiretorCC controle = new ControleGDiretorCC())
                        {
                            List<GDIRETORCCUSTO> listaDiretor =
                                controle.GetList(item.CODCOLIGADA, item.CODCCUSTO);

                            foreach (GDIRETORCCUSTO diretor in listaDiretor)
                            {
                                if (!copiasEmail.Exists(x => x == Convert.ToString(diretor.CODDIRETOR)))
                                    copiasEmail.Add(Convert.ToString(diretor.CODDIRETOR));
                            }
                        }
                    }

                    Global.Email(corpoEmail, assunto, prioridade, mail, copiasEmail);
                    Cursor.Current = Cursors.Default;
                    Global.MsgSucesso("Informações foram salvas com sucesso.");
                    salvo = true;
                    this.Close();
                }
                catch (Exception ex)
                {
                    Cursor.Current = Cursors.Default;
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
        }

        private void AtualizaRelatores()
        {
            int i = 2;
            foreach (CRELATORESCONFORM it in relatores)
            {
                it.CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
                it.CODNCONFORM = Convert.ToInt16(txtCodConformidade.Text);
                it.CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue);
                it.NUMSEQUENCIAL = i;
                it.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                it.RECCREATEDON = DateTime.Now;

                i++;
            }

            relatores.Add(new CRELATORESCONFORM
            {
                CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                CODNCONFORM = Convert.ToInt16(txtCodConformidade.Text),
                CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue),
                CODRELATOR = FormPrincipal.getUsuarioAcesso().CODUSUARIO,
                NOMEUSUARIO = FormPrincipal.getUsuarioAcesso().NOME,
                NUMSEQUENCIAL = 1,
                RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                RECCREATEDON = DateTime.Now,
            });
        }

        private void cbCCusto_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<GUSUARIO> lista = new List<GUSUARIO>();
            List<GUSUARIO> listaExecutores = new List<GUSUARIO>();

            if (!inicioCarregamento)
            {
                if (cbCCusto.SelectedItem is GCENTROCUSTO cCusto)
                {
                    relatores.Clear();

                    using (ControleGUsuario controle = new ControleGUsuario())
                    {
                        lista = controle.GetList(cCusto.CODCOLIGADA, cCusto.CODCCUSTO, "C").
                            Where(x => x.CODUSUARIO != FormPrincipal.getUsuarioAcesso().CODUSUARIO).ToList();
                        listaExecutores = controle.GetList(cCusto.CODCOLIGADA, cCusto.CODCCUSTO, "C");
                    }

                    using (ControleGCentroCusto controleCCusto = new ControleGCentroCusto())
                    {
                        GUSUARIO usr = controleCCusto.RetornaGerenteCCusto(
                            cCusto.CODCCUSTO, cCusto.CODCOLIGADA);

                        if (usr != null)
                            lblUsuário.Text = usr.NOME;
                        else
                            lblUsuário.Text = string.Empty;
                    }
                }
            }

            cbRelatores.DataSource = lista;
            cbRelatores.ValueMember = "CODUSUARIO";
            cbRelatores.DisplayMember = "NOME";
            cbRelatores.SelectedIndex = -1;

            cbExecutor.DataSource = listaExecutores;
            cbExecutor.ValueMember = "CODUSUARIO";
            cbExecutor.DisplayMember = "NOME";
            cbExecutor.SelectedIndex = -1;
        }

        public void CarregaDGVRelatores()
        {
            List<GUSUARIO> list = new List<GUSUARIO>();
            using (ControleGUsuario controle = new ControleGUsuario())
            {
                foreach (CRELATORESCONFORM item in relatores)
                {
                    if (!list.Exists(x => x.CODUSUARIO == item.CODRELATOR))
                        list.Add(controle.Find(item.CODRELATOR));
                }
            }

            Cursor.Current = Cursors.WaitCursor;
            gUSUARIOBindingSource1 = new BindingSource();
            gUSUARIOBindingSource1.DataSource = typeof(GUSUARIO);
            gUSUARIOBindingSource1.DataSource = list;
            dgvRelatores.DataSource = gUSUARIOBindingSource1;
            gUSUARIOBindingSource1.EndEdit();
            dgvRelatores.Refresh();
            Cursor.Current = Cursors.Default;
        }

        private void DesabilitaColunas()
        {
            foreach (DataGridViewColumn item in dgvRelatores.Columns)
            {
                if (item.Name != "DELETE")
                    dgvRelatores.Columns[item.Name].ReadOnly = true;
            }
        }

        private void btnAddRelatores_Click(object sender, EventArgs e)
        {
            if (cbRelatores.SelectedItem is GUSUARIO usr && cbCCusto.SelectedItem is GCENTROCUSTO cCusto)
            {
                CRELATORESCONFORM relator = new CRELATORESCONFORM
                {
                    CKSELECIONADO = false,
                    CODCCUSTO = cCusto.CODCCUSTO,
                    CODCOLIGADA = cCusto.CODCOLIGADA,
                    CODRELATOR = usr.CODUSUARIO,
                    CODNCONFORM = Convert.ToInt16(txtCodConformidade.Text),
                    NOMEUSUARIO = cbRelatores.Text,
                    NUMSEQUENCIAL = (relatores != null) ? relatores.Count + 1 : 1,
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };

                if (!relatores.Exists(x => x.CODRELATOR == relator.CODRELATOR))
                {
                    relatores.Add(relator);
                    CarregaDGVRelatores();
                }
            }
        }

        private void dgvRelatores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvRelatores.CurrentRow is DataGridViewRow row)
            {
                CRELATORESCONFORM cRelatores = new CRELATORESCONFORM();
                foreach (CRELATORESCONFORM item in relatores)
                {
                    if (item.CODRELATOR == Convert.ToInt16(row.Cells["CODUSUARIOS"].Value))
                        cRelatores = item;
                }

                relatores.Remove(cRelatores);
                CarregaDGVRelatores();
            }
        }

        private void cbSetor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (short.TryParse(Convert.ToString(cbSetor.SelectedValue), out short codSetor) && !inicioCarregamento)
            {
                using (ControleCExtratificacao controle = new ControleCExtratificacao())
                {
                    cbExtratificacao.DataSource = controle.Get(
                        x => x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA &&
                        (!x.CODSETOR.HasValue || x.CODSETOR == codSetor)
                    ).ToList();
                    cbExtratificacao.ValueMember = "CODEXTRATIF";
                    cbExtratificacao.DisplayMember = "DESCRICAO";
                    cbExtratificacao.SelectedIndex = -1;
                }
            }
        }

        private void ckFatoIsoladoSIM_CheckedChanged(object sender, EventArgs e)
        {
            ckFatoIsoladoNAO.Checked = !ckFatoIsoladoSIM.Checked;
        }

        private void ckFatoIsoladoNAO_CheckedChanged(object sender, EventArgs e)
        {
            ckFatoIsoladoSIM.Checked = !ckFatoIsoladoNAO.Checked;
        }

        private void ckAfetaQualidadeSIM_CheckedChanged(object sender, EventArgs e)
        {
            ckAfetaQualidadeNAO.Checked = !ckAfetaQualidadeSIM.Checked;
        }

        private void ckAfetaQualidadeNAO_CheckedChanged(object sender, EventArgs e)
        {
            ckAfetaQualidadeSIM.Checked = !ckAfetaQualidadeNAO.Checked;
        }
    }
}