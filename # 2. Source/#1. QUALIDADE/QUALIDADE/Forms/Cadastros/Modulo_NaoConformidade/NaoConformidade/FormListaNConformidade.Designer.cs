﻿namespace QUALIDADE.Forms.Cadastros.NaoConformidade
{
    partial class FormListaNConformidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaNConformidade));
            this.dgvListagem = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.cNCONFORMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionar = new System.Windows.Forms.ToolStripButton();
            this.tsbEditar = new System.Windows.Forms.ToolStripButton();
            this.tsbRemover = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAtualizar = new System.Windows.Forms.ToolStripButton();
            this.tsbVerPlanoAcao = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbRelatorio = new System.Windows.Forms.ToolStripButton();
            this.tsbHistorico = new System.Windows.Forms.ToolStripButton();
            this.tsbFinalizasao = new System.Windows.Forms.ToolStripButton();
            this.tsbConclusao = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslTotalItens = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsbExportar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNCONFORMBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListagem
            // 
            this.dgvListagem.AccessibleName = "Table";
            this.dgvListagem.AllowFiltering = true;
            this.dgvListagem.AllowResizingColumns = true;
            this.dgvListagem.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListagem.DataSource = this.cNCONFORMBindingSource;
            this.dgvListagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListagem.Location = new System.Drawing.Point(0, 31);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.SelectionMode = Syncfusion.WinForms.DataGrid.Enums.GridSelectionMode.Extended;
            this.dgvListagem.SelectionUnit = Syncfusion.WinForms.DataGrid.Enums.SelectionUnit.Cell;
            this.dgvListagem.Size = new System.Drawing.Size(910, 397);
            this.dgvListagem.Style.HeaderStyle.FilterIconColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.dgvListagem.TabIndex = 14;
            this.dgvListagem.Text = "sfDataGrid1";
            // 
            // cNCONFORMBindingSource
            // 
            this.cNCONFORMBindingSource.DataSource = typeof(QUALIDADE.Dominio.CNCONFORM);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionar,
            this.tsbEditar,
            this.tsbRemover,
            this.toolStripSeparator2,
            this.tsbAtualizar,
            this.tsbVerPlanoAcao,
            this.toolStripSeparator4,
            this.tsbExportar,
            this.toolStripSeparator1,
            this.toolStripSeparator3,
            this.tsbRelatorio,
            this.tsbHistorico,
            this.tsbFinalizasao,
            this.tsbConclusao});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(910, 31);
            this.toolStrip1.TabIndex = 13;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbAdicionar
            // 
            this.tsbAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionar.Image")));
            this.tsbAdicionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionar.Name = "tsbAdicionar";
            this.tsbAdicionar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionar.Size = new System.Drawing.Size(98, 28);
            this.tsbAdicionar.Text = "Adicionar";
            this.tsbAdicionar.Click += new System.EventHandler(this.tsbAdicionar_Click);
            // 
            // tsbEditar
            // 
            this.tsbEditar.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditar.Image")));
            this.tsbEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditar.Name = "tsbEditar";
            this.tsbEditar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbEditar.Size = new System.Drawing.Size(77, 28);
            this.tsbEditar.Text = "Editar";
            this.tsbEditar.Click += new System.EventHandler(this.tsbEditar_Click);
            // 
            // tsbRemover
            // 
            this.tsbRemover.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemover.Image")));
            this.tsbRemover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRemover.Name = "tsbRemover";
            this.tsbRemover.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRemover.Size = new System.Drawing.Size(94, 28);
            this.tsbRemover.Text = "Remover";
            this.tsbRemover.Click += new System.EventHandler(this.tsbRemover_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // tsbAtualizar
            // 
            this.tsbAtualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizar.Image")));
            this.tsbAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizar.Name = "tsbAtualizar";
            this.tsbAtualizar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizar.Size = new System.Drawing.Size(93, 28);
            this.tsbAtualizar.Text = "Atualizar";
            this.tsbAtualizar.Click += new System.EventHandler(this.tsbAtualizar_Click);
            // 
            // tsbVerPlanoAcao
            // 
            this.tsbVerPlanoAcao.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerPlanoAcao.Image")));
            this.tsbVerPlanoAcao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbVerPlanoAcao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerPlanoAcao.Name = "tsbVerPlanoAcao";
            this.tsbVerPlanoAcao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbVerPlanoAcao.Size = new System.Drawing.Size(148, 28);
            this.tsbVerPlanoAcao.Text = "Ver plano de ação";
            this.tsbVerPlanoAcao.Click += new System.EventHandler(this.tsbVerPlanoAcao_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // tsbRelatorio
            // 
            this.tsbRelatorio.Image = ((System.Drawing.Image)(resources.GetObject("tsbRelatorio.Image")));
            this.tsbRelatorio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRelatorio.Name = "tsbRelatorio";
            this.tsbRelatorio.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRelatorio.Size = new System.Drawing.Size(94, 28);
            this.tsbRelatorio.Text = "Relatório";
            this.tsbRelatorio.Click += new System.EventHandler(this.tsbRelatorio_Click);
            // 
            // tsbHistorico
            // 
            this.tsbHistorico.Image = ((System.Drawing.Image)(resources.GetObject("tsbHistorico.Image")));
            this.tsbHistorico.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbHistorico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbHistorico.Name = "tsbHistorico";
            this.tsbHistorico.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbHistorico.Size = new System.Drawing.Size(95, 28);
            this.tsbHistorico.Text = "Histórico";
            this.tsbHistorico.Click += new System.EventHandler(this.tsbHistorico_Click);
            // 
            // tsbFinalizasao
            // 
            this.tsbFinalizasao.Image = ((System.Drawing.Image)(resources.GetObject("tsbFinalizasao.Image")));
            this.tsbFinalizasao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbFinalizasao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFinalizasao.Name = "tsbFinalizasao";
            this.tsbFinalizasao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbFinalizasao.Size = new System.Drawing.Size(90, 20);
            this.tsbFinalizasao.Text = "Finalizar";
            this.tsbFinalizasao.ToolTipText = "Finalizar Não Confor.";
            this.tsbFinalizasao.Click += new System.EventHandler(this.tsbFinalizasao_Click);
            // 
            // tsbConclusao
            // 
            this.tsbConclusao.Image = ((System.Drawing.Image)(resources.GetObject("tsbConclusao.Image")));
            this.tsbConclusao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbConclusao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbConclusao.Name = "tsbConclusao";
            this.tsbConclusao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbConclusao.Size = new System.Drawing.Size(92, 19);
            this.tsbConclusao.Text = "Concluir";
            this.tsbConclusao.Click += new System.EventHandler(this.tsbConclusao_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslTotalItens});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(910, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslTotalItens
            // 
            this.tslTotalItens.Name = "tslTotalItens";
            this.tslTotalItens.Size = new System.Drawing.Size(40, 17);
            this.tslTotalItens.Text = "           ";
            // 
            // tsbExportar
            // 
            this.tsbExportar.Image = global::QUALIDADE.Properties.Resources.iconfinder_export_3855597__2_;
            this.tsbExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExportar.Name = "tsbExportar";
            this.tsbExportar.Size = new System.Drawing.Size(71, 28);
            this.tsbExportar.Text = "Exportar";
            this.tsbExportar.Click += new System.EventHandler(this.exportarParaXLSToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // FormListaNConformidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 450);
            this.Controls.Add(this.dgvListagem);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormListaNConformidade";
            this.Text = "Não Conformidades";
            this.Load += new System.EventHandler(this.FormListaNConformidade_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cNCONFORMBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListagem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbAdicionar;
        private System.Windows.Forms.ToolStripButton tsbEditar;
        private System.Windows.Forms.ToolStripButton tsbRemover;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbAtualizar;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslTotalItens;
        private System.Windows.Forms.BindingSource cNCONFORMBindingSource;
        private System.Windows.Forms.ToolStripButton tsbConclusao;
        private System.Windows.Forms.ToolStripButton tsbFinalizasao;
        private System.Windows.Forms.ToolStripButton tsbHistorico;
        private System.Windows.Forms.ToolStripButton tsbRelatorio;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbVerPlanoAcao;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton tsbExportar;
    }
}