﻿namespace QUALIDADE.Forms.Cadastros.NaoConformidade
{
    partial class FormNConformidade
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNConformidade));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPrincipal = new System.Windows.Forms.TabPage();
            this.txtDataConformidade = new System.Windows.Forms.MaskedTextBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.lblUsuário = new System.Windows.Forms.Label();
            this.btnAddRelatores = new System.Windows.Forms.Button();
            this.cbExecutor = new System.Windows.Forms.ComboBox();
            this.dgvRelatores = new System.Windows.Forms.DataGridView();
            this.DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.nOMEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODUSUARIOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lOGINDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sENHADataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gERENTEDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.aLTERARSENHADataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.rECCREATEDBYDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECCREATEDONDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECMODIFIEDONDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vERPLANOACAODataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.eMAILDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cNCONFORMDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cRESPCONFORMDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gPERMISSAODataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cKSELECIONADODataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cODCOLIGADADataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gUSUARIOBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cbRelatores = new System.Windows.Forms.ComboBox();
            this.lblEvidenciaTrataiva = new System.Windows.Forms.LinkLabel();
            this.lblEvidenciaObs = new System.Windows.Forms.LinkLabel();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.ckAfetaQualidadeNAO = new System.Windows.Forms.CheckBox();
            this.ckAfetaQualidadeSIM = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ckFatoIsoladoNAO = new System.Windows.Forms.CheckBox();
            this.ckFatoIsoladoSIM = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ckNaoAcaoCorrretiva = new System.Windows.Forms.CheckBox();
            this.ckSimAcaoCorrretiva = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtEvidenciaTratativa = new System.Windows.Forms.TextBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.txtDescricaoAbrangencia = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtAnaliseCausa = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAcaoImediata = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNConfRealPotencial = new System.Windows.Forms.TextBox();
            this.cbCCusto = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbExtratificacao = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbSetor = new System.Windows.Forms.ComboBox();
            this.cbTPRegistro = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbClassif = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTipoAcao = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCodConformidade = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtEvidenciaObs = new System.Windows.Forms.TextBox();
            this.tabPlanoAcao = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.calendQuando = new Syncfusion.Windows.Forms.Tools.MonthCalendarAdv();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtQuem = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.txtQue = new System.Windows.Forms.TextBox();
            this.tabAnexos = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dgvEvdTratativa = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionarEvdTrat = new System.Windows.Forms.ToolStripButton();
            this.tsbRemoverEvdTrat = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizarEvdTrat = new System.Windows.Forms.ToolStripButton();
            this.tsbVerAnexoEvdTrat = new System.Windows.Forms.ToolStripButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dgvEvdObservada = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionarEvdObs = new System.Windows.Forms.ToolStripButton();
            this.tsbRemoverEvdObs = new System.Windows.Forms.ToolStripButton();
            this.tsbVerAnexoEvdObs = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizarEvdObs = new System.Windows.Forms.ToolStripButton();
            this.gUSUARIOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelatores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gUSUARIOBindingSource1)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPlanoAcao.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calendQuando)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabAnexos.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvdTratativa)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvdObservada)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gUSUARIOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(951, 508);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 23;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(839, 508);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 22;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPrincipal);
            this.tabControl.Controls.Add(this.tabPlanoAcao);
            this.tabControl.Controls.Add(this.tabAnexos);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1069, 547);
            this.tabControl.TabIndex = 30;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.txtDataConformidade);
            this.tabPrincipal.Controls.Add(this.groupBox16);
            this.tabPrincipal.Controls.Add(this.btnAddRelatores);
            this.tabPrincipal.Controls.Add(this.cbExecutor);
            this.tabPrincipal.Controls.Add(this.dgvRelatores);
            this.tabPrincipal.Controls.Add(this.cbRelatores);
            this.tabPrincipal.Controls.Add(this.lblEvidenciaTrataiva);
            this.tabPrincipal.Controls.Add(this.lblEvidenciaObs);
            this.tabPrincipal.Controls.Add(this.groupBox14);
            this.tabPrincipal.Controls.Add(this.groupBox6);
            this.tabPrincipal.Controls.Add(this.groupBox4);
            this.tabPrincipal.Controls.Add(this.groupBox7);
            this.tabPrincipal.Controls.Add(this.groupBox15);
            this.tabPrincipal.Controls.Add(this.groupBox5);
            this.tabPrincipal.Controls.Add(this.groupBox2);
            this.tabPrincipal.Controls.Add(this.groupBox1);
            this.tabPrincipal.Controls.Add(this.cbCCusto);
            this.tabPrincipal.Controls.Add(this.label6);
            this.tabPrincipal.Controls.Add(this.label7);
            this.tabPrincipal.Controls.Add(this.label4);
            this.tabPrincipal.Controls.Add(this.cbExtratificacao);
            this.tabPrincipal.Controls.Add(this.label8);
            this.tabPrincipal.Controls.Add(this.label3);
            this.tabPrincipal.Controls.Add(this.cbSetor);
            this.tabPrincipal.Controls.Add(this.cbTPRegistro);
            this.tabPrincipal.Controls.Add(this.label9);
            this.tabPrincipal.Controls.Add(this.label5);
            this.tabPrincipal.Controls.Add(this.cbClassif);
            this.tabPrincipal.Controls.Add(this.label1);
            this.tabPrincipal.Controls.Add(this.cbTipoAcao);
            this.tabPrincipal.Controls.Add(this.label22);
            this.tabPrincipal.Controls.Add(this.txtCodConformidade);
            this.tabPrincipal.Controls.Add(this.label2);
            this.tabPrincipal.Controls.Add(this.groupBox3);
            this.tabPrincipal.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tabPrincipal.Location = new System.Drawing.Point(4, 22);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrincipal.Size = new System.Drawing.Size(1061, 521);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Text = "Cadastro Principal";
            this.tabPrincipal.UseVisualStyleBackColor = true;
            // 
            // txtDataConformidade
            // 
            this.txtDataConformidade.Location = new System.Drawing.Point(944, 36);
            this.txtDataConformidade.Mask = "99/99/9999";
            this.txtDataConformidade.Name = "txtDataConformidade";
            this.txtDataConformidade.Size = new System.Drawing.Size(100, 22);
            this.txtDataConformidade.TabIndex = 44;
            this.txtDataConformidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.lblUsuário);
            this.groupBox16.Location = new System.Drawing.Point(843, 348);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(201, 60);
            this.groupBox16.TabIndex = 21;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Responsável pelo centro de custo";
            // 
            // lblUsuário
            // 
            this.lblUsuário.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUsuário.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblUsuário.ForeColor = System.Drawing.Color.Black;
            this.lblUsuário.Location = new System.Drawing.Point(3, 18);
            this.lblUsuário.Name = "lblUsuário";
            this.lblUsuário.Size = new System.Drawing.Size(195, 39);
            this.lblUsuário.TabIndex = 32;
            this.lblUsuário.Text = "---";
            this.lblUsuário.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAddRelatores
            // 
            this.btnAddRelatores.Location = new System.Drawing.Point(273, 364);
            this.btnAddRelatores.Name = "btnAddRelatores";
            this.btnAddRelatores.Size = new System.Drawing.Size(75, 23);
            this.btnAddRelatores.TabIndex = 18;
            this.btnAddRelatores.Text = "Adicionar";
            this.btnAddRelatores.UseVisualStyleBackColor = true;
            this.btnAddRelatores.Click += new System.EventHandler(this.btnAddRelatores_Click);
            // 
            // cbExecutor
            // 
            this.cbExecutor.FormattingEnabled = true;
            this.cbExecutor.Location = new System.Drawing.Point(354, 364);
            this.cbExecutor.Name = "cbExecutor";
            this.cbExecutor.Size = new System.Drawing.Size(250, 21);
            this.cbExecutor.TabIndex = 19;
            // 
            // dgvRelatores
            // 
            this.dgvRelatores.AllowUserToAddRows = false;
            this.dgvRelatores.AutoGenerateColumns = false;
            this.dgvRelatores.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvRelatores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRelatores.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRelatores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRelatores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DELETE,
            this.nOMEDataGridViewTextBoxColumn1,
            this.CODUSUARIOS,
            this.lOGINDataGridViewTextBoxColumn1,
            this.sENHADataGridViewTextBoxColumn1,
            this.gERENTEDataGridViewCheckBoxColumn1,
            this.aLTERARSENHADataGridViewCheckBoxColumn1,
            this.rECCREATEDBYDataGridViewTextBoxColumn1,
            this.rECCREATEDONDataGridViewTextBoxColumn1,
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1,
            this.rECMODIFIEDONDataGridViewTextBoxColumn1,
            this.vERPLANOACAODataGridViewCheckBoxColumn1,
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1,
            this.eMAILDataGridViewTextBoxColumn1,
            this.cNCONFORMDataGridViewTextBoxColumn1,
            this.cRESPCONFORMDataGridViewTextBoxColumn1,
            this.gPERMISSAODataGridViewTextBoxColumn1,
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1,
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1,
            this.cKSELECIONADODataGridViewCheckBoxColumn1,
            this.cODCOLIGADADataGridViewTextBoxColumn1});
            this.dgvRelatores.DataSource = this.gUSUARIOBindingSource1;
            this.dgvRelatores.Location = new System.Drawing.Point(20, 391);
            this.dgvRelatores.Name = "dgvRelatores";
            this.dgvRelatores.ReadOnly = true;
            this.dgvRelatores.Size = new System.Drawing.Size(328, 115);
            this.dgvRelatores.TabIndex = 43;
            this.dgvRelatores.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRelatores_CellContentClick);
            // 
            // DELETE
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.NullValue = "X";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.DELETE.DefaultCellStyle = dataGridViewCellStyle2;
            this.DELETE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DELETE.HeaderText = "X";
            this.DELETE.Name = "DELETE";
            this.DELETE.ReadOnly = true;
            this.DELETE.Text = "X";
            this.DELETE.Width = 30;
            // 
            // nOMEDataGridViewTextBoxColumn1
            // 
            this.nOMEDataGridViewTextBoxColumn1.DataPropertyName = "NOME";
            this.nOMEDataGridViewTextBoxColumn1.HeaderText = "Nome";
            this.nOMEDataGridViewTextBoxColumn1.Name = "nOMEDataGridViewTextBoxColumn1";
            this.nOMEDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nOMEDataGridViewTextBoxColumn1.Width = 225;
            // 
            // CODUSUARIOS
            // 
            this.CODUSUARIOS.DataPropertyName = "CODUSUARIO";
            this.CODUSUARIOS.HeaderText = "CODUSUARIO";
            this.CODUSUARIOS.Name = "CODUSUARIOS";
            this.CODUSUARIOS.ReadOnly = true;
            this.CODUSUARIOS.Visible = false;
            // 
            // lOGINDataGridViewTextBoxColumn1
            // 
            this.lOGINDataGridViewTextBoxColumn1.DataPropertyName = "LOGIN";
            this.lOGINDataGridViewTextBoxColumn1.HeaderText = "LOGIN";
            this.lOGINDataGridViewTextBoxColumn1.Name = "lOGINDataGridViewTextBoxColumn1";
            this.lOGINDataGridViewTextBoxColumn1.ReadOnly = true;
            this.lOGINDataGridViewTextBoxColumn1.Visible = false;
            // 
            // sENHADataGridViewTextBoxColumn1
            // 
            this.sENHADataGridViewTextBoxColumn1.DataPropertyName = "SENHA";
            this.sENHADataGridViewTextBoxColumn1.HeaderText = "SENHA";
            this.sENHADataGridViewTextBoxColumn1.Name = "sENHADataGridViewTextBoxColumn1";
            this.sENHADataGridViewTextBoxColumn1.ReadOnly = true;
            this.sENHADataGridViewTextBoxColumn1.Visible = false;
            // 
            // gERENTEDataGridViewCheckBoxColumn1
            // 
            this.gERENTEDataGridViewCheckBoxColumn1.DataPropertyName = "GERENTE";
            this.gERENTEDataGridViewCheckBoxColumn1.HeaderText = "GERENTE";
            this.gERENTEDataGridViewCheckBoxColumn1.Name = "gERENTEDataGridViewCheckBoxColumn1";
            this.gERENTEDataGridViewCheckBoxColumn1.ReadOnly = true;
            this.gERENTEDataGridViewCheckBoxColumn1.Visible = false;
            // 
            // aLTERARSENHADataGridViewCheckBoxColumn1
            // 
            this.aLTERARSENHADataGridViewCheckBoxColumn1.DataPropertyName = "ALTERARSENHA";
            this.aLTERARSENHADataGridViewCheckBoxColumn1.HeaderText = "ALTERARSENHA";
            this.aLTERARSENHADataGridViewCheckBoxColumn1.Name = "aLTERARSENHADataGridViewCheckBoxColumn1";
            this.aLTERARSENHADataGridViewCheckBoxColumn1.ReadOnly = true;
            this.aLTERARSENHADataGridViewCheckBoxColumn1.Visible = false;
            // 
            // rECCREATEDBYDataGridViewTextBoxColumn1
            // 
            this.rECCREATEDBYDataGridViewTextBoxColumn1.DataPropertyName = "RECCREATEDBY";
            this.rECCREATEDBYDataGridViewTextBoxColumn1.HeaderText = "RECCREATEDBY";
            this.rECCREATEDBYDataGridViewTextBoxColumn1.Name = "rECCREATEDBYDataGridViewTextBoxColumn1";
            this.rECCREATEDBYDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rECCREATEDBYDataGridViewTextBoxColumn1.Visible = false;
            // 
            // rECCREATEDONDataGridViewTextBoxColumn1
            // 
            this.rECCREATEDONDataGridViewTextBoxColumn1.DataPropertyName = "RECCREATEDON";
            this.rECCREATEDONDataGridViewTextBoxColumn1.HeaderText = "RECCREATEDON";
            this.rECCREATEDONDataGridViewTextBoxColumn1.Name = "rECCREATEDONDataGridViewTextBoxColumn1";
            this.rECCREATEDONDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rECCREATEDONDataGridViewTextBoxColumn1.Visible = false;
            // 
            // rECMODIFIEDBYDataGridViewTextBoxColumn1
            // 
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1.DataPropertyName = "RECMODIFIEDBY";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1.HeaderText = "RECMODIFIEDBY";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1.Name = "rECMODIFIEDBYDataGridViewTextBoxColumn1";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rECMODIFIEDBYDataGridViewTextBoxColumn1.Visible = false;
            // 
            // rECMODIFIEDONDataGridViewTextBoxColumn1
            // 
            this.rECMODIFIEDONDataGridViewTextBoxColumn1.DataPropertyName = "RECMODIFIEDON";
            this.rECMODIFIEDONDataGridViewTextBoxColumn1.HeaderText = "RECMODIFIEDON";
            this.rECMODIFIEDONDataGridViewTextBoxColumn1.Name = "rECMODIFIEDONDataGridViewTextBoxColumn1";
            this.rECMODIFIEDONDataGridViewTextBoxColumn1.ReadOnly = true;
            this.rECMODIFIEDONDataGridViewTextBoxColumn1.Visible = false;
            // 
            // vERPLANOACAODataGridViewCheckBoxColumn1
            // 
            this.vERPLANOACAODataGridViewCheckBoxColumn1.DataPropertyName = "VERPLANOACAO";
            this.vERPLANOACAODataGridViewCheckBoxColumn1.HeaderText = "VERPLANOACAO";
            this.vERPLANOACAODataGridViewCheckBoxColumn1.Name = "vERPLANOACAODataGridViewCheckBoxColumn1";
            this.vERPLANOACAODataGridViewCheckBoxColumn1.ReadOnly = true;
            this.vERPLANOACAODataGridViewCheckBoxColumn1.Visible = false;
            // 
            // vEREFICACIAACAODataGridViewCheckBoxColumn1
            // 
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1.DataPropertyName = "VEREFICACIAACAO";
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1.HeaderText = "VEREFICACIAACAO";
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1.Name = "vEREFICACIAACAODataGridViewCheckBoxColumn1";
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1.ReadOnly = true;
            this.vEREFICACIAACAODataGridViewCheckBoxColumn1.Visible = false;
            // 
            // eMAILDataGridViewTextBoxColumn1
            // 
            this.eMAILDataGridViewTextBoxColumn1.DataPropertyName = "EMAIL";
            this.eMAILDataGridViewTextBoxColumn1.HeaderText = "EMAIL";
            this.eMAILDataGridViewTextBoxColumn1.Name = "eMAILDataGridViewTextBoxColumn1";
            this.eMAILDataGridViewTextBoxColumn1.ReadOnly = true;
            this.eMAILDataGridViewTextBoxColumn1.Visible = false;
            // 
            // cNCONFORMDataGridViewTextBoxColumn1
            // 
            this.cNCONFORMDataGridViewTextBoxColumn1.DataPropertyName = "CNCONFORM";
            this.cNCONFORMDataGridViewTextBoxColumn1.HeaderText = "CNCONFORM";
            this.cNCONFORMDataGridViewTextBoxColumn1.Name = "cNCONFORMDataGridViewTextBoxColumn1";
            this.cNCONFORMDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cNCONFORMDataGridViewTextBoxColumn1.Visible = false;
            // 
            // cRESPCONFORMDataGridViewTextBoxColumn1
            // 
            this.cRESPCONFORMDataGridViewTextBoxColumn1.DataPropertyName = "CRESPCONFORM";
            this.cRESPCONFORMDataGridViewTextBoxColumn1.HeaderText = "CRESPCONFORM";
            this.cRESPCONFORMDataGridViewTextBoxColumn1.Name = "cRESPCONFORMDataGridViewTextBoxColumn1";
            this.cRESPCONFORMDataGridViewTextBoxColumn1.ReadOnly = true;
            this.cRESPCONFORMDataGridViewTextBoxColumn1.Visible = false;
            // 
            // gPERMISSAODataGridViewTextBoxColumn1
            // 
            this.gPERMISSAODataGridViewTextBoxColumn1.DataPropertyName = "GPERMISSAO";
            this.gPERMISSAODataGridViewTextBoxColumn1.HeaderText = "GPERMISSAO";
            this.gPERMISSAODataGridViewTextBoxColumn1.Name = "gPERMISSAODataGridViewTextBoxColumn1";
            this.gPERMISSAODataGridViewTextBoxColumn1.ReadOnly = true;
            this.gPERMISSAODataGridViewTextBoxColumn1.Visible = false;
            // 
            // cVPACAOEFICACIADataGridViewTextBoxColumn1
            // 
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1.DataPropertyName = "CVPACAOEFICACIA";
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1.HeaderText = "CVPACAOEFICACIA";
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1.Name = "cVPACAOEFICACIADataGridViewTextBoxColumn1";
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1.ReadOnly = true;
            this.cVPACAOEFICACIADataGridViewTextBoxColumn1.Visible = false;
            // 
            // cVPACAOEFICACIA1DataGridViewTextBoxColumn1
            // 
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1.DataPropertyName = "CVPACAOEFICACIA1";
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1.HeaderText = "CVPACAOEFICACIA1";
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1.Name = "cVPACAOEFICACIA1DataGridViewTextBoxColumn1";
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1.ReadOnly = true;
            this.cVPACAOEFICACIA1DataGridViewTextBoxColumn1.Visible = false;
            // 
            // cKSELECIONADODataGridViewCheckBoxColumn1
            // 
            this.cKSELECIONADODataGridViewCheckBoxColumn1.DataPropertyName = "CKSELECIONADO";
            this.cKSELECIONADODataGridViewCheckBoxColumn1.HeaderText = "CKSELECIONADO";
            this.cKSELECIONADODataGridViewCheckBoxColumn1.Name = "cKSELECIONADODataGridViewCheckBoxColumn1";
            this.cKSELECIONADODataGridViewCheckBoxColumn1.ReadOnly = true;
            this.cKSELECIONADODataGridViewCheckBoxColumn1.Visible = false;
            // 
            // cODCOLIGADADataGridViewTextBoxColumn1
            // 
            this.cODCOLIGADADataGridViewTextBoxColumn1.DataPropertyName = "CODCOLIGADA";
            this.cODCOLIGADADataGridViewTextBoxColumn1.HeaderText = "CODCOLIGADA";
            this.cODCOLIGADADataGridViewTextBoxColumn1.Name = "cODCOLIGADADataGridViewTextBoxColumn1";
            this.cODCOLIGADADataGridViewTextBoxColumn1.ReadOnly = true;
            this.cODCOLIGADADataGridViewTextBoxColumn1.Visible = false;
            // 
            // gUSUARIOBindingSource1
            // 
            this.gUSUARIOBindingSource1.DataSource = typeof(QUALIDADE.Dominio.GUSUARIO);
            // 
            // cbRelatores
            // 
            this.cbRelatores.FormattingEnabled = true;
            this.cbRelatores.Location = new System.Drawing.Point(20, 364);
            this.cbRelatores.Name = "cbRelatores";
            this.cbRelatores.Size = new System.Drawing.Size(247, 21);
            this.cbRelatores.TabIndex = 17;
            // 
            // lblEvidenciaTrataiva
            // 
            this.lblEvidenciaTrataiva.AutoSize = true;
            this.lblEvidenciaTrataiva.Font = new System.Drawing.Font("Segoe UI", 7.25F);
            this.lblEvidenciaTrataiva.Location = new System.Drawing.Point(182, 206);
            this.lblEvidenciaTrataiva.Name = "lblEvidenciaTrataiva";
            this.lblEvidenciaTrataiva.Size = new System.Drawing.Size(85, 12);
            this.lblEvidenciaTrataiva.TabIndex = 11;
            this.lblEvidenciaTrataiva.TabStop = true;
            this.lblEvidenciaTrataiva.Text = "Arquivar Evidência";
            this.lblEvidenciaTrataiva.Click += new System.EventHandler(this.lblEvidenciaTrataiva_Click);
            // 
            // lblEvidenciaObs
            // 
            this.lblEvidenciaObs.AutoSize = true;
            this.lblEvidenciaObs.Font = new System.Drawing.Font("Segoe UI", 7.25F);
            this.lblEvidenciaObs.Location = new System.Drawing.Point(956, 71);
            this.lblEvidenciaObs.Name = "lblEvidenciaObs";
            this.lblEvidenciaObs.Size = new System.Drawing.Size(85, 12);
            this.lblEvidenciaObs.TabIndex = 9;
            this.lblEvidenciaObs.TabStop = true;
            this.lblEvidenciaObs.Text = "Arquivar Evidência";
            this.lblEvidenciaObs.Click += new System.EventHandler(this.lblEvidenciaObs_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.ckAfetaQualidadeNAO);
            this.groupBox14.Controls.Add(this.ckAfetaQualidadeSIM);
            this.groupBox14.Location = new System.Drawing.Point(535, 279);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(250, 54);
            this.groupBox14.TabIndex = 15;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Afeta a qualidade do produto / serviço?";
            // 
            // ckAfetaQualidadeNAO
            // 
            this.ckAfetaQualidadeNAO.AutoSize = true;
            this.ckAfetaQualidadeNAO.Checked = true;
            this.ckAfetaQualidadeNAO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckAfetaQualidadeNAO.Location = new System.Drawing.Point(135, 26);
            this.ckAfetaQualidadeNAO.Name = "ckAfetaQualidadeNAO";
            this.ckAfetaQualidadeNAO.Size = new System.Drawing.Size(47, 17);
            this.ckAfetaQualidadeNAO.TabIndex = 1;
            this.ckAfetaQualidadeNAO.Text = "Não";
            this.ckAfetaQualidadeNAO.UseVisualStyleBackColor = true;
            this.ckAfetaQualidadeNAO.CheckedChanged += new System.EventHandler(this.ckAfetaQualidadeNAO_CheckedChanged);
            // 
            // ckAfetaQualidadeSIM
            // 
            this.ckAfetaQualidadeSIM.AutoSize = true;
            this.ckAfetaQualidadeSIM.Location = new System.Drawing.Point(69, 26);
            this.ckAfetaQualidadeSIM.Name = "ckAfetaQualidadeSIM";
            this.ckAfetaQualidadeSIM.Size = new System.Drawing.Size(44, 17);
            this.ckAfetaQualidadeSIM.TabIndex = 0;
            this.ckAfetaQualidadeSIM.Text = "Sim";
            this.ckAfetaQualidadeSIM.UseVisualStyleBackColor = true;
            this.ckAfetaQualidadeSIM.CheckedChanged += new System.EventHandler(this.ckAfetaQualidadeSIM_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ckFatoIsoladoNAO);
            this.groupBox6.Controls.Add(this.ckFatoIsoladoSIM);
            this.groupBox6.Location = new System.Drawing.Point(535, 210);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(253, 53);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fato Isolado?";
            // 
            // ckFatoIsoladoNAO
            // 
            this.ckFatoIsoladoNAO.AutoSize = true;
            this.ckFatoIsoladoNAO.Checked = true;
            this.ckFatoIsoladoNAO.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckFatoIsoladoNAO.Location = new System.Drawing.Point(131, 26);
            this.ckFatoIsoladoNAO.Name = "ckFatoIsoladoNAO";
            this.ckFatoIsoladoNAO.Size = new System.Drawing.Size(47, 17);
            this.ckFatoIsoladoNAO.TabIndex = 1;
            this.ckFatoIsoladoNAO.Text = "Não";
            this.ckFatoIsoladoNAO.UseVisualStyleBackColor = true;
            this.ckFatoIsoladoNAO.CheckedChanged += new System.EventHandler(this.ckFatoIsoladoNAO_CheckedChanged);
            // 
            // ckFatoIsoladoSIM
            // 
            this.ckFatoIsoladoSIM.AutoSize = true;
            this.ckFatoIsoladoSIM.Location = new System.Drawing.Point(69, 26);
            this.ckFatoIsoladoSIM.Name = "ckFatoIsoladoSIM";
            this.ckFatoIsoladoSIM.Size = new System.Drawing.Size(44, 17);
            this.ckFatoIsoladoSIM.TabIndex = 0;
            this.ckFatoIsoladoSIM.Text = "Sim";
            this.ckFatoIsoladoSIM.UseVisualStyleBackColor = true;
            this.ckFatoIsoladoSIM.CheckedChanged += new System.EventHandler(this.ckFatoIsoladoSIM_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ckNaoAcaoCorrretiva);
            this.groupBox4.Controls.Add(this.ckSimAcaoCorrretiva);
            this.groupBox4.Location = new System.Drawing.Point(625, 348);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(201, 60);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Necessarío plano de ação";
            // 
            // ckNaoAcaoCorrretiva
            // 
            this.ckNaoAcaoCorrretiva.AutoSize = true;
            this.ckNaoAcaoCorrretiva.Checked = true;
            this.ckNaoAcaoCorrretiva.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckNaoAcaoCorrretiva.Location = new System.Drawing.Point(116, 27);
            this.ckNaoAcaoCorrretiva.Name = "ckNaoAcaoCorrretiva";
            this.ckNaoAcaoCorrretiva.Size = new System.Drawing.Size(47, 17);
            this.ckNaoAcaoCorrretiva.TabIndex = 1;
            this.ckNaoAcaoCorrretiva.Text = "Não";
            this.ckNaoAcaoCorrretiva.UseVisualStyleBackColor = true;
            this.ckNaoAcaoCorrretiva.CheckedChanged += new System.EventHandler(this.ckNaoAcaoCorrretiva_CheckedChanged);
            // 
            // ckSimAcaoCorrretiva
            // 
            this.ckSimAcaoCorrretiva.AutoSize = true;
            this.ckSimAcaoCorrretiva.Location = new System.Drawing.Point(44, 27);
            this.ckSimAcaoCorrretiva.Name = "ckSimAcaoCorrretiva";
            this.ckSimAcaoCorrretiva.Size = new System.Drawing.Size(44, 17);
            this.ckSimAcaoCorrretiva.TabIndex = 0;
            this.ckSimAcaoCorrretiva.Text = "Sim";
            this.ckSimAcaoCorrretiva.UseVisualStyleBackColor = true;
            this.ckSimAcaoCorrretiva.CheckedChanged += new System.EventHandler(this.ckSimAcaoCorrretiva_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtEvidenciaTratativa);
            this.groupBox7.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox7.Location = new System.Drawing.Point(17, 210);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(253, 126);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Evidência da Tratativa:";
            // 
            // txtEvidenciaTratativa
            // 
            this.txtEvidenciaTratativa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEvidenciaTratativa.Location = new System.Drawing.Point(3, 18);
            this.txtEvidenciaTratativa.Multiline = true;
            this.txtEvidenciaTratativa.Name = "txtEvidenciaTratativa";
            this.txtEvidenciaTratativa.Size = new System.Drawing.Size(247, 105);
            this.txtEvidenciaTratativa.TabIndex = 44;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.txtDescricaoAbrangencia);
            this.groupBox15.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox15.Location = new System.Drawing.Point(794, 210);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(250, 126);
            this.groupBox15.TabIndex = 16;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Descrição Abrangência:";
            // 
            // txtDescricaoAbrangencia
            // 
            this.txtDescricaoAbrangencia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescricaoAbrangencia.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtDescricaoAbrangencia.Location = new System.Drawing.Point(3, 18);
            this.txtDescricaoAbrangencia.Multiline = true;
            this.txtDescricaoAbrangencia.Name = "txtDescricaoAbrangencia";
            this.txtDescricaoAbrangencia.Size = new System.Drawing.Size(244, 105);
            this.txtDescricaoAbrangencia.TabIndex = 44;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtAnaliseCausa);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox5.Location = new System.Drawing.Point(276, 210);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(253, 126);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Análise de causa:";
            // 
            // txtAnaliseCausa
            // 
            this.txtAnaliseCausa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnaliseCausa.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtAnaliseCausa.Location = new System.Drawing.Point(3, 18);
            this.txtAnaliseCausa.Multiline = true;
            this.txtAnaliseCausa.Name = "txtAnaliseCausa";
            this.txtAnaliseCausa.Size = new System.Drawing.Size(247, 105);
            this.txtAnaliseCausa.TabIndex = 44;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtAcaoImediata);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox2.Location = new System.Drawing.Point(535, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(253, 126);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Ação Imediata Tomada:";
            // 
            // txtAcaoImediata
            // 
            this.txtAcaoImediata.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAcaoImediata.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtAcaoImediata.Location = new System.Drawing.Point(3, 18);
            this.txtAcaoImediata.Multiline = true;
            this.txtAcaoImediata.Name = "txtAcaoImediata";
            this.txtAcaoImediata.Size = new System.Drawing.Size(247, 105);
            this.txtAcaoImediata.TabIndex = 44;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNConfRealPotencial);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(276, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 126);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Não Conformidade Real ou Potencial:";
            // 
            // txtNConfRealPotencial
            // 
            this.txtNConfRealPotencial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNConfRealPotencial.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtNConfRealPotencial.Location = new System.Drawing.Point(3, 18);
            this.txtNConfRealPotencial.Multiline = true;
            this.txtNConfRealPotencial.Name = "txtNConfRealPotencial";
            this.txtNConfRealPotencial.Size = new System.Drawing.Size(247, 105);
            this.txtNConfRealPotencial.TabIndex = 44;
            // 
            // cbCCusto
            // 
            this.cbCCusto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbCCusto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCCusto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCCusto.FormattingEnabled = true;
            this.cbCCusto.Location = new System.Drawing.Point(157, 36);
            this.cbCCusto.Name = "cbCCusto";
            this.cbCCusto.Size = new System.Drawing.Size(260, 21);
            this.cbCCusto.TabIndex = 1;
            this.cbCCusto.SelectedIndexChanged += new System.EventHandler(this.cbCCusto_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(351, 348);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Executor:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 348);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Relatores:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(154, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Centro de Custo:";
            // 
            // cbExtratificacao
            // 
            this.cbExtratificacao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbExtratificacao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbExtratificacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExtratificacao.FormattingEnabled = true;
            this.cbExtratificacao.Location = new System.Drawing.Point(20, 164);
            this.cbExtratificacao.Name = "cbExtratificacao";
            this.cbExtratificacao.Size = new System.Drawing.Size(250, 21);
            this.cbExtratificacao.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 87);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Setor (Processo/Área):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(17, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Estratificação:";
            // 
            // cbSetor
            // 
            this.cbSetor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbSetor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSetor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSetor.FormattingEnabled = true;
            this.cbSetor.Location = new System.Drawing.Point(20, 104);
            this.cbSetor.Name = "cbSetor";
            this.cbSetor.Size = new System.Drawing.Size(250, 21);
            this.cbSetor.TabIndex = 5;
            this.cbSetor.SelectedIndexChanged += new System.EventHandler(this.cbSetor_SelectedIndexChanged);
            // 
            // cbTPRegistro
            // 
            this.cbTPRegistro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbTPRegistro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTPRegistro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTPRegistro.FormattingEnabled = true;
            this.cbTPRegistro.Location = new System.Drawing.Point(756, 36);
            this.cbTPRegistro.Name = "cbTPRegistro";
            this.cbTPRegistro.Size = new System.Drawing.Size(182, 21);
            this.cbTPRegistro.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(941, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Dt. Não Conform.:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(753, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Tipo de Registro:";
            // 
            // cbClassif
            // 
            this.cbClassif.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbClassif.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbClassif.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClassif.FormattingEnabled = true;
            this.cbClassif.Location = new System.Drawing.Point(603, 36);
            this.cbClassif.Name = "cbClassif";
            this.cbClassif.Size = new System.Drawing.Size(147, 21);
            this.cbClassif.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(600, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Classificação:";
            // 
            // cbTipoAcao
            // 
            this.cbTipoAcao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbTipoAcao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoAcao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoAcao.FormattingEnabled = true;
            this.cbTipoAcao.Items.AddRange(new object[] {
            "Melhoria",
            "Corretiva"});
            this.cbTipoAcao.Location = new System.Drawing.Point(423, 36);
            this.cbTipoAcao.Name = "cbTipoAcao";
            this.cbTipoAcao.Size = new System.Drawing.Size(174, 21);
            this.cbTipoAcao.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(420, 19);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "Tipo de Ação:";
            // 
            // txtCodConformidade
            // 
            this.txtCodConformidade.Location = new System.Drawing.Point(17, 36);
            this.txtCodConformidade.MaxLength = 20;
            this.txtCodConformidade.Name = "txtCodConformidade";
            this.txtCodConformidade.ReadOnly = true;
            this.txtCodConformidade.Size = new System.Drawing.Size(134, 22);
            this.txtCodConformidade.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "Cód. Não Conformidade:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtEvidenciaObs);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(794, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(250, 126);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Evidência Observada:";
            // 
            // txtEvidenciaObs
            // 
            this.txtEvidenciaObs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEvidenciaObs.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtEvidenciaObs.Location = new System.Drawing.Point(3, 18);
            this.txtEvidenciaObs.Multiline = true;
            this.txtEvidenciaObs.Name = "txtEvidenciaObs";
            this.txtEvidenciaObs.Size = new System.Drawing.Size(244, 105);
            this.txtEvidenciaObs.TabIndex = 44;
            // 
            // tabPlanoAcao
            // 
            this.tabPlanoAcao.Controls.Add(this.groupBox10);
            this.tabPlanoAcao.Location = new System.Drawing.Point(4, 22);
            this.tabPlanoAcao.Name = "tabPlanoAcao";
            this.tabPlanoAcao.Padding = new System.Windows.Forms.Padding(3);
            this.tabPlanoAcao.Size = new System.Drawing.Size(1061, 521);
            this.tabPlanoAcao.TabIndex = 1;
            this.tabPlanoAcao.Text = "Plano de Ação";
            this.tabPlanoAcao.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tableLayoutPanel2);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox10.Location = new System.Drawing.Point(3, 3);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(1055, 515);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Plano de Ação";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox13, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 21);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1043, 456);
            this.tableLayoutPanel2.TabIndex = 46;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.calendQuando);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox13.Location = new System.Drawing.Point(3, 231);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(1037, 222);
            this.groupBox13.TabIndex = 45;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Quando?";
            // 
            // calendQuando
            // 
            this.calendQuando.AllowMultipleSelection = false;
            this.calendQuando.BorderColor = System.Drawing.Color.Transparent;
            this.calendQuando.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.calendQuando.Culture = new System.Globalization.CultureInfo("pt-BR");
            this.calendQuando.DayNamesColor = System.Drawing.Color.Black;
            this.calendQuando.DayNamesFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.calendQuando.DaysHeaderInterior = new Syncfusion.Drawing.BrushInfo(System.Drawing.SystemColors.Window);
            this.calendQuando.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendQuando.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.calendQuando.GridLines = Syncfusion.Windows.Forms.Grid.GridBorderStyle.None;
            this.calendQuando.HeaderHeight = 34;
            this.calendQuando.HeaderStartColor = System.Drawing.Color.White;
            this.calendQuando.HighlightColor = System.Drawing.Color.Black;
            this.calendQuando.Iso8601CalenderFormat = true;
            this.calendQuando.Location = new System.Drawing.Point(3, 18);
            this.calendQuando.MetroColor = System.Drawing.Color.Black;
            this.calendQuando.Name = "calendQuando";
            this.calendQuando.ScrollButtonSize = new System.Drawing.Size(24, 24);
            this.calendQuando.Size = new System.Drawing.Size(1031, 201);
            this.calendQuando.Style = Syncfusion.Windows.Forms.VisualStyle.Metro;
            this.calendQuando.TabIndex = 11;
            this.calendQuando.ThemedEnabledScrollButtons = false;
            this.calendQuando.WeekFont = new System.Drawing.Font("Verdana", 8F);
            // 
            // 
            // 
            this.calendQuando.NoneButton.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.calendQuando.NoneButton.BackColor = System.Drawing.Color.Black;
            this.calendQuando.NoneButton.ForeColor = System.Drawing.Color.White;
            this.calendQuando.NoneButton.Location = new System.Drawing.Point(646, 0);
            this.calendQuando.NoneButton.Size = new System.Drawing.Size(385, 20);
            this.calendQuando.NoneButton.Text = "None kkkk";
            this.calendQuando.NoneButton.ThemeName = "Metro";
            this.calendQuando.NoneButton.UseVisualStyle = true;
            this.calendQuando.NoneButton.Visible = false;
            // 
            // 
            // 
            this.calendQuando.TodayButton.Appearance = Syncfusion.Windows.Forms.ButtonAppearance.Metro;
            this.calendQuando.TodayButton.BackColor = System.Drawing.Color.Black;
            this.calendQuando.TodayButton.ForeColor = System.Drawing.Color.White;
            this.calendQuando.TodayButton.Location = new System.Drawing.Point(0, 0);
            this.calendQuando.TodayButton.Size = new System.Drawing.Size(1031, 20);
            this.calendQuando.TodayButton.Text = "Today";
            this.calendQuando.TodayButton.ThemeName = "Metro";
            this.calendQuando.TodayButton.UseVisualStyle = true;
            this.calendQuando.TodayButton.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox11, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox12, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1037, 222);
            this.tableLayoutPanel3.TabIndex = 46;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtQuem);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox11.Location = new System.Drawing.Point(521, 3);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(513, 216);
            this.groupBox11.TabIndex = 8;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Quem?";
            // 
            // txtQuem
            // 
            this.txtQuem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuem.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtQuem.Location = new System.Drawing.Point(3, 18);
            this.txtQuem.Multiline = true;
            this.txtQuem.Name = "txtQuem";
            this.txtQuem.Size = new System.Drawing.Size(507, 195);
            this.txtQuem.TabIndex = 44;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.txtQue);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox12.Location = new System.Drawing.Point(3, 3);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(512, 216);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "O que?";
            // 
            // txtQue
            // 
            this.txtQue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQue.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtQue.Location = new System.Drawing.Point(3, 18);
            this.txtQue.Multiline = true;
            this.txtQue.Name = "txtQue";
            this.txtQue.Size = new System.Drawing.Size(506, 195);
            this.txtQue.TabIndex = 44;
            // 
            // tabAnexos
            // 
            this.tabAnexos.Controls.Add(this.tableLayoutPanel1);
            this.tabAnexos.Location = new System.Drawing.Point(4, 22);
            this.tabAnexos.Name = "tabAnexos";
            this.tabAnexos.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnexos.Size = new System.Drawing.Size(1061, 521);
            this.tabAnexos.TabIndex = 2;
            this.tabAnexos.Text = "Anexos";
            this.tabAnexos.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox9, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox8, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1042, 474);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dgvEvdTratativa);
            this.groupBox9.Controls.Add(this.toolStrip2);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox9.Location = new System.Drawing.Point(524, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(515, 468);
            this.groupBox9.TabIndex = 1;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Evidências da Tratativa:";
            // 
            // dgvEvdTratativa
            // 
            this.dgvEvdTratativa.AccessibleName = "Table";
            this.dgvEvdTratativa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEvdTratativa.Location = new System.Drawing.Point(3, 43);
            this.dgvEvdTratativa.Name = "dgvEvdTratativa";
            this.dgvEvdTratativa.Size = new System.Drawing.Size(509, 422);
            this.dgvEvdTratativa.TabIndex = 2;
            this.dgvEvdTratativa.Text = "sfDataGrid2";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionarEvdTrat,
            this.tsbRemoverEvdTrat,
            this.tsbAtualizarEvdTrat,
            this.tsbVerAnexoEvdTrat});
            this.toolStrip2.Location = new System.Drawing.Point(3, 18);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(509, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsbAdicionarEvdTrat
            // 
            this.tsbAdicionarEvdTrat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdicionarEvdTrat.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionarEvdTrat.Image")));
            this.tsbAdicionarEvdTrat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionarEvdTrat.Name = "tsbAdicionarEvdTrat";
            this.tsbAdicionarEvdTrat.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionarEvdTrat.Size = new System.Drawing.Size(40, 22);
            this.tsbAdicionarEvdTrat.Text = "Adicionar";
            this.tsbAdicionarEvdTrat.Click += new System.EventHandler(this.tsbAdicionarEvdTrat_Click);
            // 
            // tsbRemoverEvdTrat
            // 
            this.tsbRemoverEvdTrat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRemoverEvdTrat.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemoverEvdTrat.Image")));
            this.tsbRemoverEvdTrat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRemoverEvdTrat.Name = "tsbRemoverEvdTrat";
            this.tsbRemoverEvdTrat.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRemoverEvdTrat.Size = new System.Drawing.Size(40, 22);
            this.tsbRemoverEvdTrat.Text = "Remover";
            this.tsbRemoverEvdTrat.Click += new System.EventHandler(this.tsbRemoverEvdTrat_Click);
            // 
            // tsbAtualizarEvdTrat
            // 
            this.tsbAtualizarEvdTrat.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizarEvdTrat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAtualizarEvdTrat.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarEvdTrat.Image")));
            this.tsbAtualizarEvdTrat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarEvdTrat.Name = "tsbAtualizarEvdTrat";
            this.tsbAtualizarEvdTrat.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarEvdTrat.Size = new System.Drawing.Size(40, 22);
            this.tsbAtualizarEvdTrat.Text = "Atualizar";
            this.tsbAtualizarEvdTrat.Click += new System.EventHandler(this.tsbAtualizarEvdTrat_Click);
            // 
            // tsbVerAnexoEvdTrat
            // 
            this.tsbVerAnexoEvdTrat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVerAnexoEvdTrat.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerAnexoEvdTrat.Image")));
            this.tsbVerAnexoEvdTrat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerAnexoEvdTrat.Name = "tsbVerAnexoEvdTrat";
            this.tsbVerAnexoEvdTrat.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbVerAnexoEvdTrat.Size = new System.Drawing.Size(40, 22);
            this.tsbVerAnexoEvdTrat.Text = "Anexo";
            this.tsbVerAnexoEvdTrat.Click += new System.EventHandler(this.tsbVerAnexoEvdTrat_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dgvEvdObservada);
            this.groupBox8.Controls.Add(this.toolStrip1);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox8.Location = new System.Drawing.Point(3, 3);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(515, 468);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Evidências Observadas:";
            // 
            // dgvEvdObservada
            // 
            this.dgvEvdObservada.AccessibleName = "Table";
            this.dgvEvdObservada.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvEvdObservada.Location = new System.Drawing.Point(3, 43);
            this.dgvEvdObservada.Name = "dgvEvdObservada";
            this.dgvEvdObservada.Size = new System.Drawing.Size(509, 422);
            this.dgvEvdObservada.TabIndex = 1;
            this.dgvEvdObservada.Text = "sfDataGrid1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionarEvdObs,
            this.tsbRemoverEvdObs,
            this.tsbVerAnexoEvdObs,
            this.tsbAtualizarEvdObs});
            this.toolStrip1.Location = new System.Drawing.Point(3, 18);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(509, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbAdicionarEvdObs
            // 
            this.tsbAdicionarEvdObs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdicionarEvdObs.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionarEvdObs.Image")));
            this.tsbAdicionarEvdObs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionarEvdObs.Name = "tsbAdicionarEvdObs";
            this.tsbAdicionarEvdObs.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionarEvdObs.Size = new System.Drawing.Size(40, 22);
            this.tsbAdicionarEvdObs.Text = "Adicionar";
            this.tsbAdicionarEvdObs.Click += new System.EventHandler(this.tsbAdicionarEvdObs_Click);
            // 
            // tsbRemoverEvdObs
            // 
            this.tsbRemoverEvdObs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRemoverEvdObs.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemoverEvdObs.Image")));
            this.tsbRemoverEvdObs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRemoverEvdObs.Name = "tsbRemoverEvdObs";
            this.tsbRemoverEvdObs.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRemoverEvdObs.Size = new System.Drawing.Size(40, 22);
            this.tsbRemoverEvdObs.Text = "Remover";
            this.tsbRemoverEvdObs.Click += new System.EventHandler(this.tsbRemoverEvdObs_Click);
            // 
            // tsbVerAnexoEvdObs
            // 
            this.tsbVerAnexoEvdObs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVerAnexoEvdObs.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerAnexoEvdObs.Image")));
            this.tsbVerAnexoEvdObs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerAnexoEvdObs.Name = "tsbVerAnexoEvdObs";
            this.tsbVerAnexoEvdObs.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbVerAnexoEvdObs.Size = new System.Drawing.Size(40, 22);
            this.tsbVerAnexoEvdObs.Text = "Anexo";
            this.tsbVerAnexoEvdObs.Click += new System.EventHandler(this.tsbVerAnexoEvdObs_Click);
            // 
            // tsbAtualizarEvdObs
            // 
            this.tsbAtualizarEvdObs.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizarEvdObs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAtualizarEvdObs.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarEvdObs.Image")));
            this.tsbAtualizarEvdObs.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarEvdObs.Name = "tsbAtualizarEvdObs";
            this.tsbAtualizarEvdObs.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarEvdObs.Size = new System.Drawing.Size(40, 22);
            this.tsbAtualizarEvdObs.Text = "Atualizar";
            this.tsbAtualizarEvdObs.Click += new System.EventHandler(this.tsbAtualizarEvdObs_Click);
            // 
            // gUSUARIOBindingSource
            // 
            this.gUSUARIOBindingSource.DataSource = typeof(QUALIDADE.Dominio.GUSUARIO);
            // 
            // FormNConformidade
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 547);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNConformidade";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Não Conformidade";
            this.Load += new System.EventHandler(this.FormNConformidade_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPrincipal.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRelatores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gUSUARIOBindingSource1)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPlanoAcao.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calendQuando)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabAnexos.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvdTratativa)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEvdObservada)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gUSUARIOBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPrincipal;
        private System.Windows.Forms.TextBox txtCodConformidade;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPlanoAcao;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbCCusto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbExtratificacao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbClassif;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTipoAcao;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtEvidenciaObs;
        private System.Windows.Forms.TextBox txtAcaoImediata;
        private System.Windows.Forms.TextBox txtNConfRealPotencial;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox ckNaoAcaoCorrretiva;
        private System.Windows.Forms.CheckBox ckSimAcaoCorrretiva;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtAnaliseCausa;
        private System.Windows.Forms.LinkLabel lblEvidenciaObs;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.LinkLabel lblEvidenciaTrataiva;
        private System.Windows.Forms.TextBox txtEvidenciaTratativa;
        private System.Windows.Forms.TabPage tabAnexos;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton tsbAdicionarEvdTrat;
        private System.Windows.Forms.ToolStripButton tsbRemoverEvdTrat;
        private System.Windows.Forms.ToolStripButton tsbAtualizarEvdTrat;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbAdicionarEvdObs;
        private System.Windows.Forms.ToolStripButton tsbRemoverEvdObs;
        private System.Windows.Forms.ToolStripButton tsbAtualizarEvdObs;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvEvdTratativa;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvEvdObservada;
        private System.Windows.Forms.ToolStripButton tsbVerAnexoEvdObs;
        private System.Windows.Forms.ToolStripButton tsbVerAnexoEvdTrat;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox13;
        private Syncfusion.Windows.Forms.Tools.MonthCalendarAdv calendQuando;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtQuem;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox txtQue;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox txtDescricaoAbrangencia;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label lblUsuário;
        private System.Windows.Forms.ComboBox cbRelatores;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvRelatores;
        private System.Windows.Forms.BindingSource gUSUARIOBindingSource;
        private System.Windows.Forms.BindingSource gUSUARIOBindingSource1;
        private System.Windows.Forms.ComboBox cbExecutor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAddRelatores;
        private System.Windows.Forms.DataGridViewButtonColumn DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODUSUARIOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn lOGINDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sENHADataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn gERENTEDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aLTERARSENHADataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECCREATEDBYDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECCREATEDONDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECMODIFIEDBYDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECMODIFIEDONDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vERPLANOACAODataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vEREFICACIAACAODataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn eMAILDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNCONFORMDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cRESPCONFORMDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn gPERMISSAODataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cVPACAOEFICACIADataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cVPACAOEFICACIA1DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cRELATORESCONFORMsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn gCENTROCUSTOesDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cNCONFORMs1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cKSELECIONADODataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODCOLIGADADataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbSetor;
        private System.Windows.Forms.ComboBox cbTPRegistro;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.MaskedTextBox txtDataConformidade;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.CheckBox ckAfetaQualidadeNAO;
        private System.Windows.Forms.CheckBox ckAfetaQualidadeSIM;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox ckFatoIsoladoNAO;
        private System.Windows.Forms.CheckBox ckFatoIsoladoSIM;
    }
}