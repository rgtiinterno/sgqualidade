﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade.PlanoAcao;
using QUALIDADE.Forms.Configuracoes;
using QUALIDADE.Relatorios.Forms.NaoConformidade;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.NaoConformidade
{
    public partial class FormListaNConformidade : Form
    {
        public FormListaNConformidade()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"NConformidades.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListaNConformidade_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODNCONFORM", HeaderText = "Cód. Conformidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. C.Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCCUSTO", HeaderText = "Cód. C.Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TPACAO", HeaderText = "Tipo de Ação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODREGISTRO", HeaderText = "Cód. Tipo de Registro", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCREGISTRO", HeaderText = "Tipo de Registro", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCSETOR", HeaderText = "Setor Responsável", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "FINALIZADORELATOR", HeaderText = "Finalizada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "FINALIZADORESPONSAVEL", HeaderText = "Concluída", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "NECACAOCORPREV", HeaderText = "Neces. Ação Prev.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "VERIFICACAOPLANOACAO", HeaderText = "Plano de Ação Verificada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "VERIFICACAOPLANOEFICACIA", HeaderText = "Eficácia Verificada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAABERTURANC", HeaderText = "Data Abertura", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "STATUS", HeaderText = "Status", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCSTATUS", HeaderText = "Status", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCLASSIF", HeaderText = "Cód. Classificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCCLASSIF", HeaderText = "Classificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODEXTRATIF", HeaderText = "Cód. Extratificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCEXTRATIF", HeaderText = "Extratificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NCONFREALPONTENCIAL", HeaderText = "Não Conf. Real/Pot.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ACAOIMEDTOMADA", HeaderText = "Ação Tom. Imediata", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EVDOBJETIVA", HeaderText = "Evidência Obs.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMERALATOR", HeaderText = "Relator Principal", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODRELATOR", HeaderText = "Relator", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODEXECUTOR", HeaderText = "Executor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCEXECUTOR", HeaderText = "Executor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ANLISECAUSA1", HeaderText = "Analise de Causa 1", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ANLISECAUSA2", HeaderText = "Analise de Causa 2", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ANLISECAUSA3", HeaderText = "Analise de Causa 3", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PLANOACAOQUE", HeaderText = "Plano de Ação (Que?)", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PLANOACAOQUEM", HeaderText = "Plano de Ação (Quem?)", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "PLANOACAOQUANDO", HeaderText = "Plano de Ação (Quando?)", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EVDTRATATIVA", HeaderText = "Evd. Trataiva", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleCNConformidade controle = new ControleCNConformidade())
            {
                Cursor.Current = Cursors.WaitCursor;
                cNCONFORMBindingSource = new BindingSource();
                cNCONFORMBindingSource.DataSource = typeof(CNCONFORM);
                cNCONFORMBindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso()).ToList();
                dgvListagem.DataSource = cNCONFORMBindingSource;
                cNCONFORMBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {cNCONFORMBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                    tsbConclusao.Enabled = true;
                    tsbFinalizasao.Enabled = true;
                    tsbHistorico.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = false;
                    tsbConclusao.Enabled = false;
                    tsbFinalizasao.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            CNCONFORM cNaoConform = (dgvListagem.SelectedItem as CNCONFORM);

            if (cNaoConform != null && cNaoConform.CKSELECIONADO && !cNaoConform.FINALIZADORESPONSAVEL) // && !cNaoConform.FINALIZADORELATOR
            {
                if (cNaoConform.CODRELATOR == FormPrincipal.getUsuarioAcesso().CODUSUARIO)
                {
                    try
                    {
                        using (ControleCRNConformidade controle = new ControleCRNConformidade())
                        {
                            controle.Delete(x => x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODNCONFORM == cNaoConform.CODNCONFORM && x.CODCCUSTO == cNaoConform.CODCCUSTO);
                            controle.SaveAll();
                        }

                        using (ControleCRelatoresNConform controle = new ControleCRelatoresNConform())
                        {
                            controle.Delete(x => x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODNCONFORM == cNaoConform.CODNCONFORM && x.CODCCUSTO == cNaoConform.CODCCUSTO);
                            controle.SaveAll();
                        }

                        using (ControleGArquivoAnx controle = new ControleGArquivoAnx())
                        {
                            controle.Delete(x => x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODEXTERNO == cNaoConform.CODNCONFORM && x.CODCCUSTO == cNaoConform.CODCCUSTO &&
                                x.CODSISTEMA == "C");
                            controle.SaveAll();
                        }

                        using (ControleCCNConformHst controle = new ControleCCNConformHst())
                        {
                            controle.Delete(x => x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODNCONFORM == cNaoConform.CODNCONFORM && x.CODCCUSTO == cNaoConform.CODCCUSTO);
                            controle.SaveAll();
                        }

                        using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
                        {
                            controle.Delete(x =>
                                x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODNCONFORM == cNaoConform.CODNCONFORM &&
                                x.CODCCUSTO == cNaoConform.CODCCUSTO
                            );
                            controle.SaveAll();
                        }

                        using (ControleCNConformidade controle = new ControleCNConformidade())
                        {
                            controle.Delete(x => x.CODCOLIGADA == cNaoConform.CODCOLIGADA &&
                                x.CODEXTRATIF == cNaoConform.CODEXTRATIF && x.CODCCUSTO == cNaoConform.CODCCUSTO);
                            controle.SaveAll();
                        }
                        tsbAtualizar.PerformClick();
                    }
                    catch (Exception ex)
                    {
                        using (FormMsg frm = new FormMsg(ex))
                        {
                            frm.StartPosition = FormStartPosition.CenterParent;
                            frm.WindowState = FormWindowState.Normal;
                            frm.ShowDialog();

                            frm.Dispose();
                        }
                    }
                }
            }
            else
            {
                Global.MsgErro("Não é possível excluir registros concluídos ou de outros relatores.");
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            CNCONFORM cNaoConform = (dgvListagem.SelectedItem as CNCONFORM);

            if (cNaoConform != null && cNaoConform.CKSELECIONADO)
            {
                using (FormNConformidade frm = new FormNConformidade(cNaoConform))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormNConformidade frm = new FormNConformidade())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbRelatorio_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CNCONFORM cNaoConform && cNaoConform.CKSELECIONADO)
            {
                using (FormRelatorio frm = new FormRelatorio(cNaoConform))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione um registro para emitir o relatório.");
        }

        private void tsbHistorico_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CNCONFORM cNaoConform && cNaoConform.CKSELECIONADO)
            {
                using (FormHistNConformidade frm = new FormHistNConformidade(cNaoConform))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbConclusao_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CNCONFORM cNaoConform && cNaoConform.CKSELECIONADO)
            {
                if (cNaoConform.FINALIZADORELATOR)
                {
                    if (!cNaoConform.FINALIZADORESPONSAVEL)
                    {
                        using (ControleCNConformidade controle = new ControleCNConformidade())
                        {
                            using (ControleCCNConformHst controleHst = new ControleCCNConformHst())
                            {
                                using (ControleGCentroCusto controleCcusto = new ControleGCentroCusto())
                                {
                                    GCENTROCUSTO cCusto = controleCcusto.Find(cNaoConform.CODCOLIGADA, cNaoConform.CODCCUSTO);

                                    if (cCusto != null && cCusto.CODUSUGERENTE != null && cCusto.CODUSUGERENTE.Value == FormPrincipal.getUsuarioAcesso().CODUSUARIO)
                                    {
                                        CCNCONFORMHST responsavel = new CCNCONFORMHST
                                        {
                                            CODIGO = controleHst.GetID(),
                                            CODCCUSTO = cNaoConform.CODCCUSTO,
                                            CODNCONFORM = cNaoConform.CODNCONFORM,
                                            CODCOLIGADA = cNaoConform.CODCOLIGADA,
                                            STATUS = cNaoConform.STATUS,
                                            HISTORICO = $@"Concluído no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                            pelo usuário {FormPrincipal.getUsuarioAcesso().CODUSUARIO} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                            RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                            RECCREATEDON = DateTime.Now
                                        };

                                        controleHst.Create(responsavel);
                                        controleHst.SaveAll();
                                        controle.AtualizaStatus(cNaoConform);
                                        
                                        tsbAtualizar.PerformClick();
                                    }
                                    else
                                    {
                                        using (ControleCRNConformidade controleResponsaveis = new ControleCRNConformidade())
                                        {
                                            if (controleResponsaveis.Exists(cNaoConform.CODCCUSTO, cNaoConform.CODCOLIGADA,
                                                            cNaoConform.CODNCONFORM, FormPrincipal.getUsuarioAcesso().CODUSUARIO))
                                            {
                                                CCNCONFORMHST responsavel = new CCNCONFORMHST
                                                {
                                                    CODIGO = controleHst.GetID(),
                                                    CODCCUSTO = cNaoConform.CODCCUSTO,
                                                    CODNCONFORM = cNaoConform.CODNCONFORM,
                                                    CODCOLIGADA = cNaoConform.CODCOLIGADA,
                                                    STATUS = cNaoConform.STATUS,
                                                    HISTORICO = $@"Concluído no dia {DateTime.Now.ToString("dd-MM-yyyy")} às {DateTime.Now.ToString("HH:mm:ss")}, 
                                                    pelo usuário {FormPrincipal.getUsuarioAcesso().CODUSUARIO} - {FormPrincipal.getUsuarioAcesso().NOME}",
                                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                                    RECCREATEDON = DateTime.Now
                                                };

                                                controleHst.Create(responsavel);
                                                controleHst.SaveAll();
                                                controle.AtualizaStatus(cNaoConform);

                                                tsbAtualizar.PerformClick();
                                            }
                                            else
                                                Global.MsgSemPermissao();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                        MessageBox.Show(" - Não conformidade já concluída!",
                            "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                    MessageBox.Show(" - Não conformidade ainda não foi finalizada!",
                        "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a conclusão do mesmo.");
        }

        private void tsbFinalizasao_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CNCONFORM cNaoConform && cNaoConform.CKSELECIONADO)
            {
                if (cNaoConform.CODRELATOR == FormPrincipal.getUsuarioAcesso().CODUSUARIO && !cNaoConform.FINALIZADORELATOR)
                {
                    using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
                    {
                        CVPACAOEFICACIA acaoEficacia = controle.Find(
                            cNaoConform.CODNCONFORM, cNaoConform.CODCOLIGADA,
                            cNaoConform.CODCCUSTO);
                        if (acaoEficacia != null && !acaoEficacia.CUMPRIDO.HasValue)
                        {
                            using (FormVerificarPlanoAcao frm = new FormVerificarPlanoAcao(acaoEficacia))
                            {
                                frm.StartPosition = FormStartPosition.CenterParent;
                                frm.WindowState = FormWindowState.Normal;
                                frm.ShowDialog();

                                if (frm.salvo)
                                    tsbAtualizar.PerformClick();

                                frm.Dispose();
                            }
                        }
                        else if (acaoEficacia != null && acaoEficacia.CUMPRIDO.HasValue)
                            Global.MsgInformacao(" -  Plano de ação já verificado");
                        else
                            Global.MsgErro(" -  O sistema não encontrou nenhum plano de ação para ser verificado");
                    }
                }
                else
                    Global.MsgSemPermissao();
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a finalização do mesmo.");
        }

        private void tsbVerPlanoAcao_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is CNCONFORM item && item.CKSELECIONADO)
            {
                if (item.VERIFICACAOPLANOACAO)
                {
                    using (ControleCVPAcaoEficacia controleCVPAcaoEficacia = new ControleCVPAcaoEficacia())
                    {
                        CVPACAOEFICACIA acaoEficacia =
                            controleCVPAcaoEficacia.Find(item.CODNCONFORM, item.CODCOLIGADA, item.CODCCUSTO);

                        using (FormVerificarPlanoAcao frm = new FormVerificarPlanoAcao(acaoEficacia, true))
                        {
                            frm.StartPosition = FormStartPosition.CenterParent;
                            frm.WindowState = FormWindowState.Normal;
                            frm.ShowDialog();

                            frm.Dispose();
                        }
                    }
                }
                else
                    Global.MsgErro("Não conformidade selecionada não possui plano de ação verificado.");
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a finalização do mesmo.");
        }
    }
}
