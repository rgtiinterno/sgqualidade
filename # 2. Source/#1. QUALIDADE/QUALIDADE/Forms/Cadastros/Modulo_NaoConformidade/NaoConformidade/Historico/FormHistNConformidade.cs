﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.NaoConformidade
{
    public partial class FormHistNConformidade : Form
    {
        private CNCONFORM nConform;

        public FormHistNConformidade(CNCONFORM nConformidade)
        {
            InitializeComponent();
            nConform = nConformidade;
        }

        private void FormHistNConformidade_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODNCONFORM", HeaderText = "Cód. Conformidade", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. C.Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "HISTORICO", HeaderText = "Histórico", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "STATUS", HeaderText = "Status", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleCCNConformHst controle = new ControleCCNConformHst())
            {
                Cursor.Current = Cursors.WaitCursor;
                cCNCONFORMHSTBindingSource = new BindingSource();
                cCNCONFORMHSTBindingSource.DataSource = typeof(CCNCONFORMHST);
                cCNCONFORMHSTBindingSource.DataSource = controle.GetList(nConform.CODCOLIGADA, nConform.CODCCUSTO, nConform.CODNCONFORM);
                dgvListagem.DataSource = cCNCONFORMHSTBindingSource;
                cCNCONFORMHSTBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {cCNCONFORMHSTBindingSource.Count}";
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }
    }
}
