﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Extratificacoes
{
    public partial class FormListaExtratificacao : Form
    {
        public FormListaExtratificacao()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Extratificacoes.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListaExtratificacao_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODEXTRATIF", HeaderText = "Cód. Interno", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODSETOR", HeaderText = "Cód. Setor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESSETOR", HeaderText = "Setor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACAO", HeaderText = "Observação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleCExtratificacao controle = new ControleCExtratificacao())
            {
                Cursor.Current = Cursors.WaitCursor;
                cEXTRATIFICACAOBindingSource = new BindingSource();
                cEXTRATIFICACAOBindingSource.DataSource = typeof(CEXTRATIFICACAO);
                cEXTRATIFICACAOBindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                dgvListagem.DataSource = cEXTRATIFICACAOBindingSource;
                cEXTRATIFICACAOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {cEXTRATIFICACAOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormExtratificacao frm = new FormExtratificacao())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    tsbAtualizar.PerformClick();
                }

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            CEXTRATIFICACAO cExtratificacao = (dgvListagem.SelectedItem as CEXTRATIFICACAO);

            if (cExtratificacao.CKSELECIONADO)
            {
                using (FormExtratificacao frm = new FormExtratificacao(cExtratificacao))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        tsbAtualizar.PerformClick();
                    }

                    frm.Dispose();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            CEXTRATIFICACAO cExtratificacao = (dgvListagem.SelectedItem as CEXTRATIFICACAO);

            if (cExtratificacao.CKSELECIONADO)
            {
                try
                {
                    using (ControleCExtratificacao controle = new ControleCExtratificacao())
                    {
                        controle.Delete(x => x.CODCOLIGADA == cExtratificacao.CODCOLIGADA && x.CODEXTRATIF == cExtratificacao.CODEXTRATIF);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }
    }
}
