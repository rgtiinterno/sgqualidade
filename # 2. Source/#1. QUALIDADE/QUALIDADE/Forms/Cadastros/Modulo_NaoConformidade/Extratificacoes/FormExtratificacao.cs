﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Extratificacoes
{
    public partial class FormExtratificacao : Form
    {
        public bool salvo = false;
        private CEXTRATIFICACAO item;

        public FormExtratificacao(CEXTRATIFICACAO cExtratificacao = null)
        {
            InitializeComponent();
            this.item = cExtratificacao;
        }

        private void FormExtratificacao_Load(object sender, EventArgs e)
        {
            using (ControleGSetores controle = new ControleGSetores())
            {
                cbSetor.DataSource = controle.Get(x => 
                    x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                cbSetor.DisplayMember = "DESCRICAO";
                cbSetor.ValueMember = "CODIGO";
                cbSetor.SelectedIndex = -1;
            }

            if (item == null)
            {
                using (ControleCExtratificacao controle = new ControleCExtratificacao())
                {
                    txtCodExtratificacao.Text = $"{controle.GetID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA)}";
                    txtColigada.Text = $"{FormPrincipal.getUsuarioAcesso().CODCOLIGADA}";
                }
            }
            else
            {
                txtColigada.Text = $"{item.CODCOLIGADA}";
                txtCodExtratificacao.Text = $"{item.CODEXTRATIF}";
                txtNome.Text = $"{item.DESCRICAO}";
                if (item.CODSETOR.HasValue)
                    cbSetor.SelectedValue = item.CODSETOR;
                else
                    cbSetor.SelectedIndex = -1;
                txtObservacao.Text = $"{item.OBSERVACAO}";
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNome.Text))
            {
                try
                {
                    CEXTRATIFICACAO cExtratificacao = new CEXTRATIFICACAO
                    {
                        CODCOLIGADA = Convert.ToInt16(txtColigada.Text),
                        CODEXTRATIF = Convert.ToInt16(txtCodExtratificacao.Text),
                        DESCRICAO = txtNome.Text,
                        CODSETOR = cbSetor.SelectedValue.DBNullToInt(),
                        OBSERVACAO = string.IsNullOrEmpty(txtObservacao.Text) ? null : txtObservacao.Text
                    };

                    using (ControleCExtratificacao controle = new ControleCExtratificacao())
                    {
                        if (item == null)
                        {
                            cExtratificacao.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            cExtratificacao.RECCREATEDON = DateTime.Now;
                            controle.Create(cExtratificacao);
                            controle.SaveAll();
                            salvo = true;
                            this.Dispose();
                        }
                        else
                        {
                            cExtratificacao.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            cExtratificacao.RECMODIFIEDON = DateTime.Now;
                            controle.Update(cExtratificacao, x => x.CODCOLIGADA == item.CODCOLIGADA && x.CODEXTRATIF == item.CODEXTRATIF);
                            controle.SaveAll();
                            salvo = true;
                            this.Dispose();
                        }
                    }

                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Informe a todos os campos em negrito.");
            }
        }
    }
}
