﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Empresas
{
    public partial class FormEmpresa : Form
    {
        GEMPRESAS ColigadaAtual;
        private string arquivo = null;
        public bool salvo = false;

        public FormEmpresa(GEMPRESAS empresa = null)
        {
            InitializeComponent();
            ColigadaAtual = empresa;
        }

        public void PreencheCampos()
        {
            txtNomeFantasia.Text = ColigadaAtual.NOMEFANTASIA;
            txtCNPJ.Text = ColigadaAtual.CGC;
            txtNome.Text = ColigadaAtual.NOME;
            txtInscEstadual.Text = ColigadaAtual.INSCRICAOESTADUAL;
            txtTelefone.Text = ColigadaAtual.TELEFONE;
            txtFAX.Text = ColigadaAtual.FAX;
            txtRua.Text = ColigadaAtual.RUA;
            txtNumero.Text = ColigadaAtual.NUMERO;
            txtComplemento.Text = ColigadaAtual.COMPLEMENTO;
            txtBairro.Text = ColigadaAtual.BAIRRO;
            txtCidade.Text = ColigadaAtual.CIDADE;
            cbEstadoEndereco.SelectedValue = ColigadaAtual.ESTADO;
            cbPaisEndereco.SelectedText = ColigadaAtual.PAIS;
            txtCEP.Text = ColigadaAtual.CEP;

            if (ColigadaAtual.LOGO != null)
                picLogo.Image = ColigadaAtual.LOGO.ToImage();
            else
                picLogo.Image = null;


        }

        public void AlimentaCombos()
        {
            using (ControleGPais obj = new ControleGPais())
            {
                cbPaisEndereco.DataSource = obj.GetAll().ToList();
                cbPaisEndereco.ValueMember = "CODPAIS";
                cbPaisEndereco.DisplayMember = "DESCRICAO";
                cbPaisEndereco.SelectedIndex = -1;
            }

            using (ControleGEstados obj = new ControleGEstados())
            {
                cbEstadoEndereco.DataSource = obj.GetAll().ToList();
                cbEstadoEndereco.ValueMember = "CODETD";
                cbEstadoEndereco.DisplayMember = "NOME";
                cbEstadoEndereco.SelectedIndex = -1;
            }
        }

        private void FormEditaEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCancelar.PerformClick();
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (txtColigada.Text == "" || txtCNPJ.Text == "" || txtNome.Text == "" || txtNomeFantasia.Text == "" || txtRua.Text == "" || txtNumero.Text == "" || cbEstadoEndereco.SelectedIndex == -1 || txtBairro.Text == "" || txtCidade.Text == "" || cbPaisEndereco.SelectedIndex == -1 || txtCEP.Text == "")
            {
                MessageBox.Show("Preencha os campos obrigatórios.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    using (ControleGEmpresas controle = new ControleGEmpresas())
                    {
                        #region "Dados do formulário"
                        GEMPRESAS gEmpresa = new GEMPRESAS
                        {
                            CODCOLIGADA = Convert.ToInt16(txtColigada.Text),
                            NOMEFANTASIA = txtNomeFantasia.Text,
                            CGC = txtCNPJ.Text,
                            NOME = txtNome.Text,
                            INSCRICAOESTADUAL = txtInscEstadual.Text,
                            TELEFONE = txtTelefone.Text,
                            FAX = txtFAX.Text,
                            RUA = txtRua.Text,
                            NUMERO = txtNumero.Text,
                            COMPLEMENTO = txtComplemento.Text,
                            BAIRRO = txtBairro.Text,
                            CIDADE = txtCidade.Text,
                            ESTADO = Convert.ToString(cbEstadoEndereco.SelectedValue),
                            PAIS = Convert.ToString(cbPaisEndereco.SelectedValue),
                            CEP = txtCEP.Text,
                            LOGO = picLogo.Image.ToByteArray(),
                        };
                        #endregion

                        if (ColigadaAtual == null)
                        {
                            salvo = true;
                            gEmpresa.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            gEmpresa.RECCREATEDON = DateTime.Now;
                            controle.Create(gEmpresa);
                            controle.SaveAll();
                            this.Dispose();
                        }
                        else
                        {
                            salvo = true;
                            gEmpresa.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            gEmpresa.RECMODIFIEDON = DateTime.Now;
                            controle.Update(gEmpresa, x => x.CODCOLIGADA == ColigadaAtual.CODCOLIGADA);
                            controle.SaveAll();
                            this.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex.InnerException.Message);
                    frm.ShowDialog();
                }
            }
        }

        private void btnBuscaLogo_Click(object sender, EventArgs e)
        {
            this.arquivo = Funcoes.SelecionaArquivo();
            if (!string.IsNullOrEmpty(this.arquivo))
                picLogo.Load(this.arquivo);
        }

        private void btnRemoveLogo_Click(object sender, EventArgs e)
        {
            picLogo.Image = null;
            this.arquivo = null;
        }

        private void btnBuscaCepCol_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (!string.IsNullOrWhiteSpace(txtCEP.Text))
                {
                    Endereco endereco = BuscaCEP.Buscar(txtCEP.Text);

                    if (endereco != null)
                    {
                        cbEstadoEndereco.SelectedValue = endereco.UF;
                        txtCidade.Text = endereco.Cidade;
                        txtBairro.Text = endereco.Bairro;
                        txtRua.Text = string.Format("{0} {1}", endereco.TipoLogradouro, endereco.Logradouro);
                        txtNumero.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Cep não localizado...");
                    }
                }
                else
                {
                    MessageBox.Show("Informe um CEP válido");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void FormEmpresa_Load(object sender, EventArgs e)
        {
            AlimentaCombos();
            if (ColigadaAtual != null)
            {
                this.Text = $"Edição de Cadastro de Empresa.";
                PreencheCampos();
            }
            else
            {
                this.Text = $"Cadastro de Empresa.";
            }
        }
    }
}
