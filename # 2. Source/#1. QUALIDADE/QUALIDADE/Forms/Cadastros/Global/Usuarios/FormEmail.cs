﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    public partial class FormEmail : Form
    {
        private GUSUARIO usr;
        public FormEmail()
        {
            InitializeComponent();
            usr = FormPrincipal.getUsuarioAcesso();
        }

        private void FormEmail_Load(object sender, EventArgs e)
        {
            txtEmail.Text = usr.EMAIL;
            txtHost.Text = Criptografia.Decrypt(usr.HOST);
            txtPorta.Text = Criptografia.Decrypt(usr.PORTA);
            txtSenhaEmail.Text = Criptografia.Decrypt(usr.SENHAEMAIL);
            ckSSL.Checked = usr.SSLHABILITADO;
            txtEmail.ReadOnly = true;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if(validaCampos())
            {
                usr.SENHAEMAIL = string.IsNullOrWhiteSpace(txtSenhaEmail.Text) ? null : Criptografia.Encrypt(txtSenhaEmail.Text);
                usr.HOST = string.IsNullOrWhiteSpace(txtHost.Text) ? null : Criptografia.Encrypt(txtHost.Text);
                usr.PORTA = string.IsNullOrWhiteSpace(txtPorta.Text) ? null : Criptografia.Encrypt(txtPorta.Text);
                usr.SSLHABILITADO = ckSSL.Checked;
                usr.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                usr.RECMODIFIEDON = DateTime.Now;

                using (ControleGUsuario controller = new ControleGUsuario())
                {
                    controller.Update(usr);
                    Dominio.Global.MsgSucesso("Dados alterados com sucesso!");
                    this.Dispose();
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private bool validaCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrWhiteSpace(txtSenhaEmail.Text))
                msg += " - Informe a senha do e-mail.\n";

            if (string.IsNullOrWhiteSpace(txtHost.Text))
                msg += " - Informe o host.\n";

            if (string.IsNullOrWhiteSpace(txtPorta.Text))
                msg += " - Informe uma porta valida do host.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }
    }
}
