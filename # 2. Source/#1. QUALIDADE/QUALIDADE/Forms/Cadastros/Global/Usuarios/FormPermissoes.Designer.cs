﻿namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    partial class FormPermissoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo treeNodeAdvStyleInfo1 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPermissoes));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckProjetos = new System.Windows.Forms.CheckBox();
            this.ckPropostas = new System.Windows.Forms.CheckBox();
            this.ckFornecedoresPS = new System.Windows.Forms.CheckBox();
            this.ckNaoConformidades = new System.Windows.Forms.CheckBox();
            this.ckAferCalib = new System.Windows.Forms.CheckBox();
            this.ckRiscos = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tvPermissao = new Syncfusion.Windows.Forms.Tools.TreeViewAdv();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.ckFVS = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissao)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckProjetos);
            this.groupBox1.Controls.Add(this.ckPropostas);
            this.groupBox1.Controls.Add(this.ckFornecedoresPS);
            this.groupBox1.Controls.Add(this.ckNaoConformidades);
            this.groupBox1.Controls.Add(this.ckAferCalib);
            this.groupBox1.Controls.Add(this.ckFVS);
            this.groupBox1.Controls.Add(this.ckRiscos);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(12, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 299);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sistemas";
            // 
            // ckProjetos
            // 
            this.ckProjetos.AutoSize = true;
            this.ckProjetos.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckProjetos.Location = new System.Drawing.Point(17, 143);
            this.ckProjetos.Name = "ckProjetos";
            this.ckProjetos.Size = new System.Drawing.Size(127, 17);
            this.ckProjetos.TabIndex = 2;
            this.ckProjetos.Text = "Controle de Projetos";
            this.ckProjetos.UseVisualStyleBackColor = true;
            // 
            // ckPropostas
            // 
            this.ckPropostas.AutoSize = true;
            this.ckPropostas.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckPropostas.Location = new System.Drawing.Point(17, 108);
            this.ckPropostas.Name = "ckPropostas";
            this.ckPropostas.Size = new System.Drawing.Size(135, 17);
            this.ckPropostas.TabIndex = 2;
            this.ckPropostas.Text = "Controle de Propostas";
            this.ckPropostas.UseVisualStyleBackColor = true;
            // 
            // ckFornecedoresPS
            // 
            this.ckFornecedoresPS.AutoSize = true;
            this.ckFornecedoresPS.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckFornecedoresPS.Location = new System.Drawing.Point(17, 72);
            this.ckFornecedoresPS.Name = "ckFornecedoresPS";
            this.ckFornecedoresPS.Size = new System.Drawing.Size(275, 17);
            this.ckFornecedoresPS.TabIndex = 2;
            this.ckFornecedoresPS.Text = "Gestão de Fornecedores e Prestadores de Serviços";
            this.ckFornecedoresPS.UseVisualStyleBackColor = true;
            // 
            // ckNaoConformidades
            // 
            this.ckNaoConformidades.AutoSize = true;
            this.ckNaoConformidades.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckNaoConformidades.Location = new System.Drawing.Point(17, 35);
            this.ckNaoConformidades.Name = "ckNaoConformidades";
            this.ckNaoConformidades.Size = new System.Drawing.Size(178, 17);
            this.ckNaoConformidades.TabIndex = 1;
            this.ckNaoConformidades.Text = "Gestão de Não Conformidades";
            this.ckNaoConformidades.UseVisualStyleBackColor = true;
            // 
            // ckAferCalib
            // 
            this.ckAferCalib.AutoSize = true;
            this.ckAferCalib.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckAferCalib.Location = new System.Drawing.Point(17, 181);
            this.ckAferCalib.Name = "ckAferCalib";
            this.ckAferCalib.Size = new System.Drawing.Size(197, 17);
            this.ckAferCalib.TabIndex = 0;
            this.ckAferCalib.Text = "Gestão de Verificações e Calibrações ";
            this.ckAferCalib.UseVisualStyleBackColor = true;
            // 
            // ckRiscos
            // 
            this.ckRiscos.AutoSize = true;
            this.ckRiscos.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckRiscos.Location = new System.Drawing.Point(17, 256);
            this.ckRiscos.Name = "ckRiscos";
            this.ckRiscos.Size = new System.Drawing.Size(110, 17);
            this.ckRiscos.TabIndex = 0;
            this.ckRiscos.Text = "Gestão de Riscos";
            this.ckRiscos.UseVisualStyleBackColor = true;
            this.ckRiscos.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tvPermissao);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(327, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(434, 428);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Centro de Custo";
            // 
            // tvPermissao
            // 
            treeNodeAdvStyleInfo1.CheckBoxTickThickness = 1;
            treeNodeAdvStyleInfo1.CheckColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.EnsureDefaultOptionedChild = true;
            treeNodeAdvStyleInfo1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            treeNodeAdvStyleInfo1.IntermediateCheckColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.OptionButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.SelectedOptionButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            treeNodeAdvStyleInfo1.ShowCheckBox = true;
            treeNodeAdvStyleInfo1.ShowOptionButton = false;
            treeNodeAdvStyleInfo1.ShowPlusMinus = true;
            this.tvPermissao.BaseStylePairs.AddRange(new Syncfusion.Windows.Forms.Tools.StyleNamePair[] {
            new Syncfusion.Windows.Forms.Tools.StyleNamePair("Standard", treeNodeAdvStyleInfo1)});
            this.tvPermissao.BeforeTouchSize = new System.Drawing.Size(428, 407);
            this.tvPermissao.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.tvPermissao.HelpTextControl.BaseThemeName = null;
            this.tvPermissao.HelpTextControl.Location = new System.Drawing.Point(0, 0);
            this.tvPermissao.HelpTextControl.Name = "";
            this.tvPermissao.HelpTextControl.Size = new System.Drawing.Size(392, 112);
            this.tvPermissao.HelpTextControl.TabIndex = 0;
            this.tvPermissao.HelpTextControl.Visible = true;
            this.tvPermissao.InactiveSelectedNodeForeColor = System.Drawing.SystemColors.ControlText;
            this.tvPermissao.Location = new System.Drawing.Point(3, 18);
            this.tvPermissao.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.tvPermissao.Name = "tvPermissao";
            this.tvPermissao.SelectedNodeForeColor = System.Drawing.SystemColors.HighlightText;
            this.tvPermissao.ShowCheckBoxes = true;
            this.tvPermissao.ShowLines = false;
            this.tvPermissao.Size = new System.Drawing.Size(428, 407);
            this.tvPermissao.TabIndex = 0;
            this.tvPermissao.Text = "treeViewAdv1";
            this.tvPermissao.ThemeStyle.TreeNodeAdvStyle.CheckBoxTickThickness = 0;
            this.tvPermissao.ThemeStyle.TreeNodeAdvStyle.EnsureDefaultOptionedChild = true;
            // 
            // 
            // 
            this.tvPermissao.ToolTipControl.BaseThemeName = null;
            this.tvPermissao.ToolTipControl.Location = new System.Drawing.Point(0, 0);
            this.tvPermissao.ToolTipControl.Name = "";
            this.tvPermissao.ToolTipControl.Size = new System.Drawing.Size(392, 112);
            this.tvPermissao.ToolTipControl.TabIndex = 0;
            this.tvPermissao.ToolTipControl.Visible = true;
            this.tvPermissao.AfterCheck += new Syncfusion.Windows.Forms.Tools.TreeNodeAdvEventHandler(this.tvPermissao_AfterCheck);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(655, 453);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 31;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalvar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(543, 453);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(106, 27);
            this.btnSalvar.TabIndex = 30;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // ckFVS
            // 
            this.ckFVS.AutoSize = true;
            this.ckFVS.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckFVS.Location = new System.Drawing.Point(17, 220);
            this.ckFVS.Name = "ckFVS";
            this.ckFVS.Size = new System.Drawing.Size(242, 17);
            this.ckFVS.TabIndex = 0;
            this.ckFVS.Text = "Gestão de Fichas de Verificação de Serviços";
            this.ckFVS.UseVisualStyleBackColor = true;
            // 
            // FormPermissoes
            // 
            this.AcceptButton = this.btnSalvar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 492);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPermissoes";
            this.Text = "Permissões de usuário";
            this.Load += new System.EventHandler(this.FormPermissoes_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvPermissao)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckFornecedoresPS;
        private System.Windows.Forms.CheckBox ckNaoConformidades;
        private System.Windows.Forms.CheckBox ckRiscos;
        private System.Windows.Forms.GroupBox groupBox2;
        private Syncfusion.Windows.Forms.Tools.TreeViewAdv tvPermissao;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.CheckBox ckPropostas;
        private System.Windows.Forms.CheckBox ckProjetos;
        private System.Windows.Forms.CheckBox ckAferCalib;
        private System.Windows.Forms.CheckBox ckFVS;
    }
}