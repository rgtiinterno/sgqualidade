﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    public partial class FormAlterarSenha : Form
    {
        public bool salvo = false;

        public FormAlterarSenha()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validacao())
                {
                    using (ControleGUsuario controle = new ControleGUsuario())
                    {
                        if (controle.AlteraSenha(FormPrincipal.getUsuarioAcesso().CODUSUARIO, txtSenha.Text, txtNovaSenha.Text))
                        {
                            this.salvo = true;
                            this.Close();
                        }
                        else
                        {
                            FormMsg frm = new FormMsg("Erro ao alterar a senha.\nTente novamente mais tarde.");
                            frm.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex.InnerException.Message);
                frm.ShowDialog();
            }
        }

        private bool Validacao()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtNovaSenha.Text))
            {
                msg += " - Para continuar é necessário inserir todas as informações.\n";
            }

            if (Criptografia.Decrypt(FormPrincipal.getUsuarioAcesso().SENHA) != txtSenha.Text)
            {
                msg += " - A senha atual não corresponde a senha do usuário logado.\n";
            }

            if (txtSenha.Text == txtNovaSenha.Text)
            {
                msg += " - Senhas são correspondentes.\n";
            }

            if (string.IsNullOrEmpty(msg))
            {
                return true;
            }
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }
    }
}
