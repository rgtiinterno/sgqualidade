﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Windows.Forms.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    public partial class FormPermissoes : Form
    {
        private GUSUARIO usuario;
        private List<GPERMISSAO> permissoes = null;

        public FormPermissoes(GUSUARIO usr)
        {
            InitializeComponent();
            this.usuario = usr;
            this.Text = $"Permissões do usuário - < {usr.CODUSUARIO} - {usr.NOME} >";
        }

        private void FormPermissoes_Load(object sender, EventArgs e)
        {
            CarregaPermissoes();
            CarregaListagemCCusto();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CarregaPermissoes()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                this.permissoes = controle.Get(x => 
                    x.CODUSUARIO == usuario.CODUSUARIO
                ).ToList();

                if (this.permissoes != null)
                {
                    ckRiscos.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "R").Count() > 0);
                    ckPropostas.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "P").Count() > 0);
                    ckNaoConformidades.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "C").Count() > 0);
                    ckFornecedoresPS.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "F").Count() > 0);
                    ckProjetos.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "J").Count() > 0);
                    ckAferCalib.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "A").Count() > 0);
                    ckFVS.Checked = (this.permissoes.Where(x => x.CODSISTEMA == "V").Count() > 0);

                    //foreach (TreeNodeAdv no in tvPermissao.Nodes)
                    //{
                    //    foreach (TreeNodeAdv node in no.Nodes)
                    //    {
                    //        string[] centroCusto = node.Text.Trim().Split('-');

                    //        foreach (TreeNodeAdv nodeNo in node.Nodes)
                    //        {
                    //            centroCusto = nodeNo.Text.Trim().Split('-');

                    //            if (permissao.Where(x => x.CODCCUSTO == centroCusto[0].DBNullToString()).Count() > 0)
                    //                nodeNo.Checked = true;
                    //            else
                    //                nodeNo.Checked = false;
                    //        }

                    //        centroCusto = node.Text.Trim().Split('-');

                    //        if (permissao.Where(x => x.CODCCUSTO == centroCusto[0].DBNullToString()).Count() > 0)
                    //            node.Checked = true;
                    //        else
                    //            node.Checked = false;
                    //    }
                    //}
                }
            }
        }

        private void CarregaListagemCCusto()
        {
            using (ControleGEmpresas controle = new ControleGEmpresas())
            {
                using (ControleGCentroCusto controleCCusto = new ControleGCentroCusto())
                {
                    List<GEMPRESAS> listGEmpresas = controle.Get(x => x.CODCOLIGADA != 0).ToList();
                    int cont = 0;

                    foreach (GEMPRESAS empresa in listGEmpresas)
                    {
                        List<GCENTROCUSTO> listGCentroCusto = controleCCusto.Get(
                            x => x.CODCOLIGADA == empresa.CODCOLIGADA).ToList();

                        tvPermissao.Nodes.Add(
                            new TreeNodeAdv
                            {
                                Text = $"{empresa.CODCOLIGADA} - {empresa.NOME}",
                                TextColor = Color.Maroon,
                                Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold),
                                Name = $"{empresa.CODCOLIGADA}",
                                Checked = TemPermissao(empresa.CODCOLIGADA)
                            }
                        );

                        int contNv1 = 0;
                        foreach (GCENTROCUSTO cCusto in listGCentroCusto.Where(x => x.CODCCUSTO.Length == 1))
                        {
                            tvPermissao.Nodes[cont].Nodes.Add(new TreeNodeAdv
                            {
                                Text = $"{cCusto.CODCCUSTO} - {cCusto.NOME}",
                                Name = $"{empresa.CODCOLIGADA}",
                                Checked = TemPermissao(empresa.CODCOLIGADA, cCusto.CODCCUSTO)
                            });

                            foreach (GCENTROCUSTO cSubCusto in listGCentroCusto.Where(x => x.CODCCUSTO.Length == 6 && x.CODCCUSTO.Substring(0, 1) == cCusto.CODCCUSTO))
                            {
                                tvPermissao.Nodes[cont].Nodes[contNv1].Nodes.Add(
                                    new TreeNodeAdv
                                    {
                                        Text = $"{cSubCusto.CODCCUSTO} - {cSubCusto.NOME}",
                                        Name = $"{empresa.CODCOLIGADA}",
                                        Checked = TemPermissao(empresa.CODCOLIGADA, cSubCusto.CODCCUSTO)
                                    }
                                );
                            }
                            contNv1++;
                        }
                        cont++;
                    }
                }
            }
        }

        private bool TemPermissao(int codColigada, string codCCusto = null)
        {
            bool temPermissao = false;
            if (!string.IsNullOrEmpty(codCCusto))
            {
                temPermissao = this.permissoes?.Where(x =>
                    x.CODCOLIGADA == codColigada &&
                    x.CODCCUSTO == codCCusto
                ).Count() > 0;
            }
            else
            {
                temPermissao = this.permissoes?.Where(x =>
                    x.CODCOLIGADA == codColigada 
                ).Count() > 0;
            }
            return temPermissao;
        }

        private void tvPermissao_AfterCheck(object sender, TreeNodeAdvEventArgs e)
        {
            foreach (TreeNodeAdv n in e.Node.Nodes)
            {
                n.Checked = e.Node.Checked;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ckRiscos.Checked || ckPropostas.Checked || ckNaoConformidades.Checked || ckFornecedoresPS.Checked || ckProjetos.Checked || ckAferCalib.Checked || ckFVS.Checked)
            {
                Cursor.Current = Cursors.WaitCursor;
                List<GPERMISSAO> novasPermissoes = ListaPermissoes();
                try
                {
                    using (ControleGPermissao controle = new ControleGPermissao())
                    {
                        controle.ExecuteSQL(
                            "DELETE FROM GPERMISSAO WHERE CODUSUARIO=@CODUSUARIO",
                            new List<SqlParameter> { new SqlParameter("@CODUSUARIO", usuario.CODUSUARIO) }
                        );
                        controle.CreateAll(novasPermissoes);
                        controle.SaveAll();
                    }

                    Cursor.Current = Cursors.Default;
                    Global.MsgSucesso("Permissões atribuidas com sucesso");
                    this.Dispose();
                }
                catch (Exception ex)
                {
                    Cursor.Current = Cursors.Default;
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Informe ao menos um sistema para que seja dada a permissão");
            }
        }

        private List<GPERMISSAO> ListaPermissoes()
        {
            List<GPERMISSAO> permissoes = new List<GPERMISSAO>();
            DateTime dataAtual = DateTime.Now;
            string loginUsuario = FormPrincipal.getUsuarioAcesso().LOGIN;

            foreach (TreeNodeAdv node in tvPermissao.CheckedNodes)
            {
                if (node.TextColor != Color.Maroon)
                {
                    string[] centroCusto = node.Text.Trim().Split('-');

                    if (centroCusto != null && centroCusto.Length > 1)
                    {
                        short codCol = Convert.ToInt16(node.Name);
                        string codCCusto = centroCusto[0]?.Trim().DBNullToString();

                        if (!string.IsNullOrWhiteSpace(codCCusto))
                        {
                            var itens = permissoes.Where(x => x.CODCOLIGADA == codCol && x.CODCCUSTO == codCCusto);

                            if (ckRiscos.Checked && itens?.Where(x => x.CODSISTEMA == "R").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "R",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };

                                permissoes.Add(permissao);
                            }

                            if (ckProjetos.Checked && itens?.Where(x => x.CODSISTEMA == "J").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "J",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };

                                permissoes.Add(permissao);
                            }

                            if (ckPropostas.Checked && itens?.Where(x => x.CODSISTEMA == "P").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "P",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };

                                permissoes.Add(permissao);
                            }

                            if (ckNaoConformidades.Checked && itens?.Where(x => x.CODSISTEMA == "C").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "C",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };
                                permissoes.Add(permissao);
                            }

                            if (ckFornecedoresPS.Checked && itens?.Where(x => x.CODSISTEMA == "F").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "F",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };
                                permissoes.Add(permissao);
                            }

                            if (ckAferCalib.Checked && itens?.Where(x => x.CODSISTEMA == "A").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "A",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };
                                permissoes.Add(permissao);
                            }

                            if (ckFVS.Checked && itens?.Where(x => x.CODSISTEMA == "V").Count() <= 0)
                            {
                                GPERMISSAO permissao = new GPERMISSAO
                                {
                                    CODCCUSTO = codCCusto,
                                    CODCOLIGADA = codCol,
                                    CODSISTEMA = "V",
                                    CODUSUARIO = usuario.CODUSUARIO,
                                    RECCREATEDBY = loginUsuario,
                                    RECCREATEDON = dataAtual
                                };
                                permissoes.Add(permissao);
                            }
                        }
                    }
                }
            }
            return permissoes;
        }
    }
}