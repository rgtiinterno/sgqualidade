﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    public partial class FormListaUsuarios : Form
    {
        public FormListaUsuarios()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Usuarios.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListaUsuarios_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUARIO", HeaderText = "Cód. Usuário", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOME", HeaderText = "Nome", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOGIN", HeaderText = "Login", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SENHA", HeaderText = "Senha", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "GERENTE", HeaderText = "Administrador", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "DIRETORIA", HeaderText = "Diretor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "VERPLANOACAO", HeaderText = "Verifica Plano Açãoa", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "VEREFICACIAACAO", HeaderText = "Verifica Eficácia", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "ALTERARSENHA", HeaderText = "Alterar Senha", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EMAIL", HeaderText = "E-mail", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "HOST", HeaderText = "Host", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PORTA", HeaderText = "Porta SMTP", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SENHAEMAIL", HeaderText = "Senha do E-mail", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "SSLHABILITADO", HeaderText = "SSL", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleGUsuario controle = new ControleGUsuario())
            {
                Cursor.Current = Cursors.WaitCursor;
                gUSUARIOBindingSource = new BindingSource();
                gUSUARIOBindingSource.DataSource = typeof(GUSUARIO);
                gUSUARIOBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = gUSUARIOBindingSource;
                gUSUARIOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {gUSUARIOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbPermissoes.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbPermissoes.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }

                if (FormPrincipal.getUsuarioAcesso().GERENTE)
                    tsbPermissoes.Visible = true;
                else
                    tsbPermissoes.Visible = false;
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbPermissoes_Click(object sender, EventArgs e)
        {
            GUSUARIO usr = (dgvListagem.SelectedItem as GUSUARIO);

            if (usr != null && usr.CKSELECIONADO && FormPrincipal.getUsuarioAcesso().GERENTE)
            {
                using (FormPermissoes frm = new FormPermissoes(usr))
                {
                    frm.WindowState = FormWindowState.Normal;
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            GUSUARIO usr = (dgvListagem.SelectedItem as GUSUARIO);

            if (usr != null && usr.CKSELECIONADO && FormPrincipal.getUsuarioAcesso().GERENTE)
            {
                using (ControleGUsuario controller = new ControleGUsuario())
                {
                    controller.Delete(x => x.CODUSUARIO == usr.CODUSUARIO);
                    controller.SaveAll();
                    Global.MsgSucesso("Usuário deletado com sucesso.");
                }

                tsbAtualizar.PerformClick();
            }
            else if (usr == null || !usr.CKSELECIONADO)
                Global.MsgErro("Selecione ao um registro para efetuar a exclusão.");
            else
                Global.MsgSemPermissao();
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            GUSUARIO usr = (dgvListagem.SelectedItem as GUSUARIO);

            if (usr != null && usr.CKSELECIONADO && FormPrincipal.getUsuarioAcesso().GERENTE)
            {
                FormUsuarios frm = new FormUsuarios(usr);
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();

                tsbAtualizar.PerformClick();
                frm.Dispose();
            }
            else if (usr == null || !usr.CKSELECIONADO)
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            else
                Global.MsgSemPermissao();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            FormUsuarios frm = new FormUsuarios();
            frm.WindowState = FormWindowState.Normal;
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog();

            tsbAtualizar.PerformClick();
            frm.Dispose();
        }
    }
}
