﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Usuarios
{
    public partial class FormUsuarios : Form
    {
        private GUSUARIO usr;

        public FormUsuarios(GUSUARIO usuario = null)
        {
            InitializeComponent();

            if (usuario == null)
            {
                this.Text = "Cadastro de Usuário";
                using (ControleGUsuario controller = new ControleGUsuario())
                {
                    txtCodigo.Text = Convert.ToString(controller.GetNewID());
                }
            }
            else
            {
                this.usr = usuario;
                this.Text = $"Edição de cadastro - {this.usr.NOME}";
                CarregaDados();
            }
        }

        private void CarregaDados()
        {
            txtCodigo.Text = Convert.ToString(this.usr.CODUSUARIO);
            txtNome.Text = this.usr.NOME;
            txtLogin.Text = this.usr.LOGIN;
            txtSenha.Text = Criptografia.Decrypt(this.usr.SENHA);
            txtConfirmarSenha.Text = Criptografia.Decrypt(this.usr.SENHA);
            ckAdministrador.Checked = this.usr.GERENTE;
            ckAlterarSenha.Checked = this.usr.ALTERARSENHA;
            ckDiretor.Checked = this.usr.DIRETORIA;
            ckPermissaoEficacia.Checked = this.usr.VEREFICACIAACAO;
            ckPermissaoPlanoAcao.Checked = this.usr.VERPLANOACAO;
            ckDiretor.Checked = this.usr.DIRETORIA;
            txtEmail.Text = this.usr.EMAIL;
        }

        private void LimparCampos()
        {
            using (ControleGUsuario controller = new ControleGUsuario())
            {
                txtCodigo.Text = Convert.ToString(controller.GetNewID());
            }

            txtNome.Text = string.Empty;
            txtLogin.Text = string.Empty;
            txtSenha.Text = string.Empty;
            txtConfirmarSenha.Text = string.Empty;
            txtEmail.Text = string.Empty;
            ckAdministrador.Checked = false;
            ckAlterarSenha.Checked = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                using (ControleGUsuario controller = new ControleGUsuario())
                {
                    GUSUARIO usuario = new GUSUARIO
                    {
                        CODUSUARIO = Convert.ToInt16(txtCodigo.Text),
                        NOME = txtNome.Text,
                        LOGIN = txtLogin.Text,
                        SENHA = Criptografia.Encrypt(txtSenha.Text),
                        GERENTE = ckAdministrador.Checked,
                        DIRETORIA = ckDiretor.Checked,
                        ALTERARSENHA = ckAlterarSenha.Checked,
                        VEREFICACIAACAO = ckPermissaoEficacia.Checked,
                        VERPLANOACAO = ckPermissaoPlanoAcao.Checked,
                        EMAIL = txtEmail.Text,
                    };

                    if (this.usr != null)
                    {
                        usuario.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                        usuario.RECMODIFIEDON = DateTime.Now;

                        controller.Update(usuario, x => x.CODUSUARIO == this.usr.CODUSUARIO);
                        controller.SaveAll();
                        Dominio.Global.MsgSucesso("Dados alterados com sucesso!");
                        this.Dispose();
                    }
                    else
                    {
                        usuario.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                        usuario.RECCREATEDON = DateTime.Now;

                        controller.Create(usuario);
                        controller.SaveAll();

                        DialogResult dr = Dominio.Global.MsgConfirmacao("Cadastro de Usuário",
                            "Usuário cadastrado com sucesso!\nDeseja Cadastrar um novo usuário?");

                        if (dr == DialogResult.Yes)
                            LimparCampos();
                        else
                            this.Dispose();
                    }
                }
            }
        }

        private bool ValidarCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtNome.Text))
                msg += " - Informe o nome do usuário.\n";

            if (string.IsNullOrEmpty(txtLogin.Text))
                msg += " - Informe o Login.\n";

            if (string.IsNullOrEmpty(txtSenha.Text))
                msg += " - Informe uma senha.\n";

            if (string.IsNullOrEmpty(txtConfirmarSenha.Text))
                msg += " - Confirme sua senha.\n";

            if (txtSenha.Text != txtConfirmarSenha.Text)
                msg += " - As senhas não correspondem.";

            if (string.IsNullOrEmpty(txtEmail.Text) && !Dominio.Global.ValidaEmail(txtEmail.Text))
                msg += " - Informe um e-mail valído.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Dominio.Global.MsgErro(msg);
                return false;
            }
        }
    }
}
