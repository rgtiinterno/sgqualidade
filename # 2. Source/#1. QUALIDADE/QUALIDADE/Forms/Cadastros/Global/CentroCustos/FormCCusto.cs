﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    public partial class FormCCusto : Form
    {
        public bool salvo = false;
        private GCENTROCUSTO item;
        private List<GCCUSTOTAC> ListagemTAC;

        public FormCCusto(GCENTROCUSTO centroCusto = null)
        {
            InitializeComponent();
            this.item = centroCusto;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidacaoCampos())
            {
                try
                {
                    using (ControleGCentroCusto controle = new ControleGCentroCusto())
                    {
                        GCENTROCUSTO cCusto = new GCENTROCUSTO
                        {
                            NOME = txtNome.Text,
                            ATIVO = true,
                            CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                            CODCCUSTO = txtCodCCusto.Text,
                            CODREDUZIDO = txtCodCCusto.Text,
                            CODUSUGERENTE = (cbResponsavel.SelectedIndex == -1) ? null : cbResponsavel.SelectedValue.DBNullToSmallint(),
                            ENVIASPED = "F", 
                            DATAINICIOOBRA = DateTime.TryParse(txtDtInicioObra.Text, out DateTime dtInicio) ? dtInicio : (DateTime?)null,
                            DATAFIMOBRA = DateTime.TryParse(txtDtFimObra.Text, out DateTime dtFim) ? dtFim : (DateTime?)null,
                            NUMCONTRATO = string.IsNullOrEmpty(txtNumContrato.Text) ? null : txtNumContrato.Text
                        };

                        if (this.item == null)
                        {
                            cCusto.ID = controle.GetID(cCusto.CODCOLIGADA);
                            cCusto.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            cCusto.RECCREATEDON = DateTime.Now;

                            controle.Create(cCusto);
                            controle.SaveAll();

                            using (ControleTAC controleTAC = new ControleTAC())
                            {
                                controleTAC.CreateAll(ListagemTAC);
                                controleTAC.SaveAll();
                            }

                            salvo = true;
                            this.Dispose();
                        }
                        else
                        {
                            cCusto.ID = item.ID;
                            cCusto.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            cCusto.RECMODIFIEDON = DateTime.Now;
                            controle.Update(cCusto, x => x.CODCOLIGADA == cCusto.CODCOLIGADA && x.CODCCUSTO == cCusto.CODCCUSTO);
                            controle.SaveAll();


                            using (ControleTAC controleTAC = new ControleTAC())
                            {
                                controleTAC.Delete(x => x.CODCOLIGADA == item.CODCOLIGADA && x.CODCCUSTO == item.CODCCUSTO);
                                controleTAC.SaveAll();
                                controleTAC.Dispose();
                            }

                            using (ControleTAC controleTAC = new ControleTAC())
                            {
                                controleTAC.CreateAll(ListagemTAC);
                                controleTAC.SaveAll();
                            }

                            salvo = true;
                            this.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    salvo = false;
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
        }

        private bool ValidacaoCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtNome.Text))
                msg += " - Informe o nome do centro de custo.\n";

            if (string.IsNullOrEmpty(txtCodCCusto.Text))
                msg += " - Informe o código do centro de custo.";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void FormCCusto_Load(object sender, EventArgs e)
        {
            int codColigada = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
            txtColigada.Text = $"{codColigada}";

            if (item != null)
            {
                codColigada = item.CODCOLIGADA;
                txtCodCCusto.Text = item.CODCCUSTO;
                txtNome.Text = item.NOME;
                txtNumContrato.Text = item.NUMCONTRATO;
                txtDtInicioObra.Text = item.DATAINICIOOBRA.HasValue? $"{item.DATAINICIOOBRA.Value}" : null;
                txtDtFimObra.Text = item.DATAFIMOBRA.HasValue? $"{item.DATAFIMOBRA.Value}" : null;

                if (item.CODUSUGERENTE.HasValue)
                    cbResponsavel.SelectedValue = item.CODUSUGERENTE.Value;
                else
                    cbResponsavel.SelectedIndex = -1;

                using (ControleTAC controle = new ControleTAC())
                {
                    ListagemTAC = controle.GetAll().Where(x => x.CODCCUSTO == item.CODCCUSTO
                     && x.CODCOLIGADA == item.CODCOLIGADA).ToList<GCCUSTOTAC>();
                    controle.Dispose();
                }
            }

            CarregaDVG();
            DesabilitaColunas();
        }

        private void txtCodCCusto_TextChanged(object sender, EventArgs e)
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                if (!string.IsNullOrEmpty(txtCodCCusto.Text))
                {
                    cbResponsavel.DataSource = controle.GetUsuarios(Convert.ToInt16(txtColigada.Text), txtCodCCusto.Text);
                    cbResponsavel.ValueMember = "CODUSUARIO";
                    cbResponsavel.DisplayMember = "NOME";
                    cbResponsavel.SelectedIndex = -1;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (FormTAC frm = new FormTAC())
            {
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    short listaCount = 1;
                    if (ListagemTAC != null || ListagemTAC.Count > 0)
                        listaCount = Convert.ToInt16(ListagemTAC.Count);

                    frm.custo.CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
                    frm.custo.CODCCUSTO = txtCodCCusto.Text;
                    ListagemTAC.Add(frm.custo);
                    CarregaDVG();
                }
            }
        }

        private void DesabilitaColunas()
        {
            foreach (DataGridViewColumn item in dgvSetoresDistribuicao.Columns)
            {
                if (item.Name != "DELETE")
                    dgvSetoresDistribuicao.Columns[item.Name].ReadOnly = true;
            }
        }

        private void dgvSetoresDistribuicao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSetoresDistribuicao.CurrentRow is DataGridViewRow row)
            {
                GCCUSTOTAC gCCUSTOTAC = new GCCUSTOTAC();
                foreach (GCCUSTOTAC item in ListagemTAC)
                {
                    if (item.CODINTERNO == Convert.ToInt16(row.Cells["cODINTERNODataGridViewTextBoxColumn"].Value) &&
                        item.NOME == row.Cells["nOMEDataGridViewTextBoxColumn"].Value.DBNullToString())
                        gCCUSTOTAC = item;
                }

                ListagemTAC.Remove(gCCUSTOTAC);
                CarregaDVG();
            }
        }

        private void CarregaDVG()
        {
            Cursor.Current = Cursors.WaitCursor;
            gCCUSTOTACBindingSource = new BindingSource();
            gCCUSTOTACBindingSource.DataSource = typeof(GCCUSTOTAC);
            gCCUSTOTACBindingSource.DataSource = ListagemTAC;
            dgvSetoresDistribuicao.DataSource = gCCUSTOTACBindingSource;
            gCCUSTOTACBindingSource.EndEdit();
            dgvSetoresDistribuicao.Refresh();
            Cursor.Current = Cursors.Default;
        }
    }
}
