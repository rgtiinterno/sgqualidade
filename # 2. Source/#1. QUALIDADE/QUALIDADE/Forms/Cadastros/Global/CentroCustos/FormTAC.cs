﻿using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    public partial class FormTAC : Form
    {
        public bool salvo = false;
        public GCCUSTOTAC custo;

        public FormTAC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidacaoCampos())
            {
                custo = new GCCUSTOTAC
                {
                    NOME = txtNome.Text, 
                    DTINICIO = Convert.ToDateTime(txtDataInicio.Text),
                    DTFIM = Convert.ToDateTime(txtDataFim.Text)
                };
                salvo = true;
                this.Close();
            }
        }

        private bool ValidacaoCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtNome.Text))
                msg += " - Informe o nome da TAC.\n";

            if (string.IsNullOrEmpty(txtDataInicio.Text))
                msg += " - Informe uma data inicio.\n";

            if (string.IsNullOrEmpty(txtDataFim.Text))
                msg += " - Informe uma data fim.\n";

            if(!DateTime.TryParse(txtDataInicio.Text, out DateTime dtInicio))
                msg += " - Informe uma data inicio valída.\n";

            if(!DateTime.TryParse(txtDataFim.Text, out DateTime dtFim))
                msg += " - Informe uma data fim valída.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Dominio.Global.MsgInformacao(msg);
                return false;
            }
        }
    }
}
