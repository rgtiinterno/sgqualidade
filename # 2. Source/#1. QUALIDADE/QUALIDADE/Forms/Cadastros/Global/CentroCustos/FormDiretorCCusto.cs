﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    public partial class FormDiretorCCusto : Form
    {
        public bool salvo = false;
        private string codCentroCusto;

        public FormDiretorCCusto(string centroC)
        {
            InitializeComponent();
            codCentroCusto = centroC;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (cbDiretores.SelectedIndex >= 0)
            {
                using (ControleGDiretorCC controle = new ControleGDiretorCC())
                {
                    GDIRETORCCUSTO diretor = new GDIRETORCCUSTO
                    {
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODCCUSTO = codCentroCusto, 
                        CODDIRETOR = Convert.ToInt16(cbDiretores.SelectedValue), 
                        NOMEDIRETOR = cbDiretores.Text
                    };

                    controle.Create(diretor);
                    controle.SaveAll();
                    salvo = true;
                    this.Close();
                }
            }
            else
                Global.MsgErro(" - Informe o nome do diretor");
        }

        private void FormDiretorCCusto_Load(object sender, EventArgs e)
        {
            using (ControleGUsuario controle = new ControleGUsuario())
            {
                cbDiretores.DataSource = controle.Get(x => 
                    x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA && x.DIRETORIA == true).
                    OrderBy(x => x.NOME).ToList();
                cbDiretores.ValueMember = "CODUSUARIO";
                cbDiretores.DisplayMember = "NOME";
                cbDiretores.SelectedIndex = -1;
            }
        }
    }
}
