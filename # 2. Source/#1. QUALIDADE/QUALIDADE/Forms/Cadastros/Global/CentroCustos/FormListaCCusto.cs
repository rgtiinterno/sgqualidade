﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    public partial class FormListaCCusto : Form
    {
        public FormListaCCusto()
        {
            InitializeComponent();
        }

        private void FormListaCCusto_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. Centro Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODREDUZIDO", HeaderText = "Cód. Reduzido", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOME", HeaderText = "Nome", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMCONTRATO", HeaderText = "Nº Contrato", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAINICIOOBRA", HeaderText = "Dt. Inicio Obra", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAFIMOBRA", HeaderText = "Dt. Fim Obra", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODUSUGERENTE", HeaderText = "Cód. Gerente", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEGERENTE", HeaderText = "Gerente de Contrato", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "ATIVO", HeaderText = "Ativo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }
        private void CarregaDGV()
        {
            using (ControleGCentroCusto controle = new ControleGCentroCusto())
            {
                Cursor.Current = Cursors.WaitCursor;
                gCENTROCUSTOBindingSource = new BindingSource();
                gCENTROCUSTOBindingSource.DataSource = typeof(GCENTROCUSTO);
                gCENTROCUSTOBindingSource.DataSource = controle.Get(x => x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                dgvListagem.DataSource = gCENTROCUSTOBindingSource;
                gCENTROCUSTOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {gCENTROCUSTOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbAtivarDesativar.Enabled = true;
                    tsbIncluirDiretores.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbAtivarDesativar.Enabled = false;
                    tsbIncluirDiretores.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"CCusto.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAtivarDesativar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if ((dgvListagem.SelectedItem is GCENTROCUSTO cCusto) && cCusto.CKSELECIONADO)
            {
                try
                {
                    cCusto.ATIVO = !(cCusto.ATIVO);
                    cCusto.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                    cCusto.RECMODIFIEDON = DateTime.Now;

                    using (ControleGCentroCusto controle = new ControleGCentroCusto())
                    {
                        controle.Update(cCusto, x => x.CODCOLIGADA == cCusto.CODCOLIGADA && x.CODCCUSTO == cCusto.CODCCUSTO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }

            Cursor.Current = Cursors.Default;
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            GCENTROCUSTO cCusto = (dgvListagem.SelectedItem as GCENTROCUSTO);
            if (cCusto.CKSELECIONADO)
            {
                try
                {
                    using (ControleGCentroCusto controle = new ControleGCentroCusto())
                    {
                        controle.Delete(x => x.CODCOLIGADA == cCusto.CODCOLIGADA && x.CODCCUSTO == cCusto.CODCCUSTO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a exclusão do mesmo.");

            Cursor.Current = Cursors.Default;
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            GCENTROCUSTO cCusto = (dgvListagem.SelectedItem as GCENTROCUSTO);

            if (cCusto.CKSELECIONADO)
            {
                using (FormCCusto frm = new FormCCusto(cCusto))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormCCusto frm = new FormCCusto())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbIncluirDiretores_Click(object sender, EventArgs e)
        {
            GCENTROCUSTO cCusto = (dgvListagem.SelectedItem as GCENTROCUSTO);

            if (cCusto.CKSELECIONADO)
            {
                using (FormCCustoDiretor frm = new FormCCustoDiretor(cCusto))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a ação do mesmo.");
        }
    }
}
