﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.CentroCustos;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    public partial class FormCCustoDiretor : Form
    {
        private GCENTROCUSTO cCustos;

        public FormCCustoDiretor(GCENTROCUSTO centrocusto )
        {
            InitializeComponent();
            cCustos = centrocusto;
        }

        private void FormCCustoDiretor_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. Centro Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODDIRETOR", HeaderText = "Cód. Diretor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEDIRETOR", HeaderText = "Nome do Diretor", AllowEditing = false, AllowFiltering = true, Visible = true });

            CarregaDGV();

            this.Text = $"Diretores do Centro de Custo: {cCustos.CODCCUSTO} - {cCustos.NOME}";
        }
        private void CarregaDGV()
        {
            using (ControleGDiretorCC controle = new ControleGDiretorCC())
            {
                Cursor.Current = Cursors.WaitCursor;
                gDIRETORCCUSTOBindingSource = new BindingSource();
                gDIRETORCCUSTOBindingSource.DataSource = typeof(GDIRETORCCUSTO);
                gDIRETORCCUSTOBindingSource.DataSource = controle.GetList(cCustos.CODCOLIGADA, cCustos.CODCCUSTO);
                dgvListagem.DataSource = gDIRETORCCUSTOBindingSource;
                gDIRETORCCUSTOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {gDIRETORCCUSTOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormDiretorCCusto frm = new FormDiretorCCusto(cCustos.CODCCUSTO))
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            GDIRETORCCUSTO cCusto = (dgvListagem.SelectedItem as GDIRETORCCUSTO);
            if (cCusto.CKSELECIONADO)
            {
                try
                {
                    using (ControleGDiretorCC controle = new ControleGDiretorCC())
                    {
                        controle.Delete(x => x.CODCOLIGADA == cCusto.CODCOLIGADA && 
                            x.CODCCUSTO == cCusto.CODCCUSTO && x.CODDIRETOR == cCusto.CODDIRETOR);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();
                    }
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a exclusão do mesmo.");

            Cursor.Current = Cursors.Default;
        }
    }
}
