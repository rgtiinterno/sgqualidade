﻿namespace QUALIDADE.Forms.Cadastros.CentroCustos
{
    partial class FormCCusto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCCusto));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.txtCodCCusto = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtColigada = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbResponsavel = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumContrato = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDtInicioObra = new System.Windows.Forms.MaskedTextBox();
            this.txtDtFimObra = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dgvSetoresDistribuicao = new System.Windows.Forms.DataGridView();
            this.DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cODCOLIGADADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODCCUSTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODINTERNODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nOMEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTINICIODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dTFIMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECCREATEDBYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECCREATEDONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECMODIFIEDBYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rECMODIFIEDONDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gCENTROCUSTODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gCCUSTOTACBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetoresDistribuicao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCCUSTOTACBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(551, 462);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(141, 33);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(401, 462);
            this.btnGravar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(141, 33);
            this.btnGravar.TabIndex = 6;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // txtCodCCusto
            // 
            this.txtCodCCusto.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtCodCCusto.Location = new System.Drawing.Point(277, 52);
            this.txtCodCCusto.Margin = new System.Windows.Forms.Padding(4);
            this.txtCodCCusto.Name = "txtCodCCusto";
            this.txtCodCCusto.Size = new System.Drawing.Size(159, 22);
            this.txtCodCCusto.TabIndex = 0;
            this.txtCodCCusto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCodCCusto.TextChanged += new System.EventHandler(this.txtCodCCusto_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(273, 32);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 19);
            this.label7.TabIndex = 37;
            this.label7.Text = "Cód. C. Custo:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(16, 114);
            this.txtNome.Margin = new System.Windows.Forms.Padding(4);
            this.txtNome.MaxLength = 60;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(393, 22);
            this.txtNome.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(11, 95);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 36;
            this.label5.Text = "Nome:";
            // 
            // txtColigada
            // 
            this.txtColigada.Location = new System.Drawing.Point(16, 52);
            this.txtColigada.Margin = new System.Windows.Forms.Padding(4);
            this.txtColigada.MaxLength = 2;
            this.txtColigada.Name = "txtColigada";
            this.txtColigada.ReadOnly = true;
            this.txtColigada.Size = new System.Drawing.Size(105, 22);
            this.txtColigada.TabIndex = 32;
            this.txtColigada.Text = "1";
            this.txtColigada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 19);
            this.label4.TabIndex = 35;
            this.label4.Text = "Cód. Coligada:";
            // 
            // cbResponsavel
            // 
            this.cbResponsavel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbResponsavel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbResponsavel.FormattingEnabled = true;
            this.cbResponsavel.Location = new System.Drawing.Point(15, 174);
            this.cbResponsavel.Margin = new System.Windows.Forms.Padding(4);
            this.cbResponsavel.Name = "cbResponsavel";
            this.cbResponsavel.Size = new System.Drawing.Size(421, 24);
            this.cbResponsavel.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(11, 155);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 19);
            this.label1.TabIndex = 36;
            this.label1.Text = "Gerente de Contrato:";
            // 
            // txtNumContrato
            // 
            this.txtNumContrato.Location = new System.Drawing.Point(551, 52);
            this.txtNumContrato.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumContrato.MaxLength = 6;
            this.txtNumContrato.Name = "txtNumContrato";
            this.txtNumContrato.Size = new System.Drawing.Size(132, 22);
            this.txtNumContrato.TabIndex = 1;
            this.txtNumContrato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(547, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 19);
            this.label2.TabIndex = 37;
            this.label2.Text = "Nº do Contrato:";
            // 
            // txtDtInicioObra
            // 
            this.txtDtInicioObra.Location = new System.Drawing.Point(419, 114);
            this.txtDtInicioObra.Margin = new System.Windows.Forms.Padding(4);
            this.txtDtInicioObra.Mask = "99/99/9999";
            this.txtDtInicioObra.Name = "txtDtInicioObra";
            this.txtDtInicioObra.Size = new System.Drawing.Size(132, 22);
            this.txtDtInicioObra.TabIndex = 3;
            this.txtDtInicioObra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDtFimObra
            // 
            this.txtDtFimObra.Location = new System.Drawing.Point(560, 114);
            this.txtDtFimObra.Margin = new System.Windows.Forms.Padding(4);
            this.txtDtFimObra.Mask = "99/99/9999";
            this.txtDtFimObra.Name = "txtDtFimObra";
            this.txtDtFimObra.Size = new System.Drawing.Size(132, 22);
            this.txtDtFimObra.TabIndex = 4;
            this.txtDtFimObra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(556, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 19);
            this.label3.TabIndex = 37;
            this.label3.Text = "Dt. Fim Obra:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(415, 95);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 19);
            this.label6.TabIndex = 37;
            this.label6.Text = "Dt. Inicio Obra:";
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdd.Location = new System.Drawing.Point(612, 0);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(39, 22);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnAdd);
            this.groupBox6.Controls.Add(this.dgvSetoresDistribuicao);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(16, 219);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(676, 235);
            this.groupBox6.TabIndex = 38;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "TAC:";
            // 
            // dgvSetoresDistribuicao
            // 
            this.dgvSetoresDistribuicao.AllowUserToAddRows = false;
            this.dgvSetoresDistribuicao.AutoGenerateColumns = false;
            this.dgvSetoresDistribuicao.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSetoresDistribuicao.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvSetoresDistribuicao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSetoresDistribuicao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSetoresDistribuicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSetoresDistribuicao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DELETE,
            this.cODCOLIGADADataGridViewTextBoxColumn,
            this.cODCCUSTODataGridViewTextBoxColumn,
            this.cODINTERNODataGridViewTextBoxColumn,
            this.nOMEDataGridViewTextBoxColumn,
            this.dTINICIODataGridViewTextBoxColumn,
            this.dTFIMDataGridViewTextBoxColumn,
            this.rECCREATEDBYDataGridViewTextBoxColumn,
            this.rECCREATEDONDataGridViewTextBoxColumn,
            this.rECMODIFIEDBYDataGridViewTextBoxColumn,
            this.rECMODIFIEDONDataGridViewTextBoxColumn,
            this.gCENTROCUSTODataGridViewTextBoxColumn});
            this.dgvSetoresDistribuicao.DataSource = this.gCCUSTOTACBindingSource;
            this.dgvSetoresDistribuicao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetoresDistribuicao.Location = new System.Drawing.Point(4, 23);
            this.dgvSetoresDistribuicao.Margin = new System.Windows.Forms.Padding(4);
            this.dgvSetoresDistribuicao.Name = "dgvSetoresDistribuicao";
            this.dgvSetoresDistribuicao.ReadOnly = true;
            this.dgvSetoresDistribuicao.RowHeadersWidth = 51;
            this.dgvSetoresDistribuicao.Size = new System.Drawing.Size(668, 208);
            this.dgvSetoresDistribuicao.TabIndex = 51;
            this.dgvSetoresDistribuicao.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSetoresDistribuicao_CellContentClick);
            // 
            // DELETE
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.NullValue = "X";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.DELETE.DefaultCellStyle = dataGridViewCellStyle2;
            this.DELETE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DELETE.HeaderText = "X";
            this.DELETE.MinimumWidth = 6;
            this.DELETE.Name = "DELETE";
            this.DELETE.ReadOnly = true;
            this.DELETE.Text = "X";
            this.DELETE.Width = 23;
            // 
            // cODCOLIGADADataGridViewTextBoxColumn
            // 
            this.cODCOLIGADADataGridViewTextBoxColumn.DataPropertyName = "CODCOLIGADA";
            this.cODCOLIGADADataGridViewTextBoxColumn.HeaderText = "CODCOLIGADA";
            this.cODCOLIGADADataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cODCOLIGADADataGridViewTextBoxColumn.Name = "cODCOLIGADADataGridViewTextBoxColumn";
            this.cODCOLIGADADataGridViewTextBoxColumn.ReadOnly = true;
            this.cODCOLIGADADataGridViewTextBoxColumn.Visible = false;
            this.cODCOLIGADADataGridViewTextBoxColumn.Width = 137;
            // 
            // cODCCUSTODataGridViewTextBoxColumn
            // 
            this.cODCCUSTODataGridViewTextBoxColumn.DataPropertyName = "CODCCUSTO";
            this.cODCCUSTODataGridViewTextBoxColumn.HeaderText = "CODCCUSTO";
            this.cODCCUSTODataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cODCCUSTODataGridViewTextBoxColumn.Name = "cODCCUSTODataGridViewTextBoxColumn";
            this.cODCCUSTODataGridViewTextBoxColumn.ReadOnly = true;
            this.cODCCUSTODataGridViewTextBoxColumn.Visible = false;
            this.cODCCUSTODataGridViewTextBoxColumn.Width = 120;
            // 
            // cODINTERNODataGridViewTextBoxColumn
            // 
            this.cODINTERNODataGridViewTextBoxColumn.DataPropertyName = "CODINTERNO";
            this.cODINTERNODataGridViewTextBoxColumn.HeaderText = "CODINTERNO";
            this.cODINTERNODataGridViewTextBoxColumn.MinimumWidth = 6;
            this.cODINTERNODataGridViewTextBoxColumn.Name = "cODINTERNODataGridViewTextBoxColumn";
            this.cODINTERNODataGridViewTextBoxColumn.ReadOnly = true;
            this.cODINTERNODataGridViewTextBoxColumn.Visible = false;
            this.cODINTERNODataGridViewTextBoxColumn.Width = 125;
            // 
            // nOMEDataGridViewTextBoxColumn
            // 
            this.nOMEDataGridViewTextBoxColumn.DataPropertyName = "NOME";
            this.nOMEDataGridViewTextBoxColumn.HeaderText = "NOME";
            this.nOMEDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.nOMEDataGridViewTextBoxColumn.Name = "nOMEDataGridViewTextBoxColumn";
            this.nOMEDataGridViewTextBoxColumn.ReadOnly = true;
            this.nOMEDataGridViewTextBoxColumn.Width = 79;
            // 
            // dTINICIODataGridViewTextBoxColumn
            // 
            this.dTINICIODataGridViewTextBoxColumn.DataPropertyName = "DTINICIO";
            this.dTINICIODataGridViewTextBoxColumn.HeaderText = "DTINICIO";
            this.dTINICIODataGridViewTextBoxColumn.MinimumWidth = 6;
            this.dTINICIODataGridViewTextBoxColumn.Name = "dTINICIODataGridViewTextBoxColumn";
            this.dTINICIODataGridViewTextBoxColumn.ReadOnly = true;
            this.dTINICIODataGridViewTextBoxColumn.Width = 96;
            // 
            // dTFIMDataGridViewTextBoxColumn
            // 
            this.dTFIMDataGridViewTextBoxColumn.DataPropertyName = "DTFIM";
            this.dTFIMDataGridViewTextBoxColumn.HeaderText = "DTFIM";
            this.dTFIMDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.dTFIMDataGridViewTextBoxColumn.Name = "dTFIMDataGridViewTextBoxColumn";
            this.dTFIMDataGridViewTextBoxColumn.ReadOnly = true;
            this.dTFIMDataGridViewTextBoxColumn.Width = 78;
            // 
            // rECCREATEDBYDataGridViewTextBoxColumn
            // 
            this.rECCREATEDBYDataGridViewTextBoxColumn.DataPropertyName = "RECCREATEDBY";
            this.rECCREATEDBYDataGridViewTextBoxColumn.HeaderText = "RECCREATEDBY";
            this.rECCREATEDBYDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.rECCREATEDBYDataGridViewTextBoxColumn.Name = "rECCREATEDBYDataGridViewTextBoxColumn";
            this.rECCREATEDBYDataGridViewTextBoxColumn.ReadOnly = true;
            this.rECCREATEDBYDataGridViewTextBoxColumn.Visible = false;
            this.rECCREATEDBYDataGridViewTextBoxColumn.Width = 134;
            // 
            // rECCREATEDONDataGridViewTextBoxColumn
            // 
            this.rECCREATEDONDataGridViewTextBoxColumn.DataPropertyName = "RECCREATEDON";
            this.rECCREATEDONDataGridViewTextBoxColumn.HeaderText = "RECCREATEDON";
            this.rECCREATEDONDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.rECCREATEDONDataGridViewTextBoxColumn.Name = "rECCREATEDONDataGridViewTextBoxColumn";
            this.rECCREATEDONDataGridViewTextBoxColumn.ReadOnly = true;
            this.rECCREATEDONDataGridViewTextBoxColumn.Visible = false;
            this.rECCREATEDONDataGridViewTextBoxColumn.Width = 139;
            // 
            // rECMODIFIEDBYDataGridViewTextBoxColumn
            // 
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.DataPropertyName = "RECMODIFIEDBY";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.HeaderText = "RECMODIFIEDBY";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.Name = "rECMODIFIEDBYDataGridViewTextBoxColumn";
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.ReadOnly = true;
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.Visible = false;
            this.rECMODIFIEDBYDataGridViewTextBoxColumn.Width = 144;
            // 
            // rECMODIFIEDONDataGridViewTextBoxColumn
            // 
            this.rECMODIFIEDONDataGridViewTextBoxColumn.DataPropertyName = "RECMODIFIEDON";
            this.rECMODIFIEDONDataGridViewTextBoxColumn.HeaderText = "RECMODIFIEDON";
            this.rECMODIFIEDONDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.rECMODIFIEDONDataGridViewTextBoxColumn.Name = "rECMODIFIEDONDataGridViewTextBoxColumn";
            this.rECMODIFIEDONDataGridViewTextBoxColumn.ReadOnly = true;
            this.rECMODIFIEDONDataGridViewTextBoxColumn.Visible = false;
            this.rECMODIFIEDONDataGridViewTextBoxColumn.Width = 149;
            // 
            // gCENTROCUSTODataGridViewTextBoxColumn
            // 
            this.gCENTROCUSTODataGridViewTextBoxColumn.DataPropertyName = "GCENTROCUSTO";
            this.gCENTROCUSTODataGridViewTextBoxColumn.HeaderText = "GCENTROCUSTO";
            this.gCENTROCUSTODataGridViewTextBoxColumn.MinimumWidth = 6;
            this.gCENTROCUSTODataGridViewTextBoxColumn.Name = "gCENTROCUSTODataGridViewTextBoxColumn";
            this.gCENTROCUSTODataGridViewTextBoxColumn.ReadOnly = true;
            this.gCENTROCUSTODataGridViewTextBoxColumn.Visible = false;
            this.gCENTROCUSTODataGridViewTextBoxColumn.Width = 143;
            // 
            // gCCUSTOTACBindingSource
            // 
            this.gCCUSTOTACBindingSource.DataSource = typeof(QUALIDADE.Dominio.GCCUSTOTAC);
            // 
            // FormCCusto
            // 
            this.AcceptButton = this.btnGravar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 503);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.txtDtFimObra);
            this.Controls.Add(this.txtDtInicioObra);
            this.Controls.Add(this.txtNumContrato);
            this.Controls.Add(this.cbResponsavel);
            this.Controls.Add(this.txtCodCCusto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtColigada);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCCusto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Centro de Custo";
            this.Load += new System.EventHandler(this.FormCCusto_Load);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetoresDistribuicao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gCCUSTOTACBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.MaskedTextBox txtCodCCusto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtColigada;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbResponsavel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumContrato;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox txtDtInicioObra;
        private System.Windows.Forms.MaskedTextBox txtDtFimObra;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgvSetoresDistribuicao;
        private System.Windows.Forms.DataGridViewButtonColumn DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODCOLIGADADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODCCUSTODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODINTERNODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nOMEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTINICIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dTFIMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECCREATEDBYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECCREATEDONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECMODIFIEDBYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rECMODIFIEDONDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn gCENTROCUSTODataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource gCCUSTOTACBindingSource;
    }
}