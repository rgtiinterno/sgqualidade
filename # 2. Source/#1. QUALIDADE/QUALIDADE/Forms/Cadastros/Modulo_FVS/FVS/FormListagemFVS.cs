﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.NaoConformidade;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.FVS
{
    public partial class FormListagemFVS : Form
    {
        public FormListagemFVS()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"FVSs.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListagemFVS_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. Obra", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCCUSTO", HeaderText = "Obra", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODMODELO", HeaderText = "Cód. Modelo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SERVICOINSPECIONADO", HeaderText = "Serviço Inspecionado", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOCALASERVERIFICADO", HeaderText = "Local Verificado", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTINICIOSERVICO", HeaderText = "Dt. Inicio Serviço", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTFIMSERVICO", HeaderText = "Dt. Fim Serviço", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVELSERVICO", HeaderText = "Responsável", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "EMCONFORMIDADE", HeaderText = "Em Conformidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODNCONFORMIDADE", HeaderText = "CODNCONFORMIDADE", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCNCONFORMIDADE", HeaderText = "DESCNCONFORMIDADE", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleVFichaServicos controle = new ControleVFichaServicos())
            {
                Cursor.Current = Cursors.WaitCursor;
                vFICHAVERFSERVICOSBindingSource = new BindingSource();
                vFICHAVERFSERVICOSBindingSource.DataSource = typeof(VFICHAVERFSERVICOS);
                vFICHAVERFSERVICOSBindingSource.DataSource = controle.GetAll(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                dgvListagem.DataSource = vFICHAVERFSERVICOSBindingSource;
                vFICHAVERFSERVICOSBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {vFICHAVERFSERVICOSBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormFichaVerificacao frm = new FormFichaVerificacao())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    if(frm.abrirNCONFORM != null)
                    {
                        using (FormNConformidade form = new FormNConformidade(frm.abrirNCONFORM, true))
                        {
                            form.StartPosition = FormStartPosition.CenterParent;
                            form.WindowState = FormWindowState.Normal;
                            form.ShowDialog();

                            form.Dispose();
                        }
                    }

                    tsbAtualizar.PerformClick();
                }

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is VFICHAVERFSERVICOS item)
            {
                using (FormFichaVerificacao frm = new FormFichaVerificacao(item))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        if (frm.abrirNCONFORM != null)
                        {
                            using (FormNConformidade form = new FormNConformidade(frm.abrirNCONFORM, true))
                            {
                                form.StartPosition = FormStartPosition.CenterParent;
                                form.WindowState = FormWindowState.Normal;
                                form.ShowDialog();

                                form.Dispose();
                            }
                        }

                        tsbAtualizar.PerformClick();
                    }

                    frm.Dispose();
                }
            }
            else
            {
                FormMsg frm = new FormMsg("Selecione um registro para que o mesmo possa ser editado");
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is VFICHAVERFSERVICOS item)
            {
                using (ControleVFichaServicos controle = new ControleVFichaServicos())
                {
                    using (ControleVFichaItens controleDesModelo = new ControleVFichaItens())
                    {
                        controleDesModelo.Delete(item.CODIGO, item.CODCOLIGADA, item.CODMODELO);
                        controle.Delete(x => x.CODIGO == item.CODIGO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
            }
            else
            {
                FormMsg frm = new FormMsg("Selecione um registro para que o mesmo possa ser excluído");
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();

                frm.Dispose();
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbEditar.PerformClick();
        }
    }
}
