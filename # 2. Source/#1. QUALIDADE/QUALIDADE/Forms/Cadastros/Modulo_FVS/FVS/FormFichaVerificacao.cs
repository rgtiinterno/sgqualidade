﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.NaoConformidade;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.FVS
{
    public partial class FormFichaVerificacao : Form
    {
        private VFICHAVERFSERVICOS itemEdit;
        public bool salvo = false;
        public CNCONFORM abrirNCONFORM = null;
        public bool inicioCarregamento = true;

        public FormFichaVerificacao()
        {
            InitializeComponent();
        }

        public FormFichaVerificacao(VFICHAVERFSERVICOS ficha)
        {
            InitializeComponent();
            itemEdit = ficha;
        }

        private void FormFichaVerificacao_Load(object sender, EventArgs e)
        {
            CarregaCombos();

            if (itemEdit != null)
            {
                txtCodigo.Text = itemEdit.CODIGO;
                txtColigada.Text = $"{itemEdit.CODCOLIGADA}";
                txtConformidade.Text = $"{itemEdit.CODNCONFORMIDADE}";
                cbObra.SelectedValue = itemEdit.CODCCUSTO;
                cbModelo.SelectedValue = itemEdit.CODMODELO;
                txtResponsavel.Text = itemEdit.RESPONSAVELSERVICO;
                txtLocalVerificado.Text = itemEdit.LOCALASERVERIFICADO;
                txtNaoConform.Text = itemEdit.CODNCONFORMIDADE.HasValue ? $"{itemEdit.CODNCONFORMIDADE.Value}" : null;
                txtDtInicio.Text = $"{itemEdit.DTINICIOSERVICO}";
                txtDtFim.Text = $"{itemEdit.DTFIMSERVICO}";
                ckNao.Checked = itemEdit.EMCONFORMIDADE;
                txtDescricao.Text = itemEdit.DESCNCONFORMIDADE;

                if (ckSim.Checked)
                {
                    txtDescricao.Enabled = true;
                    txtDescricao.Enabled = false;


                    label7.Visible = true;
                    txtNaoConform.Visible = true;
                    lblVerConformidade.Visible = true;
                }

                using (ControleVFichaItens controle = new ControleVFichaItens())
                {
                    inspecaoBindingSource.DataSource = controle.GetList(itemEdit.CODIGO, itemEdit.CODCOLIGADA, itemEdit.CODMODELO);
                    dgvListagem.DataSource = inspecaoBindingSource;
                    dgvListagem.EndEdit();
                    inspecaoBindingSource.EndEdit();
                }

            }
            else
            {
                using (ControleVFichaServicos controle = new ControleVFichaServicos())
                {
                    txtCodigo.Text = $"{controle.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA)}";
                    txtColigada.Text = $"{FormPrincipal.getUsuarioAcesso().CODCOLIGADA}";
                }

                label7.Visible = false;
                txtNaoConform.Visible = false;
                lblVerConformidade.Visible = false;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                VFICHAVERFSERVICOS item = new VFICHAVERFSERVICOS
                {
                    CODIGO = txtCodigo.Text,
                    CODCOLIGADA = Convert.ToInt16(txtColigada.Text),
                    CODMODELO = Convert.ToString(cbModelo.SelectedValue),
                    CODCCUSTO = Convert.ToString(cbObra.SelectedValue),
                    RESPONSAVELSERVICO = txtResponsavel.Text,
                    LOCALASERVERIFICADO = txtLocalVerificado.Text,
                    SERVICOINSPECIONADO = $"{txtCodigo.Text} - {cbModelo.Text}",
                    DTINICIOSERVICO = Convert.ToDateTime(txtDtInicio.Text),
                    DTFIMSERVICO = Convert.ToDateTime(txtDtFim.Text),
                    EMCONFORMIDADE = ckNao.Checked,
                    DESCNCONFORMIDADE = ckNao.Checked ? null : txtDescricao.Text
                };

                List<VFICHAITENS> listDescModelo = new List<VFICHAITENS>();

                foreach (DataGridViewRow row in dgvListagem.Rows)
                {
                    VFICHAITENS VFICHAITENS = new VFICHAITENS
                    {
                        CODFICHA = item.CODIGO,
                        CODCOLIGADA = item.CODCOLIGADA,
                        CODMODELO = item.CODMODELO,
                        ITEM = row.Cells["ITEM"].Value.DBNullToString(),
                        ACAO = row.Cells["ACAO"].Value.DBNullToString(),
                        INSPECAO = row.Cells["INSPECAO"].Value.DBNullToString(),
                        REINSPECAO = row.Cells["REINSPENCAO"].Value.DBNullToString()
                    };

                    listDescModelo.Add(VFICHAITENS);
                }

                using (ControleVFichaServicos controle = new ControleVFichaServicos())
                {
                    using (ControleVFichaItens controleDesModelo = new ControleVFichaItens())
                    {
                        if (itemEdit != null)
                        {
                            if (item.EMCONFORMIDADE == false && itemEdit.EMCONFORMIDADE == true)
                            {
                                using (ControleCNConformidade controleNConfor = new ControleCNConformidade())
                                {
                                    int codNConformidade = Convert.ToInt32(controleNConfor.GetID(item.CODCOLIGADA));
                                    item.CODNCONFORMIDADE = codNConformidade;

                                    abrirNCONFORM = new CNCONFORM
                                    {
                                        CODNCONFORM = codNConformidade,
                                        CODCCUSTO = item.CODCCUSTO,
                                        CODCOLIGADA = item.CODCOLIGADA,
                                        TPACAO = "C",
                                        DATAABERTURANC = DateTime.Now,
                                        NCONFREALPONTENCIAL = item.DESCNCONFORMIDADE,
                                        NECACAOCORPREV = true,
                                        STATUS = "A",
                                    };
                                }

                            }

                            controle.Update(item, x => x.CODIGO == item.CODIGO);
                            controle.SaveAll();

                            controleDesModelo.Delete(item.CODIGO, item.CODCOLIGADA, item.CODMODELO);
                            controleDesModelo.AllCreate(listDescModelo);

                            salvo = true;
                            this.Close();
                        }
                        else
                        {
                            item.VFICHAITENS = null;

                            if (!item.EMCONFORMIDADE)
                            {
                                using (ControleCNConformidade controleNConfor = new ControleCNConformidade())
                                {
                                    short codNConformidade = Convert.ToInt16(controleNConfor.GetID(item.CODCOLIGADA));
                                    item.CODNCONFORMIDADE = codNConformidade;
                                    controle.Create(item);

                                    abrirNCONFORM = new CNCONFORM
                                    {
                                        CODNCONFORM = codNConformidade,
                                        CODCCUSTO = item.CODCCUSTO,
                                        CODCOLIGADA = item.CODCOLIGADA,
                                        TPACAO = "C",
                                        DATAABERTURANC = DateTime.Now,
                                        NCONFREALPONTENCIAL = item.DESCNCONFORMIDADE,
                                        NECACAOCORPREV = true,
                                        STATUS = "A",
                                    };
                                }

                            }
                            else
                                controle.Create(item);

                            controle.SaveAll();
                            controleDesModelo.AllCreate(listDescModelo);

                            salvo = true;
                            this.Close();
                        }
                    }
                }
            }
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !ckSim.Checked;
            txtDescricao.Enabled = true;
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !ckNao.Checked;
            txtDescricao.Enabled = false;
        }

        private void cbModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!inicioCarregamento)
            {
                if (cbModelo.SelectedItem is VMODELOFVS modelo)
                {
                    using (ControleVModelo controleModelo = new ControleVModelo())
                    {
                        VMODELOFVS item = controleModelo.Find(modelo.CODIGO);
                        if (item != null)
                        {
                            txtRevisao.Text = $"{item.NUMREVISAO}";
                            txtAprovador.Text = $"{item.APROVADOPOR}";
                            txtIT.Text = $"{item.CONFORMEIT}";

                            using (ControleVDescricaoModelo controle = new ControleVDescricaoModelo())
                            {
                                inspecaoBindingSource.DataSource = controle.GetList(item.CODIGO);
                                dgvListagem.DataSource = inspecaoBindingSource;
                                dgvListagem.EndEdit();
                                inspecaoBindingSource.EndEdit();
                            }
                        }
                    }
                }
            }
        }

        private void cbObra_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!inicioCarregamento)
            {
                if (cbObra.SelectedItem is GCENTROCUSTO cCusto)
                {
                    using (ControleGCentroCusto controleCCusto = new ControleGCentroCusto())
                    {
                        GCENTROCUSTO item = controleCCusto.Find(cCusto.CODCOLIGADA, cCusto.CODCCUSTO);
                        if (item != null)
                            txtNContrato.Text = $"{item.NUMCONTRATO}";
                    }
                }
            }
        }

        private void CarregaCombos()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                cbObra.DataSource = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "V");

                cbObra.ValueMember = "CODCCUSTO";
                cbObra.DisplayMember = "NOME";
                cbObra.SelectedIndex = -1;
            }

            using (ControleVModelo controle = new ControleVModelo())
            {
                cbModelo.DataSource = controle.GetAll().ToList();
                cbModelo.ValueMember = "CODIGO";
                cbModelo.DisplayMember = "NOME";
                cbModelo.SelectedIndex = -1;
            }

            inicioCarregamento = false;
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (cbModelo.SelectedIndex < 0)
                msg += " - Informe qual o modelo a ser seguido.\n";

            if (cbObra.SelectedIndex < 0)
                msg += " - Informe o centro de custo.\n";

            if (string.IsNullOrEmpty(txtLocalVerificado.Text))
                msg += " - Informe o local a ser verificado.\n";

            if (!Global.ValidaData(txtDtInicio.Text))
                msg += " - Informe a data de inicio da obra.\n";

            if (!Global.ValidaData(txtDtFim.Text))
                msg += " - Informe a data de termino da obra.\n";

            if (string.IsNullOrEmpty(txtResponsavel.Text))
                msg += " - Informe o responsável pela ficha.\n";

            if (ckSim.Checked)
            {
                if (string.IsNullOrEmpty(txtDescricao.Text))
                    msg += " - Informe a descrição da não conformidade";
            }

            if (string.IsNullOrWhiteSpace(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void btnLegenda_Click(object sender, EventArgs e)
        {
            using (FormLegenda frm = new FormLegenda())
            {
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.ShowDialog();
            }
        }

        private void lblVerConformidade_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (ControleCNConformidade controle = new ControleCNConformidade())
            {
                CNCONFORM item = controle.Find(Convert.ToInt16(txtNaoConform.Text), 
                    Convert.ToInt16(txtColigada.Text), Convert.ToString(cbObra.SelectedValue));

                if (item != null)
                {
                    using (FormNConformidade form = new FormNConformidade(item))
                    {
                        form.StartPosition = FormStartPosition.CenterParent;
                        form.WindowState = FormWindowState.Normal;
                        form.ShowDialog();

                        form.Dispose();
                    }
                }
                else
                    Global.MsgInformacao(" - Não foi possivel encontrar o registro da não conformidade.");
            }
        }
    }
}
