﻿namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.FVS
{
    partial class FormFichaVerificacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFichaVerificacao));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRevisao = new System.Windows.Forms.TextBox();
            this.txtAprovador = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtColigada = new System.Windows.Forms.TextBox();
            this.cbObra = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNContrato = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbModelo = new System.Windows.Forms.ComboBox();
            this.txtLocalVerificado = new System.Windows.Forms.TextBox();
            this.txtIT = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDtInicio = new System.Windows.Forms.MaskedTextBox();
            this.txtDtFim = new System.Windows.Forms.MaskedTextBox();
            this.txtResponsavel = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ckSim = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblVerConformidade = new System.Windows.Forms.LinkLabel();
            this.txtNaoConform = new System.Windows.Forms.TextBox();
            this.ckNao = new System.Windows.Forms.CheckBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvListagem = new System.Windows.Forms.DataGridView();
            this.ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TELERANCIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INSPECAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REINSPENCAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspecaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.txtConformidade = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inspecaoBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(770, 604);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(658, 604);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 16;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(459, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "REVISÃO:";
            // 
            // txtRevisao
            // 
            this.txtRevisao.Location = new System.Drawing.Point(520, 18);
            this.txtRevisao.Name = "txtRevisao";
            this.txtRevisao.ReadOnly = true;
            this.txtRevisao.Size = new System.Drawing.Size(72, 22);
            this.txtRevisao.TabIndex = 3;
            this.txtRevisao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtAprovador
            // 
            this.txtAprovador.Location = new System.Drawing.Point(681, 18);
            this.txtAprovador.Name = "txtAprovador";
            this.txtAprovador.ReadOnly = true;
            this.txtAprovador.Size = new System.Drawing.Size(191, 22);
            this.txtAprovador.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(599, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "APROVADOR:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "CÓDIGO:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(150, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "COLIGADA:";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(72, 18);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(72, 22);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtColigada
            // 
            this.txtColigada.Location = new System.Drawing.Point(220, 18);
            this.txtColigada.Name = "txtColigada";
            this.txtColigada.ReadOnly = true;
            this.txtColigada.Size = new System.Drawing.Size(72, 22);
            this.txtColigada.TabIndex = 1;
            this.txtColigada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbObra
            // 
            this.cbObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbObra.FormattingEnabled = true;
            this.cbObra.Location = new System.Drawing.Point(13, 81);
            this.cbObra.Name = "cbObra";
            this.cbObra.Size = new System.Drawing.Size(365, 21);
            this.cbObra.TabIndex = 5;
            this.cbObra.SelectedIndexChanged += new System.EventHandler(this.cbObra_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(12, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Obra:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(381, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Nº Contrato:";
            // 
            // txtNContrato
            // 
            this.txtNContrato.Location = new System.Drawing.Point(384, 80);
            this.txtNContrato.Name = "txtNContrato";
            this.txtNContrato.ReadOnly = true;
            this.txtNContrato.Size = new System.Drawing.Size(101, 22);
            this.txtNContrato.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(597, 65);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Modelo de FVS:";
            // 
            // cbModelo
            // 
            this.cbModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModelo.FormattingEnabled = true;
            this.cbModelo.Location = new System.Drawing.Point(598, 81);
            this.cbModelo.Name = "cbModelo";
            this.cbModelo.Size = new System.Drawing.Size(274, 21);
            this.cbModelo.TabIndex = 8;
            this.cbModelo.SelectedIndexChanged += new System.EventHandler(this.cbModelo_SelectedIndexChanged);
            // 
            // txtLocalVerificado
            // 
            this.txtLocalVerificado.Location = new System.Drawing.Point(13, 138);
            this.txtLocalVerificado.Name = "txtLocalVerificado";
            this.txtLocalVerificado.Size = new System.Drawing.Size(361, 22);
            this.txtLocalVerificado.TabIndex = 9;
            // 
            // txtIT
            // 
            this.txtIT.Location = new System.Drawing.Point(491, 80);
            this.txtIT.Name = "txtIT";
            this.txtIT.ReadOnly = true;
            this.txtIT.Size = new System.Drawing.Size(101, 22);
            this.txtIT.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(10, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Local a ser verificado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(488, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Conforme I.T:";
            // 
            // txtDtInicio
            // 
            this.txtDtInicio.Location = new System.Drawing.Point(380, 138);
            this.txtDtInicio.Mask = "99/99/9999";
            this.txtDtInicio.Name = "txtDtInicio";
            this.txtDtInicio.Size = new System.Drawing.Size(100, 22);
            this.txtDtInicio.TabIndex = 10;
            this.txtDtInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDtFim
            // 
            this.txtDtFim.Location = new System.Drawing.Point(507, 138);
            this.txtDtFim.Mask = "99/99/9999";
            this.txtDtFim.Name = "txtDtFim";
            this.txtDtFim.Size = new System.Drawing.Size(100, 22);
            this.txtDtFim.TabIndex = 12;
            this.txtDtFim.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResponsavel
            // 
            this.txtResponsavel.Location = new System.Drawing.Point(613, 138);
            this.txtResponsavel.Name = "txtResponsavel";
            this.txtResponsavel.Size = new System.Drawing.Size(259, 22);
            this.txtResponsavel.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(610, 122);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(158, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Responsável pela verificação:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(504, 122);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Dt. Fim Serviço:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(377, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Dt. Inicio Serviço:";
            // 
            // ckSim
            // 
            this.ckSim.AutoSize = true;
            this.ckSim.Location = new System.Drawing.Point(36, 64);
            this.ckSim.Name = "ckSim";
            this.ckSim.Size = new System.Drawing.Size(45, 17);
            this.ckSim.TabIndex = 14;
            this.ckSim.Text = "SIM";
            this.ckSim.UseVisualStyleBackColor = true;
            this.ckSim.CheckedChanged += new System.EventHandler(this.ckSim_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblVerConformidade);
            this.groupBox1.Controls.Add(this.txtNaoConform);
            this.groupBox1.Controls.Add(this.ckSim);
            this.groupBox1.Controls.Add(this.ckNao);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtDescricao);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(396, 180);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(476, 187);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Em caso de NÃO CONFORMIDADE, será necessário abrir uma Ação Corretiva?";
            // 
            // lblVerConformidade
            // 
            this.lblVerConformidade.AutoSize = true;
            this.lblVerConformidade.Location = new System.Drawing.Point(171, 145);
            this.lblVerConformidade.Name = "lblVerConformidade";
            this.lblVerConformidade.Size = new System.Drawing.Size(121, 13);
            this.lblVerConformidade.TabIndex = 18;
            this.lblVerConformidade.TabStop = true;
            this.lblVerConformidade.Text = "Ver não conformidade";
            this.lblVerConformidade.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblVerConformidade_LinkClicked);
            // 
            // txtNaoConform
            // 
            this.txtNaoConform.Location = new System.Drawing.Point(20, 142);
            this.txtNaoConform.Name = "txtNaoConform";
            this.txtNaoConform.ReadOnly = true;
            this.txtNaoConform.Size = new System.Drawing.Size(126, 22);
            this.txtNaoConform.TabIndex = 17;
            // 
            // ckNao
            // 
            this.ckNao.AutoSize = true;
            this.ckNao.Checked = true;
            this.ckNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckNao.Location = new System.Drawing.Point(101, 64);
            this.ckNao.Name = "ckNao";
            this.ckNao.Size = new System.Drawing.Size(50, 17);
            this.ckNao.TabIndex = 15;
            this.ckNao.Text = "NÃO";
            this.ckNao.UseVisualStyleBackColor = true;
            this.ckNao.CheckedChanged += new System.EventHandler(this.ckNao_CheckedChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label14.Location = new System.Drawing.Point(184, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(173, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "Descrição da não conformidade:";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Enabled = false;
            this.txtDescricao.Location = new System.Drawing.Point(187, 47);
            this.txtDescricao.Multiline = true;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(283, 62);
            this.txtDescricao.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Cod. Não Conf.:";
            // 
            // dgvListagem
            // 
            this.dgvListagem.AllowUserToAddRows = false;
            this.dgvListagem.AllowUserToDeleteRows = false;
            this.dgvListagem.AutoGenerateColumns = false;
            this.dgvListagem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvListagem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvListagem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListagem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM,
            this.DESCRICAO,
            this.TELERANCIA,
            this.INSPECAO,
            this.ACAO,
            this.REINSPENCAO});
            this.dgvListagem.DataSource = this.inspecaoBindingSource;
            this.dgvListagem.Location = new System.Drawing.Point(12, 373);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.RowHeadersWidth = 51;
            this.dgvListagem.Size = new System.Drawing.Size(864, 225);
            this.dgvListagem.TabIndex = 14;
            // 
            // ITEM
            // 
            this.ITEM.DataPropertyName = "item";
            this.ITEM.HeaderText = "Item";
            this.ITEM.MinimumWidth = 6;
            this.ITEM.Name = "ITEM";
            this.ITEM.ReadOnly = true;
            this.ITEM.Width = 54;
            // 
            // DESCRICAO
            // 
            this.DESCRICAO.DataPropertyName = "Descricao";
            this.DESCRICAO.HeaderText = "Descrição";
            this.DESCRICAO.MinimumWidth = 6;
            this.DESCRICAO.Name = "DESCRICAO";
            this.DESCRICAO.ReadOnly = true;
            this.DESCRICAO.Width = 81;
            // 
            // TELERANCIA
            // 
            this.TELERANCIA.DataPropertyName = "Tolerencia";
            this.TELERANCIA.HeaderText = "Tolerância";
            this.TELERANCIA.MinimumWidth = 6;
            this.TELERANCIA.Name = "TELERANCIA";
            this.TELERANCIA.ReadOnly = true;
            this.TELERANCIA.Width = 84;
            // 
            // INSPECAO
            // 
            this.INSPECAO.DataPropertyName = "Insp";
            this.INSPECAO.HeaderText = "Inspeção";
            this.INSPECAO.MinimumWidth = 6;
            this.INSPECAO.Name = "INSPECAO";
            this.INSPECAO.Width = 78;
            // 
            // ACAO
            // 
            this.ACAO.DataPropertyName = "Acao";
            this.ACAO.HeaderText = "Ação";
            this.ACAO.MinimumWidth = 6;
            this.ACAO.Name = "ACAO";
            this.ACAO.Width = 57;
            // 
            // REINSPENCAO
            // 
            this.REINSPENCAO.DataPropertyName = "Reins";
            this.REINSPENCAO.HeaderText = "Reinspenção";
            this.REINSPENCAO.MinimumWidth = 6;
            this.REINSPENCAO.Name = "REINSPENCAO";
            this.REINSPENCAO.Width = 98;
            // 
            // inspecaoBindingSource
            // 
            this.inspecaoBindingSource.DataSource = typeof(QUALIDADE.Dominio.Inspecao);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(308, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(70, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "NÃO CONF.:";
            // 
            // txtConformidade
            // 
            this.txtConformidade.Location = new System.Drawing.Point(380, 18);
            this.txtConformidade.Name = "txtConformidade";
            this.txtConformidade.ReadOnly = true;
            this.txtConformidade.Size = new System.Drawing.Size(72, 22);
            this.txtConformidade.TabIndex = 2;
            this.txtConformidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(12, 180);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(366, 187);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Legenda";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(202, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(122, 30);
            this.label20.TabIndex = 0;
            this.label20.Text = "R = Rejeitar, recusar;\r\nC = Reclassificar;";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(218, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 15);
            this.label17.TabIndex = 0;
            this.label17.Text = "O = Não conforme;";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(25, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(144, 15);
            this.label19.TabIndex = 0;
            this.label19.Text = "X = Conforme (Aprovado)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 130);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(139, 30);
            this.label16.TabIndex = 0;
            this.label16.Text = "A = Aceitar como está;\r\nT = Retrabalhar, refazer;";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Italic | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 261);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(366, 28);
            this.label18.TabIndex = 1;
            this.label18.Text = "Ação a ser tomada no serviço NÃO CONFORME";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormFichaVerificacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 643);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgvListagem);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtDtFim);
            this.Controls.Add(this.txtDtInicio);
            this.Controls.Add(this.cbModelo);
            this.Controls.Add(this.cbObra);
            this.Controls.Add(this.txtColigada);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.txtLocalVerificado);
            this.Controls.Add(this.txtIT);
            this.Controls.Add(this.txtNContrato);
            this.Controls.Add(this.txtResponsavel);
            this.Controls.Add(this.txtAprovador);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtConformidade);
            this.Controls.Add(this.txtRevisao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFichaVerificacao";
            this.Text = "Ficha Verificação de Serviço";
            this.Load += new System.EventHandler(this.FormFichaVerificacao_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inspecaoBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRevisao;
        private System.Windows.Forms.TextBox txtAprovador;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtColigada;
        private System.Windows.Forms.ComboBox cbObra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNContrato;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbModelo;
        private System.Windows.Forms.TextBox txtLocalVerificado;
        private System.Windows.Forms.TextBox txtIT;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtDtInicio;
        private System.Windows.Forms.MaskedTextBox txtDtFim;
        private System.Windows.Forms.TextBox txtResponsavel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox ckSim;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckNao;
        private System.Windows.Forms.DataGridView dgvListagem;
        private System.Windows.Forms.BindingSource inspecaoBindingSource;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TELERANCIA;
        private System.Windows.Forms.DataGridViewTextBoxColumn INSPECAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn REINSPENCAO;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtConformidade;
        private System.Windows.Forms.LinkLabel lblVerConformidade;
        private System.Windows.Forms.TextBox txtNaoConform;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
    }
}