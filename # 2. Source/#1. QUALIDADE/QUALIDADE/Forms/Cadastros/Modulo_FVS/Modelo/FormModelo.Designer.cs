﻿namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.Modelo
{
    partial class FormModelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormModelo));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRevisao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDataRevisao = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAprovador = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConformeIT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvListagem = new System.Windows.Forms.DataGridView();
            this.MODELOITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MODELODESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MODELOTOLERANCIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.acaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reinsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inspecaoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inspecaoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(571, 411);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(459, 411);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 10;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(12, 37);
            this.txtCodigo.MaxLength = 15;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(100, 26);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 19);
            this.label1.TabIndex = 13;
            this.label1.Text = "Código:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(118, 37);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(440, 26);
            this.txtNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(115, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 19);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nome:";
            // 
            // txtRevisao
            // 
            this.txtRevisao.Location = new System.Drawing.Point(564, 37);
            this.txtRevisao.Name = "txtRevisao";
            this.txtRevisao.Size = new System.Drawing.Size(100, 26);
            this.txtRevisao.TabIndex = 2;
            this.txtRevisao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(561, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 19);
            this.label3.TabIndex = 13;
            this.label3.Text = "Revisão:";
            // 
            // txtDataRevisao
            // 
            this.txtDataRevisao.Location = new System.Drawing.Point(12, 98);
            this.txtDataRevisao.Mask = "99/99/9999";
            this.txtDataRevisao.Name = "txtDataRevisao";
            this.txtDataRevisao.Size = new System.Drawing.Size(100, 26);
            this.txtDataRevisao.TabIndex = 3;
            this.txtDataRevisao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 19);
            this.label4.TabIndex = 13;
            this.label4.Text = "Data Revisão:";
            // 
            // txtAprovador
            // 
            this.txtAprovador.Location = new System.Drawing.Point(118, 98);
            this.txtAprovador.Name = "txtAprovador";
            this.txtAprovador.Size = new System.Drawing.Size(335, 26);
            this.txtAprovador.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(115, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 19);
            this.label5.TabIndex = 13;
            this.label5.Text = "Aprovador:";
            // 
            // txtConformeIT
            // 
            this.txtConformeIT.Location = new System.Drawing.Point(459, 98);
            this.txtConformeIT.MaxLength = 15;
            this.txtConformeIT.Name = "txtConformeIT";
            this.txtConformeIT.Size = new System.Drawing.Size(99, 26);
            this.txtConformeIT.TabIndex = 5;
            this.txtConformeIT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(456, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Conforme I.T:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvListagem);
            this.groupBox1.Location = new System.Drawing.Point(12, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(652, 261);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inspeção";
            // 
            // dgvListagem
            // 
            this.dgvListagem.AutoGenerateColumns = false;
            this.dgvListagem.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvListagem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvListagem.ColumnHeadersHeight = 29;
            this.dgvListagem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MODELOITEM,
            this.MODELODESCRICAO,
            this.MODELOTOLERANCIA,
            this.inspDataGridViewTextBoxColumn,
            this.acaoDataGridViewTextBoxColumn,
            this.reinsDataGridViewTextBoxColumn});
            this.dgvListagem.DataSource = this.inspecaoBindingSource;
            this.dgvListagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListagem.Location = new System.Drawing.Point(3, 22);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.RowHeadersWidth = 51;
            this.dgvListagem.Size = new System.Drawing.Size(646, 236);
            this.dgvListagem.TabIndex = 0;
            // 
            // MODELOITEM
            // 
            this.MODELOITEM.DataPropertyName = "item";
            this.MODELOITEM.HeaderText = "Item";
            this.MODELOITEM.MinimumWidth = 6;
            this.MODELOITEM.Name = "MODELOITEM";
            this.MODELOITEM.Width = 54;
            // 
            // MODELODESCRICAO
            // 
            this.MODELODESCRICAO.DataPropertyName = "Descricao";
            this.MODELODESCRICAO.HeaderText = "Descrição";
            this.MODELODESCRICAO.MinimumWidth = 6;
            this.MODELODESCRICAO.Name = "MODELODESCRICAO";
            this.MODELODESCRICAO.Width = 275;
            // 
            // MODELOTOLERANCIA
            // 
            this.MODELOTOLERANCIA.DataPropertyName = "Tolerencia";
            this.MODELOTOLERANCIA.HeaderText = "Tolerância";
            this.MODELOTOLERANCIA.MinimumWidth = 6;
            this.MODELOTOLERANCIA.Name = "MODELOTOLERANCIA";
            this.MODELOTOLERANCIA.Width = 275;
            // 
            // inspDataGridViewTextBoxColumn
            // 
            this.inspDataGridViewTextBoxColumn.DataPropertyName = "Insp";
            this.inspDataGridViewTextBoxColumn.HeaderText = "Insp";
            this.inspDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.inspDataGridViewTextBoxColumn.Name = "inspDataGridViewTextBoxColumn";
            this.inspDataGridViewTextBoxColumn.Visible = false;
            this.inspDataGridViewTextBoxColumn.Width = 54;
            // 
            // acaoDataGridViewTextBoxColumn
            // 
            this.acaoDataGridViewTextBoxColumn.DataPropertyName = "Acao";
            this.acaoDataGridViewTextBoxColumn.HeaderText = "Acao";
            this.acaoDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.acaoDataGridViewTextBoxColumn.Name = "acaoDataGridViewTextBoxColumn";
            this.acaoDataGridViewTextBoxColumn.Visible = false;
            this.acaoDataGridViewTextBoxColumn.Width = 57;
            // 
            // reinsDataGridViewTextBoxColumn
            // 
            this.reinsDataGridViewTextBoxColumn.DataPropertyName = "Reins";
            this.reinsDataGridViewTextBoxColumn.HeaderText = "Reins";
            this.reinsDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.reinsDataGridViewTextBoxColumn.Name = "reinsDataGridViewTextBoxColumn";
            this.reinsDataGridViewTextBoxColumn.Visible = false;
            this.reinsDataGridViewTextBoxColumn.Width = 60;
            // 
            // inspecaoBindingSource
            // 
            this.inspecaoBindingSource.DataSource = typeof(QUALIDADE.Dominio.Inspecao);
            // 
            // FormModelo
            // 
            this.AcceptButton = this.btnGravar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtDataRevisao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAprovador);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.txtConformeIT);
            this.Controls.Add(this.txtRevisao);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormModelo";
            this.Text = "Modelo";
            this.Load += new System.EventHandler(this.FormModelo_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inspecaoBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRevisao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtDataRevisao;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAprovador;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConformeIT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvListagem;
        private System.Windows.Forms.BindingSource inspecaoBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn MODELOITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn MODELODESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MODELOTOLERANCIA;
        private System.Windows.Forms.DataGridViewTextBoxColumn inspDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn acaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reinsDataGridViewTextBoxColumn;
    }
}