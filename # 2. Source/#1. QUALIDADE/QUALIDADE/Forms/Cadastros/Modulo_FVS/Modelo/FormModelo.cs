﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.Modelo
{
    public partial class FormModelo : Form
    {
        private VMODELOFVS itemEdit;
        public bool salvo = false;

        public FormModelo()
        {
            InitializeComponent();
        }

        public FormModelo(VMODELOFVS modelo)
        {
            InitializeComponent();
            itemEdit = modelo;
        }

        private void FormModelo_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = itemEdit.CODIGO;
                txtNome.Text = itemEdit.NOME;
                txtRevisao.Text = itemEdit.NUMREVISAO;
                txtDataRevisao.Text = itemEdit.DTREVISAO.HasValue ? itemEdit.DTREVISAO.Value.ToString("dd/MM/yyyy") : null;
                txtAprovador.Text = itemEdit.APROVADOPOR;
                txtConformeIT.Text = itemEdit.CONFORMEIT;

                using (ControleVDescricaoModelo controle = new ControleVDescricaoModelo())
                {
                    inspecaoBindingSource.DataSource = controle.GetList(itemEdit.CODIGO);
                    dgvListagem.DataSource = inspecaoBindingSource;
                    dgvListagem.EndEdit();
                    inspecaoBindingSource.EndEdit();
                }

                txtCodigo.ReadOnly = true;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (Validacao())
            {
                VMODELOFVS item = new VMODELOFVS
                {
                    CODIGO = txtCodigo.Text,
                    NOME = txtNome.Text,
                    NUMREVISAO = txtRevisao.Text,
                    DTREVISAO = DateTime.TryParse(txtDataRevisao.Text, out DateTime dtRevisao) ? dtRevisao : (DateTime?)null,
                    APROVADOPOR = txtAprovador.Text,
                    CONFORMEIT = txtConformeIT.Text
                };

                List<VDESCRICAOMODELO> listDescModelo = new List<VDESCRICAOMODELO>();
                foreach (DataGridViewRow row in dgvListagem.Rows)
                {
                    VDESCRICAOMODELO modelo = new VDESCRICAOMODELO
                    {
                        CODMODELO = item.CODIGO,
                        DESCRICAO = row.Cells["MODELODESCRICAO"].Value.DBNullToString(),
                        ITEM = row.Cells["MODELOITEM"].Value.DBNullToString(),
                        TOLERANCIA = row.Cells["MODELOTOLERANCIA"].Value.DBNullToString(),
                    };

                    if (!string.IsNullOrEmpty(modelo.ITEM))
                        listDescModelo.Add(modelo);
                }

                using (ControleVModelo controle = new ControleVModelo())
                {
                    using (ControleVDescricaoModelo controleDesModelo = new ControleVDescricaoModelo())
                    {
                        if (itemEdit != null)
                        {
                            item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECMODIFIEDON = DateTime.Now;

                            foreach (VDESCRICAOMODELO x in listDescModelo)
                            {
                                x.RECMODIFIEDBY = item.RECMODIFIEDBY;
                                x.RECMODIFIEDON = item.RECMODIFIEDON;

                                x.RECCREATEDBY = item.RECCREATEDBY;
                                x.RECCREATEDON = item.RECCREATEDON;
                            }

                            controle.Update(item, x => x.CODIGO == item.CODIGO);
                            controle.SaveAll();

                            controleDesModelo.Delete(item.CODIGO);
                            controleDesModelo.AllCreate(listDescModelo);

                            salvo = true;
                            this.Close();
                        }
                        else
                        {
                            item.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECCREATEDON = DateTime.Now;

                            foreach (VDESCRICAOMODELO x in listDescModelo)
                            {
                                x.RECCREATEDBY = item.RECCREATEDBY;
                                x.RECCREATEDON = item.RECCREATEDON;
                            }

                            item.VDESCRICAOMODELO = null;

                            controle.Create(item);
                            controle.SaveAll();
                            controleDesModelo.AllCreate(listDescModelo);

                            salvo = true;
                            this.Close();
                        }
                    }
                }
            }
        }

        private bool Validacao()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtCodigo.Text))
                msg += " - Informe o código do modelo.\n";

            if (string.IsNullOrEmpty(txtNome.Text))
                msg += " - Informe o nome descritivo do modelo.\n";

            if (string.IsNullOrEmpty(txtAprovador.Text))
                msg += " - Informe o nome do aprovador.\n";

            if (string.IsNullOrEmpty(txtConformeIT.Text))
                msg += " - Informe o código da I.T.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }
    }
}
