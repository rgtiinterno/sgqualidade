﻿using Syncfusion.CompoundFile.XlsIO.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.Assinatura
{
    public partial class FormAssinatura : Form
    {
        private int posX, posY;
        public bool salvo = false;
        private bool movimentou = false;
        private Pen pincel;
        public Image assinatura = null;

        public FormAssinatura()
        {
            InitializeComponent();
            posX = -1;
            posY = -1;

            pincel = new Pen(Color.Black, 5);
            pincel.StartCap = pincel.EndCap = System.Drawing.Drawing2D.LineCap.Round;
        }

        private void FormAssinatura_Load(object sender, EventArgs e)
        {
            lblUsuário.Text = $"{FormPrincipal.getUsuarioAcesso().NOME} {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")}";
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            assinatura = null;

            Bitmap bmp = new Bitmap(painelAssinatura.Width, painelAssinatura.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            Rectangle rect = painelAssinatura.RectangleToScreen(painelAssinatura.ClientRectangle);
            graphics.CopyFromScreen(rect.Location, Point.Empty, painelAssinatura.Size);
            graphics.Dispose();

            assinatura = (Image)bmp;
            salvo = true;
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnRemoverAssinatura_Click(object sender, EventArgs e)
        {
            Graphics g = painelAssinatura.CreateGraphics();
            g.Clear(painelAssinatura.BackColor);
        }

        private void painelAssinatura_MouseUp(object sender, MouseEventArgs e)
        {
            movimentou = false;
            posX = -1;
            posY = -1;
        }

        private void painelAssinatura_MouseMove(object sender, MouseEventArgs e)
        {
            if (movimentou && (posX != -1 && posY != -1))
            {
                Graphics g = painelAssinatura.CreateGraphics();
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                g.DrawLine(
                    pincel,
                    new Point(posX, posY),
                    e.Location
                );

                posX = e.X;
                posY = e.Y;
            }
        }

        private void painelAssinatura_MouseDown(object sender, MouseEventArgs e)
        {
            movimentou = true;
            posX = e.X;
            posY = e.Y;
        }
    }
}
