﻿namespace QUALIDADE.Forms.Cadastros.Modulo_FVS.Assinatura
{
    partial class FormAssinatura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAssinatura));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.painelAssinatura = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblUsuário = new System.Windows.Forms.Label();
            this.btnRemoverAssinatura = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(361, 240);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(249, 240);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 8;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // painelAssinatura
            // 
            this.painelAssinatura.BackColor = System.Drawing.Color.White;
            this.painelAssinatura.Cursor = System.Windows.Forms.Cursors.Cross;
            this.painelAssinatura.Dock = System.Windows.Forms.DockStyle.Fill;
            this.painelAssinatura.Location = new System.Drawing.Point(3, 3);
            this.painelAssinatura.Name = "painelAssinatura";
            this.painelAssinatura.Size = new System.Drawing.Size(443, 155);
            this.painelAssinatura.TabIndex = 10;
            this.painelAssinatura.MouseDown += new System.Windows.Forms.MouseEventHandler(this.painelAssinatura_MouseDown);
            this.painelAssinatura.MouseMove += new System.Windows.Forms.MouseEventHandler(this.painelAssinatura_MouseMove);
            this.painelAssinatura.MouseUp += new System.Windows.Forms.MouseEventHandler(this.painelAssinatura_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(455, 200);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Assinatura";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.painelAssinatura, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblUsuário, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(449, 179);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblUsuário
            // 
            this.lblUsuário.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUsuário.Location = new System.Drawing.Point(3, 161);
            this.lblUsuário.Name = "lblUsuário";
            this.lblUsuário.Size = new System.Drawing.Size(443, 18);
            this.lblUsuário.TabIndex = 11;
            this.lblUsuário.Text = "RGTI TECNOLOGIA 27/07/2020 14:22:00";
            this.lblUsuário.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRemoverAssinatura
            // 
            this.btnRemoverAssinatura.Location = new System.Drawing.Point(12, 218);
            this.btnRemoverAssinatura.Name = "btnRemoverAssinatura";
            this.btnRemoverAssinatura.Size = new System.Drawing.Size(116, 23);
            this.btnRemoverAssinatura.TabIndex = 12;
            this.btnRemoverAssinatura.Text = "Remover Assinatura";
            this.btnRemoverAssinatura.UseVisualStyleBackColor = true;
            this.btnRemoverAssinatura.Click += new System.EventHandler(this.btnRemoverAssinatura_Click);
            // 
            // FormAssinatura
            // 
            this.AcceptButton = this.btnGravar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 279);
            this.Controls.Add(this.btnRemoverAssinatura);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAssinatura";
            this.Text = "Assinar documento";
            this.Load += new System.EventHandler(this.FormAssinatura_Load);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Panel painelAssinatura;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblUsuário;
        private System.Windows.Forms.Button btnRemoverAssinatura;
    }
}