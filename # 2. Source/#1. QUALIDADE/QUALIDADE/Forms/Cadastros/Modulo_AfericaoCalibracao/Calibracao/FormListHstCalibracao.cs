﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Calibracao
{
    public partial class FormListHstCalibracao : Form
    {
        private AAFERICAOEXTERNA item;

        public FormListHstCalibracao(AAFERICAOEXTERNA afericao)
        {
            InitializeComponent();
            item = afericao;
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"HistCalibracoes.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListHstCalibracao_Load(object sender, EventArgs e)
        {
            this.Text += $"{item.NOMEINSTRUMENTO}";

            dgvListagem.Columns?.Clear();
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "REV", HeaderText = "Rev.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCFO", HeaderText = "Cód. Fornecedor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FORNECEDOR", HeaderText = "Fornecedor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód.C.Custo.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CENTROCUSTO", HeaderText = "Centro de Custo.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "IDENTIFICACAO", HeaderText = "Identificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEINSTRUMENTO", HeaderText = "Instrumento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESULTADOBIT", HeaderText = "Resultado", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTCALIBRACAO", HeaderText = "Dt. Calibração", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TOLERANCIA", HeaderText = "Tolerancia", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "UNIDADEMEDIDA", HeaderText = "Unidade de Medida", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMCERTIFICADO", HeaderText = "Núm. Certificado", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "VALIDADE", HeaderText = "Data Validade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOCAL", HeaderText = "Processo/Local", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVELEQUIPAMENTO", HeaderText = "Resp. Equipamento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "APROVADOPOR", HeaderText = "Resp. Aprovação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleAAfericaoExterna controle = new ControleAAfericaoExterna())
            {
                Cursor.Current = Cursors.WaitCursor;
                aAFERICAOEXTERNABindingSource = new BindingSource();
                aAFERICAOEXTERNABindingSource.DataSource = typeof(AAFERICAOEXTERNA);
                aAFERICAOEXTERNABindingSource.DataSource = controle.GetListHistorico(item.CODCOLIGADA, item.CODIGO).ToList();
                dgvListagem.DataSource = aAFERICAOEXTERNABindingSource;
                aAFERICAOEXTERNABindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {aAFERICAOEXTERNABindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = true;
                    tsbListaAnexos.Enabled = true;
                }
                else
                {
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = false;
                    tsbListaAnexos.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbHistorico_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is AAFERICAOEXTERNA afericao && afericao.CKSELECIONADO)
            {
                using (FormHstCalibracao frm = new FormHstCalibracao(afericao))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
        }

        private void tsbListaAnexos_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is AAFERICAOEXTERNA calibracao)
            {
                using (FormListaAnexosCtrCalibracao frm = new FormListaAnexosCtrCalibracao(calibracao))
                {
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Selecione um registro para visualizar os Anexos.");
            }
        }

        private void dgvListagem_QueryCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryCellStyleEventArgs e)
        {
            if (e.DataRow != null && e.DataRow.RowData is AAFERICAOEXTERNA item)
            {
                if (e.Column.MappingName == "RESULTADOBIT" && item.RESULTADOBIT == true)
                    e.DisplayText = "APROVADO";
                if (e.Column.MappingName == "RESULTADOBIT" && item.RESULTADOBIT == false)
                    e.DisplayText = "REPROVADO";
            }
        }
    }
}
