﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.IO;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Calibracao
{
    public partial class FormCalibracao : Form
    {
        public bool salvo = false;
        private AAFERICAOEXTERNA itemEdit;

        public FormCalibracao(AAFERICAOEXTERNA afericao = null)
        {
            InitializeComponent();
            itemEdit = afericao;
        }

        private void FormCalibracao_Load(object sender, EventArgs e)
        {
            CarregaCombo();

            if (itemEdit == null)
            {
                using (ControleAAfericaoExterna controle = new ControleAAfericaoExterna())
                {
                    short coligada = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
                    txtCodColigada.Text = $"{coligada}";
                    txtCodigo.Text = $"{controle.GetNewID(coligada)}";
                    txtDtCalibracao.Text = $"{DateTime.Now}";
                }
            }
            else
            {
                txtCodigo.Text = $"{itemEdit.CODIGO}";
                txtCodColigada.Text = $"{itemEdit.CODCOLIGADA}";
                cbFornecedor.SelectedValue = itemEdit.CODCFO;
                txtInstrumento.Text = $"{itemEdit.NOMEINSTRUMENTO}";
                txtIdentificacao.Text = $"{itemEdit.IDENTIFICACAO}";
                txtUnidadeMedida.Text = $"{itemEdit.UNIDADEMEDIDA}";
                txtDtCalibracao.Text = $"{itemEdit.DTCALIBRACAO}";
                txtTolerancia.Text = $"{itemEdit.TOLERANCIA}";
                txtCertificado.Text = $"{itemEdit.NUMCERTIFICADO}";
                txtValidade.Text = $"{itemEdit.VALIDADE}";
                txtAprovadoPor.Text = $"{itemEdit.APROVADOPOR}";
                txtResponsavelEquipamento.Text = $"{itemEdit.RESPONSAVELEQUIPAMENTO}";
                txtProcessoLocal.Text = $"{itemEdit.LOCAL}";
                txtResultados.Text = $"{itemEdit.RESULTADO}";
                if (!string.IsNullOrEmpty(itemEdit.CODCCUSTO))
                    cbCentroCusto.SelectedValue = itemEdit.CODCCUSTO;
                else
                    cbCentroCusto.SelectedIndex = -1;
                txtIdentificacao.ReadOnly = true;

                if (itemEdit.RESULTADOBIT == true || itemEdit.RESULTADO == "APROVADO")
                {
                    ckAprovado.Checked = true;
                }
                else if (itemEdit.RESULTADOBIT == false)
                {
                    ckReprovado.Checked = true;
                }
            }
        }

        private void CarregaCombo()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                cbCentroCusto.DataSource = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "A");

                cbCentroCusto.ValueMember = "CODCCUSTO";
                cbCentroCusto.DisplayMember = "DESCCOMBO";
                cbCentroCusto.SelectedIndex = -1;
            }

            using (ControleFCFO controle = new ControleFCFO())
            {
                if (File.Exists($"{Directory.GetCurrentDirectory()}\\{Properties.Settings.Default.conexaoIntegracao}"))
                    cbFornecedor.DataSource = controle.GetIntegracaoRM();
                else
                    cbFornecedor.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);

                cbFornecedor.ValueMember = "CODCFO";
                cbFornecedor.DisplayMember = "NOMEFANTASIA";
                cbFornecedor.SelectedIndex = -1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                GCENTROCUSTO centroCusto = null;
                if (cbCentroCusto.SelectedIndex >= 0)
                    centroCusto = cbCentroCusto.SelectedItem as GCENTROCUSTO;

                bool result;
                if (ckAprovado.Checked)
                    result = true;
                else
                    result = false;

                AAFERICAOEXTERNA item = new AAFERICAOEXTERNA
                {
                    CODIGO = Convert.ToInt16(txtCodigo.Text),
                    CODCOLIGADA = Convert.ToInt16(txtCodColigada.Text),
                    CODCFO = Convert.ToString(cbFornecedor.SelectedValue),
                    NOMEINSTRUMENTO = txtInstrumento.Text,
                    IDENTIFICACAO = txtIdentificacao.Text,
                    UNIDADEMEDIDA = txtUnidadeMedida.Text,
                    DTCALIBRACAO = Convert.ToDateTime(txtDtCalibracao.Text),
                    TOLERANCIA = txtTolerancia.Text,
                    NUMCERTIFICADO = txtCertificado.Text,
                    VALIDADE = Convert.ToDateTime(txtValidade.Text),
                    APROVADOPOR = txtAprovadoPor.Text,
                    RESPONSAVELEQUIPAMENTO = txtResponsavelEquipamento.Text,
                    LOCAL = txtProcessoLocal.Text,
                    RESULTADO = " ",
                    RESULTADOBIT = result,
                    REV = (itemEdit == null) ? Convert.ToInt16(1) : Convert.ToInt16(itemEdit.REV + 1),
                    CODCOLCCUSTO = (centroCusto != null ? (short?)centroCusto.CODCOLIGADA : null),
                    CODCCUSTO = centroCusto != null ? centroCusto.CODCCUSTO : null,
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };

                using (ControleAAfericaoExterna controle = new ControleAAfericaoExterna())
                {
                    controle.Create(item);
                    controle.SaveAll();
                    salvo = true;
                    this.Close();
                }
            }
        }


        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (cbFornecedor.SelectedIndex < 0)
                msg += " - Informe o fornecedor.\n";

            if (string.IsNullOrEmpty(txtInstrumento.Text))
                msg += " - Informe o nome do instrumento.\n";

            if (string.IsNullOrEmpty(txtIdentificacao.Text))
                msg += " - Informe a identificação do instrumento.\n";

            if (string.IsNullOrEmpty(txtUnidadeMedida.Text))
                msg += " - Informe a unidade de medida do instrumento.\n";

            if (!DateTime.TryParse(txtDtCalibracao.Text, out DateTime dtCalibracao))
                msg += " - Informe uma data válida..\n";

            if (string.IsNullOrEmpty(txtTolerancia.Text))
                msg += " - Informe uma tolerância válida.\n";

            if (string.IsNullOrEmpty(txtCertificado.Text))
                msg += " - Informe um Certificado.\n";

            if (!DateTime.TryParse(txtValidade.Text, out DateTime dtValidade))
                msg += " - Informe uma data de validade do certificado que seja valida.\n";

            if (string.IsNullOrEmpty(txtResponsavelEquipamento.Text))
                msg += " - Informe o nome do responsável pelo equipamento.\n";

            if (string.IsNullOrEmpty(txtProcessoLocal.Text))
                msg += " - Informe o processo / local da calibração.\n";

            //if (string.IsNullOrEmpty(txtResultados.Text))
            //    msg += " - Informe o resultado.\n";


            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgAlerta(msg);
                return false;
            }
        }

        private void txtTolerancia_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtTolerancia_TextChanged(object sender, EventArgs e)
        {
        }

        private void ckAprovado_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAprovado.Checked)
                ckReprovado.Checked = false;
            else
                ckReprovado.Checked = true;
        }

        private void ckReprovado_CheckedChanged(object sender, EventArgs e)
        {
            if (ckReprovado.Checked)
                ckAprovado.Checked = false;
            else
                ckAprovado.Checked = true;
        }
    }
}
