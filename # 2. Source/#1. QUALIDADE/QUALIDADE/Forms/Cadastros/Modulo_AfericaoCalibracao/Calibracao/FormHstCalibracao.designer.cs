﻿namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Calibracao
{
    partial class FormHstCalibracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHstCalibracao));
            this.txtProcessoLocal = new System.Windows.Forms.TextBox();
            this.txtResponsavelEquipamento = new System.Windows.Forms.TextBox();
            this.txtAprovadoPor = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTolerancia = new System.Windows.Forms.TextBox();
            this.txtDtCalibracao = new System.Windows.Forms.MaskedTextBox();
            this.txtIdentificacao = new System.Windows.Forms.TextBox();
            this.txtInstrumento = new System.Windows.Forms.TextBox();
            this.cbFornecedor = new System.Windows.Forms.ComboBox();
            this.txtCodColigada = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtUnidadeMedida = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCertificado = new System.Windows.Forms.TextBox();
            this.txtValidade = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckReprovado = new System.Windows.Forms.CheckBox();
            this.ckAprovado = new System.Windows.Forms.CheckBox();
            this.txtResultados = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtProcessoLocal
            // 
            this.txtProcessoLocal.Location = new System.Drawing.Point(22, 266);
            this.txtProcessoLocal.Name = "txtProcessoLocal";
            this.txtProcessoLocal.Size = new System.Drawing.Size(505, 22);
            this.txtProcessoLocal.TabIndex = 12;
            // 
            // txtResponsavelEquipamento
            // 
            this.txtResponsavelEquipamento.Location = new System.Drawing.Point(276, 214);
            this.txtResponsavelEquipamento.Name = "txtResponsavelEquipamento";
            this.txtResponsavelEquipamento.Size = new System.Drawing.Size(249, 22);
            this.txtResponsavelEquipamento.TabIndex = 11;
            // 
            // txtAprovadoPor
            // 
            this.txtAprovadoPor.Location = new System.Drawing.Point(22, 214);
            this.txtAprovadoPor.Name = "txtAprovadoPor";
            this.txtAprovadoPor.Size = new System.Drawing.Size(250, 22);
            this.txtAprovadoPor.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(273, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "Responsável pelo Equipamento:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(19, 250);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Processo / Local";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(19, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Aprovado por:";
            // 
            // txtTolerancia
            // 
            this.txtTolerancia.Location = new System.Drawing.Point(128, 159);
            this.txtTolerancia.Name = "txtTolerancia";
            this.txtTolerancia.Size = new System.Drawing.Size(100, 22);
            this.txtTolerancia.TabIndex = 7;
            this.txtTolerancia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTolerancia.TextChanged += new System.EventHandler(this.txtTolerancia_TextChanged);
            this.txtTolerancia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTolerancia_KeyPress);
            // 
            // txtDtCalibracao
            // 
            this.txtDtCalibracao.Location = new System.Drawing.Point(22, 159);
            this.txtDtCalibracao.Mask = "99/99/9999";
            this.txtDtCalibracao.Name = "txtDtCalibracao";
            this.txtDtCalibracao.Size = new System.Drawing.Size(100, 22);
            this.txtDtCalibracao.TabIndex = 6;
            this.txtDtCalibracao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtIdentificacao
            // 
            this.txtIdentificacao.Location = new System.Drawing.Point(319, 97);
            this.txtIdentificacao.Name = "txtIdentificacao";
            this.txtIdentificacao.Size = new System.Drawing.Size(100, 22);
            this.txtIdentificacao.TabIndex = 4;
            // 
            // txtInstrumento
            // 
            this.txtInstrumento.Location = new System.Drawing.Point(22, 97);
            this.txtInstrumento.Name = "txtInstrumento";
            this.txtInstrumento.Size = new System.Drawing.Size(291, 22);
            this.txtInstrumento.TabIndex = 3;
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFornecedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.Location = new System.Drawing.Point(234, 40);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(293, 21);
            this.cbFornecedor.TabIndex = 2;
            // 
            // txtCodColigada
            // 
            this.txtCodColigada.Location = new System.Drawing.Point(128, 40);
            this.txtCodColigada.Name = "txtCodColigada";
            this.txtCodColigada.ReadOnly = true;
            this.txtCodColigada.Size = new System.Drawing.Size(100, 22);
            this.txtCodColigada.TabIndex = 1;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(22, 40);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(100, 22);
            this.txtCodigo.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 64;
            this.label3.Text = "Código Coligada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 65;
            this.label2.Text = "Código Interno:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(231, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Fornecedor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(316, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 67;
            this.label6.Text = "Identificação:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(125, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 68;
            this.label8.Text = "Tolerância: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 13);
            this.label7.TabIndex = 69;
            this.label7.Text = "Data da Calibração:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 70;
            this.label5.Text = "Instrumento:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(422, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Unidade de Medida:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(428, 403);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtUnidadeMedida
            // 
            this.txtUnidadeMedida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtUnidadeMedida.Location = new System.Drawing.Point(425, 97);
            this.txtUnidadeMedida.Name = "txtUnidadeMedida";
            this.txtUnidadeMedida.Size = new System.Drawing.Size(100, 22);
            this.txtUnidadeMedida.TabIndex = 5;
            this.txtUnidadeMedida.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(231, 143);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 13);
            this.label12.TabIndex = 51;
            this.label12.Text = "Núm. Certificado:";
            // 
            // txtCertificado
            // 
            this.txtCertificado.Location = new System.Drawing.Point(234, 159);
            this.txtCertificado.Name = "txtCertificado";
            this.txtCertificado.Size = new System.Drawing.Size(185, 22);
            this.txtCertificado.TabIndex = 8;
            // 
            // txtValidade
            // 
            this.txtValidade.Location = new System.Drawing.Point(425, 159);
            this.txtValidade.Mask = "99/99/9999";
            this.txtValidade.Name = "txtValidade";
            this.txtValidade.Size = new System.Drawing.Size(100, 22);
            this.txtValidade.TabIndex = 9;
            this.txtValidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(422, 143);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 51;
            this.label13.Text = "Data Validade:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(19, 301);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "Resultados:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckReprovado);
            this.groupBox1.Controls.Add(this.ckAprovado);
            this.groupBox1.Controls.Add(this.txtResultados);
            this.groupBox1.Location = new System.Drawing.Point(20, 317);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 57);
            this.groupBox1.TabIndex = 75;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultados:";
            // 
            // ckReprovado
            // 
            this.ckReprovado.AutoSize = true;
            this.ckReprovado.Location = new System.Drawing.Point(94, 17);
            this.ckReprovado.Name = "ckReprovado";
            this.ckReprovado.Size = new System.Drawing.Size(82, 17);
            this.ckReprovado.TabIndex = 15;
            this.ckReprovado.Text = "Reprovado";
            this.ckReprovado.UseVisualStyleBackColor = true;
            // 
            // ckAprovado
            // 
            this.ckAprovado.AutoSize = true;
            this.ckAprovado.Location = new System.Drawing.Point(12, 17);
            this.ckAprovado.Name = "ckAprovado";
            this.ckAprovado.Size = new System.Drawing.Size(76, 17);
            this.ckAprovado.TabIndex = 14;
            this.ckAprovado.Text = "Aprovado";
            this.ckAprovado.UseVisualStyleBackColor = true;
            // 
            // txtResultados
            // 
            this.txtResultados.Location = new System.Drawing.Point(6, 41);
            this.txtResultados.Multiline = true;
            this.txtResultados.Name = "txtResultados";
            this.txtResultados.Size = new System.Drawing.Size(492, 10);
            this.txtResultados.TabIndex = 13;
            this.txtResultados.Visible = false;
            // 
            // FormHstCalibracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 442);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtProcessoLocal);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtResponsavelEquipamento);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTolerancia);
            this.Controls.Add(this.txtAprovadoPor);
            this.Controls.Add(this.txtCertificado);
            this.Controls.Add(this.txtValidade);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtDtCalibracao);
            this.Controls.Add(this.txtUnidadeMedida);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtIdentificacao);
            this.Controls.Add(this.txtInstrumento);
            this.Controls.Add(this.cbFornecedor);
            this.Controls.Add(this.txtCodColigada);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormHstCalibracao";
            this.Text = "Calibração Externa";
            this.Load += new System.EventHandler(this.FormHstCalibracao_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtProcessoLocal;
        private System.Windows.Forms.TextBox txtResponsavelEquipamento;
        private System.Windows.Forms.TextBox txtAprovadoPor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTolerancia;
        private System.Windows.Forms.MaskedTextBox txtDtCalibracao;
        private System.Windows.Forms.TextBox txtIdentificacao;
        private System.Windows.Forms.TextBox txtInstrumento;
        private System.Windows.Forms.ComboBox cbFornecedor;
        private System.Windows.Forms.TextBox txtCodColigada;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtUnidadeMedida;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCertificado;
        private System.Windows.Forms.MaskedTextBox txtValidade;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckReprovado;
        private System.Windows.Forms.CheckBox ckAprovado;
        private System.Windows.Forms.TextBox txtResultados;
    }
}