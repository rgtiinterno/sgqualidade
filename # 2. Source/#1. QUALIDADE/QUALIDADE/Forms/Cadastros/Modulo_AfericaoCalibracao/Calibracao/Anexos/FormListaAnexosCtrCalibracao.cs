﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros
{
    public partial class FormListaAnexosCtrCalibracao : Form
    {
        private AAFERICAOEXTERNA _calibracao;

        public FormListaAnexosCtrCalibracao(AAFERICAOEXTERNA calibracao)
        {
            InitializeComponent();
            _calibracao = calibracao;
            this.Text = $"Anexos da Calibração Externa - {_calibracao.CODIGO} - Cli/For: {_calibracao.CODCFO} | Rev.: {_calibracao.REV}";
        }

        private void FormListaAnexosCtrCalibracao_Load(object sender, EventArgs e)
        {
            if (dgvListagem.Columns != null)
                dgvListagem.Columns.Clear();
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SEQUENCIAL", HeaderText = "Sequencial", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TAMANHO", HeaderText = "Tamanho", AllowEditing = false, AllowFiltering = false, Visible = false });
            CarregaDGV();
        }

        private void tsbVisualizar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is AAFERICAOEXTERNAANEXO anexo)
            {
                try
                {
                    AAFERICAOEXTERNAANEXO temp = null;
                    using (ControleAAfericaoExternaAnexo controller = new ControleAAfericaoExternaAnexo())
                    {
                        temp = controller.Find(anexo.CODAFERICAO, anexo.CODCOLIGADA, anexo.CODCFO, anexo.REV, anexo.SEQUENCIAL);
                    }

                    if (temp != null)
                    {
                        string arquivoTemp = $@"{Global.PastaTemp()}\{temp.DESCRICAO}{temp.EXTENSAO}";
                        Process.Start(Arquivo.DescriptPDF(temp.ARQUIVO, arquivoTemp));
                    }
                }
                catch (Exception ex)
                {
                    Mensagem.Informacao("Falha", "Não foi possível abrir o anexo.\r\n" + ex.Message);
                }
            }
        }

        private void CarregaDGV()
        {
            Cursor.Current = Cursors.WaitCursor;
            using (ControleAAfericaoExternaAnexo controller = new ControleAAfericaoExternaAnexo())
            {
                BindingSource anexoBindingSource = new BindingSource();
                anexoBindingSource.DataSource = typeof(AAFERICAOEXTERNAANEXO);
                var itens = controller.GetAllSemAnexo(
                    _calibracao.CODIGO, _calibracao.CODCOLIGADA,
                    _calibracao.CODCFO, _calibracao.REV
                );
                //var itens = controller.Get(x =>
                //    x.CODAFERICAO == _calibracao.CODIGO &&
                //    x.CODCOLIGADA == _calibracao.CODCOLIGADA &&
                //    x.CODCFO == _calibracao.CODCFO &&
                //    x.REV == _calibracao.REV
                //).ToList();
                anexoBindingSource.DataSource = itens;
                dgvListagem.DataSource = null;
                dgvListagem.DataSource = anexoBindingSource;
                anexoBindingSource.EndEdit();
                if(itens?.Count() > 0)
                {
                    tsbVisualizar.Enabled = true;
                    tsbRemover.Enabled = true;
                }
                else
                {
                    tsbVisualizar.Enabled = false;
                    tsbRemover.Enabled = false;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            string caminhoArquivo = Arquivo.SelecionaArquivo("Todos os arquivos (*.*)|*.*");

            if (!string.IsNullOrEmpty(caminhoArquivo))
            {
                using (ControleAAfericaoExternaAnexo controller = new ControleAAfericaoExternaAnexo())
                {
                    try
                    {
                        AAFERICAOEXTERNAANEXO anexo = new AAFERICAOEXTERNAANEXO
                        {
                            CODAFERICAO = _calibracao.CODIGO,
                            CODCOLIGADA = _calibracao.CODCOLIGADA,
                            CODCFO = _calibracao.CODCFO,
                            REV = _calibracao.REV,
                            SEQUENCIAL = controller.GetNewSequencial(_calibracao.CODIGO, _calibracao.CODCOLIGADA, _calibracao.CODCFO, _calibracao.REV),
                            ARQUIVO = Arquivo.EncriptPDF(caminhoArquivo),
                            EXTENSAO = Path.GetExtension(caminhoArquivo).ToLower(),
                            DESCRICAO = Path.GetFileNameWithoutExtension(caminhoArquivo),
                            TAMANHO = new FileInfo(caminhoArquivo).Length,
                            RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                            RECCREATEDON = DateTime.Now
                        };
                        controller.Create(anexo);
                        controller.SaveAll();
                        Mensagem.Informacao("Sucesso", "Anexo incluido com sucesso!");
                    }
                    catch (Exception ex)
                    {
                        Mensagem.Informacao("Falha", "Não foi possível adicionar o anexo.\r\n" + ex.Message);
                    }
                }
                tsbAtualizar.PerformClick();
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is AAFERICAOEXTERNAANEXO anexo)
            {
                DialogResult dr = Mensagem.Confirmacao("Confirmação", "Deseja realmente excluir o anexo selecionado?");

                if (dr == DialogResult.Yes)
                {
                    using (ControleAAfericaoExternaAnexo controller = new ControleAAfericaoExternaAnexo())
                    {
                        try
                        {
                            controller.Delete(x =>
                                x.CODAFERICAO == anexo.CODAFERICAO &&
                                x.CODCOLIGADA == anexo.CODCOLIGADA &&
                                x.CODCFO == anexo.CODCFO &&
                                x.REV == anexo.REV && 
                                x.SEQUENCIAL == anexo.SEQUENCIAL
                            );
                            controller.SaveAll();
                            Mensagem.Informacao("Sucesso", "Anexo excluído com sucesso!");
                        }
                        catch (Exception ex)
                        {
                            Mensagem.Informacao("Falha", "Não foi possível excluir o anexo.\r\n" + ex.Message);
                        }
                    }
                    tsbAtualizar.PerformClick();
                }
            }
            else
            {
                Mensagem.Alerta("Alerta", "Selecione um anexo para realizar a exclusão.");
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbVisualizar.PerformClick();
        }
    }
}
