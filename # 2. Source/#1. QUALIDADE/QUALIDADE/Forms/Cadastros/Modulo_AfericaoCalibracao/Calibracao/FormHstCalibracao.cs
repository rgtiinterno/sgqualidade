﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Calibracao
{
    public partial class FormHstCalibracao : Form
    {
        public bool salvo = false;
        private AAFERICAOEXTERNA itemEdit;

        public FormHstCalibracao(AAFERICAOEXTERNA afericao)
        {
            InitializeComponent();
            itemEdit = afericao;
        }

        private void FormHstCalibracao_Load(object sender, EventArgs e)
        {
            CarregaCombo();
            txtCodigo.Text = $"{itemEdit.CODIGO}";
            txtCodColigada.Text = $"{itemEdit.CODCOLIGADA}";
            cbFornecedor.SelectedValue = itemEdit.CODCFO;
            txtInstrumento.Text = $"{itemEdit.NOMEINSTRUMENTO}";
            txtIdentificacao.Text = $"{itemEdit.IDENTIFICACAO}";
            txtUnidadeMedida.Text = $"{itemEdit.UNIDADEMEDIDA}";
            txtDtCalibracao.Text = $"{itemEdit.DTCALIBRACAO}";
            txtTolerancia.Text = $"{itemEdit.TOLERANCIA}";
            txtCertificado.Text = $"{itemEdit.NUMCERTIFICADO}";
            txtValidade.Text = $"{itemEdit.VALIDADE}";
            txtAprovadoPor.Text = $"{itemEdit.APROVADOPOR}";
            txtResponsavelEquipamento.Text = $"{itemEdit.RESPONSAVELEQUIPAMENTO}";
            txtProcessoLocal.Text = $"{itemEdit.LOCAL}";
            txtResultados.Text = $"{itemEdit.RESULTADO}";

            if (itemEdit.RESULTADOBIT == true)
            {
                ckAprovado.Checked = true;
            }
            else
            {
                ckReprovado.Checked = true;
            }

            ckAprovado.Enabled = false;
            ckReprovado.Enabled = false;
            txtCodigo.ReadOnly = true;
            txtCodColigada.ReadOnly = true;
            cbFornecedor.Enabled = false;
            txtInstrumento.ReadOnly = true;
            txtIdentificacao.ReadOnly = true;
            txtUnidadeMedida.ReadOnly = true;
            txtDtCalibracao.ReadOnly = true;
            txtTolerancia.ReadOnly = true;
            txtCertificado.ReadOnly = true;
            txtValidade.ReadOnly = true;
            txtAprovadoPor.ReadOnly = true;
            txtResponsavelEquipamento.ReadOnly = true;
            txtProcessoLocal.ReadOnly = true;
            txtResultados.ReadOnly = true;
        }

        private void CarregaCombo()
        {
            using (ControleFCFO controle = new ControleFCFO())
            {
                cbFornecedor.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                cbFornecedor.ValueMember = "CODCFO";
                cbFornecedor.DisplayMember = "NOMEFANTASIA";
                cbFornecedor.SelectedIndex = -1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtTolerancia_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }

        private void txtTolerancia_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTolerancia.Text))
            {
                if (!string.IsNullOrEmpty(txtDtCalibracao.Text) && Global.ValidaData(txtDtCalibracao.Text))
                    txtValidade.Text = $"{Convert.ToDateTime(txtDtCalibracao.Text).AddDays(Convert.ToInt32(txtTolerancia.Text))}";
            }
            else
                txtValidade.Text = string.Empty;
        }

    }
}
