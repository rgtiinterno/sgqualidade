﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao
{
    public partial class FormHstAfericao : Form
    {
        public bool salvo = false;
        private AAFERICAOINTERNA itemEdit;

        public FormHstAfericao(AAFERICAOINTERNA afericao)
        {
            InitializeComponent();
            itemEdit = afericao;
        }

        private void FormHstAfericao_Load(object sender, EventArgs e)
        {
            CarregaCombo();
            this.Text += $" < {itemEdit.CODCFO} - {itemEdit.FORNECEDOR} >";

            txtCodigo.Text = $"{itemEdit.CODIGO}";
            txtCodColigada.Text = $"{itemEdit.CODCOLIGADA}";
            cbFornecedor.SelectedValue = itemEdit.CODCFO;
            cbFornecedor.Text = itemEdit.FORNECEDOR;
            txtInstrumento.Text = $"{itemEdit.NOMEINSTRUMENTO}";
            txtIdentificacao.Text = $"{itemEdit.IDENTIFICACAO}";
            cbFrequencia.Text = itemEdit.FRENQUENCIA;
            txtDtAfericao.Text = $"{itemEdit.DTAFERICAO}";
            txtValidade.Text = $"{itemEdit.VALIDADE:dd/MM/yyyy}";
            txtResponsvelAfericao.Text = $"{itemEdit.RESPONSAVELAFERICAO}";
            txtResponsavelEquipamento.Text = $"{itemEdit.RESPONSAVELEQUIPAMENTO}";
            txtProcessoLocal.Text = $"{itemEdit.LOCAL}";
            ckConforme.Checked = itemEdit.RESULTADO ? true : false;
            ckNaoConforme.Checked = !ckConforme.Checked;

            txtCodigo.ReadOnly = true;
            txtCodColigada.ReadOnly = true;
            txtInstrumento.ReadOnly = true;
            txtIdentificacao.ReadOnly = true;
            txtDtAfericao.ReadOnly = true;
            txtValidade.ReadOnly = true;
            txtResponsvelAfericao.ReadOnly = true;
            txtResponsavelEquipamento.ReadOnly = true;
            txtProcessoLocal.ReadOnly = true;
            cbFornecedor.Enabled = false;
            cbFrequencia.Enabled = false;
            ckConforme.Enabled = false;
            ckNaoConforme.Enabled = false;
        }

        private void CarregaCombo()
        {
            using (ControleFCFO controle = new ControleFCFO())
            {
                cbFornecedor.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                cbFornecedor.ValueMember = "CODCFO";
                cbFornecedor.DisplayMember = "NOMEFANTASIA";
                cbFornecedor.SelectedIndex = -1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void cbFrequencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFrequencia.SelectedIndex >= 0)
            {
                if (DateTime.TryParse(txtDtAfericao.Text, out DateTime dtAfericao))
                {
                    DateTime hoje = DateTime.Now;
                    if (cbFrequencia.SelectedIndex == 0)
                        txtValidade.Text = $"{Convert.ToInt16(dtAfericao.AddDays(8).Subtract(hoje).TotalDays)}";
                    else if (cbFrequencia.SelectedIndex == 1)
                        txtValidade.Text = $"{Convert.ToInt16(dtAfericao.AddDays(16).Subtract(hoje).TotalDays)}";
                    else if (cbFrequencia.SelectedIndex == 2)
                        txtValidade.Text = $"{Convert.ToInt16(dtAfericao.AddMonths(1).AddDays(1).Subtract(hoje).TotalDays)}";
                    else if (cbFrequencia.SelectedIndex == 3)
                        txtValidade.Text = $"{Convert.ToInt16(dtAfericao.AddYears(1).AddDays(1).Subtract(hoje).TotalDays)}";
                }
            }
        }

        private void ckConforme_CheckedChanged(object sender, EventArgs e)
        {
            if (ckConforme.Checked)
                ckNaoConforme.Checked = false;
            else
                ckNaoConforme.Checked = true;
        }

        private void ckNaoConforme_CheckedChanged(object sender, EventArgs e)
        {
            if (ckNaoConforme.Checked)
                ckConforme.Checked = false;
            else
                ckConforme.Checked = true;
        }
    }
}
