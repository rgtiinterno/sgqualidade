﻿namespace QUALIDADE
{
    partial class FormAddLeitura070
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddLeitura070));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtObservacoes = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNaoConforme = new System.Windows.Forms.RadioButton();
            this.rbConforme = new System.Windows.Forms.RadioButton();
            this.txtDesvioToleravel = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDesvioEncontrado = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLeituraDispositivo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLeituraPadrao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtObservacoes);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtDesvioToleravel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDesvioEncontrado);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtLeituraDispositivo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtLeituraPadrao);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(541, 268);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações:";
            // 
            // txtObservacoes
            // 
            this.txtObservacoes.Location = new System.Drawing.Point(20, 154);
            this.txtObservacoes.Name = "txtObservacoes";
            this.txtObservacoes.Size = new System.Drawing.Size(361, 96);
            this.txtObservacoes.TabIndex = 10;
            this.txtObservacoes.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Observações:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbNaoConforme);
            this.groupBox2.Controls.Add(this.rbConforme);
            this.groupBox2.Location = new System.Drawing.Point(387, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(132, 90);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resultado:";
            // 
            // rbNaoConforme
            // 
            this.rbNaoConforme.AutoSize = true;
            this.rbNaoConforme.Location = new System.Drawing.Point(23, 57);
            this.rbNaoConforme.Name = "rbNaoConforme";
            this.rbNaoConforme.Size = new System.Drawing.Size(93, 17);
            this.rbNaoConforme.TabIndex = 1;
            this.rbNaoConforme.TabStop = true;
            this.rbNaoConforme.Text = "Não Conforme";
            this.rbNaoConforme.UseVisualStyleBackColor = true;
            // 
            // rbConforme
            // 
            this.rbConforme.AutoSize = true;
            this.rbConforme.Location = new System.Drawing.Point(23, 25);
            this.rbConforme.Name = "rbConforme";
            this.rbConforme.Size = new System.Drawing.Size(70, 17);
            this.rbConforme.TabIndex = 0;
            this.rbConforme.TabStop = true;
            this.rbConforme.Text = "Conforme";
            this.rbConforme.UseVisualStyleBackColor = true;
            // 
            // txtDesvioToleravel
            // 
            this.txtDesvioToleravel.Location = new System.Drawing.Point(279, 100);
            this.txtDesvioToleravel.MaxLength = 50;
            this.txtDesvioToleravel.Name = "txtDesvioToleravel";
            this.txtDesvioToleravel.Size = new System.Drawing.Size(240, 20);
            this.txtDesvioToleravel.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(276, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Desvio Tolerável:";
            // 
            // txtDesvioEncontrado
            // 
            this.txtDesvioEncontrado.Location = new System.Drawing.Point(20, 100);
            this.txtDesvioEncontrado.MaxLength = 50;
            this.txtDesvioEncontrado.Name = "txtDesvioEncontrado";
            this.txtDesvioEncontrado.Size = new System.Drawing.Size(240, 20);
            this.txtDesvioEncontrado.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Desvio Encontrado:";
            // 
            // txtLeituraDispositivo
            // 
            this.txtLeituraDispositivo.Location = new System.Drawing.Point(279, 50);
            this.txtLeituraDispositivo.MaxLength = 50;
            this.txtLeituraDispositivo.Name = "txtLeituraDispositivo";
            this.txtLeituraDispositivo.Size = new System.Drawing.Size(240, 20);
            this.txtLeituraDispositivo.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(276, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Leitura no Dispositivo (Trena de Campo):";
            // 
            // txtLeituraPadrao
            // 
            this.txtLeituraPadrao.Location = new System.Drawing.Point(20, 50);
            this.txtLeituraPadrao.MaxLength = 50;
            this.txtLeituraPadrao.Name = "txtLeituraPadrao";
            this.txtLeituraPadrao.Size = new System.Drawing.Size(240, 20);
            this.txtLeituraPadrao.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Leitura no Padrão (Trena Calibrado):";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.Location = new System.Drawing.Point(397, 288);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 1;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(478, 288);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FormAddLeitura070
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 323);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormAddLeitura070";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Verificação da Trena";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox txtObservacoes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbNaoConforme;
        private System.Windows.Forms.RadioButton rbConforme;
        private System.Windows.Forms.TextBox txtDesvioToleravel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDesvioEncontrado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLeituraDispositivo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLeituraPadrao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCancelar;
    }
}