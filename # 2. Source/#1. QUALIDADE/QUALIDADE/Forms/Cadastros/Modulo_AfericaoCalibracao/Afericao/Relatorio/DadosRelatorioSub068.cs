﻿namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio
{
    internal class DadosRelatorioSub068
    {
        public int Codigo { get; set; }
        public int CodAfericao { get; set; }
        public int CodColigada { get; set; }
        public string CodFornecedor { get; set; }
        public int Revisao { get; set; }
        public string LeituraLado01 { get; set; }
        public string LeituraLado02 { get; set; }
        public string LeituraHipotenusa { get; set; }
        public string DesvioToleravel { get; set; }
        public string Resultado { get; set; }
        public string Observacoes { get; set; }
    }
}