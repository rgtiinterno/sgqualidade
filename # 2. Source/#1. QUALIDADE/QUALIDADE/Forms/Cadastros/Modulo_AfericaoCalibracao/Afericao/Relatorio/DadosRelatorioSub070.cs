﻿namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio
{
    internal class DadosRelatorioSub070
    {
        public int Codigo { get; set; }
        public int CodAfericao { get; set; }
        public int CodColigada { get; set; }
        public string CodFornecedor { get; set; }
        public int Revisao { get; set; }
        public string LeituraPadrao { get; set; }
        public string LeituraDispositivo { get; set; }
        public string DesvioEncontrado { get; set; }
        public string DesvioToleravel { get; set; }
        public string Resultado { get; set; }
        public string Observacoes { get; set; }
    }
}