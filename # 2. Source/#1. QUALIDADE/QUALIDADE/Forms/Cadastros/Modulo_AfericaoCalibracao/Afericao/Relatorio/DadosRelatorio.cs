﻿using System;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio
{
    public class DadosRelatorio
    {
        public int CodAfericao { get; set; }
        public int CodColigada { get; set; }
        public string NomeColigada { get; set; }
        public byte[] LogoColigada { get; set; }
        public string CodFornecedor { get; set; }
        public string NomeFornecedor { get; set; }
        public int Revisao { get; set; }
        public string NumContrato { get; set; }
        public string CodTipoAfericao { get; set; }
        public string DescTipoAfericao { get; set; }
        public string NomeInstrumento { get; set; }
        public string Identificacao { get; set; }
        public int Resultado { get; set; }
        public string ResultadoStr { get; set; }
        public DateTime DataAfericao { get; set; }
        public string Frequencia { get; set; }
        public DateTime DataValidade { get; set; }
        public string ResponsavelAfericao { get; set; }
        public string Local { get; set; }
        public string ResponsavelEquipamento { get; set; }
        public short? CodColCCusto { get; set; }
        public string CodCCusto { get; set; }
        public string NomeCCusto { get; set; }
        public string Tolerancia { get; set; }
        public string UndMedida { get; set; }
        public bool Ativo { get; set; }
        public string Observacoes { get; set; }
        public bool EquipFuncionando { get; set; }
        public bool EquipLimpo { get; set; }
        public bool EquipBemCondicionado { get; set; }
        public bool EquipDanoAparente { get; set; }
        public bool Form069BolhaCentralizada { get; set; }
        public bool Form069LocalNivelado { get; set; }
        public string CalibracaoEquipamento { get; set; }
        public string CalibracaoIdentificacao { get; set; }
        public string CalibracaoLaudo { get; set; }
        public DateTime? CalibracaoData { get; set; }
        public DateTime? CalibracaoValidade { get; set; }
    }
}