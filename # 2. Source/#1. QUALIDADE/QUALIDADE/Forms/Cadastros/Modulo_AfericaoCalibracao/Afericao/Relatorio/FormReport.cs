﻿using Microsoft.Reporting.WinForms;
using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio
{
    public partial class FormReport : Form
    {
        private string _codTipoAfericao;
        private List<AAFERICAOINTERNA> _afericoes = null;
        private ControleAAfericaoInterna _controller = null;

        public FormReport(List<AAFERICAOINTERNA> afericoes, string codTipoAfericao)
        {
            InitializeComponent();
            _controller = new ControleAAfericaoInterna();
            _codTipoAfericao = codTipoAfericao;
            _afericoes = afericoes;
            this.Text = $"[Relatório] Verificação Interna - {afericoes?.First()?.DESCTIPOAFERICAO}";
            //if (codTipoAfericao == "068")
            //    this.reportViewer1.LocalReport.ReportEmbeddedResource = "QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio.RelVerifInterna068.rdlc";
            //else if (codTipoAfericao == "069")
            //    this.reportViewer1.LocalReport.ReportEmbeddedResource = "QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio.RelVerifInterna069.rdlc";
            //else if (codTipoAfericao == "070")
            //    this.reportViewer1.LocalReport.ReportEmbeddedResource = "QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio.RelVerifInterna070.rdlc";
            //else
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio.RelVerifInterna.rdlc";
            this.reportViewer1.LocalReport.DataSources?.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet", CarregaDados()));
            //this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDados", CarregaDados()));
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            this.reportViewer1.ZoomMode = ZoomMode.Percent;
            this.reportViewer1.ZoomPercent = 100;
            this.reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubreportProcessing);
            this.reportViewer1.RefreshReport();
        }

        private List<DadosRelatorio> CarregaDados()
        {
            GEMPRESAS coligada = null;
            using (ControleGEmpresas controle = new ControleGEmpresas())
            {
                coligada = controle.Find(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
            }

            List<DadosRelatorio> itens = new List<DadosRelatorio>();
            foreach (AAFERICAOINTERNA afericao in _afericoes)
            {
                itens.Add(new DadosRelatorio
                {
                    CodAfericao = afericao.CODIGO,
                    CodColigada = afericao.CODCOLIGADA,
                    NomeColigada = coligada?.NOME,
                    LogoColigada = coligada?.LOGO,
                    CodFornecedor = afericao.CODCFO,
                    NomeFornecedor = null, //CONFIRMAR / CAMPO CLIENTE? VOLTAR A EXIBIR NO FORM DE VERIFICAÇÃO ??
                    Revisao = afericao.REV,
                    CodTipoAfericao = afericao.TIPOAFERICAO,
                    DescTipoAfericao = afericao.DESCTIPOAFERICAO,
                    NomeInstrumento = afericao.NOMEINSTRUMENTO,
                    Identificacao = afericao.IDENTIFICACAO,
                    Resultado = afericao.RESULTADO ? 1 : 0,
                    ResultadoStr = afericao.RESULTADOSTR,
                    DataAfericao = afericao.DTAFERICAO,
                    Frequencia = afericao.FRENQUENCIA,
                    DataValidade = afericao.VALIDADE,
                    ResponsavelAfericao = afericao.RESPONSAVELAFERICAO,
                    Local = afericao.LOCAL,
                    ResponsavelEquipamento = afericao.RESPONSAVELEQUIPAMENTO,
                    CodColCCusto = afericao.CODCOLCCUSTO,
                    CodCCusto = afericao.CODCCUSTO,
                    NomeCCusto = afericao.CENTROCUSTO,
                    NumContrato = afericao.NUMCONTRATO, 
                    Tolerancia = afericao.TOLERANCIA,
                    UndMedida = afericao.UNDMEDIDA,
                    Ativo = afericao.ATIVO,
                    Observacoes = afericao.OBSERVACOES,
                    EquipFuncionando = afericao.EQUIPFUNCIONANDO,
                    EquipLimpo = afericao.EQUIPLIMPO,
                    EquipBemCondicionado = afericao.EQUIPBEMACOND,
                    EquipDanoAparente = afericao.EQUIPDANOAPARENTE,
                    Form069BolhaCentralizada = afericao.FORM069BOLHACENTRALIZADA,
                    Form069LocalNivelado = afericao.FORM069LOCALNIVELADO,
                  
                    CalibracaoEquipamento = null, //CONFIRMAR
                    CalibracaoIdentificacao = null, //CONFIRMAR
                    CalibracaoLaudo = null, //CONFIRMAR
                    CalibracaoData = null, //CONFIRMAR
                    CalibracaoValidade = null //CONFIRMAR
                });
            }
            return itens;
        }

        private void SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            var parametros = e.Parameters;
            int codAfericao = parametros["CodAfericao"].Values[0].DBNullToInt() ?? 0;
            int codColigada = parametros["CodColigada"].Values[0].DBNullToInt() ?? 0;
            string codFornecedor = parametros["CodFornecedor"].Values[0].DBNullToString();
            int rev = parametros["Rev"].Values[0].DBNullToInt() ?? 0;

            if (_codTipoAfericao == "068" || e.ReportPath == "SubRelLeituras068")
            {
                List<DadosRelatorioSub068> itens = new List<DadosRelatorioSub068>();
                DataTable dt = _controller.GetDataTable(
                    @"SELECT * FROM AAFERICAOINT068LEITURA 
                    WHERE CODAFERICAO=@CODAFERICAO AND CODCOLIGADA=@CODCOLIGADA 
                      AND CODCFO=@CODCFO AND REV=@REV ", new List<SqlParameter>
                    {
                        new SqlParameter("@CODAFERICAO", codAfericao),
                        new SqlParameter("@CODCOLIGADA", codColigada),
                        new SqlParameter("@CODCFO", codFornecedor),
                        new SqlParameter("@REV", rev),
                    }
                );

                if (dt?.Rows?.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        itens.Add(new DadosRelatorioSub068
                        {
                            Codigo = row["CODIGO"].DBNullToInt() ?? 0,
                            CodAfericao = row["CODAFERICAO"].DBNullToInt() ?? 0,
                            CodColigada = row["CODCOLIGADA"].DBNullToInt() ?? 0,
                            CodFornecedor = row["CODCFO"].DBNullToString(),
                            Revisao = row["REV"].DBNullToInt() ?? 0,
                            LeituraLado01 = row["LEITURALADO01"].DBNullToString(),
                            LeituraLado02 = row["LEITURALADO02"].DBNullToString(),
                            LeituraHipotenusa = row["LEITURAHIPOTENUSA"].DBNullToString(),
                            DesvioToleravel = row["DESVIOTOLERAVEL"].DBNullToString(),
                            Resultado = (row["RESULTADO"].DBNullToInt() ?? 0) == 0 ? "NC" : "C",
                            Observacoes = row["OBSERVACOES"].DBNullToString(),
                        });
                    }
                }

                if (itens.Count < 7)
                {
                    while (itens.Count < 7)
                    {
                        itens.Add(new DadosRelatorioSub068());
                    }
                }
                e.DataSources.Add(new ReportDataSource("dsLista", itens));
            }
            else if (_codTipoAfericao == "070" || e.ReportPath == "SubRelLeituras070")
            {
                List<DadosRelatorioSub070> itens = new List<DadosRelatorioSub070>();
                DataTable dt = _controller.GetDataTable(
                    @"SELECT * FROM AAFERICAOINT070LEITURA 
                    WHERE CODAFERICAO=@CODAFERICAO AND CODCOLIGADA=@CODCOLIGADA 
                      AND CODCFO=@CODCFO AND REV=@REV ", new List<SqlParameter>
                    {
                        new SqlParameter("@CODAFERICAO", codAfericao),
                        new SqlParameter("@CODCOLIGADA", codColigada),
                        new SqlParameter("@CODCFO", codFornecedor),
                        new SqlParameter("@REV", rev),
                    }
                );

                if (dt?.Rows?.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        itens.Add(new DadosRelatorioSub070
                        {
                            Codigo = row["CODIGO"].DBNullToInt() ?? 0,
                            CodAfericao = row["CODAFERICAO"].DBNullToInt() ?? 0,
                            CodColigada = row["CODCOLIGADA"].DBNullToInt() ?? 0,
                            CodFornecedor = row["CODCFO"].DBNullToString(),
                            Revisao = row["REV"].DBNullToInt() ?? 0,
                            LeituraPadrao = row["LEITURAPADRAO"].DBNullToString(),
                            LeituraDispositivo = row["LEITURADISPOSITIVO"].DBNullToString(),
                            DesvioEncontrado = row["DESVIOENCONTRADO"].DBNullToString(),
                            DesvioToleravel = row["DESVIOTOLERAVEL"].DBNullToString(),
                            Resultado = (row["RESULTADO"].DBNullToInt() ?? 0) == 0 ? "NC" : "C",
                            Observacoes = row["OBSERVACOES"].DBNullToString(),
                        });
                    }
                }

                if (itens.Count < 7)
                {
                    while (itens.Count < 7)
                    {
                        itens.Add(new DadosRelatorioSub070());
                    }
                }
                e.DataSources.Add(new ReportDataSource("dsLista", itens));
            }
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            //AAFERICAOINTERNA afericao = new AAFERICAOINTERNA();
            //using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
            //{
            //    afericao = controle.Get(x => x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA && x.CODCFO == _codcfo 
            //    && x.REV == _rev && x.CODIGO == _codigo).FirstOrDefault();
            //}

            //DadosRelatorio item = new DadosRelatorio();
            //using (ControleGEmpresas controle = new ControleGEmpresas())
            //{
            //    item.Logo = controle.Find(afericao.CODCOLIGADA).LOGO;
            //}
            //item.CodCCusto = afericao.CODCCUSTO;
            //item.CodColigada = afericao.CODCOLIGADA;
            //item.Codigo = afericao.CODIGO;
            //item.DtAfericao = afericao.DTAFERICAO;
            //item.Frequencia = afericao.FRENQUENCIA;
            //item.Identificacao = afericao.IDENTIFICACAO;
            //item.Local = afericao.LOCAL;
            //item.NomeInstrumento = afericao.NOMEINSTRUMENTO;
            //item.ResponsavelAfericao = afericao.RESPONSAVELAFERICAO;
            //item.ResponsavelEquipamento = afericao.RESPONSAVELEQUIPAMENTO;
            //item.Resultado = afericao.RESULTADO;
            //item.Validade = afericao.VALIDADE;

            //BindingSource bs = new BindingSource();
            //bs.DataSource = item;

            //ReportDataSource rds = new ReportDataSource();
            //rds.Name = "DataSet";
            //rds.Value = bs;
            //this.reportViewer1.LocalReport.DataSources.Clear();
            //this.reportViewer1.LocalReport.DataSources.Add(rds);
            //this.reportViewer1.RefreshReport();
        }


    }
}
