﻿using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE
{
    public partial class FormAddLeitura070 : Form
    {
        public AAFERICAOINT070LEITURA _itemLeitura = null;
        public bool _salvo = false;

        public FormAddLeitura070(AAFERICAOINT070LEITURA itemEdit = null)
        {
            InitializeComponent();
            _itemLeitura = itemEdit;
            if (itemEdit != null)
                PreencheForm();
        }

        private void PreencheForm()
        {
            txtLeituraPadrao.Text = _itemLeitura.LEITURAPADRAO;
            txtLeituraDispositivo.Text = _itemLeitura.LEITURADISPOSITIVO;
            txtDesvioEncontrado.Text = _itemLeitura.DESVIOENCONTRADO;
            txtDesvioToleravel.Text = _itemLeitura.DESVIOTOLERAVEL;
            txtObservacoes.Text = _itemLeitura.OBSERVACOES;
            rbConforme.Checked = _itemLeitura.RESULTADO == 1;
            rbNaoConforme.Checked = _itemLeitura.RESULTADO != 1;
        }

        private bool ValidaForm()
        {
            string msgErro = string.Empty;

            if (string.IsNullOrWhiteSpace(txtLeituraPadrao.Text))
                msgErro += $"\r\n - Informe a Leitura no Padrão";

            if (string.IsNullOrWhiteSpace(txtLeituraDispositivo.Text))
                msgErro += $"\r\n - Informe a Leitura no Dispositivo";

            if (string.IsNullOrWhiteSpace(txtDesvioEncontrado.Text))
                msgErro += $"\r\n - Informe o Desvio Encontrado";

            if (string.IsNullOrWhiteSpace(txtLeituraPadrao.Text))
                msgErro += $"\r\n - Informe o Desvio Tolerável";

            if (!rbConforme.Checked && !rbNaoConforme.Checked)
                msgErro += $"\r\n - Informe o Resultado";

            bool valido = string.IsNullOrEmpty(msgErro);
            if(!valido)
            {
                Mensagem.Alerta(
                    "Validação",
                    $"Falha na validação dos dados:\r\n{msgErro}"
                );
            }
            return valido;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if(ValidaForm())
            {
                _itemLeitura = new AAFERICAOINT070LEITURA
                {
                    LEITURAPADRAO = txtLeituraPadrao.Text.Trim(), 
                    LEITURADISPOSITIVO = txtLeituraDispositivo.Text.Trim(), 
                    DESVIOENCONTRADO = txtDesvioEncontrado.Text.Trim(), 
                    DESVIOTOLERAVEL = txtDesvioToleravel.Text.Trim(), 
                    OBSERVACOES = txtObservacoes.Text.Trim(),
                    RESULTADO = (short)(rbConforme.Checked ? 1 : 0)
                };
                _salvo = true;
                Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            _itemLeitura = null;
            _salvo = false;
            Close();
        }
    }
}