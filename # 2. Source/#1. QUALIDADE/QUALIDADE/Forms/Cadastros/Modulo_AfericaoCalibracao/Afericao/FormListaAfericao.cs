﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao.Relatorio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Enums;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao
{
    public partial class FormListaAfericao : Form
    {
        public FormListaAfericao()
        {
            InitializeComponent();
        }

        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Aferições.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }

        private void FormListaAfericao_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns?.Clear();
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "REV", HeaderText = "Rev.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCFO", HeaderText = "Cód. Fornecedor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FORNECEDOR", HeaderText = "Fornecedor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCTIPOAFERICAO", HeaderText = "Tipo de Verificação", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód.C.Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CENTROCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "IDENTIFICACAO", HeaderText = "Identificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEINSTRUMENTO", HeaderText = "Instrumento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "ATIVO", HeaderText = "Ativa", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "RESULTADO", HeaderText = "Em Conformidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTAFERICAO", HeaderText = "Dt. Verificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FRENQUENCIA", HeaderText = "Frequência", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "VALIDADE", HeaderText = "Data Validade.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVELAFERICAO", HeaderText = "Resp. Verificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVELEQUIPAMENTO", HeaderText = "Resp. Equipamento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOCAL", HeaderText = "Processo/Local", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TOLERANCIA", HeaderText = "Tolerância", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "UNDMEDIDA", HeaderText = "Unidade Medida", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
            {
                Cursor.Current = Cursors.WaitCursor;
                aAFERICAOINTERNABindingSource = new BindingSource();
                aAFERICAOINTERNABindingSource.DataSource = typeof(AAFERICAOINTERNA);
                aAFERICAOINTERNABindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                dgvListagem.DataSource = aAFERICAOINTERNABindingSource;
                aAFERICAOINTERNABindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {aAFERICAOINTERNABindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null && dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = true;
                    tsbListaAnexos.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = false;
                    tsbListaAnexos.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormAfericao frm = new FormAfericao())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is AAFERICAOINTERNA afericao && afericao.CKSELECIONADO)
            {
                using (FormAfericao frm = new FormAfericao(afericao))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione um registro para efetuar a edição do mesmo.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (dgvListagem.SelectedItem is AAFERICAOINTERNA afeicao && afeicao.CKSELECIONADO)
            {
                try
                {
                    using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                    {
                        controle.Delete(x => 
                            x.CODCOLIGADA == afeicao.CODCOLIGADA &&
                            x.CODCFO == afeicao.CODCFO && 
                            x.CODIGO == afeicao.CODIGO
                        );
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void tsbHistorico_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is AAFERICAOINTERNA afericao && afericao.CKSELECIONADO)
            {
                FormListHstAfericao form = new FormListHstAfericao(afericao);
                form.MdiParent = this.MdiParent;
                form.Show();
            }
            else
                Global.MsgErro(" - Selecione um registro para abrir o histórico do mesmo.");
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            DateTime hoje = DateTime.Now;

            if (e.RowType == RowType.DefaultRow)
            {
                if (e.RowData is AAFERICAOINTERNA item)
                {
                    if (item.ATIVO)
                    {
                        int totalDiasFaltantes = Convert.ToInt32(item.VALIDADE.Subtract(hoje).TotalDays);

                        //if (item.RESULTADO && item.RESULTADOSTR == "APROVADO")
                        //    e.Style.BackColor = Color.DarkSeaGreen;
                        //else if (!item.RESULTADO && item.RESULTADOSTR == "REAPROVADO")
                        //    e.Style.BackColor = Color.LightCoral;
                        //else
                        if (totalDiasFaltantes > 0 && totalDiasFaltantes <= 7)
                            e.Style.BackColor = Color.LightGoldenrodYellow;
                        else if (totalDiasFaltantes < 0)
                            e.Style.BackColor = Color.LightCoral;
                        else
                            e.Style.BackColor = Color.DarkSeaGreen;
                    }
                    else
                    {
                        e.Style.BackColor = Color.LightGray;
                    }
                }
            }
        }

        private void tsbListaAnexos_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is AAFERICAOINTERNA afericao)
            {
                using (FormListaAnexosCtrAfericao frm = new FormListaAnexosCtrAfericao(afericao))
                {
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Selecione um registro para visualizar os Anexos.");
            }
        }

        private void tsbRelatorio_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            List<AAFERICAOINTERNA> itensSelecionados = new List<AAFERICAOINTERNA>();
            for (int i = 0; i < dgvListagem.View.Records.Count; i++)
            {
                if (dgvListagem.View.Records[i].Data is AAFERICAOINTERNA item && item.CKSELECIONADO)
                    itensSelecionados.Add(item);
            }
            Cursor.Current = Cursors.Default;

            if (itensSelecionados.Count <= 0)
            {
                Global.MsgErro(" - Selecione ao menos um registro para gerar o relatório.");
            }
            else
            {
                var grupos = itensSelecionados.GroupBy(x => x.TIPOAFERICAO).Select(x => new { 
                    Itens = x.ToList(),
                    x.Key
                }).ToList();

                foreach (var grupo in grupos)
                {
                    FormReport formReport = new FormReport(grupo.Itens, grupo.Key);
                    formReport.MdiParent = this.MdiParent;
                    formReport.Show();
                }
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }

        private void tsbInativar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            List<AAFERICAOINTERNA> itensSelecionados = new List<AAFERICAOINTERNA>();
            for (int i = 0; i < dgvListagem.View.Records.Count; i++)
            {
                if (dgvListagem.View.Records[i].Data is AAFERICAOINTERNA item && item.CKSELECIONADO)
                    itensSelecionados.Add(item);
            }

            if (itensSelecionados.Count <= 0)
            {
                Mensagem.Confirmacao("Confirmação", "Selecione ao menos um registro para ativar/inativar.");
            }
            else
            {
                DialogResult dr = Mensagem.Confirmacao("Confirmação", "Confirmar ativação/inativação dos itens selecionados?");
                if (dr == DialogResult.Yes)
                {
                    bool atualizado = false;
                    using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                    {
                        foreach (AAFERICAOINTERNA item in itensSelecionados)
                        {
                            try
                            {
                                item.ATIVO = !item.ATIVO;
                                controle.Update(item, x =>
                                    x.CODIGO == item.CODIGO &&
                                    x.CODCOLIGADA == item.CODCOLIGADA &&
                                    x.CODCFO == item.CODCFO &&
                                    x.REV == item.REV
                                );
                                controle.SaveAll();
                                atualizado = true;
                            }
                            catch (Exception ex)
                            {
                                using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                                {
                                    frm.StartPosition = FormStartPosition.CenterParent;
                                    frm.WindowState = FormWindowState.Normal;
                                    frm.ShowDialog();
                                }
                            }
                        }
                    }
                    if (atualizado)
                        tsbAtualizar.PerformClick();
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
