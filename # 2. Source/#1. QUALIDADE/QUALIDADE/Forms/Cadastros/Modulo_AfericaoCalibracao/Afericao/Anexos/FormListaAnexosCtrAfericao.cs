﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros
{
    public partial class FormListaAnexosCtrAfericao : Form
    {
        private AAFERICAOINTERNA _afericao;

        public FormListaAnexosCtrAfericao(AAFERICAOINTERNA afericao)
        {
            InitializeComponent();
            _afericao = afericao;
            this.Text = $"Anexos da Verificação Interna - {_afericao.CODIGO} - Cli/For: {_afericao.CODCFO} | Rev.: {_afericao.REV}";
        }

        private void FormListaAnexosCtrAfericao_Load(object sender, EventArgs e)
        {
            if (dgvListagem.Columns != null)
                dgvListagem.Columns.Clear();
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SEQUENCIAL", HeaderText = "Sequencial", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TAMANHO", HeaderText = "Tamanho", AllowEditing = false, AllowFiltering = false, Visible = false });
            CarregaDGV();
        }

        private void tsbVisualizar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is AAFERICAOINTERNAANEXO anexo)
            {
                try
                {
                    AAFERICAOINTERNAANEXO temp = null;
                    using (ControleAAfericaoInternaAnexo controller = new ControleAAfericaoInternaAnexo())
                    {
                        temp = controller.Find(anexo.CODAFERICAO, anexo.CODCOLIGADA, anexo.CODCFO, anexo.REV, anexo.SEQUENCIAL);
                    }

                    if (temp != null)
                    {
                        string arquivoTemp = $@"{Global.PastaTemp()}\{temp.DESCRICAO}{temp.EXTENSAO}";
                        Process.Start(Arquivo.DescriptPDF(temp.ARQUIVO, arquivoTemp));
                    }
                }
                catch (Exception ex)
                {
                    Mensagem.Informacao("Falha", "Não foi possível abrir o anexo.\r\n" + ex.Message);
                }
            }
        }

        private void CarregaDGV()
        {
            Cursor.Current = Cursors.WaitCursor;
            using (ControleAAfericaoInternaAnexo controller = new ControleAAfericaoInternaAnexo())
            {
                BindingSource anexoBindingSource = new BindingSource();
                anexoBindingSource.DataSource = typeof(AAFERICAOINTERNAANEXO);
                var itens = controller.GetAllSemAnexo(
                    _afericao.CODIGO, _afericao.CODCOLIGADA,
                    _afericao.CODCFO, _afericao.REV 
                );
                //controller.Get(x =>
                //    x.CODAFERICAO == _afericao.CODIGO && x.CODCOLIGADA == _afericao.CODCOLIGADA && 
                //    x.CODCFO == _afericao.CODCFO && x.REV == _afericao.REV 
                //).ToList();
                anexoBindingSource.DataSource = itens;
                dgvListagem.DataSource = null;
                dgvListagem.DataSource = anexoBindingSource;
                anexoBindingSource.EndEdit();
                if(itens?.Count() > 0)
                {
                    tsbVisualizar.Enabled = true;
                    tsbRemover.Enabled = true;
                }
                else
                {
                    tsbVisualizar.Enabled = false;
                    tsbRemover.Enabled = false;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            string caminhoArquivo = Arquivo.SelecionaArquivo("Todos os arquivos (*.*)|*.*");

            if (!string.IsNullOrEmpty(caminhoArquivo))
            {
                using (ControleAAfericaoInternaAnexo controller = new ControleAAfericaoInternaAnexo())
                {
                    try
                    {
                        AAFERICAOINTERNAANEXO anexo = new AAFERICAOINTERNAANEXO
                        {
                            CODAFERICAO = _afericao.CODIGO,
                            CODCOLIGADA = _afericao.CODCOLIGADA,
                            CODCFO = _afericao.CODCFO,
                            REV = _afericao.REV,
                            SEQUENCIAL = controller.GetNewSequencial(_afericao.CODIGO, _afericao.CODCOLIGADA, _afericao.CODCFO, _afericao.REV),
                            ARQUIVO = Arquivo.EncriptPDF(caminhoArquivo),
                            EXTENSAO = Path.GetExtension(caminhoArquivo).ToLower(),
                            DESCRICAO = Path.GetFileNameWithoutExtension(caminhoArquivo),
                            TAMANHO = new FileInfo(caminhoArquivo).Length,
                            RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                            RECCREATEDON = DateTime.Now
                        };
                        controller.Create(anexo);
                        controller.SaveAll();
                        Mensagem.Informacao("Sucesso", "Anexo incluido com sucesso!");
                    }
                    catch (Exception ex)
                    {
                        Mensagem.Informacao("Falha", "Não foi possível adicionar o anexo.\r\n" + ex.Message);
                    }
                }
                tsbAtualizar.PerformClick();
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is AAFERICAOINTERNAANEXO anexo)
            {
                DialogResult dr = Mensagem.Confirmacao("Confirmação", "Deseja realmente excluir o anexo selecionado?");

                if (dr == DialogResult.Yes)
                {
                    using (ControleAAfericaoInternaAnexo controller = new ControleAAfericaoInternaAnexo())
                    {
                        try
                        {
                            controller.Delete(x =>
                                x.CODAFERICAO == _afericao.CODIGO && 
                                x.CODCOLIGADA == _afericao.CODCOLIGADA &&
                                x.CODCFO == _afericao.CODCFO && 
                                x.REV == _afericao.REV && 
                                x.SEQUENCIAL == anexo.SEQUENCIAL
                            );
                            controller.SaveAll();
                            Mensagem.Informacao("Sucesso", "Anexo excluído com sucesso!");
                        }
                        catch (Exception ex)
                        {
                            Mensagem.Informacao("Falha", "Não foi possível excluir o anexo.\r\n" + ex.Message);
                        }
                    }
                    tsbAtualizar.PerformClick();
                }
            }
            else
            {
                Mensagem.Alerta("Alerta", "Selecione um anexo para realizar a exclusão.");
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbVisualizar.PerformClick();
        }
    }
}
