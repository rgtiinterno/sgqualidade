﻿namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao
{
    partial class FormHstAfericao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHstAfericao));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFrequencia = new System.Windows.Forms.ComboBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodColigada = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbFornecedor = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtInstrumento = new System.Windows.Forms.TextBox();
            this.txtIdentificacao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDtAfericao = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtValidade = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ckConforme = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckNaoConforme = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtProcessoLocal = new System.Windows.Forms.TextBox();
            this.txtResponsavelEquipamento = new System.Windows.Forms.TextBox();
            this.txtResponsvelAfericao = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(449, 408);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "&Fechar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(422, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Frequência:";
            // 
            // cbFrequencia
            // 
            this.cbFrequencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencia.FormattingEnabled = true;
            this.cbFrequencia.Items.AddRange(new object[] {
            "Semanal",
            "Quinzenal",
            "Mensal",
            "Anual"});
            this.cbFrequencia.Location = new System.Drawing.Point(425, 97);
            this.cbFrequencia.Name = "cbFrequencia";
            this.cbFrequencia.Size = new System.Drawing.Size(115, 21);
            this.cbFrequencia.TabIndex = 5;
            this.cbFrequencia.SelectedIndexChanged += new System.EventHandler(this.cbFrequencia_SelectedIndexChanged);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(22, 40);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(100, 22);
            this.txtCodigo.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 51;
            this.label2.Text = "Código Interno:";
            // 
            // txtCodColigada
            // 
            this.txtCodColigada.Location = new System.Drawing.Point(128, 40);
            this.txtCodColigada.Name = "txtCodColigada";
            this.txtCodColigada.ReadOnly = true;
            this.txtCodColigada.Size = new System.Drawing.Size(100, 22);
            this.txtCodColigada.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Código Coligada:";
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFornecedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.Location = new System.Drawing.Point(234, 40);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(306, 21);
            this.cbFornecedor.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(231, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 51;
            this.label4.Text = "Fornecedor:";
            // 
            // txtInstrumento
            // 
            this.txtInstrumento.Location = new System.Drawing.Point(22, 97);
            this.txtInstrumento.Name = "txtInstrumento";
            this.txtInstrumento.Size = new System.Drawing.Size(291, 22);
            this.txtInstrumento.TabIndex = 3;
            // 
            // txtIdentificacao
            // 
            this.txtIdentificacao.Location = new System.Drawing.Point(319, 97);
            this.txtIdentificacao.Name = "txtIdentificacao";
            this.txtIdentificacao.Size = new System.Drawing.Size(100, 22);
            this.txtIdentificacao.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 51;
            this.label5.Text = "Instrumento:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(316, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "Identificação:";
            // 
            // txtDtAfericao
            // 
            this.txtDtAfericao.Location = new System.Drawing.Point(22, 159);
            this.txtDtAfericao.Mask = "99/99/9999";
            this.txtDtAfericao.Name = "txtDtAfericao";
            this.txtDtAfericao.Size = new System.Drawing.Size(100, 22);
            this.txtDtAfericao.TabIndex = 6;
            this.txtDtAfericao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 143);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 51;
            this.label7.Text = "Data da Verificação:";
            // 
            // txtValidade
            // 
            this.txtValidade.Location = new System.Drawing.Point(128, 159);
            this.txtValidade.Name = "txtValidade";
            this.txtValidade.ReadOnly = true;
            this.txtValidade.Size = new System.Drawing.Size(100, 22);
            this.txtValidade.TabIndex = 7;
            this.txtValidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(125, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "Validade:";
            // 
            // ckConforme
            // 
            this.ckConforme.AutoSize = true;
            this.ckConforme.Checked = true;
            this.ckConforme.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckConforme.Location = new System.Drawing.Point(46, 22);
            this.ckConforme.Name = "ckConforme";
            this.ckConforme.Size = new System.Drawing.Size(77, 17);
            this.ckConforme.TabIndex = 0;
            this.ckConforme.Text = "Conforme";
            this.ckConforme.UseVisualStyleBackColor = true;
            this.ckConforme.CheckedChanged += new System.EventHandler(this.ckConforme_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckNaoConforme);
            this.groupBox1.Controls.Add(this.ckConforme);
            this.groupBox1.Location = new System.Drawing.Point(249, 143);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(291, 52);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resultado";
            // 
            // ckNaoConforme
            // 
            this.ckNaoConforme.AutoSize = true;
            this.ckNaoConforme.Location = new System.Drawing.Point(153, 22);
            this.ckNaoConforme.Name = "ckNaoConforme";
            this.ckNaoConforme.Size = new System.Drawing.Size(101, 17);
            this.ckNaoConforme.TabIndex = 1;
            this.ckNaoConforme.Text = "Não Conforme";
            this.ckNaoConforme.UseVisualStyleBackColor = true;
            this.ckNaoConforme.CheckedChanged += new System.EventHandler(this.ckNaoConforme_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtProcessoLocal);
            this.groupBox2.Controls.Add(this.txtResponsavelEquipamento);
            this.groupBox2.Controls.Add(this.txtResponsvelAfericao);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(22, 214);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(518, 176);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Processo e Responsaveis";
            // 
            // txtProcessoLocal
            // 
            this.txtProcessoLocal.Location = new System.Drawing.Point(6, 104);
            this.txtProcessoLocal.Multiline = true;
            this.txtProcessoLocal.Name = "txtProcessoLocal";
            this.txtProcessoLocal.Size = new System.Drawing.Size(505, 66);
            this.txtProcessoLocal.TabIndex = 2;
            // 
            // txtResponsavelEquipamento
            // 
            this.txtResponsavelEquipamento.Location = new System.Drawing.Point(262, 51);
            this.txtResponsavelEquipamento.Name = "txtResponsavelEquipamento";
            this.txtResponsavelEquipamento.Size = new System.Drawing.Size(249, 22);
            this.txtResponsavelEquipamento.TabIndex = 1;
            // 
            // txtResponsvelAfericao
            // 
            this.txtResponsvelAfericao.Location = new System.Drawing.Point(6, 51);
            this.txtResponsvelAfericao.Name = "txtResponsvelAfericao";
            this.txtResponsvelAfericao.Size = new System.Drawing.Size(250, 22);
            this.txtResponsvelAfericao.TabIndex = 0;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(259, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "Responsável pelo Equipamento:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 88);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Processo / Local";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Responsável pela Verificação:";
            // 
            // FormHstAfericao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 447);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtValidade);
            this.Controls.Add(this.txtDtAfericao);
            this.Controls.Add(this.txtIdentificacao);
            this.Controls.Add(this.txtInstrumento);
            this.Controls.Add(this.cbFornecedor);
            this.Controls.Add(this.txtCodColigada);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.cbFrequencia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancelar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormHstAfericao";
            this.Text = "Verificação Interna";
            this.Load += new System.EventHandler(this.FormHstAfericao_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFrequencia;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodColigada;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbFornecedor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtInstrumento;
        private System.Windows.Forms.TextBox txtIdentificacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txtDtAfericao;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtValidade;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox ckConforme;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckNaoConforme;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtProcessoLocal;
        private System.Windows.Forms.TextBox txtResponsavelEquipamento;
        private System.Windows.Forms.TextBox txtResponsvelAfericao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
    }
}