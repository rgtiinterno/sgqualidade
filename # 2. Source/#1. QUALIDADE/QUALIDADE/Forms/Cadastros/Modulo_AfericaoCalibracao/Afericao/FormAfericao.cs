﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao
{
    public partial class FormAfericao : Form
    {
        public bool salvo = false;
        private AAFERICAOINTERNA itemEdit;
        private List<AAFERICAOINT068LEITURA> _leiturasEsquadro = null;
        private List<AAFERICAOINT070LEITURA> _leiturasTrena = null;
        private List<GCENTROCUSTO> _centrosCusto = null;

        public FormAfericao(AAFERICAOINTERNA afericao = null)
        {
            InitializeComponent();
            tabControl1.TabPages.Remove(tabPageInspVisual);
            tabControl1.TabPages.Remove(tabPageEsquadro);
            tabControl1.TabPages.Remove(tabPageNivelBolha);
            tabControl1.TabPages.Remove(tabPageTrena);
            gbResultadoVerificacao.Enabled = true;
            itemEdit = afericao;
        }

        private void FormAfericao_Load(object sender, EventArgs e)
        {
            CarregaCombo();
            if (itemEdit == null)
            {
                using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                {
                    short coligada = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;
                    txtCodColigada.Text = $"{coligada}";
                    txtCodigo.Text = $"{controle.GetNewID(coligada)}";
                    txtDtAfericao.Text = $"{DateTime.Now}";
                }
            }
            else
            {
                txtCodigo.Text = $"{itemEdit.CODIGO}";
                txtCodColigada.Text = $"{itemEdit.CODCOLIGADA}";
                txtInstrumento.Text = $"{itemEdit.NOMEINSTRUMENTO}";
                txtIdentificacao.Text = $"{itemEdit.IDENTIFICACAO}";
                cbFrequencia.Text = itemEdit.FRENQUENCIA;
                txtDtAfericao.Text = $"{itemEdit.DTAFERICAO}";
                txtValidade.Text = $"{itemEdit.VALIDADE}";
                txtResponsvelAfericao.Text = $"{itemEdit.RESPONSAVELAFERICAO}";
                txtResponsavelEquipamento.Text = $"{itemEdit.RESPONSAVELEQUIPAMENTO}";
                txtProcessoLocal.Text = $"{itemEdit.LOCAL}";
                rbConforme.Checked = itemEdit.RESULTADO;
                rbNaoConforme.Checked = !itemEdit.RESULTADO && itemEdit.RESULTADOSTR == "REPROVADO";
                if (!string.IsNullOrEmpty(itemEdit.CODCCUSTO))
                    cbCentroCusto.SelectedValue = itemEdit.CODCCUSTO;
                else
                    cbCentroCusto.SelectedIndex = -1;
                txtTolerancia.Text = itemEdit.TOLERANCIA;
                txtUndMedida.Text = itemEdit.UNDMEDIDA;
                txtIdentificacao.ReadOnly = true;

                rbTipoEsquadro.Checked = itemEdit.TIPOAFERICAO == "068";
                rbTipoBolha.Checked = itemEdit.TIPOAFERICAO == "069";
                rbTipoTrena.Checked = itemEdit.TIPOAFERICAO == "070";
                rbTipoOutros.Checked = itemEdit.TIPOAFERICAO == "000";
                if (itemEdit.TIPOAFERICAO != "000")
                {
                    txtObservacoes.Text = itemEdit.OBSERVACOES;
                    rbEquipFuncSim.Checked = itemEdit.EQUIPFUNCIONANDO;
                    rbEquipFuncNao.Checked = !itemEdit.EQUIPFUNCIONANDO;
                    rbEquipLimpoSim.Checked = itemEdit.EQUIPLIMPO;
                    rbEquipLimpoNao.Checked = !itemEdit.EQUIPLIMPO;
                    rbEquipBemAcondSim.Checked = itemEdit.EQUIPBEMACOND;
                    rbEquipBemAcondNao.Checked = !itemEdit.EQUIPBEMACOND;
                    rbEquipDanoSim.Checked = itemEdit.EQUIPDANOAPARENTE;
                    rbEquipDanoNao.Checked = !itemEdit.EQUIPDANOAPARENTE;
                    if (itemEdit.TIPOAFERICAO == "068")
                    {
                        CarregaLeiturasEsquadro();
                    }
                    if (itemEdit.TIPOAFERICAO == "069")
                    {
                        rbF069BolhaCentSim.Checked = itemEdit.FORM069BOLHACENTRALIZADA;
                        rbF069BolhaCentNao.Checked = !itemEdit.FORM069BOLHACENTRALIZADA;
                        rbF069NiveladoSim.Checked = itemEdit.FORM069LOCALNIVELADO;
                        rbF069NiveladoNao.Checked = !itemEdit.FORM069LOCALNIVELADO;
                    }
                    if (itemEdit.TIPOAFERICAO == "070")
                    {
                        using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                        {
                            _leiturasTrena = controle.GetLeituras070Trena(
                                itemEdit.CODIGO, itemEdit.CODCOLIGADA, itemEdit.CODCFO, itemEdit.REV
                            );
                        }
                    }
                }
            }
        }

        private void CarregaCombo()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                _centrosCusto = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "A");

                cbCentroCusto.DataSource = _centrosCusto;
                cbCentroCusto.ValueMember = "CODCCUSTO";
                cbCentroCusto.DisplayMember = "DESCCOMBO";
                cbCentroCusto.SelectedIndex = -1;
            }

            //using (ControleFCFO controle = new ControleFCFO())
            //{
            //    if (File.Exists($"{Directory.GetCurrentDirectory()}\\{Properties.Settings.Default.conexaoIntegracao}"))
            //        cbFornecedor.DataSource = controle.GetIntegracaoRM();
            //    else
            //        cbFornecedor.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);

            //    cbFornecedor.ValueMember = "CODCFO";
            //    cbFornecedor.DisplayMember = "NOMEFANTASIA";
            //    cbFornecedor.SelectedIndex = -1;
            //}
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                CalcDtValidade();
                GCENTROCUSTO centroCusto = null;
                if (cbCentroCusto.SelectedIndex >= 0)
                    centroCusto = cbCentroCusto.SelectedItem as GCENTROCUSTO;

                ChecaResultadoConformidade();
                string codTipoAfericao = GetCodTipoAfericao();
                using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                {
                    AAFERICAOINTERNA item = new AAFERICAOINTERNA
                    {
                        CODIGO = Convert.ToInt32(txtCodigo.Text),
                        CODCOLIGADA = Convert.ToInt16(txtCodColigada.Text),
                        CODCFO = txtCodColigada.Text,
                        NOMEINSTRUMENTO = txtInstrumento.Text,
                        IDENTIFICACAO = txtIdentificacao.Text,
                        FRENQUENCIA = cbFrequencia.Text,
                        DTAFERICAO = Convert.ToDateTime(txtDtAfericao.Text),
                        VALIDADE = Convert.ToDateTime(txtValidade.Text),
                        RESULTADO = rbConforme.Checked,
                        RESULTADOSTR = rbConforme.Checked ? "APROVADO" : rbNaoConforme.Checked ? "REPROVADO" : null,
                        RESPONSAVELAFERICAO = txtResponsvelAfericao.Text,
                        RESPONSAVELEQUIPAMENTO = txtResponsavelEquipamento.Text,
                        LOCAL = txtProcessoLocal.Text,
                        REV = (itemEdit == null) ? 1 : (itemEdit.REV + 1),
                        CODCOLCCUSTO = (centroCusto != null ? (short?)centroCusto.CODCOLIGADA : null),
                        CODCCUSTO = centroCusto != null ? centroCusto.CODCCUSTO : null,
                        UNDMEDIDA = txtUndMedida.Text,
                        TOLERANCIA = txtTolerancia.Text,
                        ATIVO = itemEdit?.ATIVO ?? true,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now,
                        TIPOAFERICAO = codTipoAfericao,
                    };

                    if (codTipoAfericao != "000")
                    {
                        item.OBSERVACOES = txtObservacoes.Text?.Trim();
                        item.EQUIPFUNCIONANDO = rbEquipFuncSim.Checked;
                        item.EQUIPLIMPO = rbEquipLimpoSim.Checked;
                        item.EQUIPBEMACOND = rbEquipBemAcondSim.Checked;
                        item.EQUIPDANOAPARENTE = rbEquipDanoSim.Checked;

                        if (codTipoAfericao == "068" && _leiturasEsquadro != null)
                        {
                            int contCod = 1;
                            foreach (var leitura in _leiturasEsquadro)
                            {
                                leitura.CODIGO = contCod;
                                leitura.CODAFERICAO = item.CODIGO;
                                leitura.CODCOLIGADA = item.CODCOLIGADA;
                                leitura.CODCFO = item.CODCFO;
                                leitura.REV = item.REV;
                                item.AAFERICAOINT068LEITURA.Add(leitura);
                                contCod++;
                            }
                        }
                        
                        if (codTipoAfericao == "069")
                        {
                            item.FORM069BOLHACENTRALIZADA = rbF069BolhaCentSim.Checked;
                            item.FORM069LOCALNIVELADO = rbF069NiveladoSim.Checked;
                        }
                        
                        if (codTipoAfericao == "070" && _leiturasTrena != null)
                        {
                            int contCod = 1;
                            foreach (var leitura in _leiturasTrena)
                            {
                                leitura.CODIGO = contCod;
                                leitura.CODAFERICAO = item.CODIGO;
                                leitura.CODCOLIGADA = item.CODCOLIGADA;
                                leitura.CODCFO = item.CODCFO;
                                leitura.REV = item.REV;
                                item.AAFERICAOINT070LEITURA.Add(leitura);
                                contCod++;
                            }
                        }
                    }
                    controle.Create(item);
                    controle.SaveAll();
                    salvo = true;
                    this.Close();
                }
            }
        }

        private string GetCodTipoAfericao()
        {
            string codTipo = "000";
            if (rbTipoEsquadro.Checked)
                codTipo = "068";
            else if (rbTipoBolha.Checked)
                codTipo = "069";
            else if (rbTipoTrena.Checked)
                codTipo = "070";
            return codTipo;
        }

        private void cbFrequencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbFrequencia.SelectedIndex >= 0)
                CalcDtValidade();
        }

        private void CalcDtValidade()
        {
            if (DateTime.TryParse(txtDtAfericao.Text, out DateTime dtAfericao))
            {
                DateTime hoje = DateTime.Now;
                if (cbFrequencia.SelectedIndex == 0)
                    txtValidade.Text = $"{dtAfericao.AddDays(7)}";
                else if (cbFrequencia.SelectedIndex == 1)
                    txtValidade.Text = $"{dtAfericao.AddDays(15)}";
                else if (cbFrequencia.SelectedIndex == 2)
                    txtValidade.Text = $"{dtAfericao.AddMonths(1)}";
                else if (cbFrequencia.SelectedIndex == 3)
                    txtValidade.Text = $"{dtAfericao.AddMonths(6)}";
                else if (cbFrequencia.SelectedIndex == 4)
                    txtValidade.Text = $"{dtAfericao.AddYears(1)}";
            }
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            //if (cbFornecedor.SelectedIndex < 0)
            //    msg += " - Informe o fornecedor.\n";

            if (string.IsNullOrEmpty(txtInstrumento.Text))
                msg += " - Informe o nome do instrumento.\n";

            if (string.IsNullOrEmpty(txtIdentificacao.Text))
                msg += " - Informe a identificação do instrumento.\n";

            if(cbFrequencia.SelectedIndex < 0 || string.IsNullOrEmpty(cbFrequencia.Text))
                msg += " - Selecione a frequência.\n";

            if (string.IsNullOrEmpty(txtResponsvelAfericao.Text))
                msg += " - Informe o nome do responsável por realizar a aferição.\n";

            if (string.IsNullOrEmpty(txtResponsavelEquipamento.Text))
                msg += " - Informe o nome do responsável pelo equipamento.\n";

            if (string.IsNullOrEmpty(txtProcessoLocal.Text))
                msg += " - Informe o processo / local da aferição.\n";

            string codTipo = GetCodTipoAfericao();
            if (codTipo == "068") //Validações Form Esquadro
            {
                if(_leiturasEsquadro == null || _leiturasEsquadro.Count <= 0)
                    msg += " - Informe ao menos uma Verificação de Esquadro.\n";
            }

            if (codTipo == "069") //Validações Form Nível Bolha
            {
                if ((!rbF069BolhaCentSim.Checked && !rbF069BolhaCentNao.Checked) || (!rbF069NiveladoSim.Checked && !rbF069NiveladoNao.Checked))
                    msg += " - Responda todas as Perguntas da Verificação de Nível Bolha.\n";
            }

            if (codTipo == "070") //Validações Form Trena
            {
                if (_leiturasTrena == null || _leiturasTrena.Count <= 0)
                    msg += " - Informe ao menos uma Verificação de Trena.\n";
            }

            if (!rbConforme.Checked && !rbNaoConforme.Checked)
                msg += " - Informe o Resultado da Verificação.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgAlerta(msg);
                return false;
            }
        }

        private void txtDtAfericao_TextChanged(object sender, EventArgs e)
        {
            if (cbFrequencia.SelectedIndex >= 0)
                CalcDtValidade();
        }

        private void btnBuscaCCusto_Click(object sender, EventArgs e)
        {
            if (_centrosCusto != null)
            {
                using (FormListaItensCombo frm = new FormListaItensCombo(
                    _centrosCusto, "NOMEGERENTE", "ATIVO", "CODUSUGERENTE", 
                    "ENVIASPED", "ID", "NUMCONTRATO", "DATAINICIOOBRA", "DATAFIMOBRA"
                 ))
                {
                    frm.ShowDialog();
                    if(frm.itemSelecionado != null && frm.itemSelecionado is GCENTROCUSTO itemCCusto)
                    {
                        cbCentroCusto.SelectedValue = itemCCusto.CODCCUSTO;
                    }
                }
            }
        }

        private void rbTipoEsquadro_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoEsquadro.Checked)
            {
                if (!tabControl1.TabPages.Contains(tabPageInspVisual))
                    tabControl1.TabPages.Add(tabPageInspVisual);
                if (!tabControl1.TabPages.Contains(tabPageEsquadro))
                    tabControl1.TabPages.Add(tabPageEsquadro);
                tabControl1.TabPages.Remove(tabPageNivelBolha);
                tabControl1.TabPages.Remove(tabPageTrena);
                gbResultadoVerificacao.Enabled = false;
            }
            ChecaResultadoConformidade();
            CarregaLeiturasEsquadro();
        }

        private void rbTipoBolha_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoBolha.Checked)
            {
                if (!tabControl1.TabPages.Contains(tabPageInspVisual))
                    tabControl1.TabPages.Add(tabPageInspVisual);
                if (!tabControl1.TabPages.Contains(tabPageNivelBolha))
                    tabControl1.TabPages.Add(tabPageNivelBolha);
                tabControl1.TabPages.Remove(tabPageEsquadro);
                tabControl1.TabPages.Remove(tabPageTrena);
                gbResultadoVerificacao.Enabled = false;
            }
            ChecaResultadoConformidade();
        }

        private void rbTipoTrena_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoTrena.Checked)
            {
                if (!tabControl1.TabPages.Contains(tabPageInspVisual))
                    tabControl1.TabPages.Add(tabPageInspVisual);
                if (!tabControl1.TabPages.Contains(tabPageTrena))
                    tabControl1.TabPages.Add(tabPageTrena);
                tabControl1.TabPages.Remove(tabPageEsquadro);
                tabControl1.TabPages.Remove(tabPageNivelBolha);
                gbResultadoVerificacao.Enabled = false;
            }
            ChecaResultadoConformidade();
            CarregaLeiturasTrena();
        }

        private void rbTipoOutros_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTipoOutros.Checked)
            {
                tabControl1.TabPages.Remove(tabPageInspVisual);
                tabControl1.TabPages.Remove(tabPageEsquadro);
                tabControl1.TabPages.Remove(tabPageNivelBolha);
                tabControl1.TabPages.Remove(tabPageTrena);
                gbResultadoVerificacao.Enabled = true;
            }
            ChecaResultadoConformidade();
        }

        private void ChecaResulConfCheckedChanged(object sender, EventArgs e)
            => ChecaResultadoConformidade();

        private void ChecaResultadoConformidade()
        {
            string codTipo = GetCodTipoAfericao();
            if (codTipo != "000")
            {
                bool conforme = (
                    codTipo != "069" ||
                    (codTipo == "069" && rbF069BolhaCentSim.Checked && rbF069NiveladoSim.Checked)
                ) && (
                    rbEquipFuncSim.Checked && rbEquipLimpoSim.Checked &&
                    rbEquipBemAcondSim.Checked && rbEquipDanoNao.Checked
                );
                rbConforme.Checked = conforme;
                rbNaoConforme.Checked = !conforme;
            }
        }

        #region "Verificação de Esquadro"
        private void CarregaLeiturasEsquadro()
        {
            dgvListaEsquadro?.Columns?.Clear();
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "LEITURALADO01", HeaderText = "Leitura Lado 01", AllowEditing = false, AllowFiltering = true });
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "LEITURALADO02", HeaderText = "Leitura Lado 02", AllowEditing = false, AllowFiltering = true });
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "LEITURAHIPOTENUSA", HeaderText = "Leitura da Hipotenusa", AllowEditing = false, AllowFiltering = true });
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "DESVIOTOLERAVEL", HeaderText = "Desvio Tolerável", AllowEditing = false, AllowFiltering = true });
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "RESULTADO", HeaderText = "Resultado", AllowEditing = false, AllowFiltering = true });
            dgvListaEsquadro.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACOES", HeaderText = "Observações", AllowEditing = false, AllowFiltering = true });
            if (itemEdit != null)
            {
                using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                {
                    _leiturasEsquadro = _leiturasEsquadro ?? controle.GetLeituras068Esquadro(
                        itemEdit.CODIGO, itemEdit.CODCOLIGADA, itemEdit.CODCFO, itemEdit.REV
                    );
                    dgvListaEsquadro.DataSource = null;
                    dgvListaEsquadro.DataSource = _leiturasEsquadro;
                    dgvListaEsquadro.Refresh();
                }
            }
            else
            {
                dgvListaEsquadro.DataSource = null;
                dgvListaEsquadro.DataSource = _leiturasEsquadro;
                dgvListaEsquadro.Refresh();
            }
        }

        private void tsbAdicionarEsquadro_Click(object sender, EventArgs e)
        {
            using (FormAddLeitura068 frm = new FormAddLeitura068())
            {
                frm.ShowDialog();
                if(frm._salvo && frm._itemLeitura != null)
                {
                    if (_leiturasEsquadro == null)
                        _leiturasEsquadro = new List<AAFERICAOINT068LEITURA>();
                    _leiturasEsquadro.Add(frm._itemLeitura);
                    CarregaLeiturasEsquadro();
                }
            }
        }

        private void tsbEditarEsquadro_Click(object sender, EventArgs e)
        {
            if(dgvListaEsquadro.CurrentItem is AAFERICAOINT068LEITURA item)
            {
                if (_leiturasEsquadro == null)
                    _leiturasEsquadro = new List<AAFERICAOINT068LEITURA>();
                int index = _leiturasEsquadro.IndexOf(item);

                if (index < 0)
                    index = _leiturasEsquadro.FindIndex(x => x.CODIGO == item.CODIGO);

                if (index >= 0)
                {
                    using (FormAddLeitura068 frm = new FormAddLeitura068(itemEdit: item))
                    {
                        frm.ShowDialog();
                        if (frm._salvo && frm._itemLeitura != null)
                        {
                            _leiturasEsquadro[index] = frm._itemLeitura;
                            CarregaLeiturasEsquadro();
                        }
                    }
                }
            }
        }

        private void tsbRemoverEsquadro_Click(object sender, EventArgs e)
        {
            if (dgvListaEsquadro.CurrentItem is AAFERICAOINT068LEITURA item)
            {
                if (_leiturasEsquadro == null)
                    _leiturasEsquadro = new List<AAFERICAOINT068LEITURA>();
                int index = _leiturasEsquadro.IndexOf(item);

                if (index < 0)
                    index = _leiturasEsquadro.FindIndex(x => x.CODIGO == item.CODIGO);

                if (index >= 0)
                {
                    DialogResult dr = Mensagem.Confirmacao("Confirmação", "Deseja realmente remover o item selecionado?");
                    if (dr == DialogResult.Yes)
                    {
                        _leiturasEsquadro.RemoveAt(index);
                        CarregaLeiturasEsquadro();
                    }
                }
            }
        }

        private void tsbAtualizarEsquadro_Click(object sender, EventArgs e)
        {
            CarregaLeiturasEsquadro();
        }

        private void dgvListaEsquadro_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbEditar.PerformClick();
        }
        #endregion

        #region "Verificação de Trena"
        private void CarregaLeiturasTrena()
        {
            dgvListaTrena?.Columns?.Clear();
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "LEITURAPADRAO", HeaderText = "Leitura no Padrão", AllowEditing = false, AllowFiltering = true });
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "LEITURADISPOSITIVO", HeaderText = "Leitura no Dispositivo", AllowEditing = false, AllowFiltering = true });
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "DESVIOENCONTRADO", HeaderText = "Desvio Encontrado", AllowEditing = false, AllowFiltering = true });
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "DESVIOTOLERAVEL", HeaderText = "Desvio Tolerável", AllowEditing = false, AllowFiltering = true });
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "RESULTADO", HeaderText = "Resultado", AllowEditing = false, AllowFiltering = true });
            dgvListaTrena.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACOES", HeaderText = "Observações", AllowEditing = false, AllowFiltering = true });
            if (itemEdit != null)
            {
                using (ControleAAfericaoInterna controle = new ControleAAfericaoInterna())
                {
                    _leiturasTrena = _leiturasTrena ?? controle.GetLeituras070Trena(
                        itemEdit.CODIGO, itemEdit.CODCOLIGADA, itemEdit.CODCFO, itemEdit.REV
                    );
                    dgvListaTrena.DataSource = null;
                    dgvListaTrena.DataSource = _leiturasTrena;
                    dgvListaTrena.Refresh();
                }
            }
            else
            {
                dgvListaTrena.DataSource = null;
                dgvListaTrena.DataSource = _leiturasTrena;
                dgvListaTrena.Refresh();
            }
        }

        private void tsbAdicionarTrena_Click(object sender, EventArgs e)
        {
            using (FormAddLeitura070 frm = new FormAddLeitura070())
            {
                frm.ShowDialog();
                if (frm._salvo && frm._itemLeitura != null)
                {
                    if (_leiturasTrena == null)
                        _leiturasTrena = new List<AAFERICAOINT070LEITURA>();
                    _leiturasTrena.Add(frm._itemLeitura);
                    CarregaLeiturasTrena();
                }
            }
        }

        private void tsbEditarTrena_Click(object sender, EventArgs e)
        {
            if (dgvListaTrena.CurrentItem is AAFERICAOINT070LEITURA item)
            {
                if (_leiturasTrena == null)
                    _leiturasTrena = new List<AAFERICAOINT070LEITURA>();
                int index = _leiturasTrena.IndexOf(item);

                if (index < 0)
                    index = _leiturasTrena.FindIndex(x => x.CODIGO == item.CODIGO);

                if (index >= 0)
                {
                    using (FormAddLeitura070 frm = new FormAddLeitura070(itemEdit: item))
                    {
                        frm.ShowDialog();
                        if (frm._salvo && frm._itemLeitura != null)
                        {
                            _leiturasTrena[index] = frm._itemLeitura;
                            CarregaLeiturasTrena();
                        }
                    }
                }
            }
        }

        private void tsbRemoverTrena_Click(object sender, EventArgs e)
        {
            if (dgvListaTrena.CurrentItem is AAFERICAOINT070LEITURA item)
            {
                if (_leiturasTrena == null)
                    _leiturasTrena = new List<AAFERICAOINT070LEITURA>();
                int index = _leiturasTrena.IndexOf(item);

                if (index < 0)
                    index = _leiturasTrena.FindIndex(x => x.CODIGO == item.CODIGO);

                if (index >= 0)
                {
                    DialogResult dr = Mensagem.Confirmacao("Confirmação", "Deseja realmente remover o item selecionado?");
                    if (dr == DialogResult.Yes)
                    {
                        _leiturasTrena.RemoveAt(index);
                        CarregaLeiturasTrena();
                    }
                }
            }
        }

        private void tsbAtualizarTrena_Click(object sender, EventArgs e)
        {
            CarregaLeiturasTrena();
        }

        private void dgvListaTrena_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbEditarTrena.PerformClick();
        }
        #endregion
    }
}