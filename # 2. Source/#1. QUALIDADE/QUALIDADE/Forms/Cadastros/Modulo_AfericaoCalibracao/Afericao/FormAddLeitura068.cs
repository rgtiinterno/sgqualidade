﻿using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE
{
    public partial class FormAddLeitura068 : Form
    {
        public AAFERICAOINT068LEITURA _itemLeitura = null;
        public bool _salvo = false;

        public FormAddLeitura068(AAFERICAOINT068LEITURA itemEdit = null)
        {
            InitializeComponent();
            _itemLeitura = itemEdit;
            if (itemEdit != null)
                PreencheForm();
        }

        private void PreencheForm()
        {
            txtLeitura01.Text = _itemLeitura.LEITURALADO01;
            txtLeitura02.Text = _itemLeitura.LEITURALADO02;
            txtLeituraHipotenusa.Text = _itemLeitura.LEITURAHIPOTENUSA;
            txtDesvioToleravel.Text = _itemLeitura.DESVIOTOLERAVEL;
            txtObservacoes.Text = _itemLeitura.OBSERVACOES;
            rbConforme.Checked = _itemLeitura.RESULTADO == 1;
            rbNaoConforme.Checked = _itemLeitura.RESULTADO != 1;
        }

        private bool ValidaForm()
        {
            string msgErro = string.Empty;

            if (string.IsNullOrWhiteSpace(txtLeitura01.Text))
                msgErro += $"\r\n - Informe a Leitura 01";

            if (string.IsNullOrWhiteSpace(txtLeitura02.Text))
                msgErro += $"\r\n - Informe a Leitura 02";

            if (string.IsNullOrWhiteSpace(txtLeituraHipotenusa.Text))
                msgErro += $"\r\n - Informe a Leitura da Hipotenusa";

            if (string.IsNullOrWhiteSpace(txtLeitura01.Text))
                msgErro += $"\r\n - Informe o Desvio Tolerável";

            if (!rbConforme.Checked && !rbNaoConforme.Checked)
                msgErro += $"\r\n - Informe o Resultado";

            bool valido = string.IsNullOrEmpty(msgErro);
            if(!valido)
            {
                Mensagem.Alerta(
                    "Validação",
                    $"Falha na validação dos dados:\r\n{msgErro}"
                );
            }
            return valido;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if(ValidaForm())
            {
                _itemLeitura = new AAFERICAOINT068LEITURA
                {
                    LEITURALADO01 = txtLeitura01.Text.Trim(), 
                    LEITURALADO02 = txtLeitura02.Text.Trim(), 
                    LEITURAHIPOTENUSA = txtLeituraHipotenusa.Text.Trim(), 
                    DESVIOTOLERAVEL = txtDesvioToleravel.Text.Trim(), 
                    OBSERVACOES = txtObservacoes.Text.Trim(),
                    RESULTADO = (short)(rbConforme.Checked ? 1 : 0)
                };
                _salvo = true;
                Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            _itemLeitura = null;
            _salvo = false;
            Close();
        }
    }
}