﻿namespace QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao
{
    partial class FormAfericao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAfericao));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.btnBuscaCCusto = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbTipoOutros = new System.Windows.Forms.RadioButton();
            this.rbTipoTrena = new System.Windows.Forms.RadioButton();
            this.rbTipoBolha = new System.Windows.Forms.RadioButton();
            this.rbTipoEsquadro = new System.Windows.Forms.RadioButton();
            this.txtUndMedida = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtTolerancia = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbCentroCusto = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtValidade = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtProcessoLocal = new System.Windows.Forms.TextBox();
            this.txtResponsavelEquipamento = new System.Windows.Forms.TextBox();
            this.txtResponsvelAfericao = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.gbResultadoVerificacao = new System.Windows.Forms.GroupBox();
            this.rbNaoConforme = new System.Windows.Forms.RadioButton();
            this.rbConforme = new System.Windows.Forms.RadioButton();
            this.txtDtAfericao = new System.Windows.Forms.MaskedTextBox();
            this.txtIdentificacao = new System.Windows.Forms.TextBox();
            this.txtInstrumento = new System.Windows.Forms.TextBox();
            this.cbFornecedor = new System.Windows.Forms.ComboBox();
            this.txtCodColigada = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.cbFrequencia = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageInspVisual = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.rbEquipDanoNao = new System.Windows.Forms.RadioButton();
            this.rbEquipBemAcondNao = new System.Windows.Forms.RadioButton();
            this.rbEquipLimpoNao = new System.Windows.Forms.RadioButton();
            this.rbEquipFuncNao = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.rbEquipFuncSim = new System.Windows.Forms.RadioButton();
            this.rbEquipLimpoSim = new System.Windows.Forms.RadioButton();
            this.rbEquipBemAcondSim = new System.Windows.Forms.RadioButton();
            this.rbEquipDanoSim = new System.Windows.Forms.RadioButton();
            this.tabPageEsquadro = new System.Windows.Forms.TabPage();
            this.dgvListaEsquadro = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.tsbEditarEsquadro = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionarEsquadro = new System.Windows.Forms.ToolStripButton();
            this.tsbEditar = new System.Windows.Forms.ToolStripButton();
            this.tsbRemoverEsquadro = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAtualizarEsquadro = new System.Windows.Forms.ToolStripButton();
            this.tabPageNivelBolha = new System.Windows.Forms.TabPage();
            this.tabPageTrena = new System.Windows.Forms.TabPage();
            this.dgvListaTrena = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionarTrena = new System.Windows.Forms.ToolStripButton();
            this.tsbEditarTrena = new System.Windows.Forms.ToolStripButton();
            this.tsbRemoverTrena = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbAtualizarTrena = new System.Windows.Forms.ToolStripButton();
            this.label19 = new System.Windows.Forms.Label();
            this.txtObservacoes = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rbF069NiveladoNao = new System.Windows.Forms.RadioButton();
            this.rbF069BolhaCentNao = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.rbF069BolhaCentSim = new System.Windows.Forms.RadioButton();
            this.rbF069NiveladoSim = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tabControl1.SuspendLayout();
            this.tabPageInfo.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbResultadoVerificacao.SuspendLayout();
            this.tabPageInspVisual.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPageEsquadro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaEsquadro)).BeginInit();
            this.tsbEditarEsquadro.SuspendLayout();
            this.tabPageNivelBolha.SuspendLayout();
            this.tabPageTrena.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaTrena)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(496, 499);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 51;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(384, 499);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 50;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageInfo);
            this.tabControl1.Controls.Add(this.tabPageInspVisual);
            this.tabControl1.Controls.Add(this.tabPageEsquadro);
            this.tabControl1.Controls.Add(this.tabPageNivelBolha);
            this.tabControl1.Controls.Add(this.tabPageTrena);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(614, 491);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.btnBuscaCCusto);
            this.tabPageInfo.Controls.Add(this.groupBox3);
            this.tabPageInfo.Controls.Add(this.txtUndMedida);
            this.tabPageInfo.Controls.Add(this.label13);
            this.tabPageInfo.Controls.Add(this.txtTolerancia);
            this.tabPageInfo.Controls.Add(this.label12);
            this.tabPageInfo.Controls.Add(this.cbCentroCusto);
            this.tabPageInfo.Controls.Add(this.label15);
            this.tabPageInfo.Controls.Add(this.txtValidade);
            this.tabPageInfo.Controls.Add(this.groupBox2);
            this.tabPageInfo.Controls.Add(this.gbResultadoVerificacao);
            this.tabPageInfo.Controls.Add(this.txtDtAfericao);
            this.tabPageInfo.Controls.Add(this.txtIdentificacao);
            this.tabPageInfo.Controls.Add(this.txtInstrumento);
            this.tabPageInfo.Controls.Add(this.cbFornecedor);
            this.tabPageInfo.Controls.Add(this.txtCodColigada);
            this.tabPageInfo.Controls.Add(this.txtCodigo);
            this.tabPageInfo.Controls.Add(this.cbFrequencia);
            this.tabPageInfo.Controls.Add(this.label3);
            this.tabPageInfo.Controls.Add(this.label2);
            this.tabPageInfo.Controls.Add(this.label4);
            this.tabPageInfo.Controls.Add(this.label6);
            this.tabPageInfo.Controls.Add(this.label8);
            this.tabPageInfo.Controls.Add(this.label7);
            this.tabPageInfo.Controls.Add(this.label5);
            this.tabPageInfo.Controls.Add(this.label1);
            this.tabPageInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInfo.Size = new System.Drawing.Size(606, 465);
            this.tabPageInfo.TabIndex = 0;
            this.tabPageInfo.Text = "Informações Gerais";
            this.tabPageInfo.UseVisualStyleBackColor = true;
            // 
            // btnBuscaCCusto
            // 
            this.btnBuscaCCusto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscaCCusto.Image = global::QUALIDADE.Properties.Resources._285651_search_icon;
            this.btnBuscaCCusto.Location = new System.Drawing.Point(557, 82);
            this.btnBuscaCCusto.Name = "btnBuscaCCusto";
            this.btnBuscaCCusto.Size = new System.Drawing.Size(25, 25);
            this.btnBuscaCCusto.TabIndex = 8;
            this.btnBuscaCCusto.UseVisualStyleBackColor = true;
            this.btnBuscaCCusto.Click += new System.EventHandler(this.btnBuscaCCusto_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbTipoOutros);
            this.groupBox3.Controls.Add(this.rbTipoTrena);
            this.groupBox3.Controls.Add(this.rbTipoBolha);
            this.groupBox3.Controls.Add(this.rbTipoEsquadro);
            this.groupBox3.Location = new System.Drawing.Point(245, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(337, 50);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo de Verificação:";
            // 
            // rbTipoOutros
            // 
            this.rbTipoOutros.AutoSize = true;
            this.rbTipoOutros.Checked = true;
            this.rbTipoOutros.Location = new System.Drawing.Point(255, 21);
            this.rbTipoOutros.Name = "rbTipoOutros";
            this.rbTipoOutros.Size = new System.Drawing.Size(61, 17);
            this.rbTipoOutros.TabIndex = 6;
            this.rbTipoOutros.TabStop = true;
            this.rbTipoOutros.Text = "Outros";
            this.rbTipoOutros.UseVisualStyleBackColor = true;
            this.rbTipoOutros.CheckedChanged += new System.EventHandler(this.rbTipoOutros_CheckedChanged);
            // 
            // rbTipoTrena
            // 
            this.rbTipoTrena.AutoSize = true;
            this.rbTipoTrena.Location = new System.Drawing.Point(193, 21);
            this.rbTipoTrena.Name = "rbTipoTrena";
            this.rbTipoTrena.Size = new System.Drawing.Size(53, 17);
            this.rbTipoTrena.TabIndex = 5;
            this.rbTipoTrena.Text = "Trena";
            this.rbTipoTrena.UseVisualStyleBackColor = true;
            this.rbTipoTrena.CheckedChanged += new System.EventHandler(this.rbTipoTrena_CheckedChanged);
            // 
            // rbTipoBolha
            // 
            this.rbTipoBolha.AutoSize = true;
            this.rbTipoBolha.Location = new System.Drawing.Point(102, 21);
            this.rbTipoBolha.Name = "rbTipoBolha";
            this.rbTipoBolha.Size = new System.Drawing.Size(82, 17);
            this.rbTipoBolha.TabIndex = 4;
            this.rbTipoBolha.Text = "Nível Bolha";
            this.rbTipoBolha.UseVisualStyleBackColor = true;
            this.rbTipoBolha.CheckedChanged += new System.EventHandler(this.rbTipoBolha_CheckedChanged);
            // 
            // rbTipoEsquadro
            // 
            this.rbTipoEsquadro.AutoSize = true;
            this.rbTipoEsquadro.Location = new System.Drawing.Point(19, 21);
            this.rbTipoEsquadro.Name = "rbTipoEsquadro";
            this.rbTipoEsquadro.Size = new System.Drawing.Size(74, 17);
            this.rbTipoEsquadro.TabIndex = 3;
            this.rbTipoEsquadro.Text = "Esquadro";
            this.rbTipoEsquadro.UseVisualStyleBackColor = true;
            this.rbTipoEsquadro.CheckedChanged += new System.EventHandler(this.rbTipoEsquadro_CheckedChanged);
            // 
            // txtUndMedida
            // 
            this.txtUndMedida.Location = new System.Drawing.Point(253, 180);
            this.txtUndMedida.MaxLength = 50;
            this.txtUndMedida.Name = "txtUndMedida";
            this.txtUndMedida.Size = new System.Drawing.Size(109, 22);
            this.txtUndMedida.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 164);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 13);
            this.label13.TabIndex = 103;
            this.label13.Text = "Unidade de Medida:";
            // 
            // txtTolerancia
            // 
            this.txtTolerancia.Location = new System.Drawing.Point(18, 180);
            this.txtTolerancia.MaxLength = 150;
            this.txtTolerancia.Name = "txtTolerancia";
            this.txtTolerancia.Size = new System.Drawing.Size(221, 22);
            this.txtTolerancia.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 13);
            this.label12.TabIndex = 102;
            this.label12.Text = "Tolerância:";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCentroCusto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCentroCusto.FormattingEnabled = true;
            this.cbCentroCusto.Location = new System.Drawing.Point(18, 84);
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(533, 21);
            this.cbCentroCusto.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 68);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 101;
            this.label15.Text = "Centro de Custo:";
            // 
            // txtValidade
            // 
            this.txtValidade.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtValidade.Location = new System.Drawing.Point(143, 236);
            this.txtValidade.Mask = "99/99/9999";
            this.txtValidade.Name = "txtValidade";
            this.txtValidade.ReadOnly = true;
            this.txtValidade.Size = new System.Drawing.Size(110, 22);
            this.txtValidade.TabIndex = 16;
            this.txtValidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtProcessoLocal);
            this.groupBox2.Controls.Add(this.txtResponsavelEquipamento);
            this.groupBox2.Controls.Add(this.txtResponsvelAfericao);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(18, 276);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(564, 176);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Processo e Responsaveis";
            // 
            // txtProcessoLocal
            // 
            this.txtProcessoLocal.Location = new System.Drawing.Point(13, 94);
            this.txtProcessoLocal.Multiline = true;
            this.txtProcessoLocal.Name = "txtProcessoLocal";
            this.txtProcessoLocal.Size = new System.Drawing.Size(540, 66);
            this.txtProcessoLocal.TabIndex = 23;
            // 
            // txtResponsavelEquipamento
            // 
            this.txtResponsavelEquipamento.Location = new System.Drawing.Point(288, 41);
            this.txtResponsavelEquipamento.Name = "txtResponsavelEquipamento";
            this.txtResponsavelEquipamento.Size = new System.Drawing.Size(265, 22);
            this.txtResponsavelEquipamento.TabIndex = 22;
            // 
            // txtResponsvelAfericao
            // 
            this.txtResponsvelAfericao.Location = new System.Drawing.Point(12, 41);
            this.txtResponsvelAfericao.Name = "txtResponsvelAfericao";
            this.txtResponsvelAfericao.Size = new System.Drawing.Size(265, 22);
            this.txtResponsvelAfericao.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(285, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 13);
            this.label10.TabIndex = 51;
            this.label10.Text = "Responsável pelo Equipamento:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 51;
            this.label11.Text = "Processo / Local";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Responsável pela Verificação:";
            // 
            // gbResultadoVerificacao
            // 
            this.gbResultadoVerificacao.Controls.Add(this.rbNaoConforme);
            this.gbResultadoVerificacao.Controls.Add(this.rbConforme);
            this.gbResultadoVerificacao.Location = new System.Drawing.Point(291, 217);
            this.gbResultadoVerificacao.Name = "gbResultadoVerificacao";
            this.gbResultadoVerificacao.Size = new System.Drawing.Size(291, 52);
            this.gbResultadoVerificacao.TabIndex = 17;
            this.gbResultadoVerificacao.TabStop = false;
            this.gbResultadoVerificacao.Text = "Resultado";
            // 
            // rbNaoConforme
            // 
            this.rbNaoConforme.AutoSize = true;
            this.rbNaoConforme.Location = new System.Drawing.Point(147, 22);
            this.rbNaoConforme.Name = "rbNaoConforme";
            this.rbNaoConforme.Size = new System.Drawing.Size(100, 17);
            this.rbNaoConforme.TabIndex = 21;
            this.rbNaoConforme.TabStop = true;
            this.rbNaoConforme.Text = "Não Conforme";
            this.rbNaoConforme.UseVisualStyleBackColor = true;
            // 
            // rbConforme
            // 
            this.rbConforme.AutoSize = true;
            this.rbConforme.Location = new System.Drawing.Point(42, 22);
            this.rbConforme.Name = "rbConforme";
            this.rbConforme.Size = new System.Drawing.Size(76, 17);
            this.rbConforme.TabIndex = 20;
            this.rbConforme.TabStop = true;
            this.rbConforme.Text = "Conforme";
            this.rbConforme.UseVisualStyleBackColor = true;
            // 
            // txtDtAfericao
            // 
            this.txtDtAfericao.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtDtAfericao.Location = new System.Drawing.Point(18, 236);
            this.txtDtAfericao.Mask = "99/99/9999";
            this.txtDtAfericao.Name = "txtDtAfericao";
            this.txtDtAfericao.Size = new System.Drawing.Size(110, 22);
            this.txtDtAfericao.TabIndex = 15;
            this.txtDtAfericao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDtAfericao.TextChanged += new System.EventHandler(this.txtDtAfericao_TextChanged);
            // 
            // txtIdentificacao
            // 
            this.txtIdentificacao.Location = new System.Drawing.Point(352, 133);
            this.txtIdentificacao.Name = "txtIdentificacao";
            this.txtIdentificacao.Size = new System.Drawing.Size(100, 22);
            this.txtIdentificacao.TabIndex = 10;
            // 
            // txtInstrumento
            // 
            this.txtInstrumento.Location = new System.Drawing.Point(18, 133);
            this.txtInstrumento.Name = "txtInstrumento";
            this.txtInstrumento.Size = new System.Drawing.Size(320, 22);
            this.txtInstrumento.TabIndex = 9;
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFornecedor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.Location = new System.Drawing.Point(377, 180);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(205, 21);
            this.cbFornecedor.TabIndex = 14;
            this.cbFornecedor.Visible = false;
            // 
            // txtCodColigada
            // 
            this.txtCodColigada.Location = new System.Drawing.Point(128, 34);
            this.txtCodColigada.Name = "txtCodColigada";
            this.txtCodColigada.ReadOnly = true;
            this.txtCodColigada.Size = new System.Drawing.Size(100, 22);
            this.txtCodColigada.TabIndex = 1;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(18, 34);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(100, 22);
            this.txtCodigo.TabIndex = 0;
            // 
            // cbFrequencia
            // 
            this.cbFrequencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencia.FormattingEnabled = true;
            this.cbFrequencia.Items.AddRange(new object[] {
            "Semanal",
            "Quinzenal",
            "Mensal",
            "Semestral",
            "Anual"});
            this.cbFrequencia.Location = new System.Drawing.Point(467, 134);
            this.cbFrequencia.Name = "cbFrequencia";
            this.cbFrequencia.Size = new System.Drawing.Size(115, 21);
            this.cbFrequencia.TabIndex = 11;
            this.cbFrequencia.SelectedIndexChanged += new System.EventHandler(this.cbFrequencia_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(125, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 94;
            this.label3.Text = "Código Coligada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 95;
            this.label2.Text = "Código Interno:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(374, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 96;
            this.label4.Text = "Fornecedor:";
            this.label4.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(349, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 97;
            this.label6.Text = "Identificação:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(140, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 13);
            this.label8.TabIndex = 98;
            this.label8.Text = "Data de Validade:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 13);
            this.label7.TabIndex = 99;
            this.label7.Text = "Data da Verificação:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 93;
            this.label5.Text = "Instrumento:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(464, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 100;
            this.label1.Text = "Frequência:";
            // 
            // tabPageInspVisual
            // 
            this.tabPageInspVisual.Controls.Add(this.tableLayoutPanel1);
            this.tabPageInspVisual.Location = new System.Drawing.Point(4, 22);
            this.tabPageInspVisual.Name = "tabPageInspVisual";
            this.tabPageInspVisual.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInspVisual.Size = new System.Drawing.Size(606, 465);
            this.tabPageInspVisual.TabIndex = 4;
            this.tabPageInspVisual.Text = "Inspeção Visual";
            this.tabPageInspVisual.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label19, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtObservacoes, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(600, 459);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // rbEquipDanoNao
            // 
            this.rbEquipDanoNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipDanoNao.AutoSize = true;
            this.rbEquipDanoNao.Location = new System.Drawing.Point(125, 6);
            this.rbEquipDanoNao.Name = "rbEquipDanoNao";
            this.rbEquipDanoNao.Size = new System.Drawing.Size(46, 17);
            this.rbEquipDanoNao.TabIndex = 11;
            this.rbEquipDanoNao.TabStop = true;
            this.rbEquipDanoNao.Text = "Não";
            this.rbEquipDanoNao.UseVisualStyleBackColor = true;
            this.rbEquipDanoNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipBemAcondNao
            // 
            this.rbEquipBemAcondNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipBemAcondNao.AutoSize = true;
            this.rbEquipBemAcondNao.Location = new System.Drawing.Point(125, 6);
            this.rbEquipBemAcondNao.Name = "rbEquipBemAcondNao";
            this.rbEquipBemAcondNao.Size = new System.Drawing.Size(46, 17);
            this.rbEquipBemAcondNao.TabIndex = 9;
            this.rbEquipBemAcondNao.TabStop = true;
            this.rbEquipBemAcondNao.Text = "Não";
            this.rbEquipBemAcondNao.UseVisualStyleBackColor = true;
            this.rbEquipBemAcondNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipLimpoNao
            // 
            this.rbEquipLimpoNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipLimpoNao.AutoSize = true;
            this.rbEquipLimpoNao.Location = new System.Drawing.Point(125, 6);
            this.rbEquipLimpoNao.Name = "rbEquipLimpoNao";
            this.rbEquipLimpoNao.Size = new System.Drawing.Size(46, 17);
            this.rbEquipLimpoNao.TabIndex = 7;
            this.rbEquipLimpoNao.TabStop = true;
            this.rbEquipLimpoNao.Text = "Não";
            this.rbEquipLimpoNao.UseVisualStyleBackColor = true;
            this.rbEquipLimpoNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipFuncNao
            // 
            this.rbEquipFuncNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipFuncNao.AutoSize = true;
            this.rbEquipFuncNao.Location = new System.Drawing.Point(125, 6);
            this.rbEquipFuncNao.Name = "rbEquipFuncNao";
            this.rbEquipFuncNao.Size = new System.Drawing.Size(46, 17);
            this.rbEquipFuncNao.TabIndex = 5;
            this.rbEquipFuncNao.TabStop = true;
            this.rbEquipFuncNao.Text = "Não";
            this.rbEquipFuncNao.UseVisualStyleBackColor = true;
            this.rbEquipFuncNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 12);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(262, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "O equipamento está funcionando perfeitamente?";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(297, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "O equipamento está limpo e em boas condições de uso?";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(289, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "O acondicionamento do equipamento está adequado?";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(163, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Há sinais de danos aparentes?";
            // 
            // rbEquipFuncSim
            // 
            this.rbEquipFuncSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipFuncSim.AutoSize = true;
            this.rbEquipFuncSim.Location = new System.Drawing.Point(25, 6);
            this.rbEquipFuncSim.Name = "rbEquipFuncSim";
            this.rbEquipFuncSim.Size = new System.Drawing.Size(43, 17);
            this.rbEquipFuncSim.TabIndex = 4;
            this.rbEquipFuncSim.TabStop = true;
            this.rbEquipFuncSim.Text = "Sim";
            this.rbEquipFuncSim.UseVisualStyleBackColor = true;
            this.rbEquipFuncSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipLimpoSim
            // 
            this.rbEquipLimpoSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipLimpoSim.AutoSize = true;
            this.rbEquipLimpoSim.Location = new System.Drawing.Point(25, 6);
            this.rbEquipLimpoSim.Name = "rbEquipLimpoSim";
            this.rbEquipLimpoSim.Size = new System.Drawing.Size(43, 17);
            this.rbEquipLimpoSim.TabIndex = 6;
            this.rbEquipLimpoSim.TabStop = true;
            this.rbEquipLimpoSim.Text = "Sim";
            this.rbEquipLimpoSim.UseVisualStyleBackColor = true;
            this.rbEquipLimpoSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipBemAcondSim
            // 
            this.rbEquipBemAcondSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipBemAcondSim.AutoSize = true;
            this.rbEquipBemAcondSim.Location = new System.Drawing.Point(25, 6);
            this.rbEquipBemAcondSim.Name = "rbEquipBemAcondSim";
            this.rbEquipBemAcondSim.Size = new System.Drawing.Size(43, 17);
            this.rbEquipBemAcondSim.TabIndex = 8;
            this.rbEquipBemAcondSim.TabStop = true;
            this.rbEquipBemAcondSim.Text = "Sim";
            this.rbEquipBemAcondSim.UseVisualStyleBackColor = true;
            this.rbEquipBemAcondSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbEquipDanoSim
            // 
            this.rbEquipDanoSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbEquipDanoSim.AutoSize = true;
            this.rbEquipDanoSim.Location = new System.Drawing.Point(25, 6);
            this.rbEquipDanoSim.Name = "rbEquipDanoSim";
            this.rbEquipDanoSim.Size = new System.Drawing.Size(43, 17);
            this.rbEquipDanoSim.TabIndex = 10;
            this.rbEquipDanoSim.TabStop = true;
            this.rbEquipDanoSim.Text = "Sim";
            this.rbEquipDanoSim.UseVisualStyleBackColor = true;
            this.rbEquipDanoSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // tabPageEsquadro
            // 
            this.tabPageEsquadro.Controls.Add(this.dgvListaEsquadro);
            this.tabPageEsquadro.Controls.Add(this.tsbEditarEsquadro);
            this.tabPageEsquadro.Location = new System.Drawing.Point(4, 22);
            this.tabPageEsquadro.Name = "tabPageEsquadro";
            this.tabPageEsquadro.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEsquadro.Size = new System.Drawing.Size(606, 465);
            this.tabPageEsquadro.TabIndex = 1;
            this.tabPageEsquadro.Text = "Verificação de Esquadro";
            this.tabPageEsquadro.UseVisualStyleBackColor = true;
            // 
            // dgvListaEsquadro
            // 
            this.dgvListaEsquadro.AccessibleName = "Table";
            this.dgvListaEsquadro.AllowEditing = false;
            this.dgvListaEsquadro.AllowFiltering = true;
            this.dgvListaEsquadro.AllowResizingColumns = true;
            this.dgvListaEsquadro.AutoGenerateColumns = false;
            this.dgvListaEsquadro.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListaEsquadro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaEsquadro.Location = new System.Drawing.Point(3, 28);
            this.dgvListaEsquadro.Name = "dgvListaEsquadro";
            this.dgvListaEsquadro.SelectionMode = Syncfusion.WinForms.DataGrid.Enums.GridSelectionMode.Extended;
            this.dgvListaEsquadro.SelectionUnit = Syncfusion.WinForms.DataGrid.Enums.SelectionUnit.Cell;
            this.dgvListaEsquadro.Size = new System.Drawing.Size(600, 434);
            this.dgvListaEsquadro.Style.HeaderStyle.FilterIconColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.dgvListaEsquadro.TabIndex = 21;
            this.dgvListaEsquadro.Text = "sfDataGrid1";
            this.dgvListaEsquadro.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.dgvListaEsquadro_CellDoubleClick);
            // 
            // tsbEditarEsquadro
            // 
            this.tsbEditarEsquadro.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionarEsquadro,
            this.tsbEditar,
            this.tsbRemoverEsquadro,
            this.toolStripSeparator2,
            this.tsbAtualizarEsquadro});
            this.tsbEditarEsquadro.Location = new System.Drawing.Point(3, 3);
            this.tsbEditarEsquadro.Name = "tsbEditarEsquadro";
            this.tsbEditarEsquadro.Size = new System.Drawing.Size(600, 25);
            this.tsbEditarEsquadro.TabIndex = 22;
            this.tsbEditarEsquadro.Text = "toolStrip1";
            // 
            // tsbAdicionarEsquadro
            // 
            this.tsbAdicionarEsquadro.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionarEsquadro.Image")));
            this.tsbAdicionarEsquadro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionarEsquadro.Name = "tsbAdicionarEsquadro";
            this.tsbAdicionarEsquadro.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionarEsquadro.Size = new System.Drawing.Size(98, 22);
            this.tsbAdicionarEsquadro.Text = "Adicionar";
            this.tsbAdicionarEsquadro.Click += new System.EventHandler(this.tsbAdicionarEsquadro_Click);
            // 
            // tsbEditar
            // 
            this.tsbEditar.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditar.Image")));
            this.tsbEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditar.Name = "tsbEditar";
            this.tsbEditar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbEditar.Size = new System.Drawing.Size(77, 22);
            this.tsbEditar.Text = "Editar";
            this.tsbEditar.Click += new System.EventHandler(this.tsbEditarEsquadro_Click);
            // 
            // tsbRemoverEsquadro
            // 
            this.tsbRemoverEsquadro.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemoverEsquadro.Image")));
            this.tsbRemoverEsquadro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRemoverEsquadro.Name = "tsbRemoverEsquadro";
            this.tsbRemoverEsquadro.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRemoverEsquadro.Size = new System.Drawing.Size(94, 22);
            this.tsbRemoverEsquadro.Text = "Remover";
            this.tsbRemoverEsquadro.Click += new System.EventHandler(this.tsbRemoverEsquadro_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAtualizarEsquadro
            // 
            this.tsbAtualizarEsquadro.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarEsquadro.Image")));
            this.tsbAtualizarEsquadro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarEsquadro.Name = "tsbAtualizarEsquadro";
            this.tsbAtualizarEsquadro.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarEsquadro.Size = new System.Drawing.Size(93, 22);
            this.tsbAtualizarEsquadro.Text = "Atualizar";
            this.tsbAtualizarEsquadro.Click += new System.EventHandler(this.tsbAtualizarEsquadro_Click);
            // 
            // tabPageNivelBolha
            // 
            this.tabPageNivelBolha.Controls.Add(this.tableLayoutPanel2);
            this.tabPageNivelBolha.Location = new System.Drawing.Point(4, 22);
            this.tabPageNivelBolha.Name = "tabPageNivelBolha";
            this.tabPageNivelBolha.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageNivelBolha.Size = new System.Drawing.Size(606, 465);
            this.tabPageNivelBolha.TabIndex = 2;
            this.tabPageNivelBolha.Text = "Verificação de Nível Bolha";
            this.tabPageNivelBolha.UseVisualStyleBackColor = true;
            // 
            // tabPageTrena
            // 
            this.tabPageTrena.Controls.Add(this.dgvListaTrena);
            this.tabPageTrena.Controls.Add(this.toolStrip1);
            this.tabPageTrena.Location = new System.Drawing.Point(4, 22);
            this.tabPageTrena.Name = "tabPageTrena";
            this.tabPageTrena.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTrena.Size = new System.Drawing.Size(606, 465);
            this.tabPageTrena.TabIndex = 3;
            this.tabPageTrena.Text = "Verificação de Trena";
            this.tabPageTrena.UseVisualStyleBackColor = true;
            // 
            // dgvListaTrena
            // 
            this.dgvListaTrena.AccessibleName = "Table";
            this.dgvListaTrena.AllowEditing = false;
            this.dgvListaTrena.AllowFiltering = true;
            this.dgvListaTrena.AllowResizingColumns = true;
            this.dgvListaTrena.AutoGenerateColumns = false;
            this.dgvListaTrena.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListaTrena.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListaTrena.Location = new System.Drawing.Point(3, 28);
            this.dgvListaTrena.Name = "dgvListaTrena";
            this.dgvListaTrena.SelectionMode = Syncfusion.WinForms.DataGrid.Enums.GridSelectionMode.Extended;
            this.dgvListaTrena.SelectionUnit = Syncfusion.WinForms.DataGrid.Enums.SelectionUnit.Cell;
            this.dgvListaTrena.Size = new System.Drawing.Size(600, 434);
            this.dgvListaTrena.Style.HeaderStyle.FilterIconColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(29)))), ((int)(((byte)(29)))));
            this.dgvListaTrena.TabIndex = 23;
            this.dgvListaTrena.Text = "sfDataGrid1";
            this.dgvListaTrena.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.dgvListaTrena_CellDoubleClick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionarTrena,
            this.tsbEditarTrena,
            this.tsbRemoverTrena,
            this.toolStripSeparator1,
            this.tsbAtualizarTrena});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(600, 25);
            this.toolStrip1.TabIndex = 24;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbAdicionarTrena
            // 
            this.tsbAdicionarTrena.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionarTrena.Image")));
            this.tsbAdicionarTrena.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionarTrena.Name = "tsbAdicionarTrena";
            this.tsbAdicionarTrena.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionarTrena.Size = new System.Drawing.Size(98, 22);
            this.tsbAdicionarTrena.Text = "Adicionar";
            this.tsbAdicionarTrena.Click += new System.EventHandler(this.tsbAdicionarTrena_Click);
            // 
            // tsbEditarTrena
            // 
            this.tsbEditarTrena.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditarTrena.Image")));
            this.tsbEditarTrena.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditarTrena.Name = "tsbEditarTrena";
            this.tsbEditarTrena.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbEditarTrena.Size = new System.Drawing.Size(77, 22);
            this.tsbEditarTrena.Text = "Editar";
            this.tsbEditarTrena.Click += new System.EventHandler(this.tsbEditarTrena_Click);
            // 
            // tsbRemoverTrena
            // 
            this.tsbRemoverTrena.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemoverTrena.Image")));
            this.tsbRemoverTrena.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRemoverTrena.Name = "tsbRemoverTrena";
            this.tsbRemoverTrena.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbRemoverTrena.Size = new System.Drawing.Size(94, 22);
            this.tsbRemoverTrena.Text = "Remover";
            this.tsbRemoverTrena.Click += new System.EventHandler(this.tsbRemoverTrena_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbAtualizarTrena
            // 
            this.tsbAtualizarTrena.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarTrena.Image")));
            this.tsbAtualizarTrena.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarTrena.Name = "tsbAtualizarTrena";
            this.tsbAtualizarTrena.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarTrena.Size = new System.Drawing.Size(93, 22);
            this.tsbAtualizarTrena.Text = "Atualizar";
            this.tsbAtualizarTrena.Click += new System.EventHandler(this.tsbAtualizarTrena_Click);
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 156);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Observações:";
            // 
            // txtObservacoes
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtObservacoes, 3);
            this.txtObservacoes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservacoes.Location = new System.Drawing.Point(4, 184);
            this.txtObservacoes.Name = "txtObservacoes";
            this.txtObservacoes.Size = new System.Drawing.Size(592, 271);
            this.txtObservacoes.TabIndex = 13;
            this.txtObservacoes.Text = "";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(600, 459);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // rbF069NiveladoNao
            // 
            this.rbF069NiveladoNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbF069NiveladoNao.AutoSize = true;
            this.rbF069NiveladoNao.Location = new System.Drawing.Point(125, 6);
            this.rbF069NiveladoNao.Name = "rbF069NiveladoNao";
            this.rbF069NiveladoNao.Size = new System.Drawing.Size(46, 17);
            this.rbF069NiveladoNao.TabIndex = 7;
            this.rbF069NiveladoNao.TabStop = true;
            this.rbF069NiveladoNao.Text = "Não";
            this.rbF069NiveladoNao.UseVisualStyleBackColor = true;
            this.rbF069NiveladoNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbF069BolhaCentNao
            // 
            this.rbF069BolhaCentNao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbF069BolhaCentNao.AutoSize = true;
            this.rbF069BolhaCentNao.Location = new System.Drawing.Point(125, 6);
            this.rbF069BolhaCentNao.Name = "rbF069BolhaCentNao";
            this.rbF069BolhaCentNao.Size = new System.Drawing.Size(46, 17);
            this.rbF069BolhaCentNao.TabIndex = 5;
            this.rbF069BolhaCentNao.TabStop = true;
            this.rbF069BolhaCentNao.Text = "Não";
            this.rbF069BolhaCentNao.UseVisualStyleBackColor = true;
            this.rbF069BolhaCentNao.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 12);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(277, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Bolha está centralizada nas duas leituras realizadas? ";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(4, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(314, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "O local de aferição está devidamente nivelado pelo padrão?";
            // 
            // rbF069BolhaCentSim
            // 
            this.rbF069BolhaCentSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbF069BolhaCentSim.AutoSize = true;
            this.rbF069BolhaCentSim.Location = new System.Drawing.Point(25, 6);
            this.rbF069BolhaCentSim.Name = "rbF069BolhaCentSim";
            this.rbF069BolhaCentSim.Size = new System.Drawing.Size(43, 17);
            this.rbF069BolhaCentSim.TabIndex = 4;
            this.rbF069BolhaCentSim.TabStop = true;
            this.rbF069BolhaCentSim.Text = "Sim";
            this.rbF069BolhaCentSim.UseVisualStyleBackColor = true;
            this.rbF069BolhaCentSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // rbF069NiveladoSim
            // 
            this.rbF069NiveladoSim.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbF069NiveladoSim.AutoSize = true;
            this.rbF069NiveladoSim.Location = new System.Drawing.Point(25, 6);
            this.rbF069NiveladoSim.Name = "rbF069NiveladoSim";
            this.rbF069NiveladoSim.Size = new System.Drawing.Size(43, 17);
            this.rbF069NiveladoSim.TabIndex = 6;
            this.rbF069NiveladoSim.TabStop = true;
            this.rbF069NiveladoSim.Text = "Sim";
            this.rbF069NiveladoSim.UseVisualStyleBackColor = true;
            this.rbF069NiveladoSim.CheckedChanged += new System.EventHandler(this.ChecaResulConfCheckedChanged);
            // 
            // panel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 2);
            this.panel1.Controls.Add(this.rbEquipFuncSim);
            this.panel1.Controls.Add(this.rbEquipFuncNao);
            this.panel1.Location = new System.Drawing.Point(401, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(195, 29);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel2, 2);
            this.panel2.Controls.Add(this.rbEquipLimpoSim);
            this.panel2.Controls.Add(this.rbEquipLimpoNao);
            this.panel2.Location = new System.Drawing.Point(401, 40);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(195, 29);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.rbEquipBemAcondSim);
            this.panel3.Controls.Add(this.rbEquipBemAcondNao);
            this.panel3.Location = new System.Drawing.Point(401, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(195, 29);
            this.panel3.TabIndex = 16;
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this.rbEquipDanoSim);
            this.panel4.Controls.Add(this.rbEquipDanoNao);
            this.panel4.Location = new System.Drawing.Point(401, 112);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(195, 29);
            this.panel4.TabIndex = 17;
            // 
            // panel5
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel5, 2);
            this.panel5.Controls.Add(this.rbF069BolhaCentSim);
            this.panel5.Controls.Add(this.rbF069BolhaCentNao);
            this.panel5.Location = new System.Drawing.Point(401, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(195, 29);
            this.panel5.TabIndex = 8;
            // 
            // panel6
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.panel6, 2);
            this.panel6.Controls.Add(this.rbF069NiveladoSim);
            this.panel6.Controls.Add(this.rbF069NiveladoNao);
            this.panel6.Location = new System.Drawing.Point(401, 40);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(195, 29);
            this.panel6.TabIndex = 9;
            // 
            // FormAfericao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(614, 536);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAfericao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Verificação Interna";
            this.Load += new System.EventHandler(this.FormAfericao_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPageInfo.ResumeLayout(false);
            this.tabPageInfo.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbResultadoVerificacao.ResumeLayout(false);
            this.gbResultadoVerificacao.PerformLayout();
            this.tabPageInspVisual.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPageEsquadro.ResumeLayout(false);
            this.tabPageEsquadro.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaEsquadro)).EndInit();
            this.tsbEditarEsquadro.ResumeLayout(false);
            this.tsbEditarEsquadro.PerformLayout();
            this.tabPageNivelBolha.ResumeLayout(false);
            this.tabPageTrena.ResumeLayout(false);
            this.tabPageTrena.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaTrena)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.TextBox txtUndMedida;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTolerancia;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbCentroCusto;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox txtValidade;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtProcessoLocal;
        private System.Windows.Forms.TextBox txtResponsavelEquipamento;
        private System.Windows.Forms.TextBox txtResponsvelAfericao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gbResultadoVerificacao;
        private System.Windows.Forms.MaskedTextBox txtDtAfericao;
        private System.Windows.Forms.TextBox txtIdentificacao;
        private System.Windows.Forms.TextBox txtInstrumento;
        private System.Windows.Forms.ComboBox cbFornecedor;
        private System.Windows.Forms.TextBox txtCodColigada;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.ComboBox cbFrequencia;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPageEsquadro;
        private System.Windows.Forms.TabPage tabPageNivelBolha;
        private System.Windows.Forms.TabPage tabPageTrena;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbTipoOutros;
        private System.Windows.Forms.RadioButton rbTipoTrena;
        private System.Windows.Forms.RadioButton rbTipoBolha;
        private System.Windows.Forms.RadioButton rbTipoEsquadro;
        private System.Windows.Forms.Button btnBuscaCCusto;
        private System.Windows.Forms.RadioButton rbNaoConforme;
        private System.Windows.Forms.RadioButton rbConforme;
        private System.Windows.Forms.TabPage tabPageInspVisual;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton rbEquipDanoNao;
        private System.Windows.Forms.RadioButton rbEquipBemAcondNao;
        private System.Windows.Forms.RadioButton rbEquipLimpoNao;
        private System.Windows.Forms.RadioButton rbEquipFuncNao;
        private System.Windows.Forms.RadioButton rbEquipFuncSim;
        private System.Windows.Forms.RadioButton rbEquipLimpoSim;
        private System.Windows.Forms.RadioButton rbEquipBemAcondSim;
        private System.Windows.Forms.RadioButton rbEquipDanoSim;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListaEsquadro;
        private System.Windows.Forms.ToolStrip tsbEditarEsquadro;
        private System.Windows.Forms.ToolStripButton tsbAdicionarEsquadro;
        private System.Windows.Forms.ToolStripButton tsbEditar;
        private System.Windows.Forms.ToolStripButton tsbRemoverEsquadro;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbAtualizarEsquadro;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListaTrena;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbAdicionarTrena;
        private System.Windows.Forms.ToolStripButton tsbEditarTrena;
        private System.Windows.Forms.ToolStripButton tsbRemoverTrena;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbAtualizarTrena;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RichTextBox txtObservacoes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton rbF069NiveladoNao;
        private System.Windows.Forms.RadioButton rbF069BolhaCentNao;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton rbF069BolhaCentSim;
        private System.Windows.Forms.RadioButton rbF069NiveladoSim;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
    }
}