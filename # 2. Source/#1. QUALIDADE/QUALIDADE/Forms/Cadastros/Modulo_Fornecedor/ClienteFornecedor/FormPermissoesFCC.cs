﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Windows.Forms.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ClienteFornecedor
{
    public partial class FormPermissoesFCC : Form
    {
        private FCFO fornecedor;

        public FormPermissoesFCC(FCFO cFornecedor)
        {
            InitializeComponent();
            this.fornecedor = cFornecedor;
            this.Text = $"Permissões do Fornecedor - < {fornecedor.CODCFO} - {fornecedor.NOME} >";
        }

        private void FormPermissoes_Load(object sender, EventArgs e)
        {
            CarregaListagemCCusto();
            CarregaPermissoes();
        }

        private void CarregaPermissoes()
        {
            using (ControleFPermissao controle = new ControleFPermissao())
            {
                List<FPERMISSAO> permissao = controle.Get(x =>
                    x.CODCOLIGADA == fornecedor.CODCOLIGADA &&
                    x.CODCFO == fornecedor.CODCFO
                ).ToList();

                if (permissao != null)
                {
                    foreach (TreeNodeAdv no in tvPermissao.Nodes)
                    {
                        foreach (TreeNodeAdv node in no.Nodes)
                        {
                            string[] centroCusto = node.Text.Trim().Split('-');

                            foreach (TreeNodeAdv nodeNo in node.Nodes)
                            {
                                centroCusto = nodeNo.Text.Trim().Split('-');

                                if (permissao.Where(x => x.CODCCUSTO == centroCusto[0].DBNullToString()).Count() > 0)
                                    nodeNo.Checked = true;
                                else
                                    nodeNo.Checked = false;
                            }

                            centroCusto = node.Text.Trim().Split('-');

                            if (permissao.Where(x => x.CODCCUSTO == centroCusto[0].DBNullToString()).Count() > 0)
                                node.Checked = true;
                            else
                                node.Checked = false;
                        }
                    }
                }
            }
        }


        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CarregaListagemCCusto()
        {
            using (ControleGEmpresas controle = new ControleGEmpresas())
            {
                using (ControleGCentroCusto controleCCusto = new ControleGCentroCusto())
                {
                    List<GEMPRESAS> listGEmpresas = new List<GEMPRESAS>();
                    GEMPRESAS empr = controle.Find(fornecedor.CODCOLIGADA);
                    listGEmpresas.Add(empr);

                    int cont = 0;

                    foreach (GEMPRESAS empresa in listGEmpresas)
                    {
                        List<GCENTROCUSTO> listGCentroCusto = controleCCusto.Get(
                            x => x.CODCOLIGADA == empresa.CODCOLIGADA).ToList();

                        tvPermissao.Nodes.Add(
                            new Syncfusion.Windows.Forms.Tools.TreeNodeAdv
                            {
                                Text = $"{empresa.CODCOLIGADA} - {empresa.NOME}",
                                TextColor = Color.Maroon,
                                Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold),
                                Name = $"{empresa.CODCOLIGADA}"
                            }
                        );

                        int contNv1 = 0;
                        foreach (GCENTROCUSTO cCusto in listGCentroCusto.Where(x => x.CODCCUSTO.Length == 1))
                        {

                            tvPermissao.Nodes[cont].Nodes.Add(new Syncfusion.Windows.Forms.Tools.TreeNodeAdv
                            {
                                Text = $"{cCusto.CODCCUSTO} - {cCusto.NOME}",
                                Name = $"{empresa.CODCOLIGADA}"
                            });

                            foreach (GCENTROCUSTO cSubCusto in listGCentroCusto.Where(x => x.CODCCUSTO.Length == 6 && x.CODCCUSTO.Substring(0, 1) == cCusto.CODCCUSTO))
                            {
                                tvPermissao.Nodes[cont].Nodes[contNv1].Nodes.Add(
                                    new Syncfusion.Windows.Forms.Tools.TreeNodeAdv
                                    {
                                        Text = $"{cSubCusto.CODCCUSTO} - {cSubCusto.NOME}",
                                        Name = $"{empresa.CODCOLIGADA}"
                                    }
                                );
                            }
                            contNv1++;
                        }
                        cont++;
                    }
                }
            }
        }

        private void tvPermissao_AfterCheck(object sender, Syncfusion.Windows.Forms.Tools.TreeNodeAdvEventArgs e)
        {
            foreach (Syncfusion.Windows.Forms.Tools.TreeNodeAdv n in e.Node.Nodes)
            {
                n.Checked = e.Node.Checked;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            List<FPERMISSAO> novasPermissoes = ListaPermissoes();
            try
            {
                using (ControleFPermissao controle = new ControleFPermissao())
                {
                    controle.Delete(x => x.CODCFO == fornecedor.CODCFO && x.CODCOLIGADA == fornecedor.CODCOLIGADA);
                    controle.SaveAll();

                    controle.CreateAll(novasPermissoes);
                    controle.SaveAll();
                }

                System.Windows.Forms.Cursor.Current = Cursors.Default;
                Global.MsgSucesso("Permissões atribuidas com sucesso");
                this.Dispose();

            }
            catch (Exception ex)
            {

                System.Windows.Forms.Cursor.Current = Cursors.Default;
                FormMsg frm = new FormMsg(ex.InnerException.Message);
                frm.ShowDialog();
            }
        }

        private List<FPERMISSAO> ListaPermissoes()
        {
            List<FPERMISSAO> permissoes = new List<FPERMISSAO>();

            foreach (TreeNodeAdv node in tvPermissao.CheckedNodes)
            {
                string[] centroCusto = node.Text.Trim().Split('-');

                if(centroCusto[0].Length > 1)
                {
                    FPERMISSAO permissao = new FPERMISSAO
                    {
                        CODCCUSTO = centroCusto[0].DBNullToString(),
                        CODCOLIGADA = Convert.ToInt16(node.Name),
                        CODCFO = fornecedor.CODCFO,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now,
                        RECMODIFIEDBY = null,
                        RECMODIFIEDON = null
                    };

                    permissoes.Add(permissao);
                }
            }

            return permissoes;
        }
    }
}