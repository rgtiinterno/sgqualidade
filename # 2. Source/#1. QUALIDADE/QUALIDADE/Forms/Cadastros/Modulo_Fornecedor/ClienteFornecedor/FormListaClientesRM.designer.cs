﻿namespace QUALIDADE.Forms.Cadastros.ClienteFornecedor
{
    partial class FormListaClientesRM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaClientesRM));
            this.dgvListagem = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tslTotalItens = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.fCFOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tsbSelecionar = new System.Windows.Forms.ToolStripButton();
            this.tsbExportar = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizar = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCFOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvListagem
            // 
            this.dgvListagem.AccessibleName = "Table";
            this.dgvListagem.AllowDraggingColumns = true;
            this.dgvListagem.AllowEditing = false;
            this.dgvListagem.AllowFiltering = true;
            this.dgvListagem.AllowResizingColumns = true;
            this.dgvListagem.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListagem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvListagem.DataSource = this.fCFOBindingSource;
            this.dgvListagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListagem.Location = new System.Drawing.Point(0, 25);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.PreviewRowPadding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.dgvListagem.SerializationController = null;
            this.dgvListagem.Size = new System.Drawing.Size(784, 414);
            this.dgvListagem.TabIndex = 4;
            this.dgvListagem.Text = "sfDataGrid1";
            this.dgvListagem.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.dgvListagem_CellDoubleClick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelecionar,
            this.toolStripSeparator4,
            this.tsbExportar,
            this.tsbAtualizar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tslTotalItens
            // 
            this.tslTotalItens.Name = "tslTotalItens";
            this.tslTotalItens.Size = new System.Drawing.Size(40, 17);
            this.tslTotalItens.Text = "           ";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslTotalItens});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // fCFOBindingSource
            // 
            this.fCFOBindingSource.DataSource = typeof(QUALIDADE.Dominio.FCFO);
            // 
            // tsbSelecionar
            // 
            this.tsbSelecionar.Image = global::QUALIDADE.Properties.Resources.todo_list_16px;
            this.tsbSelecionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelecionar.Name = "tsbSelecionar";
            this.tsbSelecionar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbSelecionar.Size = new System.Drawing.Size(101, 22);
            this.tsbSelecionar.Text = "Selecionar";
            this.tsbSelecionar.Click += new System.EventHandler(this.tsbSelecionar_Click);
            // 
            // tsbExportar
            // 
            this.tsbExportar.Image = global::QUALIDADE.Properties.Resources.iconfinder_export_3855597__2_;
            this.tsbExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExportar.Name = "tsbExportar";
            this.tsbExportar.Size = new System.Drawing.Size(71, 22);
            this.tsbExportar.Text = "Exportar";
            this.tsbExportar.Click += new System.EventHandler(this.exportarParaXLSToolStripMenuItem_Click);
            // 
            // tsbAtualizar
            // 
            this.tsbAtualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizar.Image")));
            this.tsbAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizar.Name = "tsbAtualizar";
            this.tsbAtualizar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizar.Size = new System.Drawing.Size(93, 22);
            this.tsbAtualizar.Text = "Atualizar";
            this.tsbAtualizar.Click += new System.EventHandler(this.tsbAtualizar_Click);
            // 
            // FormListaClientesRM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.dgvListagem);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "FormListaClientesRM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listagem de Clientes e Fornecedores - CorporeRM";
            this.Load += new System.EventHandler(this.FListaClientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCFOBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListagem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbSelecionar;
        private System.Windows.Forms.ToolStripButton tsbAtualizar;
        private System.Windows.Forms.ToolStripStatusLabel tslTotalItens;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.BindingSource fCFOBindingSource;
        private System.Windows.Forms.ToolStripButton tsbExportar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}