﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ClienteFornecedor
{
    public partial class FormListaClientes : Form
    {
        public FormListaClientes()
        {
            InitializeComponent();
        }

        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"ClientFornc.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }

        private void FListaClientes_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCFO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOME", HeaderText = "Razão Social", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEFANTASIA", HeaderText = "Nome Fantasia", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PAGREC", HeaderText = "Classificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCPAGREC", HeaderText = "Classificação", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PESSOAFISOUJUR", HeaderText = "Pessoa Física/Jurídica", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCPESSOAFISOUJUR", HeaderText = "Tp. Inscrição", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CGCCFO", HeaderText = "Núm. Inscrição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCTIPOFCFO", HeaderText = "Tipo de Fornecedor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "STATUS", HeaderText = "Ativo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ATIVO", HeaderText = "Ativo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "IDCFO", HeaderText = "ID", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RAMOATIV", HeaderText = "Ramo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPOCONTRIBUINTEINSS", HeaderText = "Tp. Contrib. INSS", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NACIONALIDADE", HeaderText = "Nacionalidade", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CALCULAAVP", HeaderText = "Calcula AVP", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ENTIDADEEXECUTORAPAA", HeaderText = "Ent. Executora", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "APOSENTADOOUPENSIONISTA", HeaderText = "Apos. ou Pens.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "INSCRESTADUAL", HeaderText = "Insc. Estadual", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RUA", HeaderText = "Rua", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMERO", HeaderText = "Número", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "COMPLEMENTO", HeaderText = "Complemento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "BAIRRO", HeaderText = "Bairro", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CIDADE", HeaderText = "Cidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODETD", HeaderText = "Cód. Estado", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CEP", HeaderText = "CEP", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TELEFONE", HeaderText = "Tel. Fixo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TELEX", HeaderText = "Tel. Celular", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EMAIL", HeaderText = "E-mail", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CONTATO", HeaderText = "Contato", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PAIS", HeaderText = "País", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPORUA", HeaderText = "Tp. Rua", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPOBAIRRO", HeaderText = "Tp. Bairro", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODMUNICIPIO", HeaderText = "Cód. Municipio", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FAX", HeaderText = "Fax", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPFCFO", HeaderText = "CODTPFCFO", AllowEditing = false, AllowFiltering = true, Visible = false });
            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleFCFO controle = new ControleFCFO())
            {
                Cursor.Current = Cursors.WaitCursor;
                fCFOBindingSource = new BindingSource();
                fCFOBindingSource.DataSource = typeof(FCFO);
                fCFOBindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                dgvListagem.DataSource = fCFOBindingSource;
                fCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null && dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbInativar.Enabled = true;
                    tsbPermissao.Enabled = true;
                    tsbExcluir.Enabled = true;
                    tsbAtualizar.Enabled = true;

                    if (FormPrincipal.getUsuarioAcesso().GERENTE)
                    {
                        tsbInativar.Enabled = true;
                        tsbPermissao.Enabled = true;
                    }
                    else
                    {
                        tsbInativar.Enabled = false;
                        tsbPermissao.Enabled = false;
                    }
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbPermissao.Enabled = false;
                    tsbExcluir.Enabled = false;
                    tsbInativar.Enabled = false;
                    tsbAtualizar.Enabled = true;

                    if (FormPrincipal.getUsuarioAcesso().GERENTE)
                    {
                        tsbInativar.Enabled = false;
                        tsbPermissao.Enabled = false;
                    }
                    else
                    {
                        tsbInativar.Enabled = false;
                        tsbPermissao.Enabled = false;
                    }
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormCFornecedor frm = new FormCFornecedor())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                using (FormCFornecedor frm = new FormCFornecedor(fornecedor))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                        frm.Dispose();
                    }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                try
                {
                    using (ControleFCFO controle = new ControleFCFO())
                    {
                        controle.Delete(x => x.CODCOLIGADA == fornecedor.CODCOLIGADA && x.CODCFO == fornecedor.CODCFO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void tsbInativar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                try
                {
                    using (ControleFCFO controle = new ControleFCFO())
                    {
                        fornecedor.ATIVO = (fornecedor.ATIVO == Convert.ToInt16(0)) ?
                            Convert.ToInt16(1) : Convert.ToInt16(0);

                        controle.Update(fornecedor, x => x.CODCOLIGADA == fornecedor.CODCOLIGADA &&
                            x.CODCFO == fornecedor.CODCFO);

                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            if (dgvListagem.SelectedItem is FCFO fornecedor)
            {
                using (FormCFornecedor frm = new FormCFornecedor(fornecedor))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbPermissao_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                using (FormPermissoesFCC frm = new FormPermissoesFCC(fornecedor))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a ação.");
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            if(e != null && e.RowData is FCFO fornec)
            {
                if (fornec.DOCVENCIDO > 0)
                    e.Style.BackColor = Color.FromArgb(255, 255, 192, 192);
                else if(fornec.DOCVENCEATE30D > 0)
                    e.Style.BackColor = Color.FromArgb(255, 255, 255, 192);
                else
                    e.Style.BackColor = Color.White;
            }
        }
    }
}
