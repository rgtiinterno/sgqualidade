﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ClienteFornecedor
{
    public partial class FormCFornecedor : Form
    {
        private FCFO itemEdit;
        public bool salvo = false;

        public FormCFornecedor(FCFO FCliente = null)
        {
            InitializeComponent();
            itemEdit = FCliente;
        }

        private void FormCFornecedor_Load(object sender, EventArgs e)
        {
            CarregaCombos();
            if (itemEdit == null)
            {
                using (ControleFCFO controle = new ControleFCFO())
                {
                    txtCodCliente.Text = controle.GetCodigo(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                }
            }
            else
            {
                CarregaDados();
            }
        }

        private void btnPesquisaCNPJ_Click(object sender, EventArgs e)
        {
            using (FormListaClientesRM frm = new FormListaClientesRM())
            {
                frm.ShowDialog();
                if(frm._selecionado != null)
                {
                    cbFornecedor.SelectedValue = frm._selecionado.CODTPFCFO;
                    txtNomeFantasia.Text = frm._selecionado.NOMEFANTASIA;
                    txtNome.Text = frm._selecionado.NOME;
                    cbClassificacao.SelectedIndex = Convert.ToInt16(frm._selecionado.PAGREC) - 1;
                    cbTipoInscricao.SelectedIndex = (frm._selecionado.PESSOAFISOUJUR == "F") ? 0 : 1;
                    txtNumeroInscricao.Text = frm._selecionado.CGCCFO;
                    txtInscricaoEstadual.Text = frm._selecionado.INSCRESTADUAL;
                    txtCEP.Text = frm._selecionado.CEP;
                    txtRua.Text = frm._selecionado.RUA;
                    txtNumero.Text = frm._selecionado.NUMERO;
                    txtComplemento.Text = frm._selecionado.COMPLEMENTO;
                    txtBairro.Text = frm._selecionado.BAIRRO;
                    cbPais.Text = frm._selecionado.PAIS;
                    cbEstado.SelectedValue = frm._selecionado.CODETD;
                    cbMunicipio.SelectedValue = frm._selecionado.CODMUNICIPIO;
                    txtTelefone.Text = frm._selecionado.TELEFONE;
                    txtTelCelular.Text = frm._selecionado.TELEX;
                    txtEmail.Text = frm._selecionado.EMAIL;
                    txtNomeContato.Text = frm._selecionado.CONTATO;
                }
            }

            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    if (!string.IsNullOrWhiteSpace(txtNumeroInscricao.Text))
            //    {

            //        FCliente cliente = BuscaFCliente.Buscar(txtNumeroInscricao.Text);

            //        if (cliente != null)
            //        {
            //            txtNomeFantasia.Text = cliente.fantasia;
            //            txtNome.Text = cliente.nome;
            //            txtCEP.Text = cliente.cep;
            //            txtRua.Text = cliente.logradouro;
            //            txtNumero.Text = cliente.numero;
            //            txtComplemento.Text = cliente.complemento;
            //            txtBairro.Text = cliente.bairro;
            //            cbEstado.SelectedValue = cliente.uf;
            //            cbMunicipio.Text = cliente.municipio;
            //            txtTelefone.Text = cliente.telefone.Replace("(", "").
            //                Replace(")", "").Replace("-", "").Trim();
            //            txtEmail.Text = cliente.email;
            //            cbPais.Text = "Brasil";

            //            btnPesquisaCEP.PerformClick();
            //        }
            //        else
            //        {
            //            MessageBox.Show("CPF/CNPJ não localizado...");
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Informe um CPF/CNPJ válido");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //Cursor.Current = Cursors.Default;
        }

        private void btnPesquisaCEP_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (!string.IsNullOrWhiteSpace(txtCEP.Text))
                {
                    Endereco endereco = BuscaCEP.Buscar(txtCEP.Text);

                    if (endereco != null)
                    {
                        cbEstado.SelectedValue = endereco.UF;
                        cbMunicipio.Text = endereco.Cidade;
                        txtBairro.Text = endereco.Bairro;
                        txtRua.Text = string.Format("{0} {1}", endereco.TipoLogradouro, endereco.Logradouro);
                        txtNumero.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Cep não localizado...");
                    }
                }
                else
                {
                    MessageBox.Show("Informe um CEP válido");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (ControleGMunicipios controle = new ControleGMunicipios())
            {
                if (cbEstado.SelectedIndex > -1)
                {

                    cbMunicipio.DataSource = controle.Get(x => x.CODETDMUNICIPIO == Convert.ToString(cbEstado.SelectedValue)).ToList();
                    cbMunicipio.DisplayMember = "NOMEMUNICIPIO";
                    cbMunicipio.ValueMember = "CODMUNICIPIO";
                    cbMunicipio.SelectedIndex = -1;
                }
                else
                {
                    cbMunicipio.DataSource = controle.GetAll().ToList();
                    cbMunicipio.DisplayMember = "NOMEMUNICIPIO";
                    cbMunicipio.ValueMember = "CODMUNICIPIO";
                    cbMunicipio.SelectedIndex = 0;
                }
            }
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtNomeFantasia.Text))
            {
                msg += " - Informe o nome fantasia da empresa.\n";
            }

            if (string.IsNullOrEmpty(txtNome.Text))
            {
                msg += " - Informe o nome da empresa.\n";
            }

            if (cbClassificacao.SelectedIndex < 0)
            {
                msg += " - Informe a classificação financeira.\n";
            }

            if (string.IsNullOrEmpty(txtNumeroInscricao.Text.Replace(".", "").Replace("/", "").Trim()))
            {
                msg += " - Informe a classificação financeira.\n";
            }

            if (cbTipoInscricao.SelectedIndex < 0)
            {
                msg += " - Informe o tipo de inscrição.\n";
            }

            if (string.IsNullOrEmpty(txtCEP.Text) && !Global.ValidaCEP(txtCEP.Text))
            {
                msg += " - Informe um CEP valido.\n";
            }

            if (string.IsNullOrEmpty(txtRua.Text))
            {
                msg += " - Informe a rua.\n";
            }

            if (string.IsNullOrEmpty(txtNumero.Text))
            {
                msg += " - Informe o número.\n";
            }

            if (string.IsNullOrEmpty(txtBairro.Text))
            {
                msg += " - Informe o bairro.\n";
            }

            if (cbEstado.SelectedIndex < 0)
            {
                msg += " - Informe o Estado.\n";
            }

            if (cbPais.SelectedIndex < 0)
            {
                msg += " - Informe o País.\n";
            }

            //if (string.IsNullOrEmpty(txtTelefone.Text))
            //{
            //    msg += " - Informe um telefone para contato.\n";
            //}

            //if (string.IsNullOrEmpty(txtNomeContato.Text))
            //{
            //    msg += " - Informe um contato.\n";
            //}

            //if (string.IsNullOrEmpty(txtEmail.Text) && !Global.ValidaEmail(txtEmail.Text))
            //{
            //    msg += " - Informe um e-mail valído.\n";
            //}

            if (string.IsNullOrEmpty(msg))
            {
                return true;
            }
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void cbTipoInscricao_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTipoInscricao.SelectedIndex == 0)
            {
                txtNumeroInscricao.Mask = "000,000,000-00";
            }
            else
            {
                txtNumeroInscricao.Mask = "00,000,000/0000-00";
            }
        }

        private void CarregaDados()
        {
            txtCodCliente.Text = itemEdit.CODCFO;
            cbFornecedor.SelectedValue = itemEdit.CODTPFCFO;
            txtNomeFantasia.Text = itemEdit.NOMEFANTASIA;
            txtNome.Text = itemEdit.NOME;
            cbClassificacao.SelectedIndex = Convert.ToInt16(itemEdit.PAGREC) - 1;
            cbTipoInscricao.SelectedIndex = (itemEdit.PESSOAFISOUJUR == "F") ? 0 : 1;
            txtNumeroInscricao.Text = itemEdit.CGCCFO;
            txtInscricaoEstadual.Text = itemEdit.INSCRESTADUAL;
            txtCEP.Text = itemEdit.CEP;
            txtRua.Text = itemEdit.RUA;
            txtNumero.Text = itemEdit.NUMERO;
            txtComplemento.Text = itemEdit.COMPLEMENTO;
            txtBairro.Text = itemEdit.BAIRRO;
            cbPais.Text = itemEdit.PAIS;
            cbEstado.SelectedValue = itemEdit.CODETD;
            cbMunicipio.SelectedValue = itemEdit.CODMUNICIPIO;
            txtTelefone.Text = itemEdit.TELEFONE;
            txtTelCelular.Text = itemEdit.TELEX;
            txtEmail.Text = itemEdit.EMAIL;
            txtNomeContato.Text = itemEdit.CONTATO;
        }

        private void CarregaCombos()
        {
            using (ControleGEstados controle = new ControleGEstados())
            {
                cbEstado.DataSource = controle.GetAll().ToList();
                cbEstado.DisplayMember = "NOME";
                cbEstado.ValueMember = "CODETD";
                cbEstado.SelectedIndex = -1;
            }

            using (ControleGPais controle = new ControleGPais())
            {
                cbPais.DataSource = controle.GetAll().ToList();
                cbPais.DisplayMember = "DESCRICAO";
                cbPais.ValueMember = "IDPAIS";
                cbPais.SelectedIndex = 0;
            }

            using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
            {
                cbFornecedor.DataSource = controle.GetAll().OrderBy(x => x.DESCRICAO).ToList();
                cbFornecedor.DisplayMember = "DESCRICAO";
                cbFornecedor.ValueMember = "CODIGO";
                cbFornecedor.SelectedIndex = 0;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                try
                {
                    FCFO item = new FCFO
                    {
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODCFO = txtCodCliente.Text,
                        CODTPFCFO = Convert.ToInt16(cbFornecedor.SelectedValue),
                        NOMEFANTASIA = txtNomeFantasia.Text,
                        NOME = txtNome.Text,
                        PAGREC = Convert.ToInt16(cbClassificacao.SelectedIndex + 1),
                        PESSOAFISOUJUR = (cbClassificacao.SelectedIndex == 0) ? "F" : "J",
                        CGCCFO = txtNumeroInscricao.Text,
                        INSCRESTADUAL = txtInscricaoEstadual.Text,
                        ATIVO = 1,
                        CEP = Global.RemovePVTEsp(txtCEP.Text),
                        RUA = txtRua.Text,
                        NUMERO = txtNumero.Text,
                        COMPLEMENTO = txtComplemento.Text,
                        BAIRRO = txtBairro.Text,
                        PAIS = Convert.ToString(cbPais.SelectedValue),
                        CODETD = Convert.ToString(cbEstado.SelectedValue),
                        CODMUNICIPIO = Convert.ToString(cbMunicipio.SelectedValue),
                        TELEFONE = txtTelefone.Text,
                        TELEX = txtTelCelular.Text,
                        EMAIL = txtEmail.Text,
                        CONTATO = txtNomeContato.Text,
                        CIDADE = cbMunicipio.Text
                    };

                    using (ControleFCFO controle = new ControleFCFO())
                    {
                        if (itemEdit == null)
                        {
                            item.IDCFO = controle.GetID();
                            item.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECCREATEDON = DateTime.Now;

                            controle.Create(item);
                            controle.SaveAll();
                            salvo = true;
                            this.Close();
                        }
                        else
                        {
                            item.IDCFO = itemEdit.IDCFO;
                            item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECMODIFIEDON = DateTime.Now;

                            controle.Update(item, x => x.CODCFO == itemEdit.CODCFO && x.CODCOLIGADA == itemEdit.CODCOLIGADA);
                            controle.SaveAll();
                            salvo = true;
                            this.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex);
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();
                }
            }
        }
    }
}
