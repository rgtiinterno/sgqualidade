﻿namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    partial class FormDocumento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDocumento));
            this.label2 = new System.Windows.Forms.Label();
            this.cbDocumento = new System.Windows.Forms.ComboBox();
            this.txtFornecedor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtColigada = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.txtNomeDoc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAnexo = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtDtValidade = new System.Windows.Forms.MaskedTextBox();
            this.ckNao = new System.Windows.Forms.CheckBox();
            this.ckSim = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(22, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Documento:";
            // 
            // cbDocumento
            // 
            this.cbDocumento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbDocumento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbDocumento.FormattingEnabled = true;
            this.cbDocumento.Location = new System.Drawing.Point(25, 104);
            this.cbDocumento.Name = "cbDocumento";
            this.cbDocumento.Size = new System.Drawing.Size(277, 21);
            this.cbDocumento.TabIndex = 0;
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Location = new System.Drawing.Point(308, 42);
            this.txtFornecedor.MaxLength = 2;
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.ReadOnly = true;
            this.txtFornecedor.Size = new System.Drawing.Size(93, 20);
            this.txtFornecedor.TabIndex = 40;
            this.txtFornecedor.Text = "1";
            this.txtFornecedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(305, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Cód. Fornecedor:";
            // 
            // txtColigada
            // 
            this.txtColigada.Location = new System.Drawing.Point(25, 42);
            this.txtColigada.MaxLength = 2;
            this.txtColigada.Name = "txtColigada";
            this.txtColigada.ReadOnly = true;
            this.txtColigada.Size = new System.Drawing.Size(80, 20);
            this.txtColigada.TabIndex = 41;
            this.txtColigada.Text = "1";
            this.txtColigada.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(22, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Cód. Coligada:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(304, 282);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalvar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSalvar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(192, 282);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(106, 27);
            this.btnSalvar.TabIndex = 4;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // txtNomeDoc
            // 
            this.txtNomeDoc.Location = new System.Drawing.Point(25, 158);
            this.txtNomeDoc.Name = "txtNomeDoc";
            this.txtNomeDoc.Size = new System.Drawing.Size(376, 20);
            this.txtNomeDoc.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(22, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Nome do Documento:";
            // 
            // btnAnexo
            // 
            this.btnAnexo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAnexo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnexo.Location = new System.Drawing.Point(308, 100);
            this.btnAnexo.Name = "btnAnexo";
            this.btnAnexo.Size = new System.Drawing.Size(93, 27);
            this.btnAnexo.TabIndex = 1;
            this.btnAnexo.Text = "&Anexar arquivo";
            this.btnAnexo.UseVisualStyleBackColor = true;
            this.btnAnexo.Click += new System.EventHandler(this.btnAnexo_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtDtValidade);
            this.groupBox4.Controls.Add(this.ckNao);
            this.groupBox4.Controls.Add(this.ckSim);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(25, 197);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(376, 70);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Documento possui data de validade?";
            // 
            // txtDtValidade
            // 
            this.txtDtValidade.Location = new System.Drawing.Point(257, 34);
            this.txtDtValidade.Mask = "99/99/9999";
            this.txtDtValidade.Name = "txtDtValidade";
            this.txtDtValidade.Size = new System.Drawing.Size(100, 20);
            this.txtDtValidade.TabIndex = 2;
            this.txtDtValidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ckNao
            // 
            this.ckNao.AutoSize = true;
            this.ckNao.Checked = true;
            this.ckNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckNao.Location = new System.Drawing.Point(127, 36);
            this.ckNao.Name = "ckNao";
            this.ckNao.Size = new System.Drawing.Size(46, 17);
            this.ckNao.TabIndex = 1;
            this.ckNao.Text = "Não";
            this.ckNao.UseVisualStyleBackColor = true;
            this.ckNao.CheckedChanged += new System.EventHandler(this.ckNao_CheckedChanged);
            // 
            // ckSim
            // 
            this.ckSim.AutoSize = true;
            this.ckSim.Location = new System.Drawing.Point(55, 36);
            this.ckSim.Name = "ckSim";
            this.ckSim.Size = new System.Drawing.Size(43, 17);
            this.ckSim.TabIndex = 0;
            this.ckSim.Text = "Sim";
            this.ckSim.UseVisualStyleBackColor = true;
            this.ckSim.CheckedChanged += new System.EventHandler(this.ckSim_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(254, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Data Validade:";
            // 
            // FormDocumento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 321);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.txtNomeDoc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbDocumento);
            this.Controls.Add(this.txtFornecedor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtColigada);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAnexo);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormDocumento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Novo Documento";
            this.Load += new System.EventHandler(this.FormDocumento_Load);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbDocumento;
        private System.Windows.Forms.TextBox txtFornecedor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtColigada;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TextBox txtNomeDoc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAnexo;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.MaskedTextBox txtDtValidade;
        private System.Windows.Forms.CheckBox ckNao;
        private System.Windows.Forms.CheckBox ckSim;
        private System.Windows.Forms.Label label6;
    }
}