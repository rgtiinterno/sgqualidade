﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    public partial class FormDossie : Form
    {
        private FCFO fornecedor;
        public bool salvo = false;

        public FormDossie(FCFO cFornecedor)
        {
            InitializeComponent();
            this.Text += $" - [{cFornecedor.CODCFO} - {cFornecedor.NOME}]";
            fornecedor = cFornecedor;
            using (ControleFTipoArquivo controller = new ControleFTipoArquivo())
            {
                lblTipoFornec.Text += $"{cFornecedor.CODTPFCFO} - {controller.Find(cFornecedor.CODTPFCFO)?.DESCRICAO}";
            }
        }

        private void FormDossie_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCFO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEDOC", HeaderText = "Nome Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "BYTES", HeaderText = "Bytes", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPARQUIVO", HeaderText = "Tipo do Arquivo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TPARQUIVO", HeaderText = "Tipo do Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "POSSUIVALIDADE", HeaderText = "Possuí Validade?", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVALIDADE", HeaderText = "Data Validade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleFArquivoAnx controle = new ControleFArquivoAnx())
            {
                Cursor.Current = Cursors.WaitCursor;
                fARQFCFOBindingSource = new BindingSource();
                fARQFCFOBindingSource.DataSource = typeof(FARQFCFO);
                fARQFCFOBindingSource.DataSource = controle.GetList(fornecedor.CODCOLIGADA, fornecedor.CODCFO);
                dgvListagem.DataSource = fARQFCFOBindingSource;
                fARQFCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fARQFCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbDocumentros.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbVerDocumento.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbDocumentros.Enabled = true;
                    tsbRemover.Enabled = false;
                    tsbVerDocumento.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbVerDocumento_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is FARQFCFO item && item.CKSELECIONADO)
            {
                string arquivoTemp = string.Format
                (
                    @"{0}\{1}{2}", Global.PastaTemp(),
                    item.NOMEDOC, item.EXTENSAO
                );

                Process.Start(Global.DescriptPDF(item.BYTES, arquivoTemp));
            }
            else
            {
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is FARQFCFO item && item.CKSELECIONADO)
            {
                //using (ControleFArquivoAnx controle = new ControleFArquivoAnx())
                using (QUALIDADEEntities context = new QUALIDADEEntities(SqlConn.getEntityConnection()))
                {

                    context.FARQFCFO.Remove(
                        context.FARQFCFO.Where(x => x.CODCFO == item.CODCFO && x.CODCOLIGADA == item.CODCOLIGADA
                        && x.CODTPARQUIVO == item.CODTPARQUIVO && x.NOMEDOC == item.NOMEDOC).FirstOrDefault());

                    context.SaveChanges();
                    

                    //controle.Delete(x => x.CODCFO == item.CODCFO && x.CODCOLIGADA == item.CODCOLIGADA
                    //    && x.CODTPARQUIVO == item.CODTPARQUIVO && x.NOMEDOC == item.NOMEDOC);
                    //controle.SaveAll();

                    this.salvo = true;
                    tsbAtualizar.PerformClick();
                }
            }
        }

        private void tsbDocumentros_Click(object sender, EventArgs e)
        {
            //using (FormDocumento frm = new FormDocumento(fornecedor))
            //{
            //    frm.StartPosition = FormStartPosition.CenterParent;
            //    frm.WindowState = FormWindowState.Normal;
            //    frm.ShowDialog();

            //    if (frm.salvo)
            //    {
            //        tsbAtualizar.PerformClick();
            //    }
            //}
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            if (e != null && e.RowData is FARQFCFO item)
            {
                if (item.DOCVENCIDO)
                    e.Style.BackColor = Color.FromArgb(255, 255, 192, 192);
                else if (item.DOCVENCEATE30D)
                    e.Style.BackColor = Color.FromArgb(255, 255, 255, 192);
                else
                    e.Style.BackColor = Color.White;
            }
        }
    }
}