﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    public partial class FormDocumento : Form
    {
        private string CodFornecedor;
        private FARQUIVOCFO arquivo;
        public bool salvo = false;
        private List<FARQFCFO> anexos;

        public FormDocumento(FARQUIVOCFO FArquivo, string fornecedor)
        {
            InitializeComponent();
            anexos = new List<FARQFCFO>();
            arquivo = FArquivo;
            CodFornecedor = fornecedor;
        }

        private void btnAnexo_Click(object sender, EventArgs e)
        {
            string caminhoArquivo = Funcoes.SelecionaArquivosAllFormat();
            anexos.Clear();

            if (caminhoArquivo != null)
            {
                txtNomeDoc.Text = Path.GetFileNameWithoutExtension(caminhoArquivo);
                FARQFCFO item = new FARQFCFO
                {
                    CKSELECIONADO = false,
                    CODCFO = CodFornecedor,
                    CODCOLIGADA = arquivo.CODCOLIGADA,
                    CODTPARQUIVO = Convert.ToInt32(cbDocumento.SelectedValue),
                    NOMEDOC = txtNomeDoc.Text,
                    POSSUIVALIDADE = ckSim.Checked,
                    DATAVALIDADE = DateTime.TryParse(txtDtValidade.Text, out DateTime dtValidade) ?
                        dtValidade : (DateTime?)null,
                    EXTENSAO = Path.GetExtension(caminhoArquivo).ToLower(),
                    BYTES = Global.EncriptPDF(caminhoArquivo),
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };
                anexos.Add(item);
            }
            else
                txtNomeDoc.Text = string.Empty;
        }

        private void FormDocumento_Load(object sender, EventArgs e)
        {
            using (ControleFTPArquivo controle = new ControleFTPArquivo())
            {
                cbDocumento.DataSource = controle.GetAll().ToList();
                cbDocumento.DisplayMember = "NOME";
                cbDocumento.ValueMember = "CODIGO";
                cbDocumento.SelectedIndex = 0;
            }

            txtColigada.Text = Convert.ToString(arquivo.CODCOLIGADA);
            txtFornecedor.Text = CodFornecedor;
            cbDocumento.SelectedValue = arquivo.CODTPARQUIVO;

            cbDocumento.Enabled = false;
            txtFornecedor.Enabled = false;
            txtColigada.Enabled = false;

            txtFornecedor.ReadOnly = true;
            txtColigada.ReadOnly = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ckSim.Checked && string.IsNullOrWhiteSpace(Global.RemovePVTEsp(txtDtValidade.Text)))
                Global.MsgErro(" - Informe a data de validade. ");
            else
            {
                foreach (var item in anexos)
                {
                    item.POSSUIVALIDADE = ckSim.Checked;
                    item.DATAVALIDADE = DateTime.TryParse(txtDtValidade.Text, out DateTime dtValidade) ?
                        dtValidade : (DateTime?)null;
                    item.NOMEDOC = txtNomeDoc.Text;
                }

                if (anexos.Count == 1 && !string.IsNullOrEmpty(txtNomeDoc.Text))
                {
                    using (ControleFArquivoAnx controle = new ControleFArquivoAnx())
                    {
                        controle.DeletaAnexo(anexos[0].CODCOLIGADA, anexos[0].CODCFO, anexos[0].CODTPARQUIVO);
                        controle.CreateAll(anexos);
                        controle.SaveAll();
                    }
                    salvo = true;
                    this.Close();
                }
            }
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !ckSim.Checked;

            if (ckSim.Checked)
            {
                txtDtValidade.Text = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy");
                txtDtValidade.Enabled = true;
                txtDtValidade.ReadOnly = false;
            }
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !ckNao.Checked;

            if (ckNao.Checked)
            {
                txtDtValidade.Text = string.Empty;
                txtDtValidade.Enabled = false;
                txtDtValidade.ReadOnly = true;
            }
        }
    }
}
