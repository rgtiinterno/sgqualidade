﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Enums;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    public partial class FormChecklistDossie : Form
    {
        private FCFO cFornec;
        public bool salvo = false;

        public FormChecklistDossie(FCFO fornecedor)
        {
            InitializeComponent();
            this.Text += $" - [{fornecedor.CODCFO} - {fornecedor.NOME}]";
            cFornec = fornecedor;
            using (ControleFTipoArquivo controller = new ControleFTipoArquivo())
            {
                lblTipoFornec.Text += $"{fornecedor.CODTPFCFO} - {controller.Find(fornecedor.CODTPFCFO)?.DESCRICAO}";
            }
        }

        private void FormChecklistDossie_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPCFO", HeaderText = "CODTPCFO", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPARQUIVO", HeaderText = "Cód. Arquivo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEARQUIVO", HeaderText = "Nome Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "TEMVALIDADE", HeaderText = "Tem Validade?", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVALIDADEARQUIVO", HeaderText = "Dt. Validade do Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });
            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleFArquivoFornecedor controle = new ControleFArquivoFornecedor())
            {
                Cursor.Current = Cursors.WaitCursor;
                fARQUIVOCFOBindingSource = new BindingSource();
                fARQUIVOCFOBindingSource.DataSource = typeof(FARQUIVOCFO);
                fARQUIVOCFOBindingSource.DataSource = controle.GetList(cFornec);
                dgvListagem.DataSource = fARQUIVOCFOBindingSource;
                fARQUIVOCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fARQUIVOCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbDocumentros.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbDocumentros.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbDocumentros_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FARQUIVOCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                using (FormDocumento frm = new FormDocumento(fornecedor, cFornec.CODCFO))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        this.salvo = true;
                        tsbAtualizar.PerformClick();
                    }
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            if (e.RowType == RowType.DefaultRow)
            {
                if (e.RowData is FARQUIVOCFO item)
                {
                    using (ControleFArquivoAnx controle = new ControleFArquivoAnx())
                    {
                        if (controle.Find(item.CODCOLIGADA, cFornec.CODCFO, item.CODTPARQUIVO) is FARQFCFO arquivo)
                        {
                            DateTime currentDate = DateTime.Now;
                            if (arquivo.DATAVALIDADE.HasValue && currentDate.Subtract(arquivo.DATAVALIDADE.Value).TotalDays > 0)
                                e.Style.BackColor = Color.LightCoral;
                            else
                                e.Style.BackColor = Color.DarkSeaGreen;
                        }
                        else
                            e.Style.BackColor = Color.LightGoldenrodYellow;
                    }
                }
            }
        }
    }
}
