﻿namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    partial class FormListaDossie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaDossie));
            this.dgvListagem = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslTotalItens = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslGreen = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbDocRequisitados = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbDocumentros = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbExportar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cbCentroCusto = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cbTipoFornec = new System.Windows.Forms.ToolStripComboBox();
            this.tslRed = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslYellow = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslGray = new System.Windows.Forms.ToolStripStatusLabel();
            this.fCFOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCFOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvListagem
            // 
            this.dgvListagem.AccessibleName = "Table";
            this.dgvListagem.AllowDraggingColumns = true;
            this.dgvListagem.AllowEditing = false;
            this.dgvListagem.AllowFiltering = true;
            this.dgvListagem.AllowResizingColumns = true;
            this.dgvListagem.AutoGenerateColumns = false;
            this.dgvListagem.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListagem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvListagem.DataSource = this.fCFOBindingSource;
            this.dgvListagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListagem.Location = new System.Drawing.Point(0, 25);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.PreviewRowPadding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.dgvListagem.SerializationController = null;
            this.dgvListagem.Size = new System.Drawing.Size(1122, 511);
            this.dgvListagem.TabIndex = 7;
            this.dgvListagem.Text = "sfDataGrid1";
            this.dgvListagem.QueryRowStyle += new Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventHandler(this.dgvListagem_QueryRowStyle);
            this.dgvListagem.QueryImageCellStyle += new Syncfusion.WinForms.DataGrid.Events.QueryImageCellStyleEventHandler(this.dgvListagem_QueryImageCellStyle);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslTotalItens,
            this.tslGreen,
            this.tslRed,
            this.tslYellow,
            this.tslGray});
            this.statusStrip1.Location = new System.Drawing.Point(0, 536);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(1122, 25);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslTotalItens
            // 
            this.tslTotalItens.Name = "tslTotalItens";
            this.tslTotalItens.Size = new System.Drawing.Size(40, 20);
            this.tslTotalItens.Text = "           ";
            // 
            // tslGreen
            // 
            this.tslGreen.AutoToolTip = true;
            this.tslGreen.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tslGreen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslGreen.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslGreen.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tslGreen.Image = global::QUALIDADE.Icons16x16.status_green;
            this.tslGreen.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tslGreen.Name = "tslGreen";
            this.tslGreen.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tslGreen.Size = new System.Drawing.Size(30, 20);
            this.tslGreen.Text = "Documento anexado, dentro do prazo e maior que 30 dias do vencimento";
            this.tslGreen.ToolTipText = "Documento anexado, dentro do prazo e maior que 30 dias do vencimento";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDocRequisitados,
            this.toolStripSeparator2,
            this.tsbDocumentros,
            this.tsbAtualizar,
            this.toolStripSeparator1,
            this.tsbExportar,
            this.toolStripSeparator4,
            this.toolStripSeparator5,
            this.toolStripLabel1,
            this.cbCentroCusto,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.cbTipoFornec});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1122, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbDocRequisitados
            // 
            this.tsbDocRequisitados.Image = ((System.Drawing.Image)(resources.GetObject("tsbDocRequisitados.Image")));
            this.tsbDocRequisitados.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDocRequisitados.Name = "tsbDocRequisitados";
            this.tsbDocRequisitados.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsbDocRequisitados.Size = new System.Drawing.Size(214, 22);
            this.tsbDocRequisitados.Text = "Lista de documentos requisitados";
            this.tsbDocRequisitados.Click += new System.EventHandler(this.tsbDocRequisitados_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbDocumentros
            // 
            this.tsbDocumentros.Image = ((System.Drawing.Image)(resources.GetObject("tsbDocumentros.Image")));
            this.tsbDocumentros.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDocumentros.Name = "tsbDocumentros";
            this.tsbDocumentros.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tsbDocumentros.Size = new System.Drawing.Size(147, 22);
            this.tsbDocumentros.Text = "Lista de documentos";
            this.tsbDocumentros.Click += new System.EventHandler(this.tsbDocumentros_Click);
            // 
            // tsbAtualizar
            // 
            this.tsbAtualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizar.Image")));
            this.tsbAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizar.Name = "tsbAtualizar";
            this.tsbAtualizar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizar.Size = new System.Drawing.Size(93, 22);
            this.tsbAtualizar.Text = "Atualizar";
            this.tsbAtualizar.Click += new System.EventHandler(this.tsbAtualizar_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbExportar
            // 
            this.tsbExportar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbExportar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbExportar.Image = global::QUALIDADE.Properties.Resources.iconfinder_export_3855597__2_;
            this.tsbExportar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExportar.Name = "tsbExportar";
            this.tsbExportar.Size = new System.Drawing.Size(23, 22);
            this.tsbExportar.Text = "Exportar";
            this.tsbExportar.ToolTipText = "Exportar registros";
            this.tsbExportar.Click += new System.EventHandler(this.tsbExportar_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(96, 22);
            this.toolStripLabel1.Text = "Centro de Custo:";
            // 
            // cbCentroCusto
            // 
            this.cbCentroCusto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCentroCusto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCentroCusto.Name = "cbCentroCusto";
            this.cbCentroCusto.Size = new System.Drawing.Size(250, 25);
            this.cbCentroCusto.SelectedIndexChanged += new System.EventHandler(this.cbCentroCusto_SelectedIndexChanged);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(33, 22);
            this.toolStripLabel2.Text = "Tipo:";
            // 
            // cbTipoFornec
            // 
            this.cbTipoFornec.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbTipoFornec.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbTipoFornec.Name = "cbTipoFornec";
            this.cbTipoFornec.Size = new System.Drawing.Size(200, 25);
            this.cbTipoFornec.SelectedIndexChanged += new System.EventHandler(this.cbTipoFornec_SelectedIndexChanged);
            // 
            // tslRed
            // 
            this.tslRed.AutoToolTip = true;
            this.tslRed.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tslRed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslRed.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRed.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tslRed.Image = global::QUALIDADE.Icons16x16.status_red;
            this.tslRed.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tslRed.Name = "tslRed";
            this.tslRed.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tslRed.Size = new System.Drawing.Size(30, 20);
            this.tslRed.Text = "Documento com validade vencida";
            this.tslRed.ToolTipText = "Documento com validade vencida";
            // 
            // tslYellow
            // 
            this.tslYellow.AutoToolTip = true;
            this.tslYellow.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tslYellow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslYellow.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslYellow.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tslYellow.Image = global::QUALIDADE.Icons16x16.status_yellow;
            this.tslYellow.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tslYellow.Name = "tslYellow";
            this.tslYellow.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tslYellow.Size = new System.Drawing.Size(30, 20);
            this.tslYellow.Text = "Documento anexado, dentro do prazo e menor ou igual a 30 dias do vencimento";
            this.tslYellow.ToolTipText = "Documento anexado, dentro do prazo e menor ou igual a 30 dias do vencimento";
            // 
            // tslGray
            // 
            this.tslGray.AutoToolTip = true;
            this.tslGray.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.tslGray.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslGray.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslGray.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tslGray.Image = global::QUALIDADE.Icons16x16.status_gray;
            this.tslGray.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tslGray.Name = "tslGray";
            this.tslGray.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.tslGray.Size = new System.Drawing.Size(30, 20);
            this.tslGray.Text = "Nenhum documento obrigatório anexado";
            this.tslGray.ToolTipText = "Nenhum documento obrigatório anexado";
            // 
            // fCFOBindingSource
            // 
            this.fCFOBindingSource.DataSource = typeof(QUALIDADE.Dominio.FCFO);
            // 
            // FormListaDossie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 561);
            this.Controls.Add(this.dgvListagem);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormListaDossie";
            this.Text = "Dossie";
            this.Load += new System.EventHandler(this.FormListaDossie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fCFOBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListagem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslTotalItens;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbDocumentros;
        private System.Windows.Forms.ToolStripButton tsbAtualizar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbDocRequisitados;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cbCentroCusto;
        private System.Windows.Forms.ToolStripButton tsbExportar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.BindingSource fCFOBindingSource;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripStatusLabel tslGreen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cbTipoFornec;
        private System.Windows.Forms.ToolStripStatusLabel tslRed;
        private System.Windows.Forms.ToolStripStatusLabel tslYellow;
        private System.Windows.Forms.ToolStripStatusLabel tslGray;
    }
}