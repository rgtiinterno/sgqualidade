﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGrid.Enums;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Documentos.Dossie
{
    public partial class FormListaDossie : Form
    {
        public FormListaDossie()
        {
            InitializeComponent();
        }

        private void FormListaDossie_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridImageColumn()
            {
                MappingName = "StatusImage",
                HeaderText = string.Empty,
                AllowEditing = false,
                AllowFiltering = false,
                Visible = true,
                CellStyle = new Syncfusion.WinForms.DataGrid.Styles.CellStyleInfo { HorizontalAlignment = HorizontalAlignment.Center },
                Width = 30, 
                FilterMode = Syncfusion.Data.ColumnFilter.Value,
                ImageLayout = ImageLayout.Center
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCFO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOME", HeaderText = "Razão Social", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEFANTASIA", HeaderText = "Nome Fantasia", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód.Centro Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCCCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PAGREC", HeaderText = "Classificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCPAGREC", HeaderText = "Classificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PESSOAFISOUJUR", HeaderText = "Pessoa Física/Jurídica", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCPESSOAFISOUJUR", HeaderText = "Tp. Inscrição", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CGCCFO", HeaderText = "Núm. Inscrição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "STATUS", HeaderText = "Ativo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CONFORMIDADE", HeaderText = "Conformidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "IDCFO", HeaderText = "ID", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RAMOATIV", HeaderText = "Ramo", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPOCONTRIBUINTEINSS", HeaderText = "Tp. Contrib. INSS", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NACIONALIDADE", HeaderText = "Nacionalidade", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CALCULAAVP", HeaderText = "Calcula AVP", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ENTIDADEEXECUTORAPAA", HeaderText = "Ent. Executora", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "APOSENTADOOUPENSIONISTA", HeaderText = "Apos. ou Pens.", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "INSCRESTADUAL", HeaderText = "Insc. Estadual", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RUA", HeaderText = "Rua", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMERO", HeaderText = "Número", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "COMPLEMENTO", HeaderText = "Complemento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "BAIRRO", HeaderText = "Bairro", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CIDADE", HeaderText = "Cidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODETD", HeaderText = "Cód. Estado", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CEP", HeaderText = "CEP", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TELEFONE", HeaderText = "Tel. Fixo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TELEX", HeaderText = "Tel. Celular", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EMAIL", HeaderText = "E-mail", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CONTATO", HeaderText = "Contato", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PAIS", HeaderText = "País", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPORUA", HeaderText = "Tp. Rua", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPOBAIRRO", HeaderText = "Tp. Bairro", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODMUNICIPIO", HeaderText = "Cód. Municipio", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FAX", HeaderText = "Fax", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPFCFO", HeaderText = "CODTPFCFO", AllowEditing = false, AllowFiltering = true, Visible = false });
            CarregaCentrosCusto();
            CarregaTiposFornec();
            CarregaDGV();
        }

        private void CarregaDGV(string codCCusto = null, int? codTpFornec = null)
        {
            using (ControleFCFO controle = new ControleFCFO())
            {
                Cursor.Current = Cursors.WaitCursor;
                fCFOBindingSource = new BindingSource();
                fCFOBindingSource.DataSource = typeof(FCFO);
                List<FCFO> itens = controle.GetListDossie(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, codCCusto, codTpFornec);
                fCFOBindingSource.DataSource = itens;
                dgvListagem.DataSource = fCFOBindingSource;
                fCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbDocRequisitados.Enabled = true;
                    tsbDocumentros.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbDocRequisitados.Enabled = false;
                    tsbDocumentros.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void CarregaCentrosCusto()
        {
            var centrosCusto = FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("F");
            cbCentroCusto.ComboBox.DataSource = centrosCusto;
            cbCentroCusto.ComboBox.DisplayMember = "DescCombo";
            cbCentroCusto.ComboBox.ValueMember = "CodCCusto";
            if (centrosCusto?.Count > 0)
                cbCentroCusto.ComboBox.SelectedIndex = 0;
            else
                cbCentroCusto.ComboBox.SelectedIndex = -1;
        }

        private void CarregaTiposFornec()
        {
            using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
            {
                List<TipoFornecFiltro> itensFiltro = new List<TipoFornecFiltro>();
                var tipos = controle.GetAll();
                foreach (var tp in tipos)
                {
                    itensFiltro.Add(new TipoFornecFiltro
                    {
                        CodTpFornec = tp.CODIGO,
                        Descricao = tp.DESCRICAO
                    });
                }
                cbTipoFornec.ComboBox.DataSource = itensFiltro;
                cbTipoFornec.ComboBox.DisplayMember = "DescCombo";
                cbTipoFornec.ComboBox.ValueMember = "CodTpFornec";
                cbTipoFornec.ComboBox.SelectedIndex = -1;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            string codCCusto = cbCentroCusto.SelectedIndex >= 0 && cbTipoFornec.SelectedItem is CCustoFiltro cCusto ? cCusto.CodCCusto : null;
            int? codTpFornec = cbTipoFornec.SelectedIndex >= 0 && cbTipoFornec.SelectedItem is TipoFornecFiltro tpFornec ? tpFornec.CodTpFornec : (int?)null;
            CarregaDGV(codCCusto, codTpFornec);
        }

        private void tsbDocRequisitados_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                using (FormChecklistDossie frm = new FormChecklistDossie(fornecedor))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();
                    if (frm.salvo)
                        tsbAtualizar.PerformClick();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbDocumentros_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FCFO fornecedor && fornecedor.CKSELECIONADO)
            {
                using (FormDossie frm = new FormDossie(fornecedor))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();
                    if (frm.salvo)
                        tsbAtualizar.PerformClick();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void dgvListagem_QueryRowStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryRowStyleEventArgs e)
        {
            //if (e.RowType == RowType.DefaultRow && e.RowData is FCFO item)
            //{
            //    if (item.CONFORMIDADE == "Conforme" && item.ATIVO == 1 && item.DOCVENCEATE30D <= 0)
            //        e.Style.BackColor = Color.DarkSeaGreen;
            //    else if (item.DOCVENCEATE30D > 0)
            //        e.Style.BackColor = Color.FromArgb(255, 255, 255, 192);
            //    else if ((item.CONFORMIDADE == "Não Conforme" && item.ATIVO == 1) || item.DOCVENCIDO > 0)
            //        e.Style.BackColor = Color.LightCoral;
            //    else if (item.ATIVO == 0 && item.CONFORMIDADE == "Não se Aplica")
            //        e.Style.BackColor = Color.LightGray; //Color.AntiqueWhite;
            //    else
            //        e.Style.BackColor = Color.White;
            //}
        }

        private void cbCentroCusto_SelectedIndexChanged(object sender, EventArgs e)
        {
            tsbAtualizar.PerformClick();
        }

        private void cbTipoFornec_SelectedIndexChanged(object sender, EventArgs e)
        {
            tsbAtualizar.PerformClick();
        }

        private void tsbExportar_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Planilha Excel (*.xls)|*.xls", "ListaDossieFornecedores.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    ExportMode = ExportMode.Value,
                    ExcelVersion = ExcelVersion.Excel2007,
                    ExportStyle = true,
                    ExportGroupSummary = true,
                    AllowOutlining = true
                };
                options.ExcludeColumns.Add("CKSELECIONADO");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Registros exportados com sucesso.", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvListagem_QueryImageCellStyle(object sender, Syncfusion.WinForms.DataGrid.Events.QueryImageCellStyleEventArgs e)
        {
            if (e.Column.MappingName == "StatusImage" && e.Record is FCFO item)
            {
                if (item.CONFORMIDADE == "Conforme" && item.ATIVO == 1 && item.DOCVENCEATE30D <= 0)
                {
                    e.Image = Icons16x16.status_green; //Verde – Documento anexado, dentro do prazo e maior que 30 dias do vencimento;
                }
                else if ((item.CONFORMIDADE == "Não Conforme" && item.ATIVO == 1) || item.DOCVENCIDO > 0)
                {
                    e.Image = Icons16x16.status_red; //Vermelho – Documento com validade vencida;
                }
                else if (item.DOCVENCEATE30D > 0)
                {
                    e.Image = Icons16x16.status_yellow; //Amarelo – Documento anexado, dentro do prazo e menor ou igual a 30 dias do vencimento;
                }
                else
                {
                    e.Image = Icons16x16.status_gray; //Cinza – Nenhum documento obrigatório anexado;
                }

                //    if (item.CONFORMIDADE == "Conforme" && item.ATIVO == 1 && item.DOCVENCEATE30D <= 0)
                //        e.Style.BackColor = Color.DarkSeaGreen;
                //    else if (item.DOCVENCEATE30D > 0)
                //        e.Style.BackColor = Color.FromArgb(255, 255, 255, 192);
                //    else if ((item.CONFORMIDADE == "Não Conforme" && item.ATIVO == 1) || item.DOCVENCIDO > 0)
                //        e.Style.BackColor = Color.LightCoral;
                //    else if (item.ATIVO == 0 && item.CONFORMIDADE == "Não se Aplica")
                //        e.Style.BackColor = Color.LightGray; //Color.AntiqueWhite;
                //    else
                //        e.Style.BackColor = Color.White;
            }
        }
    }
}