﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Documentos.TiposDocumentos
{
    public partial class FormDocumentos : Form
    {
        private FTPARQUIVO itemEdit;
        public bool salvo = false;

        public FormDocumentos(FTPARQUIVO arquivo = null)
        {
            InitializeComponent();
            itemEdit = arquivo;
        }

        private void FormDocumentos_Load(object sender, EventArgs e)
        {
            if (itemEdit == null)
            {
                using (ControleFTPArquivo controle = new ControleFTPArquivo())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetID());
                }
            }
            else
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtNome.Text = itemEdit.NOME;
                txtObservacao.Text = itemEdit.DESCRICAO;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                try
                {
                    using (ControleFTPArquivo controle = new ControleFTPArquivo())
                    {
                        FTPARQUIVO item = new FTPARQUIVO
                        {
                            CODIGO = Convert.ToInt32(txtCodigo.Text),
                            NOME = txtNome.Text,
                            DESCRICAO = txtObservacao.Text,
                        };

                        if (itemEdit == null)
                        {
                            item.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECCREATEDON = DateTime.Now;

                            controle.Create(item);
                            controle.SaveAll();
                            salvo = true;
                            this.Close();
                        }
                        else
                        {
                            item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            item.RECMODIFIEDON = DateTime.Now;

                            controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                            controle.SaveAll();
                            salvo = true;
                            this.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();
                    }
                }
            }
        }

        private bool ValidarCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrWhiteSpace(txtNome.Text))
            {
                msg += " - Informe o nome do documento.\n";
            }

            if (string.IsNullOrWhiteSpace(msg))
            {
                return true;
            }
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }
    }
}
