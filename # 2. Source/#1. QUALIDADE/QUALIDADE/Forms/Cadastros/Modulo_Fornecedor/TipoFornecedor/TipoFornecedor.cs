﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCF.TipoFornecedor
{
    public partial class TipoFornecedor : Form
    {
        public bool salvo = false;
        private FTPFCFO itemEdit;

        public TipoFornecedor(FTPFCFO tipo = null)
        {
            InitializeComponent();
            itemEdit = tipo;
        }

        private void TipoFornecedor_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtNome.Text = itemEdit.DESCRICAO;
            }
            else
            {
                using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                }
            }

            txtCodigo.ReadOnly = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNome.Text))
            {
                using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
                {
                    FTPFCFO item = new FTPFCFO
                    {
                        CODIGO = Convert.ToInt16(txtCodigo.Text),
                        DESCRICAO = txtNome.Text
                    };

                    if (itemEdit == null)
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe todos os campos");
        }
    }
}
