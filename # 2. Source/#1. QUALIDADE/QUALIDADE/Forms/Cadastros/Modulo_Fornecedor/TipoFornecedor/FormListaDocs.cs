﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.ClienteFornecedor;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCF.TipoFornecedor
{
    public partial class FormListaDocs : Form
    {
        private int codigo;

        public FormListaDocs(int CODIGO)
        {
            InitializeComponent();
            codigo = CODIGO;
        }

        private void FormListaDocumentos_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPCFO", HeaderText = "CODTPCFO", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODTPARQUIVO", HeaderText = "Cód. Arquivo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEARQUIVO", HeaderText = "Nome Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "TEMVALIDADE", HeaderText = "Possui Validade?", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVALIDADEARQUIVO", HeaderText = "Dt. Validade do Arquivo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleFArquivoFornecedor controle = new ControleFArquivoFornecedor())
            {
                Cursor.Current = Cursors.WaitCursor;
                fARQUIVOCFOBindingSource = new BindingSource();
                fARQUIVOCFOBindingSource.DataSource = typeof(FARQUIVOCFO);
                fARQUIVOCFOBindingSource.DataSource = controle.GetList(codigo, FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                dgvListagem.DataSource = fARQUIVOCFOBindingSource;
                fARQUIVOCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fARQUIVOCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbExcluir.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbExcluir.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormDFornecedor frm = new FormDFornecedor(codigo))
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (dgvListagem.SelectedItem is FARQUIVOCFO tipo)
            {
                try
                {
                    using (ControleFArquivoFornecedor controle = new ControleFArquivoFornecedor())
                    {
                        controle.Delete(x => x.CODTPCFO == tipo.CODTPCFO && x.CODCOLIGADA == tipo.CODCOLIGADA
                            && x.CODTPARQUIVO == tipo.CODTPARQUIVO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }
    }
}
