﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCF.TipoFornecedor
{
    public partial class FormListaTpFornecedor : Form
    {
        public FormListaTpFornecedor()
        {
            InitializeComponent();
        }

        private void FormListaTpFornecedor_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
            {
                Cursor.Current = Cursors.WaitCursor;
                fTPFCFOBindingSource = new BindingSource();
                fTPFCFOBindingSource.DataSource = typeof(FCFO);
                fTPFCFOBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = fTPFCFOBindingSource;
                fTPFCFOBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {fTPFCFOBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbExcluir.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbExcluir.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (TipoFornecedor frm = new TipoFornecedor())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();


                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FTPFCFO tipo)
            {
                using (TipoFornecedor frm = new TipoFornecedor(tipo))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();


                    frm.Dispose();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (dgvListagem.SelectedItem is FTPFCFO tipo)
            {
                try
                {
                    using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
                    {
                        controle.Delete(x => x.CODIGO == tipo.CODIGO);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex.InnerException.Message))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void tsbListaDocumento_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is FTPFCFO tipo)
            {
                using (FormListaDocs frm = new FormListaDocs(tipo.CODIGO))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro("Selecione ao um registro para efetuar a ação.");
        }
    }
}
