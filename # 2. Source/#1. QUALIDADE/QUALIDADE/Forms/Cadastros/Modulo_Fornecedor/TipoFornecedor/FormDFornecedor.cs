﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ClienteFornecedor
{
    public partial class FormDFornecedor : Form
    {
        public bool salvo = false;
        private int codigo;

        public FormDFornecedor(int CODTPCFO)
        {
            InitializeComponent();
            codigo = CODTPCFO;
        }

        private void FormDFornecedor_Load(object sender, EventArgs e)
        {
            txtColigada.Text = Convert.ToString(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
            txtFornecedor.Text = Convert.ToString(codigo);

            using (ControleFTPArquivo controle = new ControleFTPArquivo())
            {
                cbDocumento.DataSource = controle.GetAll().ToList();
                cbDocumento.DisplayMember = "NOME";
                cbDocumento.ValueMember = "CODIGO";
                cbDocumento.SelectedIndex = -1;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                using (ControleFArquivoFornecedor controle = new ControleFArquivoFornecedor())
                {
                    FARQUIVOCFO item = new FARQUIVOCFO
                    {
                        CODTPCFO = codigo,
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODTPARQUIVO = Convert.ToInt32(cbDocumento.SelectedValue),
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    controle.Create(item);
                    controle.SaveAll();
                    salvo = true;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex);
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
