﻿namespace QUALIDADE.Forms.Cadastros.ModuloCP.Metas
{
    partial class FormMetas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMetas));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.numPropostasEnviadas = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numPropostasVencidas = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAnobase = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numPropostasEnviadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPropostasVencidas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(166, 157);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 57;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(54, 157);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 56;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // numPropostasEnviadas
            // 
            this.numPropostasEnviadas.Location = new System.Drawing.Point(162, 25);
            this.numPropostasEnviadas.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numPropostasEnviadas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPropostasEnviadas.Name = "numPropostasEnviadas";
            this.numPropostasEnviadas.Size = new System.Drawing.Size(90, 20);
            this.numPropostasEnviadas.TabIndex = 58;
            this.numPropostasEnviadas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numPropostasEnviadas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 59;
            this.label1.Text = "Nº de propostas enviadas:";
            // 
            // numPropostasVencidas
            // 
            this.numPropostasVencidas.Location = new System.Drawing.Point(162, 61);
            this.numPropostasVencidas.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numPropostasVencidas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPropostasVencidas.Name = "numPropostasVencidas";
            this.numPropostasVencidas.Size = new System.Drawing.Size(90, 20);
            this.numPropostasVencidas.TabIndex = 58;
            this.numPropostasVencidas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numPropostasVencidas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 13);
            this.label2.TabIndex = 59;
            this.label2.Text = "Nº de propostas vencidas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "Ano base de referência:";
            // 
            // txtAnobase
            // 
            this.txtAnobase.Location = new System.Drawing.Point(162, 96);
            this.txtAnobase.MaxLength = 4;
            this.txtAnobase.Name = "txtAnobase";
            this.txtAnobase.Size = new System.Drawing.Size(90, 20);
            this.txtAnobase.TabIndex = 60;
            this.txtAnobase.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAnobase.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // FormMetas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 196);
            this.Controls.Add(this.txtAnobase);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numPropostasVencidas);
            this.Controls.Add(this.numPropostasEnviadas);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMetas";
            this.Text = "Metas";
            this.Load += new System.EventHandler(this.FormMetas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numPropostasEnviadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPropostasVencidas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.NumericUpDown numPropostasEnviadas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numPropostasVencidas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAnobase;
    }
}