﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Metas
{
    public partial class FormMetas : Form
    {
        private PMETAS itemEdit;
        public bool salvo = false;
        public FormMetas(PMETAS meta = null)
        {
            InitializeComponent();
            itemEdit = meta;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }

        private void FormMetas_Load(object sender, EventArgs e)
        {
            string anoAtual = Convert.ToString(DateTime.Now.Year);
            if (itemEdit != null)
            {
                numPropostasEnviadas.Value = itemEdit.NUMPROPOSTASENVIADAS;
                numPropostasVencidas.Value = itemEdit.NUMPROPOSTASVENCIDAS;
                txtAnobase.Text = itemEdit.ANOREF;
            }
            else
                txtAnobase.Text = anoAtual;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            PMETAS item = new PMETAS
            {
                ANOREF = txtAnobase.Text,
                NUMPROPOSTASENVIADAS = Convert.ToInt32(numPropostasEnviadas.Value),
                NUMPROPOSTASVENCIDAS = Convert.ToInt32(numPropostasVencidas.Value),
            };

            using (ControlePMetas controle = new ControlePMetas())
            {
                if (itemEdit == null)
                {
                    item.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                    item.RECCREATEDON = DateTime.Now;
                    controle.Create(item);
                    controle.SaveAll();
                    salvo = true;
                    this.Close();
                }
                else
                {
                    item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                    item.RECMODIFIEDON = DateTime.Now;
                    controle.Update(item, itemEdit.ANOREF);
                    salvo = true;
                    this.Close();
                }
            }
        }
    }
}
