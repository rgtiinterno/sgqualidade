﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Interesses
{
    public partial class FormInteresse : Form
    {
        public bool salvo = false;
        private PINTERESSE itemEdit;

        public FormInteresse(PINTERESSE interesse = null)
        {
            InitializeComponent();
            itemEdit = interesse;
        }

        private void FormInteresse_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtCodigo.ReadOnly = true;
                txtDescricao.Text = itemEdit.DESCRICAO;
            }
            else
            {
                using (ControlePInteresses controle = new ControlePInteresses())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                    txtCodigo.ReadOnly = true;
                }
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDescricao.Text))
            {
                PINTERESSE item = new PINTERESSE
                {
                    CODIGO = Convert.ToInt16(txtCodigo.Text),
                    DESCRICAO = txtDescricao.Text
                };

                using (ControlePInteresses controle = new ControlePInteresses())
                {
                    if (itemEdit != null)
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe a descrição");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}