﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Clientes
{
    public partial class FormListaPCliente : Form
    {
        public FormListaPCliente()
        {
            InitializeComponent();
        }
        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Clientes.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
        private void FormListaPCliente_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCLIENTE", HeaderText = "Cód. Cliente", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOMEFANTASIA", HeaderText = "Nome Fantasia", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CGC", HeaderText = "CPF/CNPJ", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOME", HeaderText = "Razão Social", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "INSCRICAOESTADUAL", HeaderText = "Inscrição Estadual", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RUA", HeaderText = "Logradouro", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMERO", HeaderText = "Número", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "COMPLEMENTO", HeaderText = "Complemento", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CIDADE", HeaderText = "Cidade", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ESTADO", HeaderText = "UF", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PAIS", HeaderText = "País", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CEP", HeaderText = "CEP", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EMAIL", HeaderText = "E-mail", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TELEFONE", HeaderText = "Telefone", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CONTATO", HeaderText = "Nome Contato", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "RECMODIFIEDBY", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "RECMODIFIEDON", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOGO", HeaderText = "LOGO", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControlePCliente controle = new ControlePCliente())
            {
                Cursor.Current = Cursors.WaitCursor;
                pCLIENTEBindingSource = new BindingSource();
                pCLIENTEBindingSource.DataSource = typeof(PCLIENTES);
                pCLIENTEBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = pCLIENTEBindingSource;
                pCLIENTEBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {pCLIENTEBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormCliente frm = new FormCliente())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    tsbAtualizar.PerformClick();
                }

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            PCLIENTES cliente = (dgvListagem.SelectedItem as PCLIENTES);

            if (cliente.CKSELECIONADO)
            {
                using (FormCliente frm = new FormCliente(cliente))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        tsbAtualizar.PerformClick();
                    }

                    frm.Dispose();
                }
            }
            else
            {
                Global.MsgErro("Selecione ao um registro para efetuar a edição do mesmo.");
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            PCLIENTES cliente = (dgvListagem.SelectedItem as PCLIENTES);
            if (cliente.CKSELECIONADO)
            {
                try
                {
                    using (ControlePCliente controle = new ControlePCliente())
                    {
                        controle.Delete(x => x.CODCOLIGADA == cliente.CODCOLIGADA && x.CODCLIENTE == cliente.CODCLIENTE);
                        controle.SaveAll();
                        tsbAtualizar.PerformClick();
                    }
                }
                catch (Exception ex)
                {
                    using (FormMsg frm = new FormMsg(ex))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }
    }
}
