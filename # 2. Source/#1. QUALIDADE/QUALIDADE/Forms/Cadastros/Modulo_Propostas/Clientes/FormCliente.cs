﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Clientes
{
    public partial class FormCliente : Form
    {
        PCLIENTES ClienteAtual;
        private string arquivo = null;
        public bool salvo = false;

        public FormCliente(PCLIENTES Cliente = null)
        {
            InitializeComponent();
            ClienteAtual = Cliente;
        }

        public void PreencheCampos()
        {
            txtNomeFantasia.Text = ClienteAtual.NOMEFANTASIA;
            txtCNPJ.Text = ClienteAtual.CGC;
            txtNome.Text = ClienteAtual.NOME;
            txtInscEstadual.Text = ClienteAtual.INSCRICAOESTADUAL;
            txtTelefone.Text = ClienteAtual.TELEFONE;
            txtContato2.Text = ClienteAtual.CONTATO;
            txtRua.Text = ClienteAtual.RUA;
            txtNumero.Text = ClienteAtual.NUMERO;
            txtComplemento.Text = ClienteAtual.COMPLEMENTO;
            txtBairro.Text = ClienteAtual.BAIRRO;
            txtCidade.Text = ClienteAtual.CIDADE;
            cbEstadoEndereco.SelectedValue = ClienteAtual.ESTADO;
            cbPaisEndereco.SelectedText = ClienteAtual.PAIS;
            txtCEP.Text = ClienteAtual.CEP;

            if (ClienteAtual.LOGO != null)
                picLogo.Image = ClienteAtual.LOGO.ToImage();
            else
                picLogo.Image = null;


        }

        public void AlimentaCombos()
        {
            using (ControleGPais obj = new ControleGPais())
            {
                cbPaisEndereco.DataSource = obj.GetAll().ToList();
                cbPaisEndereco.ValueMember = "CODPAIS";
                cbPaisEndereco.DisplayMember = "DESCRICAO";
                cbPaisEndereco.SelectedIndex = -1;
            }

            using (ControleGEstados obj = new ControleGEstados())
            {
                cbEstadoEndereco.DataSource = obj.GetAll().ToList();
                cbEstadoEndereco.ValueMember = "CODETD";
                cbEstadoEndereco.DisplayMember = "NOME";
                cbEstadoEndereco.SelectedIndex = 0;
            }
        }

        private void FormEditaCliente_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnCancelar.PerformClick();
                    break;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (txtColigada.Text == "" || txtCNPJ.Text == "" || txtNome.Text == "" || txtNomeFantasia.Text == "" || txtRua.Text == "" || txtNumero.Text == "" || cbEstadoEndereco.SelectedIndex == -1 || txtBairro.Text == "" || txtCidade.Text == "" || cbPaisEndereco.SelectedIndex == -1 || txtCEP.Text == "")
            {
                MessageBox.Show("Preencha os campos obrigatórios.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    using (ControlePCliente controle = new ControlePCliente())
                    {
                        #region "Dados do formulário"
                        PCLIENTES gCliente = new PCLIENTES
                        {
                            CODCOLIGADA = Convert.ToInt16(txtColigada.Text),
                            NOMEFANTASIA = txtNomeFantasia.Text,
                            CGC = txtCNPJ.Text,
                            NOME = txtNome.Text,
                            INSCRICAOESTADUAL = txtInscEstadual.Text,
                            TELEFONE = txtTelefone.Text,
                            CONTATO = txtContato.Text,
                            CONTATO2 = txtContato2.Text,
                            CONTATO3 = txtContato3.Text,
                            RUA = txtRua.Text,
                            NUMERO = txtNumero.Text,
                            COMPLEMENTO = txtComplemento.Text,
                            BAIRRO = txtBairro.Text,
                            CIDADE = txtCidade.Text,
                            ESTADO = Convert.ToString(cbEstadoEndereco.SelectedValue),
                            PAIS = Convert.ToString(cbPaisEndereco.SelectedValue),
                            CEP = txtCEP.Text,
                            EMAIL = txtEmail.Text,
                            LOGO = picLogo.Image.ToByteArray(),
                        };
                        #endregion

                        if (ClienteAtual == null)
                        {
                            salvo = true;
                            gCliente.CODCLIENTE = controle.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                            gCliente.RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            gCliente.RECCREATEDON = DateTime.Now;
                            controle.Create(gCliente);
                            controle.SaveAll();
                            this.Dispose();
                        }
                        else
                        {
                            salvo = true;
                            gCliente.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                            gCliente.RECMODIFIEDON = DateTime.Now;
                            controle.Update(gCliente, x => x.CODCOLIGADA == ClienteAtual.CODCOLIGADA && x.CODCLIENTE == ClienteAtual.CODCLIENTE);
                            controle.SaveAll();
                            this.Dispose();
                        }
                    }
                }
                catch (Exception ex)
                {
                    FormMsg frm = new FormMsg(ex);
                    frm.ShowDialog();
                }
            }
        }

        private void btnBuscaLogo_Click(object sender, EventArgs e)
        {
            this.arquivo = Funcoes.SelecionaArquivo();
            if (!string.IsNullOrEmpty(this.arquivo))
                picLogo.Load(this.arquivo);
        }

        private void btnRemoveLogo_Click(object sender, EventArgs e)
        {
            picLogo.Image = null;
            this.arquivo = null;
        }

        private void btnBuscaCepCol_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (!string.IsNullOrWhiteSpace(txtCEP.Text))
                {
                    Endereco endereco = BuscaCEP.Buscar(txtCEP.Text);

                    if (endereco != null)
                    {
                        cbEstadoEndereco.SelectedValue = endereco.UF;
                        txtCidade.Text = endereco.Cidade;
                        txtBairro.Text = endereco.Bairro;
                        txtRua.Text = string.Format("{0} {1}", endereco.TipoLogradouro, endereco.Logradouro);
                        txtNumero.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Cep não localizado...");
                    }
                }
                else
                {
                    MessageBox.Show("Informe um CEP válido");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor.Current = Cursors.Default;
        }

        private void FormCliente_Load(object sender, EventArgs e)
        {
            AlimentaCombos();
            if (ClienteAtual != null)
            {
                this.Text = $"Edição de Cadastro de Cliente.";
                PreencheCampos();
            }
            else
            {
                this.Text = $"Cadastro de Cliente.";
            }
        }
    }
}
