﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Status
{
    public partial class FormListaStatus : Form
    {
        public FormListaStatus()
        {
            InitializeComponent();
        }

        private void FormListaStatus_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControlePStatus controle = new ControlePStatus())
            {
                Cursor.Current = Cursors.WaitCursor;
                pSTATUSBindingSource = new BindingSource();
                pSTATUSBindingSource.DataSource = typeof(PSTATUS);
                pSTATUSBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = pSTATUSBindingSource;
                pSTATUSBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {pSTATUSBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormStatus frm = new FormStatus())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    tsbAtualizar.PerformClick();
                }

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is PSTATUS item && item.CKSELECIONADO)
            {
                using (FormStatus frm = new FormStatus(item))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        tsbAtualizar.PerformClick();
                    }

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro("Selecione ao menos um registro.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is PSTATUS item && item.CKSELECIONADO)
            {
                using (ControlePStatus controle = new ControlePStatus())
                {
                    controle.Delete(x => x.CODIGO == item.CODIGO);
                    controle.SaveAll();
                    tsbAtualizar.PerformClick();
                }
            }
            else
                Global.MsgErro("Selecione ao menos um registro.");
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }
    }
}