﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Status
{
    public partial class FormStatus : Form
    {
        public bool salvo = false;
        private PSTATUS itemEdit;

        public FormStatus(PSTATUS status = null)
        {
            InitializeComponent();
            itemEdit = status;
        }

        private void FormStatus_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtCodigo.ReadOnly = true;
                txtDescricao.Text = itemEdit.DESCRICAO;
            }
            else
            {
                using (ControlePStatus controle = new ControlePStatus())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                    txtCodigo.ReadOnly = true;
                }
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDescricao.Text))
            {
                PSTATUS item = new PSTATUS
                {
                    CODIGO = Convert.ToInt16(txtCodigo.Text),
                    DESCRICAO = txtDescricao.Text
                };

                using (ControlePStatus controle = new ControlePStatus())
                {
                    if (itemEdit != null)
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe a descrição");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}