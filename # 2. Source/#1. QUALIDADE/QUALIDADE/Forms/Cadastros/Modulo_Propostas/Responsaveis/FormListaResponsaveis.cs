﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Interesses
{
    public partial class FormListaResponsaveis : Form
    {
        public FormListaResponsaveis()
        {
            InitializeComponent();
        }

        private void FormListaResponsaveis_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Nome", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "AIVO", HeaderText = "Habilitado", AllowEditing = false, AllowFiltering = true, Visible = true });
            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControlePResponsaveis controle = new ControlePResponsaveis())
            {
                Cursor.Current = Cursors.WaitCursor;
                pINTERESSEBindingSource = new BindingSource();
                pINTERESSEBindingSource.DataSource = typeof(PRESPONSAVEL);
                pINTERESSEBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = pINTERESSEBindingSource;
                pINTERESSEBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {pINTERESSEBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormResponsavel frm = new FormResponsavel())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
                if (frm.salvo)
                    tsbAtualizar.PerformClick();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is PRESPONSAVEL item && item.CKSELECIONADO)
            {
                using (FormResponsavel frm = new FormResponsavel(item))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();
                    if (frm.salvo)
                        tsbAtualizar.PerformClick();
                }
            }
            else
                Global.MsgErro("Selecione um registro.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is PINTERESSE item && item.CKSELECIONADO)
            {
                using (ControlePResponsaveis controle = new ControlePResponsaveis())
                {
                    controle.Delete(x => x.CODIGO == item.CODIGO);
                    controle.SaveAll();
                    tsbAtualizar.PerformClick();
                }
            }
            else
                Global.MsgErro("Selecione ao menos um registro.");
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbExportar_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Responsáveis.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }
    }
}