﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Interesses
{
    public partial class FormResponsavel : Form
    {
        public bool salvo = false;
        private PRESPONSAVEL itemEdit;

        public FormResponsavel(PRESPONSAVEL item = null)
        {
            InitializeComponent();
            itemEdit = item;
        }

        private void FormResponsavel_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtCodigo.ReadOnly = true;
                txtDescricao.Text = itemEdit.DESCRICAO;
                ckHabilitado.Checked = itemEdit.ATIVO;
            }
            else
            {
                using (ControlePResponsaveis controle = new ControlePResponsaveis())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                    txtCodigo.ReadOnly = true;
                }
            }
            txtDescricao.Select();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDescricao.Text))
            {
                Global.MsgErro(" - Informe o Nome");
            }
            else
            { 
                PRESPONSAVEL item = new PRESPONSAVEL
                {
                    CODIGO = Convert.ToInt32(txtCodigo.Text),
                    DESCRICAO = txtDescricao.Text, 
                    ATIVO = ckHabilitado.Checked
                };

                using (ControlePResponsaveis controle = new ControlePResponsaveis())
                {
                    if (itemEdit != null)
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}