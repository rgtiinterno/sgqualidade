﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Dashboard
{
    public partial class FormFiltro : Form
    {
        private bool global;
        public bool salvo = false;
        public int AnoInicio;
        public int AnoFim;
        public short? codCliente;

        public FormFiltro(bool Global = true)
        {
            InitializeComponent();
            global = Global;
        }

        private void FormFiltro_Load(object sender, EventArgs e)
        {
            if (global)
                cbCliente.Enabled = false;
            else
            {
                cbCliente.Enabled = true;
                using (ControlePCliente controle = new ControlePCliente())
                {
                    cbCliente.DataSource = controle.Get(x => 
                        x.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                    cbCliente.ValueMember = "CODCLIENTE";
                    cbCliente.DisplayMember = "NOMEFANTASIA";
                    cbCliente.SelectedIndex = -1;
                }
            }

            txtFim.Text = DateTime.Now.Year.ToString();
            txtInicio.Text = DateTime.Now.AddDays(-1).Year.ToString();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInicio.Text))
                txtInicio.Text = DateTime.Now.AddDays(-1).Year.ToString();

            if (string.IsNullOrEmpty(txtFim.Text))
                txtFim.Text = DateTime.Now.Year.ToString();

            AnoFim = Convert.ToInt32(txtFim.Text);
            AnoInicio = Convert.ToInt32(txtInicio.Text);
            codCliente = (cbCliente.SelectedIndex < 0) ? (short?)null : 
                Convert.ToInt16(cbCliente.SelectedValue);

            salvo = true;
            this.Close();
        }

        private void txtInicio_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }

        private void txtFim_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }
    }
}
