﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGridConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Dashboard
{
    public partial class FormTabela : Form
    {
        private BindingSource bind;

        public FormTabela(BindingSource source, string text)
        {
            InitializeComponent();
            this.Text = text;
            bind = source;
        }

        private void FormTabela_Load(object sender, EventArgs e)
        {
            dgvChart3.DataSource = bind;
            lblStatus.Text = $"Total de registros: {bind.Count}";
            bind.EndEdit();
        }

        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            string arquivo = Funcoes.SalvaArquivo("Planilha Excel (*.xls)|*.xls", $"{this.Text}.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions();
                var excelEngine = dgvChart3.ExportToExcel(dgvChart3.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo);
                System.Windows.Forms.Cursor.Current = Cursors.Default;
                Global.MsgInformacao("Registros exportados com sucesso.");
            }
        }
    }
}
