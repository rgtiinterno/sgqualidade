﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using System;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Dashboard
{
    public partial class FormGrafico : Form
    {
        private object obj;
        private string eixoX;
        private string eixoY;
        private bool showLabel;
        private SeriesChartType tpSerie;

        public FormGrafico(string EixoX, string EixoY, bool ValueShowLabel, SeriesChartType type, object graphic, string text)
        {
            InitializeComponent();

            eixoX = EixoX;
            eixoY = EixoY;
            showLabel = ValueShowLabel;
            tpSerie = type;
            obj = graphic;
            this.Text = text;
        }

        private void FormGrafico_Load(object sender, EventArgs e)
        {
            chart.DataSource = obj;
            Global.CreateGraphic(eixoX, eixoY, showLabel, tpSerie, ref chart);
        }

        private void tsbSalvar_Click(object sender, EventArgs e)
        {
            string caminho = Funcoes.SelecionaDiretorioSaida() + $"\\{this.Text}.png";
            this.chart.SaveImage(caminho, ChartImageFormat.Png);

            Global.MsgSucesso("Imagem salva com sucesso!");
        }
    }
}
