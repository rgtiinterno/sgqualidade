﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using Syncfusion.WinForms.DataGridConverter;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Dashboard
{
    public partial class FormDashboard : Form
    {
        private bool inicio = true;
        private bool inicioCombo = true;
        private Filtro filtro = new Filtro();
        private Filtro filtroGlobal = new Filtro();

        public FormDashboard()
        {
            InitializeComponent();
        }

        private void FormDashboard_Load(object sender, EventArgs e)
        {
            CarregaCombos();
            CarregaGraficos();
            inicioCombo = false;
        }

        private void CarregaCombos()
        {
            cbChart1.DataSource = Graficos.GetList();
            cbChart1.ValueMember = "Valor";
            cbChart1.DisplayMember = "Resumo";
            cbChart1.SelectedIndex = 0;

            cbChart2.DataSource = Graficos.GetList();
            cbChart2.ValueMember = "Valor";
            cbChart2.DisplayMember = "Resumo";
            cbChart2.SelectedIndex = 0;
        }

        private void CarregaGraficos()
        {
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                #region: "GRÁFICO 01 - GRÁFICO DE PROPOSTAS POR STATUS"
                SeriesChartType typeChart1 = (SeriesChartType)Enum.Parse(typeof(SeriesChartType),
                    Convert.ToString(cbChart1.SelectedValue));

                chart1.DataSource = controle.GetDashPropostasGeralStatus();
                Global.CreateGraphic("EixoX", "EixoY", true, typeChart1, ref chart1);
                #endregion

                #region: "GRÁFICO 02 - GRÁFICO DE PROPOSTAS NO ANO CORRENTE"
                SeriesChartType typeChart2 = (SeriesChartType)Enum.Parse(typeof(SeriesChartType),
                    Convert.ToString(cbChart2.SelectedValue));

                chart2.DataSource = controle.GetDashPropostasGeralNoAno();
                Global.CreateGraphic("EixoX", "EixoY", true, typeChart2, ref chart2);
                #endregion

                #region: "TABELA 03 - TABELA GLOBAL: PROPOSTAS ENVIADAS X VENCIDAS"
                if (filtroGlobal != null)
                {
                    BindingSource bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGerais(
                       filtroGlobal.inicio, filtroGlobal.fim);
                    dgvChart4.DataSource = bind;
                    bind.EndEdit();
                }
                else
                {
                    BindingSource bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGerais();
                    dgvChart3.DataSource = bind;
                    bind.EndEdit();
                }

                using (ControlePMetas metas = new ControlePMetas())
                {
                    labelTable.Text = metas.Info();
                }
                #endregion

                #region: "TABELA 04 - PROPOSTAS ENVIADAS X VENCIDAS"
                if (filtro != null)
                {
                    BindingSource bind2 = new BindingSource();
                    bind2.DataSource = controle.GetTablePropostasGeraisPorClientes(
                    filtro.inicio, filtro.fim, filtro.cliente);
                    dgvChart4.DataSource = bind2;
                    bind2.EndEdit();
                }
                else
                {
                    BindingSource bind2 = new BindingSource();
                    bind2.DataSource = controle.GetTablePropostasGeraisPorClientes();
                    dgvChart4.DataSource = bind2;
                    bind2.EndEdit();
                }
                #endregion
            }
        }

        private void FormDashboard_Activated(object sender, EventArgs e)
        {
            if (!inicio)
                CarregaGraficos();

            inicio = false;
        }

        private void chart1_DoubleClick(object sender, EventArgs e)
        {
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                SeriesChartType type = (SeriesChartType)Enum.Parse(typeof(SeriesChartType),
                    Convert.ToString(cbChart1.SelectedValue));

                FormGrafico frm = new FormGrafico("EixoX", "EixoY", true,
                type, controle.GetDashPropostasGeralStatus(), "GRÁFICO DE PROPOSTAS POR STATUS");
                frm.MdiParent = this.MdiParent;
                frm.Show();
            }
        }

        private void chart2_DoubleClick(object sender, EventArgs e)
        {
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                SeriesChartType type = (SeriesChartType)Enum.Parse(typeof(SeriesChartType),
                    Convert.ToString(cbChart2.SelectedValue));

                FormGrafico frm = new FormGrafico("EixoX", "EixoY", true,
                type, controle.GetDashPropostasGeralNoAno(), "GRÁFICO DE PROPOSTAS NO ANO CORRENTE");
                frm.MdiParent = this.MdiParent;
                frm.Show();
            }
        }

        private void cbChart1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!inicioCombo)
                CarregaGraficos();
        }

        private void cbChart2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!inicioCombo)
                CarregaGraficos();
        }

        private void btnFiltrarGerarChart3_Click(object sender, EventArgs e)
        {
            using (FormFiltro frm = new FormFiltro())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    using (ControlePPropostas controle = new ControlePPropostas())
                    {
                        BindingSource bind = new BindingSource();
                        bind.DataSource = controle.GetTablePropostasGerais(frm.AnoInicio, frm.AnoFim);
                        dgvChart3.DataSource = bind;
                        bind.EndEdit();
                    }

                    filtroGlobal = new Filtro
                    {
                        cliente = null,
                        fim = frm.AnoFim,
                        inicio = frm.AnoInicio,
                    };
                }


                frm.Dispose();
            }
        }

        private void btnFiltrarGerarChart4_Click(object sender, EventArgs e)
        {
            using (FormFiltro frm = new FormFiltro(false))
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    using (ControlePPropostas controle = new ControlePPropostas())
                    {
                        BindingSource bind = new BindingSource();
                        bind.DataSource = controle.GetTablePropostasGeraisPorClientes(frm.AnoInicio,
                               frm.AnoFim, frm.codCliente);
                        dgvChart4.DataSource = bind;
                        bind.EndEdit();
                    }


                    filtro = new Filtro
                    {
                        cliente = frm.codCliente,
                        fim = frm.AnoFim,
                        inicio = frm.AnoInicio,
                    };
                }

                frm.Dispose();
            }
        }

        public bool GeraArquivoGlobal(int InicioGlobal, int FimGlobal, string arquivo)
        {
            string IGlobal = Criptografia.Encrypt(InicioGlobal.ToString());
            string FGlobal = Criptografia.Encrypt(FimGlobal.ToString());

            try
            {
                using (StreamWriter sw = new StreamWriter(arquivo, false))
                {
                    sw.WriteLine("Inico;" + IGlobal);
                    sw.WriteLine("Fim;" + FGlobal);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao criar arquivo de conexão: '" + arquivo + "'.\n" + ex.Message);
                return false;
            }
        }

        public bool GeraArquivo(int Inicio, int Fim, short? Cliente, string arquivo)
        {
            string InicioPeriodo = Criptografia.Encrypt(Inicio.ToString());
            string fimPeriodo = Criptografia.Encrypt(Fim.ToString());
            string CodCliente = string.Empty;
            if (Cliente != null)
                CodCliente = Cliente.Value.ToString();

            try
            {
                using (StreamWriter sw = new StreamWriter(arquivo, false))
                {
                    sw.WriteLine("Inico;" + InicioPeriodo);
                    sw.WriteLine("Fim;" + fimPeriodo);
                    sw.WriteLine("Cliente;" + CodCliente);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao criar arquivo de conexão: '" + arquivo + "'.\n" + ex.Message);
                return false;
            }
        }

        private void tsbAtualizarTGPEV_Click(object sender, EventArgs e)
        {
            CarregaGraficos();
        }

        private void tsbExportarTGPEV_Click(object sender, EventArgs e)
        {
            string arquivo = Funcoes.SalvaArquivo("Planilha Excel (*.xls)|*.xls",
                "TABELA GLOBAL: PROPOSTAS ENVIADAS X VENCIDAS.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions();
                var excelEngine = dgvChart3.ExportToExcel(dgvChart3.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo);
                System.Windows.Forms.Cursor.Current = Cursors.Default;
                Global.MsgInformacao("Registros exportados com sucesso.");
            }
        }

        private void tsbAmpliarTGPEV_Click(object sender, EventArgs e)
        {
            BindingSource bind = new BindingSource();
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                if (filtroGlobal != null)
                {
                    bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGerais(
                       filtroGlobal.inicio, filtroGlobal.fim);
                    dgvChart4.DataSource = bind;
                    bind.EndEdit();
                }
                else
                {
                    bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGerais();
                    dgvChart3.DataSource = bind;
                    bind.EndEdit();
                }
            }

            FormTabela frm = new FormTabela(bind, "TABELA GLOBAL: PROPOSTAS ENVIADAS X VENCIDAS");
            frm.MdiParent = this.MdiParent;
            frm.Show();
        }

        private void tsbAtualizarPEV_Click(object sender, EventArgs e)
        {
            CarregaGraficos();
        }

        private void tsbExportarPEV_Click(object sender, EventArgs e)
        {
            string arquivo = Funcoes.SalvaArquivo("Planilha Excel (*.xls)|*.xls", "PROPOSTAS ENVIADAS X VENCIDAS.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions();
                var excelEngine = dgvChart4.ExportToExcel(dgvChart4.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo);
                System.Windows.Forms.Cursor.Current = Cursors.Default;
                Global.MsgInformacao("Registros exportados com sucesso.");
            }
        }

        private void tsbAmpliarPEV_Click(object sender, EventArgs e)
        {
            BindingSource bind = new BindingSource();
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                if (filtro != null)
                {
                    bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGeraisPorClientes(
                    filtro.inicio, filtro.fim, filtro.cliente);
                    dgvChart4.DataSource = bind;
                    bind.EndEdit();
                }
                else
                {
                    bind = new BindingSource();
                    bind.DataSource = controle.GetTablePropostasGeraisPorClientes();
                    dgvChart4.DataSource = bind;
                    bind.EndEdit();
                }
            }

            FormTabela frm = new FormTabela(bind, "PROPOSTAS ENVIADAS X VENCIDAS");
            frm.MdiParent = this.MdiParent;
            frm.Show();
        }
    }
}
