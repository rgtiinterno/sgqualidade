﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Propostas
{
    public partial class FormListaPropostas : Form
    {
        public FormListaPropostas()
        {
            InitializeComponent();
        }

        private void FormListaPropostas_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Cód. Proposta", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODSETOR", HeaderText = "Cód. Setor", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCLIENTE", HeaderText = "Cód. Cliente", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NROCONTRATO", HeaderText = "Nro. Contrato", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODINTERESSE", HeaderText = "Cód. Interesse", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODSTATUS", HeaderText = "Cód. Status", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SETOR", HeaderText = "Setor", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CLIENTE", HeaderText = "Cliente", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "PROCESSO", HeaderText = "Processo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "INTERESSE", HeaderText = "Interesse", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "STATUS", HeaderText = "Status", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridNumericColumn() { MappingName = "VLRPROPOSTA", HeaderText = "Vlr. da Proposta", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "REVISAO", HeaderText = "Rev.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAINICIO", HeaderText = "Data Inicio", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAENVIO", HeaderText = "Data Envio", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAANALISECRITICA", HeaderText = "Data Análise Crítica", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAVENCIDO", HeaderText = "Data Vencido", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "VISITA", HeaderText = "Visita", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TEMVISITA", HeaderText = "Visita?", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVEL", HeaderText = "Responsável", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "TEMANEXO", HeaderText = "Anexos?", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACOES", HeaderText = "Observações", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControlePPropostas controle = new ControlePPropostas())
            {
                Cursor.Current = Cursors.WaitCursor;
                pPROPOSTABindingSource = new BindingSource();
                pPROPOSTABindingSource.DataSource = typeof(PPROPOSTAS);

                if (string.IsNullOrWhiteSpace(txtAno.Text) || !Global.ValidaAno(txtAno.Text))
                    pPROPOSTABindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                else
                    pPROPOSTABindingSource.DataSource = controle.GetList(
                            FormPrincipal.getUsuarioAcesso().CODCOLIGADA, Convert.ToInt32(txtAno.Text)).ToList();

                dgvListagem.DataSource = pPROPOSTABindingSource;
                pPROPOSTABindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {pPROPOSTABindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormPropostas frm = new FormPropostas())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            PPROPOSTAS propostas = (dgvListagem.SelectedItem as PPROPOSTAS);

            if (propostas.CKSELECIONADO)
            {
                using (FormPropostas frm = new FormPropostas(propostas))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
            {
                using (FormMsg frm = new FormMsg("Selecione um registro valído para " +
                    "a edição do mesmo."))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            PPROPOSTAS propostas = (dgvListagem.SelectedItem as PPROPOSTAS);
            if (propostas.CKSELECIONADO)
            {
                if (propostas.STATUS == "Elaboração de Proposta")
                {
                    try
                    {
                        using (ControlePPropostasHst controle = new ControlePPropostasHst())
                        {
                            controle.Delete(x => x.CODCOLIGADA == propostas.CODCOLIGADA &&
                                x.CODPROPOSTA == propostas.CODIGO);
                            controle.SaveAll();
                        }

                        using (ControlePPropostasAnexo controle = new ControlePPropostasAnexo())
                        {
                            controle.Delete(x => x.CODCOLIGADA == propostas.CODCOLIGADA &&
                                x.CODPROPOSTA == propostas.CODIGO);
                            controle.SaveAll();
                        }

                        using (ControlePAnalise controle = new ControlePAnalise())
                        {
                            controle.Delete(x => x.CODCOLIGADA == propostas.CODCOLIGADA &&
                                x.CODPROPOSTA == propostas.CODIGO);
                            controle.SaveAll();
                        }

                        using (ControlePPropostas controle = new ControlePPropostas())
                        {
                            controle.Delete(x => x.CODCOLIGADA == propostas.CODCOLIGADA &&
                                x.CODIGO == propostas.CODIGO);
                            controle.SaveAll();
                        }

                        tsbAtualizar.PerformClick();
                    }
                    catch (Exception ex)
                    {
                        using (FormMsg frm = new FormMsg(ex))
                        {
                            frm.StartPosition = FormStartPosition.CenterParent;
                            frm.WindowState = FormWindowState.Normal;
                            frm.ShowDialog();
                        }
                    }
                }
                else
                {
                    using (FormMsg frm = new FormMsg("O sistema não conseguiu realizar a exclusão da proposta." + Environment.NewLine +
                        $"Proposta com o status: {propostas.STATUS}"))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        frm.Dispose();
                    }
                }
            }
            else
            {
                using (FormMsg frm = new FormMsg("Selecione um registro valído para " +
                    "efetuar a exclusão do mesmo."))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
        }

        private void tsbHistorico_Click(object sender, EventArgs e)
        {
            PPROPOSTAS propostas = (dgvListagem.SelectedItem as PPROPOSTAS);

            if (propostas.CKSELECIONADO)
            {
                using (FormHistoricos frm = new FormHistoricos(propostas))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
            else
            {
                using (FormMsg frm = new FormMsg("Selecione um registro valído para " +
                    "a listagem do Histórico."))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbStatus_Click(object sender, EventArgs e)
        {
            PPROPOSTAS propostas = (dgvListagem.SelectedItem as PPROPOSTAS);

            if (propostas != null && propostas.CKSELECIONADO)
            {
                using (FormStatusProposta frm = new FormStatusProposta(propostas))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
            {
                using (FormMsg frm = new FormMsg("Selecione um registro valído para " +
                    "a alteração de status da proposta."))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    frm.Dispose();
                }
            }
        }

        private void tsbExportar_Click(object sender, EventArgs e)
        {
            string arquivo = Funcoes.SalvaArquivo("Planilha Excel (*.xls)|*.xls", "Lista de Propostas.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions();
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo);
                Cursor.Current = Cursors.Default;
                Global.MsgSucesso("Registros exportados com sucesso.");
            }
        }

        private void tsbFiltro_Click(object sender, EventArgs e)
        {
            tsbAtualizar.PerformClick();
        }
    }
}
