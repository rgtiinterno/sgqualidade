﻿namespace QUALIDADE.Forms.Cadastros.ModuloCP.Propostas
{
    partial class FormPropostas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPropostas));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.miniToolStrip = new System.Windows.Forms.ToolStrip();
            this.tabAnexos = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvAnexos = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.btnRemover = new System.Windows.Forms.ToolStripButton();
            this.btnVer = new System.Windows.Forms.ToolStripButton();
            this.btnAtualizar = new System.Windows.Forms.ToolStripButton();
            this.tabAnalise = new System.Windows.Forms.TabPage();
            this.tablePerguntas = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPrincipal = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCodInterno = new System.Windows.Forms.TextBox();
            this.cbResponsavel = new System.Windows.Forms.ComboBox();
            this.txtDtDeclinio = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ckCAPEXNao = new System.Windows.Forms.CheckBox();
            this.ckCAPEXSim = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ckTACNao = new System.Windows.Forms.CheckBox();
            this.ckTACSim = new System.Windows.Forms.CheckBox();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNroContrato = new System.Windows.Forms.TextBox();
            this.txtCodColigada = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDtVencida = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtVlrProposta = new System.Windows.Forms.TextBox();
            this.txtDtInicio = new System.Windows.Forms.MaskedTextBox();
            this.txtDtAnaliseCritica = new System.Windows.Forms.MaskedTextBox();
            this.txtDtEnvio = new System.Windows.Forms.MaskedTextBox();
            this.cbInteresse = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ckNao = new System.Windows.Forms.CheckBox();
            this.btnAnexar = new System.Windows.Forms.Button();
            this.ckSim = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProcesso = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbCliente = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cbSetor = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtRevisao = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabAnexos.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexos)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.tabAnalise.SuspendLayout();
            this.tablePerguntas.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(695, 476);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 101;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnGravar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGravar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGravar.Location = new System.Drawing.Point(583, 476);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(106, 27);
            this.btnGravar.TabIndex = 100;
            this.btnGravar.Text = "&Salvar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // miniToolStrip
            // 
            this.miniToolStrip.AccessibleName = "Nova seleção de item";
            this.miniToolStrip.AccessibleRole = System.Windows.Forms.AccessibleRole.ButtonDropDown;
            this.miniToolStrip.AutoSize = false;
            this.miniToolStrip.CanOverflow = false;
            this.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.miniToolStrip.Location = new System.Drawing.Point(129, 3);
            this.miniToolStrip.Name = "miniToolStrip";
            this.miniToolStrip.Size = new System.Drawing.Size(380, 25);
            this.miniToolStrip.TabIndex = 0;
            // 
            // tabAnexos
            // 
            this.tabAnexos.Controls.Add(this.panel1);
            this.tabAnexos.Controls.Add(this.toolStrip1);
            this.tabAnexos.Location = new System.Drawing.Point(4, 22);
            this.tabAnexos.Name = "tabAnexos";
            this.tabAnexos.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnexos.Size = new System.Drawing.Size(805, 444);
            this.tabAnexos.TabIndex = 2;
            this.tabAnexos.Text = "Anexos";
            this.tabAnexos.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvAnexos);
            this.panel1.Location = new System.Drawing.Point(3, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 341);
            this.panel1.TabIndex = 2;
            // 
            // dgvAnexos
            // 
            this.dgvAnexos.AccessibleName = "Table";
            this.dgvAnexos.AllowEditing = false;
            this.dgvAnexos.AllowFiltering = true;
            this.dgvAnexos.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvAnexos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAnexos.Location = new System.Drawing.Point(0, 0);
            this.dgvAnexos.Name = "dgvAnexos";
            this.dgvAnexos.SerializationController = null;
            this.dgvAnexos.Size = new System.Drawing.Size(799, 341);
            this.dgvAnexos.TabIndex = 3;
            this.dgvAnexos.Text = "sfDataGrid1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.btnRemover,
            this.btnVer,
            this.btnAtualizar});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(799, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            this.btnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnAdd.Size = new System.Drawing.Size(40, 22);
            this.btnAdd.Text = "Adicionar";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemover
            // 
            this.btnRemover.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnRemover.Image = ((System.Drawing.Image)(resources.GetObject("btnRemover.Image")));
            this.btnRemover.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnRemover.Size = new System.Drawing.Size(40, 22);
            this.btnRemover.Text = "Remover";
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // btnVer
            // 
            this.btnVer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnVer.Image = ((System.Drawing.Image)(resources.GetObject("btnVer.Image")));
            this.btnVer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnVer.Name = "btnVer";
            this.btnVer.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnVer.Size = new System.Drawing.Size(40, 22);
            this.btnVer.Text = "Anexo";
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnAtualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.btnAtualizar.Size = new System.Drawing.Size(40, 22);
            this.btnAtualizar.Text = "Atualizar";
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // tabAnalise
            // 
            this.tabAnalise.AutoScroll = true;
            this.tabAnalise.Controls.Add(this.tablePerguntas);
            this.tabAnalise.Location = new System.Drawing.Point(4, 22);
            this.tabAnalise.Name = "tabAnalise";
            this.tabAnalise.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalise.Size = new System.Drawing.Size(805, 444);
            this.tabAnalise.TabIndex = 3;
            this.tabAnalise.Text = "Análise Crítica";
            this.tabAnalise.UseVisualStyleBackColor = true;
            // 
            // tablePerguntas
            // 
            this.tablePerguntas.AutoScroll = true;
            this.tablePerguntas.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tablePerguntas.ColumnCount = 4;
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.Controls.Add(this.label7, 3, 0);
            this.tablePerguntas.Controls.Add(this.label8, 2, 0);
            this.tablePerguntas.Controls.Add(this.label9, 1, 0);
            this.tablePerguntas.Controls.Add(this.label10, 0, 0);
            this.tablePerguntas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerguntas.Location = new System.Drawing.Point(3, 3);
            this.tablePerguntas.MaximumSize = new System.Drawing.Size(789, 377);
            this.tablePerguntas.MinimumSize = new System.Drawing.Size(789, 377);
            this.tablePerguntas.Name = "tablePerguntas";
            this.tablePerguntas.RowCount = 1;
            this.tablePerguntas.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablePerguntas.Size = new System.Drawing.Size(789, 377);
            this.tablePerguntas.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(711, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 21);
            this.label7.TabIndex = 3;
            this.label7.Text = "N/A";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(632, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 21);
            this.label8.TabIndex = 2;
            this.label8.Text = "NÃO";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(553, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "SIM";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(542, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "PERGUNTA";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.label17);
            this.tabPrincipal.Controls.Add(this.txtCodInterno);
            this.tabPrincipal.Controls.Add(this.cbResponsavel);
            this.tabPrincipal.Controls.Add(this.txtDtDeclinio);
            this.tabPrincipal.Controls.Add(this.label16);
            this.tabPrincipal.Controls.Add(this.groupBox6);
            this.tabPrincipal.Controls.Add(this.groupBox3);
            this.tabPrincipal.Controls.Add(this.cbStatus);
            this.tabPrincipal.Controls.Add(this.label15);
            this.tabPrincipal.Controls.Add(this.txtNroContrato);
            this.tabPrincipal.Controls.Add(this.txtCodColigada);
            this.tabPrincipal.Controls.Add(this.label14);
            this.tabPrincipal.Controls.Add(this.label13);
            this.tabPrincipal.Controls.Add(this.txtDtVencida);
            this.tabPrincipal.Controls.Add(this.label12);
            this.tabPrincipal.Controls.Add(this.txtVlrProposta);
            this.tabPrincipal.Controls.Add(this.txtDtInicio);
            this.tabPrincipal.Controls.Add(this.txtDtAnaliseCritica);
            this.tabPrincipal.Controls.Add(this.txtDtEnvio);
            this.tabPrincipal.Controls.Add(this.cbInteresse);
            this.tabPrincipal.Controls.Add(this.groupBox4);
            this.tabPrincipal.Controls.Add(this.groupBox2);
            this.tabPrincipal.Controls.Add(this.groupBox1);
            this.tabPrincipal.Controls.Add(this.label4);
            this.tabPrincipal.Controls.Add(this.cbCliente);
            this.tabPrincipal.Controls.Add(this.label1);
            this.tabPrincipal.Controls.Add(this.label5);
            this.tabPrincipal.Controls.Add(this.label3);
            this.tabPrincipal.Controls.Add(this.label6);
            this.tabPrincipal.Controls.Add(this.label11);
            this.tabPrincipal.Controls.Add(this.cbSetor);
            this.tabPrincipal.Controls.Add(this.label2);
            this.tabPrincipal.Controls.Add(this.label22);
            this.tabPrincipal.Controls.Add(this.groupBox5);
            this.tabPrincipal.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tabPrincipal.Location = new System.Drawing.Point(4, 22);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrincipal.Size = new System.Drawing.Size(805, 444);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Text = "Cadastro Principal";
            this.tabPrincipal.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(369, 251);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 46;
            this.label17.Text = "Cód.Interno:";
            // 
            // txtCodInterno
            // 
            this.txtCodInterno.Location = new System.Drawing.Point(372, 267);
            this.txtCodInterno.MaxLength = 80;
            this.txtCodInterno.Name = "txtCodInterno";
            this.txtCodInterno.Size = new System.Drawing.Size(220, 22);
            this.txtCodInterno.TabIndex = 15;
            // 
            // cbResponsavel
            // 
            this.cbResponsavel.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbResponsavel.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbResponsavel.FormattingEnabled = true;
            this.cbResponsavel.Location = new System.Drawing.Point(485, 121);
            this.cbResponsavel.Name = "cbResponsavel";
            this.cbResponsavel.Size = new System.Drawing.Size(297, 21);
            this.cbResponsavel.TabIndex = 8;
            // 
            // txtDtDeclinio
            // 
            this.txtDtDeclinio.Location = new System.Drawing.Point(602, 175);
            this.txtDtDeclinio.Mask = "99/99/9999";
            this.txtDtDeclinio.Name = "txtDtDeclinio";
            this.txtDtDeclinio.ReadOnly = true;
            this.txtDtDeclinio.Size = new System.Drawing.Size(107, 22);
            this.txtDtDeclinio.TabIndex = 11;
            this.txtDtDeclinio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(599, 159);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "Dt. Declínio:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.ckCAPEXNao);
            this.groupBox6.Controls.Add(this.ckCAPEXSim);
            this.groupBox6.Location = new System.Drawing.Point(602, 361);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(180, 53);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "CAPEX:";
            // 
            // ckCAPEXNao
            // 
            this.ckCAPEXNao.AutoSize = true;
            this.ckCAPEXNao.Checked = true;
            this.ckCAPEXNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckCAPEXNao.Location = new System.Drawing.Point(101, 23);
            this.ckCAPEXNao.Name = "ckCAPEXNao";
            this.ckCAPEXNao.Size = new System.Drawing.Size(47, 17);
            this.ckCAPEXNao.TabIndex = 21;
            this.ckCAPEXNao.Text = "Não";
            this.ckCAPEXNao.UseVisualStyleBackColor = true;
            this.ckCAPEXNao.CheckedChanged += new System.EventHandler(this.ckCAPEXNao_CheckedChanged);
            // 
            // ckCAPEXSim
            // 
            this.ckCAPEXSim.AutoSize = true;
            this.ckCAPEXSim.Location = new System.Drawing.Point(36, 23);
            this.ckCAPEXSim.Name = "ckCAPEXSim";
            this.ckCAPEXSim.Size = new System.Drawing.Size(44, 17);
            this.ckCAPEXSim.TabIndex = 20;
            this.ckCAPEXSim.Text = "Sim";
            this.ckCAPEXSim.UseVisualStyleBackColor = true;
            this.ckCAPEXSim.CheckedChanged += new System.EventHandler(this.ckCAPEXSim_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ckTACNao);
            this.groupBox3.Controls.Add(this.ckTACSim);
            this.groupBox3.Location = new System.Drawing.Point(602, 302);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(180, 53);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "TAC:";
            // 
            // ckTACNao
            // 
            this.ckTACNao.AutoSize = true;
            this.ckTACNao.Checked = true;
            this.ckTACNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckTACNao.Location = new System.Drawing.Point(101, 23);
            this.ckTACNao.Name = "ckTACNao";
            this.ckTACNao.Size = new System.Drawing.Size(47, 17);
            this.ckTACNao.TabIndex = 19;
            this.ckTACNao.Text = "Não";
            this.ckTACNao.UseVisualStyleBackColor = true;
            this.ckTACNao.CheckedChanged += new System.EventHandler(this.ckTACNao_CheckedChanged);
            // 
            // ckTACSim
            // 
            this.ckTACSim.AutoSize = true;
            this.ckTACSim.Location = new System.Drawing.Point(36, 23);
            this.ckTACSim.Name = "ckTACSim";
            this.ckTACSim.Size = new System.Drawing.Size(44, 17);
            this.ckTACSim.TabIndex = 18;
            this.ckTACSim.Text = "Sim";
            this.ckTACSim.UseVisualStyleBackColor = true;
            this.ckTACSim.CheckedChanged += new System.EventHandler(this.ckTACSim_CheckedChanged);
            // 
            // cbStatus
            // 
            this.cbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbStatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(624, 18);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(158, 21);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(576, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "Status:";
            // 
            // txtNroContrato
            // 
            this.txtNroContrato.Location = new System.Drawing.Point(348, 18);
            this.txtNroContrato.Name = "txtNroContrato";
            this.txtNroContrato.Size = new System.Drawing.Size(213, 22);
            this.txtNroContrato.TabIndex = 1;
            // 
            // txtCodColigada
            // 
            this.txtCodColigada.BackColor = System.Drawing.SystemColors.Control;
            this.txtCodColigada.Enabled = false;
            this.txtCodColigada.Location = new System.Drawing.Point(70, 18);
            this.txtCodColigada.Name = "txtCodColigada";
            this.txtCodColigada.Size = new System.Drawing.Size(100, 22);
            this.txtCodColigada.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(261, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Nro. Contrato:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(8, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Coligada:";
            // 
            // txtDtVencida
            // 
            this.txtDtVencida.Location = new System.Drawing.Point(485, 222);
            this.txtDtVencida.Mask = "99/99/9999";
            this.txtDtVencida.Name = "txtDtVencida";
            this.txtDtVencida.ReadOnly = true;
            this.txtDtVencida.Size = new System.Drawing.Size(107, 22);
            this.txtDtVencida.TabIndex = 13;
            this.txtDtVencida.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(482, 206);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Dt. Vencida:";
            // 
            // txtVlrProposta
            // 
            this.txtVlrProposta.Location = new System.Drawing.Point(101, 414);
            this.txtVlrProposta.Name = "txtVlrProposta";
            this.txtVlrProposta.Size = new System.Drawing.Size(107, 22);
            this.txtVlrProposta.TabIndex = 6;
            this.txtVlrProposta.Visible = false;
            this.txtVlrProposta.TextChanged += new System.EventHandler(this.txtVlrProposta_TextChanged);
            this.txtVlrProposta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVlrProposta_KeyPress);
            // 
            // txtDtInicio
            // 
            this.txtDtInicio.Location = new System.Drawing.Point(372, 175);
            this.txtDtInicio.Mask = "99/99/9999";
            this.txtDtInicio.Name = "txtDtInicio";
            this.txtDtInicio.Size = new System.Drawing.Size(107, 22);
            this.txtDtInicio.TabIndex = 9;
            this.txtDtInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDtAnaliseCritica
            // 
            this.txtDtAnaliseCritica.Location = new System.Drawing.Point(372, 222);
            this.txtDtAnaliseCritica.Mask = "99/99/9999";
            this.txtDtAnaliseCritica.Name = "txtDtAnaliseCritica";
            this.txtDtAnaliseCritica.Size = new System.Drawing.Size(107, 22);
            this.txtDtAnaliseCritica.TabIndex = 12;
            this.txtDtAnaliseCritica.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtDtEnvio
            // 
            this.txtDtEnvio.Location = new System.Drawing.Point(485, 175);
            this.txtDtEnvio.Mask = "99/99/9999";
            this.txtDtEnvio.Name = "txtDtEnvio";
            this.txtDtEnvio.Size = new System.Drawing.Size(107, 22);
            this.txtDtEnvio.TabIndex = 10;
            this.txtDtEnvio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbInteresse
            // 
            this.cbInteresse.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbInteresse.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbInteresse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInteresse.FormattingEnabled = true;
            this.cbInteresse.Location = new System.Drawing.Point(567, 70);
            this.cbInteresse.Name = "cbInteresse";
            this.cbInteresse.Size = new System.Drawing.Size(215, 21);
            this.cbInteresse.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ckNao);
            this.groupBox4.Controls.Add(this.btnAnexar);
            this.groupBox4.Controls.Add(this.ckSim);
            this.groupBox4.Location = new System.Drawing.Point(602, 213);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(180, 83);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Visita";
            // 
            // ckNao
            // 
            this.ckNao.AutoSize = true;
            this.ckNao.Checked = true;
            this.ckNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckNao.Location = new System.Drawing.Point(101, 21);
            this.ckNao.Name = "ckNao";
            this.ckNao.Size = new System.Drawing.Size(47, 17);
            this.ckNao.TabIndex = 16;
            this.ckNao.Text = "Não";
            this.ckNao.UseVisualStyleBackColor = true;
            this.ckNao.CheckedChanged += new System.EventHandler(this.ckNao_CheckedChanged);
            // 
            // btnAnexar
            // 
            this.btnAnexar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnexar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAnexar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnexar.Location = new System.Drawing.Point(36, 44);
            this.btnAnexar.Name = "btnAnexar";
            this.btnAnexar.Size = new System.Drawing.Size(112, 27);
            this.btnAnexar.TabIndex = 17;
            this.btnAnexar.Text = "Anexar Rel. Visita";
            this.btnAnexar.UseVisualStyleBackColor = true;
            this.btnAnexar.Click += new System.EventHandler(this.btnAnexar_Click);
            // 
            // ckSim
            // 
            this.ckSim.AutoSize = true;
            this.ckSim.Location = new System.Drawing.Point(36, 21);
            this.ckSim.Name = "ckSim";
            this.ckSim.Size = new System.Drawing.Size(44, 17);
            this.ckSim.TabIndex = 15;
            this.ckSim.Text = "Sim";
            this.ckSim.UseVisualStyleBackColor = true;
            this.ckSim.CheckedChanged += new System.EventHandler(this.ckSim_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtObservacao);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox2.Location = new System.Drawing.Point(11, 291);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(581, 145);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Observações";
            // 
            // txtObservacao
            // 
            this.txtObservacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservacao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtObservacao.Location = new System.Drawing.Point(3, 18);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(575, 124);
            this.txtObservacao.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProcesso);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(11, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 179);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Processo:";
            // 
            // txtProcesso
            // 
            this.txtProcesso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProcesso.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtProcesso.Location = new System.Drawing.Point(3, 18);
            this.txtProcesso.Multiline = true;
            this.txtProcesso.Name = "txtProcesso";
            this.txtProcesso.Size = new System.Drawing.Size(340, 158);
            this.txtProcesso.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(564, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Interesse:";
            // 
            // cbCliente
            // 
            this.cbCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCliente.FormattingEnabled = true;
            this.cbCliente.Location = new System.Drawing.Point(11, 70);
            this.cbCliente.Name = "cbCliente";
            this.cbCliente.Size = new System.Drawing.Size(331, 21);
            this.cbCliente.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(8, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "Cliente:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(482, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Responsável:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(19, 397);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Vlr. Proposta:";
            this.label3.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(369, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Dt. Inicio Proposta:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(369, 206);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Dt. Análise Crítica:";
            // 
            // cbSetor
            // 
            this.cbSetor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbSetor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbSetor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSetor.FormattingEnabled = true;
            this.cbSetor.Items.AddRange(new object[] {
            "Melhoria",
            "Corretiva"});
            this.cbSetor.Location = new System.Drawing.Point(348, 69);
            this.cbSetor.Name = "cbSetor";
            this.cbSetor.Size = new System.Drawing.Size(213, 21);
            this.cbSetor.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(482, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Dt. Envio Proposta:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(345, 52);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(37, 13);
            this.label22.TabIndex = 32;
            this.label22.Text = "Setor:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtRevisao);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox5.Location = new System.Drawing.Point(363, 103);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(116, 48);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Revisão";
            // 
            // txtRevisao
            // 
            this.txtRevisao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRevisao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtRevisao.Location = new System.Drawing.Point(3, 18);
            this.txtRevisao.Name = "txtRevisao";
            this.txtRevisao.Size = new System.Drawing.Size(110, 22);
            this.txtRevisao.TabIndex = 7;
            this.txtRevisao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPrincipal);
            this.tabControl.Controls.Add(this.tabAnalise);
            this.tabControl.Controls.Add(this.tabAnexos);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(813, 470);
            this.tabControl.TabIndex = 33;
            // 
            // FormPropostas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 511);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGravar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPropostas";
            this.Text = "Propostas";
            this.Load += new System.EventHandler(this.FormPropostas_Load);
            this.tabAnexos.ResumeLayout(false);
            this.tabAnexos.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexos)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabAnalise.ResumeLayout(false);
            this.tablePerguntas.ResumeLayout(false);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPrincipal.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.ToolStrip miniToolStrip;
        private System.Windows.Forms.TabPage tabAnexos;
        private System.Windows.Forms.Panel panel1;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvAnexos;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripButton btnRemover;
        private System.Windows.Forms.ToolStripButton btnVer;
        private System.Windows.Forms.ToolStripButton btnAtualizar;
        private System.Windows.Forms.TabPage tabAnalise;
        private System.Windows.Forms.TableLayoutPanel tablePerguntas;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPrincipal;
        private System.Windows.Forms.TextBox txtVlrProposta;
        private System.Windows.Forms.MaskedTextBox txtDtInicio;
        private System.Windows.Forms.MaskedTextBox txtDtEnvio;
        private System.Windows.Forms.ComboBox cbInteresse;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox ckNao;
        private System.Windows.Forms.Button btnAnexar;
        private System.Windows.Forms.CheckBox ckSim;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtProcesso;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbSetor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtRevisao;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.MaskedTextBox txtDtAnaliseCritica;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNroContrato;
        private System.Windows.Forms.TextBox txtCodColigada;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtDtVencida;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox ckCAPEXNao;
        private System.Windows.Forms.CheckBox ckCAPEXSim;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox ckTACNao;
        private System.Windows.Forms.CheckBox ckTACSim;
        private System.Windows.Forms.MaskedTextBox txtDtDeclinio;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cbResponsavel;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCodInterno;
    }
}