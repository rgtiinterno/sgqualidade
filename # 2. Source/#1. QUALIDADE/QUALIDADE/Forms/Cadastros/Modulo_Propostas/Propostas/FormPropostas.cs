﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Propostas
{
    public partial class FormPropostas : Form
    {
        public bool salvo = false;
        private PPROPOSTAS itemEdit;
        private List<PPROPANEXO> anexos;
        private int codigo;

        public FormPropostas(PPROPOSTAS propostas = null)
        {
            InitializeComponent();
            itemEdit = propostas;
            anexos = new List<PPROPANEXO>();
            txtCodColigada.Text = FormPrincipal.getUsuarioAcesso().CODCOLIGADA.ToString();
            if (itemEdit != null)
            {
                codigo = itemEdit.CODIGO;
                PreencheTabela(codigo, itemEdit.CODCOLIGADA);
            }
            else
            {
                using (ControlePPropostas controle = new ControlePPropostas())
                {
                    codigo = controle.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                    PreencheTabela(codigo, FormPrincipal.getUsuarioAcesso().CODCOLIGADA);
                }
            }
        }

        private void PreencheTabela(int codigo, int coligada)
        {
            List<Pergunta> perguntas = new Pergunta().GetPerguntas(codigo, coligada);
            tablePerguntas.RowCount = perguntas.Count + 1;

            for (int i = 1; i <= perguntas.Count; i++)
            {
                tablePerguntas.Controls.Add(GetLabel(perguntas[i - 1], 0, i), 0, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Sim", 1, i), 1, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Nao", 2, i), 2, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "NA", 3, i), 3, i);
            }
        }

        private Control GetLabel(Pergunta pergunta, int coluna, int linha)
        {
            return new Label
            {
                Dock = DockStyle.Fill,
                Name = $"lblPergunta{pergunta.Codigo}",
                TabIndex = coluna + linha,
                Text = pergunta.DescPergunta,
                TextAlign = ContentAlignment.MiddleLeft
            };
        }

        private Control GetCheckbox(Pergunta pergunta, string resposta, int coluna, int linha)
        {
            bool checado = false;
            if (resposta == "Sim")
                checado = pergunta.Sim;
            else if (resposta == "Nao")
                checado = pergunta.Nao;
            else if (resposta == "NA")
                checado = pergunta.NA;

            CheckBox check = new CheckBox
            {
                Dock = DockStyle.Fill,
                CheckAlign = ContentAlignment.MiddleCenter,
                UseVisualStyleBackColor = true,
                Name = $"ck{resposta}{pergunta.Codigo}",
                TabIndex = coluna + linha,
                TextAlign = ContentAlignment.MiddleLeft,
                Tag = pergunta,
                Checked = checado
            };
            check.CheckedChanged += new EventHandler(OnCheckOpt);

            return check;
        }

        private void OnCheckOpt(object sender, EventArgs args)
        {
            CheckBox check = (sender as CheckBox);
            if (check != null)
            {
                if (check.Tag is Pergunta pergunta)
                {
                    string nomeSim = $"ckSim{pergunta.Codigo}";
                    string nomeNao = $"ckNao{pergunta.Codigo}";
                    string nomeNA = $"ckNA{pergunta.Codigo}";
                    CheckBox checkSim = GetCheckBox(nomeSim);
                    CheckBox checkNao = GetCheckBox(nomeNao);
                    CheckBox checkNA = GetCheckBox(nomeNA);

                    if (check.Checked && check.Name == nomeSim)
                    {
                        checkNao.Checked = false;
                        checkNA.Checked = false;
                    }
                    else if (check.Checked && check.Name == nomeNao)
                    {
                        checkSim.Checked = false;
                        checkNA.Checked = false;
                    }
                    else if (check.Checked && check.Name == nomeNA)
                    {
                        checkSim.Checked = false;
                        checkNao.Checked = false;
                    }
                }
            }
        }

        private CheckBox GetCheckBox(string nome)
        {
            Control control = tablePerguntas.Controls.Find(nome, true).FirstOrDefault();
            if (control != null && (control is CheckBox check))
                return check;
            return null;
        }

        private void FormPropostas_Load(object sender, EventArgs e)
        {
            tabControl.TabPages.Remove(tabAnexos);
            txtDtInicio.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDtAnaliseCritica.Text = DateTime.Now.ToString("dd/MM/yyyy");
            CarregaCombos();
            if (itemEdit != null)
            {
                cbCliente.SelectedValue = itemEdit.CODCLIENTE;
                cbInteresse.SelectedValue = itemEdit.CODINTERESSE;
                cbSetor.SelectedValue = itemEdit.CODSETOR;
                txtProcesso.Text = itemEdit.PROCESSO;
                txtRevisao.Text = itemEdit.REVISAO;
                txtDtEnvio.Text = itemEdit.DATAENVIO.DBNullToString();
                txtDtInicio.Text = itemEdit.DATAINICIO.DBNullToString();
                txtDtAnaliseCritica.Text = itemEdit.DATAANALISECRITICA.DBNullToString();
                txtCodInterno.Text = itemEdit.CODINTERNO;
                txtVlrProposta.Text = itemEdit.VLRPROPOSTA.DBNullToString();
                if (itemEdit.CODRESPONSAVEL.HasValue)
                    cbResponsavel.SelectedValue = itemEdit.CODRESPONSAVEL.Value;
                else
                    cbResponsavel.SelectedIndex = -1;
                //txtResponsavel.Text = itemEdit.RESPONSAVEL;
                ckNao.Checked = !itemEdit.VISITA;
                ckSim.Checked = itemEdit.VISITA;
                txtObservacao.Text = itemEdit.OBSERVACOES;
                txtNroContrato.Text = itemEdit.NROCONTRATO.DBNullToString();
                txtCodColigada.Text = itemEdit.CODCOLIGADA.ToString();
                txtDtVencida.Text = itemEdit.DATAVENCIDO.HasValue ? itemEdit.DATAVENCIDO.Value.ToString("dd/MM/yyyy") : null;
                cbCliente.Enabled = false;
                cbSetor.Enabled = false;
                cbStatus.SelectedValue = itemEdit.CODSTATUS;
                ckTACSim.Checked = itemEdit.TAC;
                ckTACNao.Checked = !itemEdit.TAC;
                ckCAPEXSim.Checked = itemEdit.CAPEX;
                ckCAPEXNao.Checked = !itemEdit.CAPEX;
                txtDtDeclinio.Text = itemEdit.DATADECLINIO.HasValue ? itemEdit.DATADECLINIO.Value.ToString("dd/MM/yyyy") : null;
                using (ControlePPropostasAnexo controleAnx = new ControlePPropostasAnexo())
                {
                    anexos = controleAnx.GetAnexos(itemEdit.CODCOLIGADA, itemEdit.CODIGO);
                }
                CarregaDGVAnexos();
            }
            else
            {
                cbStatus.SelectedValue = 2;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                PPROPOSTAS item = new PPROPOSTAS
                {
                    CODIGO = codigo,
                    CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    CODCLIENTE = Convert.ToInt16(cbCliente.SelectedValue),
                    CODINTERESSE = Convert.ToInt16(cbInteresse.SelectedValue),
                    CODSETOR = Convert.ToInt16(cbSetor.SelectedValue),
                    PROCESSO = txtProcesso.Text,
                    REVISAO = txtRevisao.Text,
                    CODSTATUS = cbStatus.SelectedIndex >= 0 ? cbStatus.SelectedValue.DBNullToInt().Value : 2,
                    VLRPROPOSTA = 0.00M,
                    DATAENVIO = !string.IsNullOrEmpty(Global.RemovePVTEsp(txtDtEnvio.Text)) ?
                        Convert.ToDateTime(txtDtEnvio.Text) : (DateTime?)null,
                    DATAANALISECRITICA = !string.IsNullOrEmpty(Global.RemovePVTEsp(txtDtAnaliseCritica.Text)) ?
                        Convert.ToDateTime(txtDtAnaliseCritica.Text) : (DateTime?)null,
                    DATAINICIO = Convert.ToDateTime(txtDtInicio.Text),
                    VISITA = ckSim.Checked,
                    OBSERVACOES = txtObservacao.Text,
                    CODRESPONSAVEL = cbResponsavel.SelectedValue.DBNullToInt(), 
                    RESPONSAVEL = cbResponsavel.Text,
                    TEMANEXO = (dgvAnexos != null && dgvAnexos.View != null &&
                        dgvAnexos.View.Records != null && (dgvAnexos.View.Records.Count > 0)
                    ),
                    NROCONTRATO = txtNroContrato.Text,
                    DATAVENCIDO = cbStatus.Text?.ToLower() == "vencido" ? txtDtVencida.Text.DBNullToDateTime() : null, 
                    TAC = ckCAPEXSim.Checked, 
                    CAPEX = ckCAPEXSim.Checked, 
                    DATADECLINIO = cbStatus.Text?.ToLower() == "declinado" ? txtDtDeclinio.Text.DBNullToDateTime() : null, 
                    CODINTERNO = txtCodInterno.Text.Trim(), 
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };

                List<PANALISE> listaAnalise = GetAnalise(item.CODIGO, item.CODCOLIGADA);

                using (ControlePPropostas controle = new ControlePPropostas())
                {
                    using (ControlePPropostasHst controleHst = new ControlePPropostasHst())
                    {
                        using (ControlePAnalise controleAnalise = new ControlePAnalise())
                        {
                            if (itemEdit == null)
                            {
                                PPROPOSTASHST proposta = new PPROPOSTASHST
                                {
                                    CODIGO = controleHst.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA),
                                    CODPROPOSTA = item.CODIGO,
                                    CODCOLIGADA = item.CODCOLIGADA,
                                    NOVAREVISAO = item.REVISAO,
                                    DATAENVIO = item.DATAENVIO,
                                    VLRPROPOSTA = item.VLRPROPOSTA,
                                    OBSERVACOES = item.OBSERVACOES,
                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                    RECCREATEDON = DateTime.Now
                                };

                                controle.Create(item);
                                controleHst.Create(proposta);
                                controleAnalise.CreateAll(listaAnalise);

                                controle.SaveAll();
                                controleHst.SaveAll();
                                controleAnalise.SaveAll();

                                if (anexos != null)
                                {
                                    using (ControlePPropostasAnexo controleAnx = new ControlePPropostasAnexo())
                                    {
                                        anexos.ForEach(x => x.CODPROPOSTA = item.CODIGO);
                                        controleAnx.CreateAll(anexos);
                                        controleAnx.SaveAll();
                                    }
                                }

                                salvo = true;
                                this.Close();
                            }
                            else
                            {
                                if (controleAnalise.Find(1, item.CODCOLIGADA) is PANALISE analiseItem)
                                {
                                    controleAnalise.Delete(x => x.CODCOLIGADA == item.CODCOLIGADA && x.CODPROPOSTA == item.CODIGO);
                                    controleAnalise.SaveAll();
                                }

                                item.CODSTATUS = itemEdit.CODSTATUS;
                                item.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                                item.RECMODIFIEDON = DateTime.Now;

                                PPROPOSTASHST proposta = new PPROPOSTASHST
                                {
                                    CODIGO = controleHst.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA),
                                    CODPROPOSTA = item.CODIGO,
                                    CODCOLIGADA = item.CODCOLIGADA,
                                    NOVAREVISAO = item.REVISAO,
                                    DATAENVIO = item.DATAENVIO,
                                    VLRPROPOSTA = item.VLRPROPOSTA,
                                    OBSERVACOES = item.OBSERVACOES,
                                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                                    RECCREATEDON = DateTime.Now
                                };

                                controle.Update(item, x => x.CODIGO == itemEdit.CODIGO && x.CODCOLIGADA == itemEdit.CODCOLIGADA);
                                controleHst.Create(proposta);
                                controleAnalise.CreateAll(listaAnalise);

                                controle.SaveAll();
                                controleHst.SaveAll();
                                controleAnalise.SaveAll();

                                using (ControlePPropostasAnexo controleAnx = new ControlePPropostasAnexo())
                                {
                                    controleAnx.Delete(item.CODCOLIGADA, item.CODIGO);
                                    controleAnx.SaveAll();

                                    if (anexos != null)
                                    {
                                        anexos.ForEach(x => x.CODPROPOSTA = item.CODIGO);
                                        controleAnx.CreateAll(anexos);
                                        controleAnx.SaveAll();
                                    }
                                }

                                salvo = true;
                                this.Close();
                            }
                        }
                    }
                }
            }
        }

        private List<PANALISE> GetAnalise(int CodProposta, int CodColigada)
        {
            List<PANALISE> lista = new List<PANALISE>();
            TableLayoutControlCollection control = tablePerguntas.Controls;

            for (int i = 1; i < tablePerguntas.RowCount; i++)
            {
                CheckBox nomeSim = control.Find($"ckSim{i}", true).FirstOrDefault() as CheckBox;
                CheckBox nomeNao = control.Find($"ckNao{i}", true).FirstOrDefault() as CheckBox;
                CheckBox nomeNA = control.Find($"ckNA{i}", true).FirstOrDefault() as CheckBox;

                PANALISE item = new PANALISE
                {
                    CODCOLIGADA = CodColigada,
                    CODPROPOSTA = CodProposta,
                    CODPERGUNTA = Convert.ToInt16(i),
                    SIM = nomeSim.Checked,
                    NAO = nomeNao.Checked,
                    NA = nomeNA.Checked,
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };

                lista.Add(item);
            }

            return lista;
        }

        private void CarregaCombos()
        {
            using (ControlePSetores controle = new ControlePSetores())
            {
                cbSetor.DataSource = controle.GetAll().ToList();
                cbSetor.ValueMember = "CODIGO";
                cbSetor.DisplayMember = "DESCRICAO";
                cbSetor.SelectedIndex = -1;
            }

            using (ControlePResponsaveis controle = new ControlePResponsaveis())
            {
                cbResponsavel.DataSource = controle.GetAll().Where(x => x.ATIVO).OrderBy(x => x.DESCRICAO).ToList();
                cbResponsavel.ValueMember = "CODIGO";
                cbResponsavel.DisplayMember = "DESCRICAO";
                cbResponsavel.SelectedIndex = -1;
            }

            using (ControlePInteresses controle = new ControlePInteresses())
            {
                cbInteresse.DataSource = controle.GetAll().ToList();
                cbInteresse.ValueMember = "CODIGO";
                cbInteresse.DisplayMember = "DESCRICAO";
                cbInteresse.SelectedIndex = -1;
            }

            using (ControlePCliente controle = new ControlePCliente())
            {
                cbCliente.DataSource = controle.Get(x => x.CODCOLIGADA ==
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA).ToList();
                cbCliente.ValueMember = "CODCLIENTE";
                cbCliente.DisplayMember = "NOMEFANTASIA";
                cbCliente.SelectedIndex = -1;
            }

            using (ControlePStatus controle = new ControlePStatus())
            {
                cbStatus.DataSource = controle.GetAll().ToList();
                cbStatus.ValueMember = "CODIGO";
                cbStatus.DisplayMember = "DESCRICAO";
                cbStatus.SelectedIndex = -1;
            }
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (cbCliente.SelectedIndex < 0)
                msg += " - Informe o cliente.\n";

            if (cbInteresse.SelectedIndex < 0)
                msg += " - Informe o nível de interesse da proposta.\n";

            if (cbSetor.SelectedIndex < 0)
                msg += " - Informe o setor referente a proposta.\n";

            if (cbStatus.SelectedIndex < 0)
                msg += " - Informe o status.\n";

            if (string.IsNullOrWhiteSpace(txtProcesso.Text))
                msg += " - Informe o campo de processo.\n";

            if (string.IsNullOrWhiteSpace(txtRevisao.Text))
                msg += " - Informe o campo de revisão.\n";

            if (!string.IsNullOrEmpty(Global.RemovePVTEsp(txtDtEnvio.Text)) && !Global.ValidaData(txtDtEnvio.Text))
                msg += " - Informe uma data de envio válida.\n";

            if (string.IsNullOrEmpty(Global.RemovePVTEsp(txtDtInicio.Text)) || !Global.ValidaData(txtDtInicio.Text))
                msg += " - Informe uma data de inicio válida.\n";

            if (cbResponsavel.SelectedIndex < 0 || !cbResponsavel.SelectedValue.DBNullToInt().HasValue)
                msg += " - Selecione o responsável.\n";

            if (cbStatus.Text?.ToLower() == "declinado" && !txtDtDeclinio.Text.DBNullToDateTime().HasValue)
                msg += " - Informe uma data de declínio válida.\n";

            if (cbStatus.Text?.ToLower() == "vencido" && !txtDtVencida.Text.DBNullToDateTime().HasValue)
                msg += " - Informe uma data de vencimento válida.\n";

            if (string.IsNullOrWhiteSpace(txtCodInterno.Text))
                msg += " - Informe o código interno.\n";

            if (string.IsNullOrWhiteSpace(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void CarregaDGVAnexos()
        {
            if (dgvAnexos.Columns != null)
                dgvAnexos.Columns.Clear();

            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "CODPROPOSTA", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "SEQUENCIAL", HeaderText = "Cód. SequÊncial", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Arquivo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "ARQUIVO", HeaderText = "Bytes", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDON", HeaderText = "Data Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Data Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            BindingSource bind = new BindingSource();
            bind.DataSource = typeof(PPROPANEXO);
            bind.DataSource = anexos;
            dgvAnexos.DataSource = bind;
            bind.EndEdit();

            if (dgvAnexos != null && dgvAnexos.View != null &&
                dgvAnexos.View.Records != null && ((dgvAnexos.View.Records.Count) > 0))
            {
                btnAdd.Enabled = true;
                btnRemover.Enabled = true;
                btnVer.Enabled = true;
                btnAtualizar.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = true;
                btnRemover.Enabled = false;
                btnVer.Enabled = false;
                btnAtualizar.Enabled = true;
            }

            if (anexos != null && anexos.Count > 0)
            {
                if (!tabControl.TabPages.Contains(tabAnexos))
                    tabControl.TabPages.Add(tabAnexos);
            }
            else
            {
                if (tabControl.TabPages.Contains(tabAnexos))
                    tabControl.TabPages.Remove(tabAnexos);
            }
        }

        private void btnAnexar_Click(object sender, EventArgs e)
        {
            string[] caminhoArquivo = Funcoes.SelecionaArquivoAllFormat();

            int numSeq = (anexos == null) ? 1 : anexos.Count + 1;

            if (caminhoArquivo != null)
            {
                foreach (string arquivo in caminhoArquivo)
                {
                    PPROPANEXO item = new PPROPANEXO
                    {
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        DESCRICAO = Path.GetFileNameWithoutExtension(arquivo),
                        EXTENSAO = Path.GetExtension(arquivo).ToLower(),
                        ARQUIVO = Global.EncriptPDF(arquivo),
                        SEQUENCIAL = numSeq,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    anexos.Add(item);
                    numSeq++;
                }
            }

            CarregaDGVAnexos();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string[] caminhoArquivo = Funcoes.SelecionaArquivoAllFormat();

            int numSeq = (anexos == null) ? 1 : anexos.Count + 1;

            if (caminhoArquivo != null)
            {
                foreach (string arquivo in caminhoArquivo)
                {
                    PPROPANEXO item = new PPROPANEXO
                    {
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        DESCRICAO = Path.GetFileNameWithoutExtension(arquivo),
                        EXTENSAO = Path.GetExtension(arquivo).ToLower(),
                        ARQUIVO = Global.EncriptPDF(arquivo),
                        SEQUENCIAL = numSeq,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    anexos.Add(item);
                    numSeq++;
                }
            }
            CarregaDGVAnexos();
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            if (dgvAnexos.CurrentItem is PPROPANEXO item)
            {
                anexos.Remove(item);
                btnAtualizar.PerformClick();
            }
            else
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            if (dgvAnexos.CurrentItem is PPROPANEXO item)
            {
                string arquivoTemp = string.Format
                (
                    @"{0}\{1}{2}", Global.PastaTemp(),
                    item.DESCRICAO, item.EXTENSAO
                );

                PPROPANEXO anexo = null;
                using (ControlePPropostasAnexo controleAnx = new ControlePPropostasAnexo())
                {
                    anexo = controleAnx.GetAnexo(item.CODCOLIGADA, item.CODPROPOSTA, item.SEQUENCIAL);
                }

                if (anexo != null && anexo.ARQUIVO != null)
                    Process.Start(Global.DescriptPDF(anexo.ARQUIVO, arquivoTemp));
            }
            else
                Global.MsgErro("Para excutar a função é necessário que selecione ao menos um registro.");
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGVAnexos();
        }

        private void dgvAnexos_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            btnVer.PerformClick();
        }

        private void txtVlrProposta_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }

        private void txtVlrProposta_TextChanged(object sender, EventArgs e)
        {
            Global.MoedaMask(ref txtVlrProposta);
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !ckSim.Checked;
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !ckNao.Checked;
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDtDeclinio.ReadOnly = !(cbStatus.SelectedIndex >= 0 && cbStatus.Text?.ToLower() == "declinado");
            txtDtVencida.ReadOnly = !(cbStatus.SelectedIndex >= 0 && cbStatus.Text?.ToLower() == "vencido");
        }

        private void ckTACSim_CheckedChanged(object sender, EventArgs e)
        {
            ckTACNao.Checked = !ckTACSim.Checked;
        }

        private void ckTACNao_CheckedChanged(object sender, EventArgs e)
        {
            ckTACSim.Checked = !ckTACNao.Checked;
        }

        private void ckCAPEXSim_CheckedChanged(object sender, EventArgs e)
        {
            ckCAPEXNao.Checked = !ckCAPEXSim.Checked;
        }

        private void ckCAPEXNao_CheckedChanged(object sender, EventArgs e)
        {
            ckCAPEXSim.Checked = !ckCAPEXNao.Checked;
        }
    }
}
