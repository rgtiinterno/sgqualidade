﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Propostas
{
    public partial class FormStatusProposta : Form
    {
        public bool salvo = false;
        private PPROPOSTAS item;

        public FormStatusProposta(PPROPOSTAS proposta)
        {
            InitializeComponent();
            item = proposta;
        }

        private void FormStatusProposta_Load(object sender, EventArgs e)
        {
            using (ControlePStatus controle = new ControlePStatus())
            {
                cbStatus.DataSource = controle.GetAll().ToList();
                cbStatus.ValueMember = "CODIGO";
                cbStatus.DisplayMember = "DESCRICAO";
                cbStatus.SelectedIndex = -1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (cbStatus.SelectedIndex >= 0 && Convert.ToInt16(cbStatus.SelectedValue) != item.CODSTATUS)
            {
                Funcoes.ExecutaFuncao($@"UPDATE PPROPOSTAS SET 
                CODSTATUS = '{Convert.ToInt16(cbStatus.SelectedValue)}' 
                WHERE CODIGO = '{item.CODIGO}' AND CODCOLIGADA = '{item.CODCOLIGADA}'");
                using (ControlePPropostasHst controle = new ControlePPropostasHst())
                {
                    PPROPOSTASHST proposta = new PPROPOSTASHST
                    {
                        CODIGO = controle.GetNewID(FormPrincipal.getUsuarioAcesso().CODCOLIGADA),
                        CODPROPOSTA = item.CODIGO,
                        CODCOLIGADA = item.CODCOLIGADA,
                        NOVAREVISAO = item.REVISAO,
                        DATAENVIO = item.DATAENVIO,
                        VLRPROPOSTA = item.VLRPROPOSTA,
                        OBSERVACOES = $"Status da proposta foi alterado para {cbStatus.Text}",
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    controle.Create(proposta);
                    controle.SaveAll();
                }

                salvo = true;
                this.Close();
            }
            else
                Global.MsgErro(" - Informe um novo Status");
        }
    }
}
