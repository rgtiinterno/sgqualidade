﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Propostas
{
    public partial class FormHistoricos : Form
    {
        private PPROPOSTAS item;

        public FormHistoricos(PPROPOSTAS proposta)
        {
            InitializeComponent();
            item = proposta;
        }


        private void CarregaDGV()
        {
            using (ControlePPropostasHst controle = new ControlePPropostasHst())
            {
                Cursor.Current = Cursors.WaitCursor;
                pPROPOSTASHSTBindingSource = new BindingSource();
                pPROPOSTASHSTBindingSource.DataSource = typeof(PPROPOSTASHST);
                pPROPOSTASHSTBindingSource.DataSource = controle.GetList(item);
                dgvListagem.DataSource = pPROPOSTASHSTBindingSource;
                pPROPOSTASHSTBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {pPROPOSTASHSTBindingSource.Count}";
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void FormHistoricos_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODPROPOSTA", HeaderText = "Cód. Proposta", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Cód. Interno", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NOVAREVISAO", HeaderText = "Revisão", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAENVIO", HeaderText = "Data Envio", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridNumericColumn() { MappingName = "VLRPROPOSTA", HeaderText = "Vlr. da Proposta", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "OBSERVACOES", HeaderText = "Observações", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }
    }
}
