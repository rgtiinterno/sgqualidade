﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Analise
{
    public partial class FormPerguntas : Form
    {
        public bool salvo = false;
        private PPERGUNTAS itemEdit;

        public FormPerguntas(PPERGUNTAS pergunta = null)
        {
            InitializeComponent();
            itemEdit = pergunta;

            if (itemEdit != null)
                txtPergunta.Text = itemEdit.PERGUNTA;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPergunta.Text))
            {
                using (ControlePPerguntas controle = new ControlePPerguntas())
                {
                    PPERGUNTAS item = new PPERGUNTAS
                    {
                        CODIGO = controle.GetNewID(),
                        PERGUNTA = txtPergunta.Text,
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    if (itemEdit != null)
                    {
                        item.CODIGO = itemEdit.CODIGO;
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
            {
                FormMsg frm = new FormMsg("Informe a pergunta");
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();
            }
        }
    }
}
