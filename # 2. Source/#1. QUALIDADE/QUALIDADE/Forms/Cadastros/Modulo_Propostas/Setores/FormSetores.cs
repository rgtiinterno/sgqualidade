﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCP.Setores
{
    public partial class FormSetores : Form
    {
        public bool salvo = false;
        private PSETORES itemEdit;

        public FormSetores(PSETORES setores = null)
        {
            InitializeComponent();
            itemEdit = setores;
        }

        private void FormSetores_Load(object sender, EventArgs e)
        {
            if (itemEdit != null)
            {
                txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
                txtCodigo.ReadOnly = true;
                txtDescricao.Text = itemEdit.DESCRICAO;
            }
            else
            {
                using (ControlePSetores controle = new ControlePSetores())
                {
                    txtCodigo.Text = Convert.ToString(controle.GetNewID());
                    txtCodigo.ReadOnly = true;
                }
            }
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtDescricao.Text))
            {
                PSETORES item = new PSETORES
                {
                    CODIGO = Convert.ToInt16(txtCodigo.Text),
                    DESCRICAO = txtDescricao.Text
                };

                using (ControlePSetores controle = new ControlePSetores())
                {
                    if (itemEdit != null)
                    {
                        controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        controle.Create(item);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
            else
                Global.MsgErro(" - Informe a descrição");
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}