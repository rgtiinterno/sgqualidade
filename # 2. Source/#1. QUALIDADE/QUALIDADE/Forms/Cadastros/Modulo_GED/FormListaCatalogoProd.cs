﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_GED
{
    public partial class FormListaCatalogoProd : Form
    {
        public FormListaCatalogoProd()
        {
            InitializeComponent();
            Global.ApenasAdmin(tsbAdicionar, tsbEditar, tsbExcluir, tsbAdicionarAnexo, tsbExcluirAnexos);
        }

        private void FormListaCatalogoProd_Load(object sender, EventArgs e)
        {
            CarregaDados();
            if (!FormPrincipal.getUsuarioAcesso().GERENTE)
            {
                tsbAdicionar.Enabled = false;
                tsbEditar.Enabled = false;
                tsbExcluir.Enabled = false;
                tsbAdicionarAnexo.Enabled = false;
                tsbExcluirAnexos.Enabled = false;
            }
        }

        private void CarregaDados()
        {
            treeViewCatalogo.Nodes.Clear();
            using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
            {
                Cursor.Current = Cursors.WaitCursor;
                List<CatalogoDocumentos> itens = controller.GetallList();
                if (itens != null && itens.Count > 0)
                {
                    foreach (CatalogoDocumentos item in itens.Where(x => string.IsNullOrEmpty(x.CODITEMPAI)))
                    {
                        treeViewCatalogo.Nodes.Add(GetNode(item));
                    }
                }
                //if (treeViewCatalogo.Nodes != null && treeViewCatalogo.Nodes.Count > 0)
                //{
                //    treeViewCatalogo.SortWithChildNodes = false;
                //    treeViewCatalogo.Nodes.Sort(new CatalogoNodeSorter());
                //}
                Cursor.Current = Cursors.Default;
            }
        }

        private TreeNodeAdv GetNode(CatalogoDocumentos item)
        {
            TreeNodeAdv node = new TreeNodeAdv
            {
                Text = item.DESCRICAO,
                Tag = item
            };

            if (item.ITENSFILHOS != null && item.ITENSFILHOS.Count > 0)
            {
                foreach (CatalogoDocumentos subItem in item.ITENSFILHOS)
                {
                    node.Nodes.Add(GetNode(subItem));
                }
            }

            return node;
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            string codItemPai = null;
            if (treeViewCatalogo.SelectedNode != null && treeViewCatalogo.SelectedNode.Tag is CatalogoDocumentos item)
                codItemPai = item.CODIGO;

            using (FormCatalogo frm = new FormCatalogo(itemPaiSelecionado: codItemPai))
            {
                frm.ShowDialog();
                if (frm.salvo)
                    btnAtualiza.PerformClick();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (treeViewCatalogo.SelectedNode != null && treeViewCatalogo.SelectedNode.Tag is CatalogoDocumentos item)
            {
                using (FormCatalogo frm = new FormCatalogo(itemAtual: item))
                {
                    frm.ShowDialog();
                    if (frm.salvo)
                        btnAtualiza.PerformClick();
                }
            }
        }

        private void tsbExcluir_Click(object sender, EventArgs e)
        {
            if (treeViewCatalogo.SelectedNode != null && treeViewCatalogo.SelectedNode.Tag is CatalogoDocumentos item)
            {
                DialogResult dr = Global.MsgConfirmacao("Confirmação", "Deseja realmente excluir o item selecionado?");

                if (dr == DialogResult.Yes)
                {
                    using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
                    {
                        if (controller.Delete(item.CODIGO))
                        {
                            treeViewCatalogo.SelectedNode.Remove();
                            //btnAtualiza.PerformClick();
                        }
                    }
                }
            }
        }

        private void FormListaCatalogoProd_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Dispose();
                    break;

                case Keys.F5:
                    btnAtualiza.PerformClick();
                    break;
            }
        }

        private void btnAtualiza_Click(object sender, EventArgs e)
        {
            CarregaDados();
        }

        private void tsbExpandirTodos_Click(object sender, EventArgs e)
        {
            treeViewCatalogo.ExpandAll();
        }

        private void tsbRecolherTodos_Click(object sender, EventArgs e)
        {
            treeViewCatalogo.CollapseAll();
        }

        private void treeViewCatalogo_NodeMouseDoubleClick(object sender, TreeViewAdvMouseClickEventArgs e)
        {
            tsbEditar.PerformClick();
        }

        #region "Anexos"
        private void treeViewCatalogo_AfterSelect(object sender, EventArgs e)
        {
            if (!splitContainer1.Panel2Collapsed)
            {
                if (treeViewCatalogo.SelectedNode != null && treeViewCatalogo.SelectedNode.Tag is CatalogoDocumentos item)
                {
                    CarregaAnexos(item.CODIGO);
                }
            }
        }

        private void CarregaAnexos(string codCatalogoProduto)
        {
            if (dgvAnexos.Columns != null)
                dgvAnexos.Columns.Clear();

            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "NOMEANEXO", HeaderText = "NOME", AllowEditing = false, AllowFiltering = true });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "EXTENSAO", AllowEditing = false, AllowFiltering = true });
            dgvAnexos.Columns.Add(new GridTextColumn() { MappingName = "CODIGOSEQ", HeaderText = "CÓD. SEQUENCIAL", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvAnexos.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAEMISSAO", HeaderText = "DATA EMISSÃO", AllowEditing = false, AllowFiltering = true, Format = "dd/MM/yyyy" });
            dgvAnexos.Columns.Add(new GridDateTimeColumn() { MappingName = "DATAREVISAO", HeaderText = "DATA REVISÃO", AllowEditing = false, AllowFiltering = true, Format = "dd/MM/yyyy" });

            using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
            {
                BindingSource anexoBindingSource = new BindingSource();
                anexoBindingSource.DataSource = typeof(CatalogoDocumentosAnexo);
                anexoBindingSource.DataSource = controller.RetornaListaAnexos(codCatalogoProduto);
                dgvAnexos.DataSource = null;
                dgvAnexos.DataSource = anexoBindingSource;
                anexoBindingSource.EndEdit();
            }
        }

        private void tsbAdicionarAnexo_Click(object sender, EventArgs e)
        {
            if (treeViewCatalogo.SelectedNode != null && treeViewCatalogo.SelectedNode.Tag is CatalogoDocumentos item)
            {
                using (FormAddAnexoCatalogo frmAnexo = new FormAddAnexoCatalogo())
                {
                    frmAnexo.ShowDialog();
                    if (frmAnexo.salvo)
                    {
                        using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
                        {
                            int codSeq = (dgvAnexos == null || dgvAnexos.RowCount <= 0) ? 1 : controller.GeraNovoCodigoAnexo(item.CODIGO);
                            foreach (string caminhoArquivo in frmAnexo.arquivos)
                            {
                                if (!string.IsNullOrEmpty(caminhoArquivo))
                                {
                                    CatalogoDocumentosAnexo anexo = new CatalogoDocumentosAnexo
                                    {
                                        CKSELECIONADO = false,
                                        CODCATALOGO = item.CODIGO,
                                        CODIGOSEQ = codSeq,
                                        CAMINHO = caminhoArquivo,
                                        NOMEANEXO = Path.GetFileNameWithoutExtension(caminhoArquivo),
                                        ARQUIVOANEXO = Global.EncriptPDF(caminhoArquivo),
                                        EXTENSAO = Path.GetExtension(caminhoArquivo).ToLower(),
                                        DATAEMISSAO = frmAnexo.dtEmissao,
                                        DATAREVISAO = frmAnexo.dtRevisao
                                    };
                                    controller.IncluiAnexo(anexo);
                                }
                                codSeq++;
                            }
                        }
                        tsbAtualizarAnexos.PerformClick();
                    }
                }
            }
            else
            {
                Global.MsgInformacao("Selecione o Item do Catálogo que deseja adicionar um anexo.");
            }
        }

        private void tsbExcluirAnexos_Click(object sender, EventArgs e)
        {
            if (dgvAnexos.CurrentItem is CatalogoDocumentosAnexo anexo)
            {
                DialogResult dr = Global.MsgConfirmacao("Confirmação", "Deseja realmente excluir o anexo selecionado?");

                if (dr == DialogResult.Yes)
                {
                    using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
                    {
                        if (controller.ExcluiAnexo(anexo))
                            Global.MsgInformacao("Anexo excluído com sucesso!");
                    }
                    tsbAtualizarAnexos.PerformClick();
                }
            }
            else
            {
                Global.MsgInformacao("Selecione um anexo para realizar a exclusão.");
            }
        }

        private void tsbVerAnexo_Click(object sender, EventArgs e)
        {
            if (dgvAnexos.CurrentItem is CatalogoDocumentosAnexo anexo)
            {
                string arquivoTemp = $@"{Global.PastaTemp()}\{anexo.NOMEANEXO}{anexo.EXTENSAO}";
                Process.Start(Global.DescriptPDF(anexo.ARQUIVOANEXO, arquivoTemp));
            }
        }

        private void tsbAtualizarAnexos_Click(object sender, EventArgs e)
        {
            treeViewCatalogo_AfterSelect(sender, null);
        }

        private void dgvAnexos_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbVerAnexo.PerformClick();
        }
        #endregion
    }
}