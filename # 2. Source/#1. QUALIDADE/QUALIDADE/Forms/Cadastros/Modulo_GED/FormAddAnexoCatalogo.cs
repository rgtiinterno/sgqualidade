﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE
{
    public partial class FormAddAnexoCatalogo : Form
    {
        public bool salvo = false;
        public string[] arquivos = null;
        public DateTime? dtEmissao = null;
        public DateTime? dtRevisao = null;

        public FormAddAnexoCatalogo()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if(ValidaCampos())
            {
                this.arquivos = txtAnexos.Lines;
                this.dtEmissao = DateTime.TryParse(txtDataEmissao.Text, out DateTime dtEmissao) ? dtEmissao : (DateTime?)null;
                this.dtRevisao = DateTime.TryParse(txtDataRevisao.Text, out DateTime dtRevisao) ? dtEmissao : (DateTime?)null;
                this.salvo = true;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.salvo = false;
            this.Dispose();
        }


        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtAnexos.Text) || txtAnexos.Lines == null || txtAnexos.Lines.Length <= 0)
                msg += " - Selecione ao menos um arquivo para anexar.\n";

            if (!string.IsNullOrEmpty(txtDataEmissao.Text.Replace("/","").Trim()) && !DateTime.TryParse(txtDataEmissao.Text, out DateTime dtEmissao))
                msg += " - Data de Emissão em formato inválido.\n";

            if (!string.IsNullOrEmpty(txtDataRevisao.Text.Replace("/", "").Trim()) && !DateTime.TryParse(txtDataRevisao.Text, out DateTime dtRevisao))
                msg += " - Data de Emissão em formato inválido.\n";

            if (string.IsNullOrEmpty(msg))
            {
                return true;
            }
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void txtAnexos_DoubleClick(object sender, EventArgs e)
        {
            btnSelecionar.PerformClick();
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            txtAnexos.Lines = Funcoes.SelecionaArquivoAllFormat();
        }
    }
}