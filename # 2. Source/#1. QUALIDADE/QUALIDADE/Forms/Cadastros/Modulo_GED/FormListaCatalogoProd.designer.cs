﻿namespace QUALIDADE.Forms.Cadastros.Modulo_GED
{
    partial class FormListaCatalogoProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaCatalogoProd));
            Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo treeNodeAdvStyleInfo1 = new Syncfusion.Windows.Forms.Tools.TreeNodeAdvStyleInfo();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionar = new System.Windows.Forms.ToolStripButton();
            this.tsbEditar = new System.Windows.Forms.ToolStripButton();
            this.tsbExcluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbExpandirTodos = new System.Windows.Forms.ToolStripButton();
            this.tsbRecolherTodos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAtualiza = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeViewCatalogo = new Syncfusion.Windows.Forms.Tools.TreeViewAdv();
            this.tbcAnexos = new System.Windows.Forms.TabControl();
            this.tbpAnexos = new System.Windows.Forms.TabPage();
            this.dgvAnexos = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.tsbAdicionarAnexo = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizarAnexos = new System.Windows.Forms.ToolStripButton();
            this.tsbExcluirAnexos = new System.Windows.Forms.ToolStripButton();
            this.tsbVerAnexo = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewCatalogo)).BeginInit();
            this.tbcAnexos.SuspendLayout();
            this.tbpAnexos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexos)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionar,
            this.tsbEditar,
            this.tsbExcluir,
            this.toolStripSeparator1,
            this.tsbExpandirTodos,
            this.tsbRecolherTodos,
            this.toolStripSeparator2,
            this.btnAtualiza});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 39);
            this.toolStrip1.TabIndex = 20;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbAdicionar
            // 
            this.tsbAdicionar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionar.Image")));
            this.tsbAdicionar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAdicionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionar.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbAdicionar.Name = "tsbAdicionar";
            this.tsbAdicionar.Size = new System.Drawing.Size(86, 36);
            this.tsbAdicionar.Text = "Adicionar";
            this.tsbAdicionar.ToolTipText = "Adicionar item";
            this.tsbAdicionar.Click += new System.EventHandler(this.tsbAdicionar_Click);
            // 
            // tsbEditar
            // 
            this.tsbEditar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbEditar.Image = ((System.Drawing.Image)(resources.GetObject("tsbEditar.Image")));
            this.tsbEditar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEditar.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbEditar.Name = "tsbEditar";
            this.tsbEditar.Size = new System.Drawing.Size(65, 36);
            this.tsbEditar.Text = "Editar";
            this.tsbEditar.ToolTipText = "Editar item";
            this.tsbEditar.Click += new System.EventHandler(this.tsbEditar_Click);
            // 
            // tsbExcluir
            // 
            this.tsbExcluir.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbExcluir.Image = ((System.Drawing.Image)(resources.GetObject("tsbExcluir.Image")));
            this.tsbExcluir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExcluir.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbExcluir.Name = "tsbExcluir";
            this.tsbExcluir.Size = new System.Drawing.Size(70, 36);
            this.tsbExcluir.Text = "Excluir";
            this.tsbExcluir.ToolTipText = "Excluir item";
            this.tsbExcluir.Click += new System.EventHandler(this.tsbExcluir_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // tsbExpandirTodos
            // 
            this.tsbExpandirTodos.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbExpandirTodos.Image = ((System.Drawing.Image)(resources.GetObject("tsbExpandirTodos.Image")));
            this.tsbExpandirTodos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExpandirTodos.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbExpandirTodos.Name = "tsbExpandirTodos";
            this.tsbExpandirTodos.Size = new System.Drawing.Size(106, 36);
            this.tsbExpandirTodos.Text = "Expandir todos";
            this.tsbExpandirTodos.ToolTipText = "Expandir todos os itens";
            this.tsbExpandirTodos.Click += new System.EventHandler(this.tsbExpandirTodos_Click);
            // 
            // tsbRecolherTodos
            // 
            this.tsbRecolherTodos.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbRecolherTodos.Image = ((System.Drawing.Image)(resources.GetObject("tsbRecolherTodos.Image")));
            this.tsbRecolherTodos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRecolherTodos.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbRecolherTodos.Name = "tsbRecolherTodos";
            this.tsbRecolherTodos.Size = new System.Drawing.Size(106, 36);
            this.tsbRecolherTodos.Text = "Recolher todos";
            this.tsbRecolherTodos.ToolTipText = "Recolher todos os itens";
            this.tsbRecolherTodos.Click += new System.EventHandler(this.tsbRecolherTodos_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // btnAtualiza
            // 
            this.btnAtualiza.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnAtualiza.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualiza.Image")));
            this.btnAtualiza.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnAtualiza.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtualiza.Name = "btnAtualiza";
            this.btnAtualiza.Size = new System.Drawing.Size(89, 36);
            this.btnAtualiza.Text = "Atualizar";
            this.btnAtualiza.ToolTipText = "Atualizar lista (F5)";
            this.btnAtualiza.Click += new System.EventHandler(this.btnAtualiza_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 39);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeViewCatalogo);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tbcAnexos);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 523);
            this.splitContainer1.SplitterDistance = 581;
            this.splitContainer1.TabIndex = 22;
            // 
            // treeViewCatalogo
            // 
            treeNodeAdvStyleInfo1.CheckBoxTickThickness = 1;
            treeNodeAdvStyleInfo1.CheckColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.EnsureDefaultOptionedChild = true;
            treeNodeAdvStyleInfo1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            treeNodeAdvStyleInfo1.IntermediateCheckColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.OptionButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            treeNodeAdvStyleInfo1.SelectedOptionButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(210)))), ((int)(((byte)(210)))));
            treeNodeAdvStyleInfo1.ShowOptionButton = false;
            this.treeViewCatalogo.BaseStylePairs.AddRange(new Syncfusion.Windows.Forms.Tools.StyleNamePair[] {
            new Syncfusion.Windows.Forms.Tools.StyleNamePair("Standard", treeNodeAdvStyleInfo1)});
            this.treeViewCatalogo.BeforeTouchSize = new System.Drawing.Size(581, 523);
            this.treeViewCatalogo.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.treeViewCatalogo.HelpTextControl.BaseThemeName = null;
            this.treeViewCatalogo.HelpTextControl.Location = new System.Drawing.Point(0, 0);
            this.treeViewCatalogo.HelpTextControl.Name = "";
            this.treeViewCatalogo.HelpTextControl.Size = new System.Drawing.Size(392, 112);
            this.treeViewCatalogo.HelpTextControl.TabIndex = 0;
            this.treeViewCatalogo.HelpTextControl.Visible = true;
            this.treeViewCatalogo.InactiveSelectedNodeForeColor = System.Drawing.SystemColors.ControlText;
            this.treeViewCatalogo.Location = new System.Drawing.Point(0, 0);
            this.treeViewCatalogo.MetroColor = System.Drawing.Color.FromArgb(((int)(((byte)(22)))), ((int)(((byte)(165)))), ((int)(((byte)(220)))));
            this.treeViewCatalogo.Name = "treeViewCatalogo";
            this.treeViewCatalogo.SelectedNodeForeColor = System.Drawing.SystemColors.HighlightText;
            this.treeViewCatalogo.Size = new System.Drawing.Size(581, 523);
            this.treeViewCatalogo.TabIndex = 0;
            this.treeViewCatalogo.Text = "treeViewAdv1";
            this.treeViewCatalogo.ThemeStyle.TreeNodeAdvStyle.CheckBoxTickThickness = 0;
            this.treeViewCatalogo.ThemeStyle.TreeNodeAdvStyle.EnsureDefaultOptionedChild = true;
            // 
            // 
            // 
            this.treeViewCatalogo.ToolTipControl.BaseThemeName = null;
            this.treeViewCatalogo.ToolTipControl.Location = new System.Drawing.Point(0, 0);
            this.treeViewCatalogo.ToolTipControl.Name = "";
            this.treeViewCatalogo.ToolTipControl.Size = new System.Drawing.Size(392, 112);
            this.treeViewCatalogo.ToolTipControl.TabIndex = 0;
            this.treeViewCatalogo.ToolTipControl.Visible = true;
            this.treeViewCatalogo.AfterSelect += new System.EventHandler(this.treeViewCatalogo_AfterSelect);
            this.treeViewCatalogo.NodeMouseDoubleClick += new Syncfusion.Windows.Forms.Tools.TreeNodeAdvMouseClickArgs(this.treeViewCatalogo_NodeMouseDoubleClick);
            // 
            // tbcAnexos
            // 
            this.tbcAnexos.Controls.Add(this.tbpAnexos);
            this.tbcAnexos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcAnexos.Location = new System.Drawing.Point(0, 0);
            this.tbcAnexos.Name = "tbcAnexos";
            this.tbcAnexos.SelectedIndex = 0;
            this.tbcAnexos.Size = new System.Drawing.Size(423, 523);
            this.tbcAnexos.TabIndex = 0;
            // 
            // tbpAnexos
            // 
            this.tbpAnexos.Controls.Add(this.dgvAnexos);
            this.tbpAnexos.Controls.Add(this.toolStrip3);
            this.tbpAnexos.Location = new System.Drawing.Point(4, 22);
            this.tbpAnexos.Name = "tbpAnexos";
            this.tbpAnexos.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAnexos.Size = new System.Drawing.Size(415, 497);
            this.tbpAnexos.TabIndex = 2;
            this.tbpAnexos.Text = "Anexos";
            this.tbpAnexos.UseVisualStyleBackColor = true;
            // 
            // dgvAnexos
            // 
            this.dgvAnexos.AccessibleName = "Table";
            this.dgvAnexos.AllowEditing = false;
            this.dgvAnexos.AllowFiltering = true;
            this.dgvAnexos.AutoGenerateColumns = false;
            this.dgvAnexos.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvAnexos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAnexos.Location = new System.Drawing.Point(3, 28);
            this.dgvAnexos.Name = "dgvAnexos";
            this.dgvAnexos.Size = new System.Drawing.Size(409, 466);
            this.dgvAnexos.TabIndex = 5;
            this.dgvAnexos.Text = "sfDataGrid1";
            this.dgvAnexos.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.dgvAnexos_CellDoubleClick);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdicionarAnexo,
            this.tsbAtualizarAnexos,
            this.tsbExcluirAnexos,
            this.tsbVerAnexo});
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(409, 25);
            this.toolStrip3.TabIndex = 4;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // tsbAdicionarAnexo
            // 
            this.tsbAdicionarAnexo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAdicionarAnexo.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdicionarAnexo.Image")));
            this.tsbAdicionarAnexo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAdicionarAnexo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAdicionarAnexo.Name = "tsbAdicionarAnexo";
            this.tsbAdicionarAnexo.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAdicionarAnexo.Size = new System.Drawing.Size(40, 22);
            this.tsbAdicionarAnexo.Text = "Novo Anexo";
            this.tsbAdicionarAnexo.Click += new System.EventHandler(this.tsbAdicionarAnexo_Click);
            // 
            // tsbAtualizarAnexos
            // 
            this.tsbAtualizarAnexos.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbAtualizarAnexos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAtualizarAnexos.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarAnexos.Image")));
            this.tsbAtualizarAnexos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAtualizarAnexos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarAnexos.Name = "tsbAtualizarAnexos";
            this.tsbAtualizarAnexos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarAnexos.Size = new System.Drawing.Size(40, 22);
            this.tsbAtualizarAnexos.Text = "Atualizar";
            this.tsbAtualizarAnexos.Click += new System.EventHandler(this.tsbAtualizarAnexos_Click);
            // 
            // tsbExcluirAnexos
            // 
            this.tsbExcluirAnexos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbExcluirAnexos.Image = ((System.Drawing.Image)(resources.GetObject("tsbExcluirAnexos.Image")));
            this.tsbExcluirAnexos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExcluirAnexos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExcluirAnexos.Name = "tsbExcluirAnexos";
            this.tsbExcluirAnexos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbExcluirAnexos.Size = new System.Drawing.Size(40, 22);
            this.tsbExcluirAnexos.Text = "Excluir Anexo";
            this.tsbExcluirAnexos.Click += new System.EventHandler(this.tsbExcluirAnexos_Click);
            // 
            // tsbVerAnexo
            // 
            this.tsbVerAnexo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbVerAnexo.Image = ((System.Drawing.Image)(resources.GetObject("tsbVerAnexo.Image")));
            this.tsbVerAnexo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbVerAnexo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbVerAnexo.Name = "tsbVerAnexo";
            this.tsbVerAnexo.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbVerAnexo.Size = new System.Drawing.Size(40, 22);
            this.tsbVerAnexo.Text = "Ver Anexo";
            this.tsbVerAnexo.Click += new System.EventHandler(this.tsbVerAnexo_Click);
            // 
            // FormListaCatalogoProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 562);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FormListaCatalogoProd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Catálogo de Processos";
            this.Load += new System.EventHandler(this.FormListaCatalogoProd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormListaCatalogoProd_KeyDown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeViewCatalogo)).EndInit();
            this.tbcAnexos.ResumeLayout(false);
            this.tbpAnexos.ResumeLayout(false);
            this.tbpAnexos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnexos)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbExcluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripButton btnAtualiza;
        private System.Windows.Forms.ToolStripButton tsbAdicionar;
        private System.Windows.Forms.ToolStripButton tsbEditar;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tbcAnexos;
        private System.Windows.Forms.TabPage tbpAnexos;
        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvAnexos;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton tsbAdicionarAnexo;
        private System.Windows.Forms.ToolStripButton tsbAtualizarAnexos;
        private System.Windows.Forms.ToolStripButton tsbExcluirAnexos;
        private System.Windows.Forms.ToolStripButton tsbVerAnexo;
        private Syncfusion.Windows.Forms.Tools.TreeViewAdv treeViewCatalogo;
        private System.Windows.Forms.ToolStripButton tsbExpandirTodos;
        private System.Windows.Forms.ToolStripButton tsbRecolherTodos;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}