﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.Data.Extensions;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.Modulo_GED
{
    public partial class FormCatalogo : Form
    {
        private string itemPaiSelecionado = null;
        private CatalogoDocumentos itemAtual;
        private string codigo = null;
        public bool salvo = false;

        public FormCatalogo(string itemPaiSelecionado = null, CatalogoDocumentos itemAtual = null)
        {
            InitializeComponent();
            this.itemAtual = itemAtual;
            this.itemPaiSelecionado = itemPaiSelecionado;
            CarregaCatalogos();
            if (itemAtual != null)
            {
                PreencheForm();
            }
            else
            {
                using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
                {
                    codigo = $"{controller.GeraNovoCodigo()}";
                }
            }
        }

        private void PreencheForm()
        {
            this.Text = $"Catálogo: <{itemAtual.CODIGO} - {itemAtual.DESCRICAO}>";
            codigo = $"{itemAtual.CODIGO}";
            txtDescricao.Text = $"{itemAtual.DESCRICAO}";
            txtOrdem.Value = itemAtual.ORDEM;
        }

        private void CarregaCatalogos()
        {
            using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
            {
                if (itemAtual == null)
                    cbCatalogoPai.DataSource = controller.GetallList();
                else
                    cbCatalogoPai.DataSource = controller.GetallList().Where(x => x.CODIGO != itemAtual.CODIGO).ToList();
                cbCatalogoPai.DisplayMember = "DESCRICAO";
                cbCatalogoPai.ValueMember = "CODIGO";
                if (itemAtual != null && !string.IsNullOrEmpty(itemAtual.CODITEMPAI))
                    cbCatalogoPai.SelectedValue = itemAtual.CODITEMPAI;
                else if (!string.IsNullOrEmpty(itemPaiSelecionado))
                    cbCatalogoPai.SelectedValue = itemPaiSelecionado;
                else
                    cbCatalogoPai.SelectedIndex = -1;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CatalogoDocumentos item = new CatalogoDocumentos
            {
                CODIGO = codigo, 
                DESCRICAO = txtDescricao.Text.Trim(),
                CODITEMPAI = cbCatalogoPai.SelectedIndex >= 0 ? Convert.ToString(cbCatalogoPai.SelectedValue) : null, 
                ORDEM = (int)txtOrdem.Value, 
                CRIADOPOR = $"{FormPrincipal.getUsuarioAcesso().CODUSUARIO}",
                CRIADOEM = DateTime.Now,
                MODIFICADOPOR = $"{FormPrincipal.getUsuarioAcesso().CODUSUARIO}",
                MODIFICADOEM = DateTime.Now
            };

            if (Validacao.ValidaForm(item))
            {
                using (ControleCatalogoDocumento controller = new ControleCatalogoDocumento())
                {
                    if (itemAtual == null)
                        salvo = controller.Insert(item);
                    else
                        salvo = controller.Update(item);
                }

                Global.MsgInformacao("Registro salvo com sucesso.");
                salvo = true;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }
    }
}