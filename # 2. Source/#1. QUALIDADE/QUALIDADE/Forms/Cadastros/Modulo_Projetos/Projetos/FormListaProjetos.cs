﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos
{
    public partial class FormListaProjetos : Form
    {
        public FormListaProjetos()
        {
            InitializeComponent();
            CarregaCentrosCusto();
        }

        private void FormListaProjetos_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns?.Clear();
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "REVISAO", HeaderText = "Rev.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMPROJETO", HeaderText = "Número Projeto", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. Centro Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CENTROCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVEL", HeaderText = "Responsável", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FUNCAO", HeaderText = "Função", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPO", HeaderText = "Tipo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOCAL", HeaderText = "Local", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ANALISERISCO", HeaderText = "Análise de Risco", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTANALISECRITICA", HeaderText = "Dt.Análise Crítica", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "POSSUIANALISECRITICA", HeaderText = "Possui Análise Crítica?", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "DTENTREGACLIENTE", HeaderText = "Dt.Rec.Projeto pelo Cliente", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DISTRIBUICAO", HeaderText = "Distribuição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });
            CarregaDGV();
        }

        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"Projetos.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }

        private void CarregaDGV(string codCCusto = null)
        {
            using (ControleJProjetos controle = new ControleJProjetos())
            {
                Cursor.Current = Cursors.WaitCursor;
                jPROJETOSBindingSource = new BindingSource();
                jPROJETOSBindingSource.DataSource = typeof(JPROJETOS);
                List<CCustoFiltro> ccustosPermitidos = FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("J");
                List<string> codccustos = new List<string>();
                foreach (var cc in ccustosPermitidos)
                {
                    codccustos.Add(cc.CodCCusto);
                }

                if (codCCusto != null)
                {
                    if (codCCusto.Length == 1)
                    {
                        jPROJETOSBindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, null).Where(x => codccustos.Contains(x.CODCCUSTO)).Where(x => x.CODCCUSTO.Substring(0,1).Trim() == codCCusto).ToList();
                    }
                    else
                    {
                        jPROJETOSBindingSource.DataSource = controle.GetList(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, codCCusto).ToList();
                    }
                }
                else
                    jPROJETOSBindingSource.DataSource = controle.GetListPermissao(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("J")).ToList();

                dgvListagem.DataSource = jPROJETOSBindingSource;
                jPROJETOSBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {jPROJETOSBindingSource.Count}";

                if (dgvListagem?.View != null && dgvListagem.View.Records != null && dgvListagem.View.Records.Count > 0)
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbListaAnexos.Enabled = true;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbListaAnexos.Enabled = false;
                    tsbAtualizar.Enabled = true;
                    tsbHistorico.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void CarregaCentrosCusto()
        {
            var centrosCusto = FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("J");
            cbCentroCusto.ComboBox.DataSource = centrosCusto;
            cbCentroCusto.ComboBox.DisplayMember = "DescCombo";
            cbCentroCusto.ComboBox.ValueMember = "CodCCusto";
            //cbCentroCusto.ComboBox.SelectedIndex = 0;
            //cbCentroCusto.ComboBox.Text = "";
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            if(cbCentroCusto.SelectedIndex > -1 && cbCentroCusto.SelectedItem is CCustoFiltro item)
                CarregaDGV(item.CodCCusto);
            else
                CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormProjetos frm = new FormProjetos())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos && projetos.CKSELECIONADO)
            {
                using (FormProjetos frm = new FormProjetos(projetos))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione um registro para efetuar a edição do mesmo.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos && projetos.CKSELECIONADO)
            {
                using (ControleJSetoresDistribuidos controle = new ControleJSetoresDistribuidos())
                {
                    controle.Delete(x => x.CODPROJETO == projetos.CODIGO);
                    controle.SaveAll();
                }

                using (ControleJAnalise controle = new ControleJAnalise())
                {
                    controle.Delete(x => x.CODPROJETO == projetos.CODIGO);
                    controle.SaveAll();
                }

                using (ControleJProjetos controle = new ControleJProjetos())
                {
                    controle.Delete(x => x.CODIGO == projetos.CODIGO &&
                        x.CODCOLIGADA == projetos.CODCOLIGADA && x.CODCCUSTO == projetos.CODCCUSTO);
                    controle.SaveAll();
                }
                tsbAtualizar.PerformClick();
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbAprovar_Click(object sender, EventArgs e)
        {

        }

        private void tsbDesaprovar_Click(object sender, EventArgs e)
        {

        }

        private void tsbHistorico_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos && projetos.CKSELECIONADO)
            {
                FormHistoricoProjetos frm = new FormHistoricoProjetos(projetos);
                frm.MdiParent = this.MdiParent;
                frm.Show();
            }
            else
                Global.MsgErro(" - Selecione um registro para visualizar o histórico do mesmo.");
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos)
            {
                FormHistoricoProjetos frm = new FormHistoricoProjetos(projetos);
                frm.MdiParent = this.MdiParent;
                frm.Show();
            }
            else
                Global.MsgErro(" - Selecione um registro para visualizar o histórico do mesmo.");
        }

        private void tsbListaAnexos_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projeto)
            {
                using (FormListaAnexosProj frm = new FormListaAnexosProj(projeto))
                {
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Selecione um registro para visualizar os Anexos.");
            }
        }

        private void cbCentroCusto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCentroCusto.SelectedIndex >= 0 && cbCentroCusto.SelectedItem is CCustoFiltro cCusto)
            {
                CarregaDGV(cCusto.CodCCusto);
            }
            //else
            //{
            //    CarregaDGV();
            //}
        }
    }
}
