﻿namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos
{
    partial class FormProjetosHistoricos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProjetosHistoricos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPrincipal = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbAnaRisco = new System.Windows.Forms.GroupBox();
            this.ckAnaRiscoNao = new System.Windows.Forms.CheckBox();
            this.ckAnaRiscoSim = new System.Windows.Forms.CheckBox();
            this.txtAnaliseRiscos = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtLocal = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dgvSetoresDistribuicao = new System.Windows.Forms.DataGridView();
            this.jSETORESDISTRIBUICAOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtTipo = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtData = new System.Windows.Forms.MaskedTextBox();
            this.lblData = new System.Windows.Forms.Label();
            this.ckNao = new System.Windows.Forms.CheckBox();
            this.ckSim = new System.Windows.Forms.CheckBox();
            this.txtFuncao = new System.Windows.Forms.TextBox();
            this.txtResponsavelAnalise = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNumProjeto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRevisao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCCusto = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtVlrProposta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabAnalise = new System.Windows.Forms.TabPage();
            this.tablePerguntas = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDtRecebProjCliente = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.DELETE = new System.Windows.Forms.DataGridViewButtonColumn();
            this.sETORDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODIGODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODPROJETODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rEVISAODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cODSETORDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRICAODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jSETORESDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NROCOPIAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COPIADIGITAL = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.COPIAFISICA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.tabControl.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbAnaRisco.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetoresDistribuicao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jSETORESDISTRIBUICAOBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabAnalise.SuspendLayout();
            this.tablePerguntas.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.Location = new System.Drawing.Point(640, 577);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(106, 27);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPrincipal);
            this.tabControl.Controls.Add(this.tabAnalise);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(736, 563);
            this.tabControl.TabIndex = 47;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.groupBox1);
            this.tabPrincipal.Controls.Add(this.txtVlrProposta);
            this.tabPrincipal.Controls.Add(this.label10);
            this.tabPrincipal.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tabPrincipal.Location = new System.Drawing.Point(4, 22);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.Padding = new System.Windows.Forms.Padding(3);
            this.tabPrincipal.Size = new System.Drawing.Size(728, 537);
            this.tabPrincipal.TabIndex = 0;
            this.tabPrincipal.Text = "Cadastro Principal";
            this.tabPrincipal.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDtRecebProjCliente);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.gbAnaRisco);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.txtNumProjeto);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtRevisao);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtCodigo);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCCusto);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(722, 531);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações do projeto";
            // 
            // gbAnaRisco
            // 
            this.gbAnaRisco.Controls.Add(this.ckAnaRiscoNao);
            this.gbAnaRisco.Controls.Add(this.ckAnaRiscoSim);
            this.gbAnaRisco.Controls.Add(this.txtAnaliseRiscos);
            this.gbAnaRisco.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.gbAnaRisco.Location = new System.Drawing.Point(371, 161);
            this.gbAnaRisco.Name = "gbAnaRisco";
            this.gbAnaRisco.Size = new System.Drawing.Size(345, 80);
            this.gbAnaRisco.TabIndex = 45;
            this.gbAnaRisco.TabStop = false;
            this.gbAnaRisco.Text = "Análise de Riscos do Projeto:";
            // 
            // ckAnaRiscoNao
            // 
            this.ckAnaRiscoNao.AutoSize = true;
            this.ckAnaRiscoNao.Enabled = false;
            this.ckAnaRiscoNao.Location = new System.Drawing.Point(268, 0);
            this.ckAnaRiscoNao.Name = "ckAnaRiscoNao";
            this.ckAnaRiscoNao.Size = new System.Drawing.Size(47, 17);
            this.ckAnaRiscoNao.TabIndex = 46;
            this.ckAnaRiscoNao.Text = "Não";
            this.ckAnaRiscoNao.UseVisualStyleBackColor = true;
            this.ckAnaRiscoNao.CheckedChanged += new System.EventHandler(this.ckAnaRiscoNao_CheckedChanged);
            // 
            // ckAnaRiscoSim
            // 
            this.ckAnaRiscoSim.AutoSize = true;
            this.ckAnaRiscoSim.Enabled = false;
            this.ckAnaRiscoSim.Location = new System.Drawing.Point(189, 0);
            this.ckAnaRiscoSim.Name = "ckAnaRiscoSim";
            this.ckAnaRiscoSim.Size = new System.Drawing.Size(44, 17);
            this.ckAnaRiscoSim.TabIndex = 45;
            this.ckAnaRiscoSim.Text = "Sim";
            this.ckAnaRiscoSim.UseVisualStyleBackColor = true;
            this.ckAnaRiscoSim.CheckedChanged += new System.EventHandler(this.ckAnaRiscoSim_CheckedChanged);
            // 
            // txtAnaliseRiscos
            // 
            this.txtAnaliseRiscos.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtAnaliseRiscos.Location = new System.Drawing.Point(3, 19);
            this.txtAnaliseRiscos.Multiline = true;
            this.txtAnaliseRiscos.Name = "txtAnaliseRiscos";
            this.txtAnaliseRiscos.ReadOnly = true;
            this.txtAnaliseRiscos.Size = new System.Drawing.Size(339, 58);
            this.txtAnaliseRiscos.TabIndex = 44;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtLocal);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox5.Location = new System.Drawing.Point(11, 161);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(345, 80);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Local:";
            // 
            // txtLocal
            // 
            this.txtLocal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLocal.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtLocal.Location = new System.Drawing.Point(3, 18);
            this.txtLocal.Multiline = true;
            this.txtLocal.Name = "txtLocal";
            this.txtLocal.ReadOnly = true;
            this.txtLocal.Size = new System.Drawing.Size(339, 59);
            this.txtLocal.TabIndex = 44;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnAdd);
            this.groupBox6.Controls.Add(this.dgvSetoresDistribuicao);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(11, 256);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(502, 230);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Distribuido para:";
            // 
            // btnAdd
            // 
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAdd.Enabled = false;
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdd.Location = new System.Drawing.Point(459, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(29, 18);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // dgvSetoresDistribuicao
            // 
            this.dgvSetoresDistribuicao.AllowUserToAddRows = false;
            this.dgvSetoresDistribuicao.AutoGenerateColumns = false;
            this.dgvSetoresDistribuicao.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvSetoresDistribuicao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSetoresDistribuicao.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSetoresDistribuicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSetoresDistribuicao.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DELETE,
            this.sETORDataGridViewTextBoxColumn,
            this.cODIGODataGridViewTextBoxColumn,
            this.cODPROJETODataGridViewTextBoxColumn,
            this.rEVISAODataGridViewTextBoxColumn,
            this.cODSETORDataGridViewTextBoxColumn,
            this.dESCRICAODataGridViewTextBoxColumn,
            this.jSETORESDataGridViewTextBoxColumn,
            this.NROCOPIAS,
            this.COPIADIGITAL,
            this.COPIAFISICA});
            this.dgvSetoresDistribuicao.DataSource = this.jSETORESDISTRIBUICAOBindingSource;
            this.dgvSetoresDistribuicao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetoresDistribuicao.Enabled = false;
            this.dgvSetoresDistribuicao.Location = new System.Drawing.Point(3, 18);
            this.dgvSetoresDistribuicao.Name = "dgvSetoresDistribuicao";
            this.dgvSetoresDistribuicao.ReadOnly = true;
            this.dgvSetoresDistribuicao.Size = new System.Drawing.Size(496, 209);
            this.dgvSetoresDistribuicao.TabIndex = 51;
            // 
            // jSETORESDISTRIBUICAOBindingSource
            // 
            this.jSETORESDISTRIBUICAOBindingSource.DataSource = typeof(QUALIDADE.Dominio.JSETORESDISTRIBUICAO);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtTipo);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox3.Location = new System.Drawing.Point(371, 74);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(345, 80);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tipo:";
            // 
            // txtTipo
            // 
            this.txtTipo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTipo.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtTipo.Location = new System.Drawing.Point(3, 18);
            this.txtTipo.Multiline = true;
            this.txtTipo.Name = "txtTipo";
            this.txtTipo.ReadOnly = true;
            this.txtTipo.Size = new System.Drawing.Size(339, 59);
            this.txtTipo.TabIndex = 44;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtDescricao);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.groupBox2.Location = new System.Drawing.Point(11, 74);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(345, 80);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Descrição:";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescricao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtDescricao.Location = new System.Drawing.Point(3, 18);
            this.txtDescricao.Multiline = true;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.ReadOnly = true;
            this.txtDescricao.Size = new System.Drawing.Size(339, 59);
            this.txtDescricao.TabIndex = 44;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtData);
            this.groupBox4.Controls.Add(this.lblData);
            this.groupBox4.Controls.Add(this.ckNao);
            this.groupBox4.Controls.Add(this.ckSim);
            this.groupBox4.Controls.Add(this.txtFuncao);
            this.groupBox4.Controls.Add(this.txtResponsavelAnalise);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(519, 256);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(197, 230);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Análise Crítica";
            // 
            // txtData
            // 
            this.txtData.Enabled = false;
            this.txtData.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtData.Location = new System.Drawing.Point(9, 173);
            this.txtData.Mask = "00/00/0000";
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(100, 22);
            this.txtData.TabIndex = 48;
            this.txtData.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(6, 157);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(34, 13);
            this.lblData.TabIndex = 47;
            this.lblData.Text = "Data:";
            // 
            // ckNao
            // 
            this.ckNao.AutoSize = true;
            this.ckNao.Checked = true;
            this.ckNao.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckNao.Enabled = false;
            this.ckNao.Location = new System.Drawing.Point(119, 25);
            this.ckNao.Name = "ckNao";
            this.ckNao.Size = new System.Drawing.Size(48, 17);
            this.ckNao.TabIndex = 1;
            this.ckNao.Text = "Não";
            this.ckNao.UseVisualStyleBackColor = true;
            this.ckNao.CheckedChanged += new System.EventHandler(this.ckNao_CheckedChanged);
            // 
            // ckSim
            // 
            this.ckSim.AutoSize = true;
            this.ckSim.Enabled = false;
            this.ckSim.Location = new System.Drawing.Point(47, 25);
            this.ckSim.Name = "ckSim";
            this.ckSim.Size = new System.Drawing.Size(45, 17);
            this.ckSim.TabIndex = 0;
            this.ckSim.Text = "Sim";
            this.ckSim.UseVisualStyleBackColor = true;
            this.ckSim.CheckedChanged += new System.EventHandler(this.ckSim_CheckedChanged);
            // 
            // txtFuncao
            // 
            this.txtFuncao.Enabled = false;
            this.txtFuncao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtFuncao.Location = new System.Drawing.Point(9, 120);
            this.txtFuncao.MaxLength = 20;
            this.txtFuncao.Name = "txtFuncao";
            this.txtFuncao.ReadOnly = true;
            this.txtFuncao.Size = new System.Drawing.Size(182, 22);
            this.txtFuncao.TabIndex = 4;
            this.txtFuncao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResponsavelAnalise
            // 
            this.txtResponsavelAnalise.Enabled = false;
            this.txtResponsavelAnalise.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtResponsavelAnalise.Location = new System.Drawing.Point(9, 70);
            this.txtResponsavelAnalise.MaxLength = 20;
            this.txtResponsavelAnalise.Name = "txtResponsavelAnalise";
            this.txtResponsavelAnalise.ReadOnly = true;
            this.txtResponsavelAnalise.Size = new System.Drawing.Size(182, 22);
            this.txtResponsavelAnalise.TabIndex = 5;
            this.txtResponsavelAnalise.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(6, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Função:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(6, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Responsável:";
            // 
            // txtNumProjeto
            // 
            this.txtNumProjeto.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtNumProjeto.Location = new System.Drawing.Point(262, 42);
            this.txtNumProjeto.MaxLength = 20;
            this.txtNumProjeto.Name = "txtNumProjeto";
            this.txtNumProjeto.ReadOnly = true;
            this.txtNumProjeto.Size = new System.Drawing.Size(96, 22);
            this.txtNumProjeto.TabIndex = 2;
            this.txtNumProjeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(259, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Nº Projeto:";
            // 
            // txtRevisao
            // 
            this.txtRevisao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtRevisao.Location = new System.Drawing.Point(141, 42);
            this.txtRevisao.MaxLength = 20;
            this.txtRevisao.Name = "txtRevisao";
            this.txtRevisao.ReadOnly = true;
            this.txtRevisao.Size = new System.Drawing.Size(93, 22);
            this.txtRevisao.TabIndex = 1;
            this.txtRevisao.Text = "1";
            this.txtRevisao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(138, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Nº Revisão:";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtCodigo.Location = new System.Drawing.Point(11, 42);
            this.txtCodigo.MaxLength = 20;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ReadOnly = true;
            this.txtCodigo.Size = new System.Drawing.Size(101, 22);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "Código:";
            // 
            // cbCCusto
            // 
            this.cbCCusto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbCCusto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbCCusto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCCusto.Enabled = false;
            this.cbCCusto.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.cbCCusto.FormattingEnabled = true;
            this.cbCCusto.Location = new System.Drawing.Point(381, 42);
            this.cbCCusto.Name = "cbCCusto";
            this.cbCCusto.Size = new System.Drawing.Size(335, 21);
            this.cbCCusto.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(378, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "Centro de Custo:";
            // 
            // txtVlrProposta
            // 
            this.txtVlrProposta.Location = new System.Drawing.Point(101, 393);
            this.txtVlrProposta.Name = "txtVlrProposta";
            this.txtVlrProposta.Size = new System.Drawing.Size(107, 22);
            this.txtVlrProposta.TabIndex = 6;
            this.txtVlrProposta.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(19, 397);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "Vlr. Proposta:";
            this.label10.Visible = false;
            // 
            // tabAnalise
            // 
            this.tabAnalise.AutoScroll = true;
            this.tabAnalise.Controls.Add(this.tablePerguntas);
            this.tabAnalise.Location = new System.Drawing.Point(4, 22);
            this.tabAnalise.Name = "tabAnalise";
            this.tabAnalise.Padding = new System.Windows.Forms.Padding(3);
            this.tabAnalise.Size = new System.Drawing.Size(728, 498);
            this.tabAnalise.TabIndex = 3;
            this.tabAnalise.Text = "Análise Crítica";
            this.tabAnalise.UseVisualStyleBackColor = true;
            // 
            // tablePerguntas
            // 
            this.tablePerguntas.AutoScroll = true;
            this.tablePerguntas.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tablePerguntas.ColumnCount = 4;
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tablePerguntas.Controls.Add(this.label13, 3, 0);
            this.tablePerguntas.Controls.Add(this.label14, 2, 0);
            this.tablePerguntas.Controls.Add(this.label15, 1, 0);
            this.tablePerguntas.Controls.Add(this.label16, 0, 0);
            this.tablePerguntas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablePerguntas.Location = new System.Drawing.Point(3, 3);
            this.tablePerguntas.MaximumSize = new System.Drawing.Size(722, 324);
            this.tablePerguntas.MinimumSize = new System.Drawing.Size(722, 324);
            this.tablePerguntas.Name = "tablePerguntas";
            this.tablePerguntas.RowCount = 1;
            this.tablePerguntas.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablePerguntas.Size = new System.Drawing.Size(722, 324);
            this.tablePerguntas.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(650, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 21);
            this.label13.TabIndex = 3;
            this.label13.Text = "N/A";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Dock = System.Windows.Forms.DockStyle.Top;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(578, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 21);
            this.label14.TabIndex = 2;
            this.label14.Text = "NÃO";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(506, 1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 21);
            this.label15.TabIndex = 1;
            this.label15.Text = "SIM";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(4, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(495, 21);
            this.label16.TabIndex = 0;
            this.label16.Text = "PERGUNTA";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtDtRecebProjCliente
            // 
            this.txtDtRecebProjCliente.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.txtDtRecebProjCliente.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtDtRecebProjCliente.Location = new System.Drawing.Point(253, 498);
            this.txtDtRecebProjCliente.Mask = "00/00/0000";
            this.txtDtRecebProjCliente.Name = "txtDtRecebProjCliente";
            this.txtDtRecebProjCliente.Size = new System.Drawing.Size(100, 22);
            this.txtDtRecebProjCliente.TabIndex = 48;
            this.txtDtRecebProjCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 501);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(240, 13);
            this.label7.TabIndex = 49;
            this.label7.Text = "Data de recebimento do projeto pelo Cliente:";
            // 
            // DELETE
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.NullValue = "X";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Maroon;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White;
            this.DELETE.DefaultCellStyle = dataGridViewCellStyle2;
            this.DELETE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DELETE.HeaderText = "X";
            this.DELETE.Name = "DELETE";
            this.DELETE.ReadOnly = true;
            this.DELETE.Text = "X";
            this.DELETE.Width = 35;
            // 
            // sETORDataGridViewTextBoxColumn
            // 
            this.sETORDataGridViewTextBoxColumn.DataPropertyName = "SETOR";
            this.sETORDataGridViewTextBoxColumn.HeaderText = "SETOR";
            this.sETORDataGridViewTextBoxColumn.Name = "sETORDataGridViewTextBoxColumn";
            this.sETORDataGridViewTextBoxColumn.ReadOnly = true;
            this.sETORDataGridViewTextBoxColumn.Width = 90;
            // 
            // cODIGODataGridViewTextBoxColumn
            // 
            this.cODIGODataGridViewTextBoxColumn.DataPropertyName = "CODIGO";
            this.cODIGODataGridViewTextBoxColumn.HeaderText = "CODIGO";
            this.cODIGODataGridViewTextBoxColumn.Name = "cODIGODataGridViewTextBoxColumn";
            this.cODIGODataGridViewTextBoxColumn.ReadOnly = true;
            this.cODIGODataGridViewTextBoxColumn.Visible = false;
            this.cODIGODataGridViewTextBoxColumn.Width = 76;
            // 
            // cODPROJETODataGridViewTextBoxColumn
            // 
            this.cODPROJETODataGridViewTextBoxColumn.DataPropertyName = "CODPROJETO";
            this.cODPROJETODataGridViewTextBoxColumn.HeaderText = "CODPROJETO";
            this.cODPROJETODataGridViewTextBoxColumn.Name = "cODPROJETODataGridViewTextBoxColumn";
            this.cODPROJETODataGridViewTextBoxColumn.ReadOnly = true;
            this.cODPROJETODataGridViewTextBoxColumn.Visible = false;
            this.cODPROJETODataGridViewTextBoxColumn.Width = 102;
            // 
            // rEVISAODataGridViewTextBoxColumn
            // 
            this.rEVISAODataGridViewTextBoxColumn.DataPropertyName = "REVISAO";
            this.rEVISAODataGridViewTextBoxColumn.HeaderText = "REVISAO";
            this.rEVISAODataGridViewTextBoxColumn.Name = "rEVISAODataGridViewTextBoxColumn";
            this.rEVISAODataGridViewTextBoxColumn.ReadOnly = true;
            this.rEVISAODataGridViewTextBoxColumn.Visible = false;
            this.rEVISAODataGridViewTextBoxColumn.Width = 77;
            // 
            // cODSETORDataGridViewTextBoxColumn
            // 
            this.cODSETORDataGridViewTextBoxColumn.DataPropertyName = "CODSETOR";
            this.cODSETORDataGridViewTextBoxColumn.HeaderText = "CODSETOR";
            this.cODSETORDataGridViewTextBoxColumn.Name = "cODSETORDataGridViewTextBoxColumn";
            this.cODSETORDataGridViewTextBoxColumn.ReadOnly = true;
            this.cODSETORDataGridViewTextBoxColumn.Visible = false;
            this.cODSETORDataGridViewTextBoxColumn.Width = 89;
            // 
            // dESCRICAODataGridViewTextBoxColumn
            // 
            this.dESCRICAODataGridViewTextBoxColumn.DataPropertyName = "DESCRICAO";
            this.dESCRICAODataGridViewTextBoxColumn.HeaderText = "NOME COMPLETO / FUNÇÃO";
            this.dESCRICAODataGridViewTextBoxColumn.Name = "dESCRICAODataGridViewTextBoxColumn";
            this.dESCRICAODataGridViewTextBoxColumn.ReadOnly = true;
            this.dESCRICAODataGridViewTextBoxColumn.Width = 185;
            // 
            // jSETORESDataGridViewTextBoxColumn
            // 
            this.jSETORESDataGridViewTextBoxColumn.DataPropertyName = "JSETORES";
            this.jSETORESDataGridViewTextBoxColumn.HeaderText = "JSETORES";
            this.jSETORESDataGridViewTextBoxColumn.Name = "jSETORESDataGridViewTextBoxColumn";
            this.jSETORESDataGridViewTextBoxColumn.ReadOnly = true;
            this.jSETORESDataGridViewTextBoxColumn.Visible = false;
            this.jSETORESDataGridViewTextBoxColumn.Width = 81;
            // 
            // NROCOPIAS
            // 
            this.NROCOPIAS.DataPropertyName = "NROCOPIAS";
            this.NROCOPIAS.HeaderText = "NROCÓPIAS";
            this.NROCOPIAS.Name = "NROCOPIAS";
            this.NROCOPIAS.ReadOnly = true;
            this.NROCOPIAS.Width = 90;
            // 
            // COPIADIGITAL
            // 
            this.COPIADIGITAL.DataPropertyName = "COPIADIGITAL";
            this.COPIADIGITAL.HeaderText = "DIGITAL";
            this.COPIADIGITAL.Name = "COPIADIGITAL";
            this.COPIADIGITAL.ReadOnly = true;
            this.COPIADIGITAL.Width = 70;
            // 
            // COPIAFISICA
            // 
            this.COPIAFISICA.DataPropertyName = "COPIAFISICA";
            this.COPIAFISICA.HeaderText = "FÍSICA";
            this.COPIAFISICA.Name = "COPIAFISICA";
            this.COPIAFISICA.ReadOnly = true;
            this.COPIAFISICA.Width = 70;
            // 
            // FormProjetosHistoricos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(760, 611);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnCancelar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormProjetosHistoricos";
            this.Text = "Projetos";
            this.Load += new System.EventHandler(this.FormProjetosHistoricos_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPrincipal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbAnaRisco.ResumeLayout(false);
            this.gbAnaRisco.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetoresDistribuicao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jSETORESDISTRIBUICAOBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabAnalise.ResumeLayout(false);
            this.tablePerguntas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.DataGridViewTextBoxColumn SETOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODSETOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODPROJETO;
        private System.Windows.Forms.DataGridViewTextBoxColumn REVISAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn JSETORES;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPrincipal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtLocal;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView dgvSetoresDistribuicao;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtTipo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox ckNao;
        private System.Windows.Forms.CheckBox ckSim;
        private System.Windows.Forms.TextBox txtFuncao;
        private System.Windows.Forms.TextBox txtResponsavelAnalise;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNumProjeto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRevisao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCCusto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtVlrProposta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabAnalise;
        private System.Windows.Forms.TableLayoutPanel tablePerguntas;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.BindingSource jSETORESDISTRIBUICAOBindingSource;
        private System.Windows.Forms.GroupBox gbAnaRisco;
        private System.Windows.Forms.TextBox txtAnaliseRiscos;
        private System.Windows.Forms.CheckBox ckAnaRiscoNao;
        private System.Windows.Forms.CheckBox ckAnaRiscoSim;
        private System.Windows.Forms.MaskedTextBox txtData;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.MaskedTextBox txtDtRecebProjCliente;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewButtonColumn DELETE;
        private System.Windows.Forms.DataGridViewTextBoxColumn sETORDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODIGODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODPROJETODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rEVISAODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cODSETORDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRICAODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jSETORESDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn NROCOPIAS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn COPIADIGITAL;
        private System.Windows.Forms.DataGridViewCheckBoxColumn COPIAFISICA;
    }
}