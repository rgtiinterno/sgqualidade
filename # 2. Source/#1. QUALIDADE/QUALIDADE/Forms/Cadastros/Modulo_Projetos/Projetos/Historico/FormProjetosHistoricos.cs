﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos
{
    public partial class FormProjetosHistoricos : Form
    {
        public bool salvo = false;
        private bool inicio = true;
        private JPROJETOS itemEdit;
        private List<JSETORESDISTRIBUICAO> setoresDistribuicao;

        public FormProjetosHistoricos(JPROJETOS projeto)
        {
            InitializeComponent();
            itemEdit = projeto;
            setoresDistribuicao = new List<JSETORESDISTRIBUICAO>();
        }
        private void PreencheTabela(int codigo, decimal revisao)
        {
            List<Pergunta> perguntas = new Pergunta().GetPerguntasProjetos(codigo, revisao);
            tablePerguntas.RowCount = perguntas.Count + 1;

            for (int i = 1; i <= perguntas.Count; i++)
            {
                tablePerguntas.Controls.Add(GetLabel(perguntas[i - 1], 0, i), 0, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Sim", 1, i), 1, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Nao", 2, i), 2, i);
                tablePerguntas.Controls.Add(GetTextBox(perguntas[i - 1], "NA", 3, i), 3, i);
                
            }
        }

        private Control GetLabel(Pergunta pergunta, int coluna, int linha)
        {
            return new Label
            {
                Dock = DockStyle.Fill,
                Name = $"lblPergunta{pergunta.Codigo}",
                TabIndex = coluna + linha,
                Text = pergunta.DescPergunta,
                TextAlign = ContentAlignment.MiddleLeft
            };
        }

        private Control GetTextBox(Pergunta pergunta, string resposta, int coluna, int linha)
        {
            return new TextBox
            {
                Dock = DockStyle.Fill,
                Name = $"txt{resposta}{pergunta.Codigo}",
                TabIndex = coluna + linha,
                Text = pergunta.NRDO,
                Multiline = true, 
                ReadOnly = true
            };
        }

        private Control GetCheckbox(Pergunta pergunta, string resposta, int coluna, int linha)
        {
            bool checado = false;
            if (resposta == "Sim")
                checado = pergunta.Sim;
            else if (resposta == "Nao")
                checado = pergunta.Nao;

            CheckBox check = new CheckBox
            {
                Dock = DockStyle.Fill,
                CheckAlign = ContentAlignment.MiddleCenter,
                UseVisualStyleBackColor = true,
                Name = $"ck{resposta}{pergunta.Codigo}",
                TabIndex = coluna + linha,
                TextAlign = ContentAlignment.MiddleLeft,
                Tag = pergunta,
                Checked = checado,
                Enabled = false
            };
            check.CheckedChanged += new EventHandler(OnCheckOpt);

            return check;
        }

        private void OnCheckOpt(object sender, EventArgs args)
        {
            CheckBox check = (sender as CheckBox);
            if (check != null)
            {
                if (check.Tag is Pergunta pergunta)
                {
                    string nomeSim = $"ckSim{pergunta.Codigo}";
                    string nomeNao = $"ckNao{pergunta.Codigo}";
                    CheckBox checkSim = GetCheckBox(nomeSim);
                    CheckBox checkNao = GetCheckBox(nomeNao);

                    if (check.Checked && check.Name == nomeSim)
                        checkNao.Checked = false;
                    else if (check.Checked && check.Name == nomeNao)
                        checkSim.Checked = false;                    
                }
            }
        }

        private CheckBox GetCheckBox(string nome)
        {
            Control control = tablePerguntas.Controls.Find(nome, true).FirstOrDefault();
            if (control != null && (control is CheckBox check))
                return check;
            return null;
        }

        private TextBox GetTextBox(string nome)
        {
            Control control = tablePerguntas.Controls.Find(nome, true).FirstOrDefault();
            if (control != null && (control is TextBox text))
                return text;
            return null;
        }
        private List<JANALISE> GetAnalise(short CodProjeto, decimal revisao)
        {
            List<JANALISE> lista = new List<JANALISE>();
            TableLayoutControlCollection control = tablePerguntas.Controls;

            for (int i = 1; i <= tablePerguntas.RowCount; i++)
            {
                string nomeSim = $"ckSim{i}";
                string nomeNao = $"ckNao{i}";
                string nomeNA = $"txtNA{i}";

                JANALISE item = new JANALISE
                {
                    REVISAO = revisao,
                    CODPROJETO = CodProjeto,
                    CODPERGUNTA = Convert.ToInt16(i),
                    RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                    RECCREATEDON = DateTime.Now
                };

                foreach (Control c in control)
                {
                    if (c is TextBox text)
                    {
                        if (text.Name == nomeNA)
                            item.NA = c.Text;
                    }

                    if (c is CheckBox check)
                    {
                        if (check.Checked && check.Name == nomeSim)
                        {
                            item.SIM = true;
                            item.NAO = false;

                            lista.Add(item);
                        }
                        else if (check.Checked && check.Name == nomeNao)
                        {
                            item.SIM = false;
                            item.NAO = true;

                            lista.Add(item);
                        }
                    }
                }
            }

            return lista;
        }

        private void FormProjetosHistoricos_Load(object sender, EventArgs e)
        {
            tabControl.TabPages.Remove(tabAnalise);
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                cbCCusto.DataSource = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "J");

                cbCCusto.ValueMember = "CODCCUSTO";
                cbCCusto.DisplayMember = "NOME";
                cbCCusto.SelectedIndex = -1;
            }

            if (itemEdit == null)
            {
                using (ControleJProjetos controle = new ControleJProjetos())
                {
                    int codigo = controle.GetNewID();
                    txtCodigo.Text = Convert.ToString(codigo);
                    PreencheTabela(codigo, 1.0M);
                }
            }
            else
            {
                CarregaDados();
                PreencheTabela(itemEdit.CODIGO, itemEdit.REVISAO);
            }

            DesabilitaColunas();
            inicio = false;
        }

        private void CarregaDados()
        {
            txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
            txtRevisao.Text = Convert.ToString(itemEdit.REVISAO.DBNullToDecimal()).Replace(",", ".");
            txtNumProjeto.Text = itemEdit.NUMPROJETO;
            cbCCusto.SelectedValue = itemEdit.CODCCUSTO;
            txtFuncao.Text = itemEdit.FUNCAO;
            txtResponsavelAnalise.Text = itemEdit.RESPONSAVEL;
            txtDescricao.Text = itemEdit.DESCRICAO;
            txtTipo.Text = itemEdit.TIPO;
            txtLocal.Text = itemEdit.LOCAL;
            txtAnaliseRiscos.Text = itemEdit.ANALISERISCO;
            ckSim.Checked = itemEdit.POSSUIANALISECRITICA;
            ckNao.Checked = !ckSim.Checked;
            if(itemEdit.POSSUIANALISERISCO.HasValue)
                ckAnaRiscoSim.Checked = (bool)itemEdit.POSSUIANALISERISCO?.DBNullToBoolean();
            txtData.Text = !itemEdit.DTANALISECRITICA.HasValue ? null : itemEdit.DTANALISECRITICA.Value.ToString("dd/MM/yyyy");
            txtDtRecebProjCliente.Text = !itemEdit.DTENTREGACLIENTE.HasValue ? null : itemEdit.DTENTREGACLIENTE.Value.ToString("dd/MM/yyyy");

            using (ControleJSetoresDistribuidos controle = new ControleJSetoresDistribuidos())
            {
                setoresDistribuicao = controle.GetList(itemEdit.CODIGO, itemEdit.REVISAO);
                CarregaDVG();
            }

            txtNumProjeto.ReadOnly = true;
        }

        private void txtNumProjeto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Global.ApenasNumeros(e);
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !(ckSim.Checked);

            if (!tabControl.TabPages.Contains(tabAnalise))
                tabControl.TabPages.Add(tabAnalise);

            if (ckSim.Checked)
            {
                txtFuncao.Enabled = true;
                txtResponsavelAnalise.Enabled = true;
            }
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !(ckNao.Checked);

            if (tabControl.TabPages.Contains(tabAnalise))
                tabControl.TabPages.Remove(tabAnalise);

            if (ckNao.Checked)
            {
                txtFuncao.Text = string.Empty;
                txtResponsavelAnalise.Text = string.Empty;

                txtFuncao.Enabled = false;
                txtResponsavelAnalise.Enabled = false;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void CarregaDVG()
        {
            Cursor.Current = Cursors.WaitCursor;
            jSETORESDISTRIBUICAOBindingSource = new BindingSource();
            jSETORESDISTRIBUICAOBindingSource.DataSource = typeof(JSETORESDISTRIBUICAO);
            jSETORESDISTRIBUICAOBindingSource.DataSource = setoresDistribuicao;
            dgvSetoresDistribuicao.DataSource = jSETORESDISTRIBUICAOBindingSource;
            jSETORESDISTRIBUICAOBindingSource.EndEdit();
            dgvSetoresDistribuicao.Refresh();
            Cursor.Current = Cursors.Default;
        }

        private void DesabilitaColunas()
        {
            foreach (DataGridViewColumn item in dgvSetoresDistribuicao.Columns)
            {
                if (item.Name != "DELETE")
                    dgvSetoresDistribuicao.Columns[item.Name].ReadOnly = true;
            }
        }

        private void ckAnaRiscoSim_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAnaRiscoSim.Checked)
            {
                ckAnaRiscoNao.Checked = false;
                gbAnaRisco.Text = "Controle de Mitigação";
                txtAnaliseRiscos.Enabled = true;
            }
        }

        private void ckAnaRiscoNao_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAnaRiscoNao.Checked)
            {
                ckAnaRiscoSim.Checked = false;
                gbAnaRisco.Text = "Análise de Riscos do Projeto";
                txtAnaliseRiscos.Enabled = false;
            }
        }
    }
}
