﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos
{
    public partial class FormHistoricoProjetos : Form
    {
        private JPROJETOS item;

        public FormHistoricoProjetos(JPROJETOS projeto)
        {
            InitializeComponent();
            item = projeto;

            this.Text += $"< {projeto.CODIGO} - {projeto.CENTROCUSTO} >";
        }

        private void FormHistoricoProjetos_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns?.Clear();
            dgvListagem.Columns.Add(new GridCheckBoxColumn()
            {
                MappingName = "CKSELECIONADO",
                HeaderText = string.Empty,
                AllowEditing = true,
                AllowThreeState = true,
                AllowFiltering = false,
                AllowCheckBoxOnHeader = true,
                TrueValue = true,
                FalseValue = false
            });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCOLIGADA", HeaderText = "Cód. Coligada", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "REVISAO", HeaderText = "Rev.", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "NUMPROJETO", HeaderText = "Número Projeto", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODCCUSTO", HeaderText = "Cód. Centro Custo", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CENTROCUSTO", HeaderText = "Centro de Custo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODRESPONSAVEL", HeaderText = "Cód. Responsável", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RESPONSAVEL", HeaderText = "Responsável", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "FUNCAO", HeaderText = "Função", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "APROVADOR", HeaderText = "Aprovador", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TIPO", HeaderText = "Tipo", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "LOCAL", HeaderText = "Local", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "ANALISERISCO", HeaderText = "Análise de Risco", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridCheckBoxColumn() { MappingName = "POSSUIANALISECRITICA", HeaderText = "Possui Análise?", AllowEditing = false, AllowFiltering = true, Visible = true });
            //dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCANALISECRITICA", HeaderText = "Análise Crítica", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DISTRIBUICAO", HeaderText = "Distribuição", AllowEditing = false, AllowFiltering = true, Visible = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECCREATEDBY", HeaderText = "Criado Por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECCREATEDON", HeaderText = "Dt. Criação", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "RECMODIFIEDBY", HeaderText = "Modificado por", AllowEditing = false, AllowFiltering = true, Visible = false });
            dgvListagem.Columns.Add(new GridDateTimeColumn() { MappingName = "RECMODIFIEDON", HeaderText = "Dt. Modificação", AllowEditing = false, AllowFiltering = true, Visible = false });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleJProjetos controle = new ControleJProjetos())
            {
                Cursor.Current = Cursors.WaitCursor;
                jPROJETOSBindingSource = new BindingSource();
                jPROJETOSBindingSource.DataSource = typeof(JPROJETOS);
                jPROJETOSBindingSource.DataSource = controle.GetListHistorico(
                    item.CODCOLIGADA, item.CODIGO, item.CODCCUSTO).ToList();
                dgvListagem.DataSource = jPROJETOSBindingSource;
                jPROJETOSBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {jPROJETOSBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbEditar.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbEditar.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos && projetos.CKSELECIONADO)
            {
                    using (FormProjetosHistoricos frm = new FormProjetosHistoricos(projetos))
                    {
                        frm.StartPosition = FormStartPosition.CenterParent;
                        frm.WindowState = FormWindowState.Normal;
                        frm.ShowDialog();

                        if (frm.salvo)
                            tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione um registro para visualizar o mesmo.");
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projetos)
            {
                using (FormProjetosHistoricos frm = new FormProjetosHistoricos(projetos))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione um registro para visualizar o mesmo.");
        }

        private void tsbListaAnexos_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JPROJETOS projeto)
            {
                using (FormListaAnexosProj frm = new FormListaAnexosProj(projeto))
                {
                    frm.ShowDialog();
                }
            }
            else
            {
                Global.MsgErro(" - Selecione um registro para visualizar os Anexos.");
            }
        }
    }
}
