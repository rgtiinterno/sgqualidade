﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros
{
    public partial class FormListaAnexosProj : Form
    {
        private JPROJETOS _projeto;

        public FormListaAnexosProj(JPROJETOS projeto)
        {
            InitializeComponent();
            _projeto = projeto;
            this.Text = $"Anexos do Projeto - {_projeto.CODIGO} - Rev.: {_projeto.REVISAO}";
        }

        private void FormListaAnexosProj_Load(object sender, EventArgs e)
        {
            if (dgvListagem.Columns != null)
                dgvListagem.Columns.Clear();
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "SEQUENCIAL", HeaderText = "Sequencial", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Descrição", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "EXTENSAO", HeaderText = "Extensão", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "TAMANHO", HeaderText = "Tamanho", AllowEditing = false, AllowFiltering = false, Visible = false });
            CarregaDGV();
        }

        private void tsbVisualizar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is JPROJETOSANEXO anexo)
            {
                try
                {
                    byte[] arquivoAnexo = null;
                    using (ControleJProjetosAnexo controller = new ControleJProjetosAnexo())
                    {
                        arquivoAnexo = controller.Find(_projeto.CODIGO, _projeto.REVISAO, anexo.SEQUENCIAL)?.ARQUIVO;
                    }

                    if(arquivoAnexo?.Length > 0)
                    {
                        string arquivoTemp = $@"{Global.PastaTemp()}\{anexo.DESCRICAO}{anexo.EXTENSAO}";
                        Process.Start(Arquivo.DescriptPDF(arquivoAnexo, arquivoTemp));
                    }
                    else
                    {
                        Mensagem.Alerta("Falha", "Não foi possível carregar o conteúdo do arquivo anexo.");
                    }
                }
                catch (Exception ex)
                {
                    Mensagem.Erro("Falha", "Não foi possível abrir o anexo.\r\n" + ex.Message);
                }
            }
        }

        private void CarregaDGV()
        {
            Cursor.Current = Cursors.WaitCursor;
            using (ControleJProjetosAnexo controller = new ControleJProjetosAnexo())
            {
                BindingSource anexoBindingSource = new BindingSource();
                anexoBindingSource.DataSource = typeof(JPROJETOSANEXO);

                var itens = controller.getList(_projeto.CODIGO, _projeto.REVISAO).ToList();

                anexoBindingSource.DataSource = itens;
                dgvListagem.DataSource = null;
                dgvListagem.DataSource = anexoBindingSource;
                anexoBindingSource.EndEdit();
                if (itens?.Count() > 0)
                {
                    tsbVisualizar.Enabled = true;
                    tsbRemover.Enabled = true;
                }
                else
                {
                    tsbVisualizar.Enabled = false;
                    tsbRemover.Enabled = false;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            string caminhoArquivo = Arquivo.SelecionaArquivo("Todos os arquivos (*.*)|*.*");

            if (!string.IsNullOrEmpty(caminhoArquivo))
            {
                using (ControleJProjetosAnexo controller = new ControleJProjetosAnexo())
                {
                    try
                    {
                        JPROJETOSANEXO anexo = new JPROJETOSANEXO
                        {
                            CODPROJETO = _projeto.CODIGO,
                            REVISAO = _projeto.REVISAO,
                            SEQUENCIAL = controller.GetNewSequencial(_projeto.CODIGO, _projeto.REVISAO),
                            ARQUIVO = Arquivo.EncriptPDF(caminhoArquivo),
                            EXTENSAO = Path.GetExtension(caminhoArquivo).ToLower(),
                            DESCRICAO = Path.GetFileNameWithoutExtension(caminhoArquivo),
                            TAMANHO = new FileInfo(caminhoArquivo).Length,
                            RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                            RECCREATEDON = DateTime.Now
                        };
                        controller.Create(anexo);
                        controller.SaveAll();
                        Mensagem.Informacao("Sucesso", "Anexo incluido com sucesso!");
                    }
                    catch (Exception ex)
                    {
                        Mensagem.Informacao("Falha", "Não foi possível adicionar o anexo.\r\n" + ex.Message);
                    }
                }
                tsbAtualizar.PerformClick();
            }
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem is JPROJETOSANEXO anexo)
            {
                DialogResult dr = Mensagem.Confirmacao("Confirmação", "Deseja realmente excluir o anexo selecionado?");

                if (dr == DialogResult.Yes)
                {
                    using (ControleJProjetosAnexo controller = new ControleJProjetosAnexo())
                    {
                        try
                        {
                            string query = "DELETE FROM JPROJETOSANEXO " +
                                           "WHERE CODPROJETO=@CODPROJETO AND REVISAO=@REVISAO " +
                                           "AND SEQUENCIAL=@SEQUENCIAL";
                            List<SqlParameter> parametros = new List<SqlParameter>
                            {
                                new SqlParameter("@CODPROJETO", _projeto.CODIGO),
                                new SqlParameter("@REVISAO", _projeto.REVISAO),
                                new SqlParameter("@SEQUENCIAL", anexo.SEQUENCIAL)
                            };
                            controller.ExecuteSQL(query, parametros);
                            //controller.Delete(x =>
                            //    x.CODPROJETO == anexo.CODPROJETO &&
                            //    x.REVISAO == anexo.REVISAO &&
                            //    x.SEQUENCIAL == anexo.SEQUENCIAL
                            //);
                            //controller.SaveAll();
                            Mensagem.Informacao("Sucesso", "Anexo excluído com sucesso!");
                        }
                        catch (Exception ex)
                        {
                            Mensagem.Informacao("Falha", "Não foi possível excluir o anexo.\r\n" + ex.Message);
                        }
                    }
                    tsbAtualizar.PerformClick();
                }
            }
            else
            {
                Mensagem.Alerta("Alerta", "Selecione um anexo para realizar a exclusão.");
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbVisualizar.PerformClick();
        }
    }
}
