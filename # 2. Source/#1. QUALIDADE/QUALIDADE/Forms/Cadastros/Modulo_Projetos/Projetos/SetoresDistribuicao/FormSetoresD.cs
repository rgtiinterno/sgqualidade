﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos.SetoresDistribuicao
{
    public partial class FormSetoresD : Form
    {
        public bool salvo = false;
        public JSETORESDISTRIBUICAO item;

        public FormSetoresD()
        {
            InitializeComponent();
        }

        private void FormSetoresD_Load(object sender, EventArgs e)
        {
            using (ControleJSetores controle = new ControleJSetores())
            {
                var setores = controle.GetAll().ToList();
                cbSetoresDistribuicao.DataSource = setores;
                cbSetoresDistribuicao.ValueMember = "CODIGO";
                cbSetoresDistribuicao.DisplayMember = "DESCRICAO";
                if (setores?.Count() > 0)
                    cbSetoresDistribuicao.SelectedIndex = 0;
                else
                    cbSetoresDistribuicao.SelectedIndex = -1;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            using (ControleJSetoresDistribuidos controle = new ControleJSetoresDistribuidos())
            {
                item = new JSETORESDISTRIBUICAO
                {
                    CODIGO = controle.GetNewID(),
                    CODSETOR = Convert.ToInt16(cbSetoresDistribuicao.SelectedValue),
                    SETOR = cbSetoresDistribuicao.Text,
                    DESCRICAO = txtDescricao.Text,
                    NROCOPIAS = !string.IsNullOrEmpty(txtNroCopias.Text)? Convert.ToInt32(txtNroCopias.Text) : 0, 
                    COPIADIGITAL = ckCopiaDigital.Checked, 
                    COPIAFISICA = ckCopiaFisica.Checked
                };
            }
            salvo = true;
            this.Close();
        }

        private void txtNroCopias_KeyPress(object sender, KeyPressEventArgs e)
        {
            Global.ApenasNumeros(e);
        }
    }
}