﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos.SetoresDistribuicao;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos
{
    public partial class FormProjetos : Form
    {
        public bool salvo = false;
        private bool inicio = true;
        private JPROJETOS itemEdit;
        private List<JSETORESDISTRIBUICAO> setoresDistribuicao;

        public FormProjetos(JPROJETOS projeto = null)
        {
            InitializeComponent();
            itemEdit = projeto;
            setoresDistribuicao = new List<JSETORESDISTRIBUICAO>();
            if (ckAnaRiscoNao.Checked)
                txtAnaliseRiscos.Enabled = false;
        }

        private void PreencheTabela(int codigo, decimal revisao)
        {
            List<Pergunta> perguntas = new Pergunta().GetPerguntasProjetos(codigo, revisao);
            tablePerguntas.RowCount = perguntas.Count + 1;

            for (int i = 1; i <= perguntas.Count; i++)
            {
                tablePerguntas.Controls.Add(GetLabel(perguntas[i - 1], 0, i), 0, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Sim", 1, i), 1, i);
                tablePerguntas.Controls.Add(GetCheckbox(perguntas[i - 1], "Nao", 2, i), 2, i);
                tablePerguntas.Controls.Add(GetTextBox(perguntas[i - 1], "NA", 3, i), 3, i);
            }
        }

        private Control GetLabel(Pergunta pergunta, int coluna, int linha)
        {
            return new Label
            {
                Dock = DockStyle.Fill,
                Name = $"lblPergunta{pergunta.Codigo}",
                TabIndex = coluna + linha,
                Text = pergunta.DescPergunta,
                Tag = pergunta.Codigo,
                TextAlign = ContentAlignment.MiddleLeft
            };
        }

        private Control GetTextBox(Pergunta pergunta, string resposta, int coluna, int linha)
        {
            return new TextBox
            {
                Dock = DockStyle.Fill,
                Name = $"txt{resposta}{pergunta.Codigo}",
                TabIndex = coluna + linha,
                Text = pergunta.NRDO,
                Multiline = true
            };
        }

        private Control GetCheckbox(Pergunta pergunta, string resposta, int coluna, int linha)
        {
            bool checado = false;
            if (resposta == "Sim")
                checado = pergunta.Sim;
            else if (resposta == "Nao")
                checado = pergunta.Nao;

            CheckBox check = new CheckBox
            {
                Dock = DockStyle.Fill,
                CheckAlign = ContentAlignment.MiddleCenter,
                UseVisualStyleBackColor = true,
                Name = $"ck{resposta}{pergunta.Codigo}",
                TabIndex = coluna + linha,
                TextAlign = ContentAlignment.MiddleLeft,
                Tag = pergunta,
                Checked = checado
            };
            check.CheckedChanged += new EventHandler(OnCheckOpt);

            return check;
        }

        private void OnCheckOpt(object sender, EventArgs args)
        {
            CheckBox check = (sender as CheckBox);
            if (check != null)
            {
                if (check.Tag is Pergunta pergunta)
                {
                    string nomeSim = $"ckSim{pergunta.Codigo}";
                    string nomeNao = $"ckNao{pergunta.Codigo}";
                    CheckBox checkSim = GetCheckBox(nomeSim);
                    CheckBox checkNao = GetCheckBox(nomeNao);

                    if (check.Checked && check.Name == nomeSim)
                        checkNao.Checked = false;
                    else if (check.Checked && check.Name == nomeNao)
                        checkSim.Checked = false;
                }
            }
        }

        private CheckBox GetCheckBox(string nome)
        {
            Control control = tablePerguntas.Controls.Find(nome, true).FirstOrDefault();
            if (control != null && (control is CheckBox check))
                return check;
            return null;
        }

        private TextBox GetTextBox(string nome)
        {
            Control control = tablePerguntas.Controls.Find(nome, true).FirstOrDefault();
            if (control != null && (control is TextBox text))
                return text;
            return null;
        }

        private List<JANALISE> GetAnalise(int CodProjeto, decimal revisao)
        {
            List<JANALISE> lista = new List<JANALISE>();
            //TableLayoutControlCollection control = tablePerguntas.Controls;

            for (int i = 0; i < tablePerguntas.RowCount; i++)
            {
                if (i >= 1)
                {
                    string nomeSim = $"ckSim{i}";
                    string nomeNao = $"ckNao{i}";
                    string nomeNA = $"txtNA{i}";

                    JANALISE item = new JANALISE
                    {
                        REVISAO = revisao,
                        CODPROJETO = CodProjeto,
                        CODPERGUNTA = (tablePerguntas.GetControlFromPosition(0, i) as Label).Tag.DBNullToInt().Value,
                        SIM = (tablePerguntas.GetControlFromPosition(1, i) as CheckBox).Checked, 
                        NAO = (tablePerguntas.GetControlFromPosition(2, i) as CheckBox).Checked, 
                        NA = (tablePerguntas.GetControlFromPosition(3, i) as TextBox).Text, 
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };
                    lista.Add(item);


                    //bool titulo = true;
                    //foreach (Control c in control)
                    //{
                    //    if (c is TextBox text)
                    //    {
                    //        if (text.Name == nomeNA)
                    //        {
                    //            item.NA = c.Text;
                    //            titulo = false;
                    //        }
                    //    }

                    //    if (c is CheckBox check)
                    //    {
                    //        if (check.Checked && check.Name == nomeSim)
                    //        {
                    //            item.SIM = true;
                    //            item.NAO = false;
                    //            titulo = false;
                    //        }
                    //        else if (check.Checked && check.Name == nomeNao)
                    //        {
                    //            item.SIM = false;
                    //            item.NAO = true;
                    //            titulo = false;
                    //        }
                    //    }

                    //    if (c is Label label)
                    //        titulo = true;

                    //    if (!titulo)
                    //        lista.Add(item);
                    //}
                }
            }

            return lista;
        }

        private void FormProjetos_Load(object sender, EventArgs e)
        {
            tabControl.TabPages.Remove(tabAnalise);
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                cbCCusto.DataSource = controle.GetComboCCusto(
                    FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                    FormPrincipal.getUsuarioAcesso().CODUSUARIO, "J");

                cbCCusto.ValueMember = "CODCCUSTO";
                cbCCusto.DisplayMember = "NOME";
                cbCCusto.SelectedIndex = -1;
            }

            if (itemEdit == null)
            {
                using (ControleJProjetos controle = new ControleJProjetos())
                {
                    int codigo = controle.GetNewID();
                    txtCodigo.Text = Convert.ToString(codigo);
                    PreencheTabela(codigo, 1.0M);
                }
            }
            else
            {
                CarregaDados();
                PreencheTabela(itemEdit.CODIGO, itemEdit.REVISAO);
            }

            DesabilitaColunas();
            inicio = false;
        }

        private void CarregaDados()
        {
            txtCodigo.Text = Convert.ToString(itemEdit.CODIGO);
            txtRevisao.Text = Convert.ToString(itemEdit.REVISAO.DBNullToDecimal());
            txtNumProjeto.Text = itemEdit.NUMPROJETO;
            cbCCusto.SelectedValue = itemEdit.CODCCUSTO;
            txtFuncao.Text = itemEdit.FUNCAO;
            txtResponsavelAnalise.Text = itemEdit.RESPONSAVEL;
            txtDescricao.Text = itemEdit.DESCRICAO;
            txtTipo.Text = itemEdit.TIPO;
            txtLocal.Text = itemEdit.LOCAL;
            txtAnaliseRiscos.Text = itemEdit.ANALISERISCO;
            if (itemEdit.POSSUIANALISERISCO.HasValue)
                ckSim.Checked = itemEdit.POSSUIANALISECRITICA;
            ckNao.Checked = !ckSim.Checked;
            txtData.Text = !itemEdit.DTANALISECRITICA.HasValue ? null : itemEdit.DTANALISECRITICA.Value.ToString("dd/MM/yyyy");
            txtDtRecebProjCliente.Text = !itemEdit.DTENTREGACLIENTE.HasValue ? null : itemEdit.DTENTREGACLIENTE.Value.ToString("dd/MM/yyyy");

            if (itemEdit.POSSUIANALISERISCO == true)
                ckAnaRiscoSim.Checked = true;

            using (ControleJSetoresDistribuidos controle = new ControleJSetoresDistribuidos())
            {
                setoresDistribuicao = controle.GetList(itemEdit.CODIGO, itemEdit.REVISAO);
                CarregaDVG();
            }

            txtNumProjeto.ReadOnly = true;
        }

        private void txtNumProjeto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Global.ApenasNumeros(e);
        }

        private void ckSim_CheckedChanged(object sender, EventArgs e)
        {
            ckNao.Checked = !(ckSim.Checked);

            if (!tabControl.TabPages.Contains(tabAnalise))
                tabControl.TabPages.Add(tabAnalise);

            if (ckSim.Checked)
            {
                txtFuncao.Enabled = true;
                txtResponsavelAnalise.Enabled = true;
                txtData.Enabled = true;

                if (itemEdit != null)
                {
                    txtFuncao.Text = itemEdit.FUNCAO;
                    txtResponsavelAnalise.Text = itemEdit.RESPONSAVEL;
                }
            }
            else
            {
                txtData.Enabled = false;
            }
        }

        private void ckNao_CheckedChanged(object sender, EventArgs e)
        {
            ckSim.Checked = !(ckNao.Checked);

            if (tabControl.TabPages.Contains(tabAnalise))
                tabControl.TabPages.Remove(tabAnalise);

            if (ckNao.Checked)
            {
                txtFuncao.Text = string.Empty;
                txtResponsavelAnalise.Text = string.Empty;
                txtData.Text = string.Empty;

                txtFuncao.Enabled = false;
                txtResponsavelAnalise.Enabled = false;
                txtData.Enabled = false;
            }
            else
            {
                txtData.Enabled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (FormSetoresD frm = new FormSetoresD())
            {
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();

                if (frm.salvo)
                {
                    short listaCount = 1;
                    if (setoresDistribuicao != null || setoresDistribuicao.Count > 0)
                        listaCount = Convert.ToInt16(setoresDistribuicao.Count);

                    frm.item.CODIGO += listaCount;
                    frm.item.CODPROJETO = Convert.ToInt16(txtCodigo.Text);
                    frm.item.REVISAO = (decimal)txtRevisao.Text.DBNullToDecimal();
                    setoresDistribuicao.Add(frm.item);
                    CarregaDVG();
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                using (ControleJProjetos controle = new ControleJProjetos())
                {
                    JPROJETOS item = new JPROJETOS
                    {
                        CKSELECIONADO = false,
                        CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA,
                        CODIGO = Convert.ToInt16(txtCodigo.Text),
                        CODCCUSTO = Convert.ToString(cbCCusto.SelectedValue),
                        RESPONSAVEL = txtResponsavelAnalise.Text,
                        FUNCAO = txtFuncao.Text,
                        CENTROCUSTO = cbCCusto.Text,
                        DESCRICAO = txtDescricao.Text,
                        LOCAL = txtLocal.Text,
                        NUMPROJETO = txtNumProjeto.Text,
                        POSSUIANALISECRITICA = ckSim.Checked,
                        REVISAO = 0,
                        TIPO = txtTipo.Text,
                        POSSUIANALISERISCO = ckAnaRiscoSim.Checked,
                        ANALISERISCO = txtAnaliseRiscos.Text,
                        DTANALISECRITICA = txtData.Text.DBNullToDateTime(),
                        DTENTREGACLIENTE = txtDtRecebProjCliente.Text.DBNullToDateTime(),
                        RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                        RECCREATEDON = DateTime.Now
                    };

                    if (itemEdit != null)
                    {
                        DialogResult dr = Mensagem.Confirmacao("Número de Revisão", "Deseja gerar novo Número de Revisão?");

                        if (dr == DialogResult.Yes)
                        {
                            item.REVISAO = (decimal)txtRevisao.Text.DBNullToDecimal() + 1.0M;
                            List<JANALISE> listaAnaliseInsert = GetAnalise(item.CODIGO, item.REVISAO);
                            if (controle.GravaProjeto(item, setoresDistribuicao, listaAnaliseInsert))
                            {
                                Global.MsgSucesso("Operação realizada com sucesso!");
                                salvo = true;
                                this.Close();
                            }
                            else
                                Global.MsgErro("O sistema não conseguiu realizar a operação.\n" +
                                    "Tente novamente mais tarde.");
                        }
                        else
                        {
                            item.REVISAO = (decimal)txtRevisao.Text.DBNullToDecimal();
                            List<JANALISE> listaAnaliseupdate = GetAnalise(item.CODIGO, item.REVISAO);
                            if (controle.Update(item, setoresDistribuicao, listaAnaliseupdate))
                            {
                                Global.MsgSucesso("Operação realizada com sucesso!");
                                salvo = true;
                                this.Close();
                            }
                            else
                                Global.MsgErro("O sistema não conseguiu realizar a operação.\n" +
                                    "Tente novamente mais tarde.");
                        }
                    }
                    else
                    {
                        item.REVISAO = (decimal)txtRevisao.Text.DBNullToDecimal();

                        List<JANALISE> listaAnalise = GetAnalise(item.CODIGO, item.REVISAO);
                        if (controle.GravaProjeto(item, setoresDistribuicao, listaAnalise))
                        {
                            Global.MsgSucesso("Operação realizada com sucesso!");
                            salvo = true;
                            this.Close();
                        }
                        else
                            Global.MsgErro("O sistema não conseguiu realizar a operação.\n" +
                                "Tente novamente mais tarde.");
                    }
                }
            }
        }

        private void dgvSetoresDistribuicao_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvSetoresDistribuicao.CurrentRow is DataGridViewRow row)
            {
                JSETORESDISTRIBUICAO sDistribuicao = new JSETORESDISTRIBUICAO();
                foreach (JSETORESDISTRIBUICAO item in setoresDistribuicao)
                {
                    if (item.CODSETOR == Convert.ToInt16(row.Cells["cODSETORDataGridViewTextBoxColumn"].Value) &&
                        item.DESCRICAO == row.Cells["dESCRICAODataGridViewTextBoxColumn"].Value.DBNullToString())
                        sDistribuicao = item;
                }

                setoresDistribuicao.Remove(sDistribuicao);
                CarregaDVG();
            }
        }

        private void CarregaDVG()
        {
            Cursor.Current = Cursors.WaitCursor;
            jSETORESDISTRIBUICAOBindingSource = new BindingSource();
            jSETORESDISTRIBUICAOBindingSource.DataSource = typeof(JSETORESDISTRIBUICAO);
            jSETORESDISTRIBUICAOBindingSource.DataSource = setoresDistribuicao;
            dgvSetoresDistribuicao.DataSource = jSETORESDISTRIBUICAOBindingSource;
            jSETORESDISTRIBUICAOBindingSource.EndEdit();
            dgvSetoresDistribuicao.Refresh();
            Cursor.Current = Cursors.Default;
        } 

        private void DesabilitaColunas()
        {
            foreach (DataGridViewColumn item in dgvSetoresDistribuicao.Columns)
            {
                if (item.Name != "DELETE")
                    dgvSetoresDistribuicao.Columns[item.Name].ReadOnly = true;
            }
        }

        private bool ValidaCampos()
        {
            string msg = string.Empty;

            if (string.IsNullOrEmpty(txtNumProjeto.Text))
                msg += " - Informe o número do projeto.\n";

            if (cbCCusto.SelectedIndex < 0)
                msg += " - Informe um centro de custo.\n";

            //if (string.IsNullOrEmpty(txtFuncao.Text))
            //    msg += " - O centro de custo, não possui responsável cadastrado.\n";

            if (setoresDistribuicao.Count < 1)
                msg += " - Informe um ou mais setores.\n";

            if (string.IsNullOrEmpty(msg))
                return true;
            else
            {
                Global.MsgErro(msg);
                return false;
            }
        }

        private void ckAnaRiscoSim_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAnaRiscoSim.Checked)
            {
                ckAnaRiscoNao.Checked = false;
                gbAnaRisco.Text = "Controle de Mitigação";
                txtAnaliseRiscos.Enabled = true;
            }
        }

        private void ckAnaRiscoNao_CheckedChanged(object sender, EventArgs e)
        {
            if (ckAnaRiscoNao.Checked)
            {
                ckAnaRiscoSim.Checked = false;
                gbAnaRisco.Text = "Análise de Riscos do Projeto";
                txtAnaliseRiscos.Enabled = false;
            }
        }
    }
}
