﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.Analise
{
    public partial class FormJPerguntas : Form
    {
        public bool salvo = false;
        private JPERGUNTAS itemEdit;

        public FormJPerguntas(JPERGUNTAS pergunta = null)
        {
            InitializeComponent();
            itemEdit = pergunta;

            if (itemEdit != null)
            {
                txtPergunta.Text = itemEdit.PERGUNTA;
                ckAtiva.Checked = itemEdit.ATIVA;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtPergunta.Text))
            {
                using (ControleJPerguntas controle = new ControleJPerguntas())
                {
                    JPERGUNTAS item;

                    if (itemEdit == null)
                    {
                        item = new JPERGUNTAS
                        {
                            CODIGO = controle.GetNewID(),
                            PERGUNTA = txtPergunta.Text,
                            ATIVA = ckAtiva.Checked,
                            RECCREATEDBY = FormPrincipal.getUsuarioAcesso().LOGIN,
                            RECCREATEDON = DateTime.Now
                        };
                        controle.Create(item);
                    }
                    else
                    {
                        itemEdit = controle.Find(itemEdit.CODIGO);
                        itemEdit.PERGUNTA = txtPergunta.Text;
                        itemEdit.ATIVA = ckAtiva.Checked;
                        itemEdit.RECMODIFIEDBY = FormPrincipal.getUsuarioAcesso().LOGIN;
                        itemEdit.RECMODIFIEDON = DateTime.Now;
                    }
                    controle.SaveAll();
                    salvo = true;
                    this.Close();


                    //if (itemEdit != null)
                    //{
                    //    item.CODIGO = itemEdit.CODIGO;
                    //    controle.Update(item, x => x.CODIGO == itemEdit.CODIGO);
                    //    controle.SaveAll();
                    //    salvo = true;
                    //    this.Close();
                    //}
                    //else
                    //{
                    //    controle.Create(item);
                    //    controle.SaveAll();
                    //    salvo = true;
                    //    this.Close();
                    //}
                }
            }
            else
            {
                FormMsg frm = new FormMsg("Informe a pergunta");
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();
            }
        }
    }
}
