﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.SetoresDistribidos
{
    public partial class FormSetoresDistribuidos : Form
    {
        public bool salvo = false;
        private JSETORES itemEdit;

        public FormSetoresDistribuidos(JSETORES setoresDistribuidos = null)
        {
            InitializeComponent();
            itemEdit = setoresDistribuidos;

            if (itemEdit != null)
                txtDescricao.Text = itemEdit.DESCRICAO;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtDescricao.Text))
                Global.MsgErro(" Informe o nome do setor.");
            else
            {
                using (ControleJSetores controle = new ControleJSetores())
                {
                    JSETORES setores = new JSETORES
                    {
                        CODIGO = controle.GetNewID(),
                        DESCRICAO = txtDescricao.Text
                    }; 

                    if(itemEdit == null)
                    {
                        controle.Create(setores);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                    else
                    {
                        setores.CODIGO = itemEdit.CODIGO;

                        controle.Update(setores, x => x.CODIGO == itemEdit.CODIGO);
                        controle.SaveAll();
                        salvo = true;
                        this.Close();
                    }
                }
            }
        }
    }
}
