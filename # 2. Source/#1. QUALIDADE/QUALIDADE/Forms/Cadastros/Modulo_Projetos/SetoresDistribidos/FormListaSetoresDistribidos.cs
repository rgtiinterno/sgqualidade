﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using Syncfusion.WinForms.DataGrid;
using Syncfusion.WinForms.DataGridConverter;
using Syncfusion.XlsIO;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Cadastros.ModuloCPJ.SetoresDistribidos
{
    public partial class FormListaSetoresDistribidos : Form
    {
        public FormListaSetoresDistribidos()
        {
            InitializeComponent();
        }

        private void exportarParaXLSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string arquivo = Arquivo.SalvaArquivo("Arquivo XLS (*.xls)|*.xls", $"SetoresDistribuidos.xls");
            if (!string.IsNullOrEmpty(arquivo))
            {
                Cursor.Current = Cursors.WaitCursor;
                var options = new ExcelExportingOptions
                {
                    AllowOutlining = true,
                    ExcelVersion = ExcelVersion.Excel2013,
                    ExportMode = ExportMode.Text,
                    ExportGroupSummary = true
                };
                if (dgvListagem?.Columns?.Where(x => x.MappingName == "CkSelecionado")?.Count() > 0)
                    options.ExcludeColumns.Add("CkSelecionado");
                var excelEngine = dgvListagem.ExportToExcel(dgvListagem.View, options);
                var workBook = excelEngine.Excel.Workbooks[0];
                workBook.SaveAs(arquivo, ExcelSaveType.SaveAsXLS);
                Cursor.Current = Cursors.Default;
                Mensagem.Informacao("Informação", "Exportado com sucesso.");
            }
        }

        private void FormListaSetoresDistribidos_Load(object sender, EventArgs e)
        {
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "CODIGO", HeaderText = "Código", AllowEditing = false, AllowFiltering = true });
            dgvListagem.Columns.Add(new GridTextColumn() { MappingName = "DESCRICAO", HeaderText = "Setores", AllowEditing = false, AllowFiltering = true });

            CarregaDGV();
        }

        private void CarregaDGV()
        {
            using (ControleJSetores controle = new ControleJSetores())
            {
                Cursor.Current = Cursors.WaitCursor;
                jSETORESBindingSource = new BindingSource();
                jSETORESBindingSource.DataSource = typeof(JSETORES);
                jSETORESBindingSource.DataSource = controle.GetAll().ToList();
                dgvListagem.DataSource = jSETORESBindingSource;
                jSETORESBindingSource.EndEdit();
                tslTotalItens.Text = $"Total de registros: {jSETORESBindingSource.Count}";

                if (dgvListagem != null && dgvListagem.View != null &&
                    dgvListagem.View.Records != null && ((dgvListagem.View.Records.Count) > 0))
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = true;
                    tsbRemover.Enabled = true;
                    tsbAtualizar.Enabled = true;
                }
                else
                {
                    tsbAdicionar.Enabled = true;
                    tsbEditar.Enabled = false;
                    tsbRemover.Enabled = false;
                    tsbAtualizar.Enabled = true;
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void tsbAtualizar_Click(object sender, EventArgs e)
        {
            CarregaDGV();
        }

        private void tsbAdicionar_Click(object sender, EventArgs e)
        {
            using (FormSetoresDistribuidos frm = new FormSetoresDistribuidos())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (frm.salvo)
                    tsbAtualizar.PerformClick();

                frm.Dispose();
            }
        }

        private void tsbEditar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JSETORES setores)
            {
                using (FormSetoresDistribuidos frm = new FormSetoresDistribuidos(setores))
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                        tsbAtualizar.PerformClick();

                    frm.Dispose();
                }
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a edição do mesmo.");
        }

        private void tsbRemover_Click(object sender, EventArgs e)
        {
            if (dgvListagem.SelectedItem is JSETORES setores)
            {
                using (ControleJSetores controle = new ControleJSetores())
                {
                    controle.Delete(x => x.CODIGO == setores.CODIGO);
                    controle.SaveAll();
                    tsbAtualizar.PerformClick();
                }
            }
            else
                Global.MsgErro(" - Selecione ao um registro para efetuar a exclusão do mesmo.");
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbEditar.PerformClick();
        }
    }
}