﻿namespace QUALIDADE
{
    partial class FormListaItensCombo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaItensCombo));
            this.dgvListagem = new Syncfusion.WinForms.DataGrid.SfDataGrid();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbResultados = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbSelecionar = new System.Windows.Forms.ToolStripButton();
            this.tsbPesquisar = new System.Windows.Forms.ToolStripButton();
            this.txtValorPesquisa = new System.Windows.Forms.ToolStripTextBox();
            this.cbCampoPesquisa = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListagem
            // 
            this.dgvListagem.AccessibleName = "Table";
            this.dgvListagem.AllowDraggingColumns = true;
            this.dgvListagem.AllowEditing = false;
            this.dgvListagem.AllowFiltering = true;
            this.dgvListagem.AllowResizingColumns = true;
            this.dgvListagem.AutoSizeColumnsMode = Syncfusion.WinForms.DataGrid.Enums.AutoSizeColumnsMode.AllCells;
            this.dgvListagem.BackColor = System.Drawing.SystemColors.Control;
            this.dgvListagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListagem.FilterPopupMode = Syncfusion.WinForms.GridCommon.FilterPopupMode.CheckBoxFilter;
            this.dgvListagem.Location = new System.Drawing.Point(0, 25);
            this.dgvListagem.Name = "dgvListagem";
            this.dgvListagem.SelectionUnit = Syncfusion.WinForms.DataGrid.Enums.SelectionUnit.Cell;
            this.dgvListagem.ShowSortNumbers = true;
            this.dgvListagem.ShowToolTip = true;
            this.dgvListagem.Size = new System.Drawing.Size(784, 512);
            this.dgvListagem.Style.CellStyle.BackColor = System.Drawing.Color.White;
            this.dgvListagem.TabIndex = 24;
            this.dgvListagem.CellDoubleClick += new Syncfusion.WinForms.DataGrid.Events.CellClickEventHandler(this.dgvListagem_CellDoubleClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbResultados});
            this.statusStrip1.Location = new System.Drawing.Point(0, 537);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 24);
            this.statusStrip1.TabIndex = 25;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbResultados
            // 
            this.lbResultados.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.lbResultados.Name = "lbResultados";
            this.lbResultados.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lbResultados.Size = new System.Drawing.Size(45, 19);
            this.lbResultados.Text = "        ";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSelecionar,
            this.tsbPesquisar,
            this.txtValorPesquisa,
            this.cbCampoPesquisa});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 26;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbSelecionar
            // 
            this.tsbSelecionar.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tsbSelecionar.Image = global::QUALIDADE.Properties.Resources.todo_list_16px;
            this.tsbSelecionar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSelecionar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSelecionar.Margin = new System.Windows.Forms.Padding(3, 1, 3, 2);
            this.tsbSelecionar.Name = "tsbSelecionar";
            this.tsbSelecionar.Size = new System.Drawing.Size(81, 22);
            this.tsbSelecionar.Text = "Selecionar";
            this.tsbSelecionar.ToolTipText = "Adicionar item";
            this.tsbSelecionar.Click += new System.EventHandler(this.tsbSelecionar_Click);
            // 
            // tsbPesquisar
            // 
            this.tsbPesquisar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbPesquisar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPesquisar.Image = global::QUALIDADE.Properties.Resources._285651_search_icon;
            this.tsbPesquisar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPesquisar.Name = "tsbPesquisar";
            this.tsbPesquisar.Size = new System.Drawing.Size(23, 22);
            this.tsbPesquisar.Text = "Filtrar";
            this.tsbPesquisar.Click += new System.EventHandler(this.tsbPesquisar_Click);
            // 
            // txtValorPesquisa
            // 
            this.txtValorPesquisa.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.txtValorPesquisa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorPesquisa.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.txtValorPesquisa.Name = "txtValorPesquisa";
            this.txtValorPesquisa.Size = new System.Drawing.Size(250, 25);
            this.txtValorPesquisa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtValorPesquisa_KeyDown);
            // 
            // cbCampoPesquisa
            // 
            this.cbCampoPesquisa.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.cbCampoPesquisa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCampoPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbCampoPesquisa.Name = "cbCampoPesquisa";
            this.cbCampoPesquisa.Size = new System.Drawing.Size(121, 25);
            // 
            // FormListaItensCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.dgvListagem);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "FormListaItensCombo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selecionar Item";
            ((System.ComponentModel.ISupportInitialize)(this.dgvListagem)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.WinForms.DataGrid.SfDataGrid dgvListagem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lbResultados;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbSelecionar;
        private System.Windows.Forms.ToolStripButton tsbPesquisar;
        private System.Windows.Forms.ToolStripTextBox txtValorPesquisa;
        private System.Windows.Forms.ToolStripComboBox cbCampoPesquisa;
    }
}