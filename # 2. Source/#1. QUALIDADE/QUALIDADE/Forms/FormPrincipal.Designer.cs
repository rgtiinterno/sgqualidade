﻿namespace QUALIDADE.Forms
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Syncfusion.Windows.Forms.Tools.Office2016ColorTable office2016ColorTable1 = new Syncfusion.Windows.Forms.Tools.Office2016ColorTable();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPrincipal));
            this.ribbonControlAdv1 = new Syncfusion.Windows.Forms.Tools.RibbonControlAdv();
            this.tabNaoConformidade = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx6 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbPlanoAcao = new System.Windows.Forms.ToolStripButton();
            this.tsbEficaciaAcao = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx1 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbNaoConformidade = new System.Windows.Forms.ToolStripButton();
            this.tsbExtratificacao = new System.Windows.Forms.ToolStripButton();
            this.tsbTipoRegistro = new System.Windows.Forms.ToolStripButton();
            this.tsbCadSetores = new System.Windows.Forms.ToolStripButton();
            this.tabFornecedores = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx9 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbDossie = new System.Windows.Forms.ToolStripButton();
            this.tsbGerarNConformidade = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx4 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbDocumentos = new System.Windows.Forms.ToolStripButton();
            this.tsbTpFornecedor = new System.Windows.Forms.ToolStripButton();
            this.tsbFornecedores = new System.Windows.Forms.ToolStripButton();
            this.tabPropostas = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.tseGeralProposta = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbDashCP = new System.Windows.Forms.ToolStripButton();
            this.tsbPropostas = new System.Windows.Forms.ToolStripButton();
            this.tsbSetor = new System.Windows.Forms.ToolStripSplitButton();
            this.cadastroDeClientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrosDeInteressesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeMetasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeRespToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeSetoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tseGeralProjetos = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbProjetos = new System.Windows.Forms.ToolStripButton();
            this.tsbSetoresDistribuicao = new System.Windows.Forms.ToolStripButton();
            this.tsbAnaliseProjetos = new System.Windows.Forms.ToolStripButton();
            this.tseAfericaoCalibracao = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbAfericoes = new System.Windows.Forms.ToolStripButton();
            this.tsbCalibracao = new System.Windows.Forms.ToolStripButton();
            this.tseFVS = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbCadastroFVS = new System.Windows.Forms.ToolStripButton();
            this.tsbModeloFVS = new System.Windows.Forms.ToolStripButton();
            this.tabRiscos = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx3 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tabGlobal = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx8 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbColigadas = new System.Windows.Forms.ToolStripButton();
            this.tsbCCusto = new System.Windows.Forms.ToolStripButton();
            this.tsbListaUsuarios = new System.Windows.Forms.ToolStripButton();
            this.tseEmail = new System.Windows.Forms.ToolStripButton();
            this.tsbEnviaEmail = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx11 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.tsbDocEletronica = new System.Windows.Forms.ToolStripButton();
            this.tabConfiguracoes = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx5 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbAlterarColigada = new System.Windows.Forms.ToolStripButton();
            this.tsbAtualizarBD = new System.Windows.Forms.ToolStripButton();
            this.tsbBancoDados = new System.Windows.Forms.ToolStripButton();
            this.tsbImportarArquivos = new System.Windows.Forms.ToolStripButton();
            this.tsbExportarArquivos = new System.Windows.Forms.ToolStripButton();
            this.toolStripEx7 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tsbSairSistema = new System.Windows.Forms.ToolStripButton();
            this.tsbSobre = new System.Windows.Forms.ToolStripButton();
            this.tsbAlterarUsr = new System.Windows.Forms.ToolStripButton();
            this.tabProjetos = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripTabItem2 = new Syncfusion.Windows.Forms.Tools.ToolStripTabItem();
            this.toolStripEx2 = new Syncfusion.Windows.Forms.Tools.ToolStripEx();
            this.tabbedMDIManager1 = new Syncfusion.Windows.Forms.Tools.TabbedMDIManager(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblSistema = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDataSistema = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblHoraSistema = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblLoginUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblColigada = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerDataHora = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).BeginInit();
            this.ribbonControlAdv1.SuspendLayout();
            this.tabNaoConformidade.Panel.SuspendLayout();
            this.toolStripEx6.SuspendLayout();
            this.toolStripEx1.SuspendLayout();
            this.tabFornecedores.Panel.SuspendLayout();
            this.toolStripEx9.SuspendLayout();
            this.toolStripEx4.SuspendLayout();
            this.tabPropostas.Panel.SuspendLayout();
            this.tseGeralProposta.SuspendLayout();
            this.tseGeralProjetos.SuspendLayout();
            this.tseAfericaoCalibracao.SuspendLayout();
            this.tseFVS.SuspendLayout();
            this.tabRiscos.Panel.SuspendLayout();
            this.tabGlobal.Panel.SuspendLayout();
            this.toolStripEx8.SuspendLayout();
            this.toolStripEx11.SuspendLayout();
            this.tabConfiguracoes.Panel.SuspendLayout();
            this.toolStripEx5.SuspendLayout();
            this.toolStripEx7.SuspendLayout();
            this.toolStripTabItem2.Panel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbonControlAdv1
            // 
            this.ribbonControlAdv1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ribbonControlAdv1.Header.AddMainItem(tabNaoConformidade);
            this.ribbonControlAdv1.Header.AddMainItem(tabFornecedores);
            this.ribbonControlAdv1.Header.AddMainItem(tabPropostas);
            this.ribbonControlAdv1.Header.AddMainItem(tabRiscos);
            this.ribbonControlAdv1.Header.AddMainItem(tabGlobal);
            this.ribbonControlAdv1.Header.AddMainItem(tabConfiguracoes);
            this.ribbonControlAdv1.Location = new System.Drawing.Point(1, 0);
            this.ribbonControlAdv1.MenuButtonFont = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ribbonControlAdv1.MenuButtonText = "";
            this.ribbonControlAdv1.MenuButtonVisible = false;
            this.ribbonControlAdv1.MenuButtonWidth = 56;
            this.ribbonControlAdv1.MenuColor = System.Drawing.Color.DarkRed;
            this.ribbonControlAdv1.Name = "ribbonControlAdv1";
            office2016ColorTable1.BackStageItemHoverColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.BackStageItemSelectionColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.CheckedTabForeColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.DisabledControlBoxForeColor = System.Drawing.Color.White;
            office2016ColorTable1.HeaderColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.HoverCollapsedDropDownButtonForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            office2016ColorTable1.QATDropDownForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            office2016ColorTable1.QuickDropDownPressedcolor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.QuickDropDownSelectedcolor = System.Drawing.Color.Maroon;
            office2016ColorTable1.SelectedTabColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.SystemButtonBackground = System.Drawing.Color.DarkRed;
            office2016ColorTable1.TabBackColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.TabGroupBackColor = System.Drawing.Color.DarkRed;
            office2016ColorTable1.TabGroupForeColor = System.Drawing.Color.White;
            this.ribbonControlAdv1.Office2016ColorTable.Add(office2016ColorTable1);
            this.ribbonControlAdv1.OfficeColorScheme = Syncfusion.Windows.Forms.Tools.ToolStripEx.ColorScheme.Managed;
            // 
            // ribbonControlAdv1.OfficeMenu
            // 
            this.ribbonControlAdv1.OfficeMenu.Name = "OfficeMenu";
            this.ribbonControlAdv1.OfficeMenu.ShowItemToolTips = true;
            this.ribbonControlAdv1.OfficeMenu.Size = new System.Drawing.Size(12, 65);
            this.ribbonControlAdv1.QuickPanelImageLayout = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ribbonControlAdv1.RibbonHeaderImage = Syncfusion.Windows.Forms.Tools.RibbonHeaderImage.None;
            this.ribbonControlAdv1.RibbonStyle = Syncfusion.Windows.Forms.Tools.RibbonStyle.Office2016;
            this.ribbonControlAdv1.SelectedTab = this.tabNaoConformidade;
            this.ribbonControlAdv1.ShowQuickItemsDropDownButton = false;
            this.ribbonControlAdv1.ShowRibbonDisplayOptionButton = false;
            this.ribbonControlAdv1.Size = new System.Drawing.Size(1092, 191);
            this.ribbonControlAdv1.SystemText.QuickAccessDialogDropDownName = "Start menu";
            this.ribbonControlAdv1.SystemText.RenameDisplayLabelText = "&Display Name:";
            this.ribbonControlAdv1.TabIndex = 0;
            this.ribbonControlAdv1.Text = "ribbonControlAdv1";
            this.ribbonControlAdv1.ThemeName = "Office2016";
            // 
            // tabNaoConformidade
            // 
            this.tabNaoConformidade.Name = "tabNaoConformidade";
            // 
            // ribbonControlAdv1.ribbonPanel1
            // 
            this.tabNaoConformidade.Panel.Controls.Add(this.toolStripEx6);
            this.tabNaoConformidade.Panel.Controls.Add(this.toolStripEx1);
            this.tabNaoConformidade.Panel.Name = "ribbonPanel1";
            this.tabNaoConformidade.Panel.ScrollPosition = 0;
            this.tabNaoConformidade.Panel.TabIndex = 2;
            this.tabNaoConformidade.Panel.Text = "Não Conformidades";
            this.tabNaoConformidade.Position = 0;
            this.tabNaoConformidade.Size = new System.Drawing.Size(126, 25);
            this.tabNaoConformidade.Text = "Não Conformidades";
            // 
            // toolStripEx6
            // 
            this.toolStripEx6.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx6.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx6.Image = null;
            this.toolStripEx6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPlanoAcao,
            this.tsbEficaciaAcao});
            this.toolStripEx6.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx6.Name = "toolStripEx6";
            this.toolStripEx6.Office12Mode = false;
            this.toolStripEx6.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx6.Size = new System.Drawing.Size(122, 127);
            this.toolStripEx6.TabIndex = 1;
            this.toolStripEx6.Text = "Verificação";
            // 
            // tsbPlanoAcao
            // 
            this.tsbPlanoAcao.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlanoAcao.Image")));
            this.tsbPlanoAcao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbPlanoAcao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlanoAcao.Name = "tsbPlanoAcao";
            this.tsbPlanoAcao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbPlanoAcao.Size = new System.Drawing.Size(104, 110);
            this.tsbPlanoAcao.Text = "Verificação de\r\nPlano de Ação";
            this.tsbPlanoAcao.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPlanoAcao.Visible = false;
            this.tsbPlanoAcao.Click += new System.EventHandler(this.tsbPlanoAcao_Click);
            // 
            // tsbEficaciaAcao
            // 
            this.tsbEficaciaAcao.Image = ((System.Drawing.Image)(resources.GetObject("tsbEficaciaAcao.Image")));
            this.tsbEficaciaAcao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbEficaciaAcao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEficaciaAcao.Name = "tsbEficaciaAcao";
            this.tsbEficaciaAcao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbEficaciaAcao.Size = new System.Drawing.Size(113, 110);
            this.tsbEficaciaAcao.Text = "Verificação de\r\nEficacia da Ação";
            this.tsbEficaciaAcao.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbEficaciaAcao.Click += new System.EventHandler(this.tsbEficaciaAcao_Click);
            // 
            // toolStripEx1
            // 
            this.toolStripEx1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx1.Image = null;
            this.toolStripEx1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbNaoConformidade,
            this.tsbExtratificacao,
            this.tsbTipoRegistro,
            this.tsbCadSetores});
            this.toolStripEx1.Location = new System.Drawing.Point(124, 1);
            this.toolStripEx1.Name = "toolStripEx1";
            this.toolStripEx1.Office12Mode = false;
            this.toolStripEx1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx1.Size = new System.Drawing.Size(461, 127);
            this.toolStripEx1.TabIndex = 0;
            this.toolStripEx1.Text = "Cadastros";
            // 
            // tsbNaoConformidade
            // 
            this.tsbNaoConformidade.Image = ((System.Drawing.Image)(resources.GetObject("tsbNaoConformidade.Image")));
            this.tsbNaoConformidade.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbNaoConformidade.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbNaoConformidade.Name = "tsbNaoConformidade";
            this.tsbNaoConformidade.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbNaoConformidade.Size = new System.Drawing.Size(134, 110);
            this.tsbNaoConformidade.Text = "Cadastro de\r\nNão Conformidades";
            this.tsbNaoConformidade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbNaoConformidade.Click += new System.EventHandler(this.tsbNaoConformidade_Click);
            // 
            // tsbExtratificacao
            // 
            this.tsbExtratificacao.Image = ((System.Drawing.Image)(resources.GetObject("tsbExtratificacao.Image")));
            this.tsbExtratificacao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExtratificacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExtratificacao.Name = "tsbExtratificacao";
            this.tsbExtratificacao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbExtratificacao.Size = new System.Drawing.Size(99, 110);
            this.tsbExtratificacao.Text = "Cadastro de\r\nEstratificação";
            this.tsbExtratificacao.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbExtratificacao.Click += new System.EventHandler(this.tsbExtratificacao_Click);
            // 
            // tsbTipoRegistro
            // 
            this.tsbTipoRegistro.Image = ((System.Drawing.Image)(resources.GetObject("tsbTipoRegistro.Image")));
            this.tsbTipoRegistro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbTipoRegistro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTipoRegistro.Name = "tsbTipoRegistro";
            this.tsbTipoRegistro.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbTipoRegistro.Size = new System.Drawing.Size(127, 110);
            this.tsbTipoRegistro.Text = "Cadastro de Tipos \r\nde Registros";
            this.tsbTipoRegistro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbTipoRegistro.Click += new System.EventHandler(this.tsbTipoRegistro_Click);
            // 
            // tsbCadSetores
            // 
            this.tsbCadSetores.Image = ((System.Drawing.Image)(resources.GetObject("tsbCadSetores.Image")));
            this.tsbCadSetores.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCadSetores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCadSetores.Name = "tsbCadSetores";
            this.tsbCadSetores.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbCadSetores.Size = new System.Drawing.Size(92, 110);
            this.tsbCadSetores.Text = "Cadasto de \r\nSetores";
            this.tsbCadSetores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCadSetores.Click += new System.EventHandler(this.tsbCadSetores_Click);
            // 
            // tabFornecedores
            // 
            this.tabFornecedores.Name = "tabFornecedores";
            // 
            // ribbonControlAdv1.ribbonPanel2
            // 
            this.tabFornecedores.Panel.Controls.Add(this.toolStripEx9);
            this.tabFornecedores.Panel.Controls.Add(this.toolStripEx4);
            this.tabFornecedores.Panel.Name = "ribbonPanel2";
            this.tabFornecedores.Panel.ScrollPosition = 0;
            this.tabFornecedores.Panel.TabIndex = 5;
            this.tabFornecedores.Panel.Text = "Fornecedores e Prestadores de Serviço";
            this.tabFornecedores.Position = 1;
            this.tabFornecedores.Size = new System.Drawing.Size(220, 25);
            this.tabFornecedores.Text = "Fornecedores e Prestadores de Serviço";
            // 
            // toolStripEx9
            // 
            this.toolStripEx9.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx9.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx9.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx9.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx9.Image = null;
            this.toolStripEx9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDossie,
            this.tsbGerarNConformidade});
            this.toolStripEx9.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx9.Name = "toolStripEx9";
            this.toolStripEx9.Office12Mode = false;
            this.toolStripEx9.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx9.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx9.Size = new System.Drawing.Size(99, 127);
            this.toolStripEx9.TabIndex = 1;
            this.toolStripEx9.Text = "Geral";
            // 
            // tsbDossie
            // 
            this.tsbDossie.Image = ((System.Drawing.Image)(resources.GetObject("tsbDossie.Image")));
            this.tsbDossie.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDossie.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDossie.Name = "tsbDossie";
            this.tsbDossie.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbDossie.Size = new System.Drawing.Size(90, 110);
            this.tsbDossie.Text = "Dossiê do\r\nFornecedor";
            this.tsbDossie.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDossie.Click += new System.EventHandler(this.tsbDossie_Click);
            // 
            // tsbGerarNConformidade
            // 
            this.tsbGerarNConformidade.Enabled = false;
            this.tsbGerarNConformidade.Image = ((System.Drawing.Image)(resources.GetObject("tsbGerarNConformidade.Image")));
            this.tsbGerarNConformidade.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbGerarNConformidade.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGerarNConformidade.Name = "tsbGerarNConformidade";
            this.tsbGerarNConformidade.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbGerarNConformidade.Size = new System.Drawing.Size(72, 65);
            this.tsbGerarNConformidade.Text = " ";
            this.tsbGerarNConformidade.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbGerarNConformidade.Visible = false;
            this.tsbGerarNConformidade.Click += new System.EventHandler(this.tsbGerarNConformidade_Click);
            // 
            // toolStripEx4
            // 
            this.toolStripEx4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx4.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx4.Image = null;
            this.toolStripEx4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDocumentos,
            this.tsbTpFornecedor,
            this.tsbFornecedores});
            this.toolStripEx4.Location = new System.Drawing.Point(41, 1);
            this.toolStripEx4.Name = "toolStripEx4";
            this.toolStripEx4.Office12Mode = false;
            this.toolStripEx4.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx4.Size = new System.Drawing.Size(293, 127);
            this.toolStripEx4.TabIndex = 0;
            this.toolStripEx4.Text = "Cadastros";
            // 
            // tsbDocumentos
            // 
            this.tsbDocumentos.Image = ((System.Drawing.Image)(resources.GetObject("tsbDocumentos.Image")));
            this.tsbDocumentos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDocumentos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDocumentos.Name = "tsbDocumentos";
            this.tsbDocumentos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbDocumentos.Size = new System.Drawing.Size(93, 110);
            this.tsbDocumentos.Text = "Cadastro de\r\nDocumento";
            this.tsbDocumentos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDocumentos.Click += new System.EventHandler(this.tsbDocumentos_Click);
            // 
            // tsbTpFornecedor
            // 
            this.tsbTpFornecedor.Image = ((System.Drawing.Image)(resources.GetObject("tsbTpFornecedor.Image")));
            this.tsbTpFornecedor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbTpFornecedor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTpFornecedor.Name = "tsbTpFornecedor";
            this.tsbTpFornecedor.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbTpFornecedor.Size = new System.Drawing.Size(90, 110);
            this.tsbTpFornecedor.Text = "Tipo de\r\nFornecedor";
            this.tsbTpFornecedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbTpFornecedor.Click += new System.EventHandler(this.tsbTpFornecedor_Click);
            // 
            // tsbFornecedores
            // 
            this.tsbFornecedores.Image = ((System.Drawing.Image)(resources.GetObject("tsbFornecedores.Image")));
            this.tsbFornecedores.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbFornecedores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFornecedores.Name = "tsbFornecedores";
            this.tsbFornecedores.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbFornecedores.Size = new System.Drawing.Size(101, 110);
            this.tsbFornecedores.Text = "Cadastro de\r\nFornecedores";
            this.tsbFornecedores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbFornecedores.Click += new System.EventHandler(this.tsbFornecedores_Click);
            // 
            // tabPropostas
            // 
            this.tabPropostas.Name = "tabPropostas";
            // 
            // ribbonControlAdv1.ribbonPanel3
            // 
            this.tabPropostas.Panel.Controls.Add(this.tseGeralProposta);
            this.tabPropostas.Panel.Controls.Add(this.tseGeralProjetos);
            this.tabPropostas.Panel.Controls.Add(this.tseAfericaoCalibracao);
            this.tabPropostas.Panel.Controls.Add(this.tseFVS);
            this.tabPropostas.Panel.Name = "ribbonPanel3";
            this.tabPropostas.Panel.ScrollPosition = 0;
            this.tabPropostas.Panel.TabIndex = 4;
            this.tabPropostas.Panel.Text = "Controles Diversos";
            this.tabPropostas.Position = 2;
            this.tabPropostas.Size = new System.Drawing.Size(119, 25);
            this.tabPropostas.Text = "Controles Diversos";
            // 
            // tseGeralProposta
            // 
            this.tseGeralProposta.Dock = System.Windows.Forms.DockStyle.None;
            this.tseGeralProposta.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tseGeralProposta.ForeColor = System.Drawing.Color.MidnightBlue;
            this.tseGeralProposta.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tseGeralProposta.Image = null;
            this.tseGeralProposta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDashCP,
            this.tsbPropostas,
            this.tsbSetor});
            this.tseGeralProposta.Location = new System.Drawing.Point(0, 1);
            this.tseGeralProposta.Name = "tseGeralProposta";
            this.tseGeralProposta.Office12Mode = false;
            this.tseGeralProposta.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.tseGeralProposta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tseGeralProposta.Size = new System.Drawing.Size(289, 127);
            this.tseGeralProposta.TabIndex = 0;
            this.tseGeralProposta.Text = "Controle de Propostas";
            // 
            // tsbDashCP
            // 
            this.tsbDashCP.Image = ((System.Drawing.Image)(resources.GetObject("tsbDashCP.Image")));
            this.tsbDashCP.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDashCP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDashCP.Name = "tsbDashCP";
            this.tsbDashCP.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbDashCP.Size = new System.Drawing.Size(93, 110);
            this.tsbDashCP.Text = "Dashboards";
            this.tsbDashCP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDashCP.Click += new System.EventHandler(this.tsbDashCP_Click);
            // 
            // tsbPropostas
            // 
            this.tsbPropostas.Image = ((System.Drawing.Image)(resources.GetObject("tsbPropostas.Image")));
            this.tsbPropostas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbPropostas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPropostas.Name = "tsbPropostas";
            this.tsbPropostas.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbPropostas.Size = new System.Drawing.Size(82, 110);
            this.tsbPropostas.Text = "Propostas";
            this.tsbPropostas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPropostas.Click += new System.EventHandler(this.tsbPropostas_Click);
            // 
            // tsbSetor
            // 
            this.tsbSetor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeClientesToolStripMenuItem,
            this.cadastrosDeInteressesToolStripMenuItem,
            this.cadastroDeMetasToolStripMenuItem,
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem,
            this.cadastroDeRespToolStripMenuItem,
            this.cadastroDeSetoresToolStripMenuItem,
            this.cadastroDeStatusToolStripMenuItem});
            this.tsbSetor.Image = ((System.Drawing.Image)(resources.GetObject("tsbSetor.Image")));
            this.tsbSetor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSetor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSetor.Name = "tsbSetor";
            this.tsbSetor.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbSetor.Size = new System.Drawing.Size(105, 110);
            this.tsbSetor.Text = "Cadastro\r\nGerais";
            this.tsbSetor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // cadastroDeClientesToolStripMenuItem
            // 
            this.cadastroDeClientesToolStripMenuItem.Name = "cadastroDeClientesToolStripMenuItem";
            this.cadastroDeClientesToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDeClientesToolStripMenuItem.Text = "Cadastro de Clientes";
            this.cadastroDeClientesToolStripMenuItem.Click += new System.EventHandler(this.tsbClientes_Click);
            // 
            // cadastrosDeInteressesToolStripMenuItem
            // 
            this.cadastrosDeInteressesToolStripMenuItem.Name = "cadastrosDeInteressesToolStripMenuItem";
            this.cadastrosDeInteressesToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastrosDeInteressesToolStripMenuItem.Text = "Cadastro de Interesses";
            this.cadastrosDeInteressesToolStripMenuItem.Click += new System.EventHandler(this.tsbInteresse_Click);
            // 
            // cadastroDeMetasToolStripMenuItem
            // 
            this.cadastroDeMetasToolStripMenuItem.Name = "cadastroDeMetasToolStripMenuItem";
            this.cadastroDeMetasToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDeMetasToolStripMenuItem.Text = "Cadastro de Metas";
            this.cadastroDeMetasToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeMetasToolStripMenuItem_Click);
            // 
            // cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem
            // 
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem.Name = "cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem";
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem.Text = "Cadastro de Perguntas (Análise da Proposta)";
            this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem.Click += new System.EventHandler(this.cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem_Click);
            // 
            // cadastroDeRespToolStripMenuItem
            // 
            this.cadastroDeRespToolStripMenuItem.Name = "cadastroDeRespToolStripMenuItem";
            this.cadastroDeRespToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDeRespToolStripMenuItem.Text = "Cadastro de Responsáveis";
            this.cadastroDeRespToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeRespToolStripMenuItem_Click);
            // 
            // cadastroDeSetoresToolStripMenuItem
            // 
            this.cadastroDeSetoresToolStripMenuItem.Name = "cadastroDeSetoresToolStripMenuItem";
            this.cadastroDeSetoresToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDeSetoresToolStripMenuItem.Text = "Cadastro de Setores";
            this.cadastroDeSetoresToolStripMenuItem.Click += new System.EventHandler(this.tsbSetor_Click);
            // 
            // cadastroDeStatusToolStripMenuItem
            // 
            this.cadastroDeStatusToolStripMenuItem.Name = "cadastroDeStatusToolStripMenuItem";
            this.cadastroDeStatusToolStripMenuItem.Size = new System.Drawing.Size(302, 22);
            this.cadastroDeStatusToolStripMenuItem.Text = "Cadastro de Status";
            this.cadastroDeStatusToolStripMenuItem.Click += new System.EventHandler(this.tsbStatus_Click);
            // 
            // tseGeralProjetos
            // 
            this.tseGeralProjetos.Dock = System.Windows.Forms.DockStyle.None;
            this.tseGeralProjetos.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tseGeralProjetos.ForeColor = System.Drawing.Color.MidnightBlue;
            this.tseGeralProjetos.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tseGeralProjetos.Image = null;
            this.tseGeralProjetos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbProjetos,
            this.tsbSetoresDistribuicao,
            this.tsbAnaliseProjetos});
            this.tseGeralProjetos.Location = new System.Drawing.Point(291, 1);
            this.tseGeralProjetos.Name = "tseGeralProjetos";
            this.tseGeralProjetos.Office12Mode = false;
            this.tseGeralProjetos.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.tseGeralProjetos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tseGeralProjetos.Size = new System.Drawing.Size(307, 127);
            this.tseGeralProjetos.TabIndex = 1;
            this.tseGeralProjetos.Text = "Controle de Projetos";
            // 
            // tsbProjetos
            // 
            this.tsbProjetos.Image = ((System.Drawing.Image)(resources.GetObject("tsbProjetos.Image")));
            this.tsbProjetos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbProjetos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbProjetos.Name = "tsbProjetos";
            this.tsbProjetos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbProjetos.Size = new System.Drawing.Size(73, 110);
            this.tsbProjetos.Text = "Projetos";
            this.tsbProjetos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbProjetos.Click += new System.EventHandler(this.tsbProjetos_Click);
            // 
            // tsbSetoresDistribuicao
            // 
            this.tsbSetoresDistribuicao.Image = ((System.Drawing.Image)(resources.GetObject("tsbSetoresDistribuicao.Image")));
            this.tsbSetoresDistribuicao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSetoresDistribuicao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSetoresDistribuicao.Name = "tsbSetoresDistribuicao";
            this.tsbSetoresDistribuicao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbSetoresDistribuicao.Size = new System.Drawing.Size(93, 110);
            this.tsbSetoresDistribuicao.Text = "Setores de\r\nDistribuição";
            this.tsbSetoresDistribuicao.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSetoresDistribuicao.Click += new System.EventHandler(this.tsbSetoresDistribuicao_Click);
            // 
            // tsbAnaliseProjetos
            // 
            this.tsbAnaliseProjetos.Image = ((System.Drawing.Image)(resources.GetObject("tsbAnaliseProjetos.Image")));
            this.tsbAnaliseProjetos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAnaliseProjetos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAnaliseProjetos.Name = "tsbAnaliseProjetos";
            this.tsbAnaliseProjetos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAnaliseProjetos.Size = new System.Drawing.Size(132, 110);
            this.tsbAnaliseProjetos.Text = "Cad. Perguntas\r\nAnálise de Projetos ";
            this.tsbAnaliseProjetos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAnaliseProjetos.Click += new System.EventHandler(this.tsbAnaliseProjetos_Click);
            // 
            // tseAfericaoCalibracao
            // 
            this.tseAfericaoCalibracao.Dock = System.Windows.Forms.DockStyle.None;
            this.tseAfericaoCalibracao.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tseAfericaoCalibracao.ForeColor = System.Drawing.Color.MidnightBlue;
            this.tseAfericaoCalibracao.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tseAfericaoCalibracao.Image = null;
            this.tseAfericaoCalibracao.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAfericoes,
            this.tsbCalibracao});
            this.tseAfericaoCalibracao.Location = new System.Drawing.Point(600, 1);
            this.tseAfericaoCalibracao.Name = "tseAfericaoCalibracao";
            this.tseAfericaoCalibracao.Office12Mode = false;
            this.tseAfericaoCalibracao.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.tseAfericaoCalibracao.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tseAfericaoCalibracao.Size = new System.Drawing.Size(199, 127);
            this.tseAfericaoCalibracao.TabIndex = 2;
            this.tseAfericaoCalibracao.Text = "Controle de Verificação e Calibração";
            // 
            // tsbAfericoes
            // 
            this.tsbAfericoes.Image = ((System.Drawing.Image)(resources.GetObject("tsbAfericoes.Image")));
            this.tsbAfericoes.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAfericoes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAfericoes.Name = "tsbAfericoes";
            this.tsbAfericoes.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAfericoes.Size = new System.Drawing.Size(95, 110);
            this.tsbAfericoes.Text = "Controle de \r\nVerificação";
            this.tsbAfericoes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAfericoes.Click += new System.EventHandler(this.tsbAfericoes_Click);
            // 
            // tsbCalibracao
            // 
            this.tsbCalibracao.Image = ((System.Drawing.Image)(resources.GetObject("tsbCalibracao.Image")));
            this.tsbCalibracao.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCalibracao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCalibracao.Name = "tsbCalibracao";
            this.tsbCalibracao.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbCalibracao.Size = new System.Drawing.Size(95, 110);
            this.tsbCalibracao.Text = "Controle de \r\nCalibração";
            this.tsbCalibracao.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCalibracao.Click += new System.EventHandler(this.tsbCalibracao_Click);
            // 
            // tseFVS
            // 
            this.tseFVS.AutoSize = false;
            this.tseFVS.Dock = System.Windows.Forms.DockStyle.None;
            this.tseFVS.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.tseFVS.ForeColor = System.Drawing.Color.MidnightBlue;
            this.tseFVS.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tseFVS.Image = null;
            this.tseFVS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbCadastroFVS,
            this.tsbModeloFVS});
            this.tseFVS.Location = new System.Drawing.Point(801, 1);
            this.tseFVS.Name = "tseFVS";
            this.tseFVS.Office12Mode = false;
            this.tseFVS.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.tseFVS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tseFVS.Size = new System.Drawing.Size(193, 127);
            this.tseFVS.TabIndex = 3;
            this.tseFVS.Text = "Ficha de Verificação de Serviços";
            // 
            // tsbCadastroFVS
            // 
            this.tsbCadastroFVS.Image = ((System.Drawing.Image)(resources.GetObject("tsbCadastroFVS.Image")));
            this.tsbCadastroFVS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCadastroFVS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCadastroFVS.Name = "tsbCadastroFVS";
            this.tsbCadastroFVS.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbCadastroFVS.Size = new System.Drawing.Size(80, 110);
            this.tsbCadastroFVS.Text = "Cadastro \r\nde FVS";
            this.tsbCadastroFVS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCadastroFVS.Click += new System.EventHandler(this.tsbCadastroFVS_Click);
            // 
            // tsbModeloFVS
            // 
            this.tsbModeloFVS.Image = ((System.Drawing.Image)(resources.GetObject("tsbModeloFVS.Image")));
            this.tsbModeloFVS.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbModeloFVS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbModeloFVS.Name = "tsbModeloFVS";
            this.tsbModeloFVS.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbModeloFVS.Size = new System.Drawing.Size(74, 110);
            this.tsbModeloFVS.Text = "Modelo \r\nde FVS";
            this.tsbModeloFVS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbModeloFVS.Click += new System.EventHandler(this.tsbModeloFVS_Click);
            // 
            // tabRiscos
            // 
            this.tabRiscos.Name = "tabRiscos";
            // 
            // ribbonControlAdv1.ribbonPanel4
            // 
            this.tabRiscos.Panel.Controls.Add(this.toolStripEx3);
            this.tabRiscos.Panel.Name = "ribbonPanel4";
            this.tabRiscos.Panel.ScrollPosition = 0;
            this.tabRiscos.Panel.TabIndex = 8;
            this.tabRiscos.Panel.Text = "Riscos";
            this.tabRiscos.Position = 3;
            this.tabRiscos.Size = new System.Drawing.Size(55, 25);
            this.tabRiscos.Text = "Riscos";
            this.tabRiscos.Visible = false;
            // 
            // toolStripEx3
            // 
            this.toolStripEx3.AutoSize = false;
            this.toolStripEx3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx3.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx3.Image = null;
            this.toolStripEx3.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx3.Name = "toolStripEx3";
            this.toolStripEx3.Office12Mode = false;
            this.toolStripEx3.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx3.Size = new System.Drawing.Size(39, 127);
            this.toolStripEx3.TabIndex = 0;
            // 
            // tabGlobal
            // 
            this.tabGlobal.Name = "tabGlobal";
            // 
            // ribbonControlAdv1.ribbonPanel5
            // 
            this.tabGlobal.Panel.Controls.Add(this.toolStripEx8);
            this.tabGlobal.Panel.Controls.Add(this.toolStripEx11);
            this.tabGlobal.Panel.Name = "ribbonPanel5";
            this.tabGlobal.Panel.ScrollPosition = 0;
            this.tabGlobal.Panel.TabIndex = 7;
            this.tabGlobal.Panel.Text = "Processos da Qualidade e Globais";
            this.tabGlobal.Position = 4;
            this.tabGlobal.Size = new System.Drawing.Size(196, 25);
            this.tabGlobal.Text = "Processos da Qualidade e Globais";
            // 
            // toolStripEx8
            // 
            this.toolStripEx8.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx8.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx8.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx8.Image = null;
            this.toolStripEx8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbColigadas,
            this.tsbCCusto,
            this.tsbListaUsuarios,
            this.tseEmail,
            this.tsbEnviaEmail});
            this.toolStripEx8.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx8.Name = "toolStripEx8";
            this.toolStripEx8.Office12Mode = false;
            this.toolStripEx8.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx8.Size = new System.Drawing.Size(530, 127);
            this.toolStripEx8.TabIndex = 0;
            this.toolStripEx8.Text = "Cadastros Globais";
            // 
            // tsbColigadas
            // 
            this.tsbColigadas.Image = ((System.Drawing.Image)(resources.GetObject("tsbColigadas.Image")));
            this.tsbColigadas.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbColigadas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbColigadas.Name = "tsbColigadas";
            this.tsbColigadas.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbColigadas.Size = new System.Drawing.Size(93, 110);
            this.tsbColigadas.Text = "Cadastro de\r\nColigadas";
            this.tsbColigadas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbColigadas.Click += new System.EventHandler(this.tsbColigadas_Click);
            // 
            // tsbCCusto
            // 
            this.tsbCCusto.Image = ((System.Drawing.Image)(resources.GetObject("tsbCCusto.Image")));
            this.tsbCCusto.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbCCusto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCCusto.Name = "tsbCCusto";
            this.tsbCCusto.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbCCusto.Size = new System.Drawing.Size(120, 110);
            this.tsbCCusto.Text = "Cadastro de\r\nCentro de Custos";
            this.tsbCCusto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbCCusto.Click += new System.EventHandler(this.tsbCCusto_Click);
            // 
            // tsbListaUsuarios
            // 
            this.tsbListaUsuarios.Image = ((System.Drawing.Image)(resources.GetObject("tsbListaUsuarios.Image")));
            this.tsbListaUsuarios.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbListaUsuarios.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbListaUsuarios.Name = "tsbListaUsuarios";
            this.tsbListaUsuarios.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbListaUsuarios.Size = new System.Drawing.Size(92, 110);
            this.tsbListaUsuarios.Text = "Cadasto de \r\nUsuários";
            this.tsbListaUsuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbListaUsuarios.Click += new System.EventHandler(this.tsbListaUsuarios_Click);
            // 
            // tseEmail
            // 
            this.tseEmail.Image = ((System.Drawing.Image)(resources.GetObject("tseEmail.Image")));
            this.tseEmail.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tseEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tseEmail.Name = "tseEmail";
            this.tseEmail.Size = new System.Drawing.Size(102, 110);
            this.tseEmail.Text = "Configurações de\r\nE-mail";
            this.tseEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tseEmail.Click += new System.EventHandler(this.tseEmail_Click);
            // 
            // tsbEnviaEmail
            // 
            this.tsbEnviaEmail.Image = global::QUALIDADE.Properties.Resources.mail_send;
            this.tsbEnviaEmail.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbEnviaEmail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbEnviaEmail.Name = "tsbEnviaEmail";
            this.tsbEnviaEmail.Size = new System.Drawing.Size(114, 110);
            this.tsbEnviaEmail.Text = "Alertas Documentos\r\nPendentes";
            this.tsbEnviaEmail.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbEnviaEmail.ToolTipText = "Enviar e-mails\r\n de alerta sobre Documentação Pendente";
            this.tsbEnviaEmail.Click += new System.EventHandler(this.tsbEnviaEmail_Click);
            // 
            // toolStripEx11
            // 
            this.toolStripEx11.AutoSize = false;
            this.toolStripEx11.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx11.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx11.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx11.Image = null;
            this.toolStripEx11.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel2,
            this.tsbDocEletronica});
            this.toolStripEx11.Location = new System.Drawing.Point(532, 1);
            this.toolStripEx11.Name = "toolStripEx11";
            this.toolStripEx11.Office12Mode = false;
            this.toolStripEx11.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx11.Size = new System.Drawing.Size(196, 127);
            this.toolStripEx11.TabIndex = 1;
            this.toolStripEx11.Text = "Documentação da Qualidade";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(43, 110);
            this.toolStripLabel2.Text = "            ";
            // 
            // tsbDocEletronica
            // 
            this.tsbDocEletronica.Image = ((System.Drawing.Image)(resources.GetObject("tsbDocEletronica.Image")));
            this.tsbDocEletronica.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbDocEletronica.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDocEletronica.Name = "tsbDocEletronica";
            this.tsbDocEletronica.Size = new System.Drawing.Size(88, 110);
            this.tsbDocEletronica.Text = "Documentação\r\nEletrônica";
            this.tsbDocEletronica.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbDocEletronica.Click += new System.EventHandler(this.tsbGED_Click);
            // 
            // tabConfiguracoes
            // 
            this.tabConfiguracoes.Name = "tabConfiguracoes";
            // 
            // ribbonControlAdv1.ribbonPanel6
            // 
            this.tabConfiguracoes.Panel.Controls.Add(this.toolStripEx5);
            this.tabConfiguracoes.Panel.Controls.Add(this.toolStripEx7);
            this.tabConfiguracoes.Panel.Name = "ribbonPanel6";
            this.tabConfiguracoes.Panel.ScrollPosition = 0;
            this.tabConfiguracoes.Panel.TabIndex = 6;
            this.tabConfiguracoes.Panel.Text = "Configurações";
            this.tabConfiguracoes.Position = 5;
            this.tabConfiguracoes.Size = new System.Drawing.Size(98, 25);
            this.tabConfiguracoes.Text = "Configurações";
            // 
            // toolStripEx5
            // 
            this.toolStripEx5.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx5.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx5.Image = null;
            this.toolStripEx5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAlterarColigada,
            this.tsbAtualizarBD,
            this.tsbBancoDados,
            this.tsbImportarArquivos,
            this.tsbExportarArquivos});
            this.toolStripEx5.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx5.Name = "toolStripEx5";
            this.toolStripEx5.Office12Mode = false;
            this.toolStripEx5.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx5.Size = new System.Drawing.Size(303, 127);
            this.toolStripEx5.TabIndex = 3;
            this.toolStripEx5.Text = "Configurações";
            // 
            // tsbAlterarColigada
            // 
            this.tsbAlterarColigada.Image = ((System.Drawing.Image)(resources.GetObject("tsbAlterarColigada.Image")));
            this.tsbAlterarColigada.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAlterarColigada.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAlterarColigada.Name = "tsbAlterarColigada";
            this.tsbAlterarColigada.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAlterarColigada.Size = new System.Drawing.Size(77, 110);
            this.tsbAlterarColigada.Text = "Alterar \r\nColigada";
            this.tsbAlterarColigada.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAlterarColigada.Click += new System.EventHandler(this.tsbAlterarColigada_Click);
            // 
            // tsbAtualizarBD
            // 
            this.tsbAtualizarBD.Image = ((System.Drawing.Image)(resources.GetObject("tsbAtualizarBD.Image")));
            this.tsbAtualizarBD.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAtualizarBD.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAtualizarBD.Name = "tsbAtualizarBD";
            this.tsbAtualizarBD.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAtualizarBD.Size = new System.Drawing.Size(114, 110);
            this.tsbAtualizarBD.Text = "Sincronizar\r\nBanco de Dados";
            this.tsbAtualizarBD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAtualizarBD.Click += new System.EventHandler(this.tsbAtualizarBD_Click);
            // 
            // tsbBancoDados
            // 
            this.tsbBancoDados.Image = ((System.Drawing.Image)(resources.GetObject("tsbBancoDados.Image")));
            this.tsbBancoDados.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbBancoDados.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBancoDados.Name = "tsbBancoDados";
            this.tsbBancoDados.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbBancoDados.Size = new System.Drawing.Size(103, 110);
            this.tsbBancoDados.Text = "Config. Banco\r\nde Dados";
            this.tsbBancoDados.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbBancoDados.Click += new System.EventHandler(this.tsbBancoDados_Click);
            // 
            // tsbImportarArquivos
            // 
            this.tsbImportarArquivos.Enabled = false;
            this.tsbImportarArquivos.Image = ((System.Drawing.Image)(resources.GetObject("tsbImportarArquivos.Image")));
            this.tsbImportarArquivos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbImportarArquivos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbImportarArquivos.Name = "tsbImportarArquivos";
            this.tsbImportarArquivos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbImportarArquivos.Size = new System.Drawing.Size(88, 94);
            this.tsbImportarArquivos.Text = "Importar \r\nArquivos";
            this.tsbImportarArquivos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbImportarArquivos.Visible = false;
            this.tsbImportarArquivos.Click += new System.EventHandler(this.tsbImportarArquivos_Click);
            // 
            // tsbExportarArquivos
            // 
            this.tsbExportarArquivos.Enabled = false;
            this.tsbExportarArquivos.Image = ((System.Drawing.Image)(resources.GetObject("tsbExportarArquivos.Image")));
            this.tsbExportarArquivos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbExportarArquivos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExportarArquivos.Name = "tsbExportarArquivos";
            this.tsbExportarArquivos.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbExportarArquivos.Size = new System.Drawing.Size(88, 94);
            this.tsbExportarArquivos.Text = "Exportar\r\nArquivos";
            this.tsbExportarArquivos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbExportarArquivos.Visible = false;
            this.tsbExportarArquivos.Click += new System.EventHandler(this.tsbExportarArquivos_Click);
            // 
            // toolStripEx7
            // 
            this.toolStripEx7.AutoSize = false;
            this.toolStripEx7.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx7.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx7.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx7.Image = null;
            this.toolStripEx7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSairSistema,
            this.tsbSobre,
            this.tsbAlterarUsr});
            this.toolStripEx7.Location = new System.Drawing.Point(41, 1);
            this.toolStripEx7.Name = "toolStripEx7";
            this.toolStripEx7.Office12Mode = false;
            this.toolStripEx7.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripEx7.Size = new System.Drawing.Size(286, 127);
            this.toolStripEx7.TabIndex = 2;
            this.toolStripEx7.Text = "Sobre o Sistema e Controle de Acesso";
            // 
            // tsbSairSistema
            // 
            this.tsbSairSistema.Image = ((System.Drawing.Image)(resources.GetObject("tsbSairSistema.Image")));
            this.tsbSairSistema.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSairSistema.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSairSistema.Name = "tsbSairSistema";
            this.tsbSairSistema.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbSairSistema.Size = new System.Drawing.Size(72, 110);
            this.tsbSairSistema.Text = "Sair do \r\nSistema";
            this.tsbSairSistema.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSairSistema.Click += new System.EventHandler(this.tsbSairSistema_Click);
            // 
            // tsbSobre
            // 
            this.tsbSobre.Image = ((System.Drawing.Image)(resources.GetObject("tsbSobre.Image")));
            this.tsbSobre.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbSobre.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSobre.Name = "tsbSobre";
            this.tsbSobre.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.tsbSobre.Size = new System.Drawing.Size(89, 110);
            this.tsbSobre.Text = "Sobre o \r\nSistema";
            this.tsbSobre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbSobre.Click += new System.EventHandler(this.tsbSobre_Click);
            // 
            // tsbAlterarUsr
            // 
            this.tsbAlterarUsr.Image = ((System.Drawing.Image)(resources.GetObject("tsbAlterarUsr.Image")));
            this.tsbAlterarUsr.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbAlterarUsr.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAlterarUsr.Name = "tsbAlterarUsr";
            this.tsbAlterarUsr.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.tsbAlterarUsr.Size = new System.Drawing.Size(72, 110);
            this.tsbAlterarUsr.Text = "Alterar\r\nUsuário";
            this.tsbAlterarUsr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAlterarUsr.Click += new System.EventHandler(this.tsbAlterarUsr_Click);
            // 
            // tabProjetos
            // 
            this.tabProjetos.Name = "tabProjetos";
            // 
            // 
            // 
            this.tabProjetos.Panel.Name = "ribbonPanel7";
            this.tabProjetos.Panel.ScrollPosition = 0;
            this.tabProjetos.Panel.TabIndex = 9;
            this.tabProjetos.Panel.Text = "Controle de Projetos";
            this.tabProjetos.Position = -1;
            this.tabProjetos.Size = new System.Drawing.Size(129, 25);
            this.tabProjetos.Text = "Controle de Projetos";
            // 
            // toolStripTabItem2
            // 
            this.toolStripTabItem2.Name = "toolStripTabItem2";
            // 
            // 
            // 
            this.toolStripTabItem2.Panel.Controls.Add(this.toolStripEx2);
            this.toolStripTabItem2.Panel.Name = "ribbonPanel2";
            this.toolStripTabItem2.Panel.ScrollPosition = 0;
            this.toolStripTabItem2.Panel.TabIndex = 3;
            this.toolStripTabItem2.Panel.Text = "Configurações";
            this.toolStripTabItem2.Position = -1;
            this.toolStripTabItem2.Size = new System.Drawing.Size(98, 25);
            this.toolStripTabItem2.Text = "Configurações";
            // 
            // toolStripEx2
            // 
            this.toolStripEx2.AutoSize = false;
            this.toolStripEx2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripEx2.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStripEx2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.toolStripEx2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripEx2.Image = null;
            this.toolStripEx2.Location = new System.Drawing.Point(0, 1);
            this.toolStripEx2.Name = "toolStripEx2";
            this.toolStripEx2.Office12Mode = false;
            this.toolStripEx2.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.toolStripEx2.Size = new System.Drawing.Size(338, 127);
            this.toolStripEx2.TabIndex = 0;
            this.toolStripEx2.Text = "Usuários e Permissões";
            // 
            // tabbedMDIManager1
            // 
            this.tabbedMDIManager1.AttachedTo = this;
            this.tabbedMDIManager1.CloseButtonBackColor = System.Drawing.Color.White;
            this.tabbedMDIManager1.CloseButtonColor = System.Drawing.Color.White;
            this.tabbedMDIManager1.CloseButtonToolTip = "";
            this.tabbedMDIManager1.CloseOnMiddleButtonClick = true;
            this.tabbedMDIManager1.DropDownButtonToolTip = "";
            this.tabbedMDIManager1.ImageSize = new System.Drawing.Size(16, 16);
            this.tabbedMDIManager1.NeedUpdateHostedForm = false;
            this.tabbedMDIManager1.ShowCloseButton = true;
            this.tabbedMDIManager1.TabBackColor = System.Drawing.Color.DarkRed;
            this.tabbedMDIManager1.TabForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tabbedMDIManager1.TabStyle = typeof(Syncfusion.Windows.Forms.Tools.TabRendererOffice2016Colorful);
            this.tabbedMDIManager1.ThemeName = "TabRendererOffice2016Colorful";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblSistema,
            this.lblDataSistema,
            this.lblHoraSistema,
            this.lblLoginUsuario,
            this.lblColigada});
            this.statusStrip1.Location = new System.Drawing.Point(2, 559);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1086, 24);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblSistema
            // 
            this.lblSistema.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.lblSistema.Name = "lblSistema";
            this.lblSistema.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblSistema.Size = new System.Drawing.Size(279, 19);
            this.lblSistema.Text = "SGQ - Sistema de Gerenciamento de Qualidade";
            // 
            // lblDataSistema
            // 
            this.lblDataSistema.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.lblDataSistema.Name = "lblDataSistema";
            this.lblDataSistema.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblDataSistema.Size = new System.Drawing.Size(203, 19);
            this.lblDataSistema.Text = "Sexta-Feira, 20 de Março de 2020";
            // 
            // lblHoraSistema
            // 
            this.lblHoraSistema.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
            this.lblHoraSistema.Name = "lblHoraSistema";
            this.lblHoraSistema.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblHoraSistema.Size = new System.Drawing.Size(73, 19);
            this.lblHoraSistema.Text = "00:00:00";
            // 
            // lblLoginUsuario
            // 
            this.lblLoginUsuario.Name = "lblLoginUsuario";
            this.lblLoginUsuario.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblLoginUsuario.Size = new System.Drawing.Size(100, 19);
            this.lblLoginUsuario.Text = "Logado com: ";
            // 
            // lblColigada
            // 
            this.lblColigada.Name = "lblColigada";
            this.lblColigada.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblColigada.Size = new System.Drawing.Size(128, 19);
            this.lblColigada.Text = "0 - Coligada Global";
            // 
            // timerDataHora
            // 
            this.timerDataHora.Tick += new System.EventHandler(this.timerDataHora_Tick);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1090, 584);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.ribbonControlAdv1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FormPrincipal";
            this.Padding = new System.Windows.Forms.Padding(1, 0, 1, 1);
            this.ShowApplicationIcon = false;
            this.Text = "SGI - Sistema de Gestão Integrada";
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControlAdv1)).EndInit();
            this.ribbonControlAdv1.ResumeLayout(false);
            this.ribbonControlAdv1.PerformLayout();
            this.tabNaoConformidade.Panel.ResumeLayout(false);
            this.tabNaoConformidade.Panel.PerformLayout();
            this.toolStripEx6.ResumeLayout(false);
            this.toolStripEx6.PerformLayout();
            this.toolStripEx1.ResumeLayout(false);
            this.toolStripEx1.PerformLayout();
            this.tabFornecedores.Panel.ResumeLayout(false);
            this.tabFornecedores.Panel.PerformLayout();
            this.toolStripEx9.ResumeLayout(false);
            this.toolStripEx9.PerformLayout();
            this.toolStripEx4.ResumeLayout(false);
            this.toolStripEx4.PerformLayout();
            this.tabPropostas.Panel.ResumeLayout(false);
            this.tabPropostas.Panel.PerformLayout();
            this.tseGeralProposta.ResumeLayout(false);
            this.tseGeralProposta.PerformLayout();
            this.tseGeralProjetos.ResumeLayout(false);
            this.tseGeralProjetos.PerformLayout();
            this.tseAfericaoCalibracao.ResumeLayout(false);
            this.tseAfericaoCalibracao.PerformLayout();
            this.tseFVS.ResumeLayout(false);
            this.tseFVS.PerformLayout();
            this.tabRiscos.Panel.ResumeLayout(false);
            this.tabGlobal.Panel.ResumeLayout(false);
            this.tabGlobal.Panel.PerformLayout();
            this.toolStripEx8.ResumeLayout(false);
            this.toolStripEx8.PerformLayout();
            this.toolStripEx11.ResumeLayout(false);
            this.toolStripEx11.PerformLayout();
            this.tabConfiguracoes.Panel.ResumeLayout(false);
            this.tabConfiguracoes.Panel.PerformLayout();
            this.toolStripEx5.ResumeLayout(false);
            this.toolStripEx5.PerformLayout();
            this.toolStripEx7.ResumeLayout(false);
            this.toolStripEx7.PerformLayout();
            this.toolStripTabItem2.Panel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.RibbonControlAdv ribbonControlAdv1;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabNaoConformidade;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx1;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem toolStripTabItem2;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx2;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabPropostas;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx tseGeralProposta;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabFornecedores;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx4;
        private Syncfusion.Windows.Forms.Tools.TabbedMDIManager tabbedMDIManager1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblSistema;
        private System.Windows.Forms.ToolStripStatusLabel lblDataSistema;
        private System.Windows.Forms.ToolStripStatusLabel lblHoraSistema;
        private System.Windows.Forms.ToolStripStatusLabel lblLoginUsuario;
        private System.Windows.Forms.Timer timerDataHora;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabConfiguracoes;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx5;
        private System.Windows.Forms.ToolStripButton tsbAlterarColigada;
        private System.Windows.Forms.ToolStripButton tsbAtualizarBD;
        private System.Windows.Forms.ToolStripButton tsbBancoDados;
        private System.Windows.Forms.ToolStripButton tsbImportarArquivos;
        private System.Windows.Forms.ToolStripButton tsbExportarArquivos;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx7;
        private System.Windows.Forms.ToolStripButton tsbAlterarUsr;
        private System.Windows.Forms.ToolStripButton tsbSairSistema;
        private System.Windows.Forms.ToolStripButton tsbNaoConformidade;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabGlobal;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx8;
        private System.Windows.Forms.ToolStripButton tsbColigadas;
        private System.Windows.Forms.ToolStripButton tsbCCusto;
        private System.Windows.Forms.ToolStripButton tsbListaUsuarios;
        private System.Windows.Forms.ToolStripButton tsbExtratificacao;
        private System.Windows.Forms.ToolStripButton tsbSobre;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx6;
        private System.Windows.Forms.ToolStripButton tsbPlanoAcao;
        private System.Windows.Forms.ToolStripButton tsbEficaciaAcao;
        private System.Windows.Forms.ToolStripStatusLabel lblColigada;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx9;
        private System.Windows.Forms.ToolStripButton tsbDossie;
        private System.Windows.Forms.ToolStripButton tsbGerarNConformidade;
        private System.Windows.Forms.ToolStripButton tsbFornecedores;
        private System.Windows.Forms.ToolStripButton tsbDocumentos;
        private System.Windows.Forms.ToolStripButton tsbDashCP;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabRiscos;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx3;
        private System.Windows.Forms.ToolStripButton tsbPropostas;
        private System.Windows.Forms.ToolStripSplitButton tsbSetor;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeClientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrosDeInteressesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeSetoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbTpFornecedor;
        private System.Windows.Forms.ToolStripButton tsbTipoRegistro;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeMetasToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbCadSetores;
        private Syncfusion.Windows.Forms.Tools.ToolStripTabItem tabProjetos;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx tseGeralProjetos;
        private System.Windows.Forms.ToolStripButton tsbProjetos;
        private System.Windows.Forms.ToolStripButton tsbSetoresDistribuicao;
        private System.Windows.Forms.ToolStripMenuItem cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton tsbAnaliseProjetos;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx tseAfericaoCalibracao;
        private System.Windows.Forms.ToolStripButton tsbAfericoes;
        private System.Windows.Forms.ToolStripButton tsbCalibracao;
        private System.Windows.Forms.ToolStripButton tseEmail;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx10;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbGED;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx toolStripEx11;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton tsbDocEletronica;
        private Syncfusion.Windows.Forms.Tools.ToolStripEx tseFVS;
        private System.Windows.Forms.ToolStripButton tsbCadastroFVS;
        private System.Windows.Forms.ToolStripButton tsbModeloFVS;
        private System.Windows.Forms.ToolStripButton tsbEnviaEmail;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeRespToolStripMenuItem;
    }
}