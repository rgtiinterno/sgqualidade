﻿using Apollo.Updater;
using CygnusClient;
using CygnusClient.Models;
using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace QUALIDADE.Forms
{
    public partial class FormLogin : Form
    {
        private bool restart;
        private ApolloUpdater updater;
        private SistemaCliente _configConexao;
        private InicializaClient _clientLicense;

        public FormLogin()
        {
            InitializeComponent();
            _clientLicense = new InicializaClient();
            updater = new ApolloUpdater(new UpdateInfo
            {
                SystemName = Assembly.GetExecutingAssembly().GetName().Name,
                CurrentVersion = Assembly.GetExecutingAssembly().GetName().Version,
                ApplicationAssembly = Assembly.GetExecutingAssembly(),
                UpdateXmlLocation = new Uri("https://www.rgtitec.com.br/systems/updates/Qualidade/update.xml"),
            });
            updater.DoUpdate();
            txtUsuario.Text = Arquivo.LeLogin();
            if (VerificaConexao())
                AtualizaBase();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            _configConexao = new SistemaCliente
            {
                IdSistema = 33,
                NomeSistema = "SGI Qualidade",
                FormRaizProjeto = this
            };
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AtualizaBase()
        {
            using (ControleGUsuario controle = new ControleGUsuario())
            {
                controle.CriaColuna("AAFERICAOINTERNA", "TIPOAFERICAO", "VARCHAR(5) NOT NULL DEFAULT '000'");
                controle.CriaColuna("AAFERICAOINTERNA", "OBSERVACOES", "TEXT NULL");
                controle.CriaColuna("AAFERICAOINTERNA", "EQUIPFUNCIONANDO", "BIT NOT NULL DEFAULT 0");
                controle.CriaColuna("AAFERICAOINTERNA", "EQUIPLIMPO", "BIT NOT NULL DEFAULT 0");
                controle.CriaColuna("AAFERICAOINTERNA", "EQUIPBEMACOND", "BIT NOT NULL DEFAULT 0");
                controle.CriaColuna("AAFERICAOINTERNA", "EQUIPDANOAPARENTE", "BIT NOT NULL DEFAULT 0");
                controle.CriaColuna("AAFERICAOINTERNA", "FORM069BOLHACENTRALIZADA", "BIT NOT NULL DEFAULT 0");
                controle.CriaColuna("AAFERICAOINTERNA", "FORM069LOCALNIVELADO", "BIT NOT NULL DEFAULT 0");
                controle.CriaTabela
                (
                    "AAFERICAOINT068LEITURA",
                    @"CODIGO INT NOT NULL, 
	                CODAFERICAO INT NOT NULL, 
	                CODCOLIGADA INT NOT NULL, 
	                CODCFO VARCHAR(25) NOT NULL, 
	                REV INT NOT NULL, 
	                LEITURALADO01 VARCHAR(50) NOT NULL, 
	                LEITURALADO02 VARCHAR(50) NOT NULL, 
	                LEITURAHIPOTENUSA VARCHAR(50) NULL, 
	                DESVIOTOLERAVEL VARCHAR(50) NOT NULL, 
	                RESULTADO SMALLINT NOT NULL, -- 0 - NÃO CONFORME | 1 - CONFORME
	                OBSERVACOES TEXT NULL, 
	                CRIADOPOR VARCHAR(50) NULL, 
	                CRIADOEM DATETIME NULL, 
	                MODIFICADOPOR VARCHAR(50) NULL, 
	                MODIFICADOEM DATETIME NULL, 
	                CONSTRAINT PK_AAFINT068LEITURA PRIMARY KEY(CODIGO, CODAFERICAO, CODCOLIGADA, CODCFO, REV), 
	                CONSTRAINT FK_AAFINT068LEITURA_AFER FOREIGN KEY(CODAFERICAO, CODCOLIGADA, CODCFO, REV) REFERENCES AAFERICAOINTERNA(CODIGO, CODCOLIGADA, CODCFO, REV) ON DELETE CASCADE"
                );
                controle.CriaTabela
                (
                    "AAFERICAOINT070LEITURA",
                    @"CODIGO INT NOT NULL, 
	                CODAFERICAO INT NOT NULL, 
	                CODCOLIGADA INT NOT NULL, 
	                CODCFO VARCHAR(25) NOT NULL, 
	                REV INT NOT NULL, 
	                LEITURAPADRAO VARCHAR(50) NOT NULL, 
	                LEITURADISPOSITIVO VARCHAR(50) NOT NULL, 
	                DESVIOENCONTRADO VARCHAR(50) NULL, 
	                DESVIOTOLERAVEL VARCHAR(50) NOT NULL, 
	                RESULTADO SMALLINT NOT NULL, -- 0 - NÃO CONFORME | 1 - CONFORME
	                OBSERVACOES TEXT NULL, 
	                CRIADOPOR VARCHAR(50) NULL, 
	                CRIADOEM DATETIME NULL, 
	                MODIFICADOPOR VARCHAR(50) NULL, 
	                MODIFICADOEM DATETIME NULL, 
	                CONSTRAINT PK_AAFINT070LEITURA PRIMARY KEY(CODIGO, CODAFERICAO, CODCOLIGADA, CODCFO, REV), 
	                CONSTRAINT FK_AAFINT070LEITURA_AFER FOREIGN KEY(CODAFERICAO, CODCOLIGADA, CODCFO, REV) REFERENCES AAFERICAOINTERNA(CODIGO, CODCOLIGADA, CODCFO, REV) ON DELETE CASCADE"
                );
            }
        }

        private void btnAcessar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (VerificaConexao())
            {
                ControleGUsuario controle = new ControleGUsuario();
                string msg = null;
                GUSUARIO usuario = controle.Login(txtUsuario.Text, txtSenha.Text, ref msg);
                if (msg == null && usuario != null)
                {
                    controle.Dispose();
                    Cursor.Current = Cursors.Default;
                    using (FormPrincipal frm = new FormPrincipal(usuario))
                    {
                        bool conectado = _clientLicense.Conectar
                        (
                            _configConexao,
                            out string msgErro,
                            loginUsuario: txtUsuario.Text,
                            nomeUsuario: txtUsuario.Text
                        );

                        if (conectado)
                        {
                            Arquivo.GravaLogin(txtUsuario.Text);
                            this.Visible = false;
                            frm.WindowState = FormWindowState.Maximized;
                            frm.StartPosition = FormStartPosition.CenterScreen;
                            frm.ShowDialog();
                            restart = frm.restart;

                            if (restart)
                                Application.Restart();
                            else
                                Application.Exit();
                        }
                        else
                        {
                            lblMsg.ForeColor = Color.Red;
                            lblMsg.Text = msgErro;
                            lblMsg.Visible = true;
                        }
                    }
                }
                else
                {
                    lblMsg.ForeColor = Color.Red;
                    lblMsg.Text = msg;
                    txtSenha.Text = string.Empty;
                    txtUsuario.Focus();
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void btnConfigDB_Click(object sender, EventArgs e)
        {
            using (FormConfigConexao frm = new FormConfigConexao(false))
            {
                frm.WindowState = FormWindowState.Normal;
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.ShowDialog();
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    btnAcessar_Click(e, e);
                    break;
            }
        }

        private bool VerificaConexao()
        {
            bool resultado = false;
            if (!File.Exists(Properties.Settings.Default.conexaoPrincipal))
            {
                MessageBox.Show("Arquivo de conexão principal não encontrado.\n" +
                    "Por favor, configure-os para continuar.", "Alerta",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                btnConfigDB.PerformClick();
            }
            else
            {
                if (!Global.TestarConexao(SqlConn.getStringConexao()))
                {
                    MessageBox.Show("Falha na conexão com o banco de dados principal.\n" +
                        "Por favor, confira os dados da conexão.", "Alerta",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnConfigDB.PerformClick();
                }
                else
                {
                    if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
                    {
                        if (Global.TestarConexao(SqlConn.getStringIntegracao()))
                        {
                            resultado = true;
                        }
                        else
                        {
                            resultado = false;
                            MessageBox.Show("Falha na conexão com o banco de dados de integração.\n" +
                                "Por favor, confira os dados da conexão.", "Alerta",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            btnConfigDB.PerformClick();
                        }
                    }
                    else
                    {
                        resultado = true;
                    }
                }
            }

            return resultado;
        }

        private void FormLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_clientLicense != null)
                _clientLicense.Dispose();

            if (this != null)
                this.Dispose();
        }

        private void FormLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                if (_clientLicense.AbreFormConfig())
                    Application.Restart();
            }
        }
    }
}