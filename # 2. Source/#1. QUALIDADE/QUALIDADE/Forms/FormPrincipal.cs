﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Cadastros.CentroCustos;
using QUALIDADE.Forms.Cadastros.ClienteFornecedor;
using QUALIDADE.Forms.Cadastros.Documentos.Dossie;
using QUALIDADE.Forms.Cadastros.Documentos.TiposDocumentos;
using QUALIDADE.Forms.Cadastros.Empresas;
using QUALIDADE.Forms.Cadastros.Extratificacoes;
using QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Afericao;
using QUALIDADE.Forms.Cadastros.Modulo_AfericaoCalibracao.Calibracao;
using QUALIDADE.Forms.Cadastros.Modulo_FVS.FVS;
using QUALIDADE.Forms.Cadastros.Modulo_FVS.Modelo;
using QUALIDADE.Forms.Cadastros.Modulo_GED;
using QUALIDADE.Forms.Cadastros.ModuloCF.TipoFornecedor;
using QUALIDADE.Forms.Cadastros.ModuloCP.Analise;
using QUALIDADE.Forms.Cadastros.ModuloCP.Clientes;
using QUALIDADE.Forms.Cadastros.ModuloCP.Dashboard;
using QUALIDADE.Forms.Cadastros.ModuloCP.Interesses;
using QUALIDADE.Forms.Cadastros.ModuloCP.Metas;
using QUALIDADE.Forms.Cadastros.ModuloCP.Propostas;
using QUALIDADE.Forms.Cadastros.ModuloCP.Setores;
using QUALIDADE.Forms.Cadastros.ModuloCP.Status;
using QUALIDADE.Forms.Cadastros.ModuloCPJ.Analise;
using QUALIDADE.Forms.Cadastros.ModuloCPJ.Projetos;
using QUALIDADE.Forms.Cadastros.ModuloCPJ.SetoresDistribidos;
using QUALIDADE.Forms.Cadastros.ModuloNC.Registros;
using QUALIDADE.Forms.Cadastros.NaoConformidade;
using QUALIDADE.Forms.Cadastros.Setores;
using QUALIDADE.Forms.Cadastros.Usuarios;
using QUALIDADE.Forms.Cadastros.Verificacoes.NaoConformidade;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Windows.Forms.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms
{
    public partial class FormPrincipal : RibbonForm
    {
        private static GUSUARIO usuarioAcesso;
        public bool restart = false;

        public FormPrincipal(GUSUARIO usr)
        {
            InitializeComponent();
            usuarioAcesso = usr;
        }

        public static GUSUARIO getUsuarioAcesso()
        {
            if (string.IsNullOrEmpty(usuarioAcesso.EMAIL) || string.IsNullOrEmpty(usuarioAcesso.HOST) || string.IsNullOrEmpty(usuarioAcesso.SENHAEMAIL))
            {

            }
            return usuarioAcesso;
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            lblSistema.Text = this.Text;
            lblDataSistema.Text = DateTime.Now.ToString("dddd, d' de 'MMMM' de 'yyyy");
            lblLoginUsuario.Text += usuarioAcesso.NOME + " - ( " + usuarioAcesso.LOGIN + " )";
            timerDataHora.Start();
            PermissaoAcesso();
            PermissoesAdministrativas();

            if (usuarioAcesso.ALTERARSENHA)
            {
                Global.MsgInformacao(
                    "Usuário com a senha temporária, " +
                    "é necessário a alteração de senha."
                );

                using (FormAlterarSenha frm = new FormAlterarSenha())
                {
                    frm.StartPosition = FormStartPosition.CenterParent;
                    frm.WindowState = FormWindowState.Normal;
                    frm.ShowDialog();

                    if (frm.salvo)
                    {
                        Global.MsgInformacao("Para atualizar as informações, o sistema será reiniciado.");
                        tsbAlterarUsr.PerformClick();
                    }
                }
            }

            try
            {
                using (ControleGEmpresas controle = new ControleGEmpresas())
                {
                    lblColigada.Text = $"{usuarioAcesso.CODCOLIGADA} - {controle.Find(usuarioAcesso.CODCOLIGADA)?.NOME}";
                }
            }
            catch
            {
                lblColigada.Text = "";
            }
            VerificaColigadas();
        }

        private void timerDataHora_Tick(object sender, EventArgs e)
        {
            lblHoraSistema.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private void VerificaColigadas()
        {
            using (ControleGEmpresas controle = new ControleGEmpresas())
            {
                if (controle.GetAll().ToList().Count < 1)
                {
                    DialogResult dr = MessageBox.Show("O sistema não encontrou nenhuma coligada cadastrada.\n" +
                        "Deseja cadastrar uma nova coligada?", "Paramêtros Iniciais",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (dr == DialogResult.Yes)
                        tsbColigadas.PerformClick();
                    else
                    {
                        if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
                        {
                            DialogResult drIntegracao = MessageBox.Show("O sistema encontrou uma integração cadastrada.\n" +
                           "Deseja sincronizar os bancos de dados?", "Paramêtros Iniciais",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if (drIntegracao == DialogResult.Yes)
                                tsbAtualizarBD.PerformClick();
                            else
                                tsbSairSistema.PerformClick();
                        }
                        else
                            tsbSairSistema.PerformClick();
                    }
                }
            }
        }

        #region: "Não Conformidade"
        private void tsbPlanoAcao_Click(object sender, EventArgs e)
        {
            FormListaVNConformidade frm = new FormListaVNConformidade("PA");
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbEficaciaAcao_Click(object sender, EventArgs e)
        {
            FormListaVNConformidade frm = new FormListaVNConformidade("EA");
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbNaoConformidade_Click(object sender, EventArgs e)
        {
            FormListaNConformidade frm = new FormListaNConformidade();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbExtratificacao_Click(object sender, EventArgs e)
        {
            FormListaExtratificacao frm = new FormListaExtratificacao();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbTipoRegistro_Click(object sender, EventArgs e)
        {
            FormListaRegistros frm = new FormListaRegistros();
            frm.MdiParent = this;
            frm.Show();
        }


        private void tsbCadSetores_Click(object sender, EventArgs e)
        {
            FormListaSetoresGlobais frm = new FormListaSetoresGlobais();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Cliente / Fornecedor"
        private void tsbDossie_Click(object sender, EventArgs e)
        {
            FormListaDossie frm = new FormListaDossie();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbGerarNConformidade_Click(object sender, EventArgs e)
        {
            // EXECUTAR FUNÇÃO AQUI
        }

        private void tsbFornecedores_Click(object sender, EventArgs e)
        {
            FormListaClientes frm = new FormListaClientes();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbTpFornecedor_Click(object sender, EventArgs e)
        {
            FormListaTpFornecedor frm = new FormListaTpFornecedor();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbDocumentos_Click(object sender, EventArgs e)
        {
            FormListaDocumentos frm = new FormListaDocumentos();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Controle de Propostas"
        private void tsbDashCP_Click(object sender, EventArgs e)
        {
            FormDashboard frm = new FormDashboard();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbPropostas_Click(object sender, EventArgs e)
        {
            FormListaPropostas frm = new FormListaPropostas();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbClientes_Click(object sender, EventArgs e)
        {
            FormListaPCliente frm = new FormListaPCliente();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbInteresse_Click(object sender, EventArgs e)
        {
            FormListaInteresse frm = new FormListaInteresse();
            frm.MdiParent = this;
            frm.Show();
        }

        private void cadastroDeMetasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormListaMetas frm = new FormListaMetas();
            frm.MdiParent = this;
            frm.Show();
        }

        private void cadastroDePerguntasAnáliseDaPropostaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormListaPerguntas frm = new FormListaPerguntas();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbStatus_Click(object sender, EventArgs e)
        {
            FormListaStatus frm = new FormListaStatus();
            frm.MdiParent = this;
            frm.Show();
        }

        private void cadastroDeRespToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormListaResponsaveis frm = new FormListaResponsaveis();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbSetor_Click(object sender, EventArgs e)
        {
            FormListaSetores frm = new FormListaSetores();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Controle de Projetos"
        private void tsbProjetos_Click(object sender, EventArgs e)
        {
            FormListaProjetos frm = new FormListaProjetos();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbSetoresDistribuicao_Click(object sender, EventArgs e)
        {
            FormListaSetoresDistribidos frm = new FormListaSetoresDistribidos();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbAnaliseProjetos_Click(object sender, EventArgs e)
        {
            FormListaPergunta frm = new FormListaPergunta();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Verificações e Calibrações"
        private void tsbAfericoes_Click(object sender, EventArgs e)
        {
            FormListaAfericao frm = new FormListaAfericao();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbCalibracao_Click(object sender, EventArgs e)
        {
            FormListaCalibracao frm = new FormListaCalibracao();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Ficha de Verificação de Serviços"
        private void tsbCadastroFVS_Click(object sender, EventArgs e)
        {
            FormListagemFVS frm = new FormListagemFVS();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbModeloFVS_Click(object sender, EventArgs e)
        {
            FormListagemModelos frm = new FormListagemModelos();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Riscos"
        #endregion

        #region: "Global"
        private void tsbCCusto_Click(object sender, EventArgs e)
        {
            FormListaCCusto frm = new FormListaCCusto();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbColigadas_Click(object sender, EventArgs e)
        {
            FormListaEmpresas frm = new FormListaEmpresas();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsbListaUsuarios_Click(object sender, EventArgs e)
        {
            FormListaUsuarios frm = new FormListaUsuarios();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tseEmail_Click(object sender, EventArgs e)
        {
            using (FormEmail frm = new FormEmail())
            {
                frm.StartPosition = FormStartPosition.CenterScreen;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
            }
        }

        private void tsbGED_Click(object sender, EventArgs e)
        {
            FormListaCatalogoProd frm = new FormListaCatalogoProd();
            frm.MdiParent = this;
            frm.Show();
        }
        #endregion

        #region: "Configurações"
        private void tsbSairSistema_Click(object sender, EventArgs e)
        {
            restart = false;
            this.Dispose();
        }

        private void tsbAlterarUsr_Click(object sender, EventArgs e)
        {
            restart = true;
            this.Dispose();
        }

        private void tsbSobre_Click(object sender, EventArgs e)
        {
            using (FormSobre frm = new FormSobre())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
            }
        }

        private void tsbExportarArquivos_Click(object sender, EventArgs e)
        {

        }

        private void tsbImportarArquivos_Click(object sender, EventArgs e)
        {

        }

        private void tsbBancoDados_Click(object sender, EventArgs e)
        {
            using (FormConfigConexao frm = new FormConfigConexao(false))
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();
            }
        }

        private void tsbAtualizarBD_Click(object sender, EventArgs e)
        {
            if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
            {
                Configuracao config = new Configuracao();
                config.LeArquivo(Properties.Settings.Default.conexaoIntegracao);

                if (Criptografia.Decrypt(config.SysIntegracao) == "RMLabore")
                {
                    DialogResult dr = MessageBox.Show("Atenção!\n" +
                        "Este processamento realiza verificações e análises completas entre o banco de " +
                        "dados do sistema, com o banco de dados de integração, demandando tempo e processamento.\n" +
                        "Deseja continuar?",
                        "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (dr == DialogResult.Yes)
                    {
                        ControleAtualizaBD controle = new ControleAtualizaBD();
                        controle.GetIntegracaoRMLabore();
                    }
                    else
                        VerificaColigadas();
                }
            }
            else
            {
                Global.MsgErro("Conexão de integração não configurada.");
            }
        }

        private void tsbAlterarColigada_Click(object sender, EventArgs e)
        {
            using (FormAlterarColigada frm = new FormAlterarColigada())
            {
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.WindowState = FormWindowState.Normal;
                frm.ShowDialog();

                if (!frm.cancelado)
                {
                    usuarioAcesso.CODCOLIGADA = frm.codColigada;

                    foreach (Form f in this.MdiChildren)
                        f.Dispose();

                    FormPrincipal_Load(sender, e);
                }
            }
        }

        private void PermissaoAcesso()
        {
            using (ControleGPermissao controle = new ControleGPermissao())
            {
                List<GPERMISSAO> permissao = controle.Get(x => x.CODUSUARIO == usuarioAcesso.CODUSUARIO).ToList();
                if (!(usuarioAcesso.LOGIN == "rgti"))
                {
                    if (permissao != null && permissao.Count > 0)
                    {
                        if (permissao.Where(x => x.CODSISTEMA == "C").ToList().Count > 0)
                            tabNaoConformidade.Visible = true;
                        else
                            tabNaoConformidade.Visible = false;

                        if (permissao.Where(x => x.CODSISTEMA == "R").ToList().Count > 0)
                            tabPropostas.Visible = true;
                        else
                            tabPropostas.Visible = false;

                        if (permissao.Where(x => x.CODSISTEMA == "F").ToList().Count > 0)
                            tabFornecedores.Visible = true;
                        else
                            tabFornecedores.Visible = false;

                        if (
                                (permissao.Where(x => x.CODSISTEMA == "J").ToList().Count > 0) ||
                                (permissao.Where(x => x.CODSISTEMA == "P").ToList().Count > 0) ||
                                (permissao.Where(x => x.CODSISTEMA == "V").ToList().Count > 0) ||
                                (permissao.Where(x => x.CODSISTEMA == "A").ToList().Count > 0)
                          )
                        {
                            tabPropostas.Visible = true;

                            if (permissao.Where(x => x.CODSISTEMA == "A").ToList().Count > 0)
                                tseAfericaoCalibracao.Visible = true;
                            else
                                tseAfericaoCalibracao.Visible = false;

                            if ((permissao.Where(x => x.CODSISTEMA == "P").ToList().Count > 0))
                                tseGeralProposta.Visible = true;
                            else
                                tseGeralProposta.Visible = false;

                            if (permissao.Where(x => x.CODSISTEMA == "J").ToList().Count > 0)
                                tseGeralProjetos.Visible = true;
                            else
                                tseGeralProjetos.Visible = false;

                            if (permissao.Where(x => x.CODSISTEMA == "V").ToList().Count > 0)
                            {
                                tseFVS.Visible = true;

                                if (!usuarioAcesso.GERENTE)
                                    tsbModeloFVS.Enabled = false;
                            }
                            else
                                tseFVS.Visible = false;
                        }
                        else
                            tabPropostas.Visible = false;

                    }
                    else
                    {
                        tabNaoConformidade.Visible = false;
                        tabPropostas.Visible = false;
                        tabFornecedores.Visible = false;
                        tabGlobal.Visible = true;
                        tabConfiguracoes.Visible = true;
                    }
                }
            }
        }

        private void PermissoesAdministrativas()
        {
            if (!usuarioAcesso.GERENTE)
            {
                #region: "NÃO CONFORMIDADE"
                if (!usuarioAcesso.VEREFICACIAACAO)
                    tsbEficaciaAcao.Enabled = false;

                if (!usuarioAcesso.VERPLANOACAO)
                    tsbPlanoAcao.Enabled = false;

                tsbExtratificacao.Enabled = false;
                tsbTipoRegistro.Enabled = false;
                tsbCadSetores.Enabled = false;
                #endregion

                #region: "CONTROLE DE PROPOSTAS"
                #endregion

                #region: "RISCOS"
                #endregion

                #region: "FORNECEDORES E PRESTADORES DE SERVIÇO"
                tsbDocumentos.Enabled = false;
                tsbTpFornecedor.Enabled = false;
                #endregion

                #region: "GERAL"
                tsbColigadas.Enabled = false;
                tsbCCusto.Enabled = false;
                tsbListaUsuarios.Enabled = false;
                tsbAtualizarBD.Enabled = false;
                tsbBancoDados.Enabled = false;
                tsbExportarArquivos.Enabled = false;
                tsbImportarArquivos.Enabled = false;
                #endregion
            }
        }
        #endregion

        private void tsbEnviaEmail_Click(object sender, EventArgs e)
        {
            using (FormEnviaEmail frm = new FormEnviaEmail())
            {
                frm.ShowDialog();
            }
        }
    }
}