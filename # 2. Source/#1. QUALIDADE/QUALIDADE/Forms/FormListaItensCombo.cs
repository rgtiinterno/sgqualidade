﻿using Syncfusion.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE
{
    public partial class FormListaItensCombo : Form
    {
        private object itens;
        private BindingSource bs;
        public object itemSelecionado = null;
        private string[] colunasOcultas = null;
        private string[] _colunasFiltro = null;

        public FormListaItensCombo(object itensDataSource, params string[] colunasOcultas)
        {
            InitializeComponent();
            this.itens = itensDataSource;
            this.colunasOcultas = colunasOcultas;
            CarregaDados();
            CarregaCamposPesquisa();
        }

        public FormListaItensCombo(object itensDataSource, string[] colunasFiltro, params string[] colunasOcultas)
        {
            InitializeComponent();
            this.itens = itensDataSource;
            this.colunasOcultas = colunasOcultas;
            _colunasFiltro = colunasFiltro;
            CarregaDados();
            CarregaCamposPesquisa();
        }

        private void CarregaDados()
        {
            Cursor.Current = Cursors.WaitCursor;
            bs = new BindingSource();
            bs.DataSource = null;
            bs.DataSource = itens;
            dgvListagem.DataSource = null;
            dgvListagem.DataSource = bs;
            if (dgvListagem.Columns.Where(x => x.MappingName == "SELECIONADO").Count() > 0)
                dgvListagem.Columns["SELECIONADO"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "CKSELECIONADO").Count() > 0)
                dgvListagem.Columns["CKSELECIONADO"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "DESCCOMBO").Count() > 0)
                dgvListagem.Columns["DESCCOMBO"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "VALORCOMBO").Count() > 0)
                dgvListagem.Columns["VALORCOMBO"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "CRIADOPOR").Count() > 0)
                dgvListagem.Columns["CRIADOPOR"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "CRIADOEM").Count() > 0)
                dgvListagem.Columns["CRIADOEM"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "MODIFICADOPOR").Count() > 0)
                dgvListagem.Columns["MODIFICADOPOR"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "MODIFICADOEM").Count() > 0)
                dgvListagem.Columns["MODIFICADOEM"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "RECCREATEDBY").Count() > 0)
                dgvListagem.Columns["RECCREATEDBY"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "RECCREATEDON").Count() > 0)
                dgvListagem.Columns["RECCREATEDON"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "RECMODIFIEDBY").Count() > 0)
                dgvListagem.Columns["RECMODIFIEDBY"].Visible = false;
            if (dgvListagem.Columns.Where(x => x.MappingName == "RECMODIFIEDON").Count() > 0)
                dgvListagem.Columns["RECMODIFIEDON"].Visible = false;
            if (colunasOcultas != null && colunasOcultas.Length > 0)
            {
                foreach(string coluna in colunasOcultas)
                {
                    if (dgvListagem.Columns.Where(x => x.MappingName == coluna).Count() > 0)
                        dgvListagem.Columns[coluna].Visible = false;
                }
            }
            lbResultados.Text = bs.Count + " registro(s) encontrado(s).";
            Cursor.Current = Cursors.Default;
        }

        private void tsbSelecionar_Click(object sender, EventArgs e)
        {
            if (dgvListagem.CurrentItem != null)
            {
                itemSelecionado = dgvListagem.CurrentItem;
                this.Close();
            }
        }

        private void dgvListagem_CellDoubleClick(object sender, Syncfusion.WinForms.DataGrid.Events.CellClickEventArgs e)
        {
            tsbSelecionar.PerformClick();
        }

        private void CarregaCamposPesquisa()
        {
            cbCampoPesquisa.Items?.Clear();
            if(_colunasFiltro != null && _colunasFiltro.Length > 0)
            {
                foreach (var column in _colunasFiltro)
                {
                    cbCampoPesquisa.Items.Add(column);
                }
            }
            else
            {
                if (dgvListagem.Columns?.Count > 0)
                {
                    foreach (var column in dgvListagem.Columns.Where(x => x.Visible).OrderBy(x => x.MappingName))
                    {
                        cbCampoPesquisa.Items.Add(column.MappingName);
                    }
                }
            }
        }

        private void txtValorPesquisa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                tsbPesquisar.PerformClick();
        }

        private void tsbPesquisar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            dgvListagem?.ClearFilters();
            string campoPesquisa = cbCampoPesquisa.Text;
            if (!string.IsNullOrWhiteSpace(txtValorPesquisa.Text) && !string.IsNullOrEmpty(campoPesquisa))
            {
                string[] valoresPesquisa = txtValorPesquisa.Text.Split(new string[] { "%" }, StringSplitOptions.RemoveEmptyEntries);

                if (valoresPesquisa.Length <= 1)
                {
                    dgvListagem.Columns[campoPesquisa].FilterPredicates.Add(new FilterPredicate()
                    {
                        FilterBehavior = FilterBehavior.StringTyped, 
                        FilterType = FilterType.Contains,
                        IsCaseSensitive = false,
                        FilterValue = valoresPesquisa.Length == 1 ? valoresPesquisa[0] : txtValorPesquisa.Text
                    });
                }
                else
                {
                    foreach (string vlr in valoresPesquisa)
                    {
                        dgvListagem.Columns[campoPesquisa].FilterPredicates.Add(new FilterPredicate()
                        {
                            FilterType = FilterType.Contains,
                            IsCaseSensitive = false,
                            FilterValue = vlr,
                            PredicateType = PredicateType.AndAlso
                        });
                    }
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}