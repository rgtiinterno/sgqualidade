﻿namespace QUALIDADE.Forms.Configuracoes
{
    partial class FormConfigConexao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfigConexao));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.txtSenhaBD = new System.Windows.Forms.TextBox();
            this.txtUsuarioBD = new System.Windows.Forms.TextBox();
            this.txtNomeBD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtServidorBD = new System.Windows.Forms.TextBox();
            this.btnTestar = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSenhaBDRGON = new System.Windows.Forms.TextBox();
            this.txtUsuarioBDRGON = new System.Windows.Forms.TextBox();
            this.txtNomeBDRGON = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtServidorBDRGON = new System.Windows.Forms.TextBox();
            this.btnLimparRGON = new System.Windows.Forms.Button();
            this.btnTestarRGON = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbIntegracao = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnSalvar = new Syncfusion.WinForms.Controls.SfButton();
            this.btnCancelar = new Syncfusion.WinForms.Controls.SfButton();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(5, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(537, 242);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(529, 216);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Conexão principal";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(341, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 202);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.09F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.91F));
            this.tableLayoutPanel1.Controls.Add(this.btnLimpar, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtSenhaBD, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtUsuarioBD, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNomeBD, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtServidorBD, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnTestar, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(329, 202);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLimpar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.Location = new System.Drawing.Point(22, 167);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(106, 23);
            this.btnLimpar.TabIndex = 9;
            this.btnLimpar.Text = "Limpar campos";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // txtSenhaBD
            // 
            this.txtSenhaBD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSenhaBD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenhaBD.Location = new System.Drawing.Point(158, 128);
            this.txtSenhaBD.Name = "txtSenhaBD";
            this.txtSenhaBD.Size = new System.Drawing.Size(159, 20);
            this.txtSenhaBD.TabIndex = 4;
            this.txtSenhaBD.UseSystemPasswordChar = true;
            // 
            // txtUsuarioBD
            // 
            this.txtUsuarioBD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtUsuarioBD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuarioBD.Location = new System.Drawing.Point(158, 89);
            this.txtUsuarioBD.Name = "txtUsuarioBD";
            this.txtUsuarioBD.Size = new System.Drawing.Size(159, 20);
            this.txtUsuarioBD.TabIndex = 3;
            // 
            // txtNomeBD
            // 
            this.txtNomeBD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtNomeBD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeBD.Location = new System.Drawing.Point(158, 50);
            this.txtNomeBD.Name = "txtNomeBD";
            this.txtNomeBD.Size = new System.Drawing.Size(159, 20);
            this.txtNomeBD.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Banco de Dados:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Usuário:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Senha:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServidorBD
            // 
            this.txtServidorBD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtServidorBD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServidorBD.Location = new System.Drawing.Point(158, 11);
            this.txtServidorBD.Name = "txtServidorBD";
            this.txtServidorBD.Size = new System.Drawing.Size(159, 20);
            this.txtServidorBD.TabIndex = 1;
            // 
            // btnTestar
            // 
            this.btnTestar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTestar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestar.Location = new System.Drawing.Point(186, 167);
            this.btnTestar.Name = "btnTestar";
            this.btnTestar.Size = new System.Drawing.Size(104, 23);
            this.btnTestar.TabIndex = 5;
            this.btnTestar.Text = "Testar conexão";
            this.btnTestar.UseVisualStyleBackColor = true;
            this.btnTestar.Click += new System.EventHandler(this.btnTestar_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(529, 216);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Conexão de integração";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(341, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(180, 202);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.09203F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.90797F));
            this.tableLayoutPanel2.Controls.Add(this.txtSenhaBDRGON, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtUsuarioBDRGON, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtNomeBDRGON, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtServidorBDRGON, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnLimparRGON, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnTestarRGON, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.cbIntegracao, 1, 4);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(9, 6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(329, 202);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // txtSenhaBDRGON
            // 
            this.txtSenhaBDRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSenhaBDRGON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSenhaBDRGON.Location = new System.Drawing.Point(158, 107);
            this.txtSenhaBDRGON.Name = "txtSenhaBDRGON";
            this.txtSenhaBDRGON.Size = new System.Drawing.Size(159, 20);
            this.txtSenhaBDRGON.TabIndex = 4;
            this.txtSenhaBDRGON.UseSystemPasswordChar = true;
            // 
            // txtUsuarioBDRGON
            // 
            this.txtUsuarioBDRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtUsuarioBDRGON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuarioBDRGON.Location = new System.Drawing.Point(158, 74);
            this.txtUsuarioBDRGON.Name = "txtUsuarioBDRGON";
            this.txtUsuarioBDRGON.Size = new System.Drawing.Size(159, 20);
            this.txtUsuarioBDRGON.TabIndex = 3;
            // 
            // txtNomeBDRGON
            // 
            this.txtNomeBDRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtNomeBDRGON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeBDRGON.Location = new System.Drawing.Point(158, 41);
            this.txtNomeBDRGON.Name = "txtNomeBDRGON";
            this.txtNomeBDRGON.Size = new System.Drawing.Size(159, 20);
            this.txtNomeBDRGON.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Servidor:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Banco de Dados:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 77);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Usuário:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Senha:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtServidorBDRGON
            // 
            this.txtServidorBDRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtServidorBDRGON.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServidorBDRGON.Location = new System.Drawing.Point(158, 8);
            this.txtServidorBDRGON.Name = "txtServidorBDRGON";
            this.txtServidorBDRGON.Size = new System.Drawing.Size(159, 20);
            this.txtServidorBDRGON.TabIndex = 1;
            // 
            // btnLimparRGON
            // 
            this.btnLimparRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLimparRGON.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimparRGON.Location = new System.Drawing.Point(22, 172);
            this.btnLimparRGON.Name = "btnLimparRGON";
            this.btnLimparRGON.Size = new System.Drawing.Size(106, 23);
            this.btnLimparRGON.TabIndex = 9;
            this.btnLimparRGON.Text = "Limpar campos";
            this.btnLimparRGON.UseVisualStyleBackColor = true;
            this.btnLimparRGON.Click += new System.EventHandler(this.btnLimparRGON_Click);
            // 
            // btnTestarRGON
            // 
            this.btnTestarRGON.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTestarRGON.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTestarRGON.Location = new System.Drawing.Point(186, 172);
            this.btnTestarRGON.Name = "btnTestarRGON";
            this.btnTestarRGON.Size = new System.Drawing.Size(104, 23);
            this.btnTestarRGON.TabIndex = 5;
            this.btnTestarRGON.Text = "Testar conexão";
            this.btnTestarRGON.UseVisualStyleBackColor = true;
            this.btnTestarRGON.Click += new System.EventHandler(this.btnTestarRGON_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 143);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Integração com:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbIntegracao
            // 
            this.cbIntegracao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbIntegracao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbIntegracao.FormattingEnabled = true;
            this.cbIntegracao.Items.AddRange(new object[] {
            "RGON",
            "RMLabore"});
            this.cbIntegracao.Location = new System.Drawing.Point(153, 139);
            this.cbIntegracao.Name = "cbIntegracao";
            this.cbIntegracao.Size = new System.Drawing.Size(170, 21);
            this.cbIntegracao.TabIndex = 11;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button3.Location = new System.Drawing.Point(318, 253);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(109, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "Salvar";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button4.Location = new System.Drawing.Point(433, 253);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Cancelar";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnSalvar
            // 
            this.btnSalvar.AccessibleName = "Button";
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalvar.BackColor = System.Drawing.Color.Maroon;
            this.btnSalvar.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(340, 259);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(96, 28);
            this.btnSalvar.Style.BackColor = System.Drawing.Color.Maroon;
            this.btnSalvar.Style.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.TabIndex = 8;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.AccessibleName = "Button";
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.BackColor = System.Drawing.Color.Maroon;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Location = new System.Drawing.Point(442, 259);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(96, 28);
            this.btnCancelar.Style.BackColor = System.Drawing.Color.Maroon;
            this.btnCancelar.Style.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FormConfigConexao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 295);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MetroColor = System.Drawing.Color.White;
            this.MinimumSize = new System.Drawing.Size(563, 320);
            this.Name = "FormConfigConexao";
            this.ShowMaximizeBox = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurar Conexão com Banco de Dados";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConfigConexao_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.TextBox txtSenhaBD;
        private System.Windows.Forms.TextBox txtUsuarioBD;
        private System.Windows.Forms.TextBox txtNomeBD;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtServidorBD;
        private System.Windows.Forms.Button btnTestar;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnLimparRGON;
        private System.Windows.Forms.TextBox txtSenhaBDRGON;
        private System.Windows.Forms.TextBox txtUsuarioBDRGON;
        private System.Windows.Forms.TextBox txtNomeBDRGON;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtServidorBDRGON;
        private System.Windows.Forms.Button btnTestarRGON;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbIntegracao;
        private Syncfusion.WinForms.Controls.SfButton btnSalvar;
        private Syncfusion.WinForms.Controls.SfButton btnCancelar;
    }
}