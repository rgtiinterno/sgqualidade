﻿using System;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Configuracoes
{
    public partial class FormMsg : Form
    {
        public FormMsg(string detalhes)
        {
            InitializeComponent();
            txtDetalhes.Text = detalhes;
        }

        public FormMsg(Exception ex)
        {
            InitializeComponent();

            string msg = ex.InnerException?.Message ?? ex.InnerException?.InnerException?.Message;
            if (string.IsNullOrEmpty(msg) && (ex is DbEntityValidationException))
            {
                msg = string.Empty;
                DbEntityValidationException exEntity = ex as DbEntityValidationException;

                if (exEntity != null && exEntity.EntityValidationErrors != null)
                {
                    foreach (var results in exEntity.EntityValidationErrors)
                    {
                        if (results.ValidationErrors != null)
                        {
                            foreach (var erro in results.ValidationErrors)
                            {
                                msg += $"\n{erro.ErrorMessage}";
                            }
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(msg))
                msg = ex.Message;

            txtDetalhes.Text = msg;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FormMsg_Load(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = true;
            this.Size = new Size(456, 137);
        }

        private void btnDetalhes_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !(splitContainer1.Panel2Collapsed);

            if (splitContainer1.Panel2Collapsed)
            {
                this.Size = new Size(456, 137);
            }
            else
            {
                this.Size = new Size(456, 328);
            }
        }
    }
}
