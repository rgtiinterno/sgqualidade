﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Configuracoes
{
    public partial class FormAlterarColigada : Form
    {
        public bool cancelado = true;
        public short codColigada = 1;

        public FormAlterarColigada()
        {
            InitializeComponent();
        }

        private void FormAlterarColigada_Load(object sender, EventArgs e)
        {
            using (ControleGEmpresas controle = new ControleGEmpresas())
            {
                cbColigada.DataSource = controle.Get(x => x.CODCOLIGADA != 0).ToList();
                cbColigada.DisplayMember = "NOMEFANTASIA";
                cbColigada.ValueMember = "CODCOLIGADA";
                cbColigada.SelectedIndex = 0;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (cbColigada.SelectedIndex > -1)
            {
                this.cancelado = false;
                this.codColigada = Convert.ToInt16(cbColigada.SelectedValue);
                this.Dispose();
            }
            else
            {
                Global.MsgErro("Informe uma coligada");
            }
        }
    }
}
