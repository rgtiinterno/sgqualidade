﻿using Syncfusion.Windows.Forms;
using System;
using System.Reflection;

namespace QUALIDADE.Forms.Configuracoes
{
    public partial class FormSobre : MetroForm
    {
        public FormSobre()
        {
            InitializeComponent();
        }

        private void BtnFechar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FSobre_Load(object sender, EventArgs e)
        {
            lblVersaoSistema.Text += Assembly.GetExecutingAssembly().GetName().Version;
            lblNomeSistema.Text = "SGQ - Sistema de Gestão de Qualidade";
        }
    }
}
