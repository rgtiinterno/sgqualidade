﻿namespace QUALIDADE.Forms.Configuracoes
{
    partial class FormSobre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSobre));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnFechar = new Syncfusion.WinForms.Controls.SfButton();
            this.lblVersaoSistema = new System.Windows.Forms.Label();
            this.lblInformacoes = new System.Windows.Forms.Label();
            this.lblNomeSistema = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(219, 279);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnFechar
            // 
            this.btnFechar.AccessibleName = "Button";
            this.btnFechar.BackColor = System.Drawing.Color.Maroon;
            this.btnFechar.Font = new System.Drawing.Font("Segoe UI Semibold", 9F);
            this.btnFechar.ForeColor = System.Drawing.Color.White;
            this.btnFechar.Location = new System.Drawing.Point(468, 248);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(96, 28);
            this.btnFechar.Style.BackColor = System.Drawing.Color.Maroon;
            this.btnFechar.Style.ForeColor = System.Drawing.Color.White;
            this.btnFechar.TabIndex = 11;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = false;
            this.btnFechar.Click += new System.EventHandler(this.BtnFechar_Click);
            // 
            // lblVersaoSistema
            // 
            this.lblVersaoSistema.AutoSize = true;
            this.lblVersaoSistema.Location = new System.Drawing.Point(240, 71);
            this.lblVersaoSistema.Name = "lblVersaoSistema";
            this.lblVersaoSistema.Size = new System.Drawing.Size(101, 13);
            this.lblVersaoSistema.TabIndex = 12;
            this.lblVersaoSistema.Text = "Versão do Sistema: ";
            // 
            // lblInformacoes
            // 
            this.lblInformacoes.AutoSize = true;
            this.lblInformacoes.Location = new System.Drawing.Point(240, 118);
            this.lblInformacoes.Name = "lblInformacoes";
            this.lblInformacoes.Size = new System.Drawing.Size(239, 91);
            this.lblInformacoes.TabIndex = 12;
            this.lblInformacoes.Text = "Skype: suporte.rgti\r\n\r\nTel.: (31) 2515-5001 / (31) 3567-2570\r\n\r\nE-mail Gerente de" +
    " Projeto: lucas@rgtitec.com.br,\r\n\r\nE-mail Desenvolvedor: igor.albino@rgtitec.com" +
    ".br";
            // 
            // lblNomeSistema
            // 
            this.lblNomeSistema.AutoSize = true;
            this.lblNomeSistema.Location = new System.Drawing.Point(240, 47);
            this.lblNomeSistema.Name = "lblNomeSistema";
            this.lblNomeSistema.Size = new System.Drawing.Size(90, 13);
            this.lblNomeSistema.TabIndex = 12;
            this.lblNomeSistema.Text = "Nome do Sistema";
            // 
            // FormSobre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 288);
            this.Controls.Add(this.lblInformacoes);
            this.Controls.Add(this.lblNomeSistema);
            this.Controls.Add(this.lblVersaoSistema);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MetroColor = System.Drawing.Color.White;
            this.MinimizeBox = false;
            this.Name = "FormSobre";
            this.Text = "Sobre o Sistema";
            this.Load += new System.EventHandler(this.FSobre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private Syncfusion.WinForms.Controls.SfButton btnFechar;
        private System.Windows.Forms.Label lblVersaoSistema;
        private System.Windows.Forms.Label lblInformacoes;
        private System.Windows.Forms.Label lblNomeSistema;
    }
}