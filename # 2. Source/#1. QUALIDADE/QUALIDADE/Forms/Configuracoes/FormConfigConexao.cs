﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.Windows.Forms;
using System;
using System.IO;
using System.Windows.Forms;

namespace QUALIDADE.Forms.Configuracoes
{
    public partial class FormConfigConexao : MetroForm
    {
        private bool inicial = false;
        private Configuracao cfg = new Configuracao();

        public FormConfigConexao(bool inicial)
        {
            InitializeComponent();
            this.inicial = inicial;

            //Carrega dados de conexão com o banco de dados Principal
            if (File.Exists(Properties.Settings.Default.conexaoPrincipal))
            {
                cfg.LeArquivo(Properties.Settings.Default.conexaoPrincipal);

                txtServidorBD.Text = Criptografia.Decrypt(cfg.Servidor);
                txtNomeBD.Text = Criptografia.Decrypt(cfg.BancoDeDados);
                txtUsuarioBD.Text = Criptografia.Decrypt(cfg.Usuario);
                txtSenhaBD.Text = Criptografia.Decrypt(cfg.Senha);
            }
            else
                btnCancelar.Enabled = false;

            //Carrega dados de conexão com o banco de dados de Integração
            if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
            {
                cfg.LeArquivo(Properties.Settings.Default.conexaoIntegracao);

                txtServidorBDRGON.Text = Criptografia.Decrypt(cfg.Servidor);
                txtNomeBDRGON.Text = Criptografia.Decrypt(cfg.BancoDeDados);
                txtUsuarioBDRGON.Text = Criptografia.Decrypt(cfg.Usuario);
                txtSenhaBDRGON.Text = Criptografia.Decrypt(cfg.Senha);
                cbIntegracao.Text = Criptografia.Decrypt(cfg.SysIntegracao);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtServidorBD.Text = "";
            txtNomeBD.Text = "";
            txtUsuarioBD.Text = "";
            txtSenhaBD.Text = "";
        }

        private void btnLimparRGON_Click(object sender, EventArgs e)
        {
            txtServidorBDRGON.Text = "";
            txtNomeBDRGON.Text = "";
            txtUsuarioBDRGON.Text = "";
            txtSenhaBDRGON.Text = "";
            cbIntegracao.SelectedIndex = -1;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (txtServidorBD.Text == "" || txtNomeBD.Text == "" || txtUsuarioBD.Text == "" || txtSenhaBD.Text == "")
            {
                MessageBox.Show("Preencha todos os campos da Conexão Principal.",
                    "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


            else if (!string.IsNullOrEmpty(txtServidorBDRGON.Text) && string.IsNullOrEmpty(txtNomeBDRGON.Text) &&
                string.IsNullOrEmpty(txtUsuarioBDRGON.Text) && string.IsNullOrEmpty(txtSenhaBDRGON.Text))
            {
                MessageBox.Show("Preencha todos os campos da Conexão de Integração.",
                    "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                if (cbIntegracao.SelectedIndex == -1)
                {
                    MessageBox.Show("Selecione o sistema de integração.",
                        "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                GeraArquivosConexaoPrincipal();
                if (!Global.TestarConexao(SqlConn.getStringConexao()))
                {
                    MessageBox.Show("Falha na conexão principal.",
                        "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (!string.IsNullOrEmpty(txtServidorBDRGON.Text))
                {
                    GeraArquivosConexaoIntegracao();
                    if (!Global.TestarConexao(SqlConn.getStringIntegracao()))
                    {
                        MessageBox.Show("Falha na conexão de integração.",
                            "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    MessageBox.Show("Conexão configurada com sucesso.",
                        "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnCancelar.Enabled = true;

                    DialogResult result = MessageBox.Show("Fechar janela de configuração?",
                        "Mensagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        if (inicial)
                            Application.Restart();
                        else
                            this.Dispose();
                    }
                }
                else
                {
                    MessageBox.Show("Conexão configurada com sucesso.",
                        "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnCancelar.Enabled = true;

                    DialogResult result = MessageBox.Show("Fechar janela de configuração?",
                        "Mensagem", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        if (inicial)
                            Application.Restart();
                        else
                            this.Dispose();
                    }
                }
            }
        }

        private void GeraArquivosConexaoPrincipal()
        {
            try
            {
                cfg.GeraArquivo(
                    txtServidorBD.Text,
                    txtNomeBD.Text,
                    txtUsuarioBD.Text,
                    txtSenhaBD.Text,
                    Properties.Settings.Default.conexaoPrincipal,
                    null
                );
            }
            catch (Exception)
            {
                throw new Exception("Não foi possivel criar o arquivo de configuração da conexão principal.");
            }
        }

        private void GeraArquivosConexaoIntegracao()
        {
            try
            {
                cfg.GeraArquivo(
                    txtServidorBDRGON.Text,
                    txtNomeBDRGON.Text,
                    txtUsuarioBDRGON.Text,
                    txtSenhaBDRGON.Text,
                    Properties.Settings.Default.conexaoIntegracao,
                    cbIntegracao.Text
                );
            }
            catch (Exception)
            {
                throw new Exception("Não foi possivel criar o arquivo de configuração da conexão de integração.");
            }
        }

        private void btnTestar_Click(object sender, EventArgs e)
        {
            if (txtServidorBD.Text == "" || txtNomeBD.Text == "" || txtUsuarioBD.Text == "" || txtSenhaBD.Text == "")
            {
                MessageBox.Show("Preencha todos os campos para testar a conexão principal.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                GeraArquivosConexaoPrincipal();

                if (Global.TestarConexao(SqlConn.getStringConexao()))
                    MessageBox.Show("Conexão realizada com sucesso.", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Falha na conexão principal.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTestarRGON_Click(object sender, EventArgs e)
        {
            if (txtServidorBDRGON.Text == "" || txtNomeBDRGON.Text == "" || txtUsuarioBDRGON.Text == "" || txtSenhaBDRGON.Text == "")
            {
                MessageBox.Show("Preencha todos os campos para testar a conexão de integração.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (cbIntegracao.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione o sistema de integração.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                GeraArquivosConexaoIntegracao();

                if (Global.TestarConexao(SqlConn.getStringIntegracao()))
                    MessageBox.Show("Conexão realizada com sucesso.", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Falha na conexão de integração.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FormConfigConexao_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!(btnCancelar.Enabled))
            {
                e.Cancel = true;
                DialogResult result = MessageBox.Show("Para continuar, é necessário realizar esta configuração.\nDeseja sair do sistema?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                switch (result)
                {
                    case DialogResult.Yes:
                        Environment.Exit(0);
                        break;

                    case DialogResult.No:
                        e.Cancel = true;
                        break;
                }
            }
        }
    }
}