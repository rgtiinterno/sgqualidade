﻿using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Email;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace QUALIDADE
{
    public delegate void Update(int i, int total);

    public partial class FormEnviaEmail : Form
    {
        private int _qtdTotal = 0;
        private int _qtdErro = 0;
        private int _qtdSucesso = 0;

        public FormEnviaEmail()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
                backgroundWorker1.CancelAsync();
            btnCancelar.Enabled = false;
            btnContinuar.Enabled = true;
            lblEstatisticas.Text = "Cancelando...";
            this.Dispose();
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            GUSUARIO usuario = FormPrincipal.getUsuarioAcesso();
            if (string.IsNullOrEmpty(usuario.EMAIL) || string.IsNullOrEmpty(usuario.HOST) || string.IsNullOrEmpty(usuario.SENHAEMAIL))
            {
                MessageBox.Show(
                    $"Não foi possível realizar o envio de e-mails.\r\n" +
                    $"Para realizar esta ação, por favor, confira se um e-mail está informado em seu " +
                    $"cadastro de usuário e realize a configuração de smtp em " +
                    $"'Processos da Qualidade Global > Configurações de E-mail'.", 
                    "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error
                );
            }
            else if (string.IsNullOrEmpty(txtAssunto.Text))
            {
                MessageBox.Show("Informe o Assunto do E-mail", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                btnContinuar.Enabled = false;
                btnCancelar.Enabled = true;
                backgroundWorker1.RunWorkerAsync();
                progressBar1.Style = ProgressBarStyle.Marquee;
                progressBar1.MarqueeAnimationSpeed = 5;
                lblEstatisticas.Text = "Processando...";
            }
        }

        private void UpdateStatusLabel(int i, int total)
        {
            lblEstatisticas.Text = string.Format("Enviando {0} de {1}", i, total);
        }

        private void ExecutaOperacao()
        {
            Cursor.Current = Cursors.WaitCursor;
            List<CorpoEmail> emails = null;
            using (ControleGEmpresas controller = new ControleGEmpresas())
            {
                emails = controller.GetUsuariosEnvioEmail() ?? new List<CorpoEmail>();
            }
            List<string> log = new List<string>();
            TimeSpan tempo = new TimeSpan();
            Stopwatch sw = new Stopwatch();
            _qtdTotal = emails.Count;
            _qtdErro = 0;
            _qtdSucesso = 0;
            int posicaoAtual = 1;

            sw.Start();
            GUSUARIO usuario = FormPrincipal.getUsuarioAcesso();

            if (string.IsNullOrEmpty(usuario.EMAIL) || string.IsNullOrEmpty(usuario.HOST) || string.IsNullOrEmpty(usuario.SENHAEMAIL))
            {
                log.Add(
                    $"Não foi possível realizar o envio de e-mails.\r\n" +
                    $"Para realizar esta ação, por favor, confira se um e-mail está informado em seu " +
                    $"cadastro de usuário e realize a configuração de smtp em " +
                    $"'Processos da Qualidade Global > Configurações de E-mail'."
                );
            }
            else
            {
                try
                {
                    if (emails != null && emails.Count > 0)
                    {
                        EmailController emailController = new EmailController();
                        foreach (CorpoEmail item in emails)
                        {
                            item.Titulo = !string.IsNullOrEmpty(txtAssunto.Text.Trim()) ? txtAssunto.Text.Trim() : null;
                            item.MsgCompl = txtMsg.Text.Trim();
                            item.EmailsCopia = txtEmailCopia.Text.Trim();
                            lblEstatisticas.Invoke(new Update(UpdateStatusLabel), new object[] { posicaoAtual, _qtdTotal });
                            bool enviado = emailController.EnviaEmail(item, usuario, out string msgSaida);
                            if (enviado)
                            {
                                _qtdSucesso++;
                                log.Add($"[SUCESSO] {msgSaida}");
                            }
                            else
                            {
                                _qtdErro++;
                                log.Add($"[ERRO] {msgSaida}");
                            }
                            posicaoAtual++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _qtdErro++;
                    log.Add($"[ERRO] Falha ao obter e-mails para transmissão. Erro: {ex.InnerException?.InnerException?.Message ?? ex.InnerException?.Message ?? ex.Message}");
                }
                finally
                {
                    sw.Stop();
                    tempo = sw.Elapsed;
                }
            }

            try
            {
                Global.GeraLogEnvio(new Log
                {
                    Mensagens = log,
                    NumTotal = _qtdTotal,
                    NumErros = _qtdErro,
                    NumSucessos = _qtdSucesso,
                    Tempo = tempo
                });
            }
            catch (IOException ex)
            {
                MessageBox.Show("Ocorreu um erro ao gerar o arquivo de Log.\n\n" + ex.Message,
                    "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Cursor.Current = Cursors.Default;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ExecutaOperacao();
            if (backgroundWorker1.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
                lblEstatisticas.Text = "Operação Cancelada pelo Usuário!";
                lblEstatisticas.Text = string.Empty;
            }
            else if (e.Error != null)
            {
                lblEstatisticas.Text = "Ocorreu um erro durante a execução do processo!";
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
            else
            {
                lblEstatisticas.Text = "Envio concluído.";
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
            }
            btnContinuar.Enabled = true;
            btnCancelar.Enabled = false;

            Mensagem.Informacao(
                "Envio concluído",
                "Envio concluído: " +
                "\n - Total: " + _qtdTotal +
                "\n - Sucessos: " + _qtdSucesso + "" +
                "\n - Erros: " + _qtdErro
            );
        }
    }
}