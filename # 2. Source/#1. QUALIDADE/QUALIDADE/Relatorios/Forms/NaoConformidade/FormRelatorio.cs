﻿using Microsoft.Reporting.WinForms;
using QUALIDADE.Controle;
using QUALIDADE.Dominio;
using System;
using System.Windows.Forms;

namespace QUALIDADE.Relatorios.Forms.NaoConformidade
{
    public partial class FormRelatorio : Form
    {
        private CNCONFORM nConform;

        public FormRelatorio(CNCONFORM naoConformidade)
        {
            InitializeComponent();
            nConform = naoConformidade;
        }

        private void FormRelatorio_Load(object sender, EventArgs e)
        {
            using (ControleCNConformidade controle = new ControleCNConformidade())
            {
                this.RelNConformidadeBindingSource.DataSource = controle.GetRelatorio(nConform);
                this.reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                this.reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsDados", RelNConformidadeBindingSource));
                this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                this.reportViewer1.ZoomMode = ZoomMode.Percent;
                this.reportViewer1.ZoomPercent = 100;
                this.reportViewer1.RefreshReport();
            }
        }

        private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            using (ControleCVPAcaoEficacia controle = new ControleCVPAcaoEficacia())
            {
                e.DataSources.Add(new ReportDataSource("dsDados", controle.GetRelatorio(nConform)));
            }
        }
    }
}