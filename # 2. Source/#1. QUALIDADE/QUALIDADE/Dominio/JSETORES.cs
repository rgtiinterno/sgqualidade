//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QUALIDADE.Dominio
{
    using System;
    using System.Collections.Generic;
    
    public partial class JSETORES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public JSETORES()
        {
            this.JSETORESDISTRIBUICAO = new HashSet<JSETORESDISTRIBUICAO>();
        }
    
        public int CODIGO { get; set; }
        public string DESCRICAO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JSETORESDISTRIBUICAO> JSETORESDISTRIBUICAO { get; set; }
    }
}
