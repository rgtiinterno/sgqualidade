﻿using System;
using System.Data;

namespace QUALIDADE.Dominio
{
    public class Endereco
    {
        public string Resultado { get; set; }
        public string ResultadoTXT { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
        public string Bairro { get; set; }
        public string Logradouro { get; set; }
        public string TipoLogradouro { get; set; }
    }

    public class BuscaCEP
    {
        public static Endereco Buscar(string cep)
        {
            if (String.IsNullOrEmpty(cep))
                return null;

            DataSet dataSet = new DataSet();
            string xml = string.Format("http://cep.republicavirtual.com.br/web_cep.php?cep={0}&formato=xml", cep.Replace("-", "").Replace(",", ""));
            dataSet.ReadXml(xml);

            if (dataSet != null && dataSet.Tables.Count > 0)
            {
                Endereco endereco = new Endereco
                {
                    Resultado = dataSet.Tables[0].Rows[0]["resultado"].ToString(),
                    ResultadoTXT = dataSet.Tables[0].Rows[0]["resultado_txt"].ToString(),
                    UF = dataSet.Tables[0].Rows[0]["uf"].ToString(),
                    Cidade = dataSet.Tables[0].Rows[0]["cidade"].ToString(),
                    Bairro = dataSet.Tables[0].Rows[0]["bairro"].ToString(),
                    Logradouro = dataSet.Tables[0].Rows[0]["logradouro"].ToString(),
                    TipoLogradouro = dataSet.Tables[0].Rows[0]["tipo_logradouro"].ToString()
                };

                return endereco;
            }

            return null;
        }
    }
}
