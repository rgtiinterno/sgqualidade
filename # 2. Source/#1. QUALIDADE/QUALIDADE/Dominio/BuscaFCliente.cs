﻿using Newtonsoft.Json;
using QUALIDADE.Dominio;
using System.Net;

namespace QUALIDADE.Dominio
{
    public class FCliente
    {
        public string nome { get; set; }
        public string fantasia { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string municipio { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
    }

    public class BuscaFCliente
    {
        public static FCliente Buscar(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj))
                return null;

            WebClient wc = new WebClient();
            string json = wc.DownloadString(
                string.Format("https://www.receitaws.com.br/v1/cnpj/{0}", Global.RemovePVTEsp(cnpj))
                );
            return JsonConvert.DeserializeObject<FCliente>(json);
        }
    }
}
