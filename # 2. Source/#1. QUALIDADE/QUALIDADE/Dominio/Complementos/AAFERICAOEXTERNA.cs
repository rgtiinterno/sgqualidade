﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class AAFERICAOEXTERNA
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string FORNECEDOR { get; set; }

        [NotMapped]
        public string CENTROCUSTO { get; set; }
    }
}