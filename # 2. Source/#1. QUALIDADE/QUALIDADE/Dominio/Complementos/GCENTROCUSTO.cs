﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class GCENTROCUSTO
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;
        [NotMapped]
        public string NOMEGERENTE { get; set; }
        [NotMapped]
        public string DESCCOMBO => $"{CODCCUSTO} - {NOME}";
    }
}
