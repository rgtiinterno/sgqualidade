﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class AAFERICAOINTERNA
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string FORNECEDOR { get; set; }

        [NotMapped]
        public string CENTROCUSTO { get; set; }

        [NotMapped]
        public string NUMCONTRATO { get; set; }

        [NotMapped]
        public string DESCTIPOAFERICAO { 
            get { 
                switch(TIPOAFERICAO)
                {
                    case "068": return "Esquadro"; 
                    case "069": return "Nível Bolha"; 
                    case "070": return "Trena";
                    default: return "Outros";
                }
            } 
        }
    }
}