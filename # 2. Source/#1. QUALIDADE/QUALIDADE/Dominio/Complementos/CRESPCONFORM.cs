﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class CRESPCONFORM
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string NOMEUSUARIO { get; set; }
    }
}