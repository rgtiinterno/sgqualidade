﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class FARQUIVOCFO
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public bool TEMVALIDADE { get; set; } = false;

        [NotMapped]
        public string NOMEARQUIVO { get; set; }

        [NotMapped]
        public Nullable<DateTime> DATAVALIDADEARQUIVO { get; set; }
    }
}