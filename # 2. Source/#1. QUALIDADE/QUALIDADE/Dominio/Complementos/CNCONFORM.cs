﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class CNCONFORM
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string DESCEXTRATIF { get; set; }

        [NotMapped]
        public string DESCCUSTO { get; set; }

        [NotMapped]
        public string DESCCLASSIF { get; set; }

        [NotMapped]
        public string DESCSTATUS { get; set; }

        [NotMapped]
        public string NOMERALATOR { get; set; }

        [NotMapped]
        public string DESCEXECUTOR { get; set; }

        [NotMapped]
        public string DESCREGISTRO { get; set; }

        [NotMapped]
        public string DESCSETOR { get; set; }
    }
}
