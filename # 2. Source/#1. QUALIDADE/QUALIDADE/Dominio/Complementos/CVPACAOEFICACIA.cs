﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class CVPACAOEFICACIA
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string DESCCUSTO { get; set; }

        [NotMapped]
        public string NOMEUSUVERIFPACAO { get; set; }

        [NotMapped]
        public string NOMEUSUVERIFEFIC { get; set; }
    }
}
