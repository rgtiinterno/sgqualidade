﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class FARQFCFO
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string TPARQUIVO { get; set; }

        [NotMapped]
        public bool DOCVENCEATE30D
        {
            get
            {
                if (POSSUIVALIDADE && DATAVALIDADE.HasValue)
                {
                    int difDias = DATAVALIDADE.Value.Subtract(DateTime.Today).Days;
                    return difDias > 0 && difDias <= 30;
                }
                return false;
            }
        }

        [NotMapped]
        public bool DOCVENCIDO
        {
            get
            {
                if (POSSUIVALIDADE && DATAVALIDADE.HasValue)
                {
                    return DATAVALIDADE.Value.Subtract(DateTime.Today).Days <= 0;
                }
                return false;
            }
        }
    }
}