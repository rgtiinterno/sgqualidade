﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class JPROJETOS
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string CENTROCUSTO { get; set; }

        [NotMapped]
        public string DISTRIBUICAO { get; set; }
    }
}