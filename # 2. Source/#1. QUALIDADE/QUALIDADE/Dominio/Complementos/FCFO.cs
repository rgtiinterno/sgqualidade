﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class FCFO
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string DESCPESSOAFISOUJUR { get; set; }

        [NotMapped]
        public string DESCPAGREC { get; set; }

        [NotMapped]
        public bool STATUS { get; set; }

        [NotMapped]
        public string CONFORMIDADE { get; set; }

        [NotMapped]
        public string CODCCUSTO { get; set; }

        [NotMapped]
        public string DESCCCUSTO { get; set; }

        [NotMapped]
        public string DESCTIPOFCFO { get; set; }

        [NotMapped]
        public int DOCVENCEATE30D { get; set; }

        [NotMapped]
        public int DOCVENCIDO { get; set; }
    }
}