﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class PPROPOSTAS
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;

        [NotMapped]
        public string SETOR { get; set; }

        [NotMapped]
        public string CLIENTE { get; set; }

        [NotMapped]
        public string INTERESSE { get; set; }

        [NotMapped]
        public string STATUS { get; set; }

        [NotMapped]
        public string TEMVISITA { get; set; }
    }
}