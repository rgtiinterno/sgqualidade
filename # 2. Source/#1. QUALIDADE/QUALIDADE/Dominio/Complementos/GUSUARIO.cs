﻿using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public partial class GUSUARIO
    {
        [NotMapped]
        public bool CKSELECIONADO { get; set; } = false;
        [NotMapped]
        public short CODCOLIGADA { get; set; } = 1;
    }
}