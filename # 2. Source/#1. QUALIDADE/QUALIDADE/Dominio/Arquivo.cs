﻿using System;
using System.IO;
using System.IO.Compression;
using System.Windows.Forms;

namespace QUALIDADE.Dominio
{
    public class Arquivo
    {
        private static string arquivoLogin = string.Format(@"{0}\login.cfg", Directory.GetCurrentDirectory());
        public static string directoryLog = string.Format(@"{0}\arquivos\logs", Directory.GetCurrentDirectory());

        #region "PDF para banco de dados"
        public static byte[] EncriptPDF(string pdf)
        {
            byte[] arrayDeBytes;

            using (Stream stream = new FileStream(pdf, FileMode.Open))
            {
                arrayDeBytes = new byte[stream.Length + 1];
                stream.Read(arrayDeBytes, 0, arrayDeBytes.Length);
            }

            return CompressBytes(arrayDeBytes);
        }

        public static string DescriptPDF(byte[] bytesPDF, string nomeArquivo)
        {
            byte[] bytesArquivo = (bytesPDF);

            string sFile = nomeArquivo;
            File.WriteAllBytes(sFile, DecompressBytes(bytesArquivo));

            return sFile;
        }
        #endregion

        #region "Compactação de Bytes[]"
        public static byte[] CompressBytes(byte[] data)
        {
            // Fonte: http://stackoverflow.com/a/271264/194717
            using (var compressedStream = new MemoryStream())
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
            {
                zipStream.Write(data, 0, data.Length);
                zipStream.Close();
                return compressedStream.ToArray();
            }
        }

        public static byte[] DecompressBytes(byte[] bSource)
        {
            // Fonte: http://stackoverflow.com/questions/6350776/help-with-programmatic-compression-decompression-to-memorystream-with-gzipstream
            using (var inStream = new MemoryStream(bSource))
            using (var gzip = new GZipStream(inStream, CompressionMode.Decompress))
            using (var outStream = new MemoryStream())
            {
                gzip.CopyTo(outStream);
                return outStream.ToArray();
            }
        }
        #endregion

        public static void GravaLogin(string login)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(arquivoLogin, false))
                {
                    writer.WriteLine(login);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static string LeLogin()
        {
            string login = string.Empty;
            try
            {
                if (File.Exists(arquivoLogin))
                {
                    using (StreamReader reader = new StreamReader(arquivoLogin))
                    {
                        while (!reader.EndOfStream)
                        {
                            string linha = reader.ReadLine();
                            if (!string.IsNullOrEmpty(linha))
                                login = linha;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
            return login;
        }

        /// <summary>
        /// Abre janela de dialogo para seleção de um diretório
        /// </summary>
        /// <returns>string contendo o nome do diretório</returns>
        public static string SelecionaDiretorio()
        {
            string path = null;
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    path = folderBrowserDialog.SelectedPath;
                }
                catch (Exception ex)
                {
                    string message = "Não foi possível ler o arquivo do disco.\n\nErro: " + ex.Message;
                    CriaLog(message);
                    Mensagem.Erro("Erro", message);
                }
            }

            return path;
        }

        /// <summary>
        /// Abre janela de dialogo para seleção de um único arquivo
        /// </summary>
        /// <param name="filter">Tipos de arquivos suportados. Ex.: Arquivo de dados (*.txt;*.csv)|*.txt;*.csv</param>
        /// <returns></returns>
        public static string SelecionaArquivo(string filter)
        {
            string file = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = filter;
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    file = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    string message = "Não foi possível ler o arquivo do disco.\n\nErro: " + ex.Message;
                    CriaLog(message);
                    Mensagem.Erro("Erro", message);
                }
            }

            return file;
        }

        /// <summary>
        /// Abre janela de dialogo para seleção de vários arquivos
        /// </summary>
        /// <param name="filter">Tipos de arquivos suportados. Ex.: Arquivo de dados (*.txt;*.csv)|*.txt;*.csv</param>
        /// <returns></returns>
        public static string[] SelecionaArquivos(string filter)
        {
            string[] files = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = filter;
            openFileDialog.Multiselect = true;
            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    files = openFileDialog.FileNames;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o(s) arquivo(s) do disco." +
                         "\n\nErro: " + ex.Message);
                }
            }

            return files;
        }


        /// <summary>
        /// Salva o arquivo no diretório selecionado
        /// </summary>
        /// <returns>Nome do arquivo a ser salvo</returns>
        public static string SalvaArquivo()
        {
            string fileName = null;

            using (SaveFileDialog save = new SaveFileDialog())
            {
                if (save.ShowDialog() == DialogResult.OK)
                {
                    fileName = save.FileName;
                }
            }

            return fileName;
        }

        /// <summary>
        /// Salva o arquivo no diretório selecionado
        /// </summary>
        /// <param name="filter">Formato do arquivo. Exemplo: txt files (*.txt)|*.txt|All files (*.*)|*.*</param>
        /// <param name="filterIndex">Número do índice do filtro.</param>
        /// <returns>Nome do arquivo a ser salvo</returns>
        public static string SalvaArquivo(string filter)
        {
            string fileName = null;

            using (SaveFileDialog save = new SaveFileDialog())
            {
                save.Filter = filter;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    fileName = save.FileName;
                }
            }

            return fileName;
        }

        /// <summary>
        /// Salva o arquivo no diretório selecionado
        /// </summary>
        /// <param name="filter">Formato do arquivo. Exemplo: txt files (*.txt)|*.txt|All files (*.*)|*.*</param>
        /// <param name="defaultName">Nome padrão do arquivo</param>
        /// <returns>Nome do arquivo a ser salvo</returns>
        public static string SalvaArquivo(string filter, string defaultName)
        {
            string fileName = null;

            using (SaveFileDialog save = new SaveFileDialog())
            {
                save.FileName = defaultName;
                if (!String.IsNullOrEmpty(filter))
                    save.Filter = filter;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    fileName = save.FileName;
                }
            }

            return fileName;
        }

        /// <summary>
        /// Insere o valor desejado no arquivo de Log padrão do sistema
        /// </summary>
        /// <param name="message">Mensagem de Log</param>
        /// <returns></returns>
        public static void CriaLog(string text)
        {
            try
            {
                FileStream file = null;
                DateTime currentDate = DateTime.Now;

                if (!Directory.Exists(directoryLog))
                    Directory.CreateDirectory(directoryLog);

                string logFile = String.Format(@"{0}\Log_{1:ddMMyyyy}.log", directoryLog, currentDate);

                if (!File.Exists(logFile))
                {
                    file = File.Create(logFile);
                    if (file != null)
                        file.Close();
                }

                File.AppendAllText(
                    logFile,
                    String.Format("{0} {1} - {2}",
                        currentDate.ToShortDateString(), currentDate.ToLongTimeString(),
                        text + Environment.NewLine
                    )
                );
            }
            catch (Exception ex)
            {
                Mensagem.Erro("Erro", "Não foi possível gravar o log.\nErro: " + ex.Message);
            }
        }
    }
}