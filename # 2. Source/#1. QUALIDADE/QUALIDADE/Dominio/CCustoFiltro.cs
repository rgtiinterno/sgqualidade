﻿namespace QUALIDADE.Dominio
{
    public class CCustoFiltro
    {
        public int CodColigada { get; set; }
        public string CodCCusto { get; set; }
        public string Nome { get; set; }
        public string DescCombo { get; set; }
    }
}