﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;

namespace QUALIDADE.Dominio
{
    public static class Global
    {
        public static int[] tamMaxFotoEquipamentos = new int[] { 600, 400 };
        public static string dirXml = Directory.GetCurrentDirectory() + $@"\xml\";
        public static string dirLogs = Directory.GetCurrentDirectory() + $@"\log\";

        public static void CreateGraphic(string EixoX, string EixoY, bool ValueShowLabel, SeriesChartType type, ref Chart chart)
        {
            chart.Series.Clear();

            chart.Series.Add("novaSerie");
            chart.Series["novaSerie"].XValueMember = EixoX;
            chart.Series["novaSerie"].YValueMembers = EixoY;
            chart.Series["novaSerie"].IsValueShownAsLabel = ValueShowLabel;
            chart.Series["novaSerie"].ChartType = type;

            if (type != SeriesChartType.Pie && type != SeriesChartType.Doughnut &&
               type != SeriesChartType.Radar && type != SeriesChartType.Polar &&
               type != SeriesChartType.Pyramid && type != SeriesChartType.Funnel)
            {
                chart.Series["novaSerie"].IsVisibleInLegend = false;
                chart.ChartAreas["ChartArea"].AxisX.MajorGrid.Enabled = false;
                chart.ChartAreas["ChartArea"].AxisY.MajorGrid.LineColor = Color.LightGray;
            }

            chart.DataBind();
            chart.Show();
        }

        internal static void ApenasAdmin(params ToolStripButton[] botoes)
        {
            GUSUARIO usr = FormPrincipal.getUsuarioAcesso();
            if (usr != null && botoes != null)
            {
                foreach (ToolStripButton btn in botoes)
                {
                    btn.Enabled = usr.GERENTE;
                }
            }
        }

        public static void Email(string corpoEmail, string assunto, bool prioridade, MailAddress remetente, List<string> copiasEmail)
        {
            try
            {
                if (!string.IsNullOrEmpty(FormPrincipal.getUsuarioAcesso().HOST) && !string.IsNullOrEmpty(FormPrincipal.getUsuarioAcesso().PORTA))
                {
                    string tratamento = Convert.ToInt32(DateTime.Now.ToString("HH")) < 12 ?
                        "Prezados, bom dia." :
                        Convert.ToInt32(DateTime.Now.ToString("HH")) > 12 &&
                        Convert.ToInt32(DateTime.Now.ToString("HH")) < 18 ?
                        "Prezados, boa tarde." : "Prezados, boa noite.";


                    MailMessage mail = new MailMessage
                    {
                        IsBodyHtml = true,
                        DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
                        Subject = assunto,
                        Priority = prioridade ? MailPriority.High : MailPriority.Normal,
                        From = new MailAddress(FormPrincipal.getUsuarioAcesso().EMAIL, FormPrincipal.getUsuarioAcesso().NOME)
                    };

                    AlternateView altView = AlternateView.CreateAlternateViewFromString(tratamento + corpoEmail, null, MediaTypeNames.Text.Html);
                    LinkedResource imgLink = new LinkedResource(Directory.GetCurrentDirectory() + $@"\Logo.png")
                    {
                        ContentId = "ImageLogo"
                    };
                    altView.LinkedResources.Add(imgLink);
                    mail.AlternateViews.Add(altView);
                    mail.To.Add(remetente);

                    foreach (string item in copiasEmail)
                    {
                        using (ControleGUsuario controle = new ControleGUsuario())
                        {
                            GUSUARIO usr = controle.Find(Convert.ToInt16(item));
                            if (usr != null && !string.IsNullOrEmpty(usr.EMAIL))
                                mail.CC.Add(new MailAddress(usr.EMAIL, usr.NOME));
                        }
                    }

                    string host = Criptografia.Decrypt(FormPrincipal.getUsuarioAcesso().HOST);
                    int porta =
                        Convert.ToInt32(
                            Criptografia.Decrypt(FormPrincipal.getUsuarioAcesso().PORTA)
                        );

                    using (SmtpClient smtp = new SmtpClient(host, porta))
                    {
                        smtp.EnableSsl = FormPrincipal.getUsuarioAcesso().SSLHABILITADO;
                        smtp.UseDefaultCredentials = false;
                        smtp.DeliveryFormat = SmtpDeliveryFormat.International;
                        smtp.Timeout = 200000;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        smtp.Credentials = new NetworkCredential
                        {
                            UserName = FormPrincipal.getUsuarioAcesso().EMAIL,
                            Password = Criptografia.Decrypt(FormPrincipal.getUsuarioAcesso().SENHAEMAIL)
                        };

                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                GerarLog("E-mails não foram enviados." + Environment.NewLine + 
                    $"Motivo: {ex?.InnerException?.Message ?? ex?.InnerException?.InnerException?.Message ?? ex?.Message}");
            }
        }

        public static decimal ToZeroDecimal(this object valor)
        {
            if (valor == null || valor == DBNull.Value || !decimal.TryParse(Convert.ToString(valor), out decimal vlrValido))
                return 0.00m;

            return vlrValido;
        }

        public static string DBNullToString(this object valor)
        {
            if (valor == null || valor == DBNull.Value)
                return null;

            return Convert.ToString(valor);
        }

        public static string HorsToMinute(string hora)
        {
            string[] configs = new string[5];
            int horas, minutos;

            using (StreamReader rd = new StreamReader(hora))
            {
                int cont = 0;

                while (!rd.EndOfStream)
                {
                    string linha = rd.ReadLine();

                    if (!String.IsNullOrEmpty(linha))
                    {
                        //Pega apenas os valores de cada 'variável' da linha
                        string[] dados = linha.Split(';');

                        //armazena os valores em uma posição de 'configs'
                        configs[cont] = dados[1];
                    }
                    cont++;
                }
                //armazena os dados lidos do arquivo de configuração na classe
                horas = (Convert.ToInt16(configs[0]) * 60);
                minutos = Convert.ToInt16(configs[1]);
            }

            return Convert.ToString(horas + minutos);
        }

        public static string DateTimeToStringSocial(this object valor)
        {
            if (valor == null || valor == DBNull.Value ||
                string.IsNullOrEmpty(valor.ToString().Replace("/", "").Trim()))
                return null;

            return Convert.ToDateTime(valor).ToString("yyyy-MM");
        }

        public static void GeraLogEnvio(Log log)
        {
            try
            {
                if (!Directory.Exists(dirLogs))
                    Directory.CreateDirectory(dirLogs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            GUSUARIO usr = FormPrincipal.getUsuarioAcesso();
            DateTime data = DateTime.Now;
            string nomeArquivo = dirLogs + @"/Email_" + data.ToString("dd-MM-yyyy_hh-mm-ss") + ".txt";
            string cabecalho =
            "--------------------------------------------------------------------" +
            "\r\n - " + Global.AssemblyTitle + " - " + Global.AssemblyDescription +
            "\r\n - LOG DE ENVIO DE EMAIL " +
            "\r\n - Data : " + data.ToString("dd/MM/yyyy hh:mm:ss") +
            "\r\n - Usuário : " + usr.CODUSUARIO + " - " + usr.LOGIN +
            "\r\n - Estatísticas do envio : \r\n" +
            "     Tempo de Execução : " + log.Tempo + "\r\n" +
            "     Total : " + log.NumTotal + " \r\n" +
            "     Sucesso(s) : " + log.NumSucessos + " \r\n" +
            "     Erro(s) : " + log.NumErros + " \r\n" +
            "--------------------------------------------------------------------";

            try
            {
                using (StreamWriter escreve = new StreamWriter(nomeArquivo))
                {
                    escreve.WriteLine(cabecalho);

                    foreach (string msg in log.Mensagens)
                        escreve.WriteLine(msg);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void GerarLog(string msg)
        {
            try
            {
                if (!Directory.Exists(dirLogs))
                    Directory.CreateDirectory(dirLogs);

                using (StreamWriter writer =
                    new StreamWriter(($@"{dirLogs}_{DateTime.Now.ToString("ddMMyyyy")}.txt"), true))
                {
                    writer.Write(msg);
                }
            }
            catch (Exception e)
            {
                MsgErro($"Erro ao gerar Log.\nErro -> {e.Message}");
            }
        }

        public static void RemoveTagsNulas(this XmlDocument docXml)
        {
            XmlNamespaceManager mgr = new XmlNamespaceManager(docXml.NameTable);
            mgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            XmlNodeList nullFields = docXml.SelectNodes("//*[@xsi:nil='true']", mgr);
            if (nullFields != null && nullFields.Count > 0)
            {
                for (int i = 0; i < nullFields.Count; i++)
                {
                    nullFields[i].ParentNode.RemoveChild(nullFields[i]);
                }
            }

            XmlNodeList emptyElements = docXml.SelectNodes(@"//*[not(node())]");
            for (int i = 0; i < emptyElements.Count; i++)
            {
                emptyElements[i].ParentNode.RemoveChild(emptyElements[i]);
            }

            XmlNodeList emptyElements2 = docXml.SelectNodes(@"//*[count(@*) = 0 and . = '']");
            for (int i = 0; i < emptyElements2.Count; i++)
            {
                emptyElements2[i].ParentNode.RemoveChild(emptyElements2[i]);
            }
        }

        public static string RemovePVTEsp(string texto)
        {
            string novoTxt;

            novoTxt = texto.Replace(".", "").Replace(",", "")
                .Replace("/", "").Replace("-", "").Trim();

            return novoTxt;
        }

        public static char? DBNullToChar(this object valor)
        {
            if (valor == null || valor == DBNull.Value)
                return (char?)null;

            return Convert.ToChar(valor);
        }

        public static decimal? DBNullToDecimal(this object valor)
        {
            if (valor == null || valor == DBNull.Value || !decimal.TryParse(valor.ToString(), out decimal valorValido))
                return null;

            return valorValido;
        }

        public static DateTime? DBNullToDateTime(this object valor)
        {
            if (valor == null || valor == DBNull.Value || !DateTime.TryParse(valor.ToString(), out DateTime dataValida))
                return null;

            return dataValida;
        }

        public static int? DBNullToInt(this object valor)
        {
            if (valor == null || valor == DBNull.Value || !int.TryParse(valor.ToString(), out int valorValido))
                return null;

            return valorValido;
        }

        public static long? DBNullToLong(this object valor)
        {
            if (valor == null || valor == DBNull.Value || !long.TryParse(valor.ToString(), out long valorValido))
                return null;

            return valorValido;
        }

        public static string DBNullToDateTimeString(this object valor)
        {
            if (valor == null || valor == DBNull.Value)
                return null;

            if (DateTime.TryParse(valor.ToString(), out DateTime dataValida))
                return dataValida.ToString("dd/MM/yyyy");

            return Convert.ToString(valor);
        }

        public static object ToNullableParam(this object parametro)
        {
            if (parametro == null || String.IsNullOrEmpty(Convert.ToString(parametro)))
                return DBNull.Value;

            return parametro;
        }

        public static string DBNullToStringDateTime(this object obj)
        {
            if (obj == null || obj == DBNull.Value || !DateTime.TryParse(Convert.ToString(obj), out DateTime dataValida))
                return null;

            return dataValida.ToString("yyyy-MM-dd");
        }

        public static short? DBNullToSmallint(this object obj)
        {
            if (obj == null || obj == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
                return (short?)null;

            return Convert.ToInt16(obj);
        }

        public static byte[] DBNullToBytes(this object obj)
        {
            if (obj == null || obj == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
                return (byte[])null;

            return (byte[])(obj);
        }

        public static bool? DBNullToBoolean(this object obj)
        {
            if (obj == null || obj == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
                return (bool?)null;

            return Convert.ToBoolean(obj);
        }

        public static string DBNullToXmlString(this object obj)
        {
            if (obj == null || obj == DBNull.Value || string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
                return null;

            return Convert.ToString(obj).Replace("'", "").Replace("\"", "");
        }

        /// <summary>
        /// Converte uma Imagem para um Array de Bytes
        /// </summary>
        /// <param name="imageIn">Imagem</param>
        /// <returns>Array de Bytes</returns>
        public static byte[] ToByteArray(this Image imageIn)
        {
            if (imageIn == null) return null;
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, imageIn.RawFormat);
            return ms.ToArray();
        }

        /// <summary>
        /// Converte um Array de Bytes em Imagem
        /// </summary>
        /// <param name="byteArrayIn">Array de Bytes</param>
        /// <returns>Imagem</returns>
        public static Image ToImage(this byte[] byteArrayIn)
        {
            try
            {
                if (byteArrayIn == null) return null;
                MemoryStream ms = new MemoryStream(byteArrayIn);
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch
            {
                return null;
            }
        }

        public static object CheckNull(this object obj)
        {
            if (obj != null && obj != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
                return null;

            return obj;
        }

        public static T DBNullToEnum<T>(this object obj)
        {
            if (obj != null && obj != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(obj).Trim()))
            {
                var info = typeof(T);
                if (info.IsEnum)
                {
                    T resultado = (T)Enum.Parse(typeof(T), obj.DBNullToString(), true);
                    return resultado;
                }
            }

            return default;
        }

        #region "Informações do Assembly"
        public static string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public static string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public static string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public static string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public static string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public static string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        #region "Teste e Conexão DB"
        public static bool TestarConexao(string strConexao)
        {
            SqlConnection obj = new SqlConnection(strConexao);
            try
            {
                obj.Open();
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                obj.Close();
            }
        }
        #endregion

        #region "Validações"
        public static string FormataData(string data)
        {
            string dataFinal = null;

            if (data.Length >= 10)
                dataFinal = data.Substring(6, 4) + "-" + data.Substring(3, 2) + "-" + data.Substring(0, 2);

            return dataFinal;
        }

        public static void ApenasNumeros(KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != (char)8)
                e.Handled = true;
        }

        //public static ParametroGlobal RetornaParametroGlobal()
        //{
        //    ControleParametroGlobal ctrParam = new ControleParametroGlobal();

        //    return ctrParam.GetAll().SingleOrDefault();
        //}

        public static int GetDayDifFaltas(DateTime dtInicial, DateTime dtFinal)
        {
            if (((dtFinal.Subtract(dtInicial)).Days) == 0)
                return 1;
            else
                return dtFinal.Subtract(dtInicial).Days;
        }

        public static bool ValidaData(string data)
        {
            try
            {
                DateTime dataValida = new DateTime();
                return DateTime.TryParse(data, out dataValida);
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidaAno(string data)
        {
            try
            {
                data = "01-01-" + data;
                DateTime dataValida = new DateTime();
                return DateTime.TryParse(data, out dataValida);
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidaNumero(string num)
        {
            int numValido = 0;
            return int.TryParse(num, out numValido);
        }

        public static bool ValidaDecimal(string num)
        {
            decimal numValido = 0;
            return decimal.TryParse(num, out numValido);
        }

        public static bool ValidaHora(string hora)
        {
            if (hora.Length == 5)
            {
                // separa a hora o minuto e segundo em um array  
                string[] hms = hora.Split(':');

                int horas = int.Parse(hms[0]);
                int minutos = int.Parse(hms[1]);

                if ((horas < 0) || (horas > 23) || (minutos < 0) || (minutos > 60))
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidaCargaHoraria(string hora, out decimal valorEmHoras)
        {
            try
            {
                if (!String.IsNullOrEmpty(hora))
                {
                    if (hora.Length == 6)
                    {
                        // separa a hora o minuto e segundo em um array  
                        string[] hms = hora.Split(':');
                        decimal horas;
                        decimal minutos;

                        if (hms.Length == 2)
                        {
                            bool horaValida = decimal.TryParse(hms[0], out horas);
                            bool minutoValido = decimal.TryParse(hms[1], out minutos);

                            if (horaValida && minutoValido)
                            {
                                if ((horas < 0) || (hms[0].Length < 3) || (minutos < 0) || (hms[1].Length < 2) || (minutos > 59))
                                {
                                    valorEmHoras = 0;
                                    return false;
                                }
                                else
                                {
                                    decimal minutosHora = (minutos / Convert.ToDecimal(60));
                                    horas = (horas + minutosHora);
                                    valorEmHoras = horas;
                                    return true;
                                }
                            }
                            else
                            {
                                valorEmHoras = 0;
                                return false;
                            }
                        }
                        else
                        {
                            valorEmHoras = 0;
                            return false;
                        }
                    }
                    else
                    {
                        valorEmHoras = 0;
                        return false;
                    }
                }
                else
                {
                    valorEmHoras = 0;
                    return false;
                }
            }
            catch (DivideByZeroException)
            {
                valorEmHoras = 0;
                return false;

            }
        }

        public static bool ValidaEmail(string email)
        {
            bool Valido = false;
            Regex regEx = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$", RegexOptions.IgnoreCase);
            Valido = regEx.IsMatch(email);

            return Valido;
        }

        public static bool ValidaCEP(string cep)
        {
            if (cep.Length == 8)
                cep = cep.Substring(0, 5) + "-" + cep.Substring(5, 3);

            return Regex.IsMatch(cep, ("[0-9]{5}-[0-9]{3}"));
        }

        public static bool ValidaCPF(string CPF)
        {
            int[] CalcARR = null;
            int Sum = 0;
            int DV1 = 0;
            int DV2 = 0;

            CPF = (CPF.Replace(".", null)).Replace("-", null);

            if (string.IsNullOrEmpty(CPF))
            {
                return true;
            }

            if (long.Parse(CPF) == 0)
            {
                return false;
            }

            if (CPF.Length != 11)
            {
                CPF = string.Format("{0:D11}", long.Parse(CPF));
            }

            CalcARR = new int[11];

            for (int x = 0; x < CalcARR.Length; x++)
            {
                CalcARR[x] = int.Parse(CPF[x].ToString());
            }

            Sum = 0;

            for (int x = 1; x <= 9; x++)
            {
                Sum += CalcARR[x - 1] * (11 - x);
            }

            Math.DivRem(Sum, 11, out DV1);
            DV1 = 11 - DV1;
            DV1 = DV1 > 9 ? 0 : DV1;

            if (DV1 != CalcARR[9])
            {
                return false;
            }

            Sum = 0;

            for (int x = 1; x <= 10; x++)
            {
                Sum += CalcARR[x - 1] * (12 - x);
            }

            Math.DivRem(Sum, 11, out DV2);
            DV2 = 11 - DV2;
            DV2 = DV2 > 9 ? 0 : DV2;

            if (DV2 != CalcARR[10])
            {
                return false;
            }

            return true;
        }

        public static bool ValidaCNPJ(string CNPJ)
        {
            CNPJ = ((CNPJ.Replace(".", "")).Replace("/", "")).Replace("-", "");

            int[] CalcARR = null;
            int Sum = 0;
            int Multp = 0;
            int DV1 = 0;
            int DV2 = 0;

            if (string.IsNullOrEmpty(CNPJ))
            {
                return true;
            }

            if (long.Parse(CNPJ) == 0)
            {
                return false;
            }

            if (CNPJ.Length != 14)
            {
                CNPJ = string.Format("{0:D14}", long.Parse(CNPJ));
            }

            CalcARR = new int[14];

            for (int x = 0; x < CalcARR.Length; x++)
            {
                CalcARR[x] = int.Parse(CNPJ[x].ToString());
            }

            Multp = 5;
            Sum = 0;

            for (int x = 0; x < 12; x++)
            {
                Sum += CalcARR[x] * Multp;
                Multp--;
                if (Multp < 2)
                {
                    Multp = 9;
                }
            }

            Math.DivRem(Sum, 11, out DV1);

            if (DV1 < 2)
            {
                DV1 = 0;
            }
            else
            {
                DV1 = 11 - DV1;
            }

            if (DV1 != CalcARR[12])
            {
                return false;
            }

            Multp = 6;
            Sum = 0;

            for (int x = 0; x < 13; x++)
            {
                Sum += CalcARR[x] * Multp;
                Multp--;
                if (Multp < 2)
                {
                    Multp = 9;
                }
            }
            Math.DivRem(Sum, 11, out DV2);

            if (DV2 < 2)
            {
                DV2 = 0;
            }
            else
            {
                DV2 = 11 - DV2;
            }

            if (DV2 != CalcARR[13])
            {
                return false;
            }

            return true;
        }
        #endregion

        #region "Mensagens"
        public static DialogResult MsgInformacao(string msg)
        {
            return MessageBox.Show(msg, "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult MsgAlerta(string msg)
        {
            return MessageBox.Show(msg, "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult MsgSucesso(string msg)
        {
            return MessageBox.Show(msg, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult MsgConfirmacao(string caption, string msg)
        {
            return MessageBox.Show(msg, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Information);
        }

        public static DialogResult MsgErro(string msg)
        {
            return MessageBox.Show(msg, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult MsgSemPermissao()
        {
            return MessageBox.Show("Esta conta não possui permissão para executar esta ação.",
                "Acesso negado", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult NovoRegistro()
        {
            return MessageBox.Show("Deseja adicionar outro registro?",
                "Novo registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static DialogResult FecharJanela()
        {
            return MessageBox.Show("Deseja fechar esta janela?",
                "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static DialogResult FecharJanelaEdicao()
        {
            return MessageBox.Show("Fechar janela de edição?",
                "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        public static DialogResult MsgBloqueioExclusao(Exception ex)
        {
            return MessageBox.Show("Não foi possível excluir o registro selecionado, " +
                "pois este está relacionado a outro(s) dado(s).\n\nErro original:\n" + ex.Message,
                "Erro ao excluir registro", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        #region "Imagem para banco de dados"
        public static byte[] EncriptFoto(string img)
        {
            byte[] bytesImagem = null;
            FileStream fstream = new FileStream(img, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fstream);
            bytesImagem = br.ReadBytes((int)fstream.Length);

            return CompressBytes(bytesImagem);
        }

        public static Image DescriptImagem(byte[] bytesFoto)
        {
            Image img = null;
            MemoryStream mstream = new MemoryStream(DecompressBytes(bytesFoto));
            img = Image.FromStream(mstream);
            mstream.Close();

            return img;
        }
        #endregion

        #region "PDF para banco de dados"
        public static byte[] EncriptPDF(string pdf)
        {
            byte[] arrayDeBytes;

            using (Stream stream = new FileStream(pdf, FileMode.Open))
            {
                arrayDeBytes = new byte[stream.Length + 1];
                stream.Read(arrayDeBytes, 0, arrayDeBytes.Length);
            }

            return CompressBytes(arrayDeBytes);
        }

        public static string DescriptPDF(byte[] bytesPDF, string nomeArquivo)
        {
            byte[] bytesArquivo = (bytesPDF);

            string sFile = nomeArquivo;
            File.WriteAllBytes(sFile, DecompressBytes(bytesArquivo));

            return sFile;
        }
        #endregion

        #region "Compactação de Bytes[]"
        public static byte[] CompressBytes(byte[] data)
        {
            // Fonte: http://stackoverflow.com/a/271264/194717
            using (var compressedStream = new MemoryStream())
            using (var zipStream = new GZipStream(compressedStream, CompressionMode.Compress))
            {
                zipStream.Write(data, 0, data.Length);
                zipStream.Close();
                return compressedStream.ToArray();
            }
        }

        public static byte[] DecompressBytes(byte[] bSource)
        {
            // Fonte: http://stackoverflow.com/questions/6350776/help-with-programmatic-compression-decompression-to-memorystream-with-gzipstream
            using (var inStream = new MemoryStream(bSource))
            using (var gzip = new GZipStream(inStream, CompressionMode.Decompress))
            using (var outStream = new MemoryStream())
            {
                gzip.CopyTo(outStream);
                return outStream.ToArray();
            }
        }
        #endregion

        public static string RemoveAcentos(this string palavra)
        {
            return Regex.Replace(palavra, "[^a-zA-Z]+", "");
        }

        /// <summary>
        /// Resize image with a directory as source
        /// </summary>
        /// <param name="OriginalFileLocation">Image location</param>
        /// <param name="heigth">new height</param>
        /// <param name="width">new width</param>
        /// <param name="keepAspectRatio">keep the aspect ratio</param>
        /// <param name="getCenter">return the center bit of the image</param>
        /// <returns>image with new dimentions</returns>
        public static Image ResizeImageFromFile(String OriginalFileLocation, int width, int heigth, Boolean keepAspectRatio, Boolean getCenter)
        {
            int newheigth = heigth;
            System.Drawing.Image FullsizeImage = System.Drawing.Image.FromFile(OriginalFileLocation);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

            if (keepAspectRatio || getCenter)
            {
                int bmpY = 0;
                double resize = (double)FullsizeImage.Width / (double)width;//get the resize vector
                if (getCenter)
                {
                    bmpY = (int)((FullsizeImage.Height - (heigth * resize)) / 2);// gives the Y value of the part that will be cut off, to show only the part in the center
                    Rectangle section = new Rectangle(new Point(0, bmpY), new Size(FullsizeImage.Width, (int)(heigth * resize)));// create the section to cut of the original image
                    //System.Console.WriteLine("the section that will be cut off: " + section.Size.ToString() + " the Y value is minimized by: " + bmpY);
                    Bitmap orImg = new Bitmap((Bitmap)FullsizeImage);//for the correct effect convert image to bitmap.
                    FullsizeImage.Dispose();//clear the original image
                    using (Bitmap tempImg = new Bitmap(section.Width, section.Height))
                    {
                        Graphics cutImg = Graphics.FromImage(tempImg);//              set the file to save the new image to.
                        cutImg.DrawImage(orImg, 0, 0, section, GraphicsUnit.Pixel);// cut the image and save it to tempImg
                        FullsizeImage = tempImg;//save the tempImg as FullsizeImage for resizing later
                        orImg.Dispose();
                        cutImg.Dispose();
                        return FullsizeImage.GetThumbnailImage(width, heigth, null, IntPtr.Zero);
                    }
                }
                else newheigth = (int)(FullsizeImage.Height / resize);//  set the new heigth of the current image
            }//return the image resized to the given heigth and width
            return FullsizeImage.GetThumbnailImage(width, newheigth, null, IntPtr.Zero);
        }

        public static bool VerificaProgramaEmExecucao()
        {
            return Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1;
        }

        public static string PastaTemp()
        {
            string pastaTemp = "temp";

            try
            {
                if (!Directory.Exists(pastaTemp))
                {
                    Directory.CreateDirectory(pastaTemp);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            return pastaTemp;
        }

        //Adicionar no evento TextChanged

        public static void MoedaMask(ref TextBox txt)
        {
            string n = string.Empty;
            double valor = 0;

            try
            {
                n = txt.Text.Replace(",", "").Replace(".", "");

                if (n.Equals(""))
                    n = "";

                n = n.PadLeft(3, '0');

                if (n.Length > 3 & n.Substring(0, 1) == "0")
                    n = n.Substring(1, n.Length - 1);

                valor = Convert.ToDouble(n) / 100;
                txt.Text = string.Format("{0:N}", valor);
                txt.SelectionStart = txt.Text.Length;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void MoedaMask(ref ToolStripTextBox txt)
        {
            string n = string.Empty;
            double valor = 0;

            try
            {
                n = txt.TextBox.Text.Replace(",", "").Replace(".", "");

                if (n.Equals(""))
                    n = "";

                n = n.PadLeft(3, '0');

                if (n.Length > 3 & n.Substring(0, 1) == "0")
                    n = n.Substring(1, n.Length - 1);

                valor = Convert.ToDouble(n) / 100;
                txt.TextBox.Text = string.Format("{0:N}", valor);
                txt.TextBox.SelectionStart = txt.TextBox.Text.Length;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        public static void MoedaMask(ref MaskedTextBox txt)
        {
            string n = string.Empty;
            double valor = 0;

            try
            {
                n = txt.Text.Replace(",", "").Replace(".", "");

                if (n.Equals(""))
                    n = "";

                n = n.PadLeft(3, '0');

                if (n.Length > 3 & n.Substring(0, 1) == "0")
                    n = n.Substring(1, n.Length - 1);

                valor = Convert.ToDouble(n) / 100;
                txt.Text = string.Format("{0:N}", valor);
                txt.SelectionStart = txt.Text.Length;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        #region "Enum"
        public static string ToStrSexo(this string charSexo)
        {
            string sexo = null;

            switch (charSexo.ToUpper())
            {
                case "M":
                    sexo = "Masculino";
                    break;

                case "F":
                    sexo = "Feminino";
                    break;

                default:
                    sexo = "Indefinido";
                    break;
            }

            return sexo;
        }

        /// <summary>
        /// Obtém a descrição de um determinado Enumerador.
        /// Tenta retornar a descrição indicada por Description (ComponentModel) e caso não exista, retorna o proprio nome do Enumerador
        /// </summary>
        /// <param name="e">Enumerador que terá a descrição obtida.</param>
        /// <returns>String com a descrição do Enumerador.</returns>
        public static string EnumDescription(this Enum e)
        {
            Type t = e.GetType();
            DescriptionAttribute[] att = { };

            if (Enum.IsDefined(t, e))
            {
                FieldInfo fieldInfo = t.GetField(e.ToString());
                att = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            }
            return (att.Length > 0 ? att[0].Description ?? "Nulo" : e.ToString());
        }

        /// <summary>
        /// Retorna uma lista com os valores/nomes do Enum
        /// </summary>
        /// <param name="t">O Enum passado como tipo typeof(EnumNome)</param>
        /// <returns>Um KeyValuePair com valores (int) e nomes (string) do Enum</returns>
        public static IList EnumList(Type t)
        {
            ArrayList ret = new ArrayList();
            if (t != null)
            {
                Array enumValores = Enum.GetValues(t);
                foreach (Enum valor in enumValores)
                {
                    ret.Add(new KeyValuePair<int, string>(Convert.ToInt32(valor), EnumDescription(valor)));
                }
            }

            return (ret);
        }
        #endregion

        public static int GetYearDiff(this DateTime data)
        {
            int result = DateTime.Now.Year - data.Year;

            if (DateTime.Now.Month < data.Month || (DateTime.Now.Month == data.Month && DateTime.Now.Day < data.Day))
            {
                result--;
            }

            return result;
        }

        public static string NrInscToNatJuridica(string natJuridica, string tpInsc, string nInsc)
        {
            string nrInsc = string.Empty;

            if (!string.IsNullOrEmpty(natJuridica) && !string.IsNullOrEmpty(nInsc))
            {
                if ((natJuridica == "1015" || natJuridica == "1040" ||
                    natJuridica == "1074" || natJuridica == "1163") &&
                    tpInsc == "1")
                    nrInsc = nInsc;
                else
                    nrInsc = nInsc.Substring(0, 8);
            }

            return nrInsc;
        }

        /// <summary>
        /// Navega para uma rota composta por um ou mais pontos geográficos
        /// </summary>
        /// <param name="points">
        /// Um array/coleção contendo uma lista de pontos geográficos necessários para se criar a rota. Os pontos deve estar ordenados do inicial para o final!
        /// </param>
        public static void NavigateToRoute(IEnumerable<string> points, WebBrowser webBrowser1)
        {
            string baseAdress = "https://www.google.com.br/maps/dir/";

            StringBuilder url = new StringBuilder(baseAdress);
            foreach (string point in points)
            {
                url.Append(Uri.EscapeDataString(point));
                url.Append("/");
            }

            webBrowser1.Navigate(new Uri(url.ToString()));
        }

        /// <summary>
        /// Este método serve para corrigir problemas de script relacionados ao WebBrowser, 
        /// e deve ser chamado na inicialização do form!
        /// </summary>
        private static void FixBrowser()
        {
            try
            {
                Microsoft.Win32.RegistryKey regDM = null;
                bool is64 = Environment.Is64BitOperatingSystem;
                string KeyPath = "";
                if (is64)
                {
                    KeyPath = "SOFTWARE\\Wow6432Node\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION";
                }
                else
                {
                    KeyPath = "SOFTWARE\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION";
                }

                regDM = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(KeyPath, false);
                if (regDM == null)
                {
                    regDM = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(KeyPath);
                }

                Microsoft.Win32.RegistryKey Sleutel = null;
                if ((regDM != null))
                {
                    string location = System.Environment.GetCommandLineArgs()[0];
                    string appName = System.IO.Path.GetFileName(location);
                    if (regDM.GetValue(appName) == null)
                    {
                        //Sleutel onbekend
                        regDM = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(KeyPath, true);
                        Sleutel = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(KeyPath, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);

                        //What OS are we using
                        Version OsVersion = System.Environment.OSVersion.Version;

                        if (OsVersion.Major == 6 & OsVersion.Minor == 1)
                        {
                            //WIN 7
                            Sleutel.SetValue(appName, 9000, Microsoft.Win32.RegistryValueKind.DWord);
                        }
                        else if (OsVersion.Major == 6 & OsVersion.Minor == 2)
                        {
                            //WIN 8
                            Sleutel.SetValue(appName, 10000, Microsoft.Win32.RegistryValueKind.DWord);
                        }
                        else if (OsVersion.Major == 5 & OsVersion.Minor == 1)
                        {
                            //WIN xp
                            Sleutel.SetValue(appName, 8000, Microsoft.Win32.RegistryValueKind.DWord);
                        }

                        Sleutel.Close();
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}