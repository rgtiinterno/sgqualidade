﻿namespace QUALIDADE.Dominio.Email
{
    internal class AlertaCalibracao
    {
        public short CodColigada { get; set; }
        public string CodCCusto { get; set; }
        public int Total { get; set; }
        public int Aprovadas { get; set; }
        public int Reprovadas { get; set; }
        public int Pendentes { get; set; }
        public int Vencidas { get; set; }
        public int Vencem7Dias { get; set; }
    }
}