﻿namespace QUALIDADE.Dominio.Email
{
    internal class AlertaAfericao
    {
        public short CodColigada { get; set; }
        public string CodCCusto { get; set; }
        public int Total { get; set; }
        public int Conformes { get; set; }
        public int NaoConformes { get; set; }
        public int Pendentes { get; set; }
        public int Vencidas { get; set; }
        public int Vencem7Dias { get; set; }
    }
}