﻿namespace QUALIDADE.Dominio.Email
{
    internal class AlertaFornecedor
    {
        public short CodColigada { get; set; }
        public string CodCCusto { get; set; }
        public short CodColFornecedor { get; set; }
        public string CodFornecedor { get; set; }
        public string Fornecedor { get; set; }
        public string CgcFornecedor { get; set; }
        public int Requisitados { get; set; }
        public int Pendentes { get; set; }
        public int Vencidos { get; set; }
        public int Vencem7Dias { get; set; }
    }
}