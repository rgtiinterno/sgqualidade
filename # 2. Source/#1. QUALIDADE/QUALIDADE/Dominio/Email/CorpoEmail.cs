﻿using System.Collections.Generic;

namespace QUALIDADE.Dominio.Email
{
    internal class CorpoEmail
    {
        internal int CodUsuario { get; set; }
        internal string NomeUsuario { get; set; }
        internal string EmailUsuario { get; set; }
        internal string Titulo { get; set; }
        internal string MsgCompl { get; set; }
        internal string EmailsCopia { get; set; }
        internal List<CentroCustoEmail> CentroCustoEmail { get; set; }
    }

    internal class CentroCustoEmail
    {
        public short CodColigada { get; set; }
        public string CodCentroCusto { get; set; }
        public string NomeCentroCusto { get; set; }
        internal AlertaAfericao InfAfericao { get; set; }
        internal AlertaCalibracao InfCalibracao { get; set; }
        internal List<AlertaFornecedor> InfFornecedores { get; set; }
    }
}
