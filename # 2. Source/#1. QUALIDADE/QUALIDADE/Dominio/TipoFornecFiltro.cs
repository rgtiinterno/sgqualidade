﻿namespace QUALIDADE.Dominio
{
    public class TipoFornecFiltro
    {
        public int CodTpFornec { get; set; }
        public string Descricao { get; set; }
        public string DescCombo
        {
            get
            {
                return $"{CodTpFornec} - {Descricao}";
            }
        }
    }
}