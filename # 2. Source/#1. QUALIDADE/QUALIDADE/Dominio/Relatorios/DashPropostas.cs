﻿namespace QUALIDADE.Dominio.Relatorios
{
    public class DashPropostas
    {
        // Valores
        public double EixoY { get; set; }

        // Descrição
        public string EixoX { get; set; }
    }

    public class DashPropostasTable
    {
        public int Propostas_Enviadas { get; set; }
        public int Propostas_Vencidadas { get; set; }
        public string Cliente { get; set; }
    }

    public class DashPropostasTableGeral
    {
        public int Propostas_Enviadas { get; set; }
        public int Propostas_Vencidadas { get; set; }
    }


    public class Filtro
    {
        public int inicio { get; set; }
        public int fim { get; set; }
        public short? cliente { get; set; }
    }
}
