﻿using System;

namespace QUALIDADE.Dominio.Relatorios
{
    public class RelNConformidade
    {
        public int CODCOLIGADA { get; set; }
        public string CGCEMPRESA { get; set; }
        public string NOMEEMPRESA { get; set; }
        public byte[] LOGOEMPRESA { get; set; }
        public int CODNCONFORM { get; set; }
        public string CODCCUSTO { get; set; }
        public string TPACAO { get; set; }
        public System.DateTime DATAABERTURANC { get; set; }
        public string NCONFREALPONTENCIAL { get; set; }
        public string ACAOIMEDTOMADA { get; set; }
        public string EVDOBJETIVA { get; set; }
        public bool NECACAOCORPREV { get; set; }
        public string PLANOACAOQUE { get; set; }
        public string PLANOACAOQUEM { get; set; }
        public Nullable<System.DateTime> PLANOACAOQUANDO { get; set; }
        public string EVDTRATATIVA { get; set; }
        public string ANLISECAUSA1 { get; set; }
        public string ANLISECAUSA2 { get; set; }
        public string ANLISECAUSA3 { get; set; }
        public bool VERIFICACAOPLANOACAO { get; set; }
        public bool VERIFICACAOPLANOEFICACIA { get; set; }
        public bool FINALIZADORELATOR { get; set; }
        public bool FINALIZADORESPONSAVEL { get; set; }
        public string DESCEXTRATIF { get; set; }
        public string DESCCLASSIF { get; set; }
        public string DESCSTATUS { get; set; }
        public string NOMERALATOR { get; set; }
        public string DESCEXECUTOR { get; set; }
        public string RECCREATEDBY { get; set; }
        public Nullable<System.DateTime> RECCREATEDON { get; set; }
        public string RECMODIFIEDBY { get; set; }
        public Nullable<System.DateTime> RECMODIFIEDON { get; set; }
    }
}
