﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;

namespace QUALIDADE.Dominio.Relatorios
{
    public class Graficos
    {
        public string Resumo { get; set; }
        public short Valor { get; set; }

        public static List<Graficos> GetList()
        {
            return new List<Graficos>
            {
                new Graficos{ Resumo = "Gráfico de pontos", Valor = 0},
                new Graficos{ Resumo = "Gráfico de FastPoint", Valor = 1},
                new Graficos{ Resumo = "Gráfico de bolhas", Valor = 2},
                new Graficos{ Resumo = "Gráfico de linha", Valor = 3},
                new Graficos{ Resumo = "Gráfico de spline", Valor = 4},
                new Graficos{ Resumo = "Gráfico de StepLine", Valor = 5},
                new Graficos{ Resumo = "Gráfico de FastLine", Valor = 6},
                new Graficos{ Resumo = "Gráfico de barra", Valor = 7},
                new Graficos{ Resumo = "Gráfico de barras empilhadas", Valor = 8},
                new Graficos{ Resumo = "Gráfico de barras 100% empilhadas", Valor = 9},
                new Graficos{ Resumo = "Gráfico de coluna", Valor = 10},
                new Graficos{ Resumo = "Gráfico de colunas empilhadas", Valor = 11},
                new Graficos{ Resumo = "Gráfico de colunas 100% empilhadas", Valor = 12},
                new Graficos{ Resumo = "Gráfico de área", Valor = 13},
                new Graficos{ Resumo = "Gráfico de área de spline", Valor = 14},
                new Graficos{ Resumo = "Gráfico de área empilhada", Valor = 15},
                new Graficos{ Resumo = "Gráfico de áreas 100% empilhadas", Valor = 16},
                new Graficos{ Resumo = "Gráfico de pizza", Valor = 17},
                new Graficos{ Resumo = "Gráfico de rosca", Valor = 18},
                new Graficos{ Resumo = "Gráfico de ações", Valor = 19},
                new Graficos{ Resumo = "Gráfico de velas", Valor = 20},
                new Graficos{ Resumo = "Gráfico de intervalo", Valor = 21},
                new Graficos{ Resumo = "Gráfico de intervalo spline", Valor = 22},
                new Graficos{ Resumo = "Gráfico de RangeBar", Valor = 23},
                new Graficos{ Resumo = "Gráfico de coluna de intervalo", Valor = 24},
                new Graficos{ Resumo = "Gráfico de radar", Valor = 25},
                new Graficos{ Resumo = "Gráfico de polar", Valor = 26},
                new Graficos{ Resumo = "Gráfico de barras de erros", Valor = 27},
                new Graficos{ Resumo = "Gráfico de caixa", Valor = 28},
                new Graficos{ Resumo = "Gráfico de Renko", Valor = 29},
                new Graficos{ Resumo = "Gráfico de ThreeLineBreak", Valor = 30},
                new Graficos{ Resumo = "Gráfico de Kagi", Valor = 31},
                new Graficos{ Resumo = "Gráfico de PointAndFigure", Valor = 32},
                new Graficos{ Resumo = "Gráfico de funil", Valor = 33},
                new Graficos{ Resumo = "Gráfico de pirâmide", Valor = 34}
            }.OrderBy(x => x.Resumo).ToList();
        }
    }
}
