﻿using System;

namespace QUALIDADE.Dominio.Relatorios
{
    public class SubRelNConformidade
    {
        public int CODNCONFORM { get; set; }
        public int CODCOLIGADA { get; set; }
        public string CODCCUSTO { get; set; }
        public bool CUMPRIDO { get; set; }
        public Nullable<System.DateTime> DATAVERIFPACAO { get; set; }
        public string OBSERVACAOPACAO { get; set; }
        public Nullable<int> CODUSUVERIFPACAO { get; set; }
        public Nullable<System.DateTime> DTLIMITEVEFICACIA { get; set; }
        public bool ACAOEFICAZ { get; set; }
        public Nullable<int> NUMSACP { get; set; }
        public string OBSERVACAOEFIC { get; set; }
        public Nullable<int> CODUSUVERIFEFIC { get; set; }
        public Nullable<System.DateTime> DTVEFICACIACIA { get; set; }
        public string DESCCUSTO { get; set; }
        public string NOMEUSUVERIFPACAO { get; set; }
        public string NOMEUSUVERIFEFIC { get; set; }
        public string RECCREATEDBY { get; set; }
        public Nullable<System.DateTime> RECCREATEDON { get; set; }
        public string RECMODIFIEDBY { get; set; }
        public Nullable<System.DateTime> RECMODIFIEDON { get; set; }
    }
}
