﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QUALIDADE.Dominio
{
    public class Mensagem
    {
        public static DialogResult Erro(string titulo, string mensagem)
        {
            return MessageBox.Show(mensagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult Alerta(string titulo, string mensagem)
        {
            return MessageBox.Show(mensagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static DialogResult Informacao(string titulo, string mensagem)
        {
            return MessageBox.Show(mensagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static DialogResult Confirmacao(string titulo, string mensagem)
        {
            return MessageBox.Show(mensagem, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }
    }
}
