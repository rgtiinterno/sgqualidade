//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QUALIDADE.Dominio
{
    using System;
    using System.Collections.Generic;
    
    public partial class GARQANX
    {
        public int CODEXTERNO { get; set; }
        public int CODCOLIGADA { get; set; }
        public string CODCCUSTO { get; set; }
        public string CODSISTEMA { get; set; }
        public int NUMSEQUENCIAL { get; set; }
        public string NOMEDOC { get; set; }
        public string DESCRICAO { get; set; }
        public byte[] BYTES { get; set; }
        public string EXTENSAO { get; set; }
        public string CAMPOREF { get; set; }
        public string RECCREATEDBY { get; set; }
        public Nullable<System.DateTime> RECCREATEDON { get; set; }
        public string RECMODIFIEDBY { get; set; }
        public Nullable<System.DateTime> RECMODIFIEDON { get; set; }
    
        public virtual GCENTROCUSTO GCENTROCUSTO { get; set; }
        public virtual GSISTEMA GSISTEMA { get; set; }
        public virtual GEMPRESAS GEMPRESAS { get; set; }
    }
}
