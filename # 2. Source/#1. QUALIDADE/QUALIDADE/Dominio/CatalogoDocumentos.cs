﻿using Syncfusion.Windows.Forms.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QUALIDADE.Dominio
{
    public class CatalogoDocumentos
    {
        [Display(Name = "Código")]
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CODIGO { get; set; }

        [Display(Name = "Cód.Item Pai")]
        public string CODITEMPAI { get; set; }

        [Display(Name = "Descrição")]
        [Required, MaxLength(200, ErrorMessage = "Número de Caracteres excedido")]
        public string DESCRICAO { get; set; }

        [NotMapped]
        [Display(Name = "Item Pai")]
        public string DESCITEMPAI { get; set; }

        [Display(Name = "Tipo")]
        public int ORDEM { get; set; }

        [NotMapped]
        public List<CatalogoDocumentos> ITENSFILHOS { get; set; }

        public string CRIADOPOR { get; set; }

        public DateTime? CRIADOEM { get; set; }

        public string MODIFICADOPOR { get; set; }

        public DateTime? MODIFICADOEM { get; set; }
    }

    public class CatalogoNodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            CatalogoDocumentos tx = null;
            if (x is TreeNodeAdv nodeX)
                tx = nodeX.Tag as CatalogoDocumentos;

            CatalogoDocumentos ty = null;
            if (y is TreeNodeAdv nodeY)
                ty = nodeY.Tag as CatalogoDocumentos;

            if (tx.ORDEM != tx.ORDEM)
                return tx.ORDEM - ty.ORDEM;

            return string.Compare(tx.DESCRICAO, ty.DESCRICAO);
        }
    }
}