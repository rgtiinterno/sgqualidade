﻿using QUALIDADE.Controle;
using QUALIDADE.Forms;
using System.Collections.Generic;

namespace QUALIDADE.Dominio
{
    public partial class GUSUARIO
    {
        public List<CCustoFiltro> CentrosCustoPermissao(string codSistema = null)
        {
            List<CCustoFiltro> itens = new List<CCustoFiltro>();
            using (ControleGUsuario controle = new ControleGUsuario())
            {
                itens = controle.GetCCustos(FormPrincipal.getUsuarioAcesso().CODCOLIGADA, this.CODUSUARIO, codSistema);
            }
            return itens;
        }
    }
}