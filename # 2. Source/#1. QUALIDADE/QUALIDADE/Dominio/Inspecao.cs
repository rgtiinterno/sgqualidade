﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUALIDADE.Dominio
{
    public class Inspecao
    {
        public string item { get; set; }
        public string Descricao { get; set; }
        public string Tolerencia { get; set; }
        public string Insp { get; set; }
        public string Acao { get; set; }
        public string Reins { get; set; }
    }
}
