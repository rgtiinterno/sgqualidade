﻿using System;

namespace QUALIDADE.Dominio
{
    internal class CatalogoDocumentosAnexo
    {
        public bool CKSELECIONADO { get; set; }
        public string CODCATALOGO { get; set; }
        public int CODIGOSEQ { get; set; }
        public string CAMINHO { get; set; }
        public string NOMEANEXO { get; set; }
        public byte[] ARQUIVOANEXO { get; set; }
        public string EXTENSAO { get; set; }
        public DateTime? DATAEMISSAO { get; set; }
        public DateTime? DATAREVISAO { get; set; }
    }
}