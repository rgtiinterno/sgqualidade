﻿using System;
using System.Collections.Generic;

namespace QUALIDADE.Dominio
{
    public class Log
    {
        public string Modulo { get; set; }
        public List<string> Mensagens { get; set; }
        public int NumErros { get; set; }
        public int NumAlertas { get; set; }
        public int NumSucessos { get; set; }
        public int NumTotal { get; set; }
        public TimeSpan Tempo { get; set; }

        public Log()
        {
            Mensagens = new List<string>();
        }
    }
}
