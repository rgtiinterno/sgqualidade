﻿using QUALIDADE.Controle;
using QUALIDADE.Controle.Configuracoes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUALIDADE.Dominio
{
    internal class Pergunta
    {
        public int Codigo { get; set; }
        public string DescPergunta { get; set; }
        public bool Sim { get; set; }
        public bool Nao { get; set; }
        public bool NA { get; set; }
        public string NRDO { get; set; }

        public List<Pergunta> GetPerguntas(int codigo, int coligada)
        {
            List<Pergunta> lista = new List<Pergunta>();
            string sql = @"SELECT P.CODIGO, P.PERGUNTA,  
	                            ISNULL(A.SIM,0) AS SIM,  
	                            ISNULL(A.NAO,0) AS NAO,   
	                            CONVERT(VARCHAR(MAX), A.NA) AS NA 
                            FROM PPERGUNTAS P 
                            LEFT JOIN PANALISE A ON (A.CODPERGUNTA=P.CODIGO) 
                            WHERE A.CODCOLIGADA=@CODCOLIGADA AND A.CODPROPOSTA=@CODPROPOSTA ";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODPROPOSTA", codigo), 
                new SqlParameter("@CODCOLIGADA", coligada)
            };

            DataTable dt = Funcoes.RetornaDataTable(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    bool CheckSim = (row["SIM"] is Boolean itemSim) ? itemSim : false;
                    bool CheckNao = (row["NAO"] is Boolean itemNao) ? itemNao : false;

                    lista.Add(new Pergunta
                    {
                        Codigo = Convert.ToInt32(row["CODIGO"]),
                        DescPergunta = row["PERGUNTA"].DBNullToString(),
                        Sim = CheckSim,
                        Nao = CheckNao,
                        NA = (row["NA"].DBNullToString() == "0") ? false : true,
                    });
                }
            }
            else
            {
                using (ControlePPerguntas controle = new ControlePPerguntas())
                {
                    var result = controle.GetAll();

                    foreach (var item in result)
                    {
                        lista.Add(new Pergunta
                        {
                            Codigo = item.CODIGO,
                            DescPergunta = item.PERGUNTA,
                            Sim = false,
                            Nao = false,
                            NA = false
                        });
                    }
                }
            }
            
            return lista;
        }

        public List<Pergunta> GetPerguntasProjetos(int codigo, decimal rev)
        {
            List<Pergunta> lista = new List<Pergunta>();
            string sql = @"SELECT P.CODIGO, P.PERGUNTA,  
	                            ISNULL(A.SIM,0) AS SIM,  
	                            ISNULL(A.NAO,0) AS NAO,   
	                            CONVERT(VARCHAR(MAX), A.NA) AS NA 
                            FROM JPERGUNTAS P 
                            LEFT JOIN JANALISE A ON (A.CODPERGUNTA=P.CODIGO) 
                            WHERE A.REVISAO=@REVISAO AND A.CODPROJETO=@CODPROJETO 
                              AND ISNULL(P.ATIVA,1)=1 ";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODPROJETO", codigo), 
                new SqlParameter("@REVISAO", rev)
            };

            DataTable dt = Funcoes.RetornaDataTable(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    lista.Add(new Pergunta
                    {
                        Codigo = Convert.ToInt32(row["CODIGO"]),
                        DescPergunta = row["PERGUNTA"].DBNullToString(),
                        Sim = Convert.ToBoolean(row["SIM"]),
                        Nao = Convert.ToBoolean(row["NAO"]),
                        NRDO = row["NA"].DBNullToString(),
                    });
                }
            }
            else
            {
                using (ControleJPerguntas controle = new ControleJPerguntas())
                {
                    var result = controle.Get(x => x.ATIVA);

                    foreach (var item in result)
                    {
                        lista.Add(new Pergunta
                        {
                            Codigo = item.CODIGO,
                            DescPergunta = item.PERGUNTA,
                            Sim = false,
                            Nao = false,
                            NRDO = string.Empty
                        });
                    }
                }
            }
            
            return lista;
        }
    }
}
