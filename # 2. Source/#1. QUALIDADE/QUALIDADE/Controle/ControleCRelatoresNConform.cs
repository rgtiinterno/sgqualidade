﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;

namespace QUALIDADE.Controle
{
    public class ControleCRelatoresNConform : Controle<CRELATORESCONFORM>
    {
        public bool Delete(int CODCOLIGADA,  int CODNCONFORM,  string CODCCUSTO)
        {
            string sql = $"DELETE FROM CRELATORESCONFORM " +
                $"WHERE CODCCUSTO='{CODCCUSTO}' and CODCOLIGADA='{CODCOLIGADA}' and CODNCONFORM={CODNCONFORM}";

            return Funcoes.ExecutaFuncao(sql);
        }
    }
}
