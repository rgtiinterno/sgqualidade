﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Data;

namespace QUALIDADE.Controle
{
    public class ControleGEstados : Controle<GESTADOS>
    {
        public List<GESTADOS> GetIntegracaoRM()
        {
            List<GESTADOS> lista = new List<GESTADOS>();

            string query = @"SELECT [CODETD]
                                  ,[NOME]
                                  ,[RECCREATEDBY]
                                  ,[RECCREATEDON]
                                  ,[RECMODIFIEDBY]
                                  ,[RECMODIFIEDON]
                              FROM [dbo].[GETD]";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                GESTADOS estados = new GESTADOS
                {
                    CODETD = row["CODETD"].DBNullToString(),
                    NOME = row["NOME"].DBNullToString(),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(estados);
            }

            return lista;
        }
    }
}
