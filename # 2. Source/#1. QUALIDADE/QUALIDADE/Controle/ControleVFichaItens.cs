﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleVFichaItens : Controle<VFICHAITENS>
    {
        public List<Inspecao> GetList(string CODFICHA, int CODCOLIGADA, string CODMODELO)
        {
            List<Inspecao> lista = new List<Inspecao>();
            var result = from i in GetAll()
                         join m in context.VDESCRICAOMODELO on i.ITEM equals m.ITEM
                         where m.CODMODELO == CODMODELO && i.CODFICHA == CODFICHA &&
                            i.CODCOLIGADA == CODCOLIGADA && i.CODMODELO == CODMODELO 
                         select new
                         {
                             i.CODCOLIGADA, 
                             i.CODFICHA,
                             i.CODMODELO,
                             i.ITEM,
                             m.DESCRICAO, 
                             m.TOLERANCIA, 
                             i.INSPECAO, 
                             i.ACAO, 
                             i.REINSPECAO
                         };

            result.ForEach(x => lista.Add(
                new Inspecao
                {
                    item = x.ITEM,
                    Descricao = x.DESCRICAO, 
                    Tolerencia = x.TOLERANCIA, 
                    Acao = x.ACAO, 
                    Insp = x.INSPECAO, 
                    Reins = x.REINSPECAO
                })
            );

            return lista;
        }

        public bool Delete(string CODFICHA, int CODCOLIGADA, string CODMODELO)
        {
            string sql = @"DELETE FROM VFICHAITENS WHERE CODMODELO=@CODMODELO AND CODCOLIGADA=@CODCOLIGADA AND CODFICHA=@CODFICHA ";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODMODELO", CODMODELO),
                new SqlParameter("@CODCOLIGADA", CODCOLIGADA),
                new SqlParameter("@CODFICHA", CODFICHA)
            };

            return Funcoes.ExecutaFuncao(sql, parameters);
        }

        public bool AllCreate(List<VFICHAITENS> list)
        {
            string sql = @"
                    INSERT INTO [dbo].[VFICHAITENS]
                               ([CODFICHA],[CODCOLIGADA]
                               ,[CODMODELO],[ITEM]
                               ,[INSPECAO],[ACAO]
                               ,[REINSPECAO])
                    VALUES
                               (@CODFICHA, @CODCOLIGADA, @CODMODELO, 
		                       @ITEM, @INSPECAO, @ACAO, @REINSPECAO)";
            List<SqlParameter> parameters;
            try
            {
                foreach (VFICHAITENS item in list)
                {
                    parameters = new List<SqlParameter>();

                    parameters.Add(new SqlParameter("@CODFICHA", item.CODFICHA));
                    parameters.Add(new SqlParameter("@CODCOLIGADA", item.CODCOLIGADA));
                    parameters.Add(new SqlParameter("@CODMODELO", item.CODMODELO));
                    parameters.Add(new SqlParameter("@ITEM", item.ITEM));
                    parameters.Add(new SqlParameter("@INSPECAO", item.INSPECAO));
                    parameters.Add(new SqlParameter("@ACAO", item.ACAO));
                    parameters.Add(new SqlParameter("@REINSPECAO", item.REINSPECAO));

                    Funcoes.ExecutaFuncao(sql, parameters);
                }

                return true;
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex);
                frm.ShowDialog();
                return false;
            }
        }
    }
}