﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleFCFO : Controle<FCFO>
    {
        public short GetID()
        {
            int newId = 0;

            var pais = (from f in GetAll()
                        orderby f.IDCFO descending
                        select f).FirstOrDefault();

            if (pais != null)
            {
                Nullable<int> item = pais.IDCFO;
                if (item != null)
                {
                    newId = Convert.ToInt16(item.Value);
                }
            }
            return Convert.ToInt16((newId + 1));
        }

        public string GetCodigo(short codColigada)
        {
            int newId = 0;

            var pais = (from f in Get(x => x.CODCOLIGADA == codColigada)
                        orderby f.CODCFO descending
                        select f).FirstOrDefault();

            if (pais != null)
            {
                Nullable<int> item = pais.CODCFO.DBNullToInt();
                if (item != null)
                {
                    newId = Convert.ToInt32(item.Value);
                }
            }
            return Convert.ToString((newId + 1)).PadLeft(6, '0');
        }

        public List<FCFO> GetList(short codColigada)
        {
            List<FCFO> list = new List<FCFO>();
            var result = (from f in Get(x => (x.CODCOLIGADA == codColigada || x.CODCOLIGADA == 0))
                          orderby f.CODCFO ascending
                          select f);

            if (result != null)
            {
                using (ControleFTipoArquivo controle = new ControleFTipoArquivo())
                {
                    foreach (var item in result)
                    {
                        if (item.PAGREC == 1)
                        {
                            item.DESCPAGREC = "Cliente";
                        }
                        else if (item.PAGREC == 2)
                        {
                            item.DESCPAGREC = "Fornecedor";
                        }
                        else
                        {
                            item.DESCPAGREC = "Ambos";
                        }

                        if (item.PESSOAFISOUJUR == "F")
                        {
                            item.DESCPESSOAFISOUJUR = "Pessoa Física";
                        }
                        else
                        {
                            item.DESCPESSOAFISOUJUR = "Pessoa Jurídica";
                        }

                        item.STATUS = Convert.ToBoolean(item.ATIVO);
                        item.DESCTIPOFCFO = controle.Find(item.CODTPFCFO)?.DESCRICAO;
                        GetTotalDocVencidosAVencer(item.CODCOLIGADA, item.CODCFO, out int totalVencidos, out int totalAVencer);
                        item.DOCVENCEATE30D = totalAVencer;
                        item.DOCVENCIDO = totalVencidos;
                        list.Add(item);
                    }
                }
            }
            return list;
        }

        public void GetTotalDocVencidosAVencer(int codColigada, string codCfo, out int totalVencidos, out int totalAVencer)
        {
            DataTable dt = Funcoes.RetornaDataTable(
                $@"SELECT 
	                (SELECT COUNT(*) AS DOCVENCEATE30D 
	                FROM [FARQFCFO]
	                WHERE CODCOLIGADA=@CODCOLIGADA AND CODCFO=@CODCFO 
	                    AND POSSUIVALIDADE=1 AND DATAVALIDADE IS NOT NULL
	                    AND DATEDIFF(DAY, GETDATE(), DATAVALIDADE)>0 
	                    AND DATEDIFF(DAY, GETDATE(), DATAVALIDADE)<=30
	                ) AS DOCVENCEATE30D, 
	                (SELECT COUNT(*) AS DOCVENCIDO
	                FROM [FARQFCFO]
	                WHERE CODCOLIGADA=@CODCOLIGADA AND CODCFO=@CODCFO 
	                    AND POSSUIVALIDADE=1 AND DATAVALIDADE IS NOT NULL
	                    AND DATEDIFF(DAY, GETDATE(), DATAVALIDADE)<=0
	                ) AS DOCVENCIDO ",
                new List<System.Data.SqlClient.SqlParameter>
                {
                    new System.Data.SqlClient.SqlParameter("@CODCOLIGADA", codColigada),
                    new System.Data.SqlClient.SqlParameter("@CODCFO", codCfo),
                }
            );
            totalVencidos = 0;
            totalAVencer = 0;
            if(dt != null && dt.Rows.Count > 0)
            {
                totalVencidos = dt.Rows[0]["DOCVENCIDO"].DBNullToInt() ?? 0;
                totalAVencer = dt.Rows[0]["DOCVENCEATE30D"].DBNullToInt() ?? 0;
            }
        }

        public int GetTotalDocAVencer(int codColigada, string codCfo, int codTpCfo)
        {
            DataTable dt = Funcoes.RetornaDataTable(
                $@"SELECT COUNT(*) AS TOTAL FROM FCFO F 
                INNER JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO) 
                INNER JOIN FARQUIVOCFO FA ON(FA.CODCOLIGADA=A.CODCOLIGADA AND FA.CODTPCFO=F.CODTPFCFO AND FA.CODTPARQUIVO=A.CODTPARQUIVO) 
                WHERE F.CODCOLIGADA=@CODCOLIGADA AND F.CODCFO=@CODCFO AND F.CODTPFCFO=@CODTPFCFO 
                  AND A.POSSUIVALIDADE=1 AND DATAVALIDADE IS NOT NULL 
                  AND DATEDIFF(DAY, GETDATE(), DATAVALIDADE)>0 
                  AND DATEDIFF(DAY, GETDATE(), DATAVALIDADE)<=30 
                GROUP BY A.CODCOLIGADA, A.CODCFO, A.CODTPARQUIVO ",
                new List<System.Data.SqlClient.SqlParameter>
                {
                    new System.Data.SqlClient.SqlParameter("@CODCOLIGADA", codColigada),
                    new System.Data.SqlClient.SqlParameter("@CODCFO", codCfo),
                    new System.Data.SqlClient.SqlParameter("@CODTPFCFO", codCfo),
                }
            );
            return dt != null && dt.Rows.Count > 0 ? dt.Rows[0]["TOTAL"].DBNullToInt() ?? 0 : 0;
        }

        public List<FCFO> GetIntegracaoRM(short? codColigada = null)
        {
            List<FCFO> lista = new List<FCFO>();

            string query = $@"SELECT [CODCOLIGADA]
                ,[CODCFO]
                ,[NOMEFANTASIA]
                ,[NOME]
                ,[PAGREC]
                ,[PESSOAFISOUJUR]
                ,[CGCCFO]
                ,[ATIVO]
                ,[IDCFO]
                ,[RAMOATIV]
                ,[TIPOCONTRIBUINTEINSS]
                ,[NACIONALIDADE]
                ,[CALCULAAVP]
                ,[ENTIDADEEXECUTORAPAA]
                ,[APOSENTADOOUPENSIONISTA]
                ,[INSCRESTADUAL]
                ,[RUA]
                ,[NUMERO]
                ,[COMPLEMENTO]
                ,[BAIRRO]
                ,[CIDADE]
                ,[CODETD]
                ,[CEP]
                ,[TELEFONE]
                ,[TELEX]
                ,[EMAIL]
                ,[CONTATO]
                ,[PAIS]
                ,[TIPORUA]
                ,[TIPOBAIRRO]
                ,[CODMUNICIPIO]
                ,[FAX]
                ,[RECCREATEDBY]
                ,[RECCREATEDON]
                ,[RECMODIFIEDBY]
                ,[RECMODIFIEDON]
            FROM [dbo].[FCFO]
            WHERE CODCOLIGADA='0' {(codColigada.HasValue ? $" OR CODCOLIGADA='{codColigada.Value}' " : null)}
            ORDER BY [NOMEFANTASIA]";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                FCFO pais = new FCFO
                {
                    CODCOLIGADA = Convert.ToInt16(row["CODCOLIGADA"]),
                    CODCFO = row["CODCFO"].DBNullToString(),
                    NOMEFANTASIA = row["NOMEFANTASIA"].DBNullToString(),
                    NOME = row["NOME"].DBNullToString(),
                    PAGREC = Convert.ToInt16(row["PAGREC"]),
                    PESSOAFISOUJUR = row["PESSOAFISOUJUR"].DBNullToString(),
                    CGCCFO = row["CGCCFO"].DBNullToString(),
                    ATIVO = Convert.ToInt16(row["ATIVO"]),
                    IDCFO = Convert.ToInt32(row["IDCFO"]),
                    RAMOATIV = row["RAMOATIV"].DBNullToInt(),
                    TIPOCONTRIBUINTEINSS = row["TIPOCONTRIBUINTEINSS"].DBNullToSmallint(),
                    NACIONALIDADE = row["NACIONALIDADE"].DBNullToSmallint(),
                    CALCULAAVP = row["CALCULAAVP"].DBNullToSmallint(),
                    ENTIDADEEXECUTORAPAA = row["ENTIDADEEXECUTORAPAA"].DBNullToSmallint(),
                    APOSENTADOOUPENSIONISTA = row["APOSENTADOOUPENSIONISTA"].DBNullToInt(),
                    INSCRESTADUAL = row["INSCRESTADUAL"].DBNullToString(),
                    RUA = row["RUA"].DBNullToString(),
                    NUMERO = row["NUMERO"].DBNullToString(),
                    COMPLEMENTO = row["COMPLEMENTO"].DBNullToString(),
                    BAIRRO = row["BAIRRO"].DBNullToString(),
                    CIDADE = row["CIDADE"].DBNullToString(),
                    CODETD = row["CODETD"].DBNullToString(),
                    CEP = row["CEP"].DBNullToString(),
                    TELEFONE = row["TELEFONE"].DBNullToString(),
                    TELEX = row["TELEX"].DBNullToString(),
                    EMAIL = row["EMAIL"].DBNullToString(),
                    CONTATO = row["CONTATO"].DBNullToString(),
                    PAIS = row["PAIS"].DBNullToString(),
                    TIPORUA = row["TIPORUA"].DBNullToSmallint(),
                    TIPOBAIRRO = row["TIPOBAIRRO"].DBNullToSmallint(),
                    CODMUNICIPIO = row["CODMUNICIPIO"].DBNullToString(),
                    FAX = row["FAX"].DBNullToString(),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(pais);
            }

            return lista;
        }

        public List<FCFO> GetListDossie(int codColigada, string codCCusto = null, int? codTpFornec = null)
        {
            int codUsuario = FormPrincipal.getUsuarioAcesso().CODUSUARIO;
            List<FCFO> list = new List<FCFO>();
            var fPermissao = (from p in context.FPERMISSAO
                              where p.CODCOLIGADA == codColigada
                              select p).ToList();

            List<GPERMISSAO> permissoesUsr = context.GPERMISSAO.Where(x =>
                x.CODCOLIGADA == codColigada &&
                x.CODUSUARIO == codUsuario &&
                x.CODSISTEMA == "F"
            ).ToList();

            if (!string.IsNullOrEmpty(codCCusto))
                fPermissao = fPermissao.Where(x => x.CODCCUSTO.Trim() == codCCusto).ToList();

            using (ControleFArquivoFornecedor controle = new ControleFArquivoFornecedor())
            {
                foreach (var item in fPermissao)
                {
                    GPERMISSAO usrPermissao = permissoesUsr?.Where(x => x.CODCCUSTO.Trim() == item.CODCCUSTO.Trim()).FirstOrDefault();
                    if (usrPermissao != null)
                    {
                        FCFO fornecedor;
                        if (codTpFornec.HasValue)
                        {
                            fornecedor = (from f in context.FCFO
                                          where f.CODCOLIGADA == codColigada && 
                                            f.CODCFO == item.CODCFO && 
                                            f.CODTPFCFO == codTpFornec.Value
                                          select f).FirstOrDefault();
                        }
                        else
                        {
                            fornecedor = (from f in context.FCFO
                                          where f.CODCOLIGADA == codColigada && f.CODCFO == item.CODCFO
                                          select f).FirstOrDefault();
                        }

                        if (fornecedor != null)
                        {
                            bool jaAdicionado = list.Exists(x => 
                                x.CODCOLIGADA == codColigada && 
                                x.CODCFO == item.CODCFO && 
                                x.CODCCUSTO == item.CODCCUSTO
                            );

                            if (!jaAdicionado)
                            {
                                bool temVencido = controle.TemArquivoVencido(fornecedor.CODCOLIGADA, fornecedor.CODCFO, fornecedor.CODTPFCFO); 
                                bool temPendente = controle.TemArquivoPendente(fornecedor.CODCOLIGADA, fornecedor.CODCFO);
                                fornecedor.CONFORMIDADE = temVencido || temPendente ? "Não Conforme" : "Conforme";

                                if (fornecedor.ATIVO == 0)
                                    fornecedor.CONFORMIDADE = "Não se Aplica";

                                fornecedor.STATUS = fornecedor.ATIVO == 1;
                                fornecedor.CODCCUSTO = item.CODCCUSTO;
                                fornecedor.DESCCCUSTO = GetNomeCentroCusto(item.CODCOLIGADA, item.CODCCUSTO);

                                FCFO fornec = new FCFO
                                {
                                    CODCOLIGADA = fornecedor.CODCOLIGADA,
                                    CODCFO = fornecedor.CODCFO,
                                    NOMEFANTASIA = fornecedor.NOMEFANTASIA,
                                    NOME = fornecedor.NOME,
                                    PAGREC = fornecedor.PAGREC,
                                    PESSOAFISOUJUR = fornecedor.PESSOAFISOUJUR,
                                    CGCCFO = fornecedor.CGCCFO,
                                    ATIVO = fornecedor.ATIVO,
                                    IDCFO = fornecedor.IDCFO,
                                    RAMOATIV = fornecedor.RAMOATIV,
                                    TIPOCONTRIBUINTEINSS = fornecedor.TIPOCONTRIBUINTEINSS,
                                    NACIONALIDADE = fornecedor.NACIONALIDADE,
                                    CALCULAAVP = fornecedor.CALCULAAVP,
                                    ENTIDADEEXECUTORAPAA = fornecedor.ENTIDADEEXECUTORAPAA,
                                    APOSENTADOOUPENSIONISTA = fornecedor.APOSENTADOOUPENSIONISTA,
                                    INSCRESTADUAL = fornecedor.INSCRESTADUAL,
                                    RUA = fornecedor.RUA,
                                    NUMERO = fornecedor.NUMERO,
                                    COMPLEMENTO = fornecedor.COMPLEMENTO,
                                    BAIRRO = fornecedor.BAIRRO,
                                    CIDADE = fornecedor.CIDADE,
                                    CODETD = fornecedor.CODETD,
                                    CEP = fornecedor.CEP,
                                    TELEFONE = fornecedor.TELEFONE,
                                    TELEX = fornecedor.TELEX,
                                    EMAIL = fornecedor.EMAIL,
                                    CONTATO = fornecedor.CONTATO,
                                    PAIS = fornecedor.PAIS,
                                    TIPORUA = fornecedor.TIPORUA,
                                    TIPOBAIRRO = fornecedor.TIPOBAIRRO,
                                    CODMUNICIPIO = fornecedor.CODMUNICIPIO,
                                    FAX = fornecedor.FAX,
                                    RECCREATEDBY = fornecedor.RECCREATEDBY,
                                    RECCREATEDON = fornecedor.RECCREATEDON,
                                    RECMODIFIEDBY = fornecedor.RECMODIFIEDBY,
                                    RECMODIFIEDON = fornecedor.RECMODIFIEDON,
                                    CODTPFCFO = fornecedor.CODTPFCFO,
                                    CONFORMIDADE = fornecedor.CONFORMIDADE, 
                                    STATUS = fornecedor.STATUS, 
                                    CODCCUSTO = item.CODCCUSTO,
                                    DESCCCUSTO = GetNomeCentroCusto(item.CODCOLIGADA, item.CODCCUSTO),
                                };
                                fornec.DOCVENCEATE30D = GetTotalDocAVencer(
                                    fornecedor.CODCOLIGADA, 
                                    fornecedor.CODCFO, 
                                    fornecedor.CODTPFCFO
                                );
                                list.Add(fornec);
                            }
                        }
                    }
                }
            }
            return list;
        }

        private string GetNomeCentroCusto(int codColigada, string codCCusto)
        {
            DataTable dataTable = GetDataTable(
                @"SELECT NOME FROM GCENTROCUSTO WHERE CODCOLIGADA=@CODCOLIGADA AND CODCCUSTO=@CODCCUSTO",
                new List<System.Data.SqlClient.SqlParameter>
                {
                    new System.Data.SqlClient.SqlParameter("@CODCOLIGADA", codColigada),
                    new System.Data.SqlClient.SqlParameter("@CODCCUSTO", codCCusto)
                }
            );
            if (dataTable != null && dataTable.Rows.Count > 0)
                return dataTable.Rows[0]["NOME"].DBNullToString();

            return null;
        }
    }
}
