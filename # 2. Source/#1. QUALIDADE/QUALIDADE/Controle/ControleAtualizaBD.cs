﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace QUALIDADE.Controle
{
    public class ControleAtualizaBD
    {
        public void GetIntegracaoRMLabore()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                using (ControleGPais controle = new ControleGPais())
                {
                    List<GPAIS> lista = new List<GPAIS>();
                    List<GPAIS> listaRM = controle.GetIntegracaoRM();

                    foreach (GPAIS item in listaRM)
                    {
                        if (!controle.Exists(x => x.DESCRICAO == item.DESCRICAO && x.IDPAIS == item.IDPAIS))
                            lista.Add(item);
                    }

                    if (lista != null && lista.Count > 0)
                    {
                        controle.CreateAll(lista);
                        controle.SaveAll();
                    }
                }

                using (ControleGEstados controle = new ControleGEstados())
                {
                    List<GESTADOS> lista = new List<GESTADOS>();
                    List<GESTADOS> listaRM = controle.GetIntegracaoRM();

                    foreach (GESTADOS item in listaRM)
                    {
                        if (!controle.Exists(x => x.CODETD == item.CODETD))
                            lista.Add(item);
                    }

                    if (lista != null && lista.Count > 0)
                    {
                        controle.CreateAll(lista);
                        controle.SaveAll();
                    }
                }

                using (ControleGMunicipios controle = new ControleGMunicipios())
                {
                    List<GMUNICIPIO> lista = new List<GMUNICIPIO>();
                    List<GMUNICIPIO> listaRM = controle.GetIntegracaoRM();

                    foreach (GMUNICIPIO item in listaRM)
                    {
                        if (!controle.Exists(x => x.CODMUNICIPIO == item.CODMUNICIPIO && x.CODETDMUNICIPIO == item.CODETDMUNICIPIO))
                            lista.Add(item);
                    }

                    if (lista != null && lista.Count > 0)
                    {
                        controle.CreateAll(lista);
                        controle.SaveAll();
                    }
                }

                using (ControleGEmpresas controle = new ControleGEmpresas())
                {
                    using (ControleGCentroCusto controleCCusto = new ControleGCentroCusto())
                    {
                        List<GEMPRESAS> lista = new List<GEMPRESAS>();
                        List<GEMPRESAS> listaRM = controle.GetIntegracaoRM();
                        List<GCENTROCUSTO> listaCCusto = new List<GCENTROCUSTO>();
                        List<GCENTROCUSTO> listaRMCCusto = controleCCusto.GetIntegracaoRM();

                        foreach (GEMPRESAS item in listaRM)
                        {
                            if (!controle.Exists(x => x.CGC == item.CGC && x.CODCOLIGADA == item.CODCOLIGADA))
                            {
                                lista.Add(item);

                                foreach (GCENTROCUSTO cCusto in listaRMCCusto.Where(x => x.CODCOLIGADA == item.CODCOLIGADA))
                                {
                                    cCusto.CODCOLIGADA = item.CODCOLIGADA;
                                    listaCCusto.Add(cCusto);
                                }
                            }
                            else
                            {
                                foreach (GCENTROCUSTO cCusto in listaRMCCusto.Where(x => x.CODCOLIGADA == item.CODCOLIGADA))
                                {
                                    if (!controleCCusto.Exists(x => x.CODCOLIGADA == item.CODCOLIGADA && x.CODCCUSTO == cCusto.CODCCUSTO))
                                        listaCCusto.Add(cCusto);
                                }
                            }
                        }

                        if (lista != null && lista.Count > 0)
                        {
                            controle.CreateAll(lista);
                            controle.SaveAll();
                        }

                        if (listaCCusto != null && listaCCusto.Count > 0)
                        {
                            controleCCusto.CreateAll(listaCCusto);
                            controleCCusto.SaveAll();
                        }
                    }
                }
                Cursor.Current = Cursors.Default;
                Global.MsgSucesso("Integração Realizada com sucesso!");
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex);
                frm.ShowDialog();
            }
        }
    }
}
