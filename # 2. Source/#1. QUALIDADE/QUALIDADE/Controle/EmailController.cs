﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Email;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace QUALIDADE.Controle
{
    internal class EmailController
    {
        //internal void ProcessoEnviaEmails(List<CorpoEmail> emails, string titulo, string msgCompl, string emailsCopia, out int total, out int sucessos, out int erros, out string log)
        //{
        //    total = 0;
        //    sucessos = 0;
        //    erros = 0;
        //    StringBuilder builderLog = new StringBuilder();
        //    GUSUARIO usuario = FormPrincipal.getUsuarioAcesso();

        //    if (string.IsNullOrEmpty(usuario.EMAIL) || string.IsNullOrEmpty(usuario.HOST) || string.IsNullOrEmpty(usuario.SENHAEMAIL))
        //    {
        //        builderLog.AppendLine(
        //            $"Não foi possível realizar o envio de e-mails.\r\n" +
        //            $"Para realizar esta ação, por favor, confira se um e-mail está informado em seu " +
        //            $"cadastro de usuário e realize a configuração de smtp em " +
        //            $"'Processos da Qualidade Global > Configurações de E-mail'."
        //        );
        //    }
        //    else
        //    {
        //        try
        //        {
        //            if (emails != null && emails.Count > 0)
        //            {
        //                total = emails.Count;
        //                foreach (CorpoEmail item in emails)
        //                {
        //                    bool enviado = EnviaEmail(item, usuario, out string msgSaida);
        //                    if (enviado)
        //                    {
        //                        sucessos++;
        //                        builderLog.AppendLine($"[SUCESSO] {msgSaida}");
        //                    }
        //                    else
        //                    {
        //                        erros++;
        //                        builderLog.AppendLine($"[ERRO] {msgSaida}");
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            erros++;
        //            builderLog.AppendLine($"[ERRO] Falha ao obter e-mails para transmissão. Erro: {ex.InnerException?.InnerException?.Message ?? ex.InnerException?.Message ?? ex.Message}");
        //        }
        //    }
        //    log = builderLog.ToString();
        //}

        internal bool EnviaEmail(CorpoEmail conteudoEmail, GUSUARIO usuarioEnvio, out string msgSaida)
        {
            string itemDestinatario = $"Destinatário: {conteudoEmail.NomeUsuario} [{conteudoEmail.EmailUsuario}]";
            if (string.IsNullOrEmpty(conteudoEmail.EmailUsuario))
            {
                msgSaida = $"E-mail do destinatário não informado. {itemDestinatario}.";
                return false;
            }
            else
            {
                try
                {
                    string corpoEmail = TemplateHtml(conteudoEmail);
                    if (!string.IsNullOrEmpty(corpoEmail))
                    {
                        try
                        {
                            using (MailMessage msg = new MailMessage())
                            {
                                msg.From = new MailAddress(usuarioEnvio.EMAIL, usuarioEnvio.NOME);
                                msg.To.Add(new MailAddress(conteudoEmail.EmailUsuario, conteudoEmail.NomeUsuario));
                                if (!string.IsNullOrEmpty(conteudoEmail.EmailsCopia))
                                {
                                    string[] emails = conteudoEmail.EmailsCopia?.Split(',');
                                    if (emails != null && emails.Length > 0)
                                    {
                                        foreach (string emailCopia in emails)
                                        {
                                            if (!string.IsNullOrEmpty(emailCopia?.Trim()))
                                                msg.Bcc.Add(new MailAddress(emailCopia?.Trim()));
                                        }
                                    }
                                }
                                msg.Subject = string.IsNullOrEmpty(conteudoEmail.Titulo) ? "[SGI Qualidade] Aviso de Documentos Pendentes" : conteudoEmail.Titulo;
                                msg.IsBodyHtml = true;
                                msg.Body = corpoEmail;
                                using (SmtpClient enviar = new SmtpClient(Criptografia.Decrypt(usuarioEnvio.HOST), (int.TryParse(Criptografia.Decrypt(usuarioEnvio.PORTA), out int porta) ? porta : 587)))
                                {
                                    enviar.DeliveryMethod = SmtpDeliveryMethod.Network;
                                    enviar.UseDefaultCredentials = false;
                                    enviar.Credentials = new NetworkCredential(usuarioEnvio.EMAIL, Criptografia.Decrypt(usuarioEnvio.SENHAEMAIL));
                                    enviar.EnableSsl = usuarioEnvio.SSLHABILITADO;
                                    enviar.Send(msg);
                                }
                            }
                            msgSaida = $"E-mail enviado com sucesso. {itemDestinatario}.";
                            return true;
                        }
                        catch (Exception ex)
                        {
                            msgSaida = $"Falha na transmissão do e-mail. {itemDestinatario}. Erro: {ex.InnerException?.InnerException?.Message ?? ex.InnerException?.Message ?? ex.Message}";
                            return false;
                        }
                    }
                    else
                    {
                        msgSaida = $"Conteúdo do e-mail não informado. {itemDestinatario}.";
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    msgSaida = $"Falha ao montar corpo do e-mail. {itemDestinatario}. Erro: {ex.InnerException?.InnerException?.Message ?? ex.InnerException?.Message ?? ex.Message}";
                    return false;
                }
            }
        }

        private string MontaTabelaAfericoes(AlertaAfericao registro)
        {
            string tabela;
            if (registro != null)
            {
                tabela = $@"<tr style=""color:#333;"">
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Total}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Conformes}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.NaoConformes}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Pendentes}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Vencidas}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Vencem7Dias}</td>
                </tr>";
            }
            else
            {
                tabela = @"<tr style=""color:#333;"">
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                </tr>";
            }
            return tabela;
        }

        private string MontaTabelaCalibracoes(AlertaCalibracao registro)
        {
            string tabela;
            if (registro != null)
            {
                tabela = $@"<tr style=""color:#333;"">
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Total}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Aprovadas}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Reprovadas}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Pendentes}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Vencidas}</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">{registro.Vencem7Dias}</td>
                </tr>";
            }
            else
            {
                tabela = @"<tr style=""color:#333;"">
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                </tr>";
            }
            return tabela;
        }

        private string MontaTabelaFornecedores(List<AlertaFornecedor> registros)
        {
            string tabela = string.Empty;
            if (registros != null && registros.Count > 0)
            {
                foreach (AlertaFornecedor item in registros)
                {
                    tabela += $@"<tr style=""color:#333;"">
                        <td style=""border:1px solid #ddd;font-weight:bold;"">{item.Fornecedor}</td>
                        <td style=""border:1px solid #ddd;text-align:center;"">{item.Requisitados}</td>
                        <td style=""border:1px solid #ddd;text-align:center;"">{item.Pendentes}</td>
                        <td style=""border:1px solid #ddd;text-align:center;"">{item.Vencidos}</td>
                        <td style=""border:1px solid #ddd;text-align:center;"">{item.Vencem7Dias}</td>
                    </tr>";
                }
            }
            else
            {
                tabela = @"<tr style=""color:#333;"">
                    <td style=""border:1px solid #ddd;font-weight:bold;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                    <td style=""border:1px solid #ddd;text-align:center;"">N/A</td>
                </tr>";
            }
            return tabela;
        }

        private string TemplateHtml(CorpoEmail conteudoEmail)
        {
            string strEmail = string.Empty;
            if (conteudoEmail != null && conteudoEmail.CentroCustoEmail != null && conteudoEmail.CentroCustoEmail.Count > 0)
            {
                foreach (CentroCustoEmail centroCusto in conteudoEmail.CentroCustoEmail)
                {
                    strEmail += $@"<h2 style=""color:#970000;font-size:18px;margin-bottom:5px;border-bottom:1px dotted #990000;font-weight:normal;"">
                        Centro de Custo: <strong>{centroCusto.CodColigada} : {centroCusto.CodCentroCusto} - {centroCusto.NomeCentroCusto}</strong>
                    </h2>
        
                    <h5 style=""color:#970000;text-transform:uppercase;text-align:center;font-size:14px;margin-bottom:5px;"">Aferi&ccedil;&otilde;es: Controle de Verifica&ccedil;&atilde;o</h5>
                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" height=""100%"" width=""100%"">
                        <tr>
                            <td align=""center"" valign=""top"">
                                <table border=""0"" cellpadding=""8"" cellspacing=""0"" width=""100%"" id=""emailContainer"" style=""border:1px solid #ddd;font-size:12px;"">
                                    <thead>
                                        <tr>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Total</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Conformes</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Não Conformes</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Pendentes</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencidas</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencem em até 7 dias</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {MontaTabelaAfericoes(centroCusto.InfAfericao)}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
        
                    <h5 style=""color:#970000;text-transform:uppercase;text-align:center;font-size:14px;margin-bottom:5px;"">Calibra&ccedil;&otilde;es: Controle de Calibra&ccedil;&atilde;o</h5>
                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" height=""100%"" width=""100%"">
                        <tr>
                            <td align=""center"" valign=""top"">
                                <table border=""0"" cellpadding=""8"" cellspacing=""0"" width=""100%"" id=""emailContainer"" style=""border:1px solid #ddd;font-size:12px;"">
                                    <thead>
                                        <tr>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Total</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Aprovadas</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Reprovadas</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Pendentes</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencidas</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencem em até 7 dias</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {MontaTabelaCalibracoes(centroCusto.InfCalibracao)}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
        
                    <h5 style=""color:#970000;text-transform:uppercase;text-align:center;font-size:14px;margin-bottom:5px;"">Documentos de Fornecedores: Dossi&ecirc; do Fornecedor</h5>
                    <table border=""0"" cellpadding=""0"" cellspacing=""0"" height=""100%"" width=""100%"">
                        <tr>
                            <td align=""center"" valign=""top"">
                                <table border=""0"" cellpadding=""8"" cellspacing=""0"" width=""100%"" id=""emailContainer"" style=""border:1px solid #ddd;font-size:12px;"">
                                    <thead>
                                        <tr>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Fornecedor</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Requisitados</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Pendentes</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencidos</th>
                                            <th style=""background-color:#ddd;color:#970000;border:1px solid #ccc;"">Vencem em até 7 dias</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {MontaTabelaFornecedores(centroCusto.InfFornecedores)}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style=""display:block;clear:both;height:2px;margin:20px 0px;background:#ddd;"">&nbsp;</div>";
                }

                return $@"<!DOCTYPE html>
                    <html lang=""pt-br"">
                    <head>
                        <meta charset=""UTF-8"">
                        <meta http-equiv=""X-UA-Compatible"" content=""IE=edge"">
                        <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
                        <title>Email SGI</title>
                    </head>
                    <body>
                        <div style=""font-family:Arial, Helvetica, sans-serif;max-width:800px;margin:20px auto;padding:20px;background-color:#fcfcfc;border:1px solid #eee;border-radius:5px;"">
                            <h3 style=""color:#970000;font-size:20px;"">Prezado(a),</h3>
                            <p style=""font-size:14px;"">
                                Segue um resumo das informações que podem estar pentendes ou vencidas, cadastradas no Sistema SGI.
                            </p>
                            {(!string.IsNullOrEmpty(conteudoEmail.MsgCompl) ? $"<p>{conteudoEmail.MsgCompl}</p>" : null)}
                            [@CONTEUDO@]
                        </div>
                    </body>
                    </html>".Replace("[@CONTEUDO@]", strEmail);
            }
            return null;
        }
    }
}