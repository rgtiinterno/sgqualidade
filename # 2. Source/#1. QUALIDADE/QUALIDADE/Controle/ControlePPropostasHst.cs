﻿using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePPropostasHst : Controle<PPROPOSTASHST>
    {
        public int GetNewID(short CODCOLIGADA)
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == CODCOLIGADA
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<PPROPOSTASHST> GetList(PPROPOSTAS item)
        {
            List<PPROPOSTASHST> list = (from p in GetAll()
                                        where p.CODCOLIGADA == item.CODCOLIGADA &&
                                            p.CODPROPOSTA == item.CODIGO
                                        orderby p.DATAENVIO ascending
                                        select p).ToList();
            return list;
        }
    }
}