﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGEmpresas : Controle<GEMPRESAS>
    {
        public short GetID()
        {
            int newId = 0;

            var pais = (from f in GetAll()
                        orderby f.CODCOLIGADA descending
                        select f).FirstOrDefault();

            if (pais != null)
            {
                Nullable<int> item = pais.CODCOLIGADA;
                if (item != null)
                {
                    newId = Convert.ToInt16(item.Value);
                }
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<GEMPRESAS> GetIntegracaoRM()
        {
            List<GEMPRESAS> lista = new List<GEMPRESAS>();

            string query = @"SELECT [CODCOLIGADA]
                                  ,[NOMEFANTASIA]
                                  ,[CGC]
                                  ,[NOME]
                                  ,[INSCRICAOESTADUAL]
                                  ,[TELEFONE]
                                  ,[FAX]
                                  ,[EMAIL]
                                  ,[RUA]
                                  ,[NUMERO]
                                  ,[COMPLEMENTO]
                                  ,[BAIRRO]
                                  ,[CIDADE]
                                  ,[ESTADO]
                                  ,[PAIS]
                                  ,[CEP]
                                  ,[ATIVO]
                                  ,[RECCREATEDBY]
                                  ,[RECCREATEDON]
                                  ,[RECMODIFIEDBY]
                                  ,[RECMODIFIEDON]
                              FROM [dbo].[GCOLIGADA]";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                GEMPRESAS empresas = new GEMPRESAS
                {
                    CODCOLIGADA = Convert.ToInt16(row["CODCOLIGADA"]),
                    NOMEFANTASIA = row["NOMEFANTASIA"].DBNullToString(),
                    CGC = row["CGC"].DBNullToString(),
                    NOME = row["NOME"].DBNullToString(),
                    INSCRICAOESTADUAL = row["INSCRICAOESTADUAL"].DBNullToString(),
                    TELEFONE = row["TELEFONE"].DBNullToString(),
                    FAX = row["FAX"].DBNullToString(),
                    EMAIL = row["EMAIL"].DBNullToString(),
                    RUA = row["RUA"].DBNullToString(),
                    NUMERO = row["NUMERO"].DBNullToString(),
                    COMPLEMENTO = row["COMPLEMENTO"].DBNullToString(),
                    BAIRRO = row["BAIRRO"].DBNullToString(),
                    CIDADE = row["CIDADE"].DBNullToString(),
                    ESTADO = row["ESTADO"].DBNullToString(),
                    PAIS = row["PAIS"].DBNullToString(),
                    CEP = row["CEP"].DBNullToString(),
                    ATIVO = (row["ATIVO"].DBNullToString() == "T"),
                    LOGO = null,
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(empresas);
            }

            return lista;
        }
    }
}
