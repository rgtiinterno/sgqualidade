﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace QUALIDADE.Controle.Configuracoes
{
    public class Funcoes
    {
        private string dirLogImportacao = string.Format(@"{0}\Arquivos\logs\importacao", Directory.GetCurrentDirectory());
        private string dirLogErros = string.Format(@"{0}\Arquivos\logs\erros", Directory.GetCurrentDirectory());

        #region "Seleciona / Salva arquivo"

        /// <summary>
        /// Salva o arquivo no diretório selecionado
        /// </summary>
        /// <param name="filter">Formato do arquivo. Exemplo: txt files (.txt)|*.txt|All files (.*)|*.*</param>
        /// <param name="defaultName">Nome padrão do arquivo</param>
        /// <returns>Nome do arquivo a ser salvo</returns>
        public static string SalvaArquivo(string filter, string defaultName)
        {
            string fileName = null;

            using (SaveFileDialog save = new SaveFileDialog())
            {
                save.FileName = defaultName;
                if (!string.IsNullOrEmpty(filter))
                    save.Filter = filter;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    fileName = save.FileName;
                }
            }

            return fileName;
        }
        public string SelecionaArquivoTextoCSV()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Arquivo de dados (*.txt;*.csv)|*.txt;*.csv";
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public string SelecionaArquivoTexto()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Arquivo de texto (*.txt)|*.txt";
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;

                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public string[] SelecionaArquivos()
        {
            string[] arquivos = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Documento PDF (*.pdf)|*.pdf|Imagens JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            openFileDialog.Multiselect = true;
            openFileDialog.FilterIndex = 2;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivos = openFileDialog.FileNames;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivos;
        }

        public string SelecionaImagensPDF()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Documento PDF (*.pdf)|*.pdf|Imagens JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 2;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string SelecionaArquivo()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Arquivo de Imagem(*.png) | *.png|Imagens JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 2;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string[] SelecionaImagens()
        {
            string[] arquivos = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Imagens JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            openFileDialog.Multiselect = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivos = openFileDialog.FileNames;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivos;
        }

        public static string SelecionaArquivoPDF()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Documento PDF (*.pdf)|*.pdf";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string SelecionaFoto()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            openFileDialog.FilterIndex = 2;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string SelecionaDiretorioSaida()
        {
            string diretorio = null;
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    diretorio = folderBrowserDialog1.SelectedPath;
                }
                catch (Exception ex)
                {
                    throw new Exception("Não foi possível abrir o diretório selecionado." +
                            "\nErro original: " + ex.Message);
                }
            }

            return diretorio;
        }

        public static string SelecionaArquivoSaida(string nomePadrao)
        {
            string arquivo = null;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JPEG (*.jpg;*.jpeg;*.jpe)|*.jpg;*.jpeg;*.jpe";
            saveFileDialog.FileName = nomePadrao;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = saveFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível gravar o arquivo no local selecionado." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string SelecionaArquivoTextoXLSX()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Arquivo de dados (*.xlsx;*.xls)|*.xlsx;*.xls";
            openFileDialog.Multiselect = false;
            openFileDialog.FilterIndex = 1;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string[] SelecionaArquivoAllFormat()
        {
            string[] arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "All files (*.*) | *.*";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileNames;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        public static string SelecionaArquivosAllFormat()
        {
            string arquivo = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "All files (*.*) | *.*";
            openFileDialog.FilterIndex = 0;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    arquivo = openFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro: Não foi possível ler o arquivo do disco." +
                         "\nErro original: " + ex.Message);
                }
            }

            return arquivo;
        }

        #endregion

        #region "Arquivos de Log"
        public string GeraLogImportacao(Log log)
        {
            string nomeArquivo = null;

            try
            {
                if (!Directory.Exists(dirLogImportacao))
                {
                    Directory.CreateDirectory(dirLogImportacao);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            GUSUARIO usr = FormPrincipal.getUsuarioAcesso();
            DateTime data = DateTime.Now;
            nomeArquivo = dirLogImportacao + @"\" + log.Modulo.ToLower() + "_" + data.ToString("dd-MM-yyyy_hh-mm-ss") + ".txt";
            string cabecalho =
            "--------------------------------------------------------------------" +
            "\r\n - " + Global.AssemblyTitle + " - " + Global.AssemblyDescription +
            "\r\n - LOG DE IMPORTAÇÃO " +
            "\r\n - Módulo : " + log.Modulo +
            "\r\n - Data : " + data.ToString("dd/MM/yyyy hh:mm:ss") +
            "\r\n - Usuário : " + String.Format("{0} - ({1})", usr.NOME, usr.LOGIN) +
            "\r\n - Estatísticas da importação : \r\n" +
            "     Tempo de Execução : " + log.Tempo + "\r\n" +
            "     Sucesso(s) : " + log.NumSucessos + " \r\n" +
            "     Alerta(s) : " + log.NumAlertas + " \r\n" +
            "     Erro(s) : " + log.NumErros + " \r\n" +
            "--------------------------------------------------------------------";

            try
            {
                using (StreamWriter escreve = new StreamWriter(nomeArquivo))
                {
                    escreve.WriteLine(cabecalho);

                    foreach (string msg in log.Mensagens)
                        escreve.WriteLine(msg);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            return nomeArquivo;
        }

        public string GeraLogErros(Log log)
        {
            string nomeArquivo = null;

            try
            {
                if (!Directory.Exists(dirLogErros))
                {
                    Directory.CreateDirectory(dirLogErros);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            GUSUARIO usr = FormPrincipal.getUsuarioAcesso();
            DateTime data = DateTime.Now;
            nomeArquivo = dirLogErros + @"\" + log.Modulo.ToLower() + "_" + data.ToString("dd-MM-yyyy_hh-mm-ss") + ".txt";
            string cabecalho =
            "--------------------------------------------------------------------" +
            "\r\n - " + Global.AssemblyTitle + " - " + Global.AssemblyDescription +
            "\r\n - LOG DE ERROS " +
            "\r\n - Módulo : " + log.Modulo +
            "\r\n - Data : " + data.ToString("dd/MM/yyyy hh:mm:ss") +
            "\r\n - Usuário : " + String.Format("{0} - ({1})", usr.NOME, usr.LOGIN) +
            "\r\n - Estatísticas : \r\n" +
            "     Tempo de Execução : " + log.Tempo + "\r\n" +
            "     Sucesso(s) : " + log.NumSucessos + " \r\n" +
            "     Alerta(s) : " + log.NumAlertas + " \r\n" +
            "     Erro(s) : " + log.NumErros + " \r\n" +
            "--------------------------------------------------------------------";

            try
            {
                using (StreamWriter escreve = new StreamWriter(nomeArquivo))
                {
                    escreve.WriteLine(cabecalho);

                    foreach (string msg in log.Mensagens)
                        escreve.WriteLine(msg);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }

            return nomeArquivo;
        }
        #endregion

        public static DataTable RetornaDataTableIntegracao(string sqlQuery)
        {
            using (SqlCommand cmd = SqlConn.getConexaoIntegracao().CreateCommand())
            {
                try
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlQuery;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

                    DataTable dados = new DataTable();
                    dataAdapter.Fill(dados);

                    return dados;
                }
                catch (SqlException ex)
                {
                    throw new Exception(ex.ToString(), ex);
                }
            }
        }

        public static DataTable RetornaDataTable(string sqlQuery, List<SqlParameter> parameters = null)
        {
            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlQuery;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                            cmd.Parameters.AddWithValue(param.ParameterName, param.Value.ToNullableParam());
                    }

                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

                    DataTable dados = new DataTable();
                    dataAdapter.Fill(dados);

                    return dados;
                }
                catch (SqlException ex)
                {
                    throw new Exception(ex.ToString(), ex);
                }
            }
        }

        public static bool ExecutaFuncao(string sqlQuery, List<SqlParameter> parameters = null)
        {
            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlQuery;

                    if (parameters != null)
                    {
                        foreach (SqlParameter param in parameters)
                            cmd.Parameters.AddWithValue(param.ParameterName, param.Value.ToNullableParam());
                    }

                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static bool ExecutaFuncaoTransaction(List<SqlCommand> commands, string transactionName = null)
        {
            SqlTransaction transaction = null;
            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                try
                {
                    con.Open();
                    transaction = con.BeginTransaction(transactionName);
                    foreach (SqlCommand cmd in commands)
                    {
                        cmd.Connection = con;
                        cmd.Transaction = transaction;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    commands.ForEach(x => x.Dispose());
                    transaction.Dispose();
                    return true;
                }
                catch (SqlException ex)
                {
                    transaction.Rollback(transactionName);
                    commands.ForEach(x => x.Dispose());
                    transaction.Dispose();
                    Global.GerarLog("Erro ao executar comando.\n" +
                        "Transaction: " + transactionName + "\n" +
                        "Erro: " + ex.Message + "\n" +
                        "Data e Hora: " + DateTime.Now + "\n" +
                        Environment.NewLine);
                    return false;
                }
            }
        }

        public bool ExecutaComando(string sqlComando1, string sqlComando2)
        {
            SqlCommand objCmd = new SqlCommand();

            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlTransaction Sqltrans = con.BeginTransaction();
                objCmd = con.CreateCommand();
                objCmd.Transaction = Sqltrans;

                try
                {
                    objCmd.CommandText = sqlComando1;
                    objCmd.ExecuteNonQuery();
                    objCmd.CommandText = sqlComando2;
                    objCmd.ExecuteNonQuery();

                    Sqltrans.Commit();
                    return true;
                }
                catch (SqlException sqlerror)
                {
                    Sqltrans.Rollback();
                    throw sqlerror;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public bool ExecutaComandoRetornandoAfetados(string sqlComando1, string sqlComando2, ref Nullable<int> rowsAfet)
        {
            SqlCommand objCmd = new SqlCommand();
            rowsAfet = 0;

            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                SqlTransaction Sqltrans = con.BeginTransaction();
                objCmd = con.CreateCommand();
                objCmd.Transaction = Sqltrans;

                try
                {
                    objCmd.CommandText = sqlComando1;
                    rowsAfet += objCmd.ExecuteNonQuery();
                    objCmd.CommandText = sqlComando2;
                    rowsAfet += objCmd.ExecuteNonQuery();

                    Sqltrans.Commit();
                    return true;
                }
                catch (SqlException ex)
                {
                    Sqltrans.Rollback();
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public bool ExecutaFuncaoRetornandoAfetados(string sqlQuery, ref Nullable<int> rowsAfet)
        {
            rowsAfet = 0;

            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                try
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlQuery;
                    rowsAfet += cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    return true;
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
            }
        }

        public static string GetNomeBancoPrincipal()
        {
            string nomeBanco = "RGBENEF";

            using (SqlConnection con = new SqlConnection(SqlConn.getStringConexao()))
            {
                nomeBanco = con.Database;
            }

            return nomeBanco;
        }

        public static string GetNomeBancoIntegracao()
        {
            string nomeBanco = "CORPORERM";

            using (SqlConnection con = new SqlConnection(SqlConn.getStringIntegracao()))
            {
                nomeBanco = con.Database;
            }

            return nomeBanco;
        }
    }
}