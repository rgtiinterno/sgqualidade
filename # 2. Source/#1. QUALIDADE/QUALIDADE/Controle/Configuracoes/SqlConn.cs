﻿using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace QUALIDADE.Controle.Configuracoes
{
    public static class SqlConn
    {
        private static SqlConnection objCon = null;
        private static SqlConnection objConIntegracao = null;

        public static string getStringConexao()
        {
            Configuracao config = new Configuracao();
            config.LeArquivo(Properties.Settings.Default.conexaoPrincipal);

            string strConexao = "Data Source=" + Criptografia.Decrypt(config.Servidor) + ";" +
                    "Initial Catalog=" + Criptografia.Decrypt(config.BancoDeDados) + ";Persist Security Info=True;" +
                    "User ID=" + Criptografia.Decrypt(config.Usuario) + ";Password=" + Criptografia.Decrypt(config.Senha) + ";" +
                    "MultipleActiveResultSets=True;";

            return strConexao;
        }

        public static string getEntityConnection()
        {
            Configuracao config = new Configuracao();
            config.LeArquivo(Properties.Settings.Default.conexaoPrincipal);

            var providerSB = new SqlConnectionStringBuilder
            {
                InitialCatalog = Criptografia.Decrypt(config.BancoDeDados),
                DataSource = Criptografia.Decrypt(config.Servidor),
                UserID = Criptografia.Decrypt(config.Usuario),
                Password = Criptografia.Decrypt(config.Senha),
                MultipleActiveResultSets = true
            };

            var efConnection = new EntityConnectionStringBuilder();
            efConnection.Provider = "System.Data.SqlClient";
            efConnection.ProviderConnectionString = providerSB.ConnectionString;
            efConnection.Metadata = "res://*/Dominio.SGQModel.csdl|res://*/Dominio.SGQModel.ssdl|res://*/Dominio.SGQModel.msl";

            return efConnection.ConnectionString;
        }

        public static string getStringIntegracao()
        {
            Configuracao config = new Configuracao();
            config.LeArquivo(Properties.Settings.Default.conexaoIntegracao);

            string strConexao = "Data Source=" + Criptografia.Decrypt(config.Servidor) + ";" +
                    "Initial Catalog=" + Criptografia.Decrypt(config.BancoDeDados) + ";Persist Security Info=True;" +
                    "User ID=" + Criptografia.Decrypt(config.Usuario) + ";Password=" + Criptografia.Decrypt(config.Senha) + ";" +
                    "MultipleActiveResultSets=True;";

            return strConexao;
        }

        public static SqlConnection getConexaoIntegracao()
        {
            if (objConIntegracao == null)
                objConIntegracao = new SqlConnection(getStringIntegracao());

            if (objConIntegracao.State == ConnectionState.Closed)
            {
                try
                {
                    if (string.IsNullOrEmpty(objConIntegracao.ConnectionString))
                        objConIntegracao = new SqlConnection(getStringIntegracao());

                    objConIntegracao.Open();
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
            }

            return objConIntegracao;
        }

        public static SqlConnection getConexao()
        {
            if (objCon == null)
                objCon = new SqlConnection(getStringConexao());

            if (objCon.State == ConnectionState.Closed)
            {
                try
                {
                    if (string.IsNullOrEmpty(objCon.ConnectionString))
                        objCon = new SqlConnection(getStringConexao());

                    objCon.Open();
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
            }

            return objCon;
        }

        public static string getSysIntegracao()
        {
            Configuracao config = new Configuracao();
            config.LeArquivo(Properties.Settings.Default.conexaoIntegracao);

            return Criptografia.Decrypt(config.SysIntegracao);
        }
    }
}