﻿using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace QUALIDADE.Controle.Configuracoes
{
    class Configuracao
    {
        public string Servidor { get; set; }
        public string BancoDeDados { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string SysIntegracao { get; set; }

        public bool GeraArquivo(string servidor, string db, string usr, string snh, string arquivo, string sysIntegracao)
        {
            this.Servidor = Criptografia.Encrypt(servidor);
            this.BancoDeDados = Criptografia.Encrypt(db);
            this.Usuario = Criptografia.Encrypt(usr);
            this.Senha = Criptografia.Encrypt(snh);
            this.SysIntegracao = Criptografia.Encrypt(sysIntegracao);

            try
            {
                using (StreamWriter sw = new StreamWriter(arquivo, false))
                {
                    sw.WriteLine("DataSource;" + this.Servidor);
                    sw.WriteLine("InitialCatalog;" + this.BancoDeDados);
                    sw.WriteLine("UserID;" + this.Usuario);
                    sw.WriteLine("Password;" + this.Senha);

                    if (sysIntegracao != null)
                        sw.WriteLine("Integração;" + this.SysIntegracao);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao criar arquivo de conexão: '" + arquivo + "'.\n" + ex.Message);
                return false;
            }
        }

        public void LeArquivo(string arquivo)
        {
            string[] configs = new string[5];

            string programa = Assembly.GetExecutingAssembly().Location;
            string pasta = programa.Substring(0, programa.LastIndexOf('\\'));

            arquivo = pasta + "\\" + arquivo;

            try
            {
                using (StreamReader rd = new StreamReader(arquivo))
                {
                    int cont = 0;

                    while (!rd.EndOfStream)
                    {
                        string linha = rd.ReadLine();

                        if (!string.IsNullOrEmpty(linha))
                        {
                            //Pega apenas os valores de cada 'variável' da linha
                            string[] dados = linha.Split(';');

                            //armazena os valores em uma posição de 'configs'
                            configs[cont] = dados[1];
                        }
                        cont++;
                    }
                    //armazena os dados lidos do arquivo de configuração na classe
                    this.Servidor = configs[0];
                    this.BancoDeDados = configs[1];
                    this.Usuario = configs[2];
                    this.Senha = configs[3];
                    this.SysIntegracao = configs[4];
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    }
}