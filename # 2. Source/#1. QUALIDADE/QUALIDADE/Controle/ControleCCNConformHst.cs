﻿using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCCNConformHst : Controle<CCNCONFORMHST>
    {
        public int GetID()
        {
            int newId = 0;

            var nConform = (from f in GetAll()
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (nConform != null)
            {
                Nullable<int> item = nConform.CODIGO;
                if (item != null)
                {
                    newId = Convert.ToInt32(item.Value);
                }
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<CCNCONFORMHST> GetList(int codColigada, string cCusto, int codNConf)
        {
            List<CCNCONFORMHST> nConformidade = new List<CCNCONFORMHST>();
            var nConform = (from f in Get(x => x.CODCOLIGADA == codColigada &&
                                x.CODCCUSTO == cCusto && x.CODNCONFORM == codNConf
                           )
                            orderby f.RECCREATEDBY ascending
                            select f);

            if (nConform != null)
            {
                foreach (var item in nConform)
                {
                    nConformidade.Add(item);
                }
            }

            return nConformidade;
        }
    }
}
