﻿using QUALIDADE.Dominio;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePCliente : Controle<PCLIENTES>
    {
        public int GetNewID(short CODCOLIGADA)
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == CODCOLIGADA
                            orderby f.CODCLIENTE descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODCLIENTE;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }
    }
}