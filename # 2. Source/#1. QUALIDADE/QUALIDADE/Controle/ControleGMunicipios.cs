﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Data;

namespace QUALIDADE.Controle
{
    public class ControleGMunicipios : Controle<GMUNICIPIO>
    {
        public List<GMUNICIPIO> GetIntegracaoRM()
        {
            List<GMUNICIPIO> lista = new List<GMUNICIPIO>();

            string query = @"SELECT [CODMUNICIPIO]
                                  ,[CODETDMUNICIPIO]
                                  ,[NOMEMUNICIPIO]
                                  ,[RECCREATEDBY]
                                  ,[RECCREATEDON]
                                  ,[RECMODIFIEDBY]
                                  ,[RECMODIFIEDON]
                              FROM [dbo].[GMUNICIPIO]";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                GMUNICIPIO estados = new GMUNICIPIO
                {
                    CODMUNICIPIO = row["CODMUNICIPIO"].DBNullToString(),
                    CODETDMUNICIPIO = row["CODETDMUNICIPIO"].DBNullToString(),
                    NOMEMUNICIPIO = row["NOMEMUNICIPIO"].DBNullToString(),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(estados);
            }

            return lista;
        }
    }
}
