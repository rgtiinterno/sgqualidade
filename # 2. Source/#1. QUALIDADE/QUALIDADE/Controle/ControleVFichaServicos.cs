﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleVFichaServicos : Controle<VFICHAVERFSERVICOS>
    {
        public short GetNewID(short CODCOLIGADA)
        {
            short newId = 0;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == CODCOLIGADA
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                Nullable<short> item = string.IsNullOrEmpty(usuarios.CODIGO) ? (short?)null : Convert.ToInt16(usuarios.CODIGO);
                if (item != null)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<VFICHAVERFSERVICOS> GetAll(short codColigada)
        {
            List<VFICHAVERFSERVICOS> lista = new List<VFICHAVERFSERVICOS>();
            var result = from f in GetAll()
                         where f.CODCOLIGADA == codColigada
                         select f;
            foreach (var item in result)
            {
                GPERMISSAO permissao = context.GPERMISSAO.Find(item.CODCOLIGADA, FormPrincipal.getUsuarioAcesso().CODUSUARIO, item.CODCCUSTO, "V");
                if (permissao != null)
                {
                    GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                    item.DESCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                    lista.Add(item);
                }
            }

            return lista;
        }
    }
}