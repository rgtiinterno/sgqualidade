﻿using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace QUALIDADE.Controle
{
    public class ControleAAfericaoExternaAnexo : Controle<AAFERICAOEXTERNAANEXO>
    {
        public int GetNewSequencial(int codAfericao, int codColigada, string codCfo, int rev)
        {
            int ultId = 0;
            DataTable dt = GetDataTable
            (
                @"SELECT TOP 1 SEQUENCIAL FROM AAFERICAOEXTERNAANEXO 
                WHERE CODAFERICAO=@CODAFERICAO AND CODCOLIGADA=@CODCOLIGADA 
                  AND CODCFO=@CODCFO AND REV=@REV 
                ORDER BY SEQUENCIAL DESC ",
                new List<SqlParameter>
                {
                    new SqlParameter("@CODAFERICAO", codAfericao),
                    new SqlParameter("@CODCOLIGADA", codColigada),
                    new SqlParameter("@CODCFO", codCfo),
                    new SqlParameter("@REV", rev),
                }
            );

            if (dt?.Rows?.Count > 0)
                ultId = dt.Rows[0]["SEQUENCIAL"].DBNullToInt() ?? 0;

            return (ultId + 1);

            //int newId = (from f in Get(x => 
            //                x.CODAFERICAO == codAfericao &&
            //                x.CODCOLIGADA == codColigada &&
            //                x.CODCFO == codCfo &&
            //                x.REV == rev)
            //             orderby f.SEQUENCIAL descending
            //             select f.SEQUENCIAL).FirstOrDefault();
            //return (newId + 1);
        }

        public List<AAFERICAOEXTERNAANEXO> GetAllSemAnexo(int codAfericao, int codColigada, string codCfo, int rev)
        {
            List<AAFERICAOEXTERNAANEXO> itens = new List<AAFERICAOEXTERNAANEXO>();

            DataTable dt = GetDataTable
            (
                @"SELECT
	                CODAFERICAO, CODCOLIGADA, CODCFO, REV, SEQUENCIAL, 
	                EXTENSAO, DESCRICAO, TAMANHO, RECCREATEDBY, RECCREATEDON, 
	                RECMODIFIEDBY, RECMODIFIEDON 
                FROM AAFERICAOEXTERNAANEXO 
                WHERE CODAFERICAO=@CODAFERICAO AND CODCOLIGADA=@CODCOLIGADA 
                  AND CODCFO=@CODCFO AND REV=@REV ", 
                new List<SqlParameter>
                {
                    new SqlParameter("@CODAFERICAO", codAfericao),
                    new SqlParameter("@CODCOLIGADA", codColigada),
                    new SqlParameter("@CODCFO", codCfo),
                    new SqlParameter("@REV", rev),
                }
            );

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    itens.Add(new AAFERICAOEXTERNAANEXO
                    {
                        CODAFERICAO = row["CODAFERICAO"].DBNullToInt().Value,
                        CODCOLIGADA = row["CODCOLIGADA"].DBNullToInt().Value,
                        CODCFO = row["CODCFO"].DBNullToString(),
                        REV = row["REV"].DBNullToInt().Value,
                        SEQUENCIAL = row["SEQUENCIAL"].DBNullToInt().Value,
                        ARQUIVO = null,
                        EXTENSAO = row["EXTENSAO"].DBNullToString(),
                        DESCRICAO = row["DESCRICAO"].DBNullToString(),
                        TAMANHO = row["TAMANHO"].DBNullToLong(),
                        RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                        RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                        RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                        RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                    });
                }
            }
            return itens;
        }
    }
}