﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Email;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public abstract class Controle<TEntity> : IDisposable, IControle<TEntity> where TEntity : class
    {
        protected QUALIDADEEntities context = new QUALIDADEEntities(SqlConn.getEntityConnection());

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return GetAll().Where(predicate).AsQueryable();
        }

        public BindingList<TEntity> GetAllObservable()
        {
            context.Set<TEntity>().Load();
            return context.Set<TEntity>().Local.ToBindingList();
        }

        public TEntity Find(params object[] key)
        {
            return context.Set<TEntity>().Find(key);
        }

        public void Create(TEntity obj)
        {
            context.Set<TEntity>().Add(obj);
        }

        public void CreateAll(List<TEntity> obj)
        {
            context.Set<TEntity>().AddRange(obj);
        }

        public void Update(TEntity obj, Func<TEntity, bool> predicate)
        {
            var local = context.Set<TEntity>().Local.FirstOrDefault(predicate);

            if (local != null)
                context.Entry(obj).State = EntityState.Detached;

            context.Entry(obj).State = EntityState.Modified;
        }

        public void Detach(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Detached;
        }

        public void Delete(Func<TEntity, bool> predicate)
        {
            context.Set<TEntity>()
                   .Where(predicate).ToList()
                   .ForEach(del => context.Set<TEntity>().Remove(del));
        }

        public void SaveAll()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Exists(Func<TEntity, bool> predicate)
        {
            TEntity item = null;

            using (QUALIDADEEntities contextAux = new QUALIDADEEntities(SqlConn.getEntityConnection()))
            {
                item = contextAux.Set<TEntity>().Where(predicate).FirstOrDefault();
            }

            return item != null;
        }

        public bool ExecuteSQL(string query, List<SqlParameter> parametros = null)
        {
            try
            {
                if (parametros == null || parametros.Count <= 0)
                    context.Database.ExecuteSqlCommand(query);
                else
                    context.Database.ExecuteSqlCommand(query, parametros.ToArray());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string query, List<SqlParameter> parametros = null)
        {
            try
            {
                DataTable dataTable = new DataTable();
                SqlConnection conn = SqlConn.getConexao();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    if(parametros != null && parametros.Count > 0)
                    {
                        foreach(SqlParameter sqlParam in parametros)
                        {
                            cmd.Parameters.AddWithValue(sqlParam.ParameterName, sqlParam.Value.ToNullableParam());
                        }
                    }
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader != null)
                        dataTable.Load(reader);
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cria uma nova Tabela no Banco de dados (SQL Server), caso ela não exista
        /// </summary>
        /// <param name="tabela">Nome da nova tabela</param>
        /// <param name="definicaoColunas">Definição das colunas da tabela</param>
        /// <returns>Status da criação</returns>
        public bool CriaTabela(string tabela, string definicaoColunas)
        {
            SqlConnection connSql = SqlConn.getConexao();

            try
            {
                if(connSql.State != ConnectionState.Open)
                    connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='" + tabela + @"') CREATE TABLE dbo." + tabela + " ( " + definicaoColunas + " )";
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                Mensagem.Erro("Falha na atualização", "Não foi possível atualizar o Banco de dados.\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Adiciona uma nova coluna em uma tabela do Banco de dados (SQL Server)
        /// </summary>
        /// <param name="tabela">Tabela que será modificada</param>
        /// <param name="coluna">Definição da nova coluna</param>
        /// <returns>Status da criação</returns>
        public bool CriaColuna(string tabela, string coluna, string tipoColuna)
        {
            SqlConnection connSql = SqlConn.getConexao();

            try
            {
                if (connSql.State != ConnectionState.Open)
                    connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"IF NOT EXISTS (SELECT 1 FROM SYSCOLUMNS WHERE ID = OBJECT_ID('{tabela}') AND NAME = '{coluna}') ALTER TABLE {tabela} ADD {coluna} {tipoColuna}";
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                Mensagem.Erro("Falha na atualização", "Não foi possível atualizar o Banco de dados.\n" + ex.Message);
                return false;
            }
        }

        public bool CriaConstraint(string tabela, string nomeConstraint, string definicao)
        {
            SqlConnection connSql = SqlConn.getConexao();

            try
            {
                if (connSql.State != ConnectionState.Open)
                    connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"IF NOT EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_NAME(OBJECT_ID)='{nomeConstraint}') ALTER TABLE {tabela} ADD CONSTRAINT {nomeConstraint} {definicao}";
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                Mensagem.Erro("Falha na atualização", "Não foi possível atualizar o Banco de dados.\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Exclui a constraint (chave primária ou estrangeira) da tabela
        /// </summary>
        /// <param name="tabela">Nome da Tabela que será verificada</param>
        /// <param name="nomeConstraint">Nome da Chave a ser excluída</param>
        /// <returns></returns>
        public bool DeletaConstraint(string tabela, string nomeConstraint)
        {
            SqlConnection connSql = SqlConn.getConexao();

            try
            {
                connSql.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = $@"IF EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_NAME(OBJECT_ID)='{nomeConstraint}') ALTER TABLE {tabela} DROP CONSTRAINT {nomeConstraint}";
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                Mensagem.Erro("Falha na atualização", "Não foi possível atualizar o Banco de dados.\n" + ex.Message);
                return false;
            }
            finally
            {
                if (connSql.State == ConnectionState.Open)
                    connSql.Close();
            }
        }

        internal List<CorpoEmail> GetUsuariosEnvioEmail()
        {
            List<CorpoEmail> itens = new List<CorpoEmail>();
            DataTable dtDiretores = GetDataTable(
                @"SELECT DI.CODDIRETOR, U.NOME, U.EMAIL FROM FPERMISSAO PC 
                INNER JOIN GDIRETORCCUSTO DI ON(DI.CODCOLIGADA=PC.CODCOLIGADA AND DI.CODCCUSTO=PC.CODCCUSTO) 
                INNER JOIN GUSUARIO U ON(U.CODUSUARIO=DI.CODDIRETOR) 
                WHERE U.DIRETORIA=1 
                GROUP BY DI.CODDIRETOR, U.NOME, U.EMAIL
                ORDER BY U.NOME"
            );

            if(dtDiretores != null && dtDiretores.Rows.Count > 0)
            {
                foreach(DataRow rowDiretor in dtDiretores.Rows)
                {
                    DataTable dtCentrosCusto = GetDataTable(
                        @"SELECT CC.CODCOLIGADA, CC.CODCCUSTO, CC.NOME FROM GCENTROCUSTO CC 
                        INNER JOIN GDIRETORCCUSTO DI ON(DI.CODCOLIGADA=CC.CODCOLIGADA AND DI.CODCCUSTO=CC.CODCCUSTO) 
                        WHERE DI.CODDIRETOR=@CODDIRETOR 
                        ORDER BY CC.CODCOLIGADA, CC.CODCCUSTO", 
                        new List<SqlParameter> { new SqlParameter("@CODDIRETOR", rowDiretor["CODDIRETOR"]) }
                    );

                    if(dtCentrosCusto != null && dtCentrosCusto.Rows.Count > 0)
                    {
                        CorpoEmail itemEmail = new CorpoEmail
                        {
                            CodUsuario = rowDiretor["CODDIRETOR"].DBNullToInt().Value,
                            NomeUsuario = rowDiretor["NOME"].DBNullToString(),
                            EmailUsuario = rowDiretor["EMAIL"].DBNullToString(),
                            CentroCustoEmail = new List<CentroCustoEmail>()
                        };

                        foreach(DataRow rowCCusto in dtCentrosCusto.Rows)
                        {
                            CentroCustoEmail itemCCusto = new CentroCustoEmail
                            {
                                CodColigada = rowCCusto["CODCOLIGADA"].DBNullToSmallint().Value, 
                                CodCentroCusto = rowCCusto["CODCCUSTO"].DBNullToString(),
                                NomeCentroCusto = rowCCusto["NOME"].DBNullToString(),
                                InfAfericao = null, 
                                InfCalibracao = null, 
                                InfFornecedores = null
                            };

                            #region "Obtém Aferições"
                            DataTable dtAfericoes = GetDataTable(
                                @"WITH TAB AS (
	                                SELECT
		                                AF.CODIGO, AF.CODCOLIGADA, AF.CODCFO, AF.NOMEINSTRUMENTO, 
		                                AF.IDENTIFICACAO, AF.CODCCUSTO, CC.NOME AS CENTROCUSTO, 
		                                (CASE 
			                                WHEN AF.RESULTADO=1 THEN 'CONFORME' 
			                                WHEN AF.RESULTADO=0 AND AF.RESULTADOSTR='REPROVADO' THEN 'NAOCONFORME' 
			                                WHEN AF.RESULTADO=0 AND AF.RESULTADOSTR IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)<0 THEN 'VENCIDA' 
			                                WHEN AF.RESULTADO=0 AND AF.RESULTADOSTR IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)<=7 THEN 'VENCE_ATE_7' 
			                                WHEN AF.RESULTADO=0 AND AF.RESULTADOSTR IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)>=0 THEN 'PENDENTE' 
		                                END) AS RESULTADO  
	                                FROM AAFERICAOINTERNA AF 
	                                INNER JOIN GCENTROCUSTO CC ON(CC.CODCOLIGADA=AF.CODCOLIGADA AND CC.CODCCUSTO=AF.CODCCUSTO) 
	                                WHERE AF.CODCOLIGADA=@CODCOLIGADA AND AF.CODCCUSTO=@CODCCUSTO AND AF.ATIVO=1 
	                                  AND AF.REV=(
		                                SELECT MAX(AF2.REV) AS REV FROM AAFERICAOINTERNA AF2 
		                                WHERE AF2.CODIGO=AF.CODIGO AND AF2.CODCOLIGADA=AF.CODCOLIGADA AND AF2.CODCFO=AF.CODCFO 
	                                  )
                                ) 
                                SELECT 
	                                T.CODCOLIGADA, T.CODCCUSTO, T.CENTROCUSTO, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO) AS TOTAL, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='CONFORME') AS CONFORMES, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='NAOCONFORME') AS NAOCONFORMES, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='VENCIDA') AS VENCIDAS, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='VENCE_ATE_7') AS VENCEM_ATE_7, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='PENDENTE') AS PENDENTES 
                                FROM TAB T 
                                GROUP BY T.CODCOLIGADA, T.CODCCUSTO, T.CENTROCUSTO
                                ORDER BY T.CENTROCUSTO",
                                new List<SqlParameter>
                                {
                                    new SqlParameter("@CODCOLIGADA", itemCCusto.CodColigada),
                                    new SqlParameter("@CODCCUSTO", itemCCusto.CodCentroCusto)
                                }
                            );

                            if (dtAfericoes != null && dtAfericoes.Rows.Count > 0)
                            {
                                DataRow rowAfericao = dtAfericoes.Rows[0];
                                itemCCusto.InfAfericao = new AlertaAfericao
                                {
                                    CodColigada = itemCCusto.CodColigada,
                                    CodCCusto = itemCCusto.CodCentroCusto,
                                    Total = rowAfericao["TOTAL"].DBNullToInt() ?? 0,
                                    Conformes = rowAfericao["CONFORMES"].DBNullToInt() ?? 0,
                                    NaoConformes = rowAfericao["NAOCONFORMES"].DBNullToInt() ?? 0,
                                    Pendentes = rowAfericao["PENDENTES"].DBNullToInt() ?? 0,
                                    Vencidas = rowAfericao["VENCIDAS"].DBNullToInt() ?? 0,
                                    Vencem7Dias = rowAfericao["VENCEM_ATE_7"].DBNullToInt() ?? 0,
                                };
                            }
                            #endregion

                            #region "Obtém Calibrações"
                            DataTable dtCalibracoes = GetDataTable(
                                @"WITH TAB AS (
	                                SELECT
		                                AF.CODIGO, AF.CODCOLIGADA, AF.CODCFO, AF.NOMEINSTRUMENTO, 
		                                AF.IDENTIFICACAO, AF.CODCCUSTO, CC.NOME AS CENTROCUSTO, 
		                                (CASE 
			                                WHEN ISNULL(AF.RESULTADOBIT,0)=1 THEN 'CONFORME' 
			                                WHEN ISNULL(AF.RESULTADOBIT,0)=0 AND CONVERT(VARCHAR,AF.RESULTADO)='REPROVADO' THEN 'NAOCONFORME' 
			                                WHEN AF.RESULTADOBIT IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)<0 THEN 'VENCIDA' 
			                                WHEN AF.RESULTADOBIT IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)<=7 THEN 'VENCE_ATE_7' 
			                                WHEN AF.RESULTADOBIT IS NULL AND DATEDIFF(DAY, GETDATE(), AF.VALIDADE)>=0 THEN 'PENDENTE' 
		                                END) AS RESULTADO  
	                                FROM AAFERICAOEXTERNA AF 
	                                INNER JOIN GCENTROCUSTO CC ON(CC.CODCOLIGADA=AF.CODCOLIGADA AND CC.CODCCUSTO=AF.CODCCUSTO) 
	                                WHERE AF.CODCOLIGADA=@CODCOLIGADA AND AF.CODCCUSTO=@CODCCUSTO AND AF.ATIVO=1 
	                                  AND AF.REV=(
		                                SELECT MAX(AF2.REV) AS REV FROM AAFERICAOEXTERNA AF2 
		                                WHERE AF2.CODIGO=AF.CODIGO AND AF2.CODCOLIGADA=AF.CODCOLIGADA AND AF2.CODCFO=AF.CODCFO 
	                                  )
                                ) 
                                SELECT 
	                                T.CODCOLIGADA, T.CODCCUSTO, T.CENTROCUSTO, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO) AS TOTAL, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='CONFORME') AS CONFORMES, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='NAOCONFORME') AS NAOCONFORMES, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='VENCIDA') AS VENCIDAS, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='VENCE_ATE_7') AS VENCEM_ATE_7, 
	                                (SELECT COUNT(*) FROM TAB T2 WHERE T2.CODCOLIGADA=T.CODCOLIGADA AND T2.CODCCUSTO=T.CODCCUSTO AND T2.RESULTADO='PENDENTE') AS PENDENTES 
                                FROM TAB T 
                                GROUP BY T.CODCOLIGADA, T.CODCCUSTO, T.CENTROCUSTO
                                ORDER BY T.CENTROCUSTO",
                                new List<SqlParameter>
                                {
                                    new SqlParameter("@CODCOLIGADA", itemCCusto.CodColigada),
                                    new SqlParameter("@CODCCUSTO", itemCCusto.CodCentroCusto)
                                }
                            );

                            if (dtCalibracoes != null && dtCalibracoes.Rows.Count > 0)
                            {
                                DataRow rowCalibracao = dtCalibracoes.Rows[0];
                                itemCCusto.InfCalibracao = new AlertaCalibracao
                                {
                                    CodColigada = itemCCusto.CodColigada,
                                    CodCCusto = itemCCusto.CodCentroCusto,
                                    Total = rowCalibracao["TOTAL"].DBNullToInt() ?? 0,
                                    Aprovadas = rowCalibracao["CONFORMES"].DBNullToInt() ?? 0,
                                    Reprovadas = rowCalibracao["NAOCONFORMES"].DBNullToInt() ?? 0,
                                    Pendentes = rowCalibracao["PENDENTES"].DBNullToInt() ?? 0,
                                    Vencidas = rowCalibracao["VENCIDAS"].DBNullToInt() ?? 0,
                                    Vencem7Dias = rowCalibracao["VENCEM_ATE_7"].DBNullToInt() ?? 0,
                                };
                            }
                            #endregion

                            #region "Obtém Fornecedores"
                            DataTable dtFornecedores = GetDataTable(
                                @"SELECT * FROM (
	                                SELECT 
		                                FO.CODCOLIGADA, FO.CODCFO, FO.NOMEFANTASIA AS FORNECEDOR, FO.CGCCFO, FO.PAGREC, 
		                                FO.PESSOAFISOUJUR, FO.CODTPFCFO, TP.DESCRICAO AS DESCTPFCFO, FO.IDCFO, 
		                                (
			                                SELECT COUNT(*) AS TOTAL FROM FARQUIVOCFO TA 
			                                WHERE TA.CODCOLIGADA=FO.CODCOLIGADA AND TA.CODTPCFO=FO.CODTPFCFO 
		                                ) AS REQUISITADOS, 
		                                (
			                                SELECT COUNT(*) AS TOTAL 
			                                FROM FARQUIVOCFO TA 
			                                LEFT JOIN FCFO F ON(F.CODCOLIGADA=TA.CODCOLIGADA AND F.CODTPFCFO=TA.CODTPCFO) 
			                                LEFT JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO AND A.CODTPARQUIVO=TA.CODTPARQUIVO) 
			                                WHERE TA.CODCOLIGADA=FO.CODCOLIGADA AND F.CODCFO=FO.CODCFO AND A.CODTPARQUIVO IS NULL 
		                                ) AS PENDENTES, 
		                                ISNULL((
			                                SELECT COUNT(*) AS TOTAL FROM FCFO F 
			                                INNER JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO) 
			                                INNER JOIN FARQUIVOCFO FA ON(FA.CODCOLIGADA=A.CODCOLIGADA AND FA.CODTPCFO=F.CODTPFCFO AND FA.CODTPARQUIVO=A.CODTPARQUIVO) 
			                                WHERE F.CODCOLIGADA=FO.CODCOLIGADA AND F.CODCFO=FO.CODCFO AND F.CODTPFCFO=FO.CODTPFCFO 
				                                AND A.POSSUIVALIDADE=1 AND DATEDIFF(DAY, GETDATE(), A.DATAVALIDADE)<0 
			                                GROUP BY A.CODCOLIGADA, A.CODCFO 
		                                ),0) AS VENCIDOS, 
		                                ISNULL((
			                                SELECT COUNT(*) AS TOTAL FROM FCFO F 
			                                INNER JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO) 
			                                INNER JOIN FARQUIVOCFO FA ON(FA.CODCOLIGADA=A.CODCOLIGADA AND FA.CODTPCFO=F.CODTPFCFO AND FA.CODTPARQUIVO=A.CODTPARQUIVO) 
			                                WHERE F.CODCOLIGADA=FO.CODCOLIGADA AND F.CODCFO=FO.CODCFO AND F.CODTPFCFO=FO.CODTPFCFO 
				                                AND A.POSSUIVALIDADE=1 AND DATEDIFF(DAY, GETDATE(), A.DATAVALIDADE)>=0 AND DATEDIFF(DAY, GETDATE(), A.DATAVALIDADE)<=7 
			                                GROUP BY A.CODCOLIGADA, A.CODCFO 
		                                ),0) AS VENCEM_ATE_7 
	                                FROM FCFO FO 
	                                INNER JOIN FTPFCFO TP ON(TP.CODIGO=FO.CODTPFCFO) 
	                                INNER JOIN FPERMISSAO PE ON(PE.CODCOLIGADA=FO.CODCOLIGADA AND PE.CODCFO=FO.CODCFO) 
	                                INNER JOIN GCENTROCUSTO CC ON(CC.CODCOLIGADA=PE.CODCOLIGADA AND CC.CODCCUSTO=PE.CODCCUSTO) 
	                                WHERE FO.CODCOLIGADA=@CODCOLIGADA AND CC.CODCCUSTO=@CODCCUSTO AND FO.ATIVO=1 
                                ) IT 
                                WHERE (IT.VENCIDOS>0 OR IT.PENDENTES>0 OR IT.VENCEM_ATE_7>0) 
                                ORDER BY IT.CODCOLIGADA, IT.FORNECEDOR ",
                                new List<SqlParameter>
                                {
                                    new SqlParameter("@CODCOLIGADA", itemCCusto.CodColigada),
                                    new SqlParameter("@CODCCUSTO", itemCCusto.CodCentroCusto)
                                }
                            );

                            if(dtFornecedores != null && dtFornecedores.Rows.Count > 0)
                            {
                                itemCCusto.InfFornecedores = new List<AlertaFornecedor>();
                                foreach (DataRow rowFornec in dtFornecedores.Rows)
                                {
                                    itemCCusto.InfFornecedores.Add(new AlertaFornecedor
                                    {
                                        CodColigada = itemCCusto.CodColigada,
                                        CodCCusto = itemCCusto.CodCentroCusto,
                                        CodColFornecedor = rowFornec["CODCOLIGADA"].DBNullToSmallint() ?? 0,
                                        CodFornecedor = rowFornec["CODCFO"].DBNullToString(),
                                        Fornecedor = rowFornec["FORNECEDOR"].DBNullToString(),
                                        CgcFornecedor = rowFornec["CGCCFO"].DBNullToString(),
                                        Requisitados = rowFornec["REQUISITADOS"].DBNullToInt() ?? 0,
                                        Pendentes = rowFornec["PENDENTES"].DBNullToInt() ?? 0,
                                        Vencidos = rowFornec["VENCIDOS"].DBNullToInt() ?? 0,
                                        Vencem7Dias = rowFornec["VENCEM_ATE_7"].DBNullToInt() ?? 0,
                                    });
                                }
                            }
                            #endregion

                            if(itemCCusto.InfAfericao != null || itemCCusto.InfCalibracao != null || itemCCusto.InfFornecedores?.Count > 0)
                                itemEmail.CentroCustoEmail.Add(itemCCusto);
                        }

                        if (itemEmail.CentroCustoEmail?.Count > 0)
                            itens.Add(itemEmail);
                    }
                }
            }
            return itens;
        }

        public void Dispose()
        {
            if (context != null)
                context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}