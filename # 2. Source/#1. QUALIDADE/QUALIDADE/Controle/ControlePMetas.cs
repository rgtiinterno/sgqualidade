﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePMetas : Controle<PMETAS>
    {
        public string Info()
        {
            var item = (from m in GetAll()
                        orderby m.ANOREF descending
                        select m).FirstOrDefault<PMETAS>();

            return $"Metas:         Nº Propostas Enviadas: {item.NUMPROPOSTASENVIADAS}  |   " +
                $"Nº Propostas Vencidas: {item.NUMPROPOSTASVENCIDAS}  |   Ano ref.: {item.ANOREF}";
        }


        public bool Update(PMETAS item, string anoRefAnterior)
        {
            string sql = $@"UPDATE PMETAS SET NUMPROPOSTASENVIADAS='{item.NUMPROPOSTASENVIADAS}', 
	        NUMPROPOSTASVENCIDAS='{item.NUMPROPOSTASVENCIDAS}', ANOREF='{item.ANOREF}', 
            RECMODIFIEDBY='{item.RECMODIFIEDBY}', RECMODIFIEDON='{item.RECMODIFIEDON}' 
            WHERE ANOREF='{anoRefAnterior}'";

            return Funcoes.ExecutaFuncao(sql);
        }
    }
}
