﻿using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleFArquivoFornecedor : Controle<FARQUIVOCFO>
    {
        public bool TemArquivoVencido(int codColigada, string codCfo, int codTipoCfo)
        {
            bool temArquivoPendente = false;
            DataTable dataTable = this.GetDataTable(
                @"SELECT COUNT(*) AS TOTAL FROM FCFO F 
                INNER JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO) 
                INNER JOIN FARQUIVOCFO FA ON(FA.CODCOLIGADA=A.CODCOLIGADA AND FA.CODTPCFO=F.CODTPFCFO AND FA.CODTPARQUIVO=A.CODTPARQUIVO) 
                WHERE F.CODCOLIGADA=@CODCOLIGADA AND F.CODCFO=@CODCFO AND F.CODTPFCFO=@CODTPFCFO 
                    AND A.POSSUIVALIDADE=1 AND DATEDIFF(DAY, GETDATE(), A.DATAVALIDADE)<0 
                GROUP BY A.CODCOLIGADA, A.CODCFO, A.CODTPARQUIVO", 
                new List<SqlParameter>() { 
                    new SqlParameter("@CODCOLIGADA", codColigada),
                    new SqlParameter("@CODCFO", codCfo),
                    new SqlParameter("@CODTPFCFO", codTipoCfo),
                });

            if(dataTable != null && dataTable.Rows.Count > 0)
                temArquivoPendente = (dataTable.Rows[0]["TOTAL"].DBNullToInt() ?? 0) > 0;

            return temArquivoPendente;
        }

        public bool TemArquivoPendente(int codColigada, string codCfo)
        {
            bool temArquivoPendente = false;
            DataTable dataTable = this.GetDataTable(
                @"SELECT COUNT(*) AS TOTAL 
                FROM FARQUIVOCFO TA 
                LEFT JOIN FCFO F ON(F.CODCOLIGADA=TA.CODCOLIGADA AND F.CODTPFCFO=TA.CODTPCFO) 
                LEFT JOIN FARQFCFO A ON(A.CODCOLIGADA=F.CODCOLIGADA AND A.CODCFO=F.CODCFO AND A.CODTPARQUIVO=TA.CODTPARQUIVO) 
                WHERE TA.CODCOLIGADA=@CODCOLIGADA AND F.CODCFO=@CODCFO AND A.CODTPARQUIVO IS NULL ",
                new List<SqlParameter>() {
                    new SqlParameter("@CODCOLIGADA", codColigada),
                    new SqlParameter("@CODCFO", codCfo)
                });

            if (dataTable != null && dataTable.Rows.Count > 0)
                temArquivoPendente = (dataTable.Rows[0]["TOTAL"].DBNullToInt() ?? 0) > 0;

            return temArquivoPendente;
        }

        public List<FARQUIVOCFO> GetList(FCFO cFornec)
        {
            List<FARQUIVOCFO> list = new List<FARQUIVOCFO>();
            var result = (from f in GetAll()
                          where f.CODTPCFO == cFornec.CODTPFCFO && f.CODCOLIGADA == cFornec.CODCOLIGADA
                          select f);

            foreach (var item in result)
            {
                FARQUIVOCFO fArq = new FARQUIVOCFO
                {
                    CKSELECIONADO = false,
                    CODTPCFO = item.CODTPCFO,
                    CODCOLIGADA = item.CODCOLIGADA,
                    CODTPARQUIVO = item.CODTPARQUIVO,
                    RECCREATEDBY = item.RECCREATEDBY,
                    RECCREATEDON = item.RECCREATEDON,
                    RECMODIFIEDBY = item.RECMODIFIEDBY,
                    RECMODIFIEDON = item.RECMODIFIEDON
                };

                FARQFCFO arquivo = context.FARQFCFO.Find(item.CODCOLIGADA, cFornec.CODCFO, item.CODTPARQUIVO);
                if (arquivo != null)
                {
                    fArq.DATAVALIDADEARQUIVO = arquivo.DATAVALIDADE;
                    fArq.TEMVALIDADE = arquivo.POSSUIVALIDADE;
                }

                FTPARQUIVO tpArquivo = context.FTPARQUIVO.Find(item.CODTPARQUIVO);
                if (tpArquivo != null)
                    fArq.NOMEARQUIVO = tpArquivo.NOME;
                list.Add(fArq);
            }

            return list;
        }

        public List<FARQUIVOCFO> GetList(int codigo, short codColigada)
        {
            List<FARQUIVOCFO> list = new List<FARQUIVOCFO>();
            var result = (from f in GetAll()
                          where f.CODTPCFO == codigo && f.CODCOLIGADA == codColigada
                          select f);

            foreach (var item in result)
            {
                FARQUIVOCFO fArq = new FARQUIVOCFO
                {
                    CKSELECIONADO = false,
                    CODTPCFO = item.CODTPCFO,
                    CODCOLIGADA = item.CODCOLIGADA,
                    CODTPARQUIVO = item.CODTPARQUIVO,
                    RECCREATEDBY = item.RECCREATEDBY,
                    RECCREATEDON = item.RECCREATEDON,
                    RECMODIFIEDBY = item.RECMODIFIEDBY,
                    RECMODIFIEDON = item.RECMODIFIEDON
                };

                FTPARQUIVO tpArquivo = context.FTPARQUIVO.Find(item.CODTPARQUIVO);
                if (tpArquivo != null)
                {
                    fArq.NOMEARQUIVO = tpArquivo.NOME;
                    fArq.DATAVALIDADEARQUIVO = tpArquivo.DATAVALIDADE;
                }

                list.Add(fArq);
            }

            return list;
        }
    }
}