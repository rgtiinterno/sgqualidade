﻿using QUALIDADE.Dominio;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePResponsaveis : Controle<PRESPONSAVEL>
    {
        public int GetNewID()
        {
            int newId = 0;
            var itens = (from f in GetAll()
                        orderby f.CODIGO descending
                        select f).FirstOrDefault();

            if (itens != null)
            {
                int? item = itens?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }
    }
}
