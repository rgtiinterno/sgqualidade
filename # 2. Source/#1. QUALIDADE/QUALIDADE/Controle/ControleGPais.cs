﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGPais : Controle<GPAIS>
    {
        public short GetID()
        {
            int newId = 0;

            var pais = (from f in GetAll()
                        orderby f.IDPAIS descending
                        select f).FirstOrDefault();

            if (pais != null)
            {
                Nullable<int> item = pais.IDPAIS;
                if (item != null)
                {
                    newId = Convert.ToInt16(item.Value);
                }
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<GPAIS> GetIntegracaoRM()
        {
            List<GPAIS> lista = new List<GPAIS>();

            string query = @"SELECT [IDPAIS]
                                  ,[CODPAIS]
                                  ,[DESCRICAO]
                                  ,[RECCREATEDBY]
                                  ,[RECCREATEDON]
                                  ,[RECMODIFIEDBY]
                                  ,[RECMODIFIEDON]
                              FROM [dbo].[GPAIS]";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                GPAIS pais = new GPAIS
                {
                    CODPAIS = row["CODPAIS"].DBNullToString(),
                    DESCRICAO = row["DESCRICAO"].DBNullToString(),
                    IDPAIS = Convert.ToInt16(row["IDPAIS"]),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(pais);
            }

            return lista;
        }
    }
}
