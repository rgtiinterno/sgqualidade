﻿using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGPermissao : Controle<GPERMISSAO>
    {
        public List<GCENTROCUSTO> GetComboCCusto(int codColigada, int codUsuario, string codSistema)
        {
            List<GPERMISSAO> permissao = Get(x => x.CODCOLIGADA == codColigada &&
            x.CODUSUARIO == codUsuario && x.CODSISTEMA == codSistema).ToList();

            List<GCENTROCUSTO> cCusto = new List<GCENTROCUSTO>();

            permissao.ForEach(x => cCusto.Add(
                context.GCENTROCUSTO.Find(x.CODCOLIGADA, x.CODCCUSTO)
                )
            );

            for (int i = 0; i < cCusto.Count; i++)
            {
                cCusto[i].NOME = $"{cCusto[i].CODCCUSTO} - {cCusto[i].NOME}";
            }

            return cCusto;
        }

        public List<GUSUARIO> GetUsuarios(short codColigada, string cCusto)
        {
            List<GUSUARIO> lista = new List<GUSUARIO>();
            var result = (from p in GetAll()
                          where p.CODCCUSTO == cCusto && p.CODCOLIGADA == codColigada
                          select p);

            foreach (var item in result)
            {
                if (!lista.Exists(x => x.CODCOLIGADA == item.CODCOLIGADA && x.CODUSUARIO == item.CODUSUARIO))
                    lista.Add(context.GUSUARIO.Find(item.CODUSUARIO));
            }

            return lista;
        }
    }
}
