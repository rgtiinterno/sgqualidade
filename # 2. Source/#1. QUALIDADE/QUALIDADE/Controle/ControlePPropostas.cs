﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePPropostas : Controle<PPROPOSTAS>
    {
        public int GetNewID(short CODCOLIGADA)
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == CODCOLIGADA
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<PPROPOSTAS> GetList(short CODCOLIGADA)
        {
            List<PPROPOSTAS> list = new List<PPROPOSTAS>();
            var result = (from c in GetAll()
                          where c.CODCOLIGADA == CODCOLIGADA
                          select c);

            foreach (var item in result)
            {
                if (item.VISITA)
                    item.TEMVISITA = "Sim";
                else
                    item.TEMVISITA = "Não";

                PINTERESSE interesse = context.PINTERESSE.Find(item.CODINTERESSE);
                if (interesse != null)
                    item.INTERESSE = interesse.DESCRICAO;

                PSTATUS status = context.PSTATUS.Find(item.CODSTATUS);
                if (status != null)
                    item.STATUS = status.DESCRICAO;

                PSETORES setore = context.PSETORES.Find(item.CODSETOR);
                if (setore != null)
                    item.SETOR = setore.DESCRICAO;

                PCLIENTES clientes = context.PCLIENTES.Find(item.CODCOLIGADA, item.CODCLIENTE);
                if (clientes != null)
                    item.CLIENTE = clientes.NOMEFANTASIA;

                list.Add(item);
            }

            return list;
        }


        public List<PPROPOSTAS> GetList(short CODCOLIGADA, int Ano)
        {
            List<PPROPOSTAS> list = new List<PPROPOSTAS>();
            var result = (from c in GetAll()
                          where c.CODCOLIGADA == CODCOLIGADA &&
                            c.DATAINICIO.Year == Ano
                          select c);

            foreach (var item in result)
            {
                if (item.VISITA)
                    item.TEMVISITA = "Sim";
                else
                    item.TEMVISITA = "Não";

                PINTERESSE interesse = context.PINTERESSE.Find(item.CODINTERESSE);
                if (interesse != null)
                    item.INTERESSE = interesse.DESCRICAO;

                PSTATUS status = context.PSTATUS.Find(item.CODSTATUS);
                if (status != null)
                    item.STATUS = status.DESCRICAO;

                PSETORES setore = context.PSETORES.Find(item.CODSETOR);
                if (setore != null)
                    item.SETOR = setore.DESCRICAO;

                PCLIENTES clientes = context.PCLIENTES.Find(item.CODCOLIGADA, item.CODCLIENTE);
                if (clientes != null)
                    item.CLIENTE = clientes.NOMEFANTASIA;

                list.Add(item);
            }

            return list;
        }

        public List<DashPropostas> GetDashPropostasGeralStatus()
        {
            List<DashPropostas> lista = new List<DashPropostas>();
            var result = from p in GetAll()
                         where p.CODSTATUS != 1 && p.CODSTATUS != 4
                         orderby p.CODSTATUS
                         group p by p.CODSTATUS into p
                         select new
                         {
                             X = p.Key,
                             Y = p.Count()
                         };

            int total = result.Count();
            foreach (var item in result)
            {
                DashPropostas dash = new DashPropostas();
                dash.EixoY = item.Y;

                PSTATUS status = context.PSTATUS.Find(item.X);
                if (status != null)
                    dash.EixoX = status.DESCRICAO;

                lista.Add(dash);
            }

            return lista;
        }

        public List<DashPropostas> GetDashPropostasGeralNoAno()
        {
            List<DashPropostas> lista = new List<DashPropostas>();
            var result = from p in GetAll()
                         where p.DATAINICIO.Year == DateTime.Now.Year &&
                               p.CODSTATUS != 1 && p.CODSTATUS != 4
                         orderby p.DATAINICIO.Month
                         group p by p.DATAINICIO.Month into p
                         select new
                         {
                             X = p.Key,
                             Y = p.Count()
                         };

            int total = result.Count();
            List<int> meses = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            foreach (var item in result)
            {
                DashPropostas dash = new DashPropostas
                {
                    EixoY = item.Y,
                    EixoX = new CultureInfo("pt-BR").DateTimeFormat.GetMonthName(item.X)
                };
                dash.EixoX = char.ToUpper(dash.EixoX[0]) + dash.EixoX.Substring(1);

                lista.Add(dash);
                meses.Remove(item.X);
            }

            foreach (int item in meses)
            {
                DashPropostas dash = new DashPropostas
                {
                    EixoY = 0,
                    EixoX = new CultureInfo("pt-BR").DateTimeFormat.GetMonthName(item)
                };
                dash.EixoX = char.ToUpper(dash.EixoX[0]) + dash.EixoX.Substring(1);

                lista.Add(dash);
            }

            return lista.OrderBy(x =>
                DateTime.ParseExact(x.EixoX, "MMMM", CultureInfo.CurrentCulture).Month).ToList();
        }

        public DataTable GetTablePropostasGerais(int inicio, int fim)
        {
            string sql = $@"SELECT 
                            YEAR(P.DATAENVIO) AS ANO, 
	                            COUNT(*) AS ENVIADAS,
	                            (
		                            SELECT 
			                            COUNT(*) FROM PPROPOSTAS 
		                            WHERE CODSTATUS='3' AND 
			                            YEAR(P.DATAENVIO) BETWEEN {inicio} AND {fim}  AND 
										YEAR(DATAVENCIDO)=YEAR(P.DATAENVIO)
	                            ) AS VENCIDAS 
                            FROM PPROPOSTAS P
                            WHERE CODSTATUS IN (2, 3, 5, 6) AND 
	                            YEAR(P.DATAVENCIDO) BETWEEN {inicio} AND {fim} 
                            GROUP BY YEAR(P.DATAENVIO)
                            ORDER BY YEAR(P.DATAENVIO) DESC";

            return Funcoes.RetornaDataTable(sql);
        }

        public DataTable GetTablePropostasGerais()
        {
            string sql = $@"SELECT 
                            YEAR(P.DATAENVIO) AS ANO, 
	                            COUNT(*) AS ENVIADAS,
	                            (
		                            SELECT 
			                            COUNT(*) FROM PPROPOSTAS 
		                            WHERE CODSTATUS='3'  AND 
										YEAR(DATAVENCIDO)=YEAR(P.DATAENVIO)
	                            ) AS VENCIDAS 
                            FROM PPROPOSTAS P
                            WHERE CODSTATUS IN (2, 3, 5, 6) 
                            GROUP BY YEAR(P.DATAENVIO)
                            ORDER BY YEAR(P.DATAENVIO) DESC";

            return Funcoes.RetornaDataTable(sql);
        }

        public DataTable GetTablePropostasGeraisPorClientes(int inicio, int fim, short? cliente)
        {
            string sql = string.Empty;

            if (cliente != null)
                sql = $@"SELECT 
	                            C.NOMEFANTASIA AS CLIENTE, 
	                            COUNT(*) AS ENVIADAS,
	                            (
		                            SELECT 
			                            COUNT(*) FROM PPROPOSTAS 
		                            WHERE CODSTATUS='3' AND 
			                            CODCLIENTE=P.CODCLIENTE AND 
			                            YEAR(DATAVENCIDO) BETWEEN {inicio} AND {fim}
	                            ) AS VENCIDAS 
                            FROM PPROPOSTAS P
	                            INNER JOIN PCLIENTES C ON (C.CODCOLIGADA = P.CODCOLIGADA AND C.CODCLIENTE = P.CODCLIENTE)
                            WHERE CODSTATUS IN (2, 3, 5, 6) AND 
	                            YEAR(P.DATAVENCIDO) BETWEEN {inicio} AND {fim} AND C.CODCLIENTE='{cliente}'
                            GROUP BY C.NOMEFANTASIA, P.CODCLIENTE 
                            ORDER BY C.NOMEFANTASIA";
            else
                sql = $@"SELECT 
	                            C.NOMEFANTASIA AS CLIENTE, 
	                            COUNT(*) AS ENVIADAS,
	                            (
		                            SELECT 
			                            COUNT(*) FROM PPROPOSTAS 
		                            WHERE CODSTATUS='3' AND 
			                            CODCLIENTE=P.CODCLIENTE AND 
			                            YEAR(DATAVENCIDO) BETWEEN {inicio} AND {fim}
	                            ) AS VENCIDAS 
                            FROM PPROPOSTAS P
	                            INNER JOIN PCLIENTES C ON (C.CODCOLIGADA = P.CODCOLIGADA AND C.CODCLIENTE=P.CODCLIENTE)
                            WHERE CODSTATUS IN (2, 3, 5, 6) AND 
	                            YEAR(P.DATAVENCIDO) BETWEEN {inicio} AND {fim} 
                            GROUP BY C.NOMEFANTASIA, P.CODCLIENTE
                            ORDER BY C.NOMEFANTASIA";

            return Funcoes.RetornaDataTable(sql);
        }

        public DataTable GetTablePropostasGeraisPorClientes()
        {
            string sql = $@"SELECT 
	                            C.NOMEFANTASIA AS CLIENTE, 
	                            COUNT(*) AS ENVIADAS,
	                            (
		                            SELECT 
			                            COUNT(*) FROM PPROPOSTAS 
		                            WHERE CODSTATUS='3' AND CODCLIENTE=P.CODCLIENTE
	                            ) AS VENCIDAS
                            FROM PPROPOSTAS P
	                            INNER JOIN PCLIENTES C ON (C.CODCOLIGADA = P.CODCOLIGADA AND C.CODCLIENTE = P.CODCLIENTE)
                            WHERE CODSTATUS IN (2, 3, 5, 6)  
                            GROUP BY C.NOMEFANTASIA, P.CODCLIENTE 
                            ORDER BY C.NOMEFANTASIA";

            return Funcoes.RetornaDataTable(sql);
        }
    }
}