﻿using QUALIDADE.Dominio;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCRNConformidade : Controle<CRESPCONFORM>
    {
        public bool Exists(string codCCusto, int codColigada, int codNConform, int usrResponsavel)
        {
            var result = from r in GetAll()
                         where r.CODCCUSTO == codCCusto &&
                               r.CODCOLIGADA == codColigada &&
                               r.CODNCONFORM == codNConform &&
                               r.CODRESPONSAVEL == usrResponsavel
                         select r;

            return (result != null && result.Count() > 0);
        }
    }
}
