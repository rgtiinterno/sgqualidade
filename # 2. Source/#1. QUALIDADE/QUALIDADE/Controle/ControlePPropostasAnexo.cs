﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControlePPropostasAnexo : Controle<PPROPANEXO>
    {
        public PPROPANEXO GetAnexo(int codColigada, int codigo, int sequencial)
        {
            string sql = $@"SELECT * FROM PPROPANEXO 
                            WHERE CODCOLIGADA=@CODCOLIGADA 
                              AND CODPROPOSTA=@CODPROPOSTA 
                              AND SEQUENCIAL=@SEQUENCIAL ";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODCOLIGADA", codColigada),
                new SqlParameter("@CODPROPOSTA", codigo),
                new SqlParameter("@SEQUENCIAL", sequencial),
            };

            DataTable dt = Funcoes.RetornaDataTable(sql, parameters);

            PPROPANEXO item = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                new PPROPANEXO
                {
                    CODPROPOSTA = row["CODPROPOSTA"].DBNullToInt().Value,
                    CODCOLIGADA = row["CODCOLIGADA"].DBNullToInt().Value,
                    SEQUENCIAL = row["SEQUENCIAL"].DBNullToInt().Value,
                    EXTENSAO = row["EXTENSAO"].DBNullToString(),
                    ARQUIVO = row["ARQUIVO"].DBNullToBytes(),
                    DESCRICAO = row["DESCRICAO"].DBNullToString(),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };
            }
            return item;
        }


        public List<PPROPANEXO> GetAnexos(int codColigada, int codigo)
        {
            string sql = $@"SELECT * FROM PPROPANEXO 
                            WHERE CODCOLIGADA=@CODCOLIGADA 
                              AND CODPROPOSTA=@CODPROPOSTA ";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODCOLIGADA", codColigada),
                new SqlParameter("@CODPROPOSTA", codigo),
            };

            DataTable dt = Funcoes.RetornaDataTable(sql, parameters);

            List<PPROPANEXO> itens = new List<PPROPANEXO>();
            if(dt != null && dt.Rows.Count > 0 )
            {
                foreach (DataRow row in dt.Rows)
                {
                    itens.Add(new PPROPANEXO
                    {
                        CODPROPOSTA = row["CODPROPOSTA"].DBNullToInt().Value,
                        CODCOLIGADA = row["CODCOLIGADA"].DBNullToInt().Value,
                        SEQUENCIAL = row["SEQUENCIAL"].DBNullToInt().Value,
                        EXTENSAO = row["EXTENSAO"].DBNullToString(), 
                        DESCRICAO = row["DESCRICAO"].DBNullToString(),
                        RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                        RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(), 
                        RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                        RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                    });
                }
            }
            return itens;
        }

        public int GetNewID()
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == FormPrincipal.getUsuarioAcesso().CODCOLIGADA
                            orderby f.SEQUENCIAL descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                Nullable<int> item = usuarios.SEQUENCIAL;
                if (item != null)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }

        public bool Delete(int codColigada, int codigo)
        {
            string sql = $@"DELETE FROM PPROPANEXO WHERE CODCOLIGADA=@CODCOLIGADA AND CODPROPOSTA=@CODPROPOSTA";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODCOLIGADA", codColigada),
                new SqlParameter("@CODPROPOSTA", codigo),
            };

            return Funcoes.ExecutaFuncao(sql, parameters);
        }
    }
}