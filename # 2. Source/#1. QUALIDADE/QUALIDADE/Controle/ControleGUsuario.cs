﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGUsuario : Controle<GUSUARIO>
    {
        public GUSUARIO Login(string login, string senha, ref string msg)
        {
            GUSUARIO usr = null;

            try
            {
                if (string.IsNullOrEmpty(login))
                    msg = "Informe o Usuário.";
                else if (string.IsNullOrEmpty(senha))
                    msg = "Informe a Senha.";
                else
                {
                    var resultado = Get(x =>
                                            x.LOGIN.ToLower() == login.ToLower() &&
                                            x.SENHA == Criptografia.Encrypt(senha)
                                       ).FirstOrDefault();

                    if (resultado == null)
                        msg = "Login ou senha inválidos.";
                    else
                        usr = (GUSUARIO)resultado;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao tentar realizar o login no sistema.", ex);
            }

            return usr;
        }

        public bool DeveAlterarSenha(int codigo)
        {
            bool deveAlterar = false;
            var resultado = Find(codigo);

            if (resultado != null)
                deveAlterar = resultado.ALTERARSENHA;

            return deveAlterar;
        }

        public bool AlteraSenha(int codigo, string senhaAtual, string novaSenha)
        {
            GUSUARIO usuario = Find(codigo);

            if (usuario.SENHA == Criptografia.Encrypt(senhaAtual))
            {
                usuario.SENHA = Criptografia.Encrypt(novaSenha);
                usuario.ALTERARSENHA = false;
                SaveAll();

                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetNewID()
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            orderby f.CODUSUARIO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODUSUARIO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<GUSUARIO> GetList(int codColigada, string cCusto, string sistema)
        {
            List<GUSUARIO> lista = new List<GUSUARIO>();
            var result = (from u in context.GPERMISSAO
                          where u.CODSISTEMA == sistema && u.CODCOLIGADA == codColigada
                          && u.CODCCUSTO == cCusto
                          select u);

            foreach (var item in result)
            {
                if (!lista.Exists(x => x.CODUSUARIO == item.CODUSUARIO))
                    lista.Add(context.GUSUARIO.Find(item.CODUSUARIO));
            }

            return lista;
        }

        public bool Update(GUSUARIO user)
        {
            string sql = @"UPDATE [dbo].[GUSUARIO]
                           SET [NOME] = @NOME
                              ,[LOGIN] = @LOGIN
                              ,[SENHA] = @SENHA
                              ,[EMAIL] = @EMAIL
                              ,[GERENTE] = @GERENTE
                              ,[ALTERARSENHA] = @ALTERARSENHA
                              ,[VERPLANOACAO] = @VERPLANOACAO
                              ,[VEREFICACIAACAO] = @VEREFICACIAACAO
                              ,[RECMODIFIEDBY] = @RECMODIFIEDBY
                              ,[RECMODIFIEDON] = @RECMODIFIEDON
                              ,[DIRETORIA] = @DIRETORIA
                              ,[HOST] = @HOST
                              ,[PORTA] = @PORTA
                              ,[SENHAEMAIL] = @SENHAEMAIL
                              ,[SSLHABILITADO] = @SSLHABILITADO
                         WHERE [CODUSUARIO] = @CODUSUARIO";

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@NOME", user.NOME),
                new SqlParameter("@LOGIN", user.LOGIN),
                new SqlParameter("@SENHA", user.SENHA),
                new SqlParameter("@EMAIL", user.EMAIL),
                new SqlParameter("@GERENTE", user.GERENTE),
                new SqlParameter("@ALTERARSENHA", user.ALTERARSENHA),
                new SqlParameter("@VERPLANOACAO", user.VERPLANOACAO),
                new SqlParameter("@VEREFICACIAACAO", user.VEREFICACIAACAO),
                new SqlParameter("@RECMODIFIEDBY", user.RECMODIFIEDBY),
                new SqlParameter("@RECMODIFIEDON", user.RECMODIFIEDON),
                new SqlParameter("@DIRETORIA", user.DIRETORIA),
                new SqlParameter("@HOST", user.HOST),
                new SqlParameter("@PORTA", user.PORTA),
                new SqlParameter("@SENHAEMAIL", user.SENHAEMAIL),
                new SqlParameter("@SSLHABILITADO", user.SSLHABILITADO),
                new SqlParameter("@CODUSUARIO", user.CODUSUARIO)
            };

            return Funcoes.ExecutaFuncao(sql, parameters);
        }

        public List<CCustoFiltro> GetCCustos(int codColigada, int codUsuario, string codSistema = null)
        {
            List<CCustoFiltro> itens = new List<CCustoFiltro>();
            DataTable dataTable = this.GetDataTable($@"SELECT 
	            P.CODCOLIGADA, P.CODCCUSTO, CC.NOME
            FROM GUSUARIO U 
            INNER JOIN GPERMISSAO P ON(P.CODUSUARIO=U.CODUSUARIO) 
            LEFT JOIN GCENTROCUSTO CC ON(CC.CODCOLIGADA=P.CODCOLIGADA AND CC.CODCCUSTO=P.CODCCUSTO) 
            WHERE P.CODCOLIGADA='{codColigada}' AND P.CODUSUARIO='{codUsuario}' 
              {(string.IsNullOrEmpty(codSistema) ? null : $" AND P.CODSISTEMA='{codSistema}' ")}
            ORDER BY P.CODCOLIGADA, P.CODCCUSTO ");

            if(dataTable != null && dataTable.Rows.Count > 0)
            {
                itens.Add(new CCustoFiltro
                { 
                    CodColigada = codColigada, 
                    CodCCusto = null, 
                    DescCombo = " -- Todos"
                });

                foreach (DataRow row in dataTable.Rows)
                {
                    itens.Add(new CCustoFiltro
                    {
                        CodColigada = row["CODCOLIGADA"].DBNullToSmallint().Value,
                        CodCCusto = row["CODCCUSTO"].DBNullToString(),
                        Nome = row["NOME"].DBNullToString(),
                        DescCombo = $"{row["CODCCUSTO"].DBNullToString()} - {row["NOME"].DBNullToString()}"
                    });
                }
            }
            return itens;
        }
    }
}
