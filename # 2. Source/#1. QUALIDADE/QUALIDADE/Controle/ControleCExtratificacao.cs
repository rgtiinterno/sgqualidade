﻿using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCExtratificacao : Controle<CEXTRATIFICACAO>
    {
        public int GetID(short codColigada)
        {
            int newId = 0;

            var cExtratificacao = (from f in Get(x => x.CODCOLIGADA == codColigada)
                                   orderby f.CODEXTRATIF descending
                                   select f).FirstOrDefault();

            if (cExtratificacao != null)
            {
                Nullable<int> item = cExtratificacao.CODEXTRATIF;
                if (item != null)
                {
                    newId = Convert.ToInt32(item.Value);
                }
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<CEXTRATIFICACAO> GetList(short CODCOLIGADA)
        {
            List<CEXTRATIFICACAO> lista = new List<CEXTRATIFICACAO>();
            var result = from c in GetAll()
                         where c.CODCOLIGADA == CODCOLIGADA
                         select c;

            foreach (var item in result)
            {
                GSETORES setores = item.CODSETOR.HasValue ? context.GSETORES.Find(item.CODSETOR, item.CODCOLIGADA) : null;
                if (setores != null)
                    item.DESSETOR = setores.DESCRICAO;

                lista.Add(item);
            }

            return lista;
        }
    }
}
