﻿using QUALIDADE.Dominio;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleFTPArquivo : Controle<FTPARQUIVO>
    {
        public short GetID()
        {
            int newId = 0;

            var pais = (from f in GetAll()
                        orderby f.CODIGO descending
                        select f).FirstOrDefault();

            if (pais != null)
            {
                Nullable<int> item = pais.CODIGO;
                if (item != null)
                {
                    newId = Convert.ToInt16(item.Value);
                }
            }
            return Convert.ToInt16((newId + 1));
        }
    }
}
