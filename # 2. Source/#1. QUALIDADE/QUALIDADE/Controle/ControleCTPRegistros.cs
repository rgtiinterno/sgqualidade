﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCTPRegistros : Controle<CTPREGISTRO>
    {
        public int GetNewID()
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            orderby f.CODREGISTRO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODREGISTRO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }
    }
}