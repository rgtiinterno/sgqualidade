﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleAAfericaoInterna : Controle<AAFERICAOINTERNA>
    {
        public int GetNewID(short codColigada)
        {
            int newId = 0;

            var result = (from r in context.AAFERICAOINTERNA
                          where r.CODCOLIGADA == codColigada
                          select r).OrderByDescending(x => x.CODIGO).FirstOrDefault();

            if (result != null)
            {
                int? item = result?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }

        public List<AAFERICAOINTERNA> GetList(short codColigada)
        {
            List<AAFERICAOINTERNA> lista = new List<AAFERICAOINTERNA>();
            List<AAFERICAOINTERNA> projetos = GetAll().ToList();
            List<CCustoFiltro> permissoes = FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("A");

            var result = from p in projetos 
                         join perm in permissoes on
                         new { CodColigada = p.CODCOLIGADA, CodCCusto = p.CODCCUSTO }
                         equals new { perm.CodColigada, perm.CodCCusto }
                         where p.CODCOLIGADA == codColigada &&
                               p.REV == (
                                    from j in projetos
                                    where j.CODCOLIGADA == codColigada &&
                                          j.CODIGO == p.CODIGO 
                                    select j.REV
                               ).Max()
                         select p;

            foreach (var item in result)
            {
                if (File.Exists($"{Directory.GetCurrentDirectory()}\\{Properties.Settings.Default.conexaoIntegracao}"))
                {
                    DataTable dt = Funcoes.RetornaDataTableIntegracao(
                        $@"SELECT * FROM FCFO WHERE CODCFO='{item.CODCFO}' AND CODCOLIGADA='0' ");

                    if (dt != null && dt.Rows.Count > 0)
                        item.FORNECEDOR = dt.Rows[0]["NOMEFANTASIA"].DBNullToString();
                    else
                    {
                        FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO) ?? context.FCFO.Find(0, item.CODCFO);
                        if (fFornecedor != null)
                            item.FORNECEDOR = fFornecedor.NOMEFANTASIA;
                    }
                }
                else
                {
                    FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO) ?? context.FCFO.Find(0, item.CODCFO);
                    if (fFornecedor != null)
                        item.FORNECEDOR = fFornecedor.NOMEFANTASIA;
                }

                if (item.CODCOLCCUSTO.HasValue && !string.IsNullOrEmpty(item.CODCCUSTO))
                {
                    var centroCusto = context.GCENTROCUSTO.Find(item.CODCOLCCUSTO.Value, item.CODCCUSTO);
                    item.CENTROCUSTO = centroCusto?.NOME;
                    item.NUMCONTRATO = centroCusto?.NUMCONTRATO;
                }
                lista.Add(item);
            }
            return lista;
        }

        public List<AAFERICAOINT068LEITURA> GetLeituras068Esquadro(int codAfericao, int codColigada, string codCfo, int rev)
        {
            return context.AAFERICAOINT068LEITURA?.Where(x =>
                x.CODAFERICAO == codAfericao &&
                x.CODCOLIGADA == codColigada &&
                x.CODCFO == codCfo &&
                x.REV == rev
            )?.ToList() ?? new List<AAFERICAOINT068LEITURA>();
        }

        public List<AAFERICAOINT070LEITURA> GetLeituras070Trena(int codAfericao, int codColigada, string codCfo, int rev)
        {
            return context.AAFERICAOINT070LEITURA?.Where(x =>
                x.CODAFERICAO == codAfericao &&
                x.CODCOLIGADA == codColigada &&
                x.CODCFO == codCfo &&
                x.REV == rev
            )?.ToList() ?? new List<AAFERICAOINT070LEITURA>();
        }

        public List<AAFERICAOINTERNA> GetListHistorico(int codColigada, int codigo)
        {
            List<AAFERICAOINTERNA> lista = new List<AAFERICAOINTERNA>();
            var result = from p in GetAll()
                         where p.CODCOLIGADA == codColigada &&
                               p.CODIGO == codigo
                         orderby p.REV descending
                         select p;

            foreach (var item in result)
            {
                if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
                {
                    DataTable dt = Funcoes.RetornaDataTableIntegracao(
                        $@"SELECT * FROM FCFO WHERE CODCFO='{item.CODCFO}' AND CODCOLIGADA='0'"
                    );

                    if (dt != null && dt.Rows.Count > 0)
                        item.FORNECEDOR = dt.Rows[0]["NOMEFANTASIA"].DBNullToString();
                    else
                    {
                        FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO);
                        if (fFornecedor != null)
                            item.FORNECEDOR = fFornecedor.NOME;
                    }
                }
                else
                {
                    FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO);
                    if (fFornecedor != null)
                        item.FORNECEDOR = fFornecedor.NOME;
                }

                if (item.CODCOLCCUSTO.HasValue && !string.IsNullOrEmpty(item.CODCCUSTO))
                    item.CENTROCUSTO = context.GCENTROCUSTO.Find(item.CODCOLCCUSTO.Value, item.CODCCUSTO)?.NOME;

                lista.Add(item);
            }

            return lista;
        }
    }
}