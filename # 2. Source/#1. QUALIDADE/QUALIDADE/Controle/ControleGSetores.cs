﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGSetores : Controle<GSETORES>
    {
        public int GetNewID()
        {
            int newId = 0;
            int CODCOLIGADA = FormPrincipal.getUsuarioAcesso().CODCOLIGADA;

            var usuarios = (from f in GetAll()
                            where f.CODCOLIGADA == CODCOLIGADA
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt16((newId + 1));
        }
    }
}