﻿using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleFArquivoAnx : Controle<FARQFCFO>
    {
        public bool DeletaAnexo(int codColigada, string codCfo, int codTpArquivo)
        {
            return ExecuteSQL(
                @"DELETE FROM FARQFCFO 
                WHERE CODCOLIGADA=@CODCOLIGADA 
                AND CODCFO=@CODCFO 
                AND CODTPARQUIVO=@CODTPARQUIVO ", 
                new List<SqlParameter>
                {
                    new SqlParameter("@CODCOLIGADA", codColigada), 
                    new SqlParameter("@CODCFO", codCfo), 
                    new SqlParameter("@CODTPARQUIVO", codTpArquivo),
                }
            );
        }

        public List<FARQFCFO> GetList(int codColigada, string CodForn)
        {
            List<FARQFCFO> list = new List<FARQFCFO>();
            var result = (from x in GetAll()
                          where x.CODCFO == CodForn && x.CODCOLIGADA == codColigada
                          select x);

            if (result != null)
            {
                foreach (var item in result)
                {
                    FTPARQUIVO arquivo = context.FTPARQUIVO.Find(item.CODTPARQUIVO);
                    if (arquivo != null)
                        item.TPARQUIVO = arquivo.NOME;

                    list.Add(item);
                }
            }

            return list;
        }
    }
}
