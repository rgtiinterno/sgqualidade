﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using QUALIDADE.Forms;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCVPAcaoEficacia : Controle<CVPACAOEFICACIA>
    {
        public List<CVPACAOEFICACIA> GetList(short CODCOLIGADA, string nomeForm)
        {
            List<CVPACAOEFICACIA> list = new List<CVPACAOEFICACIA>();
            var result = (from c in context.CVPACAOEFICACIA
                          where c.CODCOLIGADA == CODCOLIGADA
                          select c);

            foreach (var item in result)
            {
                GPERMISSAO permissao =
                    context.GPERMISSAO.Find(CODCOLIGADA, FormPrincipal.getUsuarioAcesso().CODUSUARIO,
                                                item.CODCCUSTO, "C"
                                            );

                if (permissao != null)
                {
                    GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(CODCOLIGADA, item.CODCCUSTO);
                    item.DESCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                    GUSUARIO usuarioPA = context.GUSUARIO.Find(item.CODUSUVERIFPACAO);
                    GUSUARIO usuarioEF = context.GUSUARIO.Find(item.CODUSUVERIFEFIC);

                    if (usuarioPA != null)
                        item.NOMEUSUVERIFPACAO = usuarioPA.NOME;

                    if (usuarioEF != null)
                        item.NOMEUSUVERIFEFIC = usuarioEF.NOME;

                    list.Add(item);
                }
            }

            return list;
        }

        public List<SubRelNConformidade> GetRelatorio(CNCONFORM nConform)
        {
            List<SubRelNConformidade> it = new List<SubRelNConformidade>();
            SubRelNConformidade rel = new SubRelNConformidade();
            var result = (from c in context.CVPACAOEFICACIA
                          where c.CODCOLIGADA == nConform.CODCOLIGADA &&
                            c.CODCCUSTO == nConform.CODCCUSTO && c.CODNCONFORM == nConform.CODNCONFORM
                          select c);

            foreach (var item in result)
            {
                rel = new SubRelNConformidade();
                rel.CODNCONFORM = item.CODNCONFORM;
                rel.CODCOLIGADA = item.CODCOLIGADA;
                rel.CODCCUSTO = item.CODCCUSTO;
                rel.DATAVERIFPACAO = item.DATAVERIFPACAO;
                rel.CODUSUVERIFPACAO = item.CODUSUVERIFPACAO;
                rel.DTLIMITEVEFICACIA = item.DTLIMITEVEFICACIA;
                rel.NUMSACP = item.NUMSACP;
                rel.CODUSUVERIFEFIC = item.CODUSUVERIFEFIC;
                rel.DTVEFICACIACIA = item.DTVEFICACIACIA;
                rel.RECCREATEDBY = item.RECCREATEDBY;
                rel.RECCREATEDON = item.RECCREATEDON;
                rel.RECMODIFIEDBY = item.RECMODIFIEDBY;
                rel.RECMODIFIEDON = item.RECMODIFIEDON;

                GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(nConform.CODCOLIGADA, item.CODCCUSTO);
                rel.DESCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                GUSUARIO usuarioPA = context.GUSUARIO.Find(item.CODUSUVERIFPACAO);
                GUSUARIO usuarioEF = context.GUSUARIO.Find(item.CODUSUVERIFEFIC);

                if (usuarioPA != null)
                    rel.NOMEUSUVERIFPACAO = usuarioPA.NOME;

                if (usuarioEF != null)
                    rel.NOMEUSUVERIFEFIC = usuarioEF.NOME;

                if (!item.CUMPRIDO.HasValue)
                {
                    rel.OBSERVACAOPACAO = "PLANO AINDA NÃO VERIFICADO";
                    rel.CUMPRIDO = false;
                }
                else
                {
                    rel.OBSERVACAOPACAO = item.OBSERVACAOPACAO;
                    rel.CUMPRIDO = item.CUMPRIDO.Value;
                }

                if (!item.ACAOEFICAZ.HasValue)
                {
                    rel.OBSERVACAOEFIC = "EFICIENCIA DA AÇÃO AINDA NÃO VERIFICADA";
                    rel.ACAOEFICAZ = false;
                }
                else
                {
                    rel.OBSERVACAOEFIC = item.OBSERVACAOEFIC;
                    rel.ACAOEFICAZ = item.ACAOEFICAZ.Value;
                }

                it.Add(rel);
            }

            return it;
        }

        public bool updatePlanoAcao(CVPACAOEFICACIA item)
        {
            string sqlQuery = @"UPDATE CVPACAOEFICACIA SET CUMPRIDO=@CUMPRIDO,
            DATAVERIFPACAO=@DATAVERIFPACAO, OBSERVACAOPACAO=@OBSERVACAOPACAO, CODUSUVERIFPACAO=@CODUSUVERIFPACAO, 
            DTLIMITEVEFICACIA=@DTLIMITEVEFICACIA WHERE CODCOLIGADA=@CODCOLIGADA AND CODCCUSTO=@CODCCUSTO 
                AND CODNCONFORM=@CODNCONFORM";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CUMPRIDO", item.CUMPRIDO.Value? 1 : 0),
                new SqlParameter("@DATAVERIFPACAO", item.DATAVERIFPACAO.Value),
                new SqlParameter("@OBSERVACAOPACAO", item.OBSERVACAOPACAO),
                new SqlParameter("@CODUSUVERIFPACAO", item.CODUSUVERIFPACAO),
                new SqlParameter("@DTLIMITEVEFICACIA", item.DTLIMITEVEFICACIA.Value),
                new SqlParameter("@CODCOLIGADA", item.CODCOLIGADA),
                new SqlParameter("@CODCCUSTO", item.CODCCUSTO),
                new SqlParameter("@CODNCONFORM", item.CODNCONFORM)
            };

            return Funcoes.ExecutaFuncao(sqlQuery, parameters);
        }
    }
}
