﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleJProjetos : Controle<JPROJETOS>
    {
        public List<JPROJETOS> GetList(short codColigada, string codCCusto = null)
        {
            List<JPROJETOS> lista = new List<JPROJETOS>();
            List<JPROJETOS> projetos = GetAll().ToList();

            var result = from p in projetos
                         where p.CODCOLIGADA == codColigada &&
                               p.REVISAO == (
                                    from j in projetos
                                    where j.CODCOLIGADA == codColigada &&
                                          j.CODIGO == p.CODIGO && j.CODCCUSTO == p.CODCCUSTO
                                    select j.REVISAO
                               ).Max()
                         select p;


            if (!string.IsNullOrEmpty(codCCusto))
                result = result.Where(x => x.CODCCUSTO.Trim() == codCCusto).ToList();

            foreach (var item in result)
            {
                GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                if (cCusto != null)
                    item.CENTROCUSTO = cCusto.NOME;

                string strDist = string.Empty;
                var itensDist = from d in context.JSETORESDISTRIBUICAO.Include("JSETORES") 
                                where d.CODPROJETO == item.CODIGO &&
                                      d.REVISAO == item.REVISAO 
                                select d;
                if(itensDist != null && itensDist.Count() > 0)
                {
                    foreach(var itemDist in itensDist)
                    {
                        strDist += $"{(string.IsNullOrEmpty(strDist) ? "" : "\r\n")}Setor: {itemDist.JSETORES.DESCRICAO} | Nome/Função: {itemDist.DESCRICAO} | Cópia Digital: {(itemDist.COPIADIGITAL ? "sim" : "não")} | Cópia Física: {(itemDist.COPIAFISICA ? "sim" : "não")}";
                    }
                }
                item.DISTRIBUICAO = strDist;

                lista.Add(item);
            }

            return lista;
        }

        public List<JPROJETOS> GetListPermissao(short codColigada, List<CCustoFiltro> listaCCustos)
        {
            List<JPROJETOS> lista = new List<JPROJETOS>();
            List<JPROJETOS> projetos = GetAll().ToList();

            var result = from p in projetos
                         where p.CODCOLIGADA == codColigada &&
                               p.REVISAO == (
                                    from j in projetos
                                    where j.CODCOLIGADA == codColigada &&
                                          j.CODIGO == p.CODIGO && j.CODCCUSTO == p.CODCCUSTO
                                    select j.REVISAO
                               ).Max()
                         select p;


            if (listaCCustos.Count() > 0)
            {
                List<string> ccustos = new List<string>();
                foreach (var cc in listaCCustos)
                {
                    ccustos.Add(cc.CodCCusto);
                }
                result = result.Where(x => ccustos.Contains(x.CODCCUSTO.Trim())).ToList();


                foreach (var item in result)
                {
                    GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                    if (cCusto != null)
                        item.CENTROCUSTO = cCusto.NOME;

                    string strDist = string.Empty;
                    var itensDist = from d in context.JSETORESDISTRIBUICAO.Include("JSETORES")
                                    where d.CODPROJETO == item.CODIGO &&
                                          d.REVISAO == item.REVISAO
                                    select d;
                    if (itensDist != null && itensDist.Count() > 0)
                    {
                        foreach (var itemDist in itensDist)
                        {
                            strDist += $"{(string.IsNullOrEmpty(strDist) ? "" : "\r\n")}Setor: {itemDist.JSETORES.DESCRICAO} | Nome/Função: {itemDist.DESCRICAO} | Cópia Digital: {(itemDist.COPIADIGITAL ? "sim" : "não")} | Cópia Física: {(itemDist.COPIAFISICA ? "sim" : "não")}";
                        }
                    }
                    item.DISTRIBUICAO = strDist;

                    lista.Add(item);
                }
            }
            return lista;
        }
        public List<JPROJETOS> GetListHistorico(int codColigada, int codigo, string centoCusto)
        {
            List<JPROJETOS> lista = new List<JPROJETOS>();
            var result = from p in GetAll()
                         where p.CODCOLIGADA == codColigada &&
                               p.CODIGO == codigo && p.CODCCUSTO == centoCusto
                         orderby p.REVISAO descending
                         select p;

            foreach (var item in result)
            {
                GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                if (cCusto != null)
                    item.CENTROCUSTO = cCusto.NOME;

                string strDist = string.Empty;
                var itensDist = from d in context.JSETORESDISTRIBUICAO.Include("JSETORES")
                                where d.CODPROJETO == item.CODIGO &&
                                      d.REVISAO == item.REVISAO
                                select d;
                if (itensDist != null && itensDist.Count() > 0)
                {
                    foreach (var itemDist in itensDist)
                    {
                        strDist += $"{(string.IsNullOrEmpty(strDist) ? "" : "\r\n")}Setor: {itemDist.JSETORES.DESCRICAO} | Nome/Função: {itemDist.DESCRICAO} | Cópia Digital: {(itemDist.COPIADIGITAL ? "sim" : "não")} | Cópia Física: {(itemDist.COPIAFISICA ? "sim" : "não")}";
                    }
                }
                item.DISTRIBUICAO = strDist;

                lista.Add(item);
            }

            return lista;
        }

        public int GetNewID()
        {
            int newId = 0;

            var usuarios = (from f in GetAll()
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }

        public bool Update(JPROJETOS item, List<JSETORESDISTRIBUICAO> list, List<JANALISE> listaAnalise)
        {
            using (DbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    String query = "UPDATE JPROJETOS SET " +
                        "CODIGO = @CODIGO, " +
                        "REVISAO = @REVISAO, " +
                        "CODCCUSTO = @CODCCUSTO, " +
                        "NUMPROJETO = @NUMPROJETO, " +
                        "DESCRICAO = @DESCRICAO, " +
                        "TIPO = @TIPO, " +
                        "LOCAL = @LOCAL, " +
                        "POSSUIANALISECRITICA = @POSSUIANALISECRITICA, " +
                        "FUNCAO = @FUNCAO, " +
                        "RESPONSAVEL = @RESPONSAVEL, " +
                        "ANALISERISCO = @ANALISERISCO, " +
                        "POSSUIANALISERISCO = @POSSUIANALISERISCO, " +
                        "DTANALISECRITICA = @DTANALISECRITICA, " +
                        "DTENTREGACLIENTE = @DTENTREGACLIENTE, " +
                        "RECMODIFIEDBY = @RECMODIFIEDBY," +
                        "RECMODIFIEDON = @RECMODIFIEDON " +
                        "WHERE CODIGO = @CODIGO AND REVISAO = @REVISAO";

                    List<SqlParameter> parameters = new List<SqlParameter>
                    {
                         new SqlParameter("@CODIGO", item.CODIGO),
                         new SqlParameter("@REVISAO", item.REVISAO),
                         new SqlParameter("@CODCCUSTO", item.CODCCUSTO),
                         new SqlParameter("@NUMPROJETO", item.NUMPROJETO),
                         new SqlParameter("@DESCRICAO", item.DESCRICAO),
                         new SqlParameter("@TIPO", item.TIPO),
                         new SqlParameter("@LOCAL", item.LOCAL),
                         new SqlParameter("@POSSUIANALISECRITICA", item.POSSUIANALISECRITICA),
                         new SqlParameter("@RESPONSAVEL", item.RESPONSAVEL),
                         new SqlParameter("@ANALISERISCO", item.ANALISERISCO),
                         new SqlParameter("@DTANALISECRITICA", item.DTANALISECRITICA),
                         new SqlParameter("@DTENTREGACLIENTE", item.DTENTREGACLIENTE),
                         new SqlParameter("@FUNCAO", item.FUNCAO),
                         new SqlParameter("@POSSUIANALISERISCO", item.POSSUIANALISERISCO),
                         new SqlParameter("@RECMODIFIEDBY", FormPrincipal.getUsuarioAcesso().LOGIN),
                         new SqlParameter("@RECMODIFIEDON", DateTime.Now),
                    };
                    bool salvo = Funcoes.ExecutaFuncao(query, parameters);

                    if (salvo)
                    {
                        query = "Delete from JSETORESDISTRIBUICAO " +
                            "WHERE CODPROJETO = @CODPROJETO AND REVISAO = @REVISAO";

                        List<SqlParameter> parametersList = new List<SqlParameter>
                        {
                            new SqlParameter("@CODPROJETO", item.CODIGO),
                            new SqlParameter("@REVISAO", item.REVISAO),
                        };

                        salvo = Funcoes.ExecutaFuncao(query, parametersList);

                        query = "Delete from JANALISE " +
                            "WHERE CODPROJETO = @CODPROJETO AND REVISAO = @REVISAO";

                        List<SqlParameter> parametersListAnalise = new List<SqlParameter>
                        {
                            new SqlParameter("@CODPROJETO", item.CODIGO),
                            new SqlParameter("@REVISAO", item.REVISAO),
                        };

                        salvo = Funcoes.ExecutaFuncao(query, parametersListAnalise);

                        if (salvo)
                        {
                            foreach (JSETORESDISTRIBUICAO setor in list)
                            {
                                setor.JSETORES = null;
                                setor.REVISAO = item.REVISAO;
                                context.JSETORESDISTRIBUICAO.Add(setor);
                            }
                            context.SaveChanges();

                            foreach (JANALISE analise in listaAnalise)
                            {
                                analise.JPERGUNTAS = null;
                                analise.JPROJETOS = null;
                                analise.REVISAO = item.REVISAO;
                                context.JANALISE.Add(analise);
                            }
                            context.SaveChanges();

                            transaction.Commit();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    FormMsg frm = new FormMsg(ex);
                    frm.WindowState = System.Windows.Forms.FormWindowState.Normal;
                    frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                    frm.ShowDialog();

                    return false;
                }
            }

        }


        public bool GravaProjeto(JPROJETOS item, List<JSETORESDISTRIBUICAO> list, List<JANALISE> listaAnalise)
        {
            using (DbContextTransaction transaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.JPROJETOS.Add(item);
                    context.SaveChanges();

                    foreach (JSETORESDISTRIBUICAO setor in list)
                    {
                        setor.JSETORES = null;
                        setor.REVISAO = item.REVISAO;
                        context.JSETORESDISTRIBUICAO.Add(setor);
                    }
                    context.SaveChanges();

                    foreach (JANALISE analise in listaAnalise)
                    {
                        analise.JPERGUNTAS = null;
                        analise.JPROJETOS = null;
                        analise.REVISAO = item.REVISAO;
                        context.JANALISE.Add(analise);
                    }
                    context.SaveChanges();

                    transaction.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    FormMsg frm = new FormMsg(ex);
                    frm.WindowState = System.Windows.Forms.FormWindowState.Normal;
                    frm.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
                    frm.ShowDialog();

                    return false;
                }
            }
        }

    }
}