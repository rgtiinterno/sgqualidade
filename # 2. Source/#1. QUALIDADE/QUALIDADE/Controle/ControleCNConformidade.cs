﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Dominio.Relatorios;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleCNConformidade : Controle<CNCONFORM>
    {
        public bool AtualizaStatus(CNCONFORM conform)
        {
            string sqlQuery = @"UPDATE CNCONFORM SET STATUS=@STATUS, FINALIZADORESPONSAVEL=@FINALIZADORESPONSAVEL, 
            RECMODIFIEDBY=@RECMODIFIEDBY, RECMODIFIEDON=@RECMODIFIEDON 
            WHERE CODCOLIGADA=@CODCOLIGADA AND CODCCUSTO=@CODCCUSTO AND CODNCONFORM=@CODNCONFORM";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@STATUS", "C"),
                new SqlParameter("@RECMODIFIEDBY", FormPrincipal.getUsuarioAcesso().LOGIN),
                new SqlParameter("@RECMODIFIEDON", DateTime.Now),
                new SqlParameter("@FINALIZADORESPONSAVEL", 1),
                new SqlParameter("@CODCOLIGADA", conform.CODCOLIGADA),
                new SqlParameter("@CODCCUSTO", conform.CODCCUSTO),
                new SqlParameter("@CODNCONFORM", conform.CODNCONFORM),
            };

            return Funcoes.ExecutaFuncao(sqlQuery, parameters);
        }

        public int GetID(int codColigada)
        {
            int newId = 0;
            if(context.CNCONFORM.Any())
                newId = context.CNCONFORM.Where(x => x.CODCOLIGADA == codColigada)?.Max(x => x.CODNCONFORM) ?? 0;
            return (newId + 1);
        }

        public List<CNCONFORM> GetList(GUSUARIO usr)
        {
            List<CNCONFORM> list = new List<CNCONFORM>();
            var result = (from c in context.CNCONFORM
                          where c.CODCOLIGADA == usr.CODCOLIGADA
                          select c);

            if (!usr.GERENTE)
            {
                foreach (var item in result)
                {
                    GPERMISSAO permissao = context.GPERMISSAO.Find(item.CODCOLIGADA, usr.CODUSUARIO, item.CODCCUSTO, "C");
                    if (permissao != null)
                    {
                        GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                        item.DESCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                        CEXTRATIFICACAO extrat = context.CEXTRATIFICACAO.Find(item.CODCOLIGADA, item.CODEXTRATIF);
                        if (extrat != null)
                            item.DESCEXTRATIF = $"{item.CODEXTRATIF} - {extrat.DESCRICAO}";

                        CTPCLASSIFICACAO classif = context.CTPCLASSIFICACAO.Find(item.CODCLASSIF);
                        if (classif != null)
                            item.DESCCLASSIF = $"{item.CODCLASSIF} - {classif.DESCRICAO}";

                        CTPSITUACAO situacao = context.CTPSITUACAO.Find(item.STATUS);
                        if (situacao != null)
                            item.DESCSTATUS = $"{situacao.DESCRICAO}";

                        if (item.TPACAO == "C")
                            item.TPACAO = "Corretiva";
                        else
                            item.TPACAO = "Melhoria";

                        GUSUARIO usuario = context.GUSUARIO.Find(item.CODRELATOR);
                        if (usuario != null)
                            item.NOMERALATOR = $"{usuario.NOME}";

                        GUSUARIO usuarioEx = context.GUSUARIO.Find(item.CODEXECUTOR);
                        if (usuarioEx != null)
                            item.DESCEXECUTOR = $"{usuarioEx.NOME}";

                        CVPACAOEFICACIA eficacia = context.CVPACAOEFICACIA.Find(item.CODCOLIGADA, item.CODNCONFORM, item.CODCCUSTO);
                        if (eficacia != null)
                        {
                            item.VERIFICACAOPLANOACAO = (eficacia.CUMPRIDO.HasValue);
                            item.VERIFICACAOPLANOEFICACIA = (eficacia.ACAOEFICAZ.HasValue);
                        }

                        CTPREGISTRO registro = context.CTPREGISTRO.Find(item.CODREGISTRO);
                        if (registro != null)
                            item.DESCREGISTRO = registro.DESCRICAO;

                        GSETORES setor = context.GSETORES.Find(
                            (item.CODSETOR > 0 ? item.CODSETOR : context.CEXTRATIFICACAO.Find(item.CODEXTRATIF, item.CODCOLIGADA).CODSETOR), 
                            item.CODCOLIGADA
                        );
                        if (setor != null)
                            item.DESCSETOR = setor.DESCRICAO;

                        list.Add(item);
                    }
                }
            }
            else
            {
                foreach (var item in result)
                {
                    GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                    item.DESCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                    CEXTRATIFICACAO extrat = context.CEXTRATIFICACAO.Find(item.CODCOLIGADA, item.CODEXTRATIF);
                    if (extrat != null)
                        item.DESCEXTRATIF = $"{item.CODEXTRATIF} - {extrat.DESCRICAO}";

                    CTPCLASSIFICACAO classif = context.CTPCLASSIFICACAO.Find(item.CODCLASSIF);
                    if (classif != null)
                        item.DESCCLASSIF = $"{item.CODCLASSIF} - {classif.DESCRICAO}";

                    CTPSITUACAO situacao = context.CTPSITUACAO.Find(item.STATUS);
                    if (situacao != null)
                        item.DESCSTATUS = $"{situacao.DESCRICAO}";

                    if (item.TPACAO == "C")
                        item.TPACAO = "Corretiva";
                    else
                        item.TPACAO = "Melhoria";

                    GUSUARIO usuario = context.GUSUARIO.Find(item.CODRELATOR);
                    if (usuario != null)
                        item.NOMERALATOR = $"{usuario.NOME}";

                    GUSUARIO usuarioEx = context.GUSUARIO.Find(item.CODEXECUTOR);
                    if (usuarioEx != null)
                        item.DESCEXECUTOR = $"{usuarioEx.NOME}";

                    CVPACAOEFICACIA eficacia = context.CVPACAOEFICACIA.Find(item.CODCOLIGADA, item.CODNCONFORM, item.CODCCUSTO);
                    if (eficacia != null)
                    {
                        item.VERIFICACAOPLANOACAO = (eficacia.CUMPRIDO.HasValue);
                        item.VERIFICACAOPLANOEFICACIA = (eficacia.ACAOEFICAZ.HasValue);
                    }

                    CTPREGISTRO registro = context.CTPREGISTRO.Find(item.CODREGISTRO);
                    if (registro != null)
                        item.DESCREGISTRO = registro.DESCRICAO;

                    GSETORES setor = context.GSETORES.Find(
                        (item.CODSETOR > 0 ? item.CODSETOR : context.CEXTRATIFICACAO.Find(item.CODEXTRATIF, item.CODCOLIGADA).CODSETOR),
                        item.CODCOLIGADA
                    );
                    if (setor != null)
                        item.DESCSETOR = setor.DESCRICAO;

                    list.Add(item);
                }
            }

            return list;
        }

        public RelNConformidade GetRelatorio(CNCONFORM nConform)
        {
            RelNConformidade rel = new RelNConformidade();
            var result = (from c in GetAll()
                          where c.CODCOLIGADA == nConform.CODCOLIGADA &&
                            c.CODNCONFORM == nConform.CODNCONFORM &&
                            c.CODCCUSTO == nConform.CODCCUSTO
                          select c);

            foreach (var item in result)
            {
                rel.CODNCONFORM = item.CODNCONFORM;
                rel.DATAABERTURANC = item.DATAABERTURANC;
                rel.NCONFREALPONTENCIAL = item.NCONFREALPONTENCIAL;
                rel.ACAOIMEDTOMADA = item.ACAOIMEDTOMADA;
                rel.EVDOBJETIVA = item.EVDOBJETIVA;
                rel.NECACAOCORPREV = item.NECACAOCORPREV;
                rel.PLANOACAOQUE = item.PLANOACAOQUE;
                rel.PLANOACAOQUEM = item.PLANOACAOQUEM;
                rel.PLANOACAOQUANDO = item.PLANOACAOQUANDO;
                rel.EVDTRATATIVA = item.EVDTRATATIVA;
                rel.ANLISECAUSA1 = item.ANLISECAUSA1;
                rel.ANLISECAUSA2 = item.ANLISECAUSA2;
                rel.ANLISECAUSA3 = item.ANLISECAUSA3;
                rel.VERIFICACAOPLANOACAO = item.VERIFICACAOPLANOACAO;
                rel.VERIFICACAOPLANOEFICACIA = item.VERIFICACAOPLANOEFICACIA;
                rel.FINALIZADORELATOR = item.FINALIZADORELATOR;
                rel.FINALIZADORESPONSAVEL = item.FINALIZADORESPONSAVEL;
                rel.RECCREATEDBY = item.RECCREATEDBY;
                rel.RECCREATEDON = item.RECCREATEDON;
                rel.RECMODIFIEDBY = item.RECMODIFIEDBY;
                rel.RECMODIFIEDON = item.RECMODIFIEDON;

                GCENTROCUSTO cCusto = context.GCENTROCUSTO.Find(item.CODCOLIGADA, item.CODCCUSTO);
                rel.CODCCUSTO = $"{item.CODCCUSTO} - {cCusto.NOME}";

                CEXTRATIFICACAO extrat = context.CEXTRATIFICACAO.Find(item.CODCOLIGADA, item.CODEXTRATIF);
                if (extrat != null)
                    rel.DESCEXTRATIF = $"{item.CODEXTRATIF} - {extrat.DESCRICAO}";

                CTPCLASSIFICACAO classif = context.CTPCLASSIFICACAO.Find(item.CODCLASSIF);
                if (classif != null)
                    rel.DESCCLASSIF = $"{item.CODCLASSIF} - {classif.DESCRICAO}";

                CTPSITUACAO situacao = context.CTPSITUACAO.Find(item.STATUS);
                if (situacao != null)
                    rel.DESCSTATUS = $"{situacao.DESCRICAO}";

                if (item.TPACAO == "C")
                    rel.TPACAO = "Corretiva";
                else
                    rel.TPACAO = "Melhoria";

                GUSUARIO usuario = context.GUSUARIO.Find(item.CODRELATOR);
                if (usuario != null)
                    rel.NOMERALATOR = $"{usuario.NOME}";

                GUSUARIO usuarioEx = context.GUSUARIO.Find(item.CODEXECUTOR);
                if (usuarioEx != null)
                    rel.DESCEXECUTOR = $"{usuarioEx.NOME}";

                GEMPRESAS empresa = context.GEMPRESAS.Find(item.CODCOLIGADA);
                if (empresa != null)
                {
                    rel.CODCOLIGADA = empresa.CODCOLIGADA;
                    rel.LOGOEMPRESA = empresa.LOGO;
                    rel.CGCEMPRESA = empresa.CGC;
                    rel.NOMEEMPRESA = empresa.NOMEFANTASIA;
                }
            }

            return rel;
        }

        public bool UpdatePlanoAcao(int CODCOLIGADA, string CODCCUSTO, int CODNCONFORM, bool VERIFICACAOPLANOACAO)
        {
            string sqlQuery = @"UPDATE CNCONFORM SET STATUS=@STATUS, VERIFICACAOPLANOACAO=@VERIFICACAOPLANOACAO, 
            RECMODIFIEDBY=@RECMODIFIEDBY, RECMODIFIEDON=@RECMODIFIEDON, FINALIZADORELATOR=@FINALIZADORELATOR 
            WHERE CODCOLIGADA=@CODCOLIGADA AND CODCCUSTO=@CODCCUSTO AND CODNCONFORM=@CODNCONFORM";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@STATUS", "F"),
                new SqlParameter("@VERIFICACAOPLANOACAO", VERIFICACAOPLANOACAO? 1 : 0),
                new SqlParameter("@RECMODIFIEDBY", FormPrincipal.getUsuarioAcesso().LOGIN),
                new SqlParameter("@RECMODIFIEDON", DateTime.Now),
                new SqlParameter("@FINALIZADORELATOR", 1),
                new SqlParameter("@CODCOLIGADA", CODCOLIGADA),
                new SqlParameter("@CODCCUSTO", CODCCUSTO),
                new SqlParameter("@CODNCONFORM", CODNCONFORM),
            };

            return Funcoes.ExecutaFuncao(sqlQuery, parameters);
        }
    }
}