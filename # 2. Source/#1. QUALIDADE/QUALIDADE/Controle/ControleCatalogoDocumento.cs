﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using Syncfusion.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    internal class ControleCatalogoDocumento : Controle<DCATALOGODOCUMENTOS>
    {
        public int GeraNovoCodigoAnexo(string codCatalogo)
        {
            int codigo = 1;
            string query = $@"SELECT (ISNULL(MAX(CODIGOSEQ),0)+1) AS CODIGOSEQ FROM DCATALOGODOCUMENTOSANEXO WHERE CODCATALOGO='{codCatalogo}' ";
            DataTable dataTable = Funcoes.RetornaDataTable(query);

            if (dataTable != null && dataTable.Rows.Count > 0)
                codigo = Convert.ToInt32(dataTable.Rows[0]["CODIGOSEQ"]);

            return codigo;
        }

        public bool Insert(CatalogoDocumentos item)
        {
            try
            {
                string query = $@"INSERT INTO DCATALOGODOCUMENTOS(
                    CODIGO, CODITEMPAI, DESCRICAO, ORDEM, CRIADOPOR, CRIADOEM
                ) VALUES(
                    @CODIGO, @CODITEMPAI, @DESCRICAO, @ORDEM, @CRIADOPOR, @CRIADOEM
                )";

                List<SqlParameter> parametros = new List<SqlParameter>
                {
                    new SqlParameter("@CODIGO", item.CODIGO),
                    new SqlParameter("@CODITEMPAI", item.CODITEMPAI),
                    new SqlParameter("@DESCRICAO", item.DESCRICAO),
                    new SqlParameter("@ORDEM", item.ORDEM),
                    new SqlParameter("@CRIADOPOR", item.CRIADOPOR),
                    new SqlParameter("@CRIADOEM", item.CRIADOEM)
                };

                Funcoes.ExecutaFuncao(query, parametros);
                return true;
            }
            catch (Exception ex)
            {
                Global.MsgErro($"Falha ao inserir registro.\n{ex.Message}");
                return false;
            }
        }

        public bool Update(CatalogoDocumentos item)
        {
            try
            {
                string query = $@"UPDATE DCATALOGODOCUMENTOS SET 
                    DESCRICAO=@DESCRICAO, CODITEMPAI=@CODITEMPAI, ORDEM=@ORDEM, 
                    MODIFICADOPOR=@MODIFICADOPOR, MODIFICADOEM=@MODIFICADOEM 
                WHERE CODIGO=@CODIGO";

                List<SqlParameter> parametros = new List<SqlParameter>
                {
                    new SqlParameter("@CODIGO", item.CODIGO),
                    new SqlParameter("@CODITEMPAI", item.CODITEMPAI),
                    new SqlParameter("@DESCRICAO", item.DESCRICAO),
                    new SqlParameter("@ORDEM", item.ORDEM),
                    new SqlParameter("@MODIFICADOPOR", item.MODIFICADOPOR),
                    new SqlParameter("@MODIFICADOEM", item.MODIFICADOEM)
                };

                Funcoes.ExecutaFuncao(query, parametros);
                return true;
            }
            catch (Exception ex)
            {
                Global.MsgErro($"Falha ao atualizar registro.\n{ex.Message}");
                return false;
            }
        }

        public bool Delete(string codigo)
        {
            try
            {
                string query = $@"DELETE FROM DCATALOGODOCUMENTOS WHERE CODIGO=@CODIGO";

                List<SqlParameter> parametros = new List<SqlParameter>
                {
                    new SqlParameter("@CODIGO", codigo)
                };

                Funcoes.ExecutaFuncao(query, parametros);
                return true;
            }
            catch (Exception ex)
            {
                Global.MsgErro($"Falha ao exluir registro.\n{ex.Message}");
                return false;
            }
        }

        public new DataTable GetAll()
        {
            string query = $@"SELECT 
	            S.CODIGO, S.DESCRICAO, S.CODITEMPAI, P.DESCRICAO AS DESCITEMPAI, 
                S.ORDEM, S.CRIADOPOR, S.CRIADOEM, S.MODIFICADOPOR, S.MODIFICADOEM 
            FROM DCATALOGODOCUMENTOS S 
            LEFT JOIN DCATALOGODOCUMENTOS P ON(P.CODIGO=S.CODITEMPAI) 
            ORDER BY S.CODIGO, P.ORDEM, S.DESCRICAO";

            return Funcoes.RetornaDataTable(query);
        }

        public List<CatalogoDocumentos> GetallList()
        {
            List<CatalogoDocumentos> itens = new List<CatalogoDocumentos>();
            DataTable dtItens = GetAll();

            if (dtItens != null && dtItens.Rows.Count > 0)
            {
                foreach (DataRow row in dtItens.Rows)
                {
                    itens.Add(new CatalogoDocumentos
                    {
                        CODIGO = Convert.ToString(row["CODIGO"]),
                        CODITEMPAI = row["CODITEMPAI"].DBNullToString(),
                        DESCRICAO = row["DESCRICAO"].DBNullToString(),
                        DESCITEMPAI = row["DESCITEMPAI"].DBNullToString(),
                        ORDEM = row["ORDEM"].DBNullToInt() ?? 0,
                        CRIADOPOR = row["CRIADOPOR"].DBNullToString(),
                        CRIADOEM = row["CRIADOEM"].DBNullToDateTime(),
                        MODIFICADOPOR = row["MODIFICADOPOR"].DBNullToString(),
                        MODIFICADOEM = row["MODIFICADOEM"].DBNullToDateTime(),
                    });
                }
            }

            if(itens != null && itens.Count > 0)
            {
                for(int i = 0; i < itens.Count; i++)
                {
                    itens[i].ITENSFILHOS = itens.Where(x => x.CODITEMPAI == itens[i].CODIGO).ToList();
                }
            }

            return itens.OrderBy(x => x.CODIGO).ThenBy(x => x.ORDEM).ToList();
        }

        public DataRow GetItem(string codigo)
        {
            string query = $@"SELECT S.* FROM DCATALOGODOCUMENTOS S 
            WHERE S.CODIGO=@CODIGO ";

            List<SqlParameter> parametros = new List<SqlParameter>
            {
                new SqlParameter("@CODIGO", codigo),
            };

            DataTable dataTable = Funcoes.RetornaDataTable(query, parametros);
            return dataTable != null && dataTable.Rows.Count > 0 ? dataTable.Rows[0] : null;
        }

        public CatalogoDocumentos GetModel(string codigo)
        {
            CatalogoDocumentos item = null;
            DataRow row = GetItem(codigo);
            if (row != null)
            {
                item = new CatalogoDocumentos
                {
                    CODIGO = Convert.ToString(row["CODIGO"]),
                    CODITEMPAI = row["CODITEMPAI"].DBNullToString(),
                    DESCRICAO = row["DESCRICAO"].DBNullToString(),
                    ORDEM = row["ORDEM"].DBNullToInt() ?? 0,
                    CRIADOPOR = row["CRIADOPOR"].DBNullToString(),
                    CRIADOEM = row["CRIADOEM"].DBNullToDateTime(),
                    MODIFICADOPOR = row["MODIFICADOPOR"].DBNullToString(),
                    MODIFICADOEM = row["MODIFICADOEM"].DBNullToDateTime(),
                };
            }
            return item;
        }

        public string GeraNovoCodigo()
        {
            string codigo = "0000000001";
            string query = $@"SELECT (ISNULL(MAX(CODIGO),0)+1) AS CODIGO FROM DCATALOGODOCUMENTOS";
            DataTable dataTable = Funcoes.RetornaDataTable(query);

            if (dataTable != null && dataTable.Rows.Count > 0)
                codigo = Convert.ToString(Convert.ToInt32(dataTable.Rows[0]["CODIGO"])).PadLeft(10, '0');

            return codigo;
        }

        public List<CatalogoDocumentosAnexo> RetornaListaAnexos(string codBemVeiculo)
        {
            string query = @"SELECT * FROM DCATALOGODOCUMENTOSANEXO WHERE CODCATALOGO=@CODCATALOGO";

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODCATALOGO", codBemVeiculo)
            };

            List<CatalogoDocumentosAnexo> anexos = new List<CatalogoDocumentosAnexo>();
            DataTable dt = Funcoes.RetornaDataTable(query, parameters);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    CatalogoDocumentosAnexo anexo = new CatalogoDocumentosAnexo
                    {
                        CKSELECIONADO = false,
                        CODCATALOGO = codBemVeiculo,
                        ARQUIVOANEXO = item["ARQUIVOANEXO"].DBNullToBytes(),
                        NOMEANEXO = item["NOMEANEXO"].DBNullToString(),
                        EXTENSAO = item["EXTENSAO"].DBNullToString(),
                        CODIGOSEQ = Convert.ToInt16(item["CODIGOSEQ"]),
                        CAMINHO = null,
                        DATAEMISSAO = item["DATAEMISSAO"].DBNullToDateTime(),
                        DATAREVISAO = item["DATAREVISAO"].DBNullToDateTime(),
                    };
                    anexos.Add(anexo);
                }
            }
            return anexos.OrderBy(x => x.NOMEANEXO).ToList();
        }

        public bool IncluiAnexo(CatalogoDocumentosAnexo anexo)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@CODCATALOGO", anexo.CODCATALOGO.ToNullableParam()),
                    new SqlParameter("@CODIGOSEQ", anexo.CODIGOSEQ.ToNullableParam()),
                    new SqlParameter("@NOMEANEXO", anexo.NOMEANEXO.ToNullableParam()),
                    new SqlParameter("@EXTENSAO", anexo.EXTENSAO.ToNullableParam()),
                    new SqlParameter("@ARQUIVOANEXO", anexo.ARQUIVOANEXO.ToNullableParam()),
                    new SqlParameter("@DATAEMISSAO", anexo.DATAEMISSAO.ToNullableParam()),
                    new SqlParameter("@DATAREVISAO", anexo.DATAREVISAO.ToNullableParam())
                };

                string query = @"INSERT INTO DCATALOGODOCUMENTOSANEXO
                (
                    CODCATALOGO, CODIGOSEQ, DATAEMISSAO, DATAREVISAO, 
                    NOMEANEXO, EXTENSAO, ARQUIVOANEXO
                ) VALUES(
                    @CODCATALOGO, @CODIGOSEQ, @DATAEMISSAO, @DATAREVISAO, 
                    @NOMEANEXO, @EXTENSAO, @ARQUIVOANEXO
                )";

                Funcoes.ExecutaFuncao(query, parameters);
                return true;
            }
            catch (Exception ex)
            {
                Global.MsgErro("Falha incluir anexo.\n" + ex.Message);
                return false;
            }
        }

        public bool ExcluiAnexo(CatalogoDocumentosAnexo anexo)
        {
            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>
                {
                    new SqlParameter("@CODCATALOGO", anexo.CODCATALOGO),
                    new SqlParameter("@CODIGOSEQ", anexo.CODIGOSEQ)
                };
                string query = @"DELETE FROM DCATALOGODOCUMENTOSANEXO 
                    WHERE CODCATALOGO=@CODCATALOGO AND CODIGOSEQ=@CODIGOSEQ";
                Funcoes.ExecutaFuncao(query, parameters);
                return true;
            }
            catch (Exception ex)
            {
                Global.MsgErro("Falha excluir anexo.\n" + ex.Message);
                return false;
            }
        }
    }
}