﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleJPerguntas : Controle<JPERGUNTAS>
    {
        public int GetNewID()
        {
            int newId = 0;
            var usuarios = (from f in GetAll()
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }
    }
}