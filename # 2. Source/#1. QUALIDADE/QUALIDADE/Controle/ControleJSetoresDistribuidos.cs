﻿using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleJSetoresDistribuidos : Controle<JSETORESDISTRIBUICAO>
    {
        public int GetNewID()
        {
            int newId = 0;
            var usuarios = (from f in GetAll()
                            orderby f.CODIGO descending
                            select f).FirstOrDefault();

            if (usuarios != null)
            {
                int? item = usuarios?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<JSETORESDISTRIBUICAO> GetList(int codProjeto, decimal revisao)
        {
            List<JSETORESDISTRIBUICAO> list = new List<JSETORESDISTRIBUICAO>();
            var result = from j in GetAll()
                         where j.CODPROJETO == codProjeto &&
                            j.REVISAO == revisao
                         select j;

            if (result != null)
            {
                foreach (var item in result.ToList())
                {
                    JSETORES setor = context.JSETORES.Find(item.CODSETOR);
                    if (setor != null)
                        item.SETOR = setor.DESCRICAO;

                    list.Add(item);
                }
            }

            list.ForEach(x => x.REVISAO = (revisao + 1.0M));
            return list;
        }
    }
}