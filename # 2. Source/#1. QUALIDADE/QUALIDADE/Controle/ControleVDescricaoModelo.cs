﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleVDescricaoModelo : Controle<VDESCRICAOMODELO>
    {
        public List<Inspecao> GetList(string codigo)
        {
            List<Inspecao> list = new List<Inspecao>();
            string sql = @"SELECT * FROM VDESCRICAOMODELO WHERE CODMODELO=@CODIGO";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODIGO", codigo)
            };

            DataTable dt = Funcoes.RetornaDataTable(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow it in dt.Rows)
                {
                    Inspecao inspecao = new Inspecao
                    {
                        item = it["ITEM"].DBNullToString(),
                        Tolerencia = it["TOLERANCIA"].DBNullToString(),
                        Descricao = it["DESCRICAO"].DBNullToString(),
                    };

                    list.Add(inspecao);
                }
            }

            return list;
        }

        public bool Delete(string codigo)
        {
            string sql = @"DELETE FROM VDESCRICAOMODELO WHERE CODMODELO=@CODIGO";
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@CODIGO", codigo)
            };

            return Funcoes.ExecutaFuncao(sql, parameters);
        }

        public bool AllCreate(List<VDESCRICAOMODELO> list)
        {
            List<SqlCommand> commands = new List<SqlCommand>();
            SqlCommand cmd;

            try
            {
                foreach (VDESCRICAOMODELO item in list)
                {
                    if (!string.IsNullOrEmpty(item.ITEM))
                    {
                        cmd = new SqlCommand();
                         cmd.CommandText = $@"
                        INSERT INTO [VDESCRICAOMODELO]
                                   ([CODMODELO],[ITEM]
                                   ,[DESCRICAO],[TOLERANCIA]
                                   ,[RECCREATEDBY],[RECCREATEDON])
                        VALUES
                                   (@CODMODELO, @ITEM
                                   , @DESCRICAO, @TOLERANCIA
                                   , @RECCREATEDBY, @RECCREATEDON)";

                        cmd.Parameters.AddWithValue("@CODMODELO", item.CODMODELO);
                        cmd.Parameters.AddWithValue("@ITEM", item.ITEM);
                        cmd.Parameters.AddWithValue("@DESCRICAO", item.DESCRICAO);
                        cmd.Parameters.AddWithValue("@TOLERANCIA", item.TOLERANCIA.ToNullableParam());
                        cmd.Parameters.AddWithValue("@RECCREATEDBY", item.RECCREATEDBY.ToNullableParam());
                        cmd.Parameters.AddWithValue("@RECCREATEDON", item.RECCREATEDON.ToNullableParam());
                    
                        commands.Add(cmd);
                    }
                }

                return Funcoes.ExecutaFuncaoTransaction(commands, 
                    $"Cad_VDESCRICAOMODELO");
            }
            catch (Exception ex)
            {
                FormMsg frm = new FormMsg(ex);
                frm.ShowDialog();
                return false;
            }
        }
    }
}
