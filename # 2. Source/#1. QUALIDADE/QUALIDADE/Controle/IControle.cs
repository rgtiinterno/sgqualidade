﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace QUALIDADE.Controle
{
    public interface IControle<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> Get(Func<TEntity, bool> predicate);

        BindingList<TEntity> GetAllObservable();

        TEntity Find(params object[] key);

        void Create(TEntity obj);

        void CreateAll(List<TEntity> obj);

        void Update(TEntity obj, Func<TEntity, bool> predicate);

        void Detach(TEntity obj);

        void Delete(Func<TEntity, bool> predicate);

        void SaveAll();

        bool Exists(Func<TEntity, bool> predicate);
    }
}
