﻿using QUALIDADE.Dominio;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleJProjetosAnexo : Controle<JPROJETOSANEXO>
    {
        public int GetNewSequencial(int codProjeto, decimal revisao)
        {
            int? newId = (from x in context.Set<JPROJETOSANEXO>()
                         where x.CODPROJETO == codProjeto && x.REVISAO == revisao
                         select x.SEQUENCIAL).Any() ?
                         (from x in context.Set<JPROJETOSANEXO>()
                           where x.CODPROJETO == codProjeto && x.REVISAO == revisao
                           select x.SEQUENCIAL).Max() : 0;

            //var itens = Get(x => x.CODPROJETO == codProjeto && x.REVISAO == revisao)
            //            orderby f.SEQUENCIAL descending
            //            select f).First();

            //if (itens != null)
            //{
            //    int? item = itens?.SEQUENCIAL;
            //    if (item.HasValue)
            //        newId = item.Value;
            //}

            return (int)newId + 1;
        }

        public List<JPROJETOSANEXO> getList(int codigo, decimal versao)
        {
            List<JPROJETOSANEXO> retorno = new List<JPROJETOSANEXO>();

            var lista = (from x in context.Set<JPROJETOSANEXO>()
                         where x.CODPROJETO == codigo && x.REVISAO == versao
                         select new 
                         {
                             SEQUENCIAL = x.SEQUENCIAL,
                             EXTENSAO = x.EXTENSAO,
                             DESCRICAO = x.DESCRICAO,
                             TAMANHO = x.TAMANHO
                         }).ToList();

            foreach( var v in lista)
            {
                JPROJETOSANEXO item = new JPROJETOSANEXO();
                item.SEQUENCIAL = v.SEQUENCIAL;
                item.EXTENSAO = v.EXTENSAO;
                item.DESCRICAO = v.DESCRICAO;
                item.TAMANHO = v.TAMANHO;
                retorno.Add(item);
            }

            return retorno;
        }
    }
}