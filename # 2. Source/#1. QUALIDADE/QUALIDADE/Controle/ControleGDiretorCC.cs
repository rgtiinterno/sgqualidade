﻿using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGDiretorCC : Controle<GDIRETORCCUSTO>
    {
        public List<GDIRETORCCUSTO> GetList(int CodColigada, string codCCusto)
        {
            List<GDIRETORCCUSTO> lista = new List<GDIRETORCCUSTO>();
            var result = from d in GetAll()
                         where d.CODCOLIGADA == CodColigada && d.CODCCUSTO == codCCusto
                         select d;

            foreach (var item in result)
            {
                GUSUARIO usuario = context.GUSUARIO.Find(item.CODDIRETOR);
                if (usuario != null)
                    item.NOMEDIRETOR = usuario.NOME;

                lista.Add(item);
            }

            return lista;
        }
    }
}