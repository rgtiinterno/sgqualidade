﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using QUALIDADE.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleAAfericaoExterna : Controle<AAFERICAOEXTERNA>
    {
        public int GetNewID(int codColigada)
        {
            int newId = 0;

            var result = (from r in context.AAFERICAOEXTERNA
                          where r.CODCOLIGADA == codColigada
                          select r).OrderByDescending(x => x.CODIGO).FirstOrDefault();

            if (result != null)
            {
                int? item = result?.CODIGO;
                if (item.HasValue)
                    newId = item.Value;
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<AAFERICAOEXTERNA> GetList(short codColigada, string codCCusto = null)
        {
            List<AAFERICAOEXTERNA> lista = new List<AAFERICAOEXTERNA>();
            List<AAFERICAOEXTERNA> projetos = GetAll().ToList();
            List<CCustoFiltro> permissoes = FormPrincipal.getUsuarioAcesso().CentrosCustoPermissao("A");

            var result = from p in projetos
                         join perm in permissoes on 
                         new { CodColigada = p.CODCOLIGADA, CodCCusto = p.CODCCUSTO } 
                         equals new { perm.CodColigada, perm.CodCCusto } 
                         where p.CODCOLIGADA == codColigada &&
                               p.REV == (
                                    from j in projetos
                                    where j.CODCOLIGADA == codColigada &&
                                          j.CODIGO == p.CODIGO 
                                    select j.REV
                               ).Max()
                         select p;


            if (!string.IsNullOrEmpty(codCCusto))
                result = result.Where(x => x.CODCCUSTO.Trim() == codCCusto).ToList();

            foreach (var item in result)
            {
                if (File.Exists($"{Directory.GetCurrentDirectory()}\\{Properties.Settings.Default.conexaoIntegracao}"))
                {
                    DataTable dt = Funcoes.RetornaDataTableIntegracao(
                        $@"SELECT * FROM FCFO WHERE CODCFO='{item.CODCFO}' AND CODCOLIGADA='0'");

                    if (dt != null && dt.Rows.Count > 0)
                        item.FORNECEDOR = dt.Rows[0]["NOMEFANTASIA"].DBNullToString();
                    else
                    {
                        FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO) ?? context.FCFO.Find(0, item.CODCFO);
                        if (fFornecedor != null)
                            item.FORNECEDOR = fFornecedor.NOMEFANTASIA;
                    }
                }
                else
                {
                    FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO) ?? context.FCFO.Find(0, item.CODCFO);
                    if (fFornecedor != null)
                        item.FORNECEDOR = fFornecedor.NOMEFANTASIA;
                }

                if (item.CODCOLCCUSTO.HasValue && !string.IsNullOrEmpty(item.CODCCUSTO))
                    item.CENTROCUSTO = context.GCENTROCUSTO.Find(item.CODCOLCCUSTO.Value, item.CODCCUSTO)?.NOME;

                lista.Add(item);
            }

            return lista;
        }

        public List<AAFERICAOEXTERNA> GetListHistorico(int codColigada, int codigo)
        {
            List<AAFERICAOEXTERNA> lista = new List<AAFERICAOEXTERNA>();
            var result = from p in GetAll()
                         where p.CODCOLIGADA == codColigada &&
                               p.CODIGO == codigo
                         orderby p.REV descending
                         select p;

            foreach (var item in result)
            {
                if (File.Exists(Properties.Settings.Default.conexaoIntegracao))
                {
                    DataTable dt = Funcoes.RetornaDataTableIntegracao(
                        $@"SELECT * FROM FCFO WHERE CODCFO='{item.CODCFO}' AND CODCOLIGADA='0'");

                    if (dt != null && dt.Rows.Count > 0)
                        item.FORNECEDOR = dt.Rows[0]["NOMEFANTASIA"].DBNullToString();
                    else
                    {
                        FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO);
                        if (fFornecedor != null)
                            item.FORNECEDOR = fFornecedor.NOME;
                    }
                }
                else
                {
                    FCFO fFornecedor = context.FCFO.Find(item.CODCOLIGADA, item.CODCFO);
                    if (fFornecedor != null)
                        item.FORNECEDOR = fFornecedor.NOME;
                }

                if (item.CODCOLCCUSTO.HasValue && !string.IsNullOrEmpty(item.CODCCUSTO))
                    item.CENTROCUSTO = context.GCENTROCUSTO.Find(item.CODCOLCCUSTO.Value, item.CODCCUSTO)?.NOME;

                lista.Add(item);
            }

            return lista;
        }
    }
}