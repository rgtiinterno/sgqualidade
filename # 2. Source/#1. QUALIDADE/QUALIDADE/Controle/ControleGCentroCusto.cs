﻿using QUALIDADE.Controle.Configuracoes;
using QUALIDADE.Dominio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace QUALIDADE.Controle
{
    public class ControleGCentroCusto : Controle<GCENTROCUSTO>
    {
        public int GetID(int codColigada)
        {
            int newId = 0;

            var cCusto = (from f in Get(x => x.CODCOLIGADA == codColigada)
                          orderby f.ID descending
                          select f).FirstOrDefault();

            if (cCusto != null)
            {
                int? item = cCusto?.ID;
                if (item.HasValue)
                    newId = Convert.ToInt32(item.Value);
            }
            return Convert.ToInt32((newId + 1));
        }

        public List<GCENTROCUSTO> GetIntegracaoRM()
        {
            List<GCENTROCUSTO> lista = new List<GCENTROCUSTO>();

            string query = @"SELECT [CODCOLIGADA]
                              ,[CODCCUSTO]
                              ,[NOME]
                              ,[CODREDUZIDO]
                              ,[ATIVO]
                              ,[ENVIASPED]
                              ,[ID]
                              ,[RECCREATEDBY]
                              ,[RECCREATEDON]
                              ,[RECMODIFIEDBY]
                              ,[RECMODIFIEDON]
                          FROM [dbo].[GCCUSTO]
                          WHERE LEN([CODCCUSTO]) <= 6";

            DataTable data = Funcoes.RetornaDataTableIntegracao(query);

            foreach (DataRow row in data.Rows)
            {
                GCENTROCUSTO cCUsto = new GCENTROCUSTO
                {
                    CODCOLIGADA = Convert.ToInt16(row["CODCOLIGADA"]),
                    NOME = row["NOME"].DBNullToString(),
                    CODREDUZIDO = row["CODREDUZIDO"].DBNullToString(),
                    CODCCUSTO = row["CODCCUSTO"].DBNullToString(),
                    ATIVO = (row["ATIVO"].DBNullToString() == "T"),
                    ENVIASPED = row["ENVIASPED"].DBNullToString(),
                    ID = Convert.ToInt32(row["ID"]),
                    RECCREATEDBY = row["RECCREATEDBY"].DBNullToString(),
                    RECCREATEDON = row["RECCREATEDON"].DBNullToDateTime(),
                    RECMODIFIEDBY = row["RECMODIFIEDBY"].DBNullToString(),
                    RECMODIFIEDON = row["RECMODIFIEDON"].DBNullToDateTime(),
                };

                lista.Add(cCUsto);
            }

            return lista;
        }

        public GUSUARIO RetornaGerenteCCusto(string codCCusto, int codColigada)
        {
            GUSUARIO user = new GUSUARIO();
            var result = (from c in GetAll()
                          where c.CODCCUSTO == codCCusto && c.CODCOLIGADA == codColigada
                          select c);

            foreach (var item in result)
                user = context.GUSUARIO.Find(item.CODUSUGERENTE);

            return user;
        }
    }
}
