﻿using System.Reflection;
using System.Runtime.InteropServices;

// As informações gerais sobre um assembly são controladas por
// conjunto de atributos. Altere estes valores de atributo para modificar as informações
// associadas a um assembly.
[assembly: AssemblyTitle("SGI")]
[assembly: AssemblyDescription("Sistema de Gestão Integrada")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RGTI - Soluções em Tecnologia")]
[assembly: AssemblyProduct("SGI - Sistema de Gestão Integrada")]
[assembly: AssemblyCopyright("Copyright © 2020 - 2025")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Definir ComVisible como false torna os tipos neste assembly invisíveis
// para componentes COM. Caso precise acessar um tipo neste assembly de
// COM, defina o atributo ComVisible como true nesse tipo.
[assembly: ComVisible(false)]

// O GUID a seguir será destinado à ID de typelib se este projeto for exposto para COM
[assembly: Guid("6b7dbb3b-27d3-4520-9ab0-b193264fbdc1")]

// As informações da versão de um assembly consistem nos quatro valores a seguir:
//
//      Versão Principal
//      Versão Secundária 
//      Número da Versão
//      Revisão
//
// É possível especificar todos os valores ou usar como padrão os Números de Build e da Revisão
// usando o "*" como mostrado abaixo:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.4.8")]
[assembly: AssemblyFileVersion("1.0.4.8")]
