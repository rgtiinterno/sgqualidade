﻿using QUALIDADE.Forms;
using QUALIDADE.Forms.Configuracoes;
using System;
using System.IO;
using System.Windows.Forms;

namespace QUALIDADE
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (!File.Exists(Properties.Settings.Default.conexaoPrincipal))
            {
                MessageBox.Show("O arquivo de configuração não foi encontrado." +
                    "\nConfigure-o para continuar.",
                    "Falha na configuração", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Application.Run(new FormConfigConexao(true));
            }
            else
                Application.Run(new FormLogin());
        }
    }
}
